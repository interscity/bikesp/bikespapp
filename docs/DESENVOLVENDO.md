# Desenvolvendo para o BikeSP

## Front-End

### Arquivos

Os arquivos dentro da pasta `app/` são todos relacionados ao aplicativo. A maioria dos arquivos onde você irá realizar mudanças estão dentro da pasta `src/`, os demais arquivos, são, em sua maioria, de configuração que só precisam ser modificados em casos muito específicos. A pasta `android/` possui arquivos relacionados à versão de Android do aplicativo que é um projeto Java que pode ser aberto no Android Studio.

### Executando

Para o correto funcionamento do app, você deve primeiro **iniciar o servidor**.

Se você for usar seu próprio dispositivo Android, conecte ele ao seu computador via USB agora. Caso contrário o emulador será iniciado. Todos os comandos mencionados abaixo esperam que você esteja na pasta `app/`

Em um terminal execute `npx react-native start` para iniciar o Metro. Depois basta teclar `a` e a compilação do aplicativo vai iniciar.

O Metro é um servidor de desenvolvimento usado pelo React-Native para prover, dentre outras coisas, *hot-reloading* que é a capacidade de ver as modificações que você fizer na UI em tempo real sem ter que recompilar o aplicativo. Nele você também pode ver os logs emitidos pelo app durante sua execução.

Apesar do *hot-reloading*, às vezes alguma modificação que você fizer pode necessitar recompilação e até que você pare o Metro e o inicie novamente usando `npx react-native start --reset-cache` (em especial modificações nas variáveis de ambiente) para ter certeza que a modificação terá efeito no aplicativo.

## Back-End

### Arquivos

Os arquivos na pasta `servidor/bikespserver/` são todos arquivos para o servidor back-end, já a pasta `servidor/vite/` é para nosso dashboard de administração. Por fim, a pasta `servidor/etl/` armazena scripts utilitários para gerenciamento dos dados.

### Executando

Basta executar

```sh
devbox run servidor # Executa o servidor em foreground
# ou
devbox services start servidor # Executa o servidor em background
```

Assim como o Metro, o flask também provê *hot-reloading* e nele você pode ver os logs sendo exibidos a cada requisição recebida pelo servidor.

O banco de dados é iniciado e encerrado automaticamente com ambos comandos como um serviço rodando em background. Para você poder ver os serviços sendo executados em background execute `devbox services attach`, isso vai iniciar a TUI do Process Compose (ferramenta usada pelo devbox para gerenciar esses serviços).

Para simplesmente ver o status dos serviços basta rodar `devbox services ls`

## Contribuindo

Primeiramente, sempre crie uma branch para cada coisa com a qual for trabalhar e sempre derive ela da `main` (exceto em raras exceções). Não precisa de ficar com pena do GitLab, pode criar branches sem medo.

Nós gostamos de fazer commits em português (idealmente com verbos no imperativo) usando o padrão [*conventional commits*](https://gist.github.com/qoomon/5dfcdf8eec66a051ecd85625518cfd13).

Dessa forma quando se ler um commit, você claramente entende o tipo de mudanças que ele trás (*feature*, refatoração, *fix*, etc), eventualmente o escopo da mudança (app, servidor, etc) e o que vai acontecer se você mergear esse commit ("Cria nova rota ...", "Corrige alinhamento do botão ...", etc)

Seria ótimo se seus commits seguissem o conceito de commit atômico, ou seja, um commit que implementa uma parte "pequena" de tudo que você está implementando na branch mas que seja funcional (ou pelo menos não esteja completamente quebrado)

Quando você terminar, execute os linters e rode as baterias de testes:

- `devbox run lint`: informa se existe algum problema de estilo ou corretude com o código que você escreveu
- `devbox run lint --fix`: corrige alguns problemas que possam ter sido apontados pelo comando acima, outros talvez necessitem correção manual. Após corrigir rode `devbox run lint` novamente e faça um novo commit
- `devbox run test`: executa a nossa bateria de testes automatizados, caso acuse algum erro, corrija e faça um novo commit

Se seu código for aprovado pelos linters e pela bateria de testes, então parabéns, sua contribuição está pronta, agora basta dar um `push` na branch e abrir seu Merge Request pelo site do GitLab

Em algum momento, algum outro desenvolvedor vai revisar seu código, sugerir correções, comentários, discutir mudanças futuras, etc. Se alguma mudança for solicitada, basta fazer um novo commit e dar outro `push`, o Merge Request é atualizado automaticamente

Quando tudo estiver certo, sua contribuição será integrada à branch main.
