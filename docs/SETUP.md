# Configurando o Projeto para Desenvolvimento

Aqui neste documento está listado um passo-a-passo com as instruções para se configurar o projeto localmente para desenvolvimento. Vale ressaltar que as instruções aqui contidas estão pensadas para usuários de GNU/Linux e para desenvolvimento Android.

## Primeiras Instruções

- Arquivos de script mencionados podem ser encontrados dentro da pasta `scripts/` se forem relativos ao projeto ou `servidor/etl` se forem relativos ao back-end
- Leia todos os `README.md` dentro das pastas que você visitar, há um motivo para eles estarem lá.
- O documento está dividido em seções, se é sua primeira vez lendo leia todas as seções principais.

## Arquitetura do Projeto

O projeto possui 3 componentes principais:

- Servidor Flask para o back-end
- Aplicativo React-Native para o front-end
- Banco de Dados PostgreSQL para os dados dos usuários

Tanto o servidor como o aplicativo dependedem de variáveis de ambiente configuradas no arquivo `.env` (chaves de API, endereços de IP, etc) que **não podem** estar no repositório. Em cada seção é explicado como povoar corretamente o arquivo `.env`.

Além disso, o projeto inteiro possui múltiplas baterias de teste, uma com testes unitários para o app e outra para o servidor, além de duas baterias de testes de integração - uma que testa apenas a integração entre o aplicativo e servidor, e outra que testa o sistema inteiro (e2e), rodando o aplicativo em um emulador (via `detox`) e uma versão do servidor rodada localmente. É importante sempre executar testes antes de abrir merge requests

## Prerequisitos

Algumas dependências podem ser necessárias para a correta configuração do ambiente de desenvolvimento, veja abaixo os comandos para algumas distros

### Debian/Ubuntu/PopOS e derivados

```bash
sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

### Arch/Manjaro e derivados

```bash
sudo pacman -Syu --needed base-devel openssl zlib xz tk
```

### Fedora/RedHat e derivados

```bash
sudo dnf install make gcc patch zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel libuuid-devel gdbm-libs libnsl2
```

## Devbox

Para facilitar a instalação das ferramentas específicas e bibliotecas, fazemos uso do `devbox`. Um utilitário para criação de ambientes de desenvolvimento extremamente poderoso.

Para instalar o `devbox` execute:

```sh
curl -fsSL https://get.jetify.com/devbox | bash
```

Para fazer o setup do projeto basta:

```sh
# Configura o shell atual para desenvolvimento
# Preencha o `.env` antes
devbox shell
# Realiza uma configuração inicial das dependencias (node, java, python e instala bibliotecas)
devbox run setup
```

## Direnv

O `direnv` é um utilitário de terminal que permite que uma série de configurações sejam aplicadas ao terminal no momento em que um dado diretório é aberto. O `direnv` aqui é configurado pelo arquivo `.envrc` e está integrado ao `devbox`. Assim eles garantem que ao você abrir o diretório do projeto todas configurações necessárias sejam aplicadas para a criação de um ambiente de desenvolvimento coerente e funcional.

Para instalar o `direnv` verifique a documentação completa em https://direnv.net/docs/installation.html. Mas para a maioria das distros a instalação é simples:

```sh
# Ubuntu/Debian e derivados
sudo apt install direnv
# Arch Linux/Manjaro e derivados
sudo pacman -Syu direnv
# Fedora/Red Hat e derivados
sudo dnf install direnv
```

Para configurar, verifique a documentação completa em https://direnv.net/docs/hook.html. Mas para a maioria dos shels a configuração é simples, basta copiar o comando correspondente e colocar no final do arquivo indicado:

- Bash, adicione em **~/.bashrc**: `eval "$(direnv hook bash)"`
- Zsh, adicione em **~/.zshrc**: `eval "$(direnv hook zsh)"`
- Fish, adicione em **~/.config/fish/config.fish**: `direnv hook fish | source`

Com essa configuração, sempre que você entrar no diretório do projeto do BikeSP o comando `devbox shell` será executado automaticamente.

## Front-End

Como mencionado acima, usamos React-Native 0.71.13 para a criação do aplicativo que é o front-end para o cliente.

### Dependências

- JDK 17 (devbox)
- Android SDK 34
- Node 18 (devbox)

#### Android SDK 34

O Android SDK é um conjunto de dependências usadas para o desenvolvimento de aplicativos Android. Para instalar usamos o utilitário SDK Manager disponibilizado pela JetBrains com o Android Studio.

Após instalar o Android Studio siga os passos abaixo:

- Certifique que a pasta `~/Android/Sdk` existe
- Adicione `export ANDROID_HOME="${HOME}/Android/Sdk"` ao seu `~/.bashrc`
- Adicione também `export PATH="${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/emulator:${ANDROID_HOME}/cmdline-tools/latest/bin:${PATH}"`
- Abra um novo terminal ou execute `source ~/.bashrc` para que as modificações feitas sejam aplicadas no terminal que você está usando.

Vamos instalar os pacotes de desenvolvimento Android necessários, abra o Android Studio, vá no SDK Manager e instale os seguintes pacotes:

- SDK Platforms > Android 14.0 > _Android SDK Platform 34_, _Sources for Android 34_ e _Intel x86_64 Atom System Image_
- SDK Tools > _Android SDK Build-Tools_ > 34.0.0
- SDK Tools > _Android Emulator_, _Android Emulator hypervisor driver_ e _Android SDK Platform-Tools_.

#### dotenv

O app depende da configuração das seguintes variáveis de ambiente no `.env`:

```bash
REACT_APP_PK_MAPBOX= # Chave pública da API do MapBox
REACT_APP_SK_MAPBOX= # Chave privada da API do MapBox
SERVER_HOST= # Sempre use o endereço do servidor local que
ENCRIPT_PK= # Chave de criptografia usada no banco de dados
CHAVE_INTEGRIDADE= # Chave usada para assinar as viagens enviadas
```

Solicite a algum membro da equipe para que te passe, por algum canal privado, as informações para preencher o `.env`.

#### Emulador

Dentre os pacotes instalados com o `sdkmanager` você tem o `emulator` que é um utilitário de terminal para subir um emulador de android. Assim que você iniciar a compilação do app sem um dispositivo Android conectado o emulador vai subir automaticamente.

Crie um emulador usando o AVD Manager do Android Studio certificando-se de usar a versão correta da SDK (34).

De agora em diante, o emulador será iniciado automaticamente sempre que você iniciar o aplicativo sem um dispositivo conectado via USB. É importante ressaltar que o emulador é pesado e é necessário no mínimo 8GB de RAM para utilizar (ainda assim pode necessitar de swap).

#### Conectando dispositivo Android

1. Ative a depuração USB no seu dispositivo.
   - Isso normalmente é feito indo em "Configurações" -> "Sobre" -> "Informação de Software" e tocando 7 vezes em "Versão de Compilação", isso ativa as "Ferramentas de Desenvolvedor".
   - Depois disso vá em "Configurações" -> "Ferramentas de Desenvolvedor" e ative a opção "Depuração USB".
   - Sempre que um computador for conectado ao seu dispositivo ele irá perguntar se você deseja permitir a depuração USB.
   - Se você não conseguiu encontrar os caminhos supracitados, pesquise no google "Como ativar a depuração USB em ..." para o seu dispositivo Android.
2. Descubra o código de fabricante do seu dispositivo.
   - Use o utilitário de terminal `lsusb` para ver todos dispositivos conectados.
   - Em seguida, conecte seu dispositivo usando o cabo USB
   - Rode novamente `lsusb` e descubra qual é o seu dispositivo
   - Na linha correspondente ao seu dispositivo haverá um campo da forma `xxxx:yyyy` ao lado do texto `ID`, o valor `xxxx` é o código de fabricante que precisamos saber
3. Modifique suas regras de dispositivos USB (`udev`)
   - Execute `echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="xxxx", MODE="0666", GROUP="plugdev"' | sudo tee /etc/udev/rules.d/51-android-usb.rules` substituindo `xxxx` pelo código de fabricante descoberto no passo anterior
4. Verifique usando o Android Debug Bridge (`adb`)
   - Certifique-se que seu dispositivo está devidamente conectado ao computador
   - Execute `adb devices` para ter a lista de dispositivos conectados
   - Se o comando retornar algo abaixo de `List of devices attached` e essa linha tiver a palavra `device` na coluna direita, então tudo está pronto.

## Back-End

O back-end é o responsável por dar vida ao aplicativo e gerenciar os dados produzidos pelos nossos usuários. Nossa stack de back end utiliza Python e a biblioteca Flask para a criação de um servidor que funciona como uma API não muito complexa.

### Dependências

- Python 3.10 (devbox)

### Configuração

#### dotenv

As seguintes variáveis de ambiente são necessárias para a correta execução do back-end

```bash
# Em comum com o app
ENCRIPT_PK=""
CHAVE_INTEGRIDADE=""
# Configurações do servidor
IGNORA_USER_AUTH_SALVANDO_VIAGEM=""
SECRET_KEY_FLASK=""
INICIO_PILOTO_TIMESTAMP=""
MIN_VERSAO_APP=""

# Usuário do dashboard de administração local
ADM_USER=""
ADM_PASS=""

# Banco de dados a ser usado pelo servidor
BD_NOME=""
BD_USUARIO=""
BD_SENHA=""
BD_HOST=""

# Banco de dados a ser usado pela suit de testes
BD_NOME_TESTE=""
BD_USUARIO_TESTE=""
BD_SENHA_TESTE=""

# ETL config
CAMINHO_RESPOSTAS_FORMS=""
TOMTOM_API_KEY=""
SENHA_GOOGLE=""

# Usado apenas para configurar o envio de emails
MAIL_USERNAME=""
MAIL_PASSWORD=""
SMOKE_TESTS_RECIPIENTS=""
SCHEME=""

# Usado pela lógica de validação de viagens
INVALIDA_EMULADORES="True"

# EXPORTAÇÃO DO LIMESURVEY --
USUARIO_LIMESURVEY=""
SENHA_LIMESURVEY=""

# Docker Compose Deployment
SERVER_DEPLOYMENT_VERSION=""
DATABASE_FOLDER=""
SERVER_USER=""
SERVER_PASSWD=""
```

## Banco de Dados

É fundamental termos um banco de dados local para uso durante o desenvolvimento. Nossa arquitetura possui dois bancos de dados: `bancobikesp` (usado pelo servidor) e `bancotestesbikesp` (usado pela suite de testes). Lembre que esses nomes são configurados no `.env`.

Usamos PostgreSQL para os bancos de dados de produção e de desenvolvimento. Ele é fornecido como um serviço pelo `devbox`

Para iniciar o banco, execute `devbox services start banco`, para encerrar execute `devbox services stop banco`. Você pode configurar em que porta executar o banco de dados com a variável `BD_PORT`.

Importante ressaltar que a _locale_ do banco deve ser `pt_BR.utf8` (isso é configurado por padrão com o `devbox`)

### Configurando os DBs

Os comandos a seguir assumem que você está dentro da pasta `servidor/`

Você certamente vai precisar de testar o aplicativo, para isso, preencha o [formulário de cadastro](https://survey.ccsl.ime.usp.br/limesurvey/index.php/612292?lang=pt-BR) do BikeSP para que possamos prosseguir.

1. Solicite que alguém lhe envie o .csv contendo suas respostas
2. Crie uma coorte com `python etl/respostas_forms/set_grupo_pesquisa.py -o insercao -g 1 -v 0.22` (`1` é a id da coorte e `0.22` é a remuneração por km das pessoas dessa coorte)
3. Execute `python etl/respostas_forms/exporta_insere.py -csv caminho/do/seu.csv` para adicionar o seu usuário no banco de dados de produção
4. Agora você possui um usuário dentro do banco de dados, basta completar o cadastro dentro da tela "cadastrar" dentro do aplicativo

### Nota sobre os DBs

Os banco de dados criados acima são apenas para o desenvolvimento do back-end. Um dos bancos é usado pelo servidor e o outro apenas pela suite de testes. Portanto, nenhuma modificação feita nesses bancos afeta o banco de produção.

### Restaurando Bancos de Dados

Essa parte do tutorial é útil quando alguém for migrar um banco de dados de um cluster para outro.

Primeiramente, use `pg_dump -h <hostname> -p <port> -U <username> -Fc -b -v -f <dumpfilelocation.sql> -d  <database_name>` para criar um arquivo de dump. Esse arquivo será usado posteriormente.

Quando for criar um novo cluster para o banco, atente-se ao _locale_, ele deve ser `pt_BR.utf8` e configurar isso será diferente dependendo de como você está criando esse banco (localmente, container, etc). Tome cuidado também com o fuso horário que deve ser `America/Sao_Paulo` (ou qualquer outro de onde esse projeto esteja sendo executado). Isso está configurado corretamente no Dockerfile em `servidor/bikespdb/Dockerfile.banco`

Após criar o novo cluster crie um banco.

Por fim, use `pg_restore -v -h <hostname> -p <port> -U <username> -d <database_name> -j 2 <dumpfilelocation.sql>` para carregar o dump gerado anteriormente no banco recém criado.

Mais informações sobre migração podem ser encontradas em [https://docs.aws.amazon.com/dms/latest/sbs/chap-manageddatabases.postgresql-rds-postgresql-full-load-pd_dump.html]

## Migrando do Ambiente Antigo

Se você usava o ambiente de desenvolvimento antigo do BikeSP anterior à adoção do devbox aqui vai um tutorial de como migrar seu ambiente e seu workflow.

### Dependências Locais

Aqui vai uma lista de coisas que você não precisa mais ter instalado localmente:

- Pyenv e Poetry: agora o devbox configura automaticamente um virtualenv com a versão do python 3.10.15
- Node.js: a versão do Node é controlada pelo devbox e independe do seu sistema
- PostgreSQL: o devbox configura o banco completamente e de forma independente do seu sistema, você pode até configurar em que porta ele deve rodar, apenas precisamos de povoar ele com seus dados
- Docker: apenas vamos usar no ambiente de produção

### .env

Uma modificação que você tem que fazer é fundir os seus antigos `app/.env` e `servidor/.env` em um único `.env` na raíz do projeto. Além disso preste atenção a duas novas variáveis de ambiente:

- SERVER_PORT_TESTE: a porta que o servidor de testes vai usar
- BD_PORT: a porta que o banco de dados vai usar

E a remoção da variável

- BD_HOST_TESTE

A única desvantagem é que mudanças nesse `.env` só são recarregadas se você fechar o `devbox shell` atual ou forçar o reload do `direnv` com `cd` e depois `cd -`

### Configurando o Projeto

Se você usar o direnv, então só de dar `cd` para o diretório do projeto o ambiente já está configurado, caso contrário, lembre-se de sempre estar em um `devbox shell` para poder usar as ferramentas corretamente

Execute `devbox run setup` e isso irá configurar tudo que está pendente

### Executando o Projeto

Agora para executar é tudo bem mais simples. Para iniciar o backend rode `devbox run servidor` ou `devbox services start servidor`, a segunda opção vai executar ele em background.

Quando se inicia o servidor, por qualquer um dos métodos, o banco de dados inicia e encerra automaticamente. Mas se você precisar de subir o banco manualmente, basta rodar `devbox service start banco` e usar `psql` ou alguma interface gráfica para se conectar ao banco.

Para terminar qualquer serviço use `devbox service stop <nome do serviço>` (omita o nome para terminar todos)

Para iniciar o app, basta executar `devbox run app` ou `devbox run app --reset-cache` e isso irá iniciar tanto o banco, quanto o servidor, quanto o Metro automaticamente

Antes de abrir um MR, lembre-se de usar `devbox run lint` (eventualmente `devbox run lint --fix`) e `devbox run test`
