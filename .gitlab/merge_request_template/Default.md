Mergeando: %{source_branch} -> %{target_branch}

> Não está pronto? Marque como Draft

*Descreva o que foi feito*

*Descreva o que falta fazer*

*Observações, detalhes e troques do que foi implementado*

## Verificações

- [ ] rebaseou em %{target_branch}
- [ ] executou os linters
- [ ] passou nas baterias de teste
- [ ] testou o app em emulador
- [ ] testou o app em dispositivo físico
- [ ] testou integração entre app e servidor