# BikeSP

[![Built with Devbox](https://www.jetify.com/img/devbox/shield_galaxy.svg)](https://www.jetify.com/devbox/docs/contributor-quickstart/)

Em 2016, a prefeitura de São Paulo aprovou uma lei que visva promover o uso de bicicletas como meio de transporte através do incentivo monetário a ciclistas na forma do programa Bike SP. Como parte da pesquisa para embasar a implementação dessa política desenvolvemos um projeto piloto, cuja execução demanda um aplicativo que registre e valide viagens de bicicleta feitas pelos usuários, para que possamos posteriormente remunerá-los por elas.

A fim de trazer esse aplicativo à realidade, o grupo de pesquisa do [InterSCity](https://interscity.org/about/) se comprometeu em implementar e testar o sistema que será utilizado para o projeto piloto. Para mais informações, visite a [página do projeto piloto](https://interscity.org/bikesp/piloto/)

## Contribuindo

Além do aplicativo em si, presente em `/app`, esse repositório também conta com o servidor, presente em `/servidor`, que é o responsável principalmente pela autenticação dos usuários e pelo armazenamento e validação das viagens de bicicleta.

Para informações específicas do servidor, leia o atentamente o arquivo [servidor/README.md](servidor/README.md).
Para informações do aplicativo, leia atentamente as orientações presentes em [app/README.md](app/README.md).
Um tutorial completo para o setup do ambiente de desenvolvimento pode ser encontrado no diretório `docs`.

Para outras instruções e informações sobre desenvolvimento e arquitetura, acesse a [wiki](https://gitlab.com/interscity/bikesp/bikespapp/-/wikis/home).

### Dependências

A ordem e a maneira em que as dependências podem ser instaladas são indicadas na [documentação de setup](./docs/SETUP.md). Aqui apenas citamos as essenciais:

Devbox: https://www.jetify.com/docs/devbox/

Direnv: https://direnv.net/#basic-installation

Node.js: https://nodejs.org/en (instalado pelo devbox)

Python: https://www.python.org (instalado pelo devbox)

PostgreSQL: https://www.postgresql.org/download/ (instalado pelo devbox)

OpenJDK 17 (ou outro JDK): https://openjdk.org/projects/jdk/17/ (instalado pelo devbox)

Android Studio (SDK 34): https://developer.android.com/studio?hl=pt-br

### Setup do Ambiente de Desenvolvimento

Copie o arquivo .env.demo para criar um .env próprio (nunca faça commit desse arquivo).

```
cp .env.demo .env
```

Preencha cada um dos arquivos com as variáveis corretas e respectivas ao seu ambiente.

Certifique-se que está em um shell do `devbox` e instale as dependências

```
devbox shell
devbox run setup
```