import sys
import re
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import atualizacoes
from servidor.bikespserver.remuneracao.gera_df_envio import gera_df_envio
from servidor.tests.utils.mocks import resposta_mock_pessoa

from servidor.tests.bikespserver.endpoints.test_regViagem import mockEnvioViagem,aux_adiciona_2_users, aux_adiciona_sessao_pessoa, \
											aux_adiciona_locs_users
from servidor.tests.utils.pessoa import aux_atribui_grupo_pesquisa, aux_adiciona_pessoa, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido
from servidor.tests.utils.trajeto import cria_trajeto_str, cria_trajeto

def test_gera_df_envio(conexao_bd_testes):

	df = gera_df_envio(force_prod=False)
	assert df.empty

	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	bilhete_unico_ativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":2.4,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)
	
	con.commit()


	df = gera_df_envio(force_prod=False)
	assert df.empty


	bilhete_unico_ativo_remunerado = bilhete_unico_ativo
	bilhete_unico_ativo_remunerado["NumBilheteUnico"] = "19912412"
	bilhete_unico_ativo_remunerado["aguardandoEnvio"] = 10
	bilhete_unico_ativo_remunerado["ativo"] = True
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo_remunerado,cur)
	con.commit()

	df = gera_df_envio(force_prod=False)
	assert df.empty == False
	dic = df.to_dict("records")
	assert len(dic) == 1
	assert dic[0]["id"] == "19912412"
	assert dic[0]["valor"] == 10
	assert dic[0]["codigo"] == "691"

	bilhete_unico_nao_ativo = bilhete_unico_ativo
	bilhete_unico_nao_ativo["NumBilheteUnico"] = "124312412"
	bilhete_unico_nao_ativo["ativo"] = False
	bilhete_unico_nao_ativo["aguardandoEnvio"] = 10
	insercoes.insere_BILHETEUNICO(bilhete_unico_nao_ativo,cur)
	con.commit()

	df = gera_df_envio(force_prod=False)
	dic = df.to_dict("records")
	assert len(dic) == 1
	assert dic[0]["id"] == "19912412"
	assert dic[0]["valor"] == 10
	assert dic[0]["codigo"] == "691"


	bilhete_unico_ativo_2 = bilhete_unico_ativo
	bilhete_unico_ativo_2["NumBilheteUnico"] = "1412"
	bilhete_unico_ativo_2["aguardandoEnvio"] = 50.24
	bilhete_unico_ativo_2["ativo"] = True
	insercoes.insere_BILHETEUNICO(bilhete_unico_nao_ativo,cur)
	con.commit()

	df = gera_df_envio(force_prod=False)
	dic = df.to_dict("records")
	assert len(dic) == 2
	assert dic[1]["id"] == "1412"
	assert dic[1]["valor"] == 50.24
	assert dic[1]["codigo"] == "691"

	bilhete_unico_ativo_remunerado = bilhete_unico_ativo
	bilhete_unico_ativo_remunerado["NumBilheteUnico"] = "19912412"
	bilhete_unico_ativo_remunerado["aguardandoEnvio"] = 65
	bilhete_unico_ativo_remunerado["ativo"] = True
	atualizacoes.atualiza_bilhete_unico(bilhete_unico_ativo_remunerado,cur)
	con.commit()

	df = gera_df_envio(force_prod=False)
	assert df.empty == False
	dic = df.to_dict("records")
	assert dic[1]["id"] == "19912412"
	assert dic[1]["valor"] == 65
	assert dic[1]["codigo"] == "691"


def test_atualizacao_apos_viagem(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,[[0,0],[0.10800010800010823,0]])	
	aux_adiciona_locs_user(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_atribui_grupo_pesquisa(cur,remuneracao=8.0,id_pessoa=1,id_grupo=1)

	aux_adiciona_sessao_pessoa(con,1,mockEnvioViagem['token'])

	bilhete_unico_ativo = {
		"NumBilheteUnico":dados_pessoa["NumBilheteUnico"],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":2.4,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)

	con.commit()

	df = gera_df_envio(force_prod=False)
	assert df.empty	

	mockRemuneracaoCorreta = {
		"user": dados_pessoa["CPF"],
		"token" : mockEnvioViagem['token'],
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "trabalho",
		"destino": "destino",
		"trajeto": """ + cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0]) + """ }""")
	}

	resp = client.post("/api/regViagem/", data=mockRemuneracaoCorreta)
	resp_conteudo = resp.json
	assert resp_conteudo['remuneracao'] == 8

	df = gera_df_envio(force_prod=False)
	assert df.empty == False
	dic = df.to_dict("records")
	assert dic[0]["valor"] == 10.4
	assert dic[0]["codigo"] == "691"
