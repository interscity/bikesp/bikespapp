import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import insercoes


from servidor.bikespserver.remuneracao.confirma_envio_sptrans import confirma_envio_sptrans

from servidor.bikespserver.bd.insercoes import insere_BILHETEUNICO

#pylint: enable=wrong-import-position


def test_confirma_envio_sptrans(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)

	assert confirma_envio_sptrans("12312401",-1,cur) == (False,"VALOR <= 0",(0,0))
	assert confirma_envio_sptrans("12312401",1,cur) == (False,"BILHETE UNICO INEXISTENTE",(0,0))

	insere_BILHETEUNICO(dados_bilhete_unico,cur)
	cur.execute("UPDATE BILHETEUNICO SET aguardandoEnvio = 100")

	assert confirma_envio_sptrans(dados_bilhete_unico["NumBilheteUnico"],50,cur) == (True,"SUCESSO",(50,50))
	assert confirma_envio_sptrans(dados_bilhete_unico["NumBilheteUnico"],25,cur) == (True,"SUCESSO",(75,25))
	assert confirma_envio_sptrans(dados_bilhete_unico["NumBilheteUnico"],20,cur) == (True,"SUCESSO",(95,5))

	cur.execute("SELECT * FROM HISTORICOENVIOSPTRANS;")
	resultado = cur.fetchall()

	assert len(resultado) == 3
	assert resultado[0][2] == "R$ 50,00"
	assert resultado[1][2] == "R$ 25,00"
	assert resultado[2][2] == "R$ 20,00"

def test_confirma_envio_com_observacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insere_BILHETEUNICO(dados_bilhete_unico,cur)
	envio_1 =  {
		"NumBilheteUnico":dados_bilhete_unico["NumBilheteUnico"],
		"DataEnvio":"2021-01-01",
		"Valor":10,
		"Confirmado":False,
		"Observacao":"OBERSVACAO ALEATORIA"
	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_1,cur)
	envio_2 =  {
		"NumBilheteUnico":dados_bilhete_unico["NumBilheteUnico"],
		"DataEnvio":"2021-01-02",
		"Valor":100,
		"Confirmado":False,
		"Observacao":"ISSO NÃO É MAIS UM BONUS"
	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_2,cur)
	envio_3 =  {
		"NumBilheteUnico":dados_bilhete_unico["NumBilheteUnico"],
		"DataEnvio":"2021-01-03",
		"Valor":200,
		"Confirmado":False,
		"Observacao":None
	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_3,cur)
	
	cur.execute("UPDATE BILHETEUNICO SET aguardandoEnvio = 240")
	
	assert confirma_envio_sptrans(dados_bilhete_unico["NumBilheteUnico"],240,cur) == (True,"SUCESSO",(240,0))
	
	cur.execute("SELECT * FROM HISTORICOENVIOSPTRANS ORDER BY dataenvio ASC;")
	resultado = cur.fetchall()
	print(resultado)
	assert len(resultado) == 4
	assert resultado[0][2] == "R$ 10,00"
	assert resultado[1][2] == "R$ 100,00"
	assert resultado[2][2] == "R$ 200,00"	
	assert resultado[3][2] == "R$ 240,00"	

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": "34352",
	"Rua":"",
	"Numero":"",
	"Complemento":"",
	"CEP":"",
	"Bairro":"",
	"Cidade":"",
	"Estado":"",
	"Email": "lorwqjifwep",
	"Telefone": "",
}


dados_bilhete_unico = {
	"NumBilheteUnico":"111111111",
	"idPessoa":1,
	"dataInicio":"2023-09-25 15:33:32.987674-03:00",
	"dataFim":None,
	"ativo":True,
	"concedido":0,
	"aguardandoEnvio":0,
	"aguardandoResposta":0
}