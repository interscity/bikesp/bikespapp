import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import insercoes


from servidor.bikespserver.remuneracao import insere_remuneracao

from servidor.bikespserver.bd.insercoes import insere_BILHETEUNICO

#pylint: enable=wrong-import-position
def test_insere_remuneraca_para_enviar(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()


	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_1,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_2,cur)


	cur.execute("SELECT * FROM BILHETEUNICO WHERE idPessoa = 1 AND ativo = 't'")

	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert money_para_float(resultado[0][-2]) == 0

	insere_remuneracao.insere_remuneracao_para_enviar(1,300,cur)

	cur.execute("SELECT * FROM BILHETEUNICO WHERE idPessoa = 1 AND ativo = 't'")

	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert money_para_float(resultado[0][-2]) == 300

	insere_remuneracao.insere_remuneracao_para_enviar(1,2.23,cur)

	cur.execute("SELECT * FROM BILHETEUNICO WHERE idPessoa = 1 AND ativo = 't'")

	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert money_para_float(resultado[0][-2]) == 302.23

def money_para_float(money):
	return float(money.replace("R$ ","").replace(".","").replace(",","."))

def test_transporta_aguardando_envio_concedido(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_1,cur)

	concedido = 400
	aguardando_envio = 700
	cur.execute("UPDATE BILHETEUNICO SET (concedido,aguardandoEnvio) = (%s,%s);",[concedido,aguardando_envio])

	valor = 1
	insere_remuneracao.transporta_aguardando_envio_concedido(dados_bilhete_unico_1["NumBilheteUnico"],valor,cur)

	cur.execute("SELECT concedido,aguardandoEnvio,aguardandoResposta FROM BILHETEUNICO WHERE bilheteUnico = %s",[dados_bilhete_unico_1["NumBilheteUnico"]])
	resultado = cur.fetchone()
	assert [money_para_float(resultado[0]),money_para_float(resultado[1]),money_para_float(resultado[2])] == [concedido+valor,aguardando_envio-valor,0]

	concedido += valor
	aguardando_envio -= valor

	valor = 300
	insere_remuneracao.transporta_aguardando_envio_concedido(dados_bilhete_unico_1["NumBilheteUnico"],valor,cur)
	cur.execute("SELECT concedido,aguardandoEnvio,aguardandoResposta FROM BILHETEUNICO WHERE bilheteUnico = %s",[dados_bilhete_unico_1["NumBilheteUnico"]])
	resultado = cur.fetchone()
	assert [money_para_float(resultado[0]),money_para_float(resultado[1]),money_para_float(resultado[2])] == [concedido+valor,aguardando_envio-valor,0]

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": "34352",
	"Rua":"",
	"Numero":"",
	"Complemento":"",
	"CEP":"",
	"Bairro":"",
	"Cidade":"",
	"Estado":"",
	"Email": "lorwqjifwep",
	"Telefone": "",
}

dados_bilhete_unico_1 = {
	"NumBilheteUnico":"111111111",
	"idPessoa":1,
	"dataInicio":"2023-09-25 15:33:32.987674-03:00",
	"dataFim":None,
	"ativo":True,
	"concedido":0,
	"aguardandoEnvio":0,
	"aguardandoResposta":0
}

dados_bilhete_unico_2 = {
	"NumBilheteUnico":"134141",
	"idPessoa":1,
	"dataInicio":"2023-09-25 14:33:32.987674-03:00",
	"dataFim":"2023-09-25 15:33:32.987674-03:00",
	"ativo":False,
	"concedido":0,
	"aguardandoEnvio":800,
	"aguardandoResposta":0
}