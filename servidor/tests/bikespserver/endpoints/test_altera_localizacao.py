import sys
from pathlib import Path
from datetime import date

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.bikespserver.bd import consultas, insercoes
from servidor.tests.utils.pessoa import aux_adiciona_locs_user, aux_adiciona_pessoa, aux_adiciona_sessao
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes

endpoint = "/api/alteraLocalizacao/"
endpoint_checa = endpoint + "checa/"

mock_endereco = "Praça Charles Miller|0||Pacaembu|01234010|São Paulo|SP"
mock_coordenadas = [-23.5504533, -46.6339112]

def test_url_existe(client):
	# endpoint checa
	resp = client.post(endpoint_checa)
	assert resp.status_code != 404

	# endpoint altera
	resp = client.post(endpoint)
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 100000

	# endpoint checa
	resp = client.post(endpoint_checa, data={'user': lixao,'token':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

	# endpoint altera
	resp = client.post(endpoint, data={'user': lixao,'token':"token", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):
	# endpoint checa
	resp = client.post(endpoint_checa, data={'ewioruewo': "fwfw"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

	# endpoint altera
	resp = client.post(endpoint, data={'ewioruewo': "fwfw"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	# endpoint checa
	resp = client.post(endpoint_checa, data={"user":"11111111111","token":"tokencorreto"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

	# endpoint altera
	resp = client.post(endpoint, data={"user":"11111111111","token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	con.commit()

	# endpoint checa
	resp = client.post(endpoint_checa, data={"user":info_pessoa["CPF"],"token":"tokencorreto"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

	# endpoint altera
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	# endpoint checa
	resp = client.post(endpoint_checa, data={"user":info_pessoa["CPF"],"token":"tokenerrado"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

	# endpoint altera
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokenerrado", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_consulta_liberado(client,conexao_bd_testes):
    con = conexao_bd_testes
    cur = con.cursor()
    info_pessoa = aux_adiciona_pessoa(cur)
    aux_adiciona_sessao(cur,token="tokencorreto")
    con.commit()

    # usuário nunca alterou localização
    resp = client.post(endpoint_checa, data={"user":info_pessoa["CPF"],"token":"tokencorreto"})
    assert resp.status_code == 200
    assert resp.json["resposta"] == True

    # usuário alterou localização há mais de 30 dias
    cur.execute(
        """
        INSERT INTO ALTERACAOLOCALIZACAO (idPessoa,data) VALUES (%s,%s);
        """, ['1', '1900-01-01'])
    con.commit()
    resp = client.post(endpoint_checa, data={"user":info_pessoa["CPF"],"token":"tokencorreto"})
    assert resp.status_code == 200
    assert resp.json["resposta"] == True

def test_consulta_bloqueado(client,conexao_bd_testes):
    con = conexao_bd_testes
    cur = con.cursor()
    info_pessoa = aux_adiciona_pessoa(cur)
    aux_adiciona_sessao(cur,token="tokencorreto")
    con.commit()

    # usuário alterou localização há menos de 30 dias
    cur = insercoes.insere_ALTERACAOLOCALIZACAO(1, cur)
    con.commit()
    resp = client.post(endpoint_checa, data={"user":info_pessoa["CPF"],"token":"tokencorreto"})
    assert resp.status_code == 200
    assert resp.json["resposta"] == False
    assert resp.json["dias"] == 31

def test_altera_id_nao_existe(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"2",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 403
	assert resp.json['estado'] == "Erro"
	assert resp.json['erro'] == "A localização informada não pode ser modificada."

def test_altera_estacao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["estacao"])
	aux_adiciona_locs_user(cur,[1],1,["estacao"])
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 403
	assert resp.json['estado'] == "Erro"
	assert resp.json['erro'] == "A localização informada não pode ser modificada."

def test_altera_localizacao_nao_vinculada(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["qualquer"])
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 403
	assert resp.json['estado'] == "Erro"
	assert resp.json['erro'] == "A localização informada não pode ser modificada."

def test_altera_apelido_residencia(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["qualquer"])
	insercoes.insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":1,"apelido":"Residência"},cur)
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 403
	assert resp.json['estado'] == "Erro"
	assert resp.json['erro'] == "Não é possível alterar o apelido de uma residência."

def test_altera_sucesso(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])
	con.commit()

	cur.execute("SELECT endereco FROM LOCALIZACAO")
	localizacoes = cur.fetchall()
	assert len(localizacoes) == 1
	assert localizacoes[0][0] != mock_endereco

	cur.execute("SELECT endereco FROM PESSOA")
	endereco_antigo = cur.fetchall()
	assert len(endereco_antigo) == 1

	cur.execute("SELECT apelido FROM APELIDOLOCALIZACAO")
	apelidos = cur.fetchall()
	assert len(apelidos) == 1
	assert apelidos[0][0] != "novo apelido"

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 200
	assert resp.json['estado'] == "Sucesso"

	cur.execute("SELECT endereco FROM LOCALIZACAO")
	localizacoes = cur.fetchall()
	assert len(localizacoes) == 1
	assert localizacoes[0][0] == mock_endereco

	cur.execute("SELECT endereco FROM PESSOA")
	endereco_atual = cur.fetchall()
	assert len(endereco_atual) == 1
	assert endereco_atual == endereco_antigo

	cur.execute("SELECT apelido FROM APELIDOLOCALIZACAO")
	apelidos = cur.fetchall()
	assert len(apelidos) == 1
	assert apelidos[0][0] == "novo apelido"

	alteracao = consultas.alteracao_localizacao_existe(1,cur)
	assert alteracao != None
	assert alteracao[0] == date.today()

def test_altera_sucesso_apenas_apelido(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	insercoes.insere_LOCALIZACAO({'coordenadas':mock_coordenadas, \
		'endereco':mock_endereco,'tipoLocalizacao':"qualquer"},cur)
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])
	con.commit()

	cur.execute("SELECT endereco FROM LOCALIZACAO")
	localizacoesAntes = cur.fetchall()
	assert len(localizacoesAntes) == 1

	cur.execute("SELECT apelido FROM APELIDOLOCALIZACAO")
	apelidos = cur.fetchall()
	assert len(apelidos) == 1
	assert apelidos[0][0] != "novo apelido"

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 200
	assert resp.json['estado'] == "Sucesso"

	cur.execute("SELECT endereco FROM LOCALIZACAO")
	localizacoesDepois = cur.fetchall()
	assert len(localizacoesDepois) == 1
	assert localizacoesAntes == localizacoesDepois

	cur.execute("SELECT apelido FROM APELIDOLOCALIZACAO")
	apelidos = cur.fetchall()
	assert len(apelidos) == 1
	assert apelidos[0][0] == "novo apelido"

def test_altera_sucesso_residencia(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["qualquer"])
	insercoes.insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":1,"apelido":"Residência"},cur)
	con.commit()

	cur.execute("SELECT endereco FROM LOCALIZACAO")
	localizacoes = cur.fetchall()
	assert len(localizacoes) == 1
	assert localizacoes[0][0] != mock_endereco

	cur.execute("SELECT endereco FROM PESSOA")
	endereco_antigo = cur.fetchall()
	assert len(endereco_antigo) == 1

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"Residência"})
	assert resp.status_code == 200
	assert resp.json['estado'] == "Sucesso"

	cur.execute("SELECT endereco FROM LOCALIZACAO")
	localizacoes = cur.fetchall()
	assert len(localizacoes) == 1
	assert localizacoes[0][0] == mock_endereco

	cur.execute("SELECT endereco FROM PESSOA")
	endereco_atual = cur.fetchall()
	assert len(endereco_atual) == 1
	assert endereco_atual[0][0] == mock_endereco
	assert endereco_antigo[0][0] != mock_endereco

	cur.execute("SELECT apelido FROM APELIDOLOCALIZACAO")
	apelidos = cur.fetchall()
	assert len(apelidos) == 1
	assert apelidos[0][0] == "Residência"

	alteracao = consultas.alteracao_localizacao_existe(1,cur)
	assert alteracao != None
	assert alteracao[0] == date.today()

def test_altera_sem_autorizacao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	info_localizacao = aux_adiciona_localizacoes(cur,coordenadas=[mock_coordenadas],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1", 'novo_endereco':mock_endereco,'novo_apelido':"novo apelido"})
	assert resp.status_code == 200
	assert resp.json['estado'] == "Sucesso"

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto", \
		'id_localizacao':"1",'novo_endereco':mock_endereco,'novo_apelido':"mais novo apelido"})
	assert resp.status_code == 403
	assert resp.json['estado'] == "Erro"
	assert resp.json['erro'] == "Alteração não autorizada."
