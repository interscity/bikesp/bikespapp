import sys
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao
from servidor.tests.bikespserver.endpoints.test_gera_extrato import aux_cria_dict_envio, aux_adiciona_envio_sptrans

endpoint = "/api/exibeBonus/"

def test_url_existe(client):
	resp = client.post(endpoint)
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post(endpoint, data={'user': lixao,'token':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post(endpoint, data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	resp = client.post(endpoint, data={"user":"11111111111","token":"tokencorreto"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	con.commit()
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokenerrado"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_exibe_bonus_ok(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	token = "token"
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token=token)
	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert resp.status_code == 200
	assert resp.json["bonusUsuario"] == []

	dict_bonus = {
		"nome":"bonus1",
		"valor":"R$ 1,00",
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":datetime(2023, 10, 10, 20, 1, 20),
		"data_fim":datetime(2023, 10, 10, 20, 1, 21),
	}
	insercoes.insere_BONUS(dict_bonus,cur)

	dict_bonus["nome"] = "bonus2"
	dict_bonus["visivel"] = False
	insercoes.insere_BONUS(dict_bonus,cur)
	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert resp.status_code == 200
	assert len(resp.json["bonusUsuario"]) == 1
	assert len(resp.json["bonusUsuario"][0]) == 8
	assert resp.json["bonusUsuario"][0][-1] is None

	dict_bonus["nome"] = "bonus3"
	dict_bonus["visivel"] = True
	insercoes.insere_BONUS(dict_bonus,cur)
	dict_bonus["nome"] = "bonus4"
	insercoes.insere_BONUS(dict_bonus,cur)
	dict_bonus["nome"] = "bonus5"
	dict_bonus["visivel"] = False
	insercoes.insere_BONUS(dict_bonus,cur)
	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert len(resp.json["bonusUsuario"]) == 3
	assert len(resp.json["bonusUsuario"][0]) == 8
	assert len(resp.json["bonusUsuario"][1]) == 8
	assert len(resp.json["bonusUsuario"][2]) == 8
	assert resp.json["bonusUsuario"][0][-1] is None
	assert resp.json["bonusUsuario"][1][-1] is None
	assert resp.json["bonusUsuario"][2][-1] is None

	pessoa_bonus = {
		"idPessoa":1,
		"idBonus": 1,
		"dataConcessao":datetime(2023, 10, 10, 20, 1, 20)
	}
	insercoes.insere_PESSOABONUS(pessoa_bonus,cur)
	pessoa_bonus["idBonus"] = 3
	insercoes.insere_PESSOABONUS(pessoa_bonus,cur)
	pessoa_bonus["idBonus"] = 5
	insercoes.insere_PESSOABONUS(pessoa_bonus,cur)

	con.commit()
	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert len(resp.json["bonusUsuario"]) == 3
	assert resp.json["bonusUsuario"][0][-1] is not None
	assert resp.json["bonusUsuario"][1][-1] is not None
	assert resp.json["bonusUsuario"][2][-1] is None