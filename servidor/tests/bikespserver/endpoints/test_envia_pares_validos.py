import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.bikespserver.bd import insercoes
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido
from servidor.tests.utils.viagem import aux_adiciona_viagem

def test_url_existe(client):
	resp = client.post("/api/recebeParesValidos/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/recebeParesValidos/", data={'user': lixao,'token':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/recebeParesValidos/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	con = conexao_bd_testes

	resp = client.post("/api/recebeParesValidos/", data={"user":"11111111111","token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	dados_pessoa = aux_adiciona_tudo(con,token="")

	resp = client.post("/api/recebeParesValidos/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	dados_pessoa = aux_adiciona_tudo(con)

	resp = client.post("/api/recebeParesValidos/", data={"user":dados_pessoa["CPF"],"token":"tokenerrado"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'


def test_pares_validos_corretos(client,conexao_bd_testes):
	con = conexao_bd_testes
	dados_pessoa = aux_adiciona_tudo(con)

	resp = client.post("/api/recebeParesValidos/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})
	assert resp.status_code == 200
	assert len(resp.json["paresValidos"].keys()) == 3
	assert resp.json["paresValidos"]["1&3"]["distancia"] == 200


def aux_adiciona_tudo(con,token="tokencorreto"):
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	lista_coords = [
		[-45.12312,-48.0013],
		[-45.124,-48.011],
		[-45.123,-48.011],
		[-45.323,-48.011]
	]
	lista_tipos = [
		"qualquer",
		"qualquer",
		"estacao",
		"estacao"
	]
	aux_adiciona_localizacoes(cur,coordenadas=lista_coords,tipos=lista_tipos)
	aux_adiciona_par_valido(cur,id_localizacao_1=1,id_localizacao_2=2,distancia=10)
	aux_adiciona_par_valido(cur,id_localizacao_1=1,id_localizacao_2=3,distancia=200)
	aux_adiciona_par_valido(cur,id_localizacao_1=2,id_localizacao_2=3,distancia=300)
	aux_adiciona_par_valido(cur,id_localizacao_1=1,id_localizacao_2=4,distancia=400)
	aux_adiciona_locs_user(cur,ids_localizacoes=[1,2,3],tipos=lista_tipos)
	if token != "":
		aux_adiciona_sessao(cur,token)
	con.commit()
	cur.close()
	return dados_pessoa