import sys
import pytest
from pathlib import Path
from freezegun import freeze_time


sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes

def test_url_existe(client):
	resp = client.post("/api/checaSess/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/checaSess/", data={'email': lixao,'token':"token"})

	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/checaSess/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	resp = client.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': 'asa'})

	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

def test_nao_logado(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	resp = client.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': 'asa'})
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

def test_token_errado(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	resp_login = client.post("/api/login/", data=dados_request_login)

	resp = client.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': 'asa'})

	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

def test_token_expirado(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	resp_login = client.post("/api/login/", data=dados_request_login)

	freezer = freeze_time("2100-01-14 03:21:34", tz_offset=-3)
	freezer.start()
	
	resp = client.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': 'asa'})

	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

	freezer.stop()

def test_token_correto(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	cur = con.cursor()

	resp_login = client.post("/api/login/", data=dados_request_login)

	assert resp_login.status_code == 200
	token = resp_login.json['token']

	resp = client.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': token})

	assert resp.status_code == 200
	assert resp.json['estado'] == 'OK'

	cur.close()

@pytest.mark.parametrize('custom_env', [{"MIN_VERSAO_APP":"0.0.1"}])
def test_versao_nao_enviada(client_custom_env,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	cur = con.cursor()

	dados_request_login["versao"] = "0.0.1"
	resp_login = client_custom_env.post("/api/login/", data=dados_request_login)

	assert resp_login.status_code == 200
	token = resp_login.json['token']
	resp = client_custom_env.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': token})
	resp_json = resp.json	
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'App desatualizado'

@pytest.mark.parametrize('custom_env', [{"MIN_VERSAO_APP":"0.0.1"}])
def test_versao_desatualizada(client_custom_env,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	cur = con.cursor()

	dados_request_login["versao"] = "0.0.1"
	resp_login = client_custom_env.post("/api/login/", data=dados_request_login)

	assert resp_login.status_code == 200
	token = resp_login.json['token']
	resp = client_custom_env.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': token,"versao":"0.0.0"})
	resp_json = resp.json	
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'App desatualizado'

@pytest.mark.parametrize('custom_env', [{"MIN_VERSAO_APP":"0.0.1"}])
def test_versao_atualizada(client_custom_env,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	cur = con.cursor()

	dados_request_login["versao"] = "0.0.1"
	resp_login = client_custom_env.post("/api/login/", data=dados_request_login)

	assert resp_login.status_code == 200
	token = resp_login.json['token']
	resp = client_custom_env.post("/api/checaSess/", data={'email': dados_cadastro['email'],'token': token,"versao":"0.0.6"})
	resp_json = resp.json	
	assert resp.status_code == 200


def aux_cadastra_usuario(con):
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)
	
	print("Coloquei pessoa")
	con.commit()
	cur.close()

	cur = con.cursor()

	# cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': dados_cadastro['senha']},cur)
	print("Coloquei usuario")
	con.commit()
	cur.close()


dados_request_login = {
	'email' : 'email@email.com',
	'senha' : 'asdasidjahsdukiasdadasda'
}

dados_cadastro = {
		'email' : 'email@email.com',
		'cpf' : 11111111111,
		'bu' : 111111111,
		'senha' : 'asdasidjahsdukiasdadasda'
	}

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": dados_cadastro['cpf'],
	"Email": dados_cadastro['email'],
	"Telefone": "",
	"Rua": "",
	"Numero": "",
	"Complemento": "",
	"Bairro": "",
	"CEP": "",
	"Cidade": "",
	"Estado": "",
	"NumBilheteUnico": dados_cadastro['bu']
}

dados_bilhete_unico = {
	"NumBilheteUnico":dados_cadastro['bu'],
	"idPessoa":1,
	"dataInicio":"2023-09-25 15:33:32.987674-03:00",
	"dataFim":None,
	"ativo":True,
	"concedido":0,
	"aguardandoEnvio":0,
	"aguardandoResposta":0
}