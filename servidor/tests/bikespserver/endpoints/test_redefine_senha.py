import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.autenticacao import gerencia_token_redefinicao_senha as gerencia_redef
from servidor.bikespserver.autenticacao import gerencia_sessao
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa

def test_url_existe(client):
	resp = client.post("/esqueceuSenha/redefinirSenha/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	nova_senha = lixao
	confirma_senha = lixao
	token = "token"
	resp = client.post("/esqueceuSenha/redefinirSenha/", data={'nova_senha': nova_senha,'confirma_senha':confirma_senha,"token":token})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413
	assert resp.json['erro'] == 'Dados em excesso'

def test_request_errada(client,conexao_bd_testes):
	nova_senha = "senha"
	confirma_senha = nova_senha
	token = "token"
	resp = client.post("/esqueceuSenha/redefinirSenha/", data={'aafsafafnova_senha': nova_senha,'confirma_senha':confirma_senha,"token":token})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Requisição inválida'

def test_senhas_diferem(client,conexao_bd_testes):
	nova_senha = "senha"
	confirma_senha = nova_senha
	token = "token"
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	resp = client.post("/esqueceuSenha/redefinirSenha/", data={'nova_senha': nova_senha,'confirma_senha':"alkgalkgjlka","token":token})
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Senhas diferem'

def test_token_invalido(client,conexao_bd_testes):
	nova_senha = "senha"
	confirma_senha = nova_senha
	token = "token"
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	resp = client.post("/esqueceuSenha/redefinirSenha/", data={'nova_senha': nova_senha,'confirma_senha':confirma_senha,"token":token})
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Link de redefinição de senha expirado.'	

def test_sucesso_redefine_senha(client,conexao_bd_testes):
	nova_senha = "senha_nova"
	confirma_senha = nova_senha	
	email = resposta_mock_pessoa["Email"]
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	aux_cadastra_usuario(con,"senha_velha")

	resp = client.get("/esqueceuSenha/?email=" + email)
	token = resp.json["linkRedefinicao"].split("token=")[1] 	
	assert gerencia_redef.token_redefinicao_valido(token,cur) == True

	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()

	resp = client.post("/esqueceuSenha/redefinirSenha/", data={'nova_senha': nova_senha,'confirma_senha':confirma_senha,"token":token})
	assert resp.status_code == 200

	resp = client.post("/api/login/", data={"email":email,"senha":"senha_velha"})
	assert resp.status_code == 401

	resp = client.post("/api/login/", data={"email":email,"senha":"senha_nova"})
	assert resp.status_code == 200
	resp_json = resp.json	
	assert gerencia_sessao.token_valido(1,resp_json["token"],cur) == True

	assert gerencia_redef.token_redefinicao_valido(token,cur) == False

def test_get_redefine_senha(client,conexao_bd_testes):
	nova_senha = "senha_nova"
	confirma_senha = nova_senha	
	email = resposta_mock_pessoa["Email"]
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	aux_cadastra_usuario(con,"senha_velha")

	resp = client.get("/esqueceuSenha/?email=" + email)
	token = resp.json["linkRedefinicao"].split("token=")[1] 	

	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()

	resp = client.get("/esqueceuSenha/redefinirSenha/")
	assert resp.status_code == 404

	resp = client.get("/esqueceuSenha/redefinirSenha/?token=errado")
	assert resp.status_code == 200
	assert "Link de redefinição de senha expirado" in resp.text

	resp = client.get("/esqueceuSenha/redefinirSenha/?token=" + token)
	assert resp.status_code == 200
	assert "Link de redefinição de senha expirado" not in resp.text

def aux_cadastra_usuario(con,senha="senha"):
	cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': senha},cur)
	con.commit()
