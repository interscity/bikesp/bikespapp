import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao

def test_url_existe(client):
	resp = client.post("/api/geraExtrato/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/geraExtrato/", data={'user': lixao,'token':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/geraExtrato/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):

	resp = client.post("/api/geraExtrato/", data={"user":"11111111111","token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	con.commit()
	resp = client.post("/api/geraExtrato/", data={"user":info_pessoa["CPF"],"token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'


def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post("/api/geraExtrato/", data={"user":info_pessoa["CPF"],"token":"tokenerrado"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_geracao_extrato_bu(client,conexao_bd_testes):

	con = conexao_bd_testes

	token = "token"
	aux_adiciona_usuario(con,token,dados_vazios_pessoa)

	resp = client.post("/api/geraExtrato/",data={"user":dados_cadastro["cpf"],"token":token})

	assert resp.json["bilheteUnico"] == []
	assert resp.json["creditos"] == []

	bilhetes_unicos = [
		aux_cria_dict_bilhete_unico("302432",1,"2023-10-10",None,True,10,30,0),
		aux_cria_dict_bilhete_unico("302232432",1,"2023-11-10",None,False,200,0,0),
		aux_cria_dict_bilhete_unico("213132",1,"2022-01-01","2023-10-10",False,0.32,0,0)
	]

	aux_adiciona_bilhetes_unicos(con,bilhetes_unicos)

	resp = client.post("/api/geraExtrato/",data={"user":dados_cadastro["cpf"],"token":token})
	assert len(resp.json["bilheteUnico"]) == len(bilhetes_unicos)
	assert resp.json["bilheteUnico"][0][0] == "302432"
	assert resp.json["bilheteUnico"][1][0] == "302232432"
	assert resp.json["bilheteUnico"][2][0] == "213132"
	assert len(resp.json["bilheteUnico"][0]) == 6
	assert resp.json["creditos"] == []

	aux_adiciona_usuario(con,"token2",dados_vazios_pessoa_2,id_pessoa=2)

	bilhete_outro = [
		aux_cria_dict_bilhete_unico("666",2,"2023-10-10",None,True,10,30,0),
	]

	aux_adiciona_bilhetes_unicos(con,bilhete_outro)

	resp = client.post("/api/geraExtrato/",data={"user":dados_cadastro["cpf"],"token":token})
	assert len(resp.json["bilheteUnico"]) == len(bilhetes_unicos)
	assert resp.json["bilheteUnico"][0][0] == "302432"
	assert resp.json["bilheteUnico"][1][0] == "302232432"
	assert resp.json["bilheteUnico"][2][0] == "213132"
	assert len(resp.json["bilheteUnico"][1]) == 6

def test_geracao_credito(client,conexao_bd_testes):
	con = conexao_bd_testes

	token = "token"
	aux_adiciona_usuario(con,token,dados_vazios_pessoa)

	bilhetes_unicos = [
		aux_cria_dict_bilhete_unico("302432",1,"2023-10-10",None,True,10,30,0),
		aux_cria_dict_bilhete_unico("302232432",1,"2023-11-10",None,False,200,0,0),
		aux_cria_dict_bilhete_unico("213132",1,"2022-01-01","2023-10-10",False,0.32,0,0)
	]

	aux_adiciona_bilhetes_unicos(con,bilhetes_unicos)

	resp = client.post("/api/geraExtrato/",data={"user":dados_cadastro["cpf"],"token":token})
	assert len(resp.json["bilheteUnico"]) == len(bilhetes_unicos)
	assert resp.json["bilheteUnico"][0][0] == "302432"
	assert resp.json["bilheteUnico"][1][0] == "302232432"
	assert resp.json["bilheteUnico"][2][0] == "213132"
	assert len(resp.json["bilheteUnico"][1]) == 6

	envios_sptrans = [
		aux_cria_dict_envio("302432","2023-10-25",400,True),
		aux_cria_dict_envio("213132","2023-10-29",100,False),
		aux_cria_dict_envio("302432","2023-10-26",300,True),
		aux_cria_dict_envio("302432","2023-10-27",200,True)
	]

	aux_adiciona_envio_sptrans(con,envios_sptrans)

	resp = client.post("/api/geraExtrato/",data={"user":dados_cadastro["cpf"],"token":token})
	assert len(resp.json["bilheteUnico"]) == len(bilhetes_unicos)
	assert resp.json["bilheteUnico"][0][0] == "302432"
	assert resp.json["bilheteUnico"][1][0] == "302232432"
	assert resp.json["bilheteUnico"][2][0] == "213132"
	assert len(resp.json["bilheteUnico"][1]) == 6
	assert len(resp.json["creditos"]) == len(envios_sptrans)
	assert resp.json["creditos"][0][2] == "R$ 100,00"
	assert resp.json["creditos"][-1][2] == "R$ 400,00"

def aux_cria_dict_bilhete_unico(bu,pessoa,inicio,fim,ativo,concedido,aguard_envio,aguard_resp):
	return {
		"NumBilheteUnico":bu,
		"idPessoa":pessoa,
		"dataInicio":inicio,
		"dataFim":fim,
		"ativo":ativo,
		"concedido":concedido,
		"aguardandoEnvio":aguard_envio,
		"aguardandoResposta":aguard_resp
	}

def aux_adiciona_bilhetes_unicos(con,bilhetes_unicos):
	cur = con.cursor()

	for bilhete_unico in bilhetes_unicos:
		insercoes.insere_BILHETEUNICO(bilhete_unico,cur)

	con.commit()
	cur.close()

def aux_adiciona_usuario(con,token,dados_vazios_pessoa,id_pessoa=1):
	cur = con.cursor()
	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)

	insercoes.insere_USUARIO({'idPessoa':id_pessoa , 'senha': dados_cadastro['senha']},cur)

	sessao_dict = {
		"idPessoa": id_pessoa,
		"token": token, 
		"dataValidade" : "2042/07/04 09:45:00 PM GMT-3"
	}

	insercoes.insere_SESSAO(sessao_dict,cur)

	con.commit()
	cur.close()


def aux_cria_dict_envio(bilhete_unico,data,valor,confirmado,observacao=""):
	'''
		Cria dicionário correspondente aos atributos
		da tabela HISTORICOENVIOSPTRANS
	'''
	if observacao == "":
		observacao = None
	return {
		"NumBilheteUnico":bilhete_unico,
		"DataEnvio":data,
		"Valor":valor,
		"Confirmado":confirmado,
		"Observacao":observacao,
	}

def aux_adiciona_envio_sptrans(con,envios_sptrans,cur=""):
	'''
		Insira na tabela historicoenviosptrans 
	'''
	commitar = False
	if cur == "":
		cur = con.cursor()
		commitar = True
	for envio in envios_sptrans:
		insercoes.insere_HISTORICOENVIOSPTRANS(envio,cur)

	if commitar:
		con.commit()
		cur.close()

dados_cadastro = {
		'email' : 'email@email.com',
		'cpf' : 11111111111,
		'bu' : 111111111,
		'senha' : 'asdasidjahsdukiasdadasda'
	}

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": dados_cadastro['cpf'],
	"Email": dados_cadastro['email'],
	"Telefone": "",
	"Rua": "",
	"Numero": "",
	"Complemento": "",
	"Bairro": "",
	"CEP": "",
	"Cidade": "",
	"Estado": "",
	"NumBilheteUnico": dados_cadastro['bu']
}

dados_vazios_pessoa_2 = {
	"Nome": "Amigao2",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": "2342098203",
	"Email": "alo@alo.com",
	"Telefone": "",
	"Rua": "",
	"Numero": "",
	"Complemento": "",
	"Bairro": "",
	"CEP": "",
	"Cidade": "",
	"Estado": "",
	"NumBilheteUnico": "9999999"
}