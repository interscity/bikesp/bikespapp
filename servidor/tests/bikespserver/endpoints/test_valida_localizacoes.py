import sys
import json
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes

endpoint = "/api/validaLocs/"

def test_url_existe(client):
	resp = client.post(endpoint)
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 100000
	resp = client.post(endpoint, data={'user': lixao,'token':"token","locsErradas":""})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post(endpoint, data={'user': "user",'token':"token"})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	resp = client.post(endpoint, data={"user":"11111111111","token":"tokencorreto","locsErradas":""})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	con.commit()
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto","locsErradas":""})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokenerrado","locsErradas":""})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_validacao_ja_realizada(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	cur.execute("UPDATE PESSOA SET validouLocais = 't'")
	token = "tokencorreto"
	aux_adiciona_sessao(cur,token=token)
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":token,"locsErradas":""})
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == "Validação já realizada"

def test_locs_erradas_nao_json(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	token = "tokencorreto"
	aux_adiciona_sessao(cur,token=token)
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":token,"locsErradas":"'qrqpokrqop':'wekjlçwkf',':'"})
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == "Requisição inválida"

def test_validacao_vazia_ok(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	token = "tokencorreto"
	aux_adiciona_sessao(cur,token=token)
	con.commit()

	locs_erradas = []
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":token,"locsErradas":json.dumps(locs_erradas)})
	assert resp.status_code == 200
	assert resp.json["validado"] == True
	cur.execute("SELECT validouLocais FROM PESSOA;")
	assert cur.fetchone()[0] == True

def test_validacao_loc_inexistente(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	token = "tokencorreto"
	aux_adiciona_sessao(cur,token=token)
	con.commit()
	locs_erradas = [
		{"idLocalizacao":1,"coordenadas":{"lat":1,"long":2}}
	]
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":token,"locsErradas":json.dumps(locs_erradas)})
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == "Localização inexistente"

def test_validacao_loc_ok(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,1]])
	token = "tokencorreto"
	aux_adiciona_sessao(cur,token=token)
	con.commit()
	locs_erradas = [
		{"idLocalizacao":1,"coordenadas":{"lat":1,"long":2}},
		{"idLocalizacao":2,"coordenadas":{"lat":-23,"long":46}},
	]
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":token,"locsErradas":json.dumps(locs_erradas)})
	assert resp.status_code == 200
	assert resp.json["validado"] == True
