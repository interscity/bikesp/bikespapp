import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.autenticacao import gerencia_token_redefinicao_senha as gerencia_redef
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa

def test_url_existe(client):
	resp = client.post("/esqueceuSenha/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.get("/esqueceuSenha/?email=" + lixao)
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413
	assert resp.json['erro'] == 'Dados em excesso'

def test_request_errada(client,conexao_bd_testes):

	resp = client.get("/esqueceuSenha/?afkalflak=1")
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Requisição inválida'

	resp = client.get("/esqueceuSenha/?email=idsadksafal&aasdkjalf=asklfaslfkakjl")
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400


def test_email_inexistente(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	resp = client.get("/esqueceuSenha/?email=inexistente@email.com")
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Pessoa não cadastrada na pesquisa'	

def test_usuario_sem_login(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()

	resp = client.get("/esqueceuSenha/?email=" + resposta_mock_pessoa["Email"])
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Usuário não se cadastrou no aplicativo'

def test_sucesso_esqueceu_senha(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	aux_cadastra_usuario(con,"senha")

	resp = client.get("/esqueceuSenha/?email=" + resposta_mock_pessoa["Email"])
	assert resp.status_code == 200
	assert 'linkRedefinicao' in resp.json
	token = resp.json["linkRedefinicao"].split("token=")[1] 	
	assert gerencia_redef.token_redefinicao_valido(token,cur) == True


def aux_cadastra_usuario(con,senha="senha"):
	cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': senha},cur)
	con.commit()
