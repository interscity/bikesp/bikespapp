import sys
from pathlib import Path
from freezegun import freeze_time
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.autenticacao import gerencia_sessao
from servidor.bikespserver.endpoints import troca_bu
from servidor.bikespserver.utils.coords import fuso_horario
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.utils import parsers
from servidor.tests.utils.mocks import resposta_mock_pessoa

def test_urls_existem(client):
	resp = client.get("/trocaBU/")
	assert resp.status_code != 404
	resp = client.get("/trocaBU/redefinirBU")
	assert resp.status_code != 404

def test_requisita_troca_bu_req_invalida(client):
	lixao = 'a' * 1000
	resp = client.get("/trocaBU/?cpf=" + lixao + "&token=token")
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413
	resp = client.get("/trocaBU/?safasfasfa=fafaf&token=token")
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	return

def test_requisita_troca_bu_falta_cadastro(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	resp = client.get("/trocaBU/?cpf=141049198041&token=token")
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Pessoa não cadastrada na pesquisa'	
	resp = client.get("/trocaBU/?cpf=" + str(resposta_mock_pessoa["CPF"]) + "&token=token")
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Usuário não se cadastrou no aplicativo'

def test_requisita_troca_bu_token_invalido(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	aux_cadastra_usuario(con,"senha")
	con.commit()
	resp = client.get("/trocaBU/?cpf=" + str(resposta_mock_pessoa["CPF"]) + "&token=token")
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Token inválido'
	aux_adiciona_sessao(con,"tokenerrado")
	resp = client.get("/trocaBU/?cpf=" + str(resposta_mock_pessoa["CPF"]) + "&token=token")
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Token inválido'

def test_requisita_troca_bu_sucesso(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	aux_cadastra_usuario(con,"senha")
	aux_adiciona_sessao(con,"token")
	con.commit()

	resp = client.get("/trocaBU/?cpf=" + str(resposta_mock_pessoa["CPF"]) + "&token=token")
	assert resp.status_code == 200
	assert "redefinirBU/?token=token" in resp.json["linkRedefinicao"] 
	return

def test_requisita_troca_bu_pouco_tempo(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	aux_cadastra_usuario(con,"senha")
	aux_adiciona_sessao(con,"token")
	bilhete_unico_novo_1 = {
		"NumBilheteUnico":"214214324",
		"idPessoa":1,
		"dataInicio":"2023-09-12 15:33:32.987674-03:00",
		"dataFim":datetime.now(tz=fuso_horario),
		"ativo":False,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_novo_1,cur)
	con.commit()

	resp = client.get("/trocaBU/?cpf=" + str(resposta_mock_pessoa["CPF"]) + "&token=token")
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Bilhete único trocado há pouco tempo'
	freezer = freeze_time("2100-06-01 00:21:00", tz_offset=-3)
	freezer.start()
	resp = client.get("/trocaBU/?cpf=" + str(resposta_mock_pessoa["CPF"]) + "&token=token")
	assert resp.status_code == 200
	assert "redefinirBU/?token=token" in resp.json["linkRedefinicao"]
	freezer.stop()
	return

def test_redefine_bu_req_invalida(client):
	lixao = 'a' * 1000
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': lixao,'token':"token","novo_bu":"219342042"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413
	resp = client.post("/trocaBU/redefinirBU/", data={'qrqr23r2cpf': "wqrwqrqw",'token':"token","novo_bu":"219342042"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': "142141412",'token':"token","novo_bu":"21934adada2042"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Bilhete Único inválido. Confira se o digitou corretamente'
	return

def test_redefine_bu_nao_cadastro(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': "142141412",'token':"token","novo_bu":"219342042"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == 'CPF não encontrado'

	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': resposta_mock_pessoa["CPF"],'token':"aflasçflçafkalç","novo_bu":"219342042"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Usuário não se cadastrou no aplicativo"

	aux_cadastra_usuario(con,"senha")
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': resposta_mock_pessoa["CPF"],'token':"aflasçflçafkalç","novo_bu":"219342042"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Requisição inválida"

	return

def test_redefine_bu_ja_existe(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	aux_cadastra_usuario(con,"senha")
	aux_adiciona_sessao(con,"token")
	bilhete_unico_novo_1 = {
		"NumBilheteUnico":"214214324",
		"idPessoa":1,
		"dataInicio":"2023-09-12 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":False,
		"concedido":88,
		"aguardandoEnvio":64,
		"aguardandoResposta":99
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_novo_1,cur)
	con.commit()
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': resposta_mock_pessoa["CPF"],'token':"token","novo_bu":"214214324"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Bilhete único inválido"
	return

def test_redefine_bu_sucesso(client,conexao_bd_testes):
	freezer = freeze_time("2024-01-01 06:00:00", tz_offset=-3)
	freezer.start()
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	aux_cadastra_usuario(con,"senha")
	aux_adiciona_sessao(con,"token")
	con.commit()
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': resposta_mock_pessoa["CPF"],'token':"token","novo_bu":"219342042"})
	assert resp.status_code == 200
	
	resultado = consultas.bilhete_unico_existe("219342042",cur)
	assert resultado[2] == datetime(2024,1,1,0,0,0)
	assert resultado[3] is None
	assert resultado[4] == True
	assert parsers.parse_dinheiro_float(resultado[5]) == 0
	assert parsers.parse_dinheiro_float(resultado[6]) == 0
	assert parsers.parse_dinheiro_float(resultado[7]) == 0
	freezer.stop()

	return

def test_redefine_bu_pouco_tempo(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	aux_cadastra_usuario(con,"senha")
	aux_adiciona_sessao(con,"token")
	bilhete_unico_novo_1 = {
		"NumBilheteUnico":"214214324",
		"idPessoa":1,
		"dataInicio":"2023-09-12 15:33:32.987674-03:00",
		"dataFim":datetime.now(tz=fuso_horario),
		"ativo":False,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_novo_1,cur)
	con.commit()

	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': resposta_mock_pessoa["CPF"],'token':"token","novo_bu":"219342042"})
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Bilhete único trocado há pouco tempo'
	freezer = freeze_time("2100-06-01 00:21:00", tz_offset=-3)
	freezer.start()
	resp = client.post("/trocaBU/redefinirBU/", data={'cpf': resposta_mock_pessoa["CPF"],'token':"token","novo_bu":"219342042"})
	assert resp.status_code == 200
	freezer.stop()
	return


def test_pode_trocar_novamente():
	freezer = freeze_time("2023-06-01 00:21:00", tz_offset=-3)
	freezer.start()
	assert troca_bu.pode_trocar_novamente(None,30) == True
	assert troca_bu.pode_trocar_novamente(datetime(2023,6,1,0,21),30) == False
	assert troca_bu.pode_trocar_novamente(datetime(2023,5,1,0,21),30) == True
	assert troca_bu.pode_trocar_novamente(datetime(2023,5,2,0,22),30) == False
	assert troca_bu.pode_trocar_novamente(datetime(2023,4,1,0,22),30) == True
	freezer.stop()
	return

def test_adiciona_novo_bu(conexao_bd_testes):
	freezer = freeze_time("2024-01-01 06:00:00", tz_offset=-3)
	freezer.start()
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	bilhete_unico_novo_1 = {
		"NumBilheteUnico":"214214324",
		"idPessoa":1,
		"dataInicio":"2023-09-12 15:33:32.987674-03:00",
		"dataFim":"2023-10-25 15:33:32.987674-03:00",
		"ativo":False,
		"concedido":88,
		"aguardandoEnvio":64,
		"aguardandoResposta":99
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_novo_1,cur)
	con.commit()
	bilhete_unico_novo_2 = {
		"NumBilheteUnico":"99999999",
		"idPessoa":1,
		"dataInicio":"2023-10-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":21,
		"aguardandoResposta":2.2
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_novo_2,cur)
	con.commit()


	troca_bu.adiciona_novo_bu(1,"0000",cur)
	con.commit()

	resultado = consultas.bilhete_unico_existe('214214324',cur)
	assert resultado[3] is not None
	assert resultado[4] == False
	assert parsers.parse_dinheiro_float(resultado[5]) == 88
	assert parsers.parse_dinheiro_float(resultado[6]) == 0
	assert parsers.parse_dinheiro_float(resultado[7]) == 99

	resultado = consultas.bilhete_unico_existe('99999999',cur)
	assert resultado[3] == datetime(2024,1,1,0,0,0)
	assert resultado[4] == False
	assert parsers.parse_dinheiro_float(resultado[5]) == 0
	assert parsers.parse_dinheiro_float(resultado[6]) == 0
	assert parsers.parse_dinheiro_float(resultado[7]) == 2.2


	resultado = consultas.bilhete_unico_existe("0000",cur)
	assert resultado[2] == datetime(2024,1,1,0,0,0)
	assert resultado[3] is None
	assert resultado[4] == True
	assert parsers.parse_dinheiro_float(resultado[5]) == 0
	assert parsers.parse_dinheiro_float(resultado[6]) == 85
	assert parsers.parse_dinheiro_float(resultado[7]) == 0

	freezer.stop()
	return


def aux_cadastra_usuario(con,senha="senha"):
	cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': senha},cur)
	con.commit()

def aux_adiciona_sessao(con,token="token"):
	cur = con.cursor()
	sessao_dict = {
		"idPessoa": 1,
		"token": token, 
		"dataValidade" : "2242/07/04 09:45:00 PM GMT-3"
	}
	insercoes.insere_SESSAO(sessao_dict,cur)
	con.commit()