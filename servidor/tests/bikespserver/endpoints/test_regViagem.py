import sys
import re
import json
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd.consultas import get_pessoa_info
from servidor.bikespserver.bd.consultas import get_viagens_pessoa,bilhete_unico_existe
from servidor.bikespserver.utils.parsers import parse_dinheiro_float

from servidor.tests.utils.trajeto import cria_trajeto_str, cria_trajeto
from servidor.tests.utils.pessoa import aux_atribui_grupo_pesquisa
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes

from servidor.bikespserver.viagem.validaviagem.consts import NUM_MIN_AMOSTRAS_TRAJETO

from servidor.tests.utils.activites import ActivitiesBuilder
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MIN_ACITVITIES_KM, \
	PCNTG_ACTIVITIES_APROVACAO, PCNTG_ACTIVITIES_REPROVACAO

viagem_ponto_inicial = [-45.12312,-48.0013]
viagem_ponto_final = cria_trajeto([8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
		[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial)[-1]["Posicao"]


def test_url_existe(client):
	resp = client.post("/api/regViagem/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 500001
	resp = client.post("/api/regViagem/", data={'viagem': lixao,'user':"user","token":"token"})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/regViagem/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_falha_pessoa_desconhecida(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp_conteudo['erro'] == "Erro de Autenticacao"


def test_falha_sessao_errada(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,"tokenruim")

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp_conteudo['erro'] == "Erro de Autenticacao"


def test_salva_com_sucesso(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	aux_adiciona_locs_users(con,distancia=1000)

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockEnvioViagem["user"]
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	assert resp_conteudo['motivoStatus'] == 'APROVADO'
	assert resp_conteudo['idViagem'] == 1


def test_origem_desconhecida(client,conexao_bd_testes):
	con = conexao_bd_testes

	trajeto_str, ultimo_ponto = cria_trajeto_str([50]*100,[50]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockEnvioViagemOrigemDesc = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-23,46],ultimo_ponto])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagemOrigemDesc['token'])
	aux_adiciona_locs_users(con)	


	resp = client.post("/api/regViagem/", data=mockEnvioViagemOrigemDesc)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockEnvioViagemOrigemDesc["user"]
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['motivoStatus'] == 'ORIGEM_DESCONHECIDA'
	assert resp_conteudo['idViagem'] == 1

def test_destino_desconhecido(client,conexao_bd_testes):
	con = conexao_bd_testes


	mockEnvioViagemDestinoDesc = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + cria_trajeto_str([50]*100,[50]*100,pos_inicial=[-23,46]) + """ }""")
	}

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-23,46],[0,0]])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagemDestinoDesc['token'])
	aux_adiciona_locs_users(con)	


	resp = client.post("/api/regViagem/", data=mockEnvioViagemDestinoDesc)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockEnvioViagemDestinoDesc["user"]
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['motivoStatus'] == 'DESTINO_DESCONHECIDO'
	assert resp_conteudo['idViagem'] == 1

	
def test_origem_destino_desconhecidos(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[0,0],[-23,46]])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	aux_adiciona_locs_users(con)	

	mockEnvioViagemDestino = mockEnvioViagem.copy()
	mockEnvioViagemDestino["trajeto"] = cria_trajeto([0],[0],pos_inicial=viagem_ponto_inicial)

	resp = client.post("/api/regViagem/", data=mockEnvioViagemDestino)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockEnvioViagem["user"]
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['motivoStatus'] == 'ORIGEM_DESTINO_DESCONHECIDOS'
	assert resp_conteudo['idViagem'] == 1

def test_origem_igual_destino(client,conexao_bd_testes):
	con = conexao_bd_testes

	mockOrigemIgualDestino = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + cria_trajeto_str([0]*100,[50]*100,pos_inicial=[0,0]) + """ }""")
	}

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-23,46],[0,0]])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockOrigemIgualDestino['token'])
	aux_adiciona_locs_users(con)	

	resp = client.post("/api/regViagem/", data=mockOrigemIgualDestino)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockOrigemIgualDestino["user"]
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['motivoStatus'] == 'ORIGEM_IGUAL_DESTINO'
	assert resp_conteudo['idViagem'] == 1

def test_nao_bicicleta(client,conexao_bd_testes):
	con = conexao_bd_testes

	trajeto_str, ultima_pos = cria_trajeto_str([1000]*100,[5]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockNaoBicicleta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockNaoBicicleta['token'])
	aux_adiciona_locs_users(con)	

	resp = client.post("/api/regViagem/", data=mockNaoBicicleta)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockNaoBicicleta["user"]
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['motivoStatus'] == 'NAO_BICICLETA'
	assert resp_conteudo['idViagem'] == 1

def test_limite_excedido_com_remuneracao(client,conexao_bd_testes):
	con = conexao_bd_testes

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	cur = con.cursor()

	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=[1,2],id_grupo=4)

	mockViagemCorreta1 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691143647,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_locs_users(con,distancia=1000)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockViagemCorreta1)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	mockViagemCorreta2 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691147247,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockViagemCorreta2)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

	mockLimiteExcedido = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691150847,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockLimiteExcedido)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['motivoStatus'] == 'LIMITE_VIAGENS_EXCEDIDO'

def test_limite_excedido_dia_cheio_1(client,conexao_bd_testes):
	con = conexao_bd_testes
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])

	cur = con.cursor()
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=[1,2],id_grupo=4)

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockViagemCorreta1 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691103004,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_locs_users(con,distancia=1000)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockViagemCorreta1)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	mockViagemCorreta2 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691103244,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockViagemCorreta2)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

	mockLimiteNaoExcedido = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691150847,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockLimiteNaoExcedido)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

def test_limite_excedido_dia_cheio_2(client,conexao_bd_testes):
	con = conexao_bd_testes
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])

	cur = con.cursor()
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=[1,2],id_grupo=4)

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockViagemCorreta1 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691111104,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_locs_users(con,distancia=1000)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockViagemCorreta1)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	mockViagemCorreta2 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691107504,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockViagemCorreta2)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

	mockLimiteNaoExcedido1 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691125504,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockLimiteNaoExcedido1)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

	mockLimiteNaoExcedido2 = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1691129104,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}
	resp = client.post("/api/regViagem/", data=mockLimiteNaoExcedido2)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

def test_limite_excedido_sem_remuneracao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	aux_adiciona_locs_users(con,distancia=1000)

	cur = con.cursor()
	aux_atribui_grupo_pesquisa(cur,remuneracao=0.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	# print(mockEnvioViagem)
	
	mockEnvioViagem2 = dict(mockEnvioViagem)
	mockEnvioViagem2["viagem"] = mockEnvioViagem2["viagem"][:14] + "1231353512" + mockEnvioViagem2["viagem"][24:]
	resp = client.post("/api/regViagem/", data=mockEnvioViagem2)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

	mockEnvioViagem3 = dict(mockEnvioViagem)
	mockEnvioViagem3["viagem"] = mockEnvioViagem3["viagem"][:14] + "1231363512" + mockEnvioViagem3["viagem"][24:]
	resp = client.post("/api/regViagem/", data=mockEnvioViagem3)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	
	mockEnvioViagem4 = dict(mockEnvioViagem)
	mockEnvioViagem4["viagem"] = mockEnvioViagem4["viagem"][:14] + " 1231369512" + mockEnvioViagem4["viagem"][24:]
	resp = client.post("/api/regViagem/", data=mockEnvioViagem4)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['estadoViagem'] == 'Aprovado'

def test_viagem_duplicada(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	aux_adiciona_locs_users(con,distancia=1000)
	cur = con.cursor()
	aux_atribui_grupo_pesquisa(cur,remuneracao=0.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	
	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp_conteudo['erro'] == 'Viagem duplicada'

def test_remuneracao_correta(client,conexao_bd_testes):
	con = conexao_bd_testes
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_locs_users(con,distancia=1000)

	resp = client.post("/api/regViagem/", data=mockRemuneracaoCorreta)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockRemuneracaoCorreta["user"]
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	assert resp_conteudo['remuneracao'] == 0.0
	assert resp_conteudo['motivoStatus'] == 'APROVADO'
	assert resp_conteudo['idViagem'] == 1

	cur = con.cursor()
	cur.execute("DELETE FROM FEEDBACKVIAGEM; DELETE FROM VIAGEM;")
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockRemuneracaoCorreta)
	resp_conteudo = resp.json
	
	assert resp.status_code == 200
	assert resp_conteudo['remuneracao'] == 1

	cur = con.cursor()

	bilhete_unico = bilhete_unico_existe("2222",cur)
	assert parse_dinheiro_float(bilhete_unico[-2]) == 1

	aux_atribui_grupo_pesquisa(cur,remuneracao=100.0,id_pessoa=[1,2],id_grupo=4)
	cur.execute("DELETE FROM FEEDBACKVIAGEM; DELETE FROM VIAGEM;")
	con.commit()
	resp = client.post("/api/regViagem/", data=mockRemuneracaoCorreta)
	resp_conteudo = resp.json

	assert resp.status_code == 200
	assert resp_conteudo['remuneracao'] == 100

	bilhete_unico = bilhete_unico_existe("2222",cur)
	assert parse_dinheiro_float(bilhete_unico[-2]) == 101 

def test_salva_viagem(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	aux_adiciona_locs_users(con,distancia=1000)

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockEnvioViagem["user"]
	assert resp_conteudo['estadoViagem'] == 'Aprovado'
	assert resp_conteudo['remuneracao'] == 0.0
	assert resp_conteudo['motivoStatus'] == 'APROVADO'
	assert resp_conteudo['idViagem'] == 1

	assert len(get_viagens_pessoa(2,conexao_bd_testes.cursor())) > 0


def test_deslocamento_minimo(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,mockEnvioViagem['token'])
	aux_adiciona_locs_users(con,distancia=999)

	cur = con.cursor()
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	
	assert resp.status_code == 200
	assert resp_conteudo['CPF'] == mockEnvioViagem["user"]
	assert resp_conteudo['estadoViagem'] == 'Reprovado'
	assert resp_conteudo['remuneracao'] == 0.0
	assert resp_conteudo['motivoStatus'] == 'DESLOCAMENTO PEQUENO'
	assert resp_conteudo['idViagem'] == 1

def test_viagem_activities_bicicleta(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	token = "tokencorreto"
	trajeto = cria_trajeto(
			[8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
			[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial)

	tam_km = 4
	num_pontos = NUM_MIN_ACITVITIES_KM * tam_km
	builder_activities = ActivitiesBuilder(num_pontos)
	builder_activities.set_subtrajeto_bike(num_pontos*PCNTG_ACTIVITIES_APROVACAO + 2)
	builder_activities.set_subtrajeto_parado(num_pontos*0.3)
	activity_recognition_trip = builder_activities.gera_lista_medicoes()
	
	mockEnvioViagem = {
		"user": "678686",
		"token" : token,
		"viagem" : json.dumps({
			"dataInicio": 1231312312,
			"duracao": 51,
			"origem": "trabalho",
			"destino": "destino",
			"trajeto": [trajeto[0],trajeto[-1]],
			"activityRecognitionTrip": activity_recognition_trip
		})
	}
	ponto_inicial = [trajeto[0]["Posicao"]["latitude"],trajeto[0]["Posicao"]["longitude"]]
	ponto_final = [trajeto[0]["Posicao"]["latitude"],trajeto[0]["Posicao"]["longitude"]]

	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,ponto_final])
	con.commit()
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,token)
	aux_adiciona_locs_users(con,distancia=2000)
	aux_atribui_grupo_pesquisa(cur,remuneracao=10.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['motivoStatus'] == 'APROVADO'
	assert resp_conteudo['remuneracao'] == 20.0

def test_viagem_activities_nao_bicicleta(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	token = "tokencorreto"
	trajeto = cria_trajeto(
			[8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
			[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial)

	tam_km = 4
	num_pontos = NUM_MIN_ACITVITIES_KM * tam_km
	builder_activities = ActivitiesBuilder(num_pontos)
	builder_activities.set_subtrajeto_veiculo(num_pontos*PCNTG_ACTIVITIES_REPROVACAO)
	builder_activities.set_subtrajeto_bike(num_pontos*(1-PCNTG_ACTIVITIES_REPROVACAO))
	activity_recognition_trip = builder_activities.gera_lista_medicoes()
	
	mockEnvioViagem = {
		"user": "678686",
		"token" : token,
		"viagem" : json.dumps({
			"dataInicio": 1231312312,
			"duracao": 51,
			"origem": "trabalho",
			"destino": "destino",
			"trajeto": [trajeto[0],trajeto[-1]],
			"activityRecognitionTrip": activity_recognition_trip
		})
	}
	ponto_inicial = [trajeto[0]["Posicao"]["latitude"],trajeto[0]["Posicao"]["longitude"]]
	ponto_final = [trajeto[0]["Posicao"]["latitude"],trajeto[0]["Posicao"]["longitude"]]

	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,ponto_final])
	con.commit()
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,token)
	aux_adiciona_locs_users(con,distancia=2000)
	aux_atribui_grupo_pesquisa(cur,remuneracao=10.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['motivoStatus'] == 'NAO_BICICLETA'
	assert resp_conteudo['remuneracao'] == 0.0

def test_viagem_com_pausa(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	token = "tokencorreto"
	vels = [4]*100
	ts = [30] * 28 + [3600] + [30]*71
	trajeto, ultima_pos = cria_trajeto(vels,ts,pos_inicial=[0,0],de_ultima_pos=True)

	mockEnvioViagem = {
		"user": "678686",
		"token" : token,
		"viagem" : json.dumps({
			"dataInicio": 1231312312,
			"duracao": 51,
			"origem": "trabalho",
			"destino": "destino",
			"trajeto": trajeto,
			"pausas" : [{'Motivo': 'naoInformar', 'DataInicio': '1970-01-01 00:30:44 GMT-03:00', 'DataFim': '1970-01-01 01:30:44 GMT-03:00'},{'Motivo': 'naoInformar', 'DataInicio': '1970-01-01 00:30:45 GMT-03:00', 'DataFim': '1970-01-01 00:30:46 GMT-03:00'}],
		})
	}

	aux_adiciona_localizacoes(cur,[[0.0,0.0],ultima_pos])
	con.commit()
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,token)
	aux_adiciona_locs_users(con,distancia=2000)
	aux_atribui_grupo_pesquisa(cur,remuneracao=10.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['motivoStatus'] == 'APROVADO'
	assert resp_conteudo['remuneracao'] == 20.0

def test_viagem_activities_pausa(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	token = "tokencorreto"
	vels = [4]*100
	ts = [30] * 28 + [3600] + [30]*71
	trajeto, ultima_pos = cria_trajeto(vels,ts,pos_inicial=[0,0],de_ultima_pos=True)

	tam_km = 4
	num_pontos = NUM_MIN_ACITVITIES_KM * tam_km
	builder_activities = ActivitiesBuilder(num_pontos)
	builder_activities.set_subtrajeto_veiculo(num_pontos*PCNTG_ACTIVITIES_REPROVACAO)
	builder_activities.set_subtrajeto_bike(num_pontos*(1-PCNTG_ACTIVITIES_REPROVACAO))
	activity_recognition_trip = builder_activities.gera_lista_medicoes()

	mockEnvioViagem = {
		"user": "678686",
		"token" : token,
		"viagem" : json.dumps({
			"dataInicio": 1231312312,
			"duracao": 51,
			"origem": "trabalho",
			"destino": "destino",
			"trajeto": trajeto,
			"activityRecognitionTrip":activity_recognition_trip,
			"pausas" : [{'Motivo': 'naoInformar', 'DataInicio': '1970-01-01 00:30:44 GMT-03:00', 'DataFim': '1970-01-01 01:30:44 GMT-03:00'},{'Motivo': 'naoInformar', 'DataInicio': '1970-01-01 00:30:45 GMT-03:00', 'DataFim': '1970-01-01 00:30:46 GMT-03:00'}],
		})
	}

	aux_adiciona_localizacoes(cur,[[0.0,0.0],ultima_pos])
	con.commit()
	aux_adiciona_2_users(con)
	aux_adiciona_sessao_pessoa(con,2,token)
	aux_adiciona_locs_users(con,distancia=2000)
	aux_atribui_grupo_pesquisa(cur,remuneracao=10.0,id_pessoa=[1,2],id_grupo=4)
	con.commit()

	resp = client.post("/api/regViagem/", data=mockEnvioViagem)
	resp_conteudo = resp.json
	assert resp.status_code == 200
	assert resp_conteudo['motivoStatus'] == 'NAO_BICICLETA'
	assert resp_conteudo['remuneracao'] == 0.0

def aux_adiciona_locs_users(con,distancia=10):
	'''
		Função auxiliar - liga localizações
		ao usuário 1
	'''
	cur = con.cursor()
	apelido_localizacao1 = {
		"idPessoa":2,
		"idLocalizacao":1,
		"apelido": "apelido1"
	}
	apelido_localizacao2 = {
		"idPessoa":2,
		"idLocalizacao":2,
		"apelido": "apelido2"
	}
	cur = insercoes.insere_PARVALIDO({"idPonto1":1,"idPonto2":2,"distancia":distancia,"trajeto":{}},cur)
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)
	con.commit()
	cur.close()

def aux_adiciona_user(dados,con):
	cur = con.cursor()
	insercoes.insere_PESSOA(dados, cur)

	con.commit()
	cur.close()
def aux_adiciona_2_users(con):
	"""
			Função auxiliar - adiciona dois usuários, testeum e testedois
	"""
	dados_vazios_pessoa_1 = {
			"Nome": "testeum",
			"DataNascimento": "01/01/2000",
			"Genero": "",
			"Raca": "",
			"RG": "323",
			"CPF": "4342",
			"Email": "email1",
			"Telefone": "",
			"Rua": "",
			"Numero": "",
			"Complemento": "",
			"Bairro": "",
			"CEP": "",
			"Cidade": "",
			"Estado": "",
			"NumBilheteUnico": "testeum11111"
		}

	dados_vazios_pessoa_2 = {
			"Nome": "testedois",
			"DataNascimento": "01/01/2000",
			"Genero": "",
			"Raca": "",
			"RG": "8768",
			"CPF": "678686",
			"Email": "email2",
			"Telefone": "",
			"Rua": "",
			"Numero": "",
			"Complemento": "",
			"Bairro": "",
			"CEP": "",
			"Cidade": "",
			"Estado": "",
			"NumBilheteUnico": "testedois2222"
		}
	aux_adiciona_user(dados_vazios_pessoa_1,con)

	aux_adiciona_user(dados_vazios_pessoa_2,con)
	
	cur = con.cursor()
	dados_bilhete_unico = {
		"NumBilheteUnico":"testedois2222",
		"idPessoa":2,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(dados_bilhete_unico, cur)
	con.commit()


def aux_adiciona_sessao_pessoa(con,id_pessoa,token):
	"""
			Função auxiliar - cria uma sessão com o token dado
		para o usuário de id 2 
	"""
	cur = con.cursor()

	sessao_dict = {
		"idPessoa": id_pessoa,
		"token": token, 
		"dataValidade" : "2042/07/04 09:45:00 PM GMT-3"
	}

	insercoes.insere_SESSAO(sessao_dict,cur)

	con.commit()
	cur.close()

mockEnvioViagem = {
	"user": "678686",
	"token" : "tokencorreto",
	"viagem" : re.sub("[\n\t ]+", "",
"""{
	"dataInicio": 1231312312,
	"duracao": 51,
	"origem": "1",
	"destino": "2",
	"trajeto": """ + cria_trajeto_str(
		[8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
		[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial) + """ }""")
}
