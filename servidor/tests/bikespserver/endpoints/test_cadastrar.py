import sys
import copy
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas

def test_url_existe(client):
	resp = client.post("/api/cadastrar/")
	assert resp.status_code != 404

def test_request_errada(client):
	resp = client.post("/api/cadastrar/", data={'cpf': '',"bu":"","email":""})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/cadastrar/", data={'cpf': lixao,"bu":"","email":"","senha":""})
	assert resp.status_code == 413

def test_falha_pessoa_inexistente(client,conexao_bd_testes):
	resp = client.post("/api/cadastrar/", data=objRequest)
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] ==  'Aguarde a habilitação do seu perfil pelos desenvolvedores'

def test_falha_usuario_existente(client,conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)
	insercoes.insere_USUARIO({"idPessoa": 1, "senha": 'aaaaaa'},cur)

	con.commit()
	cur.close()

	resp = client.post("/api/cadastrar/", data=objRequest)
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Usuário Já Cadastrado'

def test_falha_dados_errados(client,conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	con.commit()
	cur.close()

	reqErrada = copy.deepcopy(objRequest)
	reqErrada['cpf'] = 2

	resp = client.post("/api/cadastrar/", data=reqErrada)
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] ==  'Aguarde a habilitação do seu perfil pelos desenvolvedores'

	reqErrada = copy.deepcopy(objRequest)
	reqErrada['bu'] = 2

	resp = client.post("/api/cadastrar/", data=reqErrada)
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Aguarde a habilitação do seu perfil pelos desenvolvedores'

	reqErrada = copy.deepcopy(objRequest)
	reqErrada['email'] = "aa"

	resp = client.post("/api/cadastrar/", data=reqErrada)
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] ==  'Aguarde a habilitação do seu perfil pelos desenvolvedores'

def test_cadastra_usuario(client,conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	con.commit()
	cur.close()

	resp = client.post("/api/cadastrar/", data=objRequest)
	assert resp.status_code == 200
	assert resp.json['estado'] == "Usuário Cadastrado"

	cur = con.cursor()

	buscaUsuario = consultas.usuario_senha_existe(1,objRequest['senha'],cur)
	assert buscaUsuario is True

def test_cadastra_email_maiusculo(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	con.commit()
	cur.close()

	objRequestEmailMaiusculo = objRequest
	objRequestEmailMaiusculo["email"] = objRequestEmailMaiusculo["email"].upper()

	resp = client.post("/api/cadastrar/", data=objRequest)
	assert resp.status_code == 200
	assert resp.json['estado'] == "Usuário Cadastrado"

	cur = con.cursor()

	buscaUsuario = consultas.usuario_senha_existe(1,objRequest['senha'],cur)
	assert buscaUsuario is True

objRequest = {
		'email' : 'email@email.com',
		'cpf' : 11111111111,
		'bu' : 111111111,
		'senha' : 'asdasidjahsdukiasdadasda'
	}

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": objRequest['cpf'],
	"Rua":"",
	"Numero":"",
	"Complemento":"",
	"CEP":"",
	"Bairro":"",
	"Cidade":"",
	"Estado":"",
	"Email": objRequest['email'],
	"Telefone": "",
	"NumBilheteUnico": objRequest['bu']
}

dados_bilhete_unico = {
	"NumBilheteUnico":objRequest['bu'],
	"idPessoa":1,
	"dataInicio":"2023-09-25 15:33:32.987674-03:00",
	"dataFim":None,
	"ativo":True,
	"concedido":0,
	"aguardandoEnvio":0,
	"aguardandoResposta":0
}