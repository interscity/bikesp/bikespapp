
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao
from servidor.tests.utils.viagem import aux_adiciona_viagem
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes

def test_url_existe(client):
	resp = client.post("/api/sincronizaViagens/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/sincronizaViagens/", data={'user': lixao,'token':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/sincronizaViagens/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	con.commit()
	resp = client.post("/api/sincronizaViagens/", data={"user":"11111111111","token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	dados_pessoa = aux_adiciona_pessoa(cur)
	con.commit()

	resp = client.post("/api/sincronizaViagens/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'


def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post("/api/sincronizaViagens/", data={"user":dados_pessoa["CPF"],"token":"tokenerrado"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_sincronizacao_vazia(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post("/api/sincronizaViagens/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})
	assert len(resp.json["viagens"]) == 0
	return

def test_sincronizacao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post("/api/sincronizaViagens/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})
	assert resp.status_code == 200
	assert len(resp.json["viagens"]) == 0

	aux_adiciona_viagem(cur)
	con.commit()

	resp = client.post("/api/sincronizaViagens/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})
	assert len(resp.json["viagens"]) == 1

	aux_adiciona_viagem(cur)
	con.commit()

	resp = client.post("/api/sincronizaViagens/", data={"user":dados_pessoa["CPF"],"token":"tokencorreto"})
	assert len(resp.json["viagens"]) == 2
	return