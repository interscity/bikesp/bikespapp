import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas

def test_url_existe(client):
	resp = client.post("/api/logout/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/logout/", data={'email': lixao,'token':"token"})

	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/logout/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	resp = client.post("/api/logout/", data={'email': dados_cadastro['email'],'token': 'asa'})

	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

def test_falha_nao_logado(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	resp = client.post("/api/logout/", data={'email': dados_cadastro['email'],'token': 'asa'})
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

def test_falha_token_errado(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	resp_login = client.post("/api/login/", data=dados_request_login)

	resp = client.post("/api/logout/", data={'email': dados_cadastro['email'],'token': 'asa'})

	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Sessao Invalida'

def test_faz_logout(client,conexao_bd_testes):
	
	con = conexao_bd_testes
	aux_cadastra_usuario(con)

	cur = con.cursor()

	resp_login = client.post("/api/login/", data=dados_request_login)

	assert resp_login.status_code == 200
	token = resp_login.json['token']

	resp = client.post("/api/logout/", data={'email': dados_cadastro['email'],'token': token})

	assert resp.status_code == 200
	assert resp.json['estado'] == 'Usuário Deslogado'

	sessoes = consultas.get_sessoes_usuario(1,cur)

	assert sessoes is None or sessoes == []

	cur.close()


def aux_cadastra_usuario(con):
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	
	print("Coloquei pessoa")
	con.commit()
	cur.close()

	cur = con.cursor()

	# cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': dados_cadastro['senha']},cur)
	print("Coloquei usuario")
	con.commit()
	cur.close()


dados_request_login = {
	'email' : 'email@email.com',
	'senha' : 'asdasidjahsdukiasdadasda'
}

dados_cadastro = {
		'email' : 'email@email.com',
		'cpf' : 11111111111,
		'bu' : 111111111,
		'senha' : 'asdasidjahsdukiasdadasda'
	}

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": dados_cadastro['cpf'],
	"Email": dados_cadastro['email'],
	"Telefone": "",
	"Rua": "",
	"Numero": "",
	"Complemento": "",
	"Bairro": "",
	"CEP": "",
	"Cidade": "",
	"Estado": "",
	"NumBilheteUnico": dados_cadastro['bu']
}