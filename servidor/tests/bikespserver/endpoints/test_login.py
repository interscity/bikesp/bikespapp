import sys
from pathlib import Path
import pytest

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.autenticacao import gerencia_sessao
from servidor.tests.utils.mocks import resposta_mock_pessoa

def test_url_existe(client):
	resp = client.post("/api/login/")
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post("/api/login/", data={'email': lixao,'senha':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/login/", data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	resp = client.post("/api/login/", data=dados_request_login)

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Par Usuario / Senha Nao Cadastrado'

def test_senha_errada(client,conexao_bd_testes):

	con = conexao_bd_testes

	aux_cadastra_usuario(con)

	resp = client.post("/api/login/", data=dados_request_login_errada)

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Par Usuario / Senha Nao Cadastrado'

# Usuário ja logado
def test_ja_logado(client,conexao_bd_testes,context):

	con = conexao_bd_testes
	aux_cadastra_usuario(con)
	con.commit()
	cur = con.cursor()
	with context:

		nova_secao = gerencia_sessao.gera_sessao("asadasd")
		nova_secao['idPessoa'] = 1

		insercoes.insere_SESSAO(nova_secao,con.cursor())

		con.commit()

		resp = client.post("/api/login/", data=dados_request_login)
		resp_json = resp.json

		assert resp.status_code == 200
		assert not gerencia_sessao.token_valido(1,nova_secao['token'],cur)
		assert gerencia_sessao.token_valido(1,resp_json["token"],cur)


# Tudo certo (salva sessão)
def test_loga_com_sucesso(client,conexao_bd_testes):
	con = conexao_bd_testes

	aux_cadastra_usuario(con)
	con.commit()
	cur = con.cursor()
	resp = client.post("/api/login/", data=dados_request_login)
	resp_json = resp.json	

	assert resp.status_code == 200
	assert gerencia_sessao.token_valido(1,resp_json["token"],cur) == True
	assert resp_json['CPF'] == str(dados_vazios_pessoa['CPF'])
	assert len(resp_json['locs']) == 0

def test_loga_e_busca_locs(client,conexao_bd_testes):
	con = conexao_bd_testes

	aux_cadastra_usuario(con)

	aux_insere_locs(con)

	resp = client.post("/api/login/", data=dados_request_login)
	resp_json = resp.json	

	assert resp.status_code == 200
	assert len(resp_json['locs']) == 2

def test_email_maiusculo(client,conexao_bd_testes):
	con = conexao_bd_testes

	aux_cadastra_usuario(con)

	aux_insere_locs(con)
	email = dados_request_login["email"]
	senha = dados_request_login["senha"]
	resp = client.post("/api/login/", data={"email":email.upper(),"senha":senha})
	resp_json = resp.json	
	assert resp.status_code == 200


@pytest.mark.parametrize('custom_env', [{"MIN_VERSAO_APP":"0.0.1"}])
def test_versao_nao_enviada(client_custom_env,conexao_bd_testes):
	con = conexao_bd_testes

	aux_cadastra_usuario(con)

	email = dados_request_login["email"]
	senha = dados_request_login["senha"]
	resp = client_custom_env.post("/api/login/", data={"email":email,"senha":senha})
	resp_json = resp.json	
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Seu aplicativo está desatualizado.\n Atualize-o para continuar.'

@pytest.mark.parametrize('custom_env', [{"MIN_VERSAO_APP":"0.0.1"}])
def test_versao_desatualizada(client_custom_env,conexao_bd_testes):
	con = conexao_bd_testes

	aux_cadastra_usuario(con)

	email = dados_request_login["email"]
	senha = dados_request_login["senha"]
	resp = client_custom_env.post("/api/login/", data={"email":email,"senha":senha,"versao":"0.0.0"})
	resp_json = resp.json	
	assert resp.status_code == 403
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Seu aplicativo está desatualizado.\n Atualize-o para continuar.'

@pytest.mark.parametrize('custom_env', [{"MIN_VERSAO_APP":"0.0.1"}])
def test_versao_atualizada(client_custom_env,conexao_bd_testes):
	con = conexao_bd_testes

	aux_cadastra_usuario(con)

	email = dados_request_login["email"]
	senha = dados_request_login["senha"]
	resp = client_custom_env.post("/api/login/", data={"email":email,"senha":senha,"versao":"0.5.0"})
	resp_json = resp.json	
	assert resp.status_code == 200

def test_validou_locs(client,conexao_bd_testes):
	con = conexao_bd_testes
	aux_cadastra_usuario(con)
	resp = client.post("/api/login/", data=dados_request_login)
	resp_json = resp.json
	assert resp.json["validouLocs"] == False
	con.cursor().execute("UPDATE PESSOA SET validouLocais = 't';")
	con.commit()
	resp = client.post("/api/login/", data=dados_request_login)
	resp_json = resp.json
	assert resp.json["validouLocs"] == True	


def aux_cadastra_usuario(con):
	cur = con.cursor()

	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	
	print("Coloquei pessoa")
	con.commit()
	cur.close()

	cur = con.cursor()

	# cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': dados_cadastro['senha']},cur)
	print("Coloquei usuario")
	con.commit()
	cur.close()

def aux_insere_locs(con):

	cur = con.cursor()

	# Criamos 4 locais
	loc1 = {
		"coordenadas": [0,0],
		"endereco":"Loca um",
		"tipoLocalizacao":"estacao"
	}
	loc2 = {
		"coordenadas": [0,25],
		"endereco":"Loca dois",
		"tipoLocalizacao":"qualquer"
	}
	loc3 = {
		"coordenadas": [25,0],
		"endereco":"Loca tres",
		"tipoLocalizacao":"qualquer"
	}
	loc4 = {
		"coordenadas": [25,25],
		"endereco":"Loca quatro",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(loc1,cur)
	cur = insercoes.insere_LOCALIZACAO(loc2,cur)
	cur = insercoes.insere_LOCALIZACAO(loc3,cur)
	cur = insercoes.insere_LOCALIZACAO(loc4,cur)

	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":"1"
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)

	pessoa_loc = {
		"idPessoa":1,
		"idLocalizacao":"3",
		"apelido" : "lugar feliz"
	}
	insercoes.insere_APELIDOLOCALIZACAO(pessoa_loc,cur)

	con.commit()
	cur.close()


dados_request_login_errada = {
	'email' : 'email@email.com',
	'senha' : 'asdad131sidjahsdukiasd'
}

dados_request_login = {
	'email' : 'email@email.com',
	'senha' : 'asdasidjahsdukiasdadasda'
}

dados_cadastro = {
		'email' : 'email@email.com',
		'cpf' : 11111111111,
		'bu' : 111111111,
		'senha' : 'asdasidjahsdukiasdadasda'
	}

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": dados_cadastro['cpf'],
	"Email": dados_cadastro['email'],
	"Telefone": "",
	"Rua": "",
	"Numero": "",
	"Complemento": "",
	"Bairro": "",
	"CEP": "",
	"Cidade": "",
	"Estado": "",
	"NumBilheteUnico": dados_cadastro['bu']
}