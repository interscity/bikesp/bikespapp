import sys
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes

endpoint = "/api/sincronizaLocs/"

def test_url_existe(client):
	resp = client.post(endpoint)
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 1000
	resp = client.post(endpoint, data={'user': lixao,'token':"token"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post(endpoint, data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	resp = client.post(endpoint, data={"user":"11111111111","token":"tokencorreto"})
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	con.commit()
	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokencorreto"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(endpoint, data={"user":info_pessoa["CPF"],"token":"tokenerrado"})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_sincroniza_localizacoes_ok(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	token = "token"
	token_2 = "token2"
	info_pessoa = aux_adiciona_pessoa(cur)
	info_pessoa_2 = aux_adiciona_pessoa(cur)
	aux_adiciona_sessao(cur,token=token)
	aux_adiciona_sessao(cur,id_pessoa=2,token=token_2)

	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,0],[0,1],[-1,-1],[0,-1],[-1,0]])

	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert resp.status_code == 200
	assert resp.json["locs"] == []

	aux_adiciona_locs_user(cur,ids_localizacoes=[1])
	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert len(resp.json["locs"]) == 1
	assert len(resp.json["locs"][0]) == 5

	resp = client.post(endpoint,data={"user":info_pessoa_2["CPF"],"token":token_2})
	assert len(resp.json["locs"]) == 0

	aux_adiciona_locs_user(cur,ids_localizacoes=[2,3],tipos=["estacao","estacao"])
	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert len(resp.json["locs"]) == 3

	aux_adiciona_locs_user(cur,ids_localizacoes=[2],id_pessoa=2)
	con.commit()

	resp = client.post(endpoint,data={"user":info_pessoa["CPF"],"token":token})
	assert len(resp.json["locs"]) == 3

	resp = client.post(endpoint,data={"user":info_pessoa_2["CPF"],"token":token_2})
	assert len(resp.json["locs"]) == 1