import sys,json
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido
from servidor.tests.utils.viagem import aux_adiciona_viagem
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD

url = "/api/viagensFaltantes/"

def test_url_existe(client):
	resp = client.post(url)
	assert resp.status_code != 404

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 10005
	resp = client.post(url, data={'user': lixao,'token':"token","idsViagens":json.dumps([])})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post(url, data={'ewioruewo': 'fwfw'})
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_usuario_nao_cadastrado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_localizacoes(cur)
	con.commit()

	resp = client.post(url, data={"user":"11111111111","token":"tokencorreto","idsViagens":json.dumps([])})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_nao_logado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur)
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps([])})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'


def test_token_errado(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokenerrado","idsViagens":json.dumps([])})

	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == 'Erro de Autenticacao'

def test_lista_errada(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":"["})

	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == "Requisição inválida"

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":""})
	assert resp.status_code == 400
	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps({"a":"b","c":["d","e",{"f":"g"}]})})
	assert resp.status_code == 400
	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(["a","b","c"])})
	assert resp.status_code == 400

def test_lista_vazia(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps([])})
	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert resp.json["viagensFaltantes"] == []

def test_lista_com_viagens_inexistentes(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur)
	aux_adiciona_sessao(cur,token="tokencorreto")
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(
		[1,2,3,4,5,10000,1000000000,10000000000000,1e30])})
	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert resp.json["viagensFaltantes"] == []

def test_viagens_de_outra_pessoa(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[0,1]])
	aux_adiciona_sessao(cur,token="tokencorreto")

	dados_pessoa_2 = aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur)
	aux_adiciona_locs_user(cur,id_pessoa=2)
	aux_adiciona_viagem(cur,id_usuario=2)
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(
		[1])})
	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert resp.json["viagensFaltantes"] == []

def test_viagens_corretas_pessoa(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[0,1]])
	aux_adiciona_sessao(cur,token="tokencorreto")

	aux_adiciona_par_valido(cur)
	aux_adiciona_locs_user(cur)
	aux_adiciona_viagem(cur)
	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(
		[1])})


	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert len(resp.json["viagensFaltantes"]) == 1
	assert len(resp.json["viagensFaltantes"][0]) == 10

def test_muitas_viagens(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[0,1]])
	aux_adiciona_sessao(cur,token="tokencorreto")

	aux_adiciona_par_valido(cur)
	aux_adiciona_locs_user(cur)

	qtd_viagens = 600
	for i in range(0,qtd_viagens):
		aux_adiciona_viagem(cur)

	con.commit()

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(
		["1"])})
	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert len(resp.json["viagensFaltantes"]) == 1

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(
		[1])})
	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert len(resp.json["viagensFaltantes"]) == 1

	resp = client.post(url, data={"user":dados_pessoa["CPF"],"token":"tokencorreto","idsViagens":json.dumps(
		list(range(1,qtd_viagens+1)))})
	assert resp.status_code == 200
	assert resp.json["CPF"] == dados_pessoa["CPF"]
	assert len(resp.json["viagensFaltantes"]) == 600