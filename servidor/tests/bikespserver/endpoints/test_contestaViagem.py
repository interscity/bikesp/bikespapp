import sys
import re
import json
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_sessao
from servidor.tests.utils.viagem import aux_adiciona_viagem

from servidor.bikespserver.bd import consultas

def test_url_existe(client):
	resp = client.post("/api/contestaViagem/")
	assert resp.status_code != 404

def test_request_nao_json(client):
	dados = {'user':"alo","token":"lixao",'contestacao':{"data":"lixao","idViagem":"1","justificativa":"qworkqwrq"} }
	resp = client.post("/api/contestaViagem/", data=dados,headers={ "Content-Type": "application/json" })
	
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'

	dados = {'user':"alo","token":"lixao",'contestacao':{"data":"lixao","idViagem":"1","justificativa":"qworkqwrq"} }
	resp = client.post("/api/contestaViagem/", data=str(dados),headers={ "Content-Type": "application/json" })
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'


def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 10001
	dados = {'user':"alo","token":"lixao",'contestacao':{"data":lixao,"idViagem":"1","justificativa":"qworkqwrq"} }
	resp = client.post("/api/contestaViagem/", data=json.dumps(dados),headers={ "Content-Type": "application/json" })
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

	dados = {'user':"alo","token":lixao,'contestacao':{"data":"lixao","idViagem":"1","justificativa":"qworkqwrq"} }
	resp = client.post("/api/contestaViagem/", data=json.dumps(dados),headers={ "Content-Type": "application/json" })
	
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413

def test_request_errada(client):

	resp = client.post("/api/contestaViagem/", data=json.dumps({'user': 'fwfw'}),headers={ "Content-Type": "application/json" })

	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

	dados = json.dumps({'user':"alo","token":"lixao",'contestacao':{"idViagem":"1","justificativa":"qworkqwrq"} })
	resp = client.post("/api/contestaViagem/", data=dados,headers={ "Content-Type": "application/json" })
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400

def test_falha_pessoa_desconhecida(client,conexao_bd_testes):
	con = conexao_bd_testes

	resp = client.post("/api/contestaViagem/", data=json.dumps(mockEnvioContestacao),headers={ "Content-Type": "application/json" })
	resp_conteudo = resp.json
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp_conteudo['erro'] == "Erro de Autenticacao"

def test_falha_sessao_errada(client,conexao_bd_testes):
	con = conexao_bd_testes

	resp = client.post("/api/contestaViagem/", data=json.dumps(mockEnvioContestacao),headers={ "Content-Type": "application/json" })
	resp_conteudo = resp.json
	assert resp.status_code == 401
	assert resp.json['estado'] == 'Erro'
	assert resp_conteudo['erro'] == "Erro de Autenticacao"

def test_justificativa_vazia(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	aux_adiciona_sessao(cur,token="tokencorreto")
	aux_adiciona_viagem(cur)
	con.commit()

	mockJustificativaVazia = mockEnvioContestacao.copy()
	mockJustificativaVazia["user"] = dados_pessoa["CPF"]
	mockJustificativaVazia["token"] = "tokencorreto"
	mockJustificativaVazia["contestacao"]["justificativa"] = ""

	resp = client.post("/api/contestaViagem/", data=json.dumps(mockJustificativaVazia),headers={ "Content-Type": "application/json" })
	
	assert resp.status_code == 400
	assert resp.json['estado'] == 'Erro'
	assert resp.json['erro'] == "Justificativa vazia"

def test_justificativa_grande(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)	
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	aux_adiciona_sessao(cur,token="tokencorreto")
	aux_adiciona_viagem(cur)
	con.commit()

	mockJustificativaGrande = mockEnvioContestacao.copy()
	mockJustificativaGrande["contestacao"]["justificativa"] = "a" * 500
	mockJustificativaGrande["user"] = dados_pessoa["CPF"]
	mockJustificativaGrande["token"] = "tokencorreto"

	resp = client.post("/api/contestaViagem/", data=json.dumps(mockJustificativaGrande),headers={ "Content-Type": "application/json" })
	
	assert resp.status_code == 200

	cur = con.cursor()

	resposta = consultas.contestacao_existe(1,cur)
	assert resposta is not None
	assert len(resposta[2]) == 300
	cur.close()


def test_salva_com_sucesso(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	dados_pessoa = aux_adiciona_pessoa(cur)	
	aux_adiciona_localizacoes(cur,[[-45.12312,-48.0013],[-45.124,-48.011]])
	aux_adiciona_sessao(cur,token="tokencorreto")
	aux_adiciona_viagem(cur)
	con.commit()

	mockContestacaoCorreto = mockEnvioContestacao.copy()
	mockContestacaoCorreto["contestacao"]["justificativa"] = "justificatia correta"
	mockContestacaoCorreto["user"] = dados_pessoa["CPF"]
	mockContestacaoCorreto["token"] = "tokencorreto"	
	resp = client.post("/api/contestaViagem/", data=json.dumps(mockContestacaoCorreto),headers={ "Content-Type": "application/json" })
	
	assert resp.status_code == 200

	cur = con.cursor()

	resposta = consultas.contestacao_existe(1,cur)
	assert resposta is not None
	assert resposta[2] == mockEnvioContestacao["contestacao"]["justificativa"]
	cur.close()

mockEnvioContestacao = {
	"user":"11111111111",
	"token":"tokencorreto",
	"contestacao":{
		"idViagem":1,
		"data":"2023-08-11T12:38:14.774Z",
		"justificativa":"eu não achei justo o fato de não ter sido aprovado"
	}
}
