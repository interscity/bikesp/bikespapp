import sys
from pathlib import Path
from freezegun import freeze_time

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.autenticacao import gerencia_sessao
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa

def test_cria_sessao(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	cur.close()
	with context:
		sessao = gerencia_sessao.gera_sessao(resposta_mock_pessoa["NumBilheteUnico"])
		id_pessoa = 1
		json_sessao = sessao
		assert sessao is not None
		assert "token" in json_sessao
		assert "dataValidade" in json_sessao

def test_salva_sessao(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	cur.close()
	token = ""
	with context:
		json_sessao = gerencia_sessao.gera_sessao(resposta_mock_pessoa["NumBilheteUnico"])
		id_pessoa = 1
		token = json_sessao["token"]
		gerencia_sessao.salva_sessao(id_pessoa,json_sessao)

	cur = con.cursor()
	resultado = consultas.get_sessoes_usuario(str(id_pessoa),cur)
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == json_sessao["token"]
	cur.close()

def test_token_valido(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	with context:
		json_sessao = gerencia_sessao.gera_sessao(resposta_mock_pessoa["NumBilheteUnico"])
		id_pessoa = 1
		gerencia_sessao.salva_sessao(id_pessoa,json_sessao)
		assert gerencia_sessao.token_valido(id_pessoa,json_sessao["token"],cur) == True
		assert gerencia_sessao.token_valido(id_pessoa,"token errado",cur) == False
		freezer = freeze_time("2100-01-14 03:21:34", tz_offset=-3)
		freezer.start()
		assert gerencia_sessao.token_valido(id_pessoa,json_sessao["token"],cur) == False
		freezer.stop()

def test_remove_sessao(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	with context:
		json_sessao = gerencia_sessao.gera_sessao(resposta_mock_pessoa["NumBilheteUnico"])
		id_pessoa = 1
		gerencia_sessao.salva_sessao(id_pessoa,json_sessao)
		assert gerencia_sessao.token_valido(id_pessoa,json_sessao["token"],cur) == True
		gerencia_sessao.remove_sessao(id_pessoa,json_sessao)
		assert gerencia_sessao.token_valido(id_pessoa,json_sessao["token"],cur) == False	


def test_reseta_sessoes(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	with context:
		json_sessao = gerencia_sessao.gera_sessao(resposta_mock_pessoa["NumBilheteUnico"])
		id_pessoa = 1
		gerencia_sessao.salva_sessao(id_pessoa,json_sessao)
		assert gerencia_sessao.token_valido(id_pessoa,json_sessao["token"],cur) == True
		gerencia_sessao.reseta_sessoes(id_pessoa,cur)
		con.commit()
		assert gerencia_sessao.token_valido(id_pessoa,json_sessao["token"],cur) == False

def test_checa_sessao_existente(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	cur.close()

	with context:
		assert not gerencia_sessao.checa_sessao_existente(1)
		json_sessao = gerencia_sessao.gera_sessao(resposta_mock_pessoa["NumBilheteUnico"])
		gerencia_sessao.salva_sessao(1,json_sessao)

		assert gerencia_sessao.checa_sessao_existente(1)

		freezer = freeze_time("2100-01-14 03:21:34", tz_offset=-3)
		freezer.start()
		assert not gerencia_sessao.checa_sessao_existente(1)
		freezer.stop()