import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.autenticacao.altera_senha import altera_senha
from servidor.bikespserver.autenticacao import gerencia_sessao
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa

def test_altera_senha(conexao_bd_testes,client):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	aux_cadastra_usuario(con,"senha")

	email = resposta_mock_pessoa["Email"]
	resp = client.post("/api/login/", data={"email":email,"senha":"senha"})
	assert resp.status_code == 200
	resp_json = resp.json	
	token1 = resp_json["token"]

	assert gerencia_sessao.token_valido(1,token1,cur) == True


	altera_senha(1,"novasenha",cur,chave_cripto="")
	con.commit()

	resp = client.post("/api/login/", data={"email":email,"senha":"senha"})
	assert resp.status_code == 401
	assert gerencia_sessao.token_valido(1,token1,cur) == False

	resp = client.post("/api/login/", data={"email":email,"senha":"novasenha"})
	assert resp.status_code == 200
	resp_json = resp.json	
	assert gerencia_sessao.token_valido(1,resp_json["token"],cur) == True



def aux_cadastra_usuario(con,senha="senha"):
	cur = con.cursor()
	insercoes.insere_USUARIO({'idPessoa':1 , 'senha': senha},cur)
	con.commit()
	cur.close()