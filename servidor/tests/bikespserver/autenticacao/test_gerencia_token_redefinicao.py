import sys
from pathlib import Path
from freezegun import freeze_time

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.autenticacao import gerencia_token_redefinicao_senha as gerencia_redef
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa


def test_gera_token_redef(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	cur.close()
	with context:
		sessao = gerencia_redef.gera_token_redefinicao(resposta_mock_pessoa["Email"])
		id_pessoa = 1
		json_sessao = sessao
		assert sessao is not None
		assert "token" in json_sessao
		assert "dataValidade" in json_sessao

def test_salva_token_redef(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()

	token = ""
	with context:
		json_sessao = gerencia_redef.gera_token_redefinicao(resposta_mock_pessoa["Email"])
		id_pessoa = 1
		token = json_sessao["token"]
		gerencia_redef.salva_token(id_pessoa,json_sessao,cur)

	resultado = consultas.pessoa_possui_token_redefinicao(str(id_pessoa),cur)
	assert len(resultado) == 3
	assert resultado[0] == 1
	assert resultado[1] == json_sessao["token"]

	with context:
		novo_json_sessao = gerencia_redef.gera_token_redefinicao(resposta_mock_pessoa["Email"])
		id_pessoa = 1
		token = novo_json_sessao["token"]
		gerencia_redef.salva_token(id_pessoa,novo_json_sessao,cur)

	resultado = consultas.pessoa_possui_token_redefinicao(str(id_pessoa),cur)
	assert len(resultado) == 3
	assert resultado[0] == 1
	assert resultado[1] == novo_json_sessao["token"]

def test_token_redef_valido(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	with context:
		json_sessao = gerencia_redef.gera_token_redefinicao(resposta_mock_pessoa["Email"])
		id_pessoa = 1
		gerencia_redef.salva_token(id_pessoa,json_sessao,cur)
		assert gerencia_redef.token_redefinicao_valido(json_sessao["token"],cur) == True
		assert gerencia_redef.token_redefinicao_valido("token errado",cur) == False
		freezer = freeze_time("2100-01-14 03:21:34", tz_offset=-3)
		freezer.start()
		assert gerencia_redef.token_redefinicao_valido(json_sessao["token"],cur) == False
		freezer.stop()

def test_remove_token_redef(conexao_bd_testes,context):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	with context:
		json_sessao = gerencia_redef.gera_token_redefinicao(resposta_mock_pessoa["Email"])
		id_pessoa = 1
		token = json_sessao["token"]
		gerencia_redef.salva_token(id_pessoa,json_sessao,cur)
		assert gerencia_redef.token_redefinicao_valido(json_sessao["token"],cur) == True
		gerencia_redef.remove_token_redefinicao(id_pessoa,cur)
		assert gerencia_redef.token_redefinicao_valido(json_sessao["token"],cur) == False