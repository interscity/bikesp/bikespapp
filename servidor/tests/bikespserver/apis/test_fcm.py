import sys
from pathlib import Path
from datetime import date

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.apis.fcm import __monta_mensagem


def test_monta_mensagem_simples():
	message = __monta_mensagem("token", "titulo")
	assert message.token == "token"
	assert message.data == {"title": "titulo"}

def test_monta_mensagem_com_subtitulo():
	message = __monta_mensagem("token", "titulo", subtitulo="subtitulo")
	assert message.token == "token"
	assert message.data == {"title": "titulo", "subtitle": "subtitulo"}

def test_monta_mensagem_com_mensagem():
	message = __monta_mensagem("token", "titulo", mensagem="mensagem")
	assert message.token == "token"
	assert message.data == {"title": "titulo", "body": "mensagem"}
