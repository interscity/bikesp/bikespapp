import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import insercoes

from servidor.bikespserver.feedback.consts import REMUNERACAO_POR_FEEDBACK

from servidor.tests.utils.mocks import resposta_mock_pessoa, resposta_mock
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.utils import parsers
#pylint: enable=wrong-import-position

def test_url_existe(client):
	resp = client.post("/feedbackViagemPerdida/")
	assert resp.status_code != 404

def test_request_errada(client):
	resp = client.post("/feedbackViagemPerdida/", data={"token":"token","D:resposta":"normal"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Requisição inválida'	

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 10002
	resp = client.post("/feedbackViagemPerdida/", data={"cpf":"cpf","token":lixao})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413
	assert resp.json['erro'] == "Dados em excesso"

def test_pessoa_inexistente(client,conexao_bd_testes):
	resp = client.post("/feedbackViagemPerdida/", data={"cpf":resposta_mock["CPF"],"token":"token","D:resposta":"normal"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "CPF inválido"

def test_cpf_de_outra_pessoa(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	pessoa_2 = dict(resposta_mock_pessoa)
	pessoa_2["CPF"] = "21035908205"
	pessoa_2["Email"] = "pessoa2@email.com"
	pessoa_2["NumBilheteUnico"] = "23424342"
	cur = insercoes.insere_PESSOA(pessoa_2,cur)
	sessao = {
		"idPessoa":2,
		"token":"token2",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)
	con.commit()

	resp = client.post("/feedbackViagemPerdida/", data={"cpf":pessoa_2["CPF"],"token":"token1","D:resposta":"normal"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Problema de autenticação"

def test_token_errado(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)

	resp = client.post("/feedbackViagemPerdida/", data={"cpf":resposta_mock["CPF"],"token2":"tokenErrado"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Requisição inválida"

def test_limite_diario_excedido(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token1",
		"F:quandoPerdeu":"ontem",
		"F:algoDiferente":"nao",
		"F:verApp":"1.2",
		"F:feedbackGeral":"jamais",
		"D:resposta1":"resposta1",
		"D:mongodb":"neo4j"
	}
	resp = client.post("/feedbackViagemPerdida/", data=dados)
	assert resp.status_code == 200
	resp = client.post("/feedbackViagemPerdida/", data=dados)		
	assert resp.status_code == 200
	resp = client.post("/feedbackViagemPerdida/", data=dados)		
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Você atingiu o limite diário de feedbacks de viagens perdidas"
	
def test_feedback_viagem_perdida_ok(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token1",
		"F:quandoPerdeu":"ontem",
		"F:algoDiferente":"nao",
		"F:verApp":"1.2",
		"F:feedbackGeral":"jamais",
		"D:resposta1":"resposta1",
		"D:mongodb":"neo4j"
	}
	resp = client.post("/feedbackViagemPerdida/", data=dados)
	assert resp.status_code == 200
	cur.execute("SELECT aguardandoEnvio FROM BILHETEUNICO")
	assert parsers.parse_dinheiro_float(cur.fetchone()[0]) == REMUNERACAO_POR_FEEDBACK
	cur.execute("SELECT idPessoa,quandoPerdeu FROM FEEDBACKVIAGEMPERDIDA")
	assert cur.fetchone() == (1,"ontem")
	cur.execute("SELECT respostasDinamicas FROM FEEDBACKVIAGEMPERDIDA")
	assert cur.fetchone()[0] == {"resposta1":"resposta1","mongodb":"neo4j"}

def test_feedback_viagem_perdida_ok(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token1",
	}
	resp = client.post("/feedbackViagemPerdida/", data=dados)
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "O formulário não pode ser vazio"

def aux_registra_pessoa(con):
	cur = con.cursor()
	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	dados_bilhete_unico = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	sessao = {
		"idPessoa":1,
		"token":"token1",
		"dataValidade":"2027-01-01"
	}
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_SESSAO(sessao,cur)
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	con.commit()
	cur.close()
