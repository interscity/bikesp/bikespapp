import sys,json

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import insercoes

from servidor.bikespserver.feedback.gerencia_feedback_viagem import requisita_feedback_viagem, armazena_respostas, cria_dict_feedback
from servidor.bikespserver.feedback.consts import REMUNERACAO_POR_FEEDBACK


from servidor.tests.utils.mocks import resposta_mock_pessoa, resposta_mock
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.utils import parsers
#pylint: enable=wrong-import-position

def test_requisita_feedback_viagem(conexao_bd_testes,context,request_context):

	con = conexao_bd_testes
	cur = con.cursor()

	assert requisita_feedback_viagem(1,None,"POUCOS_PONTOS",cur) == "Viagem inexistente"

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	
	cur = insercoes.insere_VIAGEM(viagem,cur)
	with context,request_context:
		link_feedback = requisita_feedback_viagem(1,1,viagem.motivoStatus,cur)
		con.commit()
		token = link_feedback.split("?token=")[1].split("&")[0]
		id_viagem = link_feedback.split("&idViagem=")[1]
		cur.execute("SELECT tokenFeedback,motivoOriginal FROM FEEDBACKVIAGEM;")
		assert cur.fetchone() == (token,viagem.motivoStatus)
		assert id_viagem == '1'

def test_url_existe(client):
	resp = client.post("/feedbackViagem/")
	assert resp.status_code != 404

def test_request_errada(client):
	resp = client.post("/feedbackViagem/", data={"token":"token","idViagem":"idViagem","D:resposta":"normal"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == 'Requisição inválida'	

def test_protecao_carga_muito_grande(client):
	lixao = 'a' * 10001
	resp = client.post("/feedbackViagem/", data={"cpf":"cpf","token":"token","idViagem":"idViagem","D:resposta":lixao})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 413
	assert resp.json['erro'] == "Dados em excesso"

def test_pessoa_inexistente(client,conexao_bd_testes):
	resp = client.post("/feedbackViagem/", data={"cpf":resposta_mock["CPF"],"token":"token","idViagem":"idViagem","D:resposta":"normal"})
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "CPF inválido"

def test_viagem_inexistente(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	con.commit()
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token",
		"idViagem":"2",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
		"D:resposta":"normal"
	}

	resp = client.post("/feedbackViagem/", data=dados)
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Requisição inválida"

def test_cpf_de_outra_pessoa(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	pessoa_2 = dict(resposta_mock_pessoa)
	pessoa_2["CPF"] = "21035908205"
	pessoa_2["Email"] = "pessoa2@email.com"
	pessoa_2["NumBilheteUnico"] = "23424342"
	cur = insercoes.insere_PESSOA(pessoa_2,cur)
	con.commit()

	aux_registra_viagem(con)
	dados = {
		"cpf":pessoa_2["CPF"],
		"token":"token",
		"idViagem":"1",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
		"D:resposta":"normal"
	}

	resp = client.post("/feedbackViagem/", data=dados)
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Requisição inválida"

def test_feedback_inexistente(client,conexao_bd_testes):
	con = conexao_bd_testes
	aux_registra_pessoa(con)
	aux_registra_viagem(con)
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token",
		"idViagem":"1",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
		"D:resposta":"normal"
	}
	resp = client.post("/feedbackViagem/", data=dados)
	assert resp.json['estado'] == 'Erro'
	assert resp.status_code == 400
	assert resp.json['erro'] == "Requisição inválida"

def test_token_errado(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	aux_registra_viagem(con)	
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"tokenerrado",
		"idViagem":"1",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
		"D:resposta":"normal"
	}
	with context,request_context:
		requisita_feedback_viagem(1,1,"POUCOS_PONTOS",cur)
		con.commit()
		resp = client.post("/feedbackViagem/", data=dados)
		assert resp.json['estado'] == 'Erro'
		assert resp.status_code == 400
		assert resp.json['erro'] == "Requisição inválida"

def test_feedback_ja_respondido(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	aux_registra_viagem(con)	
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token",
		"idViagem":"1",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
		"D:resposta":"normal"
	}
	with context,request_context:
		link_feedback = requisita_feedback_viagem(1,1,"POUCOS_PONTOS",cur)
		con.commit()
		cur.execute("UPDATE FEEDBACKVIAGEM SET respondido='t'")
		con.commit()
		token = link_feedback.split("?token=")[1].split("&")[0]
		dados["token"] = token
		resp = client.post("/feedbackViagem/", data=dados)
		assert resp.json['estado'] == 'Erro'
		assert resp.status_code == 400
		assert resp.json['erro'] == "Feedback já respondido"

def test_formulario_vazio(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	aux_registra_viagem(con)	
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token",
		"idViagem":"1",
	}

	with context,request_context:
		link_feedback = requisita_feedback_viagem(1,1,"POUCOS_PONTOS",cur)
		con.commit()
		token = link_feedback.split("?token=")[1].split("&")[0]
		dados["token"] = token
		resp = client.post("/feedbackViagem/", data=dados)
		assert resp.json['estado'] == 'Erro'
		assert resp.status_code == 400
		assert resp.json['erro'] == "O formulário não pode ser vazio"

def test_feedback_ok(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	aux_registra_viagem(con)	
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token",
		"idViagem":"1",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
		"D:resposta":"normal"
	}

	with context,request_context:
		link_feedback = requisita_feedback_viagem(1,1,"POUCOS_PONTOS",cur)
		con.commit()
		token = link_feedback.split("?token=")[1].split("&")[0]
		dados["token"] = token
		resp = client.post("/feedbackViagem/", data=dados)
		assert resp.status_code == 200
		cur.execute("SELECT aguardandoEnvio FROM BILHETEUNICO")
		assert parsers.parse_dinheiro_float(cur.fetchone()[0]) == REMUNERACAO_POR_FEEDBACK
		cur.execute("SELECT respondido FROM FEEDBACKVIAGEM")
		assert cur.fetchone()[0] == True
		cur.execute("SELECT tempoclima,respostasDinamicas FROM FEEDBACKVIAGEM")
		assert cur.fetchone() == ("aff",{"resposta":"normal"})

def test_feedback_sem_dinamicas(client,conexao_bd_testes,context,request_context):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_registra_pessoa(con)
	aux_registra_viagem(con)	
	dados = {
		"cpf":resposta_mock["CPF"],
		"token":"token",
		"idViagem":"1",
		"F:tempoClima":"aff",
		"F:outrosApps":"não",
		"F:feedbackGeral":"sim",
	}

	with context,request_context:
		link_feedback = requisita_feedback_viagem(1,1,"POUCOS_PONTOS",cur)
		con.commit()
		token = link_feedback.split("?token=")[1].split("&")[0]
		dados["token"] = token
		resp = client.post("/feedbackViagem/", data=dados)
		assert resp.status_code == 200
		cur.execute("SELECT aguardandoEnvio FROM BILHETEUNICO")
		assert parsers.parse_dinheiro_float(cur.fetchone()[0]) == REMUNERACAO_POR_FEEDBACK
		cur.execute("SELECT respondido FROM FEEDBACKVIAGEM")
		assert cur.fetchone()[0] == True
		cur.execute("SELECT tempoclima,respostasDinamicas FROM FEEDBACKVIAGEM")
		assert cur.fetchone() == ("aff",None)


def test_armazena_respostas():
	forms = {
		"cpf":"eu",
		"token":"eu",
		"F:pergunta1":"resposta1",
		"G:pergunta2":"resposta2",
		"D:pergunta3":"resposta3",
		"F:pergunta4":"resposta4",
	}
	respostas_fixas = {"pergunta1":{"tamanhoMaximo":30},"pergunta4":{"tamanhoMaximo":30}}
	assert armazena_respostas(forms,respostas_fixas) == {"pergunta1":"resposta1","pergunta4":"resposta4","respostasDinamicas":{"pergunta3":"resposta3"}}
	forms = {
		"cpf":"eu",
		"token":"eu",	
	}
	assert armazena_respostas(forms,respostas_fixas) == {}
	forms = {
		"cpf":"eu",
		"token":"eu",
		"D:pergunta3":"resposta3",		
		"D:pergunta5":"resposta5",		
	}
	assert armazena_respostas(forms,respostas_fixas) == {"respostasDinamicas":{"pergunta3":"resposta3","pergunta5":"resposta5"}}
	forms = {
		"cpf":"eu",
		"token":"eu",
		"F:pergunta1":"resposta1"
	}
	assert armazena_respostas(forms,respostas_fixas) == {"pergunta1":"resposta1"}

def test_cria_dict_feedback():
	forms = {
		"cpf":"eu",
		"token":"eu",
		"F:pergunta1":"resposta1",
		"G:pergunta2":"resposta2",
		"D:pergunta3":"resposta3",
		"F:pergunta4":"resposta4",
	}
	respostas_fixas = {"pergunta1":{"tamanhoMaximo":30},"pergunta4":{"tamanhoMaximo":30}}
	dict_respostas = armazena_respostas(forms,respostas_fixas)
	expected = {
		"idViagem":1,
		"tokenFeedback":"token",
		"motivoOriginal":"POUCOS_PONTOS",
		"respondido":False,
		"pergunta1":"resposta1",
		"pergunta4":"resposta4",
		"respostasDinamicas":json.dumps({
			"pergunta3":"resposta3"
		})
	}
	assert cria_dict_feedback(1,False,"token","POUCOS_PONTOS",dict_respostas, respostas_fixas) == expected
	
	forms = {
		"cpf":"eu",
		"token":"eu",	
	}
	dict_respostas = armazena_respostas(forms,respostas_fixas)
	expected = {
		"idViagem":1,
		"tokenFeedback":"token",
		"respondido":False,
		"motivoOriginal":"POUCOS_PONTOS",
		"pergunta1":None,
		"pergunta4":None,
		"respostasDinamicas":None
	}
	assert cria_dict_feedback(1,False,"token","POUCOS_PONTOS",dict_respostas, respostas_fixas) == expected

	forms = {
		"cpf":"eu",
		"token":"eu",
		"D:pergunta3":"resposta3",		
		"D:pergunta5":"resposta5",		
	}
	dict_respostas = armazena_respostas(forms,respostas_fixas)
	expected = {
		"idViagem":1,
		"respondido":False,
		"tokenFeedback":"token",
		"motivoOriginal":"POUCOS_PONTOS",
		"pergunta1":None,
		"pergunta4":None,
		"respostasDinamicas":json.dumps({
			"pergunta3":"resposta3",
			"pergunta5":"resposta5"
		})
	}
	assert cria_dict_feedback(1,False,"token","POUCOS_PONTOS",dict_respostas, respostas_fixas) == expected

	forms = {
		"cpf":"eu",
		"token":"eu",
		"F:pergunta1":"resposta1"
	}
	dict_respostas = armazena_respostas(forms,respostas_fixas)
	expected = {
		"idViagem":1,
		"respondido":False,
		"tokenFeedback":"token",
		"motivoOriginal":"POUCOS_PONTOS",
		"pergunta1":"resposta1",
		"pergunta4":None,
		"respostasDinamicas":None
	}
	assert cria_dict_feedback(1,False,"token","POUCOS_PONTOS",dict_respostas, respostas_fixas) == expected

def aux_registra_pessoa(con):
	cur = con.cursor()
	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	dados_bilhete_unico = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	con.commit()
	cur.close()

def aux_registra_viagem(con):
	cur = con.cursor()

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	insercoes.insere_VIAGEM(viagem,cur)
	con.commit()
	cur.close()
