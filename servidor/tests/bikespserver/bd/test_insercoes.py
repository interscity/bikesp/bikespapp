import sys
from pathlib import Path
from datetime import date

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.tests.utils.mocks import resposta_mock_pessoa, resposta_mock
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.classes.viagem import Viagem

from servidor.tests.utils.pessoa import aux_adiciona_pessoa

def test_insercao_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	cur.execute("SELECT * FROM PESSOA;")
	resultado = cur.fetchone()
	assert resultado[0] == 1
	assert resultado[1] == resposta_mock_pessoa["Nome"]
	assert resultado[3] == "M"
	assert resultado[11] == False
	assert len(resultado) == 12
	cur.close()

def test_insercao_resposta_forms(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	cur = insercoes.insere_RESPOSTA_FORMS(resposta_mock,cur)
	cur.execute("SELECT * FROM RESPOSTAFORMS;")
	resultado = cur.fetchone()
	assert resultado[0] == 1
	assert len(resultado) == 37
	cur.close()

def test_insercao_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	cur.execute("SELECT * FROM LOCALIZACAO;")
	resultado = cur.fetchone()
	assert len(resultado) == 4
	assert resultado[0] == 1
	assert resultado[2] == "(0,0)"
	cur.close()

def test_insercao_par_valido(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	par_valido = {
		"idPonto1":"1",
		"idPonto2":2,
		"distancia": 10,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido,cur)
	cur.execute("SELECT * FROM PARVALIDO;")
	resultado = cur.fetchone()
	assert resultado[0] == 1
	assert resultado[1] == 2
	assert resultado[2] == 10.0
	assert resultado[3] == par_valido["trajeto"]
	cur.close()

def test_insercao_apelido_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)

	# Criando novo apelido
	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)
	cur.execute("SELECT * FROM APELIDOLOCALIZACAO;")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert len(resultado[0]) == 3
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1
	assert resultado[0][2] == "apelido"

	# Atualizando apelido
	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido_modificado"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)
	cur.execute("SELECT * FROM APELIDOLOCALIZACAO;")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert len(resultado[0]) == 3
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1
	assert resultado[0][2] == "apelido_modificado"
	
	cur.close()

def test_insercao_pessoa_estacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":"1"
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)
	cur.execute("SELECT * FROM PESSOAESTACAO;")
	resultado = cur.fetchone()
	assert len(resultado) == 2
	assert resultado[0] == 1
	assert resultado[1] == 1
	cur.close()

def test_insercao_altera_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	id_pessoa = 1

	cur = insercoes.insere_ALTERACAOLOCALIZACAO(id_pessoa,cur)
	cur.execute("SELECT * FROM ALTERACAOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == date.today()

	cur.execute(
		"""
		UPDATE ALTERACAOLOCALIZACAO SET data=%s
		WHERE idPessoa=%s;
		""", ['1900-01-01', str(id_pessoa)])
	con.commit()
	cur = insercoes.insere_ALTERACAOLOCALIZACAO(id_pessoa,cur)
	cur.execute("SELECT * FROM ALTERACAOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == date.today()

	cur.close()

def test_insercao_grupo_pesquisa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()	
	grupo = {
		"idGrupo":4,
		"remuneracao":12.34
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo,cur)
	cur.execute("SELECT * FROM GRUPOPESQUISA;")
	resultado = cur.fetchall()
	assert resultado[0][0] == grupo["idGrupo"]
	cur.close()

def test_insercao_usuario(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	usuario = {
		"idPessoa":1,
		"senha":"senha"
	}
	cur = insercoes.insere_USUARIO(usuario,cur)
	cur.execute("SELECT * FROM auth_bikesp.USUARIO;")
	resultado = cur.fetchone()
	assert len(resultado) == 3
	assert resultado[0] == 1
	assert resultado[2] == False
	cur.close()

def test_insercao_sessao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	cur.execute("SELECT * FROM auth_bikesp.SESSAO;")
	resultado = cur.fetchone()

	assert resultado[0] == 1
	assert resultado[1] == sessao["token"]

	cur.close()

def test_atualiza_grupo_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	grupo = {
		"idGrupo":4,
		"remuneracao":12.34
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo,cur)
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	cur.execute("SELECT idGrupo FROM PESSOA")
	resultado = cur.fetchone()
	
	assert resultado[0] == None

	insercoes.atualiza_grupo_pessoa(4,1,cur)

	cur.execute("SELECT idGrupo FROM PESSOA")
	resultado = cur.fetchone()
	assert resultado[0] == 4


	insercoes.atualiza_grupo_pessoa(None,1,cur)

	cur.execute("SELECT idGrupo FROM PESSOA")
	resultado = cur.fetchone()
	
	assert resultado[0] == None

def test_insercao_viagem(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	

	cur.execute("SELECT * FROM VIAGEM")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': "",
		'metadados' : {'testeLegal' : { "aaaaaa" : 2 }}})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	
	cur = insercoes.insere_VIAGEM(viagem,cur)
	
	cur.execute("SELECT * FROM VIAGEM")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1

	cur.close()

	
def test_insercao_contestacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)


	contestacao = {
		"idViagem":1,
		"data":resposta_mock["DataResposta"],
		"justificativa":"abluabluabla"
	}

	cur.execute("SELECT * FROM CONTESTACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 0
	cur = insercoes.insere_CONTESTACAO(contestacao,cur)
	cur.execute("SELECT * FROM CONTESTACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][2] == contestacao["justificativa"]

	cur.close()


def test_insercao_reposta_trajeto(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()


	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	cur.execute("SELECT * FROM RESPOSTATRAJETO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	dict_resposta_trajeto = {
		"idPessoa":1,
		"apelido1":"Residência",
		"apelido2":"Trabalho",
		"frequencia":"ola",
		"duracao":"10",
		"comoFaz":"nao sei",
		"possibilidadeBike":"Com certeza",
	}

	cur = insercoes.insere_RESPOSTA_TRAJETO(dict_resposta_trajeto,cur)

	cur.execute("SELECT * FROM RESPOSTATRAJETO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

def test_insercao_bilhete_unico(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	cur.execute("SELECT * FROM BILHETEUNICO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	dados_bilhete_unico = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	cur.execute("SELECT * FROM BILHETEUNICO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == str(resposta_mock_pessoa['NumBilheteUnico'])
	assert resultado[0][1] == dados_bilhete_unico["idPessoa"]

def test_inseracao_token_redefinicao_senha(conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	insercoes.insere_TOKENREDEFINICAOSENHA({"idPessoa":1,"token":"tokenredefinicao","dataValidade":resposta_mock["DataResposta"]},cur)

	cur.execute("SELECT * FROM auth_bikesp.TOKENREDEFINICAOSENHA;")
	resultado = cur.fetchall()
	assert len(resultado) == 1

	insercoes.insere_TOKENREDEFINICAOSENHA({"idPessoa":1,"token":"tokenredefinicao2","dataValidade":resposta_mock["DataResposta"]},cur)

	cur.execute("SELECT * FROM auth_bikesp.TOKENREDEFINICAOSENHA;")
	resultado = cur.fetchall()
	assert len(resultado) == 1


def test_insercao_token_notificacao(conexao_bd_testes):
    con = conexao_bd_testes
    cur = con.cursor()
    cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

    insercoes.insere_TOKENNOTIFICACAO({"idPessoa":1,"token":"tokennotificacao1"},cur)
    cur.execute("SELECT * FROM auth_bikesp.TOKENNOTIFICACAO;")
    resultado = cur.fetchall()

    assert len(resultado) == 1
    assert resultado[0][0] == 1
    assert resultado[0][1] == "tokennotificacao1"

    insercoes.insere_TOKENNOTIFICACAO({"idPessoa":1,"token":"tokennotificacao2"},cur)
    cur.execute("SELECT * FROM auth_bikesp.TOKENNOTIFICACAO;")
    resultado = cur.fetchall()

    assert len(resultado) == 1
    assert resultado[0][0] == 1
    assert resultado[0][1] == "tokennotificacao2"
    
    cur.close()

def test_insercao_historico_envio_sptrans(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	bilhete_unico_ativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)
	

	cur.execute("SELECT * FROM HISTORICOENVIOSPTRANS;")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	envio_1 =  {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"DataEnvio":"2023-01-01",
		"Valor":10,
		"Confirmado":True,
		"Observacao":None
	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_1,cur)


	cur.execute("SELECT * FROM HISTORICOENVIOSPTRANS;")
	resultado = cur.fetchall()
	assert len(resultado) == 1

def test_insercao_feedback_viagem(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})

	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Reprovado",0.0)
	
	cur = insercoes.insere_VIAGEM(viagem,cur)

	cur.execute("SELECT * FROM FEEDBACKVIAGEM")
	resultado = cur.fetchall()
	assert len(resultado) == 0


	dict_feedback = {
		"idViagem":1,
		"tokenFeedback":"TOKEN",
		"respondido": False,
		"motivoOriginal":"Motivo Original",
		"tempoClima":None,
		"outrosApps":None,
		"feedbackGeral":None,
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEM(dict_feedback,cur)

	cur.execute("SELECT * FROM FEEDBACKVIAGEM")
	resultado = cur.fetchall()
	assert len(resultado) == 1

def test_insercao_feedback_viagem_perdia(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	cur.execute("SELECT * FROM FEEDBACKVIAGEMPERDIDA")
	assert len(cur.fetchall()) == 0
	dict_insercao = {
		"idPessoa":1,
		"dataFeedback":"2023-02-23 09:09:09",
		"quandoPerdeu":"Perdi a viagem",
		"algoDiferente":"diferente",
		"verApp":"2",
		"feedbackGeral":"geral",
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEMPERDIDA(dict_insercao,cur)
	cur.execute("SELECT * FROM FEEDBACKVIAGEMPERDIDA")
	resposta = cur.fetchall()
	assert len(resposta) == 1	
	assert resposta[0][0] == 1
	assert resposta[0][2] == dict_insercao["quandoPerdeu"]

def test_insercao_bonus(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur.execute("SELECT * FROM BONUS")
	assert len(cur.fetchall()) == 0

	dict_bonus = {
		"nome":"bonus",
		"valor":1.0,
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":"2023-10-10 20:01:20",
		"data_fim":"2025-01-01 20:12:04"
	}

	id_bonus = insercoes.insere_BONUS(dict_bonus,cur)
	assert id_bonus == 1
	cur.execute("SELECT * FROM BONUS")
	resultado = cur.fetchall() 
	assert len(resultado) == 1
	assert resultado[0][0] == id_bonus
	assert resultado[0][1] == dict_bonus["nome"]

def test_insercao_pessoa_bonus(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	aux_adiciona_pessoa(cur)
	dict_bonus = {
		"nome":"bonus",
		"valor":1.0,
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":"2023-10-10 20:01:20",
		"data_fim":"2025-01-01 20:12:04"
	}

	id_bonus = insercoes.insere_BONUS(dict_bonus,cur)

	cur.execute("SELECT * FROM PESSOABONUS")
	assert len(cur.fetchall()) == 0

	dict_pessoa_bonus = {
		"idPessoa":1,
		"idBonus":id_bonus,
		"dataConcessao":"2023-01-01 00:00:00"
	}

	insercoes.insere_PESSOABONUS(dict_pessoa_bonus,cur)
	
	cur.execute("SELECT * FROM PESSOABONUS")
	resultado = cur.fetchall() 
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == id_bonus