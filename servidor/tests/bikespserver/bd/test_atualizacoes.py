import sys,json
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import atualizacoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.utils import parsers
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.classes.viagem import Viagem
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.mocks import resposta_mock
from servidor.tests.utils.pessoa import aux_adiciona_pessoa

def test_atualiza_coordenada_localizacao(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	insercoes.insere_LOCALIZACAO({"coordenadas":[0,0],"endereco":"Endereco||","tipoLocalizacao":"qualquer"},cur)


	cur.execute("SELECT idLocalizacao,endereco,POINT(coordenadas) FROM LOCALIZACAO;")
	resposta = cur.fetchall()

	assert len(resposta) == 1
	assert resposta[0][0] == 1
	assert parsers.postgres_point_para_lista(resposta[0][2]) == [0,0]


	insercoes.insere_LOCALIZACAO({"coordenadas":[10,0],"endereco":"Endereco3||","tipoLocalizacao":"qualquer"},cur)
	insercoes.insere_LOCALIZACAO({"coordenadas":[5,5],"endereco":"Endereco2||","tipoLocalizacao":"qualquer"},cur)

	atualizacoes.coordenada_localizacao(1,[1,1],cur)

	cur.execute("SELECT idLocalizacao,endereco,POINT(coordenadas) FROM LOCALIZACAO;")
	resposta = cur.fetchall()

	assert len(resposta) == 3
	assert resposta[-1][0] == 1
	assert parsers.postgres_point_para_lista(resposta[-1][2]) == [1,1]

	atualizacoes.coordenada_localizacao(1,[5,5],cur)

	cur.execute("SELECT idLocalizacao,endereco,POINT(coordenadas) FROM LOCALIZACAO;")
	resposta = cur.fetchall()
	
	assert len(resposta) == 3
	assert resposta[-1][0] == 1
	assert parsers.postgres_point_para_lista(resposta[-1][2]) == [5,5]

def test_atualiza_viagem_existente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)
	assert consultas.get_viagem_info(1,cur)[2] == 1000

	assert parsers.parse_dinheiro_float(consultas.get_viagem_info(1,cur)[-1]) == 0.0
	atualizacoes.atualiza_viagem_existente(1,ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",10.0),cur)
	assert parsers.parse_dinheiro_float(consultas.get_viagem_info(1,cur)[-1]) == 10.0
	atualizacoes.atualiza_viagem_existente(1,ViagemBD(1,viagem_recebida,2000,1,2,"Aprovado",30.0),cur)
	assert consultas.get_viagem_info(1,cur)[2] == 2000
	assert parsers.parse_dinheiro_float(consultas.get_viagem_info(1,cur)[-1]) == 30.0
	atualizacoes.atualiza_viagem_existente(1,ViagemBD(1,viagem_recebida,2000,1,2,"Reprovado",30.0),cur)
	assert consultas.get_viagem_info(1,cur)[5] == "Reprovado"

def test_atualiza_contestacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	resposta_contestacao = "teste resposta"

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)


	contestacao = {
		"idViagem":1,
		"data":resposta_mock["DataResposta"],
		"justificativa":"abluabluabla"
	}

	insercoes.insere_CONTESTACAO(contestacao,cur)

	cur.execute("SELECT aprovada,resposta FROM CONTESTACAO")
	assert cur.fetchone() == (None,None)

	atualizacoes.atualiza_contestacao(1,True,resposta_contestacao,cur) 

	cur.execute("SELECT aprovada,resposta FROM CONTESTACAO")
	assert cur.fetchone() == (True,resposta_contestacao)

	atualizacoes.atualiza_contestacao(1,False,resposta_contestacao,cur) 

	cur.execute("SELECT aprovada,resposta FROM CONTESTACAO")
	assert cur.fetchone() == (False,resposta_contestacao)


def test_atualiza_bilhete_unico(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	bilhete_unico_ativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	atualizacoes.atualiza_bilhete_unico(bilhete_unico_ativo,cur)

	cur.execute("SELECT * FROM BILHETEUNICO")
	assert cur.fetchall() == []

	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)	

	cur.execute("SELECT * FROM BILHETEUNICO")
	assert cur.fetchall()[0][-2] == "R$ 0,00"

	bilhete_unico_atualizado = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 16:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":200,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(bilhete_unico_atualizado,cur)	

	cur.execute("SELECT * FROM BILHETEUNICO")
	assert cur.fetchall()[0][-2] == "R$ 0,00"

	atualizacoes.atualiza_bilhete_unico(bilhete_unico_atualizado,cur)

	cur.execute("SELECT * FROM BILHETEUNICO")
	assert cur.fetchall()[0][-2] == "R$ 200,00"

def test_atualiza_endereco_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	cur.execute("SELECT endereco FROM PESSOA")
	assert cur.fetchone()[0] == 'Praça Charles Miller|0||Pacaembu|01234900|São Paulo|SP'

	novo_endereco = {	
		"rua":"Rua 1",
		"cep":"000000",
		"numero":"0",
		"bairro":"Bairro",
		"cidade":"Cidade",
		"estado":"Estado",
		"apelido":"Residência"
	}

	atualizacoes.atualiza_endereco_PESSOA(1,novo_endereco,cur)

	cur.execute("SELECT endereco FROM PESSOA")
	assert cur.fetchone()[0] == 'Rua 1|0||Bairro|000000|Cidade|Estado'
def test_atualiza_feedback_viagem(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})

	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Reprovado",0.0)
	
	cur = insercoes.insere_VIAGEM(viagem,cur)

	dict_feedback = {
		"idViagem":1,
		"tokenFeedback":"TOKEN",
		"respondido": False,
		"motivoOriginal":"Motivo Original",
		"tempoClima":None,
		"outrosApps":None,
		"feedbackGeral":None,
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEM(dict_feedback,cur)

	cur.execute("SELECT respondido,feedbackGeral FROM FEEDBACKVIAGEM")
	assert cur.fetchone() == (False,None)

	dict_atualizacao = {
		"respondido":True,
		"outrosApps":None,
		"tempoClima":None,
		"feedbackGeral":"É ISSO AÍ",
		"respostasDinamicas":json.dumps({"PERGUNTA1":2,"PERGUNTA2":3})
	}
	atualizacoes.atualiza_feedback_viagem(dict_atualizacao,1,cur)

	cur.execute("SELECT respondido,feedbackGeral,respostasDinamicas FROM FEEDBACKVIAGEM")
	assert cur.fetchone() == (True,dict_atualizacao["feedbackGeral"],{"PERGUNTA1":2,"PERGUNTA2":3})

def test_atualiza_validacao_locs_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	aux_adiciona_pessoa(cur)
	cur.execute("SELECT validoulocais FROM PESSOA WHERE idpessoa=1;")
	assert cur.fetchone()[0] == False
	atualizacoes.atualiza_validacao_locs_pessoa(1, True, cur)
	cur.execute("SELECT validoulocais FROM PESSOA WHERE idpessoa=1;")
	assert cur.fetchone()[0] == True
	atualizacoes.atualiza_validacao_locs_pessoa(1, False, cur)
	cur.execute("SELECT validoulocais FROM PESSOA WHERE idpessoa=1;")
	assert cur.fetchone()[0] == False