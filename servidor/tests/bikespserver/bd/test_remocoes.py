import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.tests.utils.mocks import resposta_mock_pessoa, resposta_mock
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import remocoes


def test_remove_grupo_pesquisa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	grupo = {
		"idGrupo":1,
		"remuneracao":0
	}

	cur.execute("SELECT * FROM GRUPOPESQUISA;")
	resultado = cur.fetchone()
	assert resultado is None

	cur = insercoes.insere_GRUPOPESQUISA(grupo,cur)

	con.commit()
	cur.close()

	cur = con.cursor()

	cur.execute("SELECT * FROM GRUPOPESQUISA;")
	resultado = cur.fetchone()
	assert resultado is not None

	cur = remocoes.remove_GRUPOPESQUISA(grupo["idGrupo"],cur)

	cur.execute("SELECT * FROM GRUPOPESQUISA;")
	resultado = cur.fetchone()
	assert resultado is None

	cur.close()

def test_remove_sessao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	cur.execute("SELECT * FROM auth_bikesp.SESSAO;")
	resultado = cur.fetchone()
	assert resultado is not None

	cur = remocoes.remove_SESSAO(sessao["idPessoa"],sessao["token"],cur)

	cur.execute("SELECT * FROM auth_bikesp.SESSAO;")
	resultado = cur.fetchone()
	assert resultado is None
	cur.close()

def test_remove_token_redefinicao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	insercoes.insere_TOKENREDEFINICAOSENHA({"idPessoa":1,"token":"tokenredefinicao","dataValidade":resposta_mock["DataResposta"]},cur)

	cur.execute("SELECT * FROM auth_bikesp.TOKENREDEFINICAOSENHA;")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	
	remocoes.remove_TOKENREDEFINICAOSENHA(1,cur)
	
	cur.execute("SELECT * FROM auth_bikesp.TOKENREDEFINICAOSENHA;")
	resultado = cur.fetchall()
	assert len(resultado) == 0

def test_remove_apelidolocalizacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)


	localizacoes = [
		{"coordenadas": [0,0], "endereco":"Rua 1", "tipoLocalizacao":"qualquer"},
		{"coordenadas": [0,1], "endereco":"Rua 2", "tipoLocalizacao":"qualquer"},
	]

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	assert len(cur.fetchall()) == 0
	
	for local in localizacoes:
		insercoes.insere_LOCALIZACAO(local,cur)

	pessoas_locais = [
		{"idPessoa":1, "idLocalizacao":1, "apelido": "Residência"},
		{"idPessoa":1, "idLocalizacao":2, "apelido": "Escola"},
	]

	for pessoa_local in pessoas_locais:
		cur = insercoes.insere_APELIDOLOCALIZACAO(pessoa_local,cur)
	
	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	assert len(cur.fetchall()) == len(pessoas_locais)

	for pessoa_local in pessoas_locais:
		remocoes.remove_APELIDOLOCALIZACAO(pessoa_local["idPessoa"], pessoa_local["idLocalizacao"],cur)
	
	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	assert len(cur.fetchall()) == 0

def test_remove_pessoaestacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	estacoes = [
		{"coordenadas": [0,0], "endereco":"Rua 1",  "tipoLocalizacao":"estacao" },
		{"coordenadas": [0,1], "endereco":"Rua 2",  "tipoLocalizacao":"estacao" },
	]

	cur.execute("SELECT * FROM PESSOAESTACAO")
	assert len(cur.fetchall()) == 0

	for estacao in estacoes:
		cur = insercoes.insere_LOCALIZACAO(estacao,cur)

	pessoas_estacoes = [
		{"idPessoa":1, "idLocalizacao":1},
		{"idPessoa":1, "idLocalizacao":2},		
	]

	for pessoa_estacao in pessoas_estacoes:
		cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao, cur)
	
	cur.execute("SELECT * FROM PESSOAESTACAO")
	assert len(cur.fetchall()) == len(pessoas_estacoes)
	
	for pessoa_estacao in pessoas_estacoes:
		remocoes.remove_PESSOAESTACAO(pessoa_estacao["idPessoa"], pessoa_estacao["idLocalizacao"],cur)

	cur.execute("SELECT * FROM PESSOAESTACAO")
	assert len(cur.fetchall()) == 0

def test_remove_parvalido(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	par_valido = {
		"idPonto1":"1",
		"idPonto2":2,
		"distancia": 10,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}


	cur.execute("SELECT * FROM PARVALIDO")
	assert len(cur.fetchall()) == 0
	cur = insercoes.insere_PARVALIDO(par_valido,cur)

	cur.execute("SELECT * FROM PARVALIDO")
	assert len(cur.fetchall()) == 1

	remocoes.remove_PARVALIDO(1,2,cur)

	cur.execute("SELECT * FROM PARVALIDO")
	assert len(cur.fetchall()) == 0

	cur = insercoes.insere_PARVALIDO(par_valido,cur)

	cur.execute("SELECT * FROM PARVALIDO")
	assert len(cur.fetchall()) == 1
	
	remocoes.remove_PARVALIDO(2,1,cur)

	cur.execute("SELECT * FROM PARVALIDO")
	assert len(cur.fetchall()) == 0

def test_remove_LOCALIZACAO(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	localizacao = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}

	# Apagando localização sem vínculo
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

	resultado = remocoes.remove_LOCALIZACAO(1,cur)
	assert resultado

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	# Tentando apagar localização com vínculo
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 2

	resultado = remocoes.remove_LOCALIZACAO(1,cur)
	assert not resultado

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
