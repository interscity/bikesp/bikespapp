import os

def test_conecta(conexao_bd_testes):
	con = conexao_bd_testes
	assert con.info.dbname == os.getenv("BD_NOME_TESTE")
	con.close()