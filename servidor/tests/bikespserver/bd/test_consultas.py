import sys
from pathlib import Path
from datetime import datetime, date
from freezegun import freeze_time

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.mocks import resposta_mock
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes
from servidor.tests.utils.viagem import aux_adiciona_viagem
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.utils import coords
from servidor.bikespserver.utils import parsers

def test_get_pessoa_info(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()
	
	resultado_antes = consultas.get_pessoa_info(resposta_mock_pessoa["CPF"],cur)
	assert resultado_antes is None

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	resultado_depois = consultas.get_pessoa_info(resposta_mock_pessoa["CPF"],cur)
	assert resultado_depois is not None

	assert resultado_depois[0] == 1
	assert resultado_depois[1] == resposta_mock_pessoa["Email"]
	cur.close()

def test_get_viagem_info(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	resultado_antes = consultas.get_viagem_info(1,cur)
	assert resultado_antes is None

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	resultado_depois = consultas.get_viagem_info(1,cur)
	assert resultado_depois is not None
	assert resultado_depois[0] == 1

	cur.close()

def test_viagem_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	resultado_antes = consultas.viagem_existe(1,'1969-12-31 21:00:00',cur)	
	assert resultado_antes is None

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)
	resultado_depois = consultas.viagem_existe(1,'1969-12-31 21:00:00',cur)
	assert resultado_depois is not None
	assert resultado_depois[0] == 1

	cur.close()
def test_contestacao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	resultado_antes = consultas.contestacao_existe(1,cur)
	assert resultado_antes is None

	contestacao = {
		"idViagem":1,
		"data":resposta_mock["DataResposta"],
		"justificativa":"abluabluabla"
	}
	insercoes.insere_CONTESTACAO(contestacao,cur)

	resultado_depois = consultas.contestacao_existe(1,cur)
	assert resultado_depois is not None
	assert resultado_depois[0] == 1
	cur.close()

def test_get_pessoa_por_email(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	
	resultado_antes = consultas.get_pessoa_por_email(resposta_mock_pessoa["Email"],cur)
	assert resultado_antes is None

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	resultado_depois = consultas.get_pessoa_por_email(resposta_mock_pessoa["Email"],cur)
	assert resultado_depois is not None

	assert resultado_depois[0] == 1
	assert resultado_depois[1] == str(resposta_mock_pessoa["CPF"])
	cur.close()


def test_grupo_pesquisa_existe(conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()
	grupo = {
		"idGrupo":102,
		"remuneracao":2.20
	}	
	resultado_antes = consultas.grupo_pesquisa_existe(grupo["idGrupo"],cur)
	assert resultado_antes is None

	cur = insercoes.insere_GRUPOPESQUISA(grupo,cur)
	resultado_depois = consultas.grupo_pesquisa_existe(grupo["idGrupo"],cur)
	assert resultado_depois is not None

	assert resultado_depois[0] == grupo["idGrupo"]
	cur.close()
	
def test_resposta_forms_existe(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)


	get_pessoa_info = consultas.get_pessoa_info(resposta_mock_pessoa["CPF"],cur)

	resultado_antes = consultas.resposta_forms_existe(get_pessoa_info[0],cur)

	assert resultado_antes is None

	cur = insercoes.insere_RESPOSTA_FORMS(resposta_mock,cur)
	
	resultado_depois = consultas.resposta_forms_existe(get_pessoa_info[0],cur)

	assert resultado_depois is not None
	assert resultado_depois[0] == 1

	cur.close()

def test_localizacao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	resultado_antes = consultas.localizacao_existe(localizacao["coordenadas"],localizacao["endereco"],cur)
	assert resultado_antes is None

	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	
	resultado_depois = consultas.localizacao_existe(localizacao["coordenadas"],localizacao["endereco"],cur)
	assert resultado_depois is not None
	assert resultado_depois[0] == 1
	resultado_depois = consultas.localizacao_existe([1,1],localizacao["endereco"],cur)
	assert resultado_depois is None
	resultado_depois = consultas.localizacao_existe(localizacao["coordenadas"],"endereco errado",cur)
	assert resultado_depois is None
	resultado_depois = consultas.localizacao_existe([1,1],"endereco errado",cur)
	assert resultado_depois is None

	cur.execute("UPDATE LOCALIZACAO set coordenadas=POINT(1,1) WHERE idLocalizacao=1;")
	con.commit()

	resultado_depois = consultas.localizacao_existe([1,1],localizacao["endereco"],cur)
	assert resultado_depois is not None
	assert resultado_depois[0] == 1
	resultado_depois = consultas.localizacao_existe([0,0],localizacao["endereco"],cur)
	assert resultado_depois is None
	resultado_depois = consultas.localizacao_existe([1,1],"endereco errado",cur)
	assert resultado_depois is None
	resultado_depois = consultas.localizacao_existe([0,0],"endereco errado",cur)
	assert resultado_depois is None

	resultado_case = consultas.localizacao_existe([1,1],"rua rua",cur)
	assert resultado_case is not None
	assert resultado_case[0] == 1
	resultado_case = consultas.localizacao_existe([1,1],"RUA RUA",cur)
	assert resultado_case is not None
	assert resultado_case[0] == 1

	cur.close()

def test_id_localizacao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	resultado_antes = consultas.id_localizacao_existe(1,cur)
	assert resultado_antes is None
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	resultado_depois = consultas.id_localizacao_existe(1,cur)
	assert resultado_depois[0] == localizacao["endereco"]
	assert resultado_depois[2] == localizacao["tipoLocalizacao"]

def test_pessoa_estacao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":1
	}
	
	resultado_antes = consultas.pessoa_estacao_existe(pessoa_estacao["idPessoa"],pessoa_estacao["idLocalizacao"],cur)
	assert resultado_antes is None

	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)

	resultado_depois = consultas.pessoa_estacao_existe(pessoa_estacao["idPessoa"],pessoa_estacao["idLocalizacao"],cur)

	assert resultado_depois is not None
	assert resultado_depois[0] == 1
	assert resultado_depois[1] == 1
	cur.close()

def test_apelido_localizacao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido"
	}

	resultado_antes = consultas.apelido_localizacao_existe(apelido_localizacao["idPessoa"],apelido_localizacao["idLocalizacao"],cur)

	assert resultado_antes is None

	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)
	resultado_depois = consultas.apelido_localizacao_existe(apelido_localizacao["idPessoa"],apelido_localizacao["idLocalizacao"],cur)

	assert resultado_depois is not None
	assert resultado_depois[0] == 1
	assert resultado_depois[1] == 1
	assert resultado_depois[2] == apelido_localizacao["apelido"]
	cur.close()

def test_par_valido_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	par_valido = {
		"idPonto1":"1",
		"idPonto2":2,
		"distancia": 10,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}

	resultado_antes = consultas.par_valido_existe(par_valido["idPonto1"],par_valido["idPonto2"],cur)
	assert resultado_antes is None

	cur = insercoes.insere_PARVALIDO(par_valido,cur)

	resultado_depois = consultas.par_valido_existe(par_valido["idPonto1"],par_valido["idPonto2"],cur)

	assert resultado_depois is not None
	assert resultado_depois[0] == 1
	assert resultado_depois[1] == 2
	assert resultado_depois[2] == 10.0
	cur.close()

def test_get_coordenada_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)

	localizacao2 = {
		"coordenadas": [0,1],
		"endereco":"Estacao 2",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)

	localizacao3 = {
		"coordenadas": [0,-1],
		"endereco":"Rua 3",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao3,cur)

	resultado3 = consultas.get_coordenada_localizacao(3,cur)
	resultado1 = consultas.get_coordenada_localizacao(1,cur)
	resultado2 = consultas.get_coordenada_localizacao(2,cur)

	assert resultado3[0] == "(0,-1)"
	assert resultado2[0] == "(0,1)"
	assert resultado1[0] == "(0,0)"
	cur.close()

def test_alteracao_localizacao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	id_pessoa = 1

	cur.execute("SELECT data FROM ALTERACAOLOCALIZACAO")
	resultado_esperado = cur.fetchone()
	resultado_observado = consultas.alteracao_localizacao_existe(id_pessoa,cur)
	assert resultado_esperado == resultado_observado

	cur = insercoes.insere_ALTERACAOLOCALIZACAO(id_pessoa,cur)
	cur.execute("SELECT data FROM ALTERACAOLOCALIZACAO")
	resultado_esperado = cur.fetchone()
	resultado_observado = consultas.alteracao_localizacao_existe(id_pessoa,cur)
	assert resultado_esperado == resultado_observado
	assert resultado_observado[0] == date.today()

	cur.close()

def test_localizacao_pessoa_associada_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	pessoa2 = dict(resposta_mock_pessoa)
	pessoa2["Nome"] = "Pessoa 2"
	pessoa2["CPF"] = "90987654321"
	pessoa2["Email"] = "pessoa2@email.com"
	pessoa2["RG"] = "987654321"
	pessoa2["NumBilheteUnico"] = "0987654321"
	cur = insercoes.insere_PESSOA(pessoa2,cur)

	# Localização qualquer

	resultado = consultas.localizacao_pessoa_associada_existe(1,cur)
	assert resultado is not None
	assert not resultado

	localizacao_qualquer = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_qualquer,cur)

	resultado = consultas.localizacao_pessoa_associada_existe(1,cur)
	assert resultado is not None
	assert not resultado

	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":1,
		"apelido": "apelido"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)

	resultado = consultas.localizacao_pessoa_associada_existe(1,cur)
	assert resultado is not None
	assert resultado

	apelido_localizacao = {
		"idPessoa":2,
		"idLocalizacao":1,
		"apelido": "apelido"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)

	resultado = consultas.localizacao_pessoa_associada_existe(1,cur)
	assert resultado is not None
	assert resultado

	# Localização estação

	localizacao_estacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_estacao,cur)

	resultado = consultas.localizacao_pessoa_associada_existe(2,cur)
	assert resultado is not None
	assert not resultado

	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":2,
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)

	resultado = consultas.localizacao_pessoa_associada_existe(2,cur)
	assert resultado is not None
	assert resultado

	pessoa_estacao = {
		"idPessoa":2,
		"idLocalizacao":2,
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)

	resultado = consultas.localizacao_pessoa_associada_existe(2,cur)
	assert resultado is not None
	assert resultado

def test_localizacao_viagem_associada_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	localizacao_qualquer = {
		"coordenadas": [0,0],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_qualquer,cur)
	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":1,
		"apelido": "apelido"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)

	localizacao_estacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_estacao,cur)
	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":2,
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)

	resultado = consultas.localizacao_viagem_associada_existe(1,cur)
	assert resultado is not None
	assert not resultado

	resultado = consultas.localizacao_viagem_associada_existe(2,cur)
	assert resultado is not None
	assert not resultado

	resultado = consultas.localizacao_viagem_associada_existe(3,cur)
	assert resultado is not None
	assert not resultado

	viagem = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem_bd = ViagemBD(1,viagem,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem_bd,cur)

	resultado = consultas.localizacao_viagem_associada_existe(1,cur)
	assert resultado is not None
	assert resultado

	resultado = consultas.localizacao_viagem_associada_existe(2,cur)
	assert resultado is not None
	assert resultado

	resultado = consultas.localizacao_viagem_associada_existe(3,cur)
	assert resultado is not None
	assert not resultado

# Consome request TomTom
# def calcula_distancia_localizacoes_rota():
	# assert True

def test_usuario_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	usuario = {
		"idPessoa":1,
		"senha":"senha"
	}
	cur = insercoes.insere_USUARIO(usuario,cur)

	existencia_usuario = consultas.usuario_existe(usuario["idPessoa"],cur)

	assert existencia_usuario is True

	cur.close()

def test_usuario_senha_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	usuario = {
		"idPessoa":1,
		"senha":"senha"
	}
	cur = insercoes.insere_USUARIO(usuario,cur)

	par_usuario_senha_existe = consultas.usuario_senha_existe(usuario["idPessoa"],usuario["senha"],cur)

	assert par_usuario_senha_existe is True

	cur.close()

def test_usuario_token_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	resultado = consultas.usuario_token_existe(sessao["idPessoa"],sessao["token"],cur)

	assert resultado[0] == 1
	assert resultado[1] == 'token'
	cur.close()

def test_get_sessoes_usuario(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	resultado = consultas.get_sessoes_usuario(sessao["idPessoa"],cur)

	assert len(resultado) == 1
	assert resultado[0][1] == sessao["token"]
	cur.close()

def test_get_grupo_pesquisa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	resultado_antes = consultas.get_grupos_pesquisa(cur)
	assert len(resultado_antes) == 0
	grupo = {
		"idGrupo":102,
		"remuneracao":2.20
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo,cur)
	resultado_depois = consultas.get_grupos_pesquisa(cur)
	assert len(resultado_depois) == 1
	cur.close()

def test_get_token_notificacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	
	tk_notif = {
		"idPessoa":1,
		"token":"token123"
  	}
	cur = insercoes.insere_TOKENNOTIFICACAO(tk_notif,cur)
	resultado = consultas.get_token_notificacao(1,cur)
	
	assert resultado == "token123"

	cur.close() 

def test_get_tokens_notificacao_coorte(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	
	resposta_mock_pessoa2 = resposta_mock_pessoa.copy()
	resposta_mock_pessoa2["CPF"] = 24772172084
	resposta_mock_pessoa2["RG"] = 157767711
	resposta_mock_pessoa2["Email"] = "email2@email.c"
	resposta_mock_pessoa2["Nome"] = "Outro Usuário"
	resposta_mock_pessoa2["NumBilheteUnico"] = 12345678911
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa2,cur)
	
	grupo1 = {
		"idGrupo":1,
		"remuneracao":1.0
	}	
	cur = insercoes.insere_GRUPOPESQUISA(grupo1,cur)
	cur.execute("UPDATE PESSOA SET idGrupo=1;");
	
	insercoes.insere_TOKENNOTIFICACAO({"idPessoa":1,"token":"token1"},cur)
	insercoes.insere_TOKENNOTIFICACAO({"idPessoa":2,"token":"token2"},cur)
	resultado = consultas.get_tokens_notificacao_coorte(1,cur)
	
	assert len(resultado) == 2
	assert "token1" in resultado
	assert "token2" in resultado
	
	cur.close()

def test_get_token_notificacao_todos(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	resposta_mock_pessoa2 = resposta_mock_pessoa.copy()
	resposta_mock_pessoa2["CPF"] = 24772172084
	resposta_mock_pessoa2["RG"] = 157767711
	resposta_mock_pessoa2["Email"] = "email2@email.c"
	resposta_mock_pessoa2["Nome"] = "Outro Usuário"
	resposta_mock_pessoa2["NumBilheteUnico"] = 12345678911
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa2,cur)
	
	insercoes.insere_TOKENNOTIFICACAO({"idPessoa":1,"token":"token1"},cur)
	insercoes.insere_TOKENNOTIFICACAO({"idPessoa":2,"token":"token2"},cur)
	resultado = consultas.get_tokens_notificacao_todos(cur)
	
	assert len(resultado) == 2
	assert "token1" in resultado
	assert "token2" in resultado
	
	cur.close()


def test_get_todas_localizacoes_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	localizacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.get_todas_localizacoes_pessoa(1,cur) == []

	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":1,
		"apelido": "apelido"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)

	assert len(consultas.get_todas_localizacoes_pessoa(1,cur)) == 1
	assert consultas.get_todas_localizacoes_pessoa(1,cur)[0][0] == 1
	assert consultas.get_todas_localizacoes_pessoa(1,cur)[0][3] == "apelido"
	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":2
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)
	assert len(consultas.get_todas_localizacoes_pessoa(1,cur)) == 2
	assert consultas.get_todas_localizacoes_pessoa(1,cur)[1][3] == "Estacao Estacao"

	cur.close()


def test_resposta_trajeto_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()


	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.resposta_trajeto_existe(1,"Residência","Trabalho",cur) is None

	dict_resposta_trajeto = {
		"idPessoa":1,
		"apelido1":"Residência",
		"apelido2":"Trabalho",
		"frequencia":"ola",
		"duracao":"10",
		"comoFaz":"nao sei",
		"possibilidadeBike":"Com certeza",
	}

	cur = insercoes.insere_RESPOSTA_TRAJETO(dict_resposta_trajeto,cur)

	assert consultas.resposta_trajeto_existe(1,"Residência","Trabalho",cur) is not None
	assert consultas.resposta_trajeto_existe(1,"Residência","Trabalho",cur)[1] == "Residência"

	cur.close()

def test_get_viagens_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert len(consultas.get_viagens_pessoa(1,cur)) == 0

	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	assert len(consultas.get_viagens_pessoa(1,cur)) == 1
	assert len(consultas.get_viagens_pessoa(1,cur,intervalo_data=[datetime(2023,1,1),datetime.max])) == 0
	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 1692624035,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	assert len(consultas.get_viagens_pessoa(1,cur,intervalo_data=[datetime(2023,8,21,10,20),datetime(2023,8,21,10,21)])) == 1
	assert len(consultas.get_viagens_pessoa(1,cur,intervalo_data=[datetime(2023,8,21,10,19),datetime(2023,8,21,10,20)])) == 0
	assert len(consultas.get_viagens_pessoa(1,cur,intervalo_data=[datetime(2023,8,21,10,20,35),datetime(2023,8,21,10,35)])) == 1


def test_get_todas_infos_viagem(conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.get_todas_infos_viagem(1,cur) is None
	assert consultas.get_todas_infos_viagem(2,cur) is None

	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}

	par_valido = {
		"idPonto1":1,
		"idPonto2":2,
		"distancia": 10,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}

	cur = insercoes.insere_PARVALIDO(par_valido,cur)

	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)



	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)
	viagem_info_1 = consultas.get_todas_infos_viagem(1,cur)
	assert viagem_info_1 is not None
	assert viagem_info_1[0] == 1	
	assert viagem_info_1[2] == 1	
	assert viagem_info_1[3] == 2	
	assert viagem_info_1[4] == "R$ 0,00"	
	assert viagem_info_1[5] == "Aprovado"	
	assert viagem_info_1[8] == 10	
	assert viagem_info_1[9] == "APROVADO"	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 1692624035,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",10.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)
	viagem_info_2 = consultas.get_todas_infos_viagem(2,cur)
	assert viagem_info_2 is not None
	assert viagem_info_2[0] == 1	
	assert viagem_info_2[2] == 1	
	assert viagem_info_2[3] == 2	
	assert viagem_info_2[4] == "R$ 10,00"		
	assert viagem_info_2[5] == "Aprovado"	
	assert viagem_info_2[8] == 10	
	assert viagem_info_2[9] == "APROVADO"	

def test_get_pessoa_bilhete_unico(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	assert consultas.get_pessoa_bilhete_unico(resposta_mock_pessoa['NumBilheteUnico'],cur) is None

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	assert consultas.get_pessoa_bilhete_unico(resposta_mock_pessoa['NumBilheteUnico'],cur)[0] == 1

def test_get_pessoa_id(conexao_bd_testes):
	
	con = conexao_bd_testes
	cur = con.cursor()

	assert consultas.get_pessoa_id(resposta_mock_pessoa['CPF'],cur) is None

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.get_pessoa_id(resposta_mock_pessoa['CPF'],cur)[0] == 1


def test_bilhete_unico_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	assert consultas.bilhete_unico_existe(resposta_mock_pessoa['NumBilheteUnico'],cur) is None

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	existe = consultas.bilhete_unico_existe(resposta_mock_pessoa['NumBilheteUnico'],cur)
	assert existe[0] == str(resposta_mock_pessoa['NumBilheteUnico'])
	assert existe[1] == 1
	assert existe[4] == dados_bilhete_unico["ativo"]

def test_get_bilhetes_unicos_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.get_bilhetes_unicos_pessoa(1,cur) == []

	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)

	assert len(consultas.get_bilhetes_unicos_pessoa(1,cur)) == 1


def test_dump_bilhetes_ativos(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.dump_bilhetes_ativos(cur) == []
	
	bilhete_unico_ativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)

	assert len(consultas.dump_bilhetes_ativos(cur)) == 1

	bilhete_unico_nao_ativo = bilhete_unico_ativo
	bilhete_unico_nao_ativo["NumBilheteUnico"] = "124312412"
	bilhete_unico_nao_ativo["ativo"] = False
	insercoes.insere_BILHETEUNICO(bilhete_unico_nao_ativo,cur)

	assert len(consultas.dump_bilhetes_ativos(cur)) == 1

	bilhete_unico_ativo_2 = bilhete_unico_ativo
	bilhete_unico_ativo_2["NumBilheteUnico"] = "1412"
	bilhete_unico_ativo_2["ativo"] = True
	insercoes.insere_BILHETEUNICO(bilhete_unico_nao_ativo,cur)
	assert len(consultas.dump_bilhetes_ativos(cur)) == 2


def test_pessoa_possui_token_redefinicao(conexao_bd_testes):

	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	assert consultas.pessoa_possui_token_redefinicao(1,cur) is None

	insercoes.insere_TOKENREDEFINICAOSENHA({"idPessoa":1,"token":"tokenredefinicao","dataValidade":resposta_mock["DataResposta"]},cur)

	resposta = consultas.pessoa_possui_token_redefinicao(1,cur)
	assert len(resposta) == 3
	assert resposta[0] == 1
	assert resposta[1] == "tokenredefinicao"

def test_token_redefinicao_existe(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	sessao = {
		"idPessoa":1,
		"token":"token",
		"dataValidade":resposta_mock["DataResposta"]
	}
	cur = insercoes.insere_SESSAO(sessao,cur)

	assert consultas.token_redefinicao_existe("tokenredefinicao",cur) is None

	insercoes.insere_TOKENREDEFINICAOSENHA({"idPessoa":1,"token":"tokenredefinicao","dataValidade":resposta_mock["DataResposta"]},cur)

	assert consultas.token_redefinicao_existe("tokenredefinicao",cur) is not None

	insercoes.insere_TOKENREDEFINICAOSENHA({"idPessoa":1,"token":"tokenredefinicao2","dataValidade":resposta_mock["DataResposta"]},cur)

	assert consultas.token_redefinicao_existe("tokenredefinicao",cur) is None
	assert consultas.token_redefinicao_existe("tokenredefinicao2",cur) is not None

def test_get_trajeto_viagem(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	resultado_antes = consultas.get_trajeto_viagem(1,cur)
	assert resultado_antes is None

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': '{"eu sou um fake trajeto":"eu sou um fake trajeto"}'})

	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	resultado_depois = consultas.get_trajeto_viagem(1,cur)
	assert resultado_depois is not None
	assert resultado_depois[0] == '{"eu sou um fake trajeto":"eu sou um fake trajeto"}'

def test_get_ultima_troca_bu(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	ultima_troca = consultas.get_ultima_troca_bu(1,cur)
	assert ultima_troca is None

	bilhete_unico_ativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)
	con.commit()
	ultima_troca = consultas.get_ultima_troca_bu(1,cur)
	assert ultima_troca[0] == str(resposta_mock_pessoa['NumBilheteUnico'])
	assert ultima_troca[1] == None

	bilhete_unico_novo_1 = {
		"NumBilheteUnico":"214214324",
		"idPessoa":1,
		"dataInicio":"2023-09-12 15:33:32.987674-03:00",
		"dataFim":"2023-10-25 15:33:32.987674-03:00",
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_novo_1,cur)
	con.commit()
	ultima_troca = consultas.get_ultima_troca_bu(1,cur)
	assert ultima_troca[0] == "214214324"
	assert ultima_troca[1] == datetime(2023, 10, 25, 15, 33, 32, 987674)
	bilhete_unico_ativo = {
		"NumBilheteUnico":"9999999",
		"idPessoa":1,
		"dataInicio":"2023-09-30 15:33:32.987674-03:00",
		"dataFim":"2023-11-25 15:33:32.987674-03:00",
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)
	con.commit()
	ultima_troca = consultas.get_ultima_troca_bu(1,cur)
	assert ultima_troca[0] == "9999999"
	assert ultima_troca[1] == datetime(2023, 11, 25, 15, 33, 32, 987674)

def test_get_historico_envio_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	bilhete_unico_ativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)
	
	resultado = consultas.get_historico_envio_pessoa(1,cur)
	assert len(resultado) == 0
	envio_1 =  {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"DataEnvio":"2023-01-01",
		"Valor":10,
		"Confirmado":True,
		"Observacao":None,
	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_1,cur)
	envio_2 =  {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"DataEnvio":"2022-01-01",
		"Valor":100,
		"Confirmado":False,
		"Observacao":None

	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_2,cur)
	envio_3 =  {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"DataEnvio":"2024-01-01",
		"Valor":1000,
		"Confirmado":False,
		"Observacao":None,
	}
	insercoes.insere_HISTORICOENVIOSPTRANS(envio_3,cur)
	resultado = consultas.get_historico_envio_pessoa(1,cur)

	assert len(resultado) == 3
	assert resultado[0][2] == "R$ 1.000,00"
	assert resultado[1][2] == "R$ 10,00"
	assert resultado[2][2] == "R$ 100,00"

def test_get_pares_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	
	cur = con.cursor()
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	localizacao_1 = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_1,cur)
	localizacao_2 = {
		"coordenadas": [0,2],
		"endereco":"Rua Rua2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_2,cur)
	
	localizacao_3 = {
		"coordenadas": [0,3],
		"endereco":"Rua Rua3",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_3,cur)

	localizacao_4 = {
		"coordenadas": [0,4],
		"endereco":"Rua Rua4",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_4,cur)

	par_valido_1 = {
		"idPonto1":1,
		"idPonto2":2,
		"distancia": 12,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_1,cur)

	par_valido_2 = {
		"idPonto1":1,
		"idPonto2":3,
		"distancia": 13,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_2,cur)

	par_valido_3 = {
		"idPonto1":3,
		"idPonto2":4,
		"distancia": 34,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_3,cur)


	par_valido_3 = {
		"idPonto1":5,
		"idPonto2":1,
		"distancia": 34,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_3,cur)


	resposta = consultas.get_pares_localizacao(1,cur)
	assert len(resposta) == 3
	assert resposta[0][0] != 1 or resposta[0][1] != 1
	assert resposta[1][0] != 1 or resposta[1][1] != 1
	assert resposta[2][0] != 1 or resposta[2][1] != 1

	resposta = consultas.get_pares_localizacao(5,cur)
	assert len(resposta) == 1

	resposta = consultas.get_pares_localizacao(3,cur)
	assert len(resposta) == 2



def test_get_pessoa_info_pelo_id_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	assert consultas.get_pessoa_info_pelo_id_pessoa(1,cur) is None

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert list(consultas.get_pessoa_info_pelo_id_pessoa(1,cur)) == \
	[str(resposta_mock_pessoa["CPF"]),str(resposta_mock_pessoa["Email"]),None,str(resposta_mock_pessoa["Nome"])]


def test_dump_viagem(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	resultado_antes = consultas.dump_viagem(1,cur)
	assert resultado_antes is None

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	resultado_depois = consultas.dump_viagem(1,cur)
	assert len(resultado_depois) == 12

def test_get_bilhete_ativo_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert consultas.dump_bilhetes_ativos(cur) == []
	
	bilhete_unico_inativo = {
		"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
		"idPessoa":1,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":False,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}

	insercoes.insere_BILHETEUNICO(bilhete_unico_inativo,cur)

	assert consultas.get_bilhete_ativo_pessoa(1,cur) is None

	bilhete_unico_ativo = bilhete_unico_inativo
	bilhete_unico_ativo["NumBilheteUnico"] = "124312412"
	bilhete_unico_ativo["ativo"] = True
	insercoes.insere_BILHETEUNICO(bilhete_unico_ativo,cur)

	assert consultas.get_bilhete_ativo_pessoa(1,cur)[0] == "124312412"

def test_get_pessoas_para_notificar(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()


	data_notificar = datetime(2023,8,21,10,20)

	freezer = freeze_time("2023-10-18 09:38:00", tz_offset=-3)

	freezer.start()


	assert consultas.get_pessoas_para_notificar(data_notificar,cur) == []

	grupo1 = {
		"idGrupo":1,
		"remuneracao":1.0
	}	
	cur = insercoes.insere_GRUPOPESQUISA(grupo1,cur)
	grupo2 = {
		"idGrupo":2,
		"remuneracao":2.0
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo2,cur)

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	cur.execute("UPDATE PESSOA SET idGrupo = 1 WHERE idPessoa = 1;")
	con.commit()

	freezer.stop()
	
	pessoas = consultas.get_pessoas_para_notificar(data_notificar,cur)
	assert len(pessoas) == 1
	assert pessoas[0][0] == 1
	assert pessoas[0][1] == resposta_mock_pessoa["Nome"]
	assert pessoas[0][2] == resposta_mock_pessoa["Email"]
	assert pessoas[0][3] == grupo1["idGrupo"]
	assert parsers.parse_dinheiro_float(pessoas[0][5]) == grupo1["remuneracao"]

	grupo1["remuneracao"] = 3
	cur = insercoes.insere_GRUPOPESQUISA(grupo1,cur)
	con.commit()

	pessoas = consultas.get_pessoas_para_notificar(data_notificar,cur)
	assert len(pessoas) == 1
	assert parsers.parse_dinheiro_float(pessoas[0][5]) == grupo1["remuneracao"]
	
def test_get_residencia_pessoa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	assert len(consultas.get_residencia_pessoa(1,cur)) == 0

	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "Residência"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)

	assert len(consultas.get_residencia_pessoa(1,cur)) == 1
	assert consultas.get_residencia_pessoa(1,cur)[0][0] == 1

	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "Residência"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)		
	
	assert len(consultas.get_residencia_pessoa(1,cur)) == 2
	assert consultas.get_residencia_pessoa(1,cur)[0][0] == 1
	assert consultas.get_residencia_pessoa(1,cur)[1][0] == 2

def test_get_pessoas_com_ambas_localizacoes(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	pessoa_2 = dict(resposta_mock_pessoa)
	pessoa_2["Nome"] = "pessoa2"
	pessoa_2["CPF"] = "325092852"
	pessoa_2["Email"] = "pessoa2@email.com"
	pessoa_2["RG"] = "234214242"
	pessoa_2["NumBilheteUnico"] = "9240230432"
	cur = insercoes.insere_PESSOA(pessoa_2,cur)

	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0,1],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	localizacao3 = {
		"coordenadas": [0,2],
		"endereco":"Estacao 1",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao3,cur)
	localizacao4 = {
		"coordenadas": [0,3],
		"endereco":"Estacao 2",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao4,cur)


	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)) == 0
	assert len(consultas.get_pessoas_com_ambas_localizacoes(2,3,cur)) == 0
	assert len(consultas.get_pessoas_com_ambas_localizacoes(2,1,cur)) == 0
	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,3,cur)) == 0

	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":1,
		"apelido": "Residência"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)

	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)) == 0

	apelido_localizacao2 = {
		"idPessoa":2,
		"idLocalizacao":1,
		"apelido": "Residência"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)

	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)) == 0

	apelido_localizacao3 = {
		"idPessoa":1,
		"idLocalizacao":2,
		"apelido": "Escola"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao3,cur)

	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)) == 1
	assert consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)[0][0] == 1
	assert consultas.get_pessoas_com_ambas_localizacoes(2,1,cur)[0][0] == 1

	apelido_localizacao4 = {
		"idPessoa":2,
		"idLocalizacao":2,
		"apelido": "Escola"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao4,cur)
	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)) == 2

	pessoa_estacao1 = {
		"idPessoa":1,
		"idLocalizacao":3,
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao1,cur)
	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,3,cur)) == 1

	pessoa_estacao2 = {
		"idPessoa":1,
		"idLocalizacao":4,
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao2,cur)
	assert len(consultas.get_pessoas_com_ambas_localizacoes(4,3,cur)) == 1

	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,2,cur)) == 2
	assert len(consultas.get_pessoas_com_ambas_localizacoes(2,3,cur)) == 1
	assert len(consultas.get_pessoas_com_ambas_localizacoes(2,1,cur)) == 2
	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,3,cur)) == 1
	assert len(consultas.get_pessoas_com_ambas_localizacoes(1,4,cur)) == 1
	assert len(consultas.get_pessoas_com_ambas_localizacoes(3,4,cur)) == 1

def test_get_feedback_viagem_info(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)

	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)

	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})

	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Reprovado",0.0)
	
	cur = insercoes.insere_VIAGEM(viagem,cur)

	assert consultas.get_feedback_viagem_info(1,cur) is None

	dict_feedback = {
		"idViagem":1,
		"tokenFeedback":"TOKEN",
		"respondido": False,
		"motivoOriginal":"Motivo Original",
		"tempoClima":None,
		"outrosApps":None,
		"feedbackGeral":None,
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEM(dict_feedback,cur)

	resposta = consultas.get_feedback_viagem_info(1,cur)
	assert resposta[0] == dict_feedback["tokenFeedback"]
	assert resposta[1] == dict_feedback["respondido"]
	assert resposta[2] == dict_feedback["motivoOriginal"]


def test_atingiu_limite_diario_viagens_perdidas(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	freezer = freeze_time("2023-10-18 09:38:00", tz_offset=-3)

	freezer.start()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=2) == False
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=0) == True

	dict_insercao = {
		"idPessoa":1,
		"dataFeedback":"2023-10-18 06:38:09",
		"quandoPerdeu":"Perdi a viagem",
		"algoDiferente":"diferente",
		"verApp":"2",
		"feedbackGeral":"geral",
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEMPERDIDA(dict_insercao,cur)

	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=1) == True
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=2) == False

	dict_insercao = {
		"idPessoa":1,
		"dataFeedback":"2023-10-18 00:01:09",
		"quandoPerdeu":"Perdi a viagem",
		"algoDiferente":"diferente",
		"verApp":"2",
		"feedbackGeral":"geral",
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEMPERDIDA(dict_insercao,cur)
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=1) == True
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=2) == True
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=3) == False
	dict_insercao = {
		"idPessoa":1,
		"dataFeedback":"2023-10-17 23:59:09",
		"quandoPerdeu":"Perdi a viagem",
		"algoDiferente":"diferente",
		"verApp":"2",
		"feedbackGeral":"geral",
		"respostasDinamicas":None
	}
	insercoes.insere_FEEDBACKVIAGEMPERDIDA(dict_insercao,cur)
	assert consultas.atingiu_limite_diario_viagens_perdidas(1,cur,limite=3) == False


	freezer.stop()


def test_dump_pares_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	
	loc1 = {
		"coordenadas": [-45.12312,-48.0013],
		"endereco":"Rua um",
		"tipoLocalizacao":"qualquer"
	}
	loc2 = {
		"coordenadas": [-45.124,-48.011],
		"endereco":"Rua dois",
		"tipoLocalizacao":"qualquer"
	}
	loc3 = {
		"coordenadas": [-45.123,-48.011],
		"endereco":"Estacao 1",
		"tipoLocalizacao":"estacao"
	}
	loc4 = {
		"coordenadas": [-45.323,-48.011],
		"endereco":"Estacao 2",
		"tipoLocalizacao":"estacao"
	}
	insercoes.insere_LOCALIZACAO(loc1,cur)
	insercoes.insere_LOCALIZACAO(loc2,cur)
	insercoes.insere_LOCALIZACAO(loc3,cur)
	insercoes.insere_LOCALIZACAO(loc4,cur)

	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":1,
		"apelido": "apelido1"
	}
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":2,
		"apelido": "apelido2"
	}
	apelido_localizacao3 = {
		"idPessoa":1,
		"idLocalizacao":3,
		"apelido": "apelido3"
	}
	
	pares = consultas.dump_pares_localizacoes([1,2,3],cur)
	assert len(pares) == 0

	cur = insercoes.insere_PARVALIDO({"idPonto1":1,"idPonto2":2,"distancia":10,"trajeto":{}},cur)
	cur = insercoes.insere_PARVALIDO({"idPonto1":1,"idPonto2":3,"distancia":200,"trajeto":{}},cur)
	cur = insercoes.insere_PARVALIDO({"idPonto1":2,"idPonto2":3,"distancia":300,"trajeto":{}},cur)
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao3,cur)

	pares = consultas.dump_pares_localizacoes([1,2,3],cur)
	assert len(pares) == 3

def test_get_todas_infos_lista_viagens(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	pessoa_2 = dict(resposta_mock_pessoa)
	pessoa_2["Nome"] = "pessoa2"
	pessoa_2["CPF"] = "325092852"
	pessoa_2["Email"] = "pessoa2@email.com"
	pessoa_2["RG"] = "234214242"
	pessoa_2["NumBilheteUnico"] = "9240230432"
	cur = insercoes.insere_PESSOA(pessoa_2,cur)

	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': ""})
	viagem = ViagemBD(2,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	assert consultas.get_todas_infos_lista_viagens([1],3,cur) == []	
	assert consultas.get_todas_infos_lista_viagens([0],1,cur) == []	
	assert consultas.get_todas_infos_lista_viagens([],1,cur) == []	
	assert consultas.get_todas_infos_lista_viagens([1],1,cur) == []	
	assert len(consultas.get_todas_infos_lista_viagens([1],2,cur)) == 1	

	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)
	assert len(consultas.get_todas_infos_lista_viagens([1],2,cur)) == 1	
	assert len(consultas.get_todas_infos_lista_viagens([2],1,cur)) == 1
	
	maximo_viagens = 100
	for i in range(0,maximo_viagens):
		viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
		cur = insercoes.insere_VIAGEM(viagem,cur)

	assert len(consultas.get_todas_infos_lista_viagens([2],1,cur)) == 1
	assert len(consultas.get_todas_infos_lista_viagens(list(range(2,maximo_viagens+3)),1,cur)) == maximo_viagens+1

def test_dump_bonus(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	assert consultas.dump_bonus(1,cur) == None

	dict_bonus = {
		"nome":"bonus",
		"valor":"R$ 1,00",
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":datetime(2023, 10, 10, 20, 1, 20),
		"data_fim":datetime(2023, 10, 10, 20, 1, 21),
	}

	id_bonus = insercoes.insere_BONUS(dict_bonus,cur)
	
	assert list(consultas.dump_bonus(id_bonus,cur)) == (list(dict_bonus.values()))

def test_bonus_existe(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()


	dict_bonus = {
		"nome":"bonus",
		"valor":"R$ 1,00",
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":datetime(2023, 10, 10, 20, 1, 20),
		"data_fim":datetime(2023, 10, 10, 20, 1, 21),
	}
	assert consultas.bonus_existe(dict_bonus["nome"],cur) == None

	id_bonus = insercoes.insere_BONUS(dict_bonus,cur)

	assert consultas.bonus_existe(dict_bonus["nome"],cur)[0] == 1
	
def test_get_bonus_usuario(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	aux_adiciona_pessoa(cur)

	dict_bonus = {
		"nome":"bonus1",
		"valor":"R$ 1,00",
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":datetime(2023, 10, 10, 20, 1, 20),
		"data_fim":datetime(2023, 10, 10, 20, 1, 21),
	}
	insercoes.insere_BONUS(dict_bonus,cur)

	dict_bonus["nome"] = "bonus2"
	dict_bonus["visivel"] = False
	insercoes.insere_BONUS(dict_bonus,cur)

	dict_bonus["nome"] = "bonus3"
	dict_bonus["visivel"] = True
	insercoes.insere_BONUS(dict_bonus,cur)

	dict_bonus["nome"] = "bonus4"
	insercoes.insere_BONUS(dict_bonus,cur)

	dict_bonus["nome"] = "bonus5"
	dict_bonus["visivel"] = False
	insercoes.insere_BONUS(dict_bonus,cur)

	bonus_usuario = consultas.get_bonus_usuario(1,cur)
	assert len(bonus_usuario) == 3
	assert bonus_usuario[0][0] == 1
	assert bonus_usuario[0][1] == "bonus1"
	assert bonus_usuario[0][7] is None
	assert bonus_usuario[1][7] is None
	assert bonus_usuario[2][7] is None

	pessoa_bonus = {
		"idPessoa":1,
		"idBonus": 1,
		"dataConcessao":datetime(2023, 10, 10, 20, 1, 20)
	}
	insercoes.insere_PESSOABONUS(pessoa_bonus,cur)

	pessoa_bonus["idBonus"] = 3
	insercoes.insere_PESSOABONUS(pessoa_bonus,cur)

	pessoa_bonus["idBonus"] = 5
	insercoes.insere_PESSOABONUS(pessoa_bonus,cur)

	bonus_usuario = consultas.get_bonus_usuario(1,cur)
	
	assert len(bonus_usuario) == 3
	assert bonus_usuario[0][0] == 1
	assert bonus_usuario[0][1] == "bonus1"
	assert bonus_usuario[0][7] is not None

	assert bonus_usuario[1][0] == 3
	assert bonus_usuario[1][7] is not None

	assert bonus_usuario[2][0] == 4
	assert bonus_usuario[2][7] is None


def test_get_contestacoes_usuario(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	assert consultas.get_contestacoes_usuario(1,cur) == []

	aux_adiciona_pessoa(cur)
	aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,1]])
	aux_adiciona_locs_user(cur)
	aux_adiciona_viagem(cur,status="Reprovado",remuneracao=0.0)

	data_contestacao = datetime(2023,2,20,20)
	justificativa = "japjfafajfklafaks"
	contestacao = {
		"idViagem":1,
		"data":data_contestacao,
		"justificativa":justificativa
	}
	insercoes.insere_CONTESTACAO(contestacao,cur)

	assert len(consultas.get_contestacoes_usuario(1,cur)) == 1
	assert len(consultas.get_contestacoes_usuario(2,cur)) == 0
	assert consultas.get_contestacoes_usuario(1,cur)[0] == \
		(1,data_contestacao,justificativa,None,None,"R$ 0,00")

	resposta_contestacao = "não deu pra aprovar"
	cur.execute("UPDATE CONTESTACAO SET (aprovada,resposta)=('f',%s)",[resposta_contestacao])

	assert len(consultas.get_contestacoes_usuario(1,cur)) == 1
	assert len(consultas.get_contestacoes_usuario(2,cur)) == 0
	assert consultas.get_contestacoes_usuario(1,cur)[0] == \
		(1,data_contestacao,justificativa,False,resposta_contestacao,"R$ 0,00")

	aux_adiciona_viagem(cur,status="Reprovado",remuneracao=10.0)
	data_contestacao_2 = datetime(2024,2,20,20)
	justificativa_2 = "2"
	contestacao_2 = {
		"idViagem":2,
		"data":data_contestacao_2,
		"justificativa":justificativa_2
	}
	insercoes.insere_CONTESTACAO(contestacao_2,cur)
	assert len(consultas.get_contestacoes_usuario(1,cur)) == 2
	assert len(consultas.get_contestacoes_usuario(2,cur)) == 0
	assert consultas.get_contestacoes_usuario(1,cur)[0] == \
		(2,data_contestacao_2,justificativa_2,None,None,"R$ 10,00")
	cur.execute("UPDATE CONTESTACAO SET aprovada='t' where idviagem = 2")
	assert consultas.get_contestacoes_usuario(1,cur)[0] == \
		(2,data_contestacao_2,justificativa_2,True,None,"R$ 10,00")

	assert consultas.get_contestacoes_usuario(1,cur)[1] == \
		(1,data_contestacao,justificativa,False,resposta_contestacao,"R$ 0,00")

def test_get_localizacao_info(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	assert consultas.get_localizacao_info(1,cur) is None

	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[0,1],[0,2]],tipos=["qualquer","estacao","qualquer"])

	assert consultas.get_localizacao_info(10,cur) is None
	assert consultas.get_localizacao_info(1,cur)[0] == info_locs[0]["endereco"]
	assert consultas.get_localizacao_info(2,cur)[2] == info_locs[1]["tipoLocalizacao"]

def test_pessoa_validou_locs(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	assert consultas.pessoa_validou_locs(1,cur) == False

	aux_adiciona_pessoa(cur)	

	assert consultas.pessoa_validou_locs(1,cur) == False

	cur.execute("UPDATE PESSOA SET validouLocais='t'")

	assert consultas.pessoa_validou_locs(1,cur) == True

dados_bilhete_unico = {
	"NumBilheteUnico":resposta_mock_pessoa['NumBilheteUnico'],
	"idPessoa":1,
	"dataInicio":"2023-09-25 15:33:32.987674-03:00",
	"dataFim":None,
	"ativo":True,
	"concedido":0,
	"aguardandoEnvio":0,
	"aguardandoResposta":0
}
