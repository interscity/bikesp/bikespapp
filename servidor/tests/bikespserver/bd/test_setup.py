import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.tests.utils.bd import reseta_bd_teste
from servidor.bikespserver.bd import conecta
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import remocoes

def test_reseta_bd_teste():
	reseta_bd_teste()
	con = conecta.conecta()
	cur = con.cursor()

	cur.execute("""
		SELECT EXISTS (
		SELECT 1
		FROM information_schema.tables
		WHERE table_name = 'pessoa'
		) AS table_existence;
	""")
	resultado = cur.fetchone()
	con.commit()
	cur.close()
	con.close()
	assert resultado[0] == True

def test_setup(conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()

	cur.execute("""
		SELECT EXISTS (
		SELECT 1
		FROM information_schema.tables
		WHERE table_name = 'pessoa'
		) AS table_existence;
	""")
	resultado = cur.fetchone()
	con.commit()
	cur.close()
	assert resultado[0] == True


def test_trigger_grupo_pesquisa(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	grupo4 = {
		"idGrupo":4,
		"remuneracao":12.34
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo4,cur)

	cur.execute("SELECT * FROM HISTORICOGPESQUISA;")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == grupo4["idGrupo"]
	assert resultado[0][4] == "inserção"

	grupo10 = {
		"idGrupo":10,
		"remuneracao":1.23
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo10,cur)
	cur.execute("SELECT * FROM HISTORICOGPESQUISA;")
	resultado = cur.fetchall()
	assert len(resultado) == 2
	assert resultado[1][0] == grupo10["idGrupo"]
	assert resultado[1][4] == "inserção"

	con.commit()
	cur.close()

	grupo4["remuneracao"] = 0

	cur = con.cursor()
	cur = insercoes.insere_GRUPOPESQUISA(grupo4,cur)
	cur.execute("SELECT * FROM HISTORICOGPESQUISA;")
	resultado = cur.fetchall()
	assert len(resultado) == 3
	assert resultado[2][0] == grupo4["idGrupo"]
	assert resultado[2][2] == "R$ 12,34"
	assert resultado[2][3] == "R$ 0,00"
	assert resultado[2][4] == "atualização"

	con.commit()
	cur.close()

	cur = con.cursor()
	cur = remocoes.remove_GRUPOPESQUISA(grupo4["idGrupo"],cur)
	cur.execute("SELECT * FROM HISTORICOGPESQUISA;")
	resultado = cur.fetchall()
	assert len(resultado) == 4
	assert resultado[3][0] == grupo4["idGrupo"]
	assert resultado[3][4] == "remoção"

	cur.close()
