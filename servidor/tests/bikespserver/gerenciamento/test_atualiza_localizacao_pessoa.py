import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.bd.insercoes import insere_APELIDOLOCALIZACAO, insere_LOCALIZACAO
from servidor.bikespserver.gerenciamento.atualiza_localizacao_pessoa import atualiza_localizacao_pessoa, atualiza_apelido_localizacao_pessoa
from servidor.bikespserver.utils import parsers
from servidor.tests.utils.pessoa import aux_adiciona_locs_user, aux_adiciona_pessoa
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido

mock_endereco = {
	"rua": "Praça Charles Miller",
	"numero": "0",
	"bairro": "Pacaembu",
	"cep": "01234-900",
	"cidade": "São Paulo",
	"estado": "SP"
}


# Testes atualiza_localizacao_pessoa

def test_id_localizacao_inexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])

	(sucesso,status) = atualiza_localizacao_pessoa(1,2,mock_endereco,cur,force_prod=False)
	assert not sucesso
	assert status == "A localização informada não pode ser modificada."

def test_localizacao_estacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["estacao"])
	aux_adiciona_locs_user(cur,[1],1,["estacao"])

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod=False)
	assert not sucesso
	assert status == "A localização informada não pode ser modificada."

def test_localizacao_nao_vinculada(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod=False)
	assert not sucesso
	assert status == "A localização informada não pode ser modificada."

def test_usuario_com_mais_de_uma_residencia(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,1]],tipos=["qualquer", "qualquer"])
	insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":1,"apelido":"Residência"},cur)
	insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":2,"apelido":"Residência"},cur)

	(sucesso, status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod=False)
	assert not sucesso
	assert status == "Usuário possui mais de uma residência cadastrada."

def test_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])

	mock_endereco_str = parsers.parse_endereco(rua=mock_endereco["rua"],
	                                           numero=mock_endereco["numero"],
	                                           bairro=mock_endereco["bairro"],
	                                           cep=mock_endereco["cep"],
	                                           cidade=mock_endereco["cidade"],
	                                           estado=mock_endereco["estado"])

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][1] != mock_endereco_str
	assert resultado[0][3] == "qualquer"

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod=False)
	assert sucesso
	assert status == "Sucesso"

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][1] == mock_endereco_str
	assert resultado[0][3] == "qualquer"

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == 2

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

def test_sucesso_residencia(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":1,"apelido":"Residência"},cur)

	mock_endereco_str = parsers.parse_endereco(rua=mock_endereco["rua"],
	                                           numero=mock_endereco["numero"],
	                                           bairro=mock_endereco["bairro"],
	                                           cep=mock_endereco["cep"],
	                                           cidade=mock_endereco["cidade"],
	                                           estado=mock_endereco["estado"])

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][1] != mock_endereco_str
	assert resultado[0][3] == "qualquer"

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod=False)
	assert sucesso
	assert status == "Sucesso"

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][1] == mock_endereco_str
	assert resultado[0][3] == "qualquer"

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0] == (1, 2, "Residência")

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	cur.execute("SELECT endereco FROM PESSOA")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == mock_endereco_str

def test_sucesso_pares_validos(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,1],[2,2]],tipos=["qualquer","qualquer","estacao"])

	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur,[1,2,3],1,["qualquer","qualquer","estacao"])

	aux_adiciona_par_valido(cur,1,2,1000)
	aux_adiciona_par_valido(cur,1,3,1000)
	aux_adiciona_par_valido(cur,2,3,1000)

	cur.execute("SELECT idPonto1,idPonto2 FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 3
	assert resultado == [(1,2),(1,3),(2,3)]

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod = False)

	cur.execute("SELECT idPonto1,idPonto2 FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 3
	assert resultado == [(2,3),(2,4),(3,4)]

def test_sucesso_sem_alteracao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	mock_endereco_str = parsers.parse_endereco(rua=mock_endereco["rua"],
	                                           numero=mock_endereco["numero"],
	                                           bairro=mock_endereco["bairro"],
	                                           cep=mock_endereco["cep"],
	                                           cidade=mock_endereco["cidade"],
	                                           estado=mock_endereco["estado"])

	info_pessoa = aux_adiciona_pessoa(cur)
	insere_LOCALIZACAO({"endereco":mock_endereco_str,"coordenadas":[0,0],"tipoLocalizacao":"qualquer"},cur)
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][1] == mock_endereco_str
	assert resultado[0][3] == "qualquer"

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod=False)
	assert sucesso
	assert status == "Localização não alterada"

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][1] == mock_endereco_str
	assert resultado[0][3] == "qualquer"

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

def test_sucesso_localizacao_multiplos_usuarios(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,1],[2,2]],tipos=["qualquer","qualquer","estacao"])

	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur,[1,2,3],1,["qualquer","qualquer","estacao"])

	info_pessoa_2 = aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur,[1,3],2,["qualquer","estacao"])

	aux_adiciona_par_valido(cur,1,2,1000)
	aux_adiciona_par_valido(cur,1,3,1000)
	aux_adiciona_par_valido(cur,2,3,1000)

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 3

	cur.execute("SELECT idPonto1,idPonto2 FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 3
	assert resultado == [(1,2),(1,3),(2,3)]

	(sucesso,status) = atualiza_localizacao_pessoa(1,1,mock_endereco,cur,force_prod = False)

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 4

	cur.execute("SELECT idPonto1,idPonto2 FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 5
	assert resultado == [(1,2),(1,3),(2,3),(2,4),(3,4)]

# Testes atualiza_apelido_localizacao_pessoa

def test_apelido_id_localizacao_inexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])

	(sucesso,status) = atualiza_apelido_localizacao_pessoa(1,2,"apelido",cur)
	assert not sucesso
	assert status == "A localização informada não pode ser modificada."

def test_apelido_localizacao_estacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["estacao"])
	aux_adiciona_locs_user(cur,[1],1,["estacao"])

	(sucesso,status) = atualiza_apelido_localizacao_pessoa(1,1,"apelido",cur)
	assert not sucesso
	assert status == "A localização informada não pode ser modificada."

def test_apelido_localizacao_nao_vinculada(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])

	(sucesso,status) = atualiza_apelido_localizacao_pessoa(1,1,"apelido",cur)
	assert not sucesso
	assert status == "A localização informada não pode ser modificada."

def test_apelido_localizacao_residencia(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,1]],tipos=["qualquer", "qualquer"])
	insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":1,"apelido":"Residência"},cur)
	insere_APELIDOLOCALIZACAO({"idPessoa":1,"idLocalizacao":2,"apelido":"Não uma residência"},cur)

	(sucesso,status) = atualiza_apelido_localizacao_pessoa(1,1,"Não mais uma residência",cur)
	assert not sucesso
	assert status == "Não é possível alterar o apelido de uma residência."

	(sucesso,status) = atualiza_apelido_localizacao_pessoa(1,2,"Residência",cur)
	assert not sucesso
	assert status == "Não é possível alterar o apelido de uma residência."

def test_apelido_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1
	assert resultado[0][2] != "apelido"

	(sucesso,status) = atualiza_apelido_localizacao_pessoa(1,1,"apelido",cur)
	assert sucesso
	assert status == "Sucesso"

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0] == (1, 1, "apelido")
