import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.adiciona_localizacao_pessoa import adiciona_localizacao_pessoa
from servidor.bikespserver.gerenciamento.remove_localizacao_pessoa import desassocia_localizacao_pessoa
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes
from servidor.tests.utils.pessoa import aux_adiciona_pessoa

def test_desassocia_localizacao_pessoa(conexao_bd_testes):
    con = conexao_bd_testes
    cur = con.cursor()

    localizacoes = [
        {"apelido":"apelido1", "id": 1, "coord": [0, 0], "tipo": "estacao"},
        {"apelido":"apelido2", "id": 2, "coord": [0, 1], "tipo": "qualquer"},
    ]

    aux_adiciona_pessoa(cur)

    for local in localizacoes:
        aux_adiciona_localizacoes(cur,coordenadas=local["coord"],tipos=[local["tipo"]])
        [sucesso,_] = adiciona_localizacao_pessoa(1,local["id"],local["tipo"],local["apelido"],None,None,cur,False)
        
        assert sucesso

        if local["tipo"] == "qualquer":
            query = "SELECT * FROM APELIDOLOCALIZACAO"
        else:
            query = "SELECT * FROM PESSOAESTACAO"
        
        cur.execute(query)
        resultado = cur.fetchall()
        assert len(resultado) == 1

        # Tentar retirar um local que não está associado ao usuário
        # -> não deve ter nenhum efeito
        desassocia_localizacao_pessoa(1, 123, local["tipo"], cur)
        cur.execute(query)
        resultado = cur.fetchall()
        assert len(resultado) == 1

        # Retirar uma localização que está associada a o usuário
        desassocia_localizacao_pessoa(1, local["id"], local["tipo"], cur)
        cur.execute(query)
        resultado = cur.fetchall()
        assert len(resultado) == 0