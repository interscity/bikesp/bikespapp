import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.apaga_localizacao import apaga_localizacao
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido
from servidor.tests.utils.viagem import aux_adiciona_viagem

def test_id_localizacao_inexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	sucesso, status = apaga_localizacao(42, cur)
	assert not sucesso
	assert status == "A localização informada não existe."

def test_localizacao_associada_com_usuario(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	aux_adiciona_locs_user(cur,[1],1,["qualquer"])

	sucesso, status = apaga_localizacao(1, cur)
	assert not sucesso
	assert status == "A localização está associada a usuários."

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

def test_estacao_associada_com_usuario(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["estacao"])
	aux_adiciona_locs_user(cur,[1],1,["estacao"])

	sucesso, status = apaga_localizacao(1, cur)
	assert not sucesso
	assert status == "A localização está associada a usuários."

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

def test_localizacao_associada_com_viagem(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_pessoa = aux_adiciona_pessoa(cur)
	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[0,1]],tipos=["qualquer","qualquer"])
	aux_adiciona_locs_user(cur,[2],1,["qualquer"])
	aux_adiciona_viagem(cur)

	sucesso, status = apaga_localizacao(1, cur)
	assert not sucesso
	assert status == "A localização está associada a viagens."

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 2

def test_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

	sucesso, status = apaga_localizacao(1, cur)
	assert sucesso
	assert status == "Sucesso"

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

def test_sucesso_estacao(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_locs = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["estacao"])

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

	sucesso, status = apaga_localizacao(1, cur)
	assert sucesso
	assert status == "Sucesso"

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 0

def test_apaga_pares_validos(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	info_locs1 = aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"])
	info_locs1 = aux_adiciona_localizacoes(cur,coordenadas=[[0,2]],tipos=["qualquer"])
	aux_adiciona_par_valido(cur,1,2)

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 2

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

	sucesso, status = apaga_localizacao(1, cur)
	assert sucesso
	assert status == "Sucesso"

	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 0
