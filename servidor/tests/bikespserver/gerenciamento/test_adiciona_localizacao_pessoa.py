import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.adiciona_localizacao_pessoa import adiciona_localizacao_pessoa
from servidor.bikespserver.utils import parsers
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido

def test_id_localizacao_inexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	[sucesso,status] = adiciona_localizacao_pessoa(1,1,"estacao",None,None,None,cur,False)
	assert not sucesso
	assert status == "A localização informada não existe"

def test_tipo_errado_informado(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "qualquer"
	apelido = "apelido"
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])
	[sucesso,status] = adiciona_localizacao_pessoa(1,1,"estacao",apelido,None,None,cur,False)
	assert not sucesso
	assert status == "Tipo informado não condiz com tipo da localização"

def test_coordenada_invalida(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [0,0]
	tipo = "qualquer"
	apelido = "apelido"

	[sucesso,status] = adiciona_localizacao_pessoa(1,None,tipo,apelido,None,None,cur,False)
	assert not sucesso
	assert status == "A coordenada informada é inválida"
	[sucesso,status] = adiciona_localizacao_pessoa(1,None,tipo,apelido,None,coordenada,cur,False)
	assert not sucesso
	assert status == "A coordenada informada é inválida"


def test_localizacao_qualquer_sem_apelido(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "qualquer"
	
	[sucesso,status] = adiciona_localizacao_pessoa(1,None,tipo,None,"endereco",coordenada,cur,False)
	assert not sucesso
	assert status == "Apelido faltando para a localização"
	
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])
	[sucesso,status] = adiciona_localizacao_pessoa(1,1,tipo,None,None,None,cur,False)
	assert not sucesso
	assert status == "Apelido faltando para a localização"

def test_pessoa_localizacao_preexistentes(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,2],[3,4]],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_locs_user(cur,ids_localizacoes=[1,2,3],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_par_valido(cur,1,2)
	aux_adiciona_par_valido(cur,1,3)
	aux_adiciona_par_valido(cur,2,3)	


	[sucesso,status] = adiciona_localizacao_pessoa(1,1,"estacao",None,None,None,cur,False)
	assert not sucesso
	assert status == "Pessoa já está ligada a essa localização"
	return

def test_insercao_localizacao_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "qualquer"
	apelido = "apelido"
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])
	[sucesso,status] = adiciona_localizacao_pessoa(1,1,tipo,apelido,None,None,cur,False)
	assert sucesso
	cur.execute("SELECT * FROM LOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert parsers.parse_coordenadas(resultado[0][2]) == parsers.parse_coordenadas(coordenada)

def test_apelido_localizacao_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "qualquer"
	apelido = "apelido"
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])
	[sucesso,status] = adiciona_localizacao_pessoa(1,1,tipo,apelido,None,None,cur,False)
	assert sucesso
	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1
	assert resultado[0][2] == apelido

def test_pessoa_estacao_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "estacao"
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])
	[sucesso,status] = adiciona_localizacao_pessoa(1,1,tipo,None,None,None,cur,False)
	assert sucesso
	cur.execute("SELECT * FROM PESSOAESTACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == 1
	assert resultado[0][1] == 1
	return

def test_pares_validos_sucesso(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)

	coordenada = [-23.5504533, -46.6339112]
	tipo = "estacao"
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])

	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,2],[3,4]],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_locs_user(cur,ids_localizacoes=[2,3,4],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_par_valido(cur,2,3)
	aux_adiciona_par_valido(cur,2,4)
	aux_adiciona_par_valido(cur,3,4)

	[sucesso,status] = adiciona_localizacao_pessoa(1,1,tipo,None,None,None,cur,False)

	assert sucesso
	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 6
	assert resultado[0][:2] == (2,3)
	assert resultado[1][:2] == (2,4)
	assert resultado[2][:2] == (3,4)
	assert resultado[3][:2] == (1,3)
	assert resultado[4][:2] == (1,4)
	assert resultado[5][:2] == (1,2)

def test_id_localizacao_preexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "qualquer"
	apelido = "apelido"
	aux_adiciona_localizacoes(cur,coordenadas=[coordenada],tipos=[tipo])

	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,2],[3,4]],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_locs_user(cur,ids_localizacoes=[2,3,4],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_par_valido(cur,2,3)
	aux_adiciona_par_valido(cur,2,4)
	aux_adiciona_par_valido(cur,3,4)

	[sucesso,status] = adiciona_localizacao_pessoa(1,1,tipo,apelido,None,None,cur,False)
	assert sucesso

	cur.execute("SELECT * FROM APELIDOLOCALIZACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 3
	assert resultado[-1][0] == 1
	assert resultado[-1][1] == 1
	assert resultado[-1][2] == apelido
	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 6
	assert resultado[0][:2] == (2,3)
	assert resultado[1][:2] == (2,4)
	assert resultado[2][:2] == (3,4)
	assert resultado[3][:2] == (1,3)
	assert resultado[4][:2] == (1,4)
	assert resultado[5][:2] == (1,2)

def test_localizacao_nova(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	info_pessoa = aux_adiciona_pessoa(cur)
	coordenada = [-23.5504533, -46.6339112]
	tipo = "estacao"
	apelido = "apelido"
	endereco = "endereco"

	aux_adiciona_localizacoes(cur,coordenadas=[[0,0],[1,2],[3,4]],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_locs_user(cur,ids_localizacoes=[1,2,3],tipos=["estacao","qualquer","qualquer"])
	aux_adiciona_par_valido(cur,1,2)
	aux_adiciona_par_valido(cur,1,3)
	aux_adiciona_par_valido(cur,2,3)

	[sucesso,status] = adiciona_localizacao_pessoa(1,None,tipo,apelido,endereco,coordenada,cur,False)
	assert sucesso

	cur.execute("SELECT * FROM PESSOAESTACAO")
	resultado = cur.fetchall()
	assert len(resultado) == 2
	assert resultado[-1][0] == 1
	assert resultado[-1][1] == 4

	cur.execute("SELECT * FROM PARVALIDO")
	resultado = cur.fetchall()
	assert len(resultado) == 6
	assert resultado[0][:2] == (1,2)
	assert resultado[1][:2] == (1,3)
	assert resultado[2][:2] == (2,3)
	assert resultado[3][:2] == (2,4)
	assert resultado[4][:2] == (3,4)
	assert resultado[5][:2] == (1,4)
	return

