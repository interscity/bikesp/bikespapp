import sys
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.notifica_grupos_pesquisa import pessoas_para_notificar,notifica_pessoa
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.utils import parsers
from servidor.tests.utils.mocks import resposta_mock_pessoa


def test_pessoas_para_notificar(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()


	grupo1 = {
		"idGrupo":1,
		"remuneracao":1.0
	}	
	cur = insercoes.insere_GRUPOPESQUISA(grupo1,cur)
	grupo2 = {
		"idGrupo":2,
		"remuneracao":2.0
	}
	cur = insercoes.insere_GRUPOPESQUISA(grupo2,cur)

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	cur.execute("UPDATE PESSOA SET idGrupo = 1 WHERE idPessoa = 1;")
	pessoa_2 = resposta_mock_pessoa.copy()
	pessoa_2["Nome"] = "Pessoa 2"
	pessoa_2["Email"] = "email2@email.com"
	pessoa_2["RG"] = "12312094801401"
	pessoa_2["CPF"] ="983849290853"
	cur = insercoes.insere_PESSOA(pessoa_2,cur)
	cur.execute("UPDATE PESSOA SET idGrupo = 2 WHERE idPessoa = 2;")

	con.commit()

	data_notificar = datetime.now()
	usuarios = pessoas_para_notificar(con,data_notificar)
	assert len(usuarios) == 0
	data_notificar = datetime(2021,2,2)

	usuarios = pessoas_para_notificar(con,data_notificar)

	assert len(usuarios[1]["pessoas"]) == 1
	assert usuarios[1]["pessoas"][0]["idPessoa"] == 1
	assert parsers.parse_dinheiro_float(usuarios[1]["remuneracao"]) == grupo1["remuneracao"]
	assert len(usuarios[2]["pessoas"]) == 1
	assert usuarios[2]["pessoas"][0]["idPessoa"] == 2
	return

def test_notifica_pessoa():
	nome = "jorge"
	email = "jorge@email.com"
	data_inicio = datetime(2021,2,2)
	remuneracao = "R$ 10,00"
	notificacao = notifica_pessoa(nome,email,data_inicio,remuneracao,debug=True)
	assert notificacao["recebedor"] == [email]
	assert notificacao["assunto"] == "[Piloto Bike SP] Alteração na remuneração"
	mensagem = notificacao["body"]
	assert nome in mensagem
	assert str(data_inicio.strftime("%d/%m/%Y")) in mensagem
	assert remuneracao in mensagem
	assert str(datetime(2021,3,2).strftime("%d/%m/%Y")) in mensagem

	data_fim = datetime(2024,12,4)
	notificacao = notifica_pessoa(nome,email,data_inicio,remuneracao,data_fim=data_fim,debug=True)
	mensagem = notificacao["body"]
	assert nome in mensagem
	assert str(data_inicio.strftime("%d/%m/%Y")) in mensagem
	assert remuneracao in mensagem
	assert str(data_fim.strftime("%d/%m/%Y")) in mensagem
	return