import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.atualiza_coordenada_localizacao import atualiza_coordenada_localizacao
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.utils import parsers

def test_atualiza_coordenada_localizacao(conexao_bd_testes):
	con = conexao_bd_testes
	
	cur = con.cursor()
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	localizacao_1 = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_1,cur)
	localizacao_2 = {
		"coordenadas": [0,2],
		"endereco":"Rua Rua2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_2,cur)
	
	localizacao_3 = {
		"coordenadas": [0,3],
		"endereco":"Rua Rua3",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_3,cur)

	localizacao_4 = {
		"coordenadas": [0,4],
		"endereco":"Rua Rua4",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao_4,cur)

	par_valido_1 = {
		"idPonto1":1,
		"idPonto2":2,
		"distancia": 12,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_1,cur)

	par_valido_2 = {
		"idPonto1":1,
		"idPonto2":3,
		"distancia": 13,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_2,cur)

	par_valido_3 = {
		"idPonto1":3,
		"idPonto2":4,
		"distancia": 34,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_3,cur)

	par_valido_3 = {
		"idPonto1":5,
		"idPonto2":1,
		"distancia": 34,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido_3,cur)


	assert atualiza_coordenada_localizacao(1,[-23.532,-46.3241],cur,force_prod=False) == "SUCESSO"

	cur.execute("SELECT coordenadas FROM LOCALIZACAO WHERE idLocalizacao = 1;")
	resposta = cur.fetchall()
	assert len(resposta) == 1
	assert parsers.postgres_point_para_lista(resposta[0][0]) == [-23.532,-46.3241]

	cur.execute("SELECT distancia FROM PARVALIDO WHERE idPonto1 = 1 and idPonto2 = 2;")
	assert cur.fetchone()[0] != 12
	cur.execute("SELECT distancia FROM PARVALIDO WHERE idPonto1 = 1 and idPonto2 = 3;")
	assert cur.fetchone()[0] != 13
	cur.execute("SELECT distancia FROM PARVALIDO WHERE idPonto1 = 3 and idPonto2 = 4;")
	assert cur.fetchone()[0] == 34