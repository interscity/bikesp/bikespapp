import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.gerenciamento.bonus.cria_bonus import cria_bonus
#pylint: enable=wrong-import-position


def test_cria_bonus(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	insercao_bonus = {
		"nome":"bonus",
		"valor":1.0,
		"ativo":True,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":"2023-10-10 20:01:20",
		"data_fim":"2025-01-01 20:12:04"
	}

	[sucesso,status,id_novo_bonus] = cria_bonus(insercao_bonus,cur)
	assert sucesso == True
	assert id_novo_bonus == 1
	[sucesso,status,id_novo_bonus] = cria_bonus(insercao_bonus,cur)
	assert sucesso == False
	assert status == "Bônus já existe"
