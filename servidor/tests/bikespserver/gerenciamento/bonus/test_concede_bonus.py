import sys
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.gerenciamento.bonus.cria_bonus import cria_bonus
from servidor.bikespserver.gerenciamento.bonus.concede_bonus import concede_bonus
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_bilhete_unico
#pylint: enable=wrong-import-position


def test_concede_bonus_bonus_inexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	id_novo_bonus = 1
	aux_adiciona_pessoa(cur)

	insercao_pessoa_bonus = {
		"idPessoa":1,
		"idBonus": id_novo_bonus
	}
	[sucesso,status] = concede_bonus(insercao_pessoa_bonus,cur)

	assert sucesso == False
	assert status == "Bônus inexistente"

def test_concede_bonus_pessoa_inexistente(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	id_novo_bonus = aux_cria_bonus(cur)

	insercao_pessoa_bonus = {
		"idPessoa":1,
		"idBonus": id_novo_bonus
	}
	[sucesso,status] = concede_bonus(insercao_pessoa_bonus,cur)

	assert sucesso == False
	assert status == "Pessoa inexistente"


def test_concede_bonus_inativo(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	id_novo_bonus = aux_cria_bonus(cur,ativo=False)
	aux_adiciona_pessoa(cur)

	insercao_pessoa_bonus = {
		"idPessoa":1,
		"idBonus": id_novo_bonus
	}
	[sucesso,status] = concede_bonus(insercao_pessoa_bonus,cur)

	assert sucesso == False
	assert status == "Bônus inativo"

def test_concede_bonus_expirado(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	id_novo_bonus = aux_cria_bonus(cur,dataFim="1960-01-01 00:00:00")
	aux_adiciona_pessoa(cur)
	
	insercao_pessoa_bonus = {
		"idPessoa":1,
		"idBonus": id_novo_bonus
	}
	[sucesso,status] = concede_bonus(insercao_pessoa_bonus,cur)

	assert sucesso == False
	assert status == "Bônus já terminou"

def test_concede_bonus_ok(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()
	valor = 100
	id_novo_bonus = aux_cria_bonus(cur,valor=valor)
	dados_pessoa = aux_adiciona_pessoa(cur)
	aux_adiciona_bilhete_unico(cur,bilhete_unico = dados_pessoa["NumBilheteUnico"])
	
	insercao_pessoa_bonus = {
		"idPessoa":1,
		"idBonus": id_novo_bonus,
		"dataConcessao":datetime(2023, 10, 10, 20, 0)
	}
	[sucesso,status] = concede_bonus(insercao_pessoa_bonus,cur)

	cur.execute("SELECT * FROM PESSOABONUS")
	resultado = cur.fetchall()
	assert len(resultado) == 1
	assert resultado[0][0] == insercao_pessoa_bonus["idPessoa"]
	assert resultado[0][1] == insercao_pessoa_bonus["idBonus"]
	assert resultado[0][2] == insercao_pessoa_bonus["dataConcessao"]


	cur.execute("SELECT * FROM BILHETEUNICO;")
	resultado = cur.fetchall()
	assert resultado[0][0] == dados_pessoa["NumBilheteUnico"]
	assert resultado[0][6] == "R$ 100,00"



def aux_cria_bonus(cur,ativo=True,dataFim="3025-01-01 20:12:04",valor=10):
	insercao_bonus = {
		"nome":"bonus",
		"valor":valor,
		"ativo":ativo,
		"visivel":True,
		"descricao":"ESSE É O BONUS",
		"data_inicio":"2023-10-10 20:01:20",
		"data_fim":dataFim
	}
	[_,_,id_novo_bonus] = cria_bonus(insercao_bonus,cur)

	return id_novo_bonus