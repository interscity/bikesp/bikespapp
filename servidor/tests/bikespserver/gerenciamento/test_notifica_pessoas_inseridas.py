import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.notifica_pessoas_inseridas import notifica_pessoa

def test_notifica_pessoas_inseridas():
	envio = notifica_pessoa("email",debug=True)	
	assert envio["recebedor"] == ["email"]
	assert envio["assunto"] == "[Bike SP] Acesso ao aplicativo liberado"