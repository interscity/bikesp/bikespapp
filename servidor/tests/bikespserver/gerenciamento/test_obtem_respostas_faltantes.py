import sys
from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.gerenciamento.obtem_respostas_faltantes import obtem_respostas_faltantes
from servidor.bikespserver.bd.insercoes import insere_PESSOA

from servidor.tests.utils.pessoa import aux_adiciona_pessoa, escolhe_nome_aleatorio, escolhe_documento_aleatorio, escolhe_email_aleatorio

def test_obtem_respostas_faltantes(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	cpf_1 = "2304925902590"
	pessoa_info_1 = aux_forma_dados(cpf_1)
	respostas_faltantes = obtem_respostas_faltantes([{"CPF":pessoa_info_1["CPF"]}],con)
	assert len(respostas_faltantes) == 1

	insere_PESSOA(pessoa_info_1, cur)
	con.commit()

	respostas_faltantes = obtem_respostas_faltantes([{"CPF":pessoa_info_1["CPF"]}],con)
	assert len(respostas_faltantes) == 0
	
	cpf_2 = "2412412421"
	pessoa_info_2 = aux_forma_dados(cpf_2)
	respostas_faltantes = obtem_respostas_faltantes([{"CPF":pessoa_info_2["CPF"]},{"CPF":pessoa_info_1["CPF"]}],con)
	assert len(respostas_faltantes) == 1

	insere_PESSOA(pessoa_info_2, cur)
	con.commit()

	respostas_faltantes = obtem_respostas_faltantes([{"CPF":pessoa_info_2["CPF"]},{"CPF":pessoa_info_1["CPF"]}],con)
	assert len(respostas_faltantes) == 0

def aux_forma_dados(cpf):
	dados = {
		"Nome": escolhe_nome_aleatorio(10),
		"DataNascimento": "01/01/2000",
		"Genero": "",
		"Raca": "",
		"RG": escolhe_documento_aleatorio(10),
		"CPF": cpf,
		"Email": escolhe_email_aleatorio(10),
		"Telefone": "",
		"Rua": "",
		"Numero": "",
		"Complemento": "",
		"Bairro": "",
		"CEP": "",
		"Cidade": "",
		"Estado": "",
		"NumBilheteUnico": escolhe_documento_aleatorio(10)
	}
	return dados