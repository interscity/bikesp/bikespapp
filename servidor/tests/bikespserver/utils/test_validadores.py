import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.utils import validadores


def test_valida_tamanho_campos_form():
	assert validadores.valida_tamanhos_campos_form({"teste":"teste"},1000) == True
	assert validadores.valida_tamanhos_campos_form({"teste":"teste"},1) == False
	assert validadores.valida_tamanhos_campos_form({"teste":"teste"},5) == True

	campo = "a" * 1000
	assert validadores.valida_tamanhos_campos_form({"teste":campo},0) == False
	assert validadores.valida_tamanhos_campos_form({"teste":campo},999) == False
	assert validadores.valida_tamanhos_campos_form({"teste":campo},1000) == True
	assert validadores.valida_tamanhos_campos_form({"teste":campo},5000) == True
	campo2 = "a" * 10000
	assert validadores.valida_tamanhos_campos_form({"teste":campo2},10000) == True
	assert validadores.valida_tamanhos_campos_form({"teste":campo2},9000) == False
	assert validadores.valida_tamanhos_campos_form({"teste":{"c1":campo2}},9000) == False


def test_valida_chaves_form():
	assert validadores.valida_chaves_form({"teste":""},["teste"]) == True
	assert validadores.valida_chaves_form({"teste":""},["teste","nada"]) == False
	assert validadores.valida_chaves_form({"teste":"","blabla":""},["teste","nada"]) == False
	assert validadores.valida_chaves_form({"teste":"","blabla":""},["teste","blabla"]) == True
	assert validadores.valida_chaves_form({"teste":"","blabla":""},["blabla","teste"]) == True
	assert validadores.valida_chaves_form({"teste":"","blabla":""},[]) == True
	assert validadores.valida_chaves_form({},[]) == True
	assert validadores.valida_chaves_form({},["teste"]) == False

def test_valida_integridade_request():
	chave = "chave"
	dict_1 = {
		"testa":"ola",
		"ola":"testa"
	}
	assert validadores.valida_integridade_request(dict_1,chave) == False
	dict_1["integridade"] = "ola"
	assert validadores.valida_integridade_request(dict_1,chave) == False
	dict_1["integridade"] = "232f15ee7437211eb928438a2f73f72c"
	assert validadores.valida_integridade_request(dict_1,chave) == True