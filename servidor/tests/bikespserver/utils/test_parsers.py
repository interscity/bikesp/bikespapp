import sys
import math
from pathlib import Path
import pytest

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.utils import parsers

def test_remove_pontuacao():
	assert parsers.remove_pontuacao("olá, bom dia tudo bem com você?") == "olá bom dia tudo bem com você"
	assert parsers.remove_pontuacao("[0,0,1(0),2]") == "00102"
	assert parsers.remove_pontuacao("      oi      ") == "oi"

def test_remove_acentuacao():
	assert parsers.remove_acentuacao("olá, bom dia tudo bem com você?") == "ola, bom dia tudo bem com voce?"
	assert parsers.remove_acentuacao("ù ì ê õ ã") == "u i e o a"

def test_remove_quebras():
	assert parsers.remove_quebras("\n\n\n\nola\n\n\nola\t\toi") == "    ola   olaoi"

def test_troca_ponto_virgula():
	assert parsers.troca_ponto_virgula("1,000.34") == "1.000,34"
	assert parsers.troca_ponto_virgula(",...,,..") == ".,,,..,,"

def test_parse_data():
	assert parsers.parse_data("2022-10-10") == "2022-10-10"

def test_parse_genero():
	assert parsers.parse_genero("F") == "F"
	assert parsers.parse_genero("Feminino") == "F"
	assert parsers.parse_genero("FEMININO") == "F"
	assert parsers.parse_genero("Masculino") == "M"
	assert parsers.parse_genero("Homem") == "M"
	assert parsers.parse_genero("Mulher") == "F"
	assert parsers.parse_genero("M") == "M"
	assert parsers.parse_genero("nao binario") == "NB"
	assert parsers.parse_genero("outro") == "NA"


def test_parse_bool():
	assert parsers.parse_bool("Sim") == True
	assert parsers.parse_bool("Não") == False
	assert parsers.parse_bool("True") == True
	assert parsers.parse_bool(True) == True
	assert parsers.parse_bool(False) == False

def test_parse_coordenadas():
	assert parsers.parse_coordenadas([0,0]) == "0,0"
	assert parsers.parse_coordenadas({"latitude":0,"longitude":1}) == "0,1"
	assert parsers.parse_coordenadas({"lat":0,"longitude":1}) == "0,1"
	assert parsers.parse_coordenadas({"lat":0,"lng":1}) == "0,1"
	assert parsers.parse_coordenadas({"lat":0,"long":1}) == "0,1"
	assert parsers.parse_coordenadas("[0,10]") == "0,10"
	assert parsers.parse_coordenadas("(12,80)") == "12,80"

def test_parse_endereco():
	assert parsers.parse_endereco("rua do matão",1010) == "rua do matão|1010|||||"
	assert parsers.parse_endereco("rua do matão",1010,estado="São Paulo") == "rua do matão|1010|||||SP"
	assert parsers.parse_endereco("rua do matão",1010,bairro="Butantã",estado="Sao Paulo") == "rua do matão|1010||Butantã|||SP"

def test_parse_integer():
	assert parsers.parse_integer(1) == "1"
	assert parsers.parse_integer("1") == "1"
	assert parsers.parse_integer(1.0) == "1"
	assert parsers.parse_integer("1.0") == "1"

def test_parse_range():
	assert parsers.parse_range("de 0 a 1",tipo="integer") == "[0,1)" 
	assert parsers.parse_range("de R$0,00 a R$1,00",tipo="money") == "[0.00,1.00)"
	assert parsers.parse_range("Acima de R$ 10.560",tipo="money") == "[10560,)"
	assert parsers.parse_range("21 ou mais",tipo="integer") == "[21,)"

def test_parse_dinheiro_float():
	assert parsers.parse_dinheiro_float("R$ 1,00") == 1.0
	assert parsers.parse_dinheiro_float("- R$ 1,00") == -1.0
	assert parsers.parse_dinheiro_float("R$1,00") == 1.0
	assert parsers.parse_dinheiro_float("R$1,00") == 1.0
	assert parsers.parse_dinheiro_float("R$ 0,00") == 0.0
	assert parsers.parse_dinheiro_float("-R$ 0,00") == 0.0
	assert parsers.parse_dinheiro_float("R$ 4,24") == 4.24
	assert parsers.parse_dinheiro_float("-R$ 4,24") == -4.24
	assert parsers.parse_dinheiro_float("R$ 1234,50") == 1234.5
	assert parsers.parse_dinheiro_float("R$ 12.340,00") == 12340
	assert parsers.parse_dinheiro_float("-R$ 12.340,00") == -12340

	with pytest.raises(TypeError):
		parsers.parse_dinheiro_float(12)
	with pytest.raises(TypeError):
		parsers.parse_dinheiro_float(12.23)
	with pytest.raises(TypeError):
		parsers.parse_dinheiro_float("12.23")



def test_apenas_digitos():
	assert parsers.apenas_digitos("abc123") == "123"
	assert parsers.apenas_digitos("a1b2c3.,d6") == "1236"
	assert parsers.apenas_digitos("") == ""
	assert parsers.apenas_digitos("       2") == "2"
	assert parsers.apenas_digitos("\n\t\n2   \n\t   ") == "2"

def test_parser_documento():
	assert parsers.parse_documento("123456") == "123456"
	assert parsers.parse_documento("000000000000012") == "000000000000012"
	assert parsers.parse_documento("123456") == "123456"
	assert parsers.parse_documento("12   3456   ") == "123456"
	assert parsers.parse_documento("     1\t23456\n\n\n    ") == "123456"
	assert parsers.parse_documento("1.2.3.4.5.6...") == "123456"
	assert parsers.parse_documento("1.2.3.4.5.6-50") == "12345650"
	assert parsers.parse_documento("3333333X") == "3333333"
	assert parsers.parse_documento("3333333Xácasdfa") == "3333333"
	assert parsers.parse_documento("0123456") == "0123456"
def test_parser_bilhete_unico():
	assert parsers.parse_bilhete_unico("123456") == "123456"
	assert parsers.parse_bilhete_unico("1.2.3.4.5.6...") == "123456"
	assert parsers.parse_bilhete_unico("1.2.3.4.5.6-50") == "12345650"
	assert parsers.parse_bilhete_unico("3333333X") == "3333333"
	assert parsers.parse_bilhete_unico("3333333Xácasdfa") == "3333333"
	assert parsers.parse_bilhete_unico("0123456") == "0123456"
	assert parsers.parse_bilhete_unico("  \n   0  \t   123456      ") == "0123456"

def test_postgres_point_para_lista():
	assert parsers.postgres_point_para_lista("(0,0)") == [0,0]
	assert parsers.postgres_point_para_lista("(0.42,0.67)") == [0.42,0.67]
	assert parsers.postgres_point_para_lista("(24352,-104124.2423523)") == [24352,-104124.2423523]
	assert parsers.postgres_point_para_lista("(-0,4320)") == [0,4320]
	assert parsers.postgres_point_para_lista("(10,-0)") == [10,0]

def test_email():
	assert parsers.parse_email("ABCDE@email.com") == "abcde@email.com"
	assert parsers.parse_email("ABCDEABCDEABCDEABCDE") == "abcdeabcdeabcdeabcde"
	assert parsers.parse_email("ABCDEABCDEABCDEABCDE") == "abcdeabcdeabcdeabcde"
	assert parsers.parse_email("ABCDE@email.com        ") == "abcde@email.com"
	assert parsers.parse_email("      ABCDE@email.com        ") == "abcde@email.com"
	assert parsers.parse_email("ABCDE @email.com        ") == "abcde@email.com"
	assert parsers.parse_email("\t\tABCDE@email.com  \n    \t  ") == "abcde@email.com"
