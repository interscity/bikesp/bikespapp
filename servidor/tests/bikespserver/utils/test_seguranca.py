import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.utils import seguranca

def test_encripta():
	assert seguranca.encripta("senha","af32da2879cdf8a3") == "11e8hWmZrwGu5nLa0GOeMQ=="
	assert seguranca.encripta("admin","af32da2879cdf8a3") == "Ix5+j0f/Tnpn009iMI6/FQ=="

def test_md5():
	assert seguranca.hash_MD5("hash").hexdigest() == "0800fc577294c34e0b28ad2839435945"
	assert seguranca.hash_MD5("senha").hexdigest() == "e8d95a51f3af4a3b134bf6bb680a213a"
