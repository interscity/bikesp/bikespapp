import sys
import math
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.bikespserver.utils import coords
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas

from servidor.tests.utils.mocks import resposta_mock_pessoa


def test_coordenada_valida():
	assert coords.coordenada_valida([0,0]) == False
	assert coords.coordenada_valida([-1,-1]) == False
	assert coords.coordenada_valida([-23.5504533, -46.6339112]) == True
	assert coords.coordenada_valida([-23.5504, -46.6339]) == True
	assert coords.coordenada_valida([-23, -46]) == False
	assert coords.coordenada_valida([-23.47, -46.57]) == True
	assert coords.coordenada_valida([-23.56, -46.73]) == True

def test_converte_ponto():
	assert coords.converte_ponto([0,0]) == tuple([0,0])
	assert coords.converte_ponto([0.5,-100]) == tuple([0.5,-100])
	assert coords.converte_ponto({"latitude":0,"longitude":0}) == tuple([0,0])
	assert coords.converte_ponto({"latitude":123,"longitude":23.234}) == tuple([123,23.234])
	assert coords.converte_ponto("0,0") == tuple([0,0])
	assert coords.converte_ponto("-58.234,-100") == tuple([-58.234,-100])

def test_converte_p1_p2():
	assert coords.converte_p1_p2([0,0],[0,0]) == tuple([0,0,0,0])
	assert coords.converte_p1_p2([0,1],[1,0]) == tuple([0,1,1,0])
	assert coords.converte_p1_p2([-23.5504533, -46.6339112],[-23.5504, -46.6339]) == tuple([-23.5504533, -46.6339112,-23.5504, -46.6339])
	assert coords.converte_p1_p2({"latitude":0,"longitude":0},{"latitude":0,"longitude":0}) == tuple([0,0,0,0])
	assert coords.converte_p1_p2({"latitude":1,"longitude":0},{"latitude":0,"longitude":1}) == tuple([1,0,0,1])
	assert coords.converte_p1_p2([0,0],{"latitude":0,"longitude":1}) == tuple([0,0,0,1])

def test_distancia_haversine():
	assert coords.distancia_haversine([0,0],[0,0]) == 0
	assert math.floor(coords.distancia_haversine([0,0],[-23,-46])) == 5776346
	assert math.floor(coords.distancia_haversine([0,0],{"latitude":-23,"longitude":-46})) == 5776346

def test_ponto_proximo_localizacao_usuario(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	localizacao = {
		"coordenadas": [0,1],
		"endereco":"Rua Rua",
		"tipoLocalizacao":"qualquer"
	}
	estacao = {
		"coordenadas": [0,0],
		"endereco":"Estacao Estacao",
		"tipoLocalizacao":"estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao,cur)
	cur = insercoes.insere_LOCALIZACAO(estacao,cur)
	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	todos_pontos = consultas.get_todas_localizacoes_pessoa(1,cur)
	assert coords.ponto_proximo_localizacao_usuario([0,0],todos_pontos) is None
	apelido_localizacao = {
		"idPessoa":1,
		"idLocalizacao":1,
		"apelido": "apelido"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)
	pessoa_estacao = {
		"idPessoa":1,
		"idLocalizacao":2
	}
	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao,cur)

	todos_pontos = consultas.get_todas_localizacoes_pessoa(1,cur)
	assert coords.ponto_proximo_localizacao_usuario([0,0],todos_pontos) == [2,0.0]
	assert coords.ponto_proximo_localizacao_usuario([0,1],todos_pontos) == [1,0.0]
	assert coords.ponto_proximo_localizacao_usuario([0,2],todos_pontos) is None
	cur.close()



# def test_get_coordenada_estacao():
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("PALMEIRAS-BARRA FUNDA"),[-23.5254616, -46.6675134]) < 200
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("SÉ"),[-23.5503897, -46.63308095]) < 200
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("BUTANTÃ"),[-23.57191265, -46.70816848680768]) < 200
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("PINHEIROS"),[-23.56736,-46.70203,]) < 200
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("SÃO MIGUEL PAULISTA"),[-23.4905676,-46.443702]) < 200
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("EUCALIPTOS"),[-23.6096505,-46.6683913]) < 200
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("PAULISTA"),[-23.5552763,-46.6620388]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("CONSOLAÇÃO"),[-23.5577619,-46.6606095]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("SACOMÃ"),[-23.6016017,-46.6030078]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("Term. Penha"),[-23.5190122,-46.5474851]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("Term. Pinheiros"),[-23.5666386,-46.7031565]) < 150
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("Terminal Rodoviario do Tietê"),[-23.5162903,-46.6214822]) < 250
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("AYRTON SENNA-JARDIM SÃO PAULO"),[-23.4922818,-46.6192384]) < 250
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("Term. Sacomã"),[-23.6026717,-46.6040157]) < 150
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("Terminal Rodoviário Intermodal Barra Funda"),[-23.5254616, -46.6675134]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("GUILHERMINA-ESPERANÇA"),[-23.5293046, -46.5166401]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("MARECHAL DEODORO"),[-23.5339809, -46.6558993]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("VILA MADALENA"),[-23.5464956, -46.6911243]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("SÃO BENTO"),[-23.5440566, -46.6342733]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("REPÚBLICA"),[-23.5440945, -46.642665]) < 100
	# assert coords.distancia_haversine(coords.get_coordenada_estacao("ANHANGABAÚ"),[-23.547819, -46.6392744]) < 100



############### Consome API TomTom  ###############	

# def test_distancia_rota():
# 	rota = coords.get_rota_info([-23.5254616, -46.6675134],[-23.57191265, -46.70816848680768])
# 	assert  rota["distancia"] > 9000

# def test_get_coordenada_endereco():
	# endereco1 = "rua do matão|1010"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco1),[-23.559086200000003, -46.73169324321974]) < 200
	# endereco2 = "rua do matão|1010||Butantã|05508-090|são paulo|são paulo"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco2),[-23.559086200000003, -46.73169324321974]) < 170
	# endereco3 = "rua havai|338||Sumare|01259-000|são paulo|são paulo"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco3),[-23.54198, -46.68607]) < 200
	# endereco4 = "avenida paulista|2439||Bela Vista|01311-300|são paulo|são paulo"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco4),[-23.55645,	-46.66254]) < 100
	# endereco5 = "Rua São Bento|470|||01010001|São Paulo|São Paulo"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco5),[-23.54533,	-46.63459]) < 100
	# endereco6 = "Avenida Professor Almeida Prado|83.0||Butantã|05508-070||"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco6),[-23.5544917,-46.7328547]) < 250
	# endereco7 = "Avenida Mauro Marques da Silva|271||Vila Antônio|5376030|São Paulo|SP"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco7),[-23.58001, -46.76716]) < 100


	# endereco8 = "Avenida Paulista|610||Bela Vista|01311-100|São Paulo|SP"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco8),[-23.5672906,-46.6489551]) < 50

	# endereco9 = "Rua Patápio Silva|248||Jardim das Bandeiras|05436-010|São Paulo|SP"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco9),[-23.5541578,-46.6875308]) < 300
	
	# endereco10 = "Avenida Paulista|2439.0||Bela Vista|1311300|São Paulo|SP"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco10),[-23.5569216,-46.6644347]) < 250

	# endereco11 = "Praça Charles Miller|0||Pacaembu|01234010 |São Paulo|SP"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco11),[ -23.5453864,-46.6670931]) < 150
	# endereco12 = "Praça Charles Miller|s/n||Pacaembu|01234010 |São Paulo|SP"
	# assert coords.distancia_haversine(coords.get_coordenada_endereco(endereco12),[ -23.5453864,-46.6670931]) < 150
	# 