import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.viagem.validaviagem.valida_por_activities import valida_por_activities
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MIN_ACITVITIES_KM
from servidor.bikespserver.viagem.validaviagem.consts import PCNTG_ACTIVITIES_APROVACAO
from servidor.bikespserver.viagem.validaviagem.consts import PCNTG_ACTIVITIES_REPROVACAO

from servidor.bikespserver.classes.viagem import Viagem

from servidor.tests.utils.activites import ActivitiesBuilder

#pylint: enable=wrong-import-position

def test_invalida_none():
	assert not valida_por_activities(aux_cria_viagem_vazia())

def test_invalida_pequena():
	tam_km = 3

	builder_activities = ActivitiesBuilder(NUM_MIN_ACITVITIES_KM * tam_km -1)

	viagem = aux_cria_viagem_vazia(tam_km * 1000)
	viagem.activity_recognition_trip = builder_activities.gera_lista_medicoes()

	assert not valida_por_activities(viagem)

def test_ignora_amostras_sem_confianca():
	tam_km = 3

	builder_activities = ActivitiesBuilder(NUM_MIN_ACITVITIES_KM * tam_km + 2)

	viagem = aux_cria_viagem_vazia(tam_km * 1000)
	viagem.activity_recognition_trip = builder_activities.gera_lista_medicoes()
	viagem.activity_recognition_trip[0]["confidence"] = 10
	viagem.activity_recognition_trip[1]["confidence"] = 10
	viagem.activity_recognition_trip[2]["confidence"] = 10

	assert not valida_por_activities(viagem)


def test_aprova_bicicleta():

	tam_km = 4
	num_pontos = NUM_MIN_ACITVITIES_KM * tam_km

	builder_activities = ActivitiesBuilder(num_pontos)
	builder_activities.set_subtrajeto_bike(num_pontos*PCNTG_ACTIVITIES_APROVACAO + 2)
	builder_activities.set_subtrajeto_parado(num_pontos*0.3)

	viagem = aux_cria_viagem_vazia(tam_km * 1000)
	viagem.activity_recognition_trip = builder_activities.gera_lista_medicoes()

	assert valida_por_activities(viagem)
	assert viagem.status == "Aprovado"


def test_reprova_veiculo():

	tam_km = 4
	num_pontos = NUM_MIN_ACITVITIES_KM * tam_km

	builder_activities = ActivitiesBuilder(num_pontos)
	builder_activities.set_subtrajeto_veiculo(num_pontos*PCNTG_ACTIVITIES_REPROVACAO)
	builder_activities.set_subtrajeto_bike(num_pontos*(1-PCNTG_ACTIVITIES_APROVACAO))

	viagem = aux_cria_viagem_vazia(tam_km * 1000)
	viagem.activity_recognition_trip = builder_activities.gera_lista_medicoes()

	assert valida_por_activities(viagem)
	assert viagem.motivo_reprovacao == "NAO_BICICLETA"
	assert viagem.status == "Reprovado"


def aux_cria_viagem_vazia(deslocamento=1000):
	"""
		Cria uma viagem vazia com um deslocamento passado
	"""
	viagem = Viagem()
	viagem.deslocamento = deslocamento
	return viagem

# [{"date": "Fri Dec 15 08:45:41 GMT-03:00 2023", "type": "PARADO", "confidence": 100}]