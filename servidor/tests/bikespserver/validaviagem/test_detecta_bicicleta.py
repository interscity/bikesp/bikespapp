import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.viagem.validaviagem.detecta_bicicleta import detecta_bicicleta
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MIN_AMOSTRAS_TRAJETO

from servidor.bikespserver.classes.viagem import Viagem

from servidor.tests.utils.trajeto import cria_trajeto

#pylint: enable=wrong-import-position


def test_invalida_vel_baixa():
	velocidades = [1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,3,1,1,1,1,1]
	velocidades = velocidades + (NUM_MIN_AMOSTRAS_TRAJETO - len(velocidades)) * [1]
	delta_ts = [5]*max(NUM_MIN_AMOSTRAS_TRAJETO,len(velocidades))
	assert not detecta_bicicleta(cria_trajeto(velocidades,delta_ts))

def test_invalida_vel_alta():
	velocidades = [8,8,8,13,8,8,13,12,13,13,8,8,8,8,8,7,2]
	velocidades = velocidades + (NUM_MIN_AMOSTRAS_TRAJETO - len(velocidades)) * [8]
	delta_ts = [5]*max(NUM_MIN_AMOSTRAS_TRAJETO,len(velocidades))
	assert not detecta_bicicleta(cria_trajeto(velocidades,delta_ts))

def test_invalida_veloz_baixa_amostragem():
	velocidades = [8]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [15]
	delta_ts = [5]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [50]
	assert not detecta_bicicleta(cria_trajeto(velocidades,delta_ts))

def test_valida_ultimo_trecho_impreciso():
	velocidades = [5]*(NUM_MIN_AMOSTRAS_TRAJETO)
	delta_ts = [5]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [500]
	precisoes = [10]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [500]
	assert detecta_bicicleta(cria_trajeto(velocidades,delta_ts,precisoes=precisoes))

def test_invalida_ultimo_trecho_impreciso():
	velocidades = [5]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [20]
	delta_ts = [5]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [5000]
	precisoes = [10]*(NUM_MIN_AMOSTRAS_TRAJETO-1) + [500]
	assert not detecta_bicicleta(cria_trajeto(velocidades,delta_ts,precisoes=precisoes))


def test_valida_bicicleta():
	assert detecta_bicicleta(cria_trajeto([8]*NUM_MIN_AMOSTRAS_TRAJETO,[5]*NUM_MIN_AMOSTRAS_TRAJETO))