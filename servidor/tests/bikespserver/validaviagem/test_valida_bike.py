import sys
import re
import json
from pathlib import Path
from datetime import datetime, timedelta

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd.consultas import get_viagens_pessoa

from servidor.bikespserver.viagem.validaviagem.consts import NUM_MIN_AMOSTRAS_TRAJETO
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MAX_VIAGENS_REMUNERADAS_DIA
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MAX_VIAGENS_REMUNERADAS_MES
from servidor.bikespserver.viagem.validaviagem.consts import TEMPO_MAX_SEM_AMOSTRA
from servidor.bikespserver.viagem.validaviagem.consts import DELTAT_AMOSTRA_MUITO_ANTIGA
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MAX_PAUSAS
from servidor.bikespserver.viagem.validaviagem.valida_viagem import valida_viagem_bike
from servidor.bikespserver.viagem.validaviagem.detecta_bicicleta import cria_deslcoamentos_atomicos

from servidor.bikespserver.classes.viagem import Viagem

from servidor.tests.utils.trajeto import cria_trajeto_str, cria_trajeto
from servidor.tests.utils.pessoa import aux_atribui_grupo_pesquisa, aux_adiciona_pessoa, aux_adiciona_locs_user
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes, aux_adiciona_par_valido
#pylint: enable=wrong-import-position

def aux_req_json_para_viagem_recebida(entrada):
	return Viagem.from_json(json.loads(entrada["viagem"]))

mockViagemTimestamps = {
	"viagem" : re.sub("[\n\t ]+", "",
"""{
	"dataInicio": "2023-08-23  17:27:59 GMT-03:00",
	"duracao": 51,
	"origem": "1",
	"destino": "2",
	"trajeto": [
		{
			"Data" : "2023-08-23  17:27:59 GMT-03:00",
			"Posicao" : {
				"latitude" : -45.12312,
				"longitude" : -48.0012
			},
			"Precisao" : 15.231,
			"Velocidade" : 0.01
		},
		{
			"Data" : "2023-08-23  17:37:59 GMT-03:00",
			"Posicao" : {
				"latitude" : -45.124,
				"longitude" : -48.010
			},
			"Precisao" : 19.531,
			"Velocidade" : 15.01
		}
	]
}""")
}

mockViagemTimestampsRecebida = aux_req_json_para_viagem_recebida(mockViagemTimestamps)

viagem_ponto_inicial = [-45.12312,-48.0013]
viagem_ponto_final = cria_trajeto([8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
		[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial)[-1]["Posicao"]

mockEnvioViagem = {
	"user": "678686",
	"token" : "tokencorreto",
	"viagem" : re.sub("[\n\t]+", "",
"""{
	"dataInicio": 1231312312,
	"duracao": 51,
	"pausas" : "[{'Motivo': 'naoInformar', 'DataInicio': '1970-01-02 00:30:44 GMT-03:00', 'DataFim': '1970-01-02 01:30:44 GMT-03:00'}]",
	"origem": "1",
	"destino": "2",
	"trajeto": """ + cria_trajeto_str(
		[8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
		[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial) + """ }""")
}

def test_origem_desconhecida(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([50]*100,[50]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockEnvioViagemOrigemDesc = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[-23,46],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockEnvioViagemOrigemDesc)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'ORIGEM_DESCONHECIDA'
	assert resp['idViagem'] == 1

def test_destino_desconhecido(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([50]*100,[50]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockEnvioViagemDestinoDesc = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[-23,46],[0,0]])
	
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockEnvioViagemDestinoDesc)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'DESTINO_DESCONHECIDO'
	assert resp['idViagem'] == 1

def test_origem_destino_desconhecidos(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	mockEnvioViagemDestino = mockEnvioViagem.copy()
	mockEnvioViagemDestino["trajeto"] = cria_trajeto([0],[0],pos_inicial=viagem_ponto_inicial)

	aux_adiciona_localizacoes(cur,[[0,0],[-23,46]])
	
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockEnvioViagemDestino)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'ORIGEM_DESTINO_DESCONHECIDOS'
	assert resp['idViagem'] == 1

def test_origem_igual_destino(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	mockOrigemIgualDestino = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + cria_trajeto_str([0]*100,[50]*100,pos_inicial=[0,0]) + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[-23,46],[0,0]])

	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockOrigemIgualDestino)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'ORIGEM_IGUAL_DESTINO'
	assert resp['idViagem'] == 1

def test_extremamente_pequena(client, conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto, ultima_pos = cria_trajeto([4]*100,[30]*100,pos_inicial=[0,0], de_ultima_pos=True)

	trajeto_pequeno = [trajeto[0],trajeto[1],trajeto[-2],trajeto[-1]]

	mockOrigemIgualDestino = {
		"user": "testedois2222",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + json.dumps(trajeto_pequeno) + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])

	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=4000)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockOrigemIgualDestino)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'POUCOS_PONTOS'
	assert resp['idViagem'] == 1

def test_poucas_amostras_precisas(client, conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([10]*9,[40]*9,pos_inicial=[0,0],
			precisoes=[500] * 1 + [
				25] * 8, de_ultima_pos=True)

	mockOrigemIgualDestino = {
		"user": "testedois2222",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=4000)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockOrigemIgualDestino)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['motivoStatus'] == 'POUCOS_PONTOS'
	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['idViagem'] == 1

def test_muito_tempo_sem_amostra(client, conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultimo_ponto = cria_trajeto_str([1000]*99 + [10],[5]*99 + [TEMPO_MAX_SEM_AMOSTRA + 1],
			pos_inicial=[0,0], de_ultima_pos=True)

	mockOrigemIgualDestino = {
		"user": "testedois2222",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	aux_adiciona_localizacoes(cur,[[0,0],ultimo_ponto])
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	viagem = aux_req_json_para_viagem_recebida(mockOrigemIgualDestino)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'TEMPO_SEM_AMOSTRA'
	assert resp['idViagem'] == 1

def test_nao_bicicleta(client,conexao_bd_testes):
	con = conexao_bd_testes

	trajeto_str, ultima_pos = cria_trajeto_str([1000]*100,[5]*100,pos_inicial=[0,0],de_ultima_pos=True)

	mockNaoBicicleta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)

	viagem = aux_req_json_para_viagem_recebida(mockNaoBicicleta)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'NAO_BICICLETA'
	assert resp['idViagem'] == 1

def test_trata_fim_atrasado_viagem(client,conexao_bd_testes):
	con = conexao_bd_testes

	trajeto_str, ultima_pos = cria_trajeto_str([20]*50 + [3,-3]*250,[5]*550,pos_inicial=[0,0],de_ultima_pos=True)

	mockNaoBicicleta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	cur = con.cursor()

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)

	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)


	viagem = aux_req_json_para_viagem_recebida(mockNaoBicicleta)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'NAO_BICICLETA'
	assert resp['idViagem'] == 1

def test_salva_viagem(client,conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)

	viagem = aux_req_json_para_viagem_recebida(mockEnvioViagem)
	
	resp = valida_viagem_bike(viagem,1,None,cur)

	con.commit()
	
	cur.execute(
	    """
			SELECT idViagem,metadados
			FROM VIAGEM
			WHERE idPessoa = 1;
		""")
	viagens_encontradas = cur.fetchall()

	assert len(viagens_encontradas) > 0
	assert viagens_encontradas[0][-1] == {'pausas': "[{'Motivo': 'naoInformar', 'DataInicio': '1970-01-02 00:30:44 GMT-03:00', 'DataFim': '1970-01-02 01:30:44 GMT-03:00'}]"}

	cur.close()
	con.close()

def test_viagem_pausas_alteradas(client,conexao_bd_testes):
	con = conexao_bd_testes

	cur = con.cursor()
	aux_adiciona_localizacoes(cur,[viagem_ponto_inicial,viagem_ponto_final])
	aux_adiciona_pessoa(cur)
	aux_adiciona_locs_user(cur)
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)

	mockEnvioViagemMuitasPausas = {
	"user": "678686",
	"token" : "tokencorreto",
	"viagem" : re.sub("[\n\t]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "trabalho",
		"destino": "destino",
		"pausas" : "[""" + 
		(NUM_MAX_PAUSAS)*"{'Motivo': 'naoInformar', 'DataInicio': '1970-01-02 00:30:44 GMT-03:00', 'DataFim': '1970-01-02 01:30:44 GMT-03:00'}," + """{'Motivo': 'naoInformar', 'DataInicio': '1970-01-02 00:30:44 GMT-03:00', 'DataFim': '1970-01-02 01:30:44 GMT-03:00'}]",
		"trajeto": """ + cria_trajeto_str(
			[8]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
			[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),viagem_ponto_inicial) + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockEnvioViagemMuitasPausas)
	
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'VIAGEM_INVALIDA'

	cur.close()
	con.close()

def test_limite_excedido_com_remuneracao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=1)
	
	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockRemuneracaoCorreta)

	for _ in range(NUM_MAX_VIAGENS_REMUNERADAS_DIA):
		resp = valida_viagem_bike(viagem,1,1,cur)
		viagem.id_viagem = None
		assert resp['estadoViagem'] == 'Aprovado'
		# con.commit()

	viagem.id_viagem = None
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'LIMITE_VIAGENS_EXCEDIDO'
	assert resp['idViagem'] == 1

def test_limite_mensal_excedido_com_remuneracao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=1)
	
	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": """+ str(datetime.fromisoformat("1970-01-01T00:00:01").timestamp()) +""",
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockRemuneracaoCorreta)

	for _ in range(NUM_MAX_VIAGENS_REMUNERADAS_MES):
		resp = valida_viagem_bike(viagem,1,1,cur)
		viagem.id_viagem = None
		assert resp['estadoViagem'] == 'Aprovado'
		# con.commit()
		viagem.data_inicio += timedelta(hours=24//NUM_MAX_VIAGENS_REMUNERADAS_DIA).total_seconds()

	viagem.id_viagem = None
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'LIMITE_VIAGENS_EXCEDIDO'
	assert resp['idViagem'] == NUM_MAX_VIAGENS_REMUNERADAS_MES + 1

def test_limite_excedido_sem_remuneracao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)


	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)
	
	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockRemuneracaoCorreta)
	for _ in range(NUM_MAX_VIAGENS_REMUNERADAS_DIA):
		resp = valida_viagem_bike(viagem,1,None,cur)
		viagem.id_viagem = None
		assert resp['estadoViagem'] == 'Aprovado'
		con.commit()
	
	viagem.id_viagem = None
	resp = valida_viagem_bike(viagem,1,None,cur)
	assert resp['estadoViagem'] == 'Aprovado'

def test_limite_mensal_excedido_sem_remuneracao(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)


	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)
	
	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": """+ str(datetime.fromisoformat("1970-01-01T00:00:01").timestamp()) +""",
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockRemuneracaoCorreta)
	for _ in range(NUM_MAX_VIAGENS_REMUNERADAS_MES):
		resp = valida_viagem_bike(viagem,1,None,cur)
		viagem.id_viagem = None
		assert resp['estadoViagem'] == 'Aprovado'
		con.commit()
		viagem.data_inicio += timedelta(hours=24//NUM_MAX_VIAGENS_REMUNERADAS_DIA).total_seconds()
	
	viagem.id_viagem = None
	resp = valida_viagem_bike(viagem,1,None,cur)
	assert resp['estadoViagem'] == 'Aprovado'


def test_remuneracao_correta(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	
	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockRemuneracaoCorreta)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Aprovado'
	assert resp['remuneracao'] == 0.0
	assert resp['motivoStatus'] == 'APROVADO'
	assert resp['idViagem'] == 1

	aux_atribui_grupo_pesquisa(cur,remuneracao=1.0,id_pessoa=1,id_grupo=4)
	con.commit()

	resp = valida_viagem_bike(viagem,1,4,cur)
	
	assert resp['remuneracao'] == 1

	aux_atribui_grupo_pesquisa(cur,remuneracao=100.0,id_pessoa=1,id_grupo=4)

	resp = valida_viagem_bike(viagem,1,4,cur)

	assert resp['remuneracao'] == 100

def test_reprova_emulador(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([6]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	
	mockRemuneracaoCorreta = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "trabalho",
		"destino": "destino",
		"metadados" : {"emu" : true},
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockRemuneracaoCorreta)

	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['motivoStatus'] == 'VIAGEM_INVALIDA'

def test_aprova_ignorando_amostra_quebrada(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	velocidades = [10]*9
	delta_ts = [40]*9

	velocidades[5] = 0.0001
	velocidades[6] = -0.0001
	delta_ts[5] = -(DELTAT_AMOSTRA_MUITO_ANTIGA*10)
	delta_ts[6] = +(DELTAT_AMOSTRA_MUITO_ANTIGA*10)

	trajeto_str, ultima_pos = cria_trajeto_str(velocidades,delta_ts,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=4000)
	aux_adiciona_locs_user(cur)

	# print(trajeto_str)

	mockAmostraHorrorosa = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockAmostraHorrorosa)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['remuneracao'] == 0.0
	assert resp['motivoStatus'] == 'POUCOS_PONTOS'
	assert resp['idViagem'] == 1

def test_reprova_inicio_piloto(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	trajeto_str, ultima_pos = cria_trajeto_str([4]*100,[30]*100,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	
	mockViagemPrecoce = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 500,
		"duracao": 51,
		"origem": "1",
		"destino": "2",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockViagemPrecoce)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['estadoViagem'] == 'Reprovado'
	assert resp['remuneracao'] == 0.0
	assert resp['motivoStatus'] == 'PILOTO_NAO_INICIADO'
	assert resp['idViagem'] == 1

def test_cria_deslocs_resistente():
	aa = cria_deslcoamentos_atomicos(mockViagemTimestampsRecebida.trajeto)
	assert len(aa) > 0

def test_viagem_com_pausas(client,conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	vels = [4]*100
	ts = [30] * 28 + [3600] + [30]*71

	trajeto_str, ultima_pos = cria_trajeto_str(vels,ts,pos_inicial=[0,0],de_ultima_pos=True)

	aux_adiciona_localizacoes(cur,[[0,0],ultima_pos])
	aux_adiciona_pessoa(cur)
	aux_adiciona_par_valido(cur, distancia=1000)
	aux_adiciona_locs_user(cur)
	
	mockPausas = {
		"user": "678686",
		"token" : "tokencorreto",
		"viagem" : re.sub("[\n\t ]+", "",
	"""{
		"dataInicio": 1231312312,
		"duracao": 51,
		"origem": "trabalho",
		"destino": "destino",
		"pausas" : "[{'Motivo': 'naoInformar', 'DataInicio': '1970-01-01 00:30:44 GMT-03:00', 'DataFim': '1970-01-01 01:30:44 GMT-03:00'},{'Motivo': 'naoInformar', 'DataInicio': '1970-01-01 00:30:45 GMT-03:00', 'DataFim': '1970-01-01 00:30:46 GMT-03:00'}]",
		"trajeto": """ + trajeto_str + """ }""")
	}

	viagem = aux_req_json_para_viagem_recebida(mockPausas)
	resp = valida_viagem_bike(viagem,1,None,cur)

	assert resp['motivoStatus'] == 'APROVADO'
	assert resp['estadoViagem'] == 'Aprovado'
	
