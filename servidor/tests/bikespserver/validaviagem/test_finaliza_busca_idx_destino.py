import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.viagem.validaviagem.encontra_origem_destino import finaliza_busca_idx_destino
from servidor.bikespserver.viagem.validaviagem.consts import TEMPO_MAXIMO_DENTRO_RAIO

#pylint: enable=wrong-import-position


def test_finaliza_busca_idx_destino_tempo_excedido():

	amostra_atual = {'Data': TEMPO_MAXIMO_DENTRO_RAIO, 'Posicao': {'latitude': 0.09936009936009954, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.01}
	amostra_anterior = {'Data': 0, 'Posicao': {'latitude': 0.09936009936009954, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.01}
	assert finaliza_busca_idx_destino(amostra_atual,amostra_anterior,0)[0] == True
	amostra_atual = {'Data': 1, 'Posicao': {'latitude': 0.09936009936009954, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.01}
	amostra_anterior = {'Data': 0, 'Posicao': {'latitude': 0.09936009936009954, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.01}
	assert finaliza_busca_idx_destino(amostra_atual,amostra_anterior,TEMPO_MAXIMO_DENTRO_RAIO-1)[0] == True
	amostra_atual = {'Data': 1, 'Posicao': {'latitude': 0.099399, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.01}
	amostra_anterior = {'Data': 0, 'Posicao': {'latitude': 0.099360, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.01}
	assert finaliza_busca_idx_destino(amostra_atual,amostra_anterior,TEMPO_MAXIMO_DENTRO_RAIO-2)[0] == False
	
def test_finaliza_busca_idx_destino_velocidade_pequena():
	amostra_atual = {'Data': TEMPO_MAXIMO_DENTRO_RAIO-1, 'Posicao': {'latitude': 0.09936009936009954, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.00}
	amostra_anterior = {'Data': 0, 'Posicao': {'latitude': 0.09936009936009954, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.00}
	assert finaliza_busca_idx_destino(amostra_atual,amostra_anterior,0)[0] == True

def test_finaliza_busca_idx_destino_sucesso():
	amostra_atual = {'Data': 1, 'Posicao': {'latitude': 0.099399, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.00}
	amostra_anterior = {'Data': 0, 'Posicao': {'latitude': 0.099360, 'longitude': 0}, 'Precisao': 10, 'Velocidade': 0.00}
	assert finaliza_busca_idx_destino(amostra_atual,amostra_anterior,0)[0] == False