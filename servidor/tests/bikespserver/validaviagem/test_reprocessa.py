import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.viagem.validaviagem import reprocessa
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.tests.utils.mocks import resposta_mock_pessoa
from servidor.tests.utils.trajeto import cria_trajeto
from servidor.tests.utils.pessoa import aux_atribui_grupo_pesquisa

from servidor.bikespserver.utils import parsers
#pylint: enable=wrong-import-position


def test_cria_dict_viagem(conexao_bd_testes):

	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 1698764400 ,
		'duracao': 12*60,
		'origem': 1,
		'destino': 2,
		'trajeto': [{"Data":"2023-10-31 12:00:00-03:00"},{"Data":"2023-10-31 12:12:00 GMT-03:00"}]})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)

	dict_viagem = reprocessa.cria_dict_viagem(1,consultas.dump_viagem(1,cur))

	assert dict_viagem["idViagem"] == 1
	assert dict_viagem["idPessoa"] == 1
	assert dict_viagem["duracao"] == 12 * 60
	assert dict_viagem["deslocamento"] == 1000
	return

def test_reprocessa_viagem(client,conexao_bd_testes,request_context):
	con = conexao_bd_testes

	cur = con.cursor()

	cur = insercoes.insere_PESSOA(resposta_mock_pessoa,cur)
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.10800010800010823,0],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	par_valido = {
		"idPonto1":1,
		"idPonto2":2,
		"distancia": 1000,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido,cur)
	
	con.commit()

	aux_atribui_grupo_pesquisa(cur,1)

	viagem_recebida = Viagem.from_json(
		{'dataInicio': 5000,
		'duracao': 12*60,
		'origem': 1,
		'destino': 2,
		'trajeto': cria_trajeto([4]*100,[30]*100,pos_inicial=[0,0],formato_data="date")})
	

	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",1.0)
	insercoes.insere_VIAGEM(viagem,cur)

	[estado,[antes,depois]] = reprocessa.reprocessa_viagem(1,con)

	assert estado == "SUCESSO"
	assert antes == depois
	con.commit()

	aux_atribui_grupo_pesquisa(cur,10)
	
	[estado,[antes,depois]] = reprocessa.reprocessa_viagem(1,con)
	assert estado == "SUCESSO"
	assert antes != depois
	assert antes["remuneracao"] != depois["remuneracao"]
	assert parsers.parse_dinheiro_float(depois["remuneracao"]) == 10