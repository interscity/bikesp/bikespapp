import sys
import os
import json
import re
import pytest
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

# from servidor.bikespserver.validaviagem.detecta_bicicleta import detecta_bicicleta
from servidor.bikespserver.viagem.validaviagem.valida_viagem import valida_viagem_bike

from servidor.tests.bikespserver.validaviagem.test_valida_bike import aux_req_json_para_viagem_recebida
from servidor.tests.utils.localizacao import aux_adiciona_localizacoes
from servidor.tests.utils.pessoa import aux_adiciona_pessoa, aux_adiciona_locs_user
from servidor.bikespserver.utils.parsers import parse_coordenadas
#pylint: enable=wrong-import-position


diretorio_validas = Path(str(Path(__file__).absolute().parent.parent.parent) + '/mockViagens/viagensAprovaveis')
@pytest.mark.parametrize("nome_mock_validas",os.listdir(diretorio_validas))
def test_detecta_viagens_validas(client, conexao_bd_testes, nome_mock_validas):
	
		con = conexao_bd_testes
		cur = con.cursor()

		nomearq_mock = os.path.join(diretorio_validas, nome_mock_validas)
		arq_mock = open(nomearq_mock)
		print(nomearq_mock)

		trajeto_mock_str = arq_mock.read()
		trajeto_mock = json.loads(trajeto_mock_str)

		p1 = parse_coordenadas(trajeto_mock[0]["Posicao"])
		pn = parse_coordenadas(trajeto_mock[-1]["Posicao"])

		dados_pessoa = aux_adiciona_pessoa(cur)
		aux_adiciona_localizacoes(cur,[p1.split(","),pn.split(",")])
		aux_adiciona_locs_user(cur)
		con.commit()

		mock_viagem_enviada = {
			"user": dados_pessoa["CPF"],
			"token" : "tokencorreto",
			"viagem" : re.sub("[\n\t ]+", "",
		"""{
			"dataInicio": 1231312312,
			"duracao": 51,
			"origem": "1",
			"destino": "2",
			"trajeto": """ + trajeto_mock_str + """ }""")
		}
		viagem = aux_req_json_para_viagem_recebida(mock_viagem_enviada)
		resp = valida_viagem_bike(viagem,1,None,cur)


		assert resp['motivoStatus'] in ['APROVADO','DESLOCAMENTO PEQUENO']
	

diretorio_invalidas = Path(str(Path(__file__).absolute().parent.parent.parent) + '/mockViagens/viagensReprovaveis')
@pytest.mark.parametrize("nome_mock_invalidas",os.listdir(diretorio_invalidas))
def test_detecta_viagens_invalidas(client, conexao_bd_testes, nome_mock_invalidas):
	
		con = conexao_bd_testes
		cur = con.cursor()

		nomearq_mock = os.path.join(diretorio_invalidas, nome_mock_invalidas)
		arq_mock = open(nomearq_mock)
		print(nomearq_mock)

		trajeto_mock_str = arq_mock.read()
		trajeto_mock = json.loads(trajeto_mock_str)

		p1 = parse_coordenadas(trajeto_mock[0]["Posicao"])
		pn = parse_coordenadas(trajeto_mock[-1]["Posicao"])

		dados_pessoa = aux_adiciona_pessoa(cur)
		aux_adiciona_localizacoes(cur,[p1.split(","),pn.split(",")])
		aux_adiciona_locs_user(cur)
		con.commit()

		mock_viagem_enviada = {
			"user": dados_pessoa["CPF"],
			"token" : "tokencorreto",
			"viagem" : re.sub("[\n\t ]+", "",
		"""{
			"dataInicio": 1231312312,
			"duracao": 51,
			"origem": "1",
			"destino": "2",
			"trajeto": """ + trajeto_mock_str + """ }""")
		}

		viagem = aux_req_json_para_viagem_recebida(mock_viagem_enviada)
		resp = valida_viagem_bike(viagem,1,None,cur)

		assert resp['motivoStatus'] in ['POUCOS_PONTOS','NAO_BICICLETA', 'TEMPO_SEM_AMOSTRA']
		assert resp['estadoViagem'] == 'Reprovado'
		assert resp['remuneracao'] == 0.0