import sys,json

from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.contestacao.gerencia_contestacao import gerencia_contestacao
from servidor.bikespserver.contestacao.notifica_contestacao import notifica_contestacao,notifica_pessoa
#pylint: enable=wrong-import-position

def test_notifica_pessoa():
	email = "email"
	nome = "nome"
	resposta_contestacao = "resposta"
	aprovada = True
	id_viagem = 10
	data_viagem = datetime(2023,10,10,9,20,23)
	info_notificacao = notifica_pessoa(email,nome,resposta_contestacao,aprovada,id_viagem,data_viagem,debug=True)

	assert info_notificacao["body"] == "Olá nome,<br><br>Informamos que a sua contestação sobre a viagem <b>#10</b>, do dia 10/10/2023 foi <b>aprovada</b>.<br><br>Resposta da equipe do Bike SP: <br><i>resposta</i><br><br>Atenciosamente,<br>Equipe Piloto Bike SP"
	assert info_notificacao["assunto"] == "[Piloto Bike SP] Contestação aprovada"

	resposta_contestacao = ""
	info_notificacao = notifica_pessoa(email,nome,resposta_contestacao,aprovada,id_viagem,data_viagem,debug=True)
	assert info_notificacao["body"] == "Olá nome,<br><br>Informamos que a sua contestação sobre a viagem <b>#10</b>, do dia 10/10/2023 foi <b>aprovada</b>.<br><br>Atenciosamente,<br>Equipe Piloto Bike SP"
	assert info_notificacao["assunto"] == "[Piloto Bike SP] Contestação aprovada"

	resposta_contestacao = "resposta"
	aprovada = False
	info_notificacao = notifica_pessoa(email,nome,resposta_contestacao,aprovada,id_viagem,data_viagem,debug=True)
	assert info_notificacao["body"] == "Olá nome,<br><br>Informamos que a sua contestação sobre a viagem <b>#10</b>, do dia 10/10/2023 foi <b>reprovada</b>.<br><br>Resposta da equipe do Bike SP: <br><i>resposta</i><br><br>Atenciosamente,<br>Equipe Piloto Bike SP"
	assert info_notificacao["assunto"] == "[Piloto Bike SP] Contestação reprovada"	
