import sys,json

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.contestacao.gerencia_contestacao import gerencia_contestacao
#pylint: enable=wrong-import-position

def test_gerencia_contestacao_aprovar(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	resposta_contestacao = "teste resposta"
	insercoes.insere_GRUPOPESQUISA(dados_grupo_pesquisa,cur)
	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_1,cur)
	aux_insere_viagem(cur)
	insercoes.insere_CONTESTACAO(dados_contestacao,cur)

	assert gerencia_contestacao(10,True,resposta_contestacao,cur) == (False,"CONSTESTAÇÃO INVÁLIDA",0)

	cur.execute("UPDATE PESSOA SET idGrupo = NULL;")
	assert gerencia_contestacao(1,True,resposta_contestacao,cur) == (False,"PESSOA SEM GRUPO PESQUISA",0)


	cur.execute("UPDATE CONTESTACAO SET aprovada = NULL;")
	cur.execute("UPDATE PESSOA SET idGrupo = 1;")
	assert gerencia_contestacao(1,True,resposta_contestacao,cur,valor=12) == (True,"SUCESSO",12)
	cur.execute("SELECT remuneracao FROM VIAGEM WHERE idViagem = 1")
	assert cur.fetchone()[0] == "R$ 12,00"
	cur.execute("SELECT aguardandoEnvio FROM BILHETEUNICO;")
	assert cur.fetchone()[0] == "R$ 12,00"

	cur.execute("UPDATE CONTESTACAO SET aprovada = NULL;")
	cur.execute("UPDATE PESSOA SET idGrupo = 1;")
	assert gerencia_contestacao(1,True,resposta_contestacao,cur) == (True,"SUCESSO",10)
	cur.execute("SELECT remuneracao FROM VIAGEM WHERE idViagem = 1")
	assert cur.fetchone()[0] == "R$ 10,00"
	cur.execute("SELECT aguardandoEnvio FROM BILHETEUNICO;")
	assert cur.fetchone()[0] == "R$ 22,00"


	cur.execute("UPDATE CONTESTACAO SET aprovada = NULL;")
	cur.execute("UPDATE VIAGEM SET (idOrigem,idDestino) = (1,1);")
	assert gerencia_contestacao(1,True,resposta_contestacao,cur) == (False,"VALOR FALTANDO PARA VIAGEM COM ORIGEM == DESTINO",0)

	cur.execute("UPDATE CONTESTACAO SET aprovada = NULL;")
	cur.execute("UPDATE VIAGEM SET idOrigem = NULL;")
	assert gerencia_contestacao(1,True,resposta_contestacao,cur) == (False,"VALOR FALTANDO PARA VIAGEM COM ORIGEM OU DESTINO DESCONHECIDO",0)

	cur.execute("UPDATE CONTESTACAO SET aprovada = 't';")
	assert gerencia_contestacao(1,True,resposta_contestacao,cur) == (False,"CONTESTACAÇÃO JÁ GERENCIADA",0)


def test_gerencia_contestacao_reprovar(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	resposta_contestacao = "teste resposta"
	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_1,cur)
	aux_insere_viagem(cur)
	insercoes.insere_CONTESTACAO(dados_contestacao,cur)

	assert gerencia_contestacao(1,False,resposta_contestacao,cur) == (True,"SUCESSO",0)

	cur.execute("SELECT aprovada,resposta FROM CONTESTACAO;")
	assert cur.fetchall() == [(False,resposta_contestacao)]

def test_contestacao_apagar_activities(conexao_bd_testes):
	con = conexao_bd_testes
	cur = con.cursor()

	resposta_contestacao = "teste resposta"
	insercoes.insere_GRUPOPESQUISA(dados_grupo_pesquisa,cur)
	insercoes.insere_PESSOA(dados_vazios_pessoa,cur)
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_1,cur)
	aux_insere_viagem(cur)
	cur.execute("UPDATE PESSOA SET idGrupo = 1;")
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico_1,cur)
	
	trip = [{"date": "Mon Nov 27 11:11:16 GMT-03:00 2023", "type": "DESCONHECIDO", "confidence": 100},
	{"date": "Mon Nov 27 11:11:16 GMT-03:00 2023", "type": "A_PE", "confidence": 93},
	{"date": "Mon Nov 27 11:11:42 GMT-03:00 2023", "type": "A_PE", "confidence": 96},
	{"date": "Mon Nov 27 11:12:09 GMT-03:00 2023", "type": "A_PE", "confidence": 93},
	{"date": "Mon Nov 27 11:12:33 GMT-03:00 2023", "type": "A_PE", "confidence": 98}]

	metadados = {
		"emu": False,
		"vAPI": 33,
		"vApp": "0.4.1a",
		"aviao": False,
		"marca": "samsung",
		"modelo": "r9s",
		"bateria": {
			"batteryLevel": 0.9800000190734863,
			"batteryState": "charging",
			"lowPowerMode": False
		}
	}

	cur.execute("UPDATE VIAGEM SET (activityRecognitionTrip,metadados) = (%s,%s);",[json.dumps(trip),json.dumps(metadados)])	

	cur.execute("SELECT idviagem,activityRecognitionTrip,metadados FROM VIAGEM;")
	assert cur.fetchall() == [(1,trip,metadados)]

	insercoes.insere_CONTESTACAO(dados_contestacao,cur)

	assert gerencia_contestacao(1,True,resposta_contestacao,cur) == (True,"SUCESSO",10)

	cur.execute("SELECT idviagem,activityRecognitionTrip,metadados FROM VIAGEM;")
	assert cur.fetchall() == [(1,trip,metadados)]



def aux_insere_viagem(cur):
	localizacao1 = {
		"coordenadas": [0,0],
		"endereco":"Rua 1",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao1,cur)
	apelido_localizacao1 = {
		"idPessoa":1,
		"idLocalizacao":"1",
		"apelido": "apelido1"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao1,cur)
	localizacao2 = {
		"coordenadas": [0.3,0.3],
		"endereco":"Rua 2",
		"tipoLocalizacao":"qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao2,cur)
	apelido_localizacao2 = {
		"idPessoa":1,
		"idLocalizacao":"2",
		"apelido": "apelido2"
	}
	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao2,cur)	
	par_valido = {
		"idPonto1":1,
		"idPonto2":2,
		"distancia": 1000,
		"trajeto":{"posicoes":[[0,0],[0,1],[0,2]]}
	}
	cur = insercoes.insere_PARVALIDO(par_valido,cur)
	viagem_recebida = Viagem.from_json(
		{'dataInicio': 0,
		'duracao': 3000,
		'origem': 1,
		'destino': 2,
		'trajeto': [{"Data":"2023-10-31 12:00:00-03:00"},{"Data":"2023-10-31 12:12:00 GMT-03:00"}]})
	viagem = ViagemBD(1,viagem_recebida,1000,1,2,"Aprovado",0.0)
	cur = insercoes.insere_VIAGEM(viagem,cur)


dados_grupo_pesquisa = {
	"idGrupo":1,
	"remuneracao":10
}

dados_vazios_pessoa = {
	"Nome": "Amigao",
	"DataNascimento": "01/01/2000",
	"Genero": "",
	"Raca": "",
	"RG": "",
	"CPF": "34352",
	"Rua":"",
	"Numero":"",
	"Complemento":"",
	"CEP":"",
	"Bairro":"",
	"Cidade":"",
	"Estado":"",
	"Email": "lorwqjifwep",
	"Telefone": "",
}

dados_contestacao = {
	"idViagem": 1,
	"data": "2023-11-14 15:33:32.987674-03:00",
	"justificativa":"Contestação"
}

dados_bilhete_unico_1 = {
	"NumBilheteUnico":"111111111",
	"idPessoa":1,
	"dataInicio":"2023-09-25 15:33:32.987674-03:00",
	"dataFim":None,
	"ativo":True,
	"concedido":0,
	"aguardandoEnvio":0,
	"aguardandoResposta":0
}

dados_bilhete_unico_2 = {
	"NumBilheteUnico":"134141",
	"idPessoa":1,
	"dataInicio":"2023-09-25 14:33:32.987674-03:00",
	"dataFim":"2023-09-25 15:33:32.987674-03:00",
	"ativo":False,
	"concedido":0,
	"aguardandoEnvio":800,
	"aguardandoResposta":0
}