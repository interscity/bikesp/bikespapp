import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))
sys.path.append(str(Path(__file__).absolute().parent.parent))

from servidor.bikespserver.utils.coords import distancia_haversine
from servidor.tests.utils.trajeto import cria_trajeto

def test_sanity_check():
	assert True

def test_cria_trajeto():
	test_vels = [10,10,20,20]
	test_deltats = [5,5,5,5]

	trajeto_criado = cria_trajeto(test_vels,test_deltats)
	assert len(trajeto_criado) == len(test_vels) + 1

	for i in range(len(test_vels)):
		v = test_vels[i]
		t = test_deltats[i]

		pos1 = trajeto_criado[i]['Posicao']
		t1 = trajeto_criado[i]['Data']
		pos2 = trajeto_criado[i+1]['Posicao']
		t2 = trajeto_criado[i+1]['Data']

		assert abs(distancia_haversine(pos1,pos2) - v*t) < 1
		assert t2 - t1 == t
	