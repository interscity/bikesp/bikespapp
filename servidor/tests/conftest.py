# Vai ser necessário para configurar ambiente de testes de acordo com
# os testes a serem realizados. Vide:
# https://flask.palletsprojects.com/en/2.3.x/testing/#fixtures
# https://flask.palletsprojects.com/en/2.3.x/tutorial/tests/#setup-and-fixtures
import sys
import os
from pathlib import Path
import pytest

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

# pylint: disable=wrong-import-position

from servidor.tests.utils.bd import reseta_bd_teste
from servidor.bikespserver.bd import conecta
from servidor.bikespserver import create_app

# pylint: enable=wrong-import-position

@pytest.fixture()
def app():
    app = create_app(testing=True)
    app.config.update({
        "TESTING": True,
        "BD_HOST": os.getenv("BD_HOST", "localhost"),
		"BD_NOME": os.getenv("BD_NOME_TESTE"),
		"BD_USUARIO": os.getenv("BD_USUARIO_TESTE"),
		"BD_SENHA" :os.getenv("BD_SENHA_TESTE"),
		"BD_PORT" : os.getenv("BD_PORT","5432"),
		"PREFERRED_URL_SCHEME" : "http",
    })
    os.environ["INICIO_PILOTO_TIMESTAMP"] = "1000"
    os.environ["MIN_VERSAO_APP"] = ""

    # other setup can go here

    yield app

    # clean up / reset resources here

@pytest.fixture()
def client_custom_env(app,custom_env):
	test_client = app.test_client()
	for key in custom_env:
		os.environ[key] = custom_env[key]
	return test_client


@pytest.fixture()
def client(app):
    return app.test_client()

@pytest.fixture()
def context(app):
	return app.app_context()

@pytest.fixture()
def request_context(app):
	return app.test_request_context()


# @pytest.fixture()
# def runner(app):
#     return app.test_cli_runner()

@pytest.fixture()
def conexao_bd_testes():
	reseta_bd_teste()
	con = conecta.conecta()
	# Valor que será devolvido ao chamar fixture
	yield con 

	con.close()
