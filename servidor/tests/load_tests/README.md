## Load tests

Para rodar:

1) Entre no shell do poetry
2) Em um terminal, suba o servidor em modo de debug (com a flag `--debug`)
3) Em outro terminal, rode o comando `locust` neste diretório.
4) Quando a mensagem "Load tests done" for impressa, pressione Ctrl+C para visualizar os resultados do teste

Argumentos

--test-duration: duração do teste (Default: 10)
--max-users: máximo de usuários (Default: 10)
--fixed-trips: viagens por usuário (Default: Número aleatório)
