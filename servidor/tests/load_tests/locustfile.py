import gevent
from locust import HttpUser, task, events, between
from locust.env import Environment
from locust.stats import stats_printer, stats_history
from locust.log import setup_logging

import os
import numpy as np
import json
import random
import sys
import argparse
import time
from pathlib import Path

#import config

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

from servidor.tests.load_tests.setup import setup_load_tests
from servidor.tests.load_tests.setup import gera_string_aleatoria, gera_numero_aleatorio

setup_logging("INFO", None)

class Usuario(HttpUser):
	host = os.getenv("HOST_LOAD_TEST","http://127.0.0.1:8000")
	
	@task
	def task_cadastra_login(self):

		idx = self.escolhe_idx_pessoa()
		if idx == -1:
			return
		self.cadastra(idx)
		resp_login = self.loga(idx)

		if self.environment.parsed_options.fixed_trips == -1: 
			qtd_viagens = random.randint(1, 1001)
		else:
			qtd_viagens = self.environment.parsed_options.fixed_trips
			
		for i in range(0,qtd_viagens):
			self.faz_viagem(idx,resp_login)

	def escolhe_idx_pessoa(self):
		todos_idx = set(range(0,len(self.environment.parsed_options.informacoes_load_test["Pessoas"])))
		validos = todos_idx.difference(self.environment.idx_usados)
	
		if len(list(validos)) == 0:
			return -1
		idx = random.choice(list(validos))
		self.environment.idx_usados.add(idx)
		return idx		

	def cadastra(self,idx):
		email = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["Email"]
		cpf = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["CPF"]
		bu = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["NumBilheteUnico"]
		senha = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["Senha"]
		proxies = None

		self.client.post("/api/cadastrar/",data={"email":email,"cpf":cpf,"bu":bu,"senha":senha},proxies=proxies)

	def loga(self,idx):
		email = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["Email"]
		senha = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["Senha"]
		proxies = None
		resp = self.client.post("/api/login/",data={"email":email,"senha":senha},proxies=proxies)
		resp_login = {}
		if resp.status_code == 200:
			resp_login = resp.json()
		return resp_login

	def faz_viagem(self,idx,resp_login):
		if len(resp_login.keys()) == 0:
			return

		cpf = self.environment.parsed_options.informacoes_load_test["Pessoas"][idx]["CPF"]
		token = resp_login["token"]
		info_localizacao1 = list(np.array(resp_login["locs"][0][1][1:-1].split(","),dtype=float))
		info_localizacao2 = list(np.array(resp_login["locs"][1][1][1:-1].split(","),dtype=float))
		
		data_inicio = int(gera_numero_aleatorio(N=10))	
		qtd_pontos_extras = random.randint(0, 101)
		dt = 1000
		data_fim = data_inicio + dt*(qtd_pontos_extras+1)

		trajeto = [{
			"Data": data_inicio,
			"Posicao": {
				"latitude": info_localizacao1[0],
				"longitude": info_localizacao1[1]
			},
			"Precisao": 1
		}]

		for i in range(0,qtd_pontos_extras):
			nova_lat = info_localizacao1[0] + (i+1)*(info_localizacao2[0] - info_localizacao1[0])/qtd_pontos_extras
			nova_long = info_localizacao1[1] + (i+1)*(info_localizacao2[1] - info_localizacao1[1])/qtd_pontos_extras
			trajeto.append(
				{
					"Data": data_inicio + dt * (i+1),
					"Posicao": {
						"latitude": nova_lat,
						"longitude": nova_long
					},
					"Precisao": 1
				}							
			)

		trajeto.append(
			{
				"Data": data_fim,
				"Posicao": {
					"latitude": info_localizacao2[0],
					"longitude": info_localizacao2[1]
				},
				"Precisao": 1
			}
		)

		envio_viagem = {
			"user": cpf,
			"token" : token,
			"viagem" : json.dumps({
				"dataInicio": data_inicio ,
				"duracao": data_fim - data_inicio,
				"origem": "default",
				"destino": "default",
				"trajeto": trajeto
			})
		}

		resp = self.client.post("/api/regViagem/", data=envio_viagem)
	
@events.init_command_line_parser.add_listener
def _(parser):
    parser.add_argument("--test-duration", type=int, default=10)
    parser.add_argument("--max-users", type=int, default=10)
    parser.add_argument("--fixed-trips", type=int, default=-1)

@events.init.add_listener
def _(environment, **kw):

	environment.parsed_options.informacoes_load_test = setup_load_tests(numero_pessoas=environment.parsed_options.max_users)
	environment.idx_usados = set([])

	args = environment.parsed_options

	# start a greenlet that save current stats to history
	gevent.spawn(stats_history, environment.runner)

	# # start the test
	environment.runner.start(args.max_users, spawn_rate=args.max_users/args.test_duration)

	# # in duracao_teste seconds stop the runner
	gevent.spawn_later(args.test_duration, lambda: environment.runner.quit())

	# # wait for the greenlets
	environment.runner.greenlet.join()

	print("Load tests done")