import sys
import string
import random
from pathlib import Path
import pandas as pd

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))


from servidor.bikespserver.bd import conecta

from servidor.etl.respostas_forms.parse_formulario import parse, cria_tipos_notaveis
from servidor.etl.respostas_forms.insere_repostas import insere_respostas
from servidor.tests.utils.bd import reseta_bd_teste

CAMINHO_CSV_BASE = str(Path(__file__).absolute().parent) + "/respostas_forms_base.csv"

def gera_numero_aleatorio(N=5):
	numero = ''.join(random.choice(string.digits) for _ in range(N))
	return numero

def gera_nome_aleatorio(N=5):
	return "PESSOA " + gera_string_aleatoria(N)

def gera_string_aleatoria(N=10):
	s = nome = ''.join(random.choice(string.ascii_uppercase) for _ in range(N))
	return s

def gera_email_aleatorio(N=10):
	email = gera_string_aleatoria(N) + "@email.com"
	return email

def gera_rua_aleatoria(N=10):
	return "Rua " + gera_string_aleatoria(N=N) 

def cria_respostas_fakes(numero_pessoas=10):
	informacoes_pessoas = []
	df = pd.read_csv(CAMINHO_CSV_BASE,dtype=cria_tipos_notaveis())

	resposta_default = """1,1980-01-01 00:00:00,1,pt-BR,699084136,Não,N/A,Não,"Usarei bicicletas compartilhadas (Bike Itaú Bike Bradesco)",,,Não,Sim,testando o formulario,2023-08-01 00:00:00,Homem,Outros,testando outros,"012345678910",12345678910,Sim,12345678910,testador@email.c,11987654321,N/A,01234-900,Praça Charles Miller,0,,Pacaembu,São Paulo,SP,99,Acima de R$ 39.600,,,Fundamental II completo / Médio incompleto,Faz atividades esporádicas e ganhos são avulsos,Nenhum,R$ 0 a R$ 1.320,de 1 a 5,21 ou mais,de 11 a 15,de 16 a 20,de 6 a 10,0,0,0,,de 6 a 10,21 ou mais,,,,,,,,,,,Não,,,,,,,,,,,Não faço esse trajeto e nem farei durante o projeto piloto,,,,,,,,,,,,,,,,,,,,,,,,,Não,,,,,,,,,,,Metrô ou Trem,,PALMEIRAS-BARRA FUNDA,Não,,,,,,,,,,,,,,,,N/A,,,,Sim,,,99,Sim,Não,Não"""
	resposta_default = resposta_default.split(",")
	respostas_fakes = []
	for i in range(0,numero_pessoas):
		resposta_default_copia = list(resposta_default)
		
		resposta_default_copia[0] = i+1 # id resposta
		resposta_default_copia[4] = gera_numero_aleatorio() # seed respostas
		resposta_default_copia[13] = gera_nome_aleatorio() # nome
		resposta_default_copia[18] = gera_numero_aleatorio(10) # RG
		resposta_default_copia[19] = gera_numero_aleatorio(10) # CPF
		resposta_default_copia[21] = gera_numero_aleatorio(10) # NumBilheteUnico
		resposta_default_copia[22] = gera_email_aleatorio() # Email
		resposta_default_copia[23] = gera_numero_aleatorio() # Telefone
		resposta_default_copia[25] = gera_numero_aleatorio() # cep
		resposta_default_copia[26] = gera_rua_aleatoria() # logradouro

		informacoes_pessoas.append({
			"Nome":resposta_default_copia[13],
			"RG": resposta_default_copia[18],
			"CPF": resposta_default_copia[19],
			"NumBilheteUnico": resposta_default_copia[21],
			"Email": resposta_default_copia[22],
			"Senha": gera_string_aleatoria(N=9)
		})

		respostas_fakes.append(resposta_default_copia)

	return respostas_fakes, informacoes_pessoas

def setup_load_tests(numero_pessoas=100):
    """
		Configura ambiente de testes para testes de carga
    """
    informacoes_load_test = {}
    
    respostas_fakes, informacoes_pessoas = cria_respostas_fakes(numero_pessoas=numero_pessoas)
    
    reseta_bd_teste()
    
    con = conecta.conecta(force_bd_prod=False,force_bd_dev=True)
    
    df = pd.read_csv(CAMINHO_CSV_BASE,dtype=cria_tipos_notaveis())
    
    for resposta in respostas_fakes: 
        df.loc[len(df.index)] = resposta
        
    lista_respostas = parse("",df=df,force_prod=False)
    
    insere_respostas(lista_respostas,force_prod=False)
    
    informacoes_load_test["Pessoas"] = informacoes_pessoas
    
    return informacoes_load_test

if __name__ == "__main__":
	setup_load_tests()
 