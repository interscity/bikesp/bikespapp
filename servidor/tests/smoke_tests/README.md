
## Smoke Tests


Para configuar os smoke tests rode:

`crontab -u USER -e`, sendo USER o nome do usuário que rodará os scripts

Adicione a seguinte linha no arquivo de configuração do crontab>

`00 12 * * * bash /full/path/to/launch.sh`

`/full/path/to/launch.sh`  é o caminho absoluto do script `launch.sh`. Esse comando fará com que os smoke tests rodem todo dia ao meio-dia.

Por fim, basta certificar-se que o serviço do cron está rodando:

`sudo service cron start`


### Erros

Em caso de erros, será enviado um email de aviso para os emails presentes na varíavel de ambinete
`SMOKE_TESTS_RECIPIENTS=`, que deve conter os emails separados por vírgula, sem espaço.

Exemplo `SMOKE_TESTS_RECIPIENTS=exemplo@email.com,teste@email.com`.
