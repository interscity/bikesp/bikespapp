#!/bin/bash

# crontab -u USER -e
# 00 12 * * * bash /full/path/to/launch.sh
# sudo service cron start


CURRENT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

SMOKE_TESTS_SCRIPT=$CURRENT_DIR"/smoke_tests.py"

LOG_FILE=$CURRENT_DIR"/smoke.log"

function run_smoke_tests {

	cd $CURRENT_DIR/../../
	shell_exec=". $HOME/.bashrc;"
	smoke_exec="poetry run python $SMOKE_TESTS_SCRIPT;"
	exit_shell="exit;"
	final_exec=""$shell_exec""$smoke_exec""$exit_shell""
	bash --rcfile -c <(echo ""$final_exec"") >> $LOG_FILE
}

run_smoke_tests