'''
	Smoke tests.
		- Testa se os endpoints existem
	Caso algum teste falhe, um email é enviado avisando da falha
'''
import requests
import os
import json
from datetime import datetime

#import config

import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

PROD_SERVER_HOST = "https://survey.ccsl.ime.usp.br/bikesp/"

endpoints_ativos = {
	"/",
	"/api/regViagem/",
	"/api/cadastrar/",
	"/api/login/",
	"/api/checaSess/",
	"/api/logout/",
	"/api/contestaViagem/",
	"/api/sincronizaViagens/",
	"/esqueceuSenha/",
	"/esqueceuSenha/redefinirSenha?token='tokenfake'",
}

endpoints_inexistentes = {
	"/resetaBD/",
	"/naoExiste/",
	"/api/regLocal/"
}

def testa_existencia_endpoints():
	ok,report = (True,{})
	for endpoint in endpoints_ativos:
		url = PROD_SERVER_HOST + endpoint
		resposta = requests.get(url)
		if resposta.status_code == 404:
			ok = False
			report[endpoint] = "Endpoint devolvendo 404"
	for endpoint in endpoints_inexistentes:
		url = PROD_SERVER_HOST + endpoint
		resposta = requests.get(url)
		if resposta.status_code != 404:
			ok = False
			report[endpoint] = "Endpoint devolvendo 200" 
	return ok,report

def envia_email_report(report:dict):
	mail_user = os.getenv("MAIL_USERNAME")
	mail_pass = os.getenv("MAIL_PASSWORD")

	recipients = os.getenv("SMOKE_TESTS_RECIPIENTS")
	if recipients == "":
		recipients = []
	else:
		recipients = recipients.split(",")

	body = "Houve uma falha nos smoke tests. Veja report em anexo para entender onde ocorreu a falha."
	envia_email(mail_user,mail_pass,recipients,"SMOKE TESTS - FALHA",body,report)
	return

def envia_email(sender:str,senha:str,recipients:list,assunto:str,body:str,anexo_json:dict):
	'''
		Envia email contendo um anexo .json
	'''
	data_envio = datetime.now().strftime('%Y-%m-%dT%H-%M-%S')
	msg = MIMEMultipart('alternative')
	msg['Subject'] = assunto
	msg['From'] = sender
	msg['To'] = ', '.join(recipients)
	msg.attach(MIMEText(body, 'plain'))

	anexo = MIMEText(json.dumps(anexo_json,indent=2))

	nome_anexo = "report" + str(data_envio) + ".json"
	anexo.add_header('Content-Disposition', 'attachment', filename=nome_anexo)
	msg.attach(anexo)

	with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp_server:
		smtp_server.login(sender, senha)
		smtp_server.send_message(msg)
	return

def main():
	ok = True
	report = {"endpoints":{}}
	ok_endpoints,report_endpoints = testa_existencia_endpoints()
	
	ok = ok and ok_endpoints
	report["endpoints"] = report_endpoints

	if not ok:
		envia_email_report(report)

	data_run = datetime.now().strftime('%Y-%m-%dT%H-%M-%S')
	status = "OK"
	if not ok:
		status = "FAILED"

	print("[" + str(data_run) + "] - Smoke tests " + status)
	return


if __name__ == "__main__":
	main()




