import sys
import math
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

from servidor.etl.respostas_forms import parse_formulario


def test_parse_formulario():
	csv_path = str(Path(__file__).absolute().parent) + "/respostas_forms_teste.csv"
	resposta_lista = parse_formulario.parse(csv_path)
	assert len(resposta_lista) == 1
	assert isinstance(resposta_lista[0]["Endereco"]["nome"],list)
	assert isinstance(resposta_lista[0]["Nome"]["Nome"],str)
	assert isinstance(resposta_lista[0]["RG"]["RG"],str)
	assert resposta_lista[0]["RG"]["RG"] == "012345678910"
	for k in resposta_lista[0].keys():
		if isinstance(resposta_lista[0][k],dict):
			for key in resposta_lista[0][k]:
				assert isinstance(resposta_lista[0][k][key],list) or isinstance(resposta_lista[0][k][key],str) or isinstance(resposta_lista[0][k][key],int) or isinstance(resposta_lista[0][k][key],float)