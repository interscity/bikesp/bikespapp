import sys
from pathlib import Path

import argparse

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

from servidor.bikespserver.bd.conecta import conecta

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",action='store_true',required=False,help='Debug mode, insere no BD de teste')	
	arg_parser.add_argument("-id","--localizacao",action='store',required=True,help='ID da localização')
	arg_parser.add_argument("-lat","--latitude",action='store',required=True,help='Latitude da nova localização')
	arg_parser.add_argument("-lon","--longitude",action='store',required=True,help='Longitude da nova localização')

	args = arg_parser.parse_args()

	if not args.localizacao.isdigit():
		arg_parser.error("ID da localização inválida")

	try:
		if float(args.latitude) < -90 or float(args.latitude) > 90:
			raise ValueError
	except ValueError:
		arg_parser.error("Valor de latitude inválido")
	try:
		if float(args.longitude) < -90 or float(args.longitude) > 90:
			raise ValueError
	except ValueError:
		arg_parser.error("Valor de longitude inválido")

	return args



def main():

	args = parse_args()

	coordenadas = [args.latitude,args.longitude]
	id_localizacao = args.localizacao

	con = conecta(force_bd_prod=False, force_bd_dev=True)

	cur = con.cursor()

	cur.execute("""
		UPDATE LOCALIZACAO set coordenadas = POINT(%s,%s) WHERE idLocalizacao = %s;	
	""",[coordenadas[0],coordenadas[1],id_localizacao])

	con.commit()
	cur.close()
	con.close()

if __name__ == "__main__":
	main()
