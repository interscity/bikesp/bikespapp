import re
from datetime import timedelta,datetime

#####################################################################
#			                 Utilitários                            #
#####################################################################

def cria_trajeto(vels,deltaTs,pos_inicial=[-23.5503343,-46.6340507],precisoes=[10],formato_data="timestamp", de_ultima_pos=False):
	"""
		Forja um trajeto com as determinadas velocdiades e diferenças
	de tempo entre amostras.
	"""
	pos_atual = pos_inicial
	if formato_data == "timestamp":
		t = 10000
	else:
		t = "1970-01-02 00:16:40 GMT-0300:"

	ultima_pos = pos_atual

	trajeto = [{"Data" : t,	"Posicao" : {
				"latitude" : pos_atual[0],
				"longitude" : pos_atual[1]
			},"Precisao" : 10, "Velocidade" : 0.01}]

	# if len(precisoes) != len(vels):
	# 	precisoes = precisoes * (math.ceil(len(vels)/len(precisoes)))

	for i in range(len(vels)):
		v = vels[i]
		deltaT = deltaTs[i]

		if formato_data == "timestamp":
			t += deltaT
		else:
			t = datetime.strftime(datetime.strptime(t,"%Y-%m-%d %H:%M:%S %Z%z:") + timedelta(seconds=deltaT),"%Y-%m-%d %H:%M:%S %Z%z:")
		pos_atual[0] = pos_atual[0] + v*deltaT/(111111)
		ultima_pos = pos_atual
		trajeto.append({"Data" : t,	"Posicao" : {
				"latitude" : pos_atual[0],
				"longitude" : pos_atual[1]
			},"Precisao" : precisoes[i%len(precisoes)], "Velocidade" : 0.01})
	
	if de_ultima_pos:
		return trajeto,ultima_pos

	return trajeto

def cria_trajeto_str(vels,deltas,pos_inicial=[-23.5503343,-46.6340507], precisoes=[10], de_ultima_pos=False):
	"""
		Usa a função cria trajeto para criar uma string que corresponde
	 ao trajeto criado com os parâmetros dados
	"""

	if de_ultima_pos:
		trajeto, ultima_pos = cria_trajeto(vels,deltas,pos_inicial=pos_inicial,precisoes=precisoes,de_ultima_pos=True)
		return  re.sub("'",'"',str(trajeto)),ultima_pos
	else:
		return  re.sub("'",'"',str(cria_trajeto(vels,deltas,pos_inicial=pos_inicial,precisoes=precisoes,de_ultima_pos=False)))
