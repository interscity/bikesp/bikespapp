import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from servidor.bikespserver.gerenciamento.registra_resposta import converte_resposta_pessoa
from servidor.bikespserver.gerenciamento.registra_resposta import converte_resposta_forms

#####################################################################
#			     	           Mocks                                #
#####################################################################

resposta_mock_crua = {
	'PossibilidadeBike': {'PossibilidadeBike': [6, '', '', '']},
	'DataResposta': {'DataResposta': '1980-01-01 00:00:00'},
	'TemProblemaFisico': {'TemProblemaFisico': 'Não'},
	'SabeAndarDeBike': {'SabeAndarDeBike': ''},
	'TemBikePropria': {'TemBikePropria': 'Não'},
	'ComoConseguiraAcesso': {'ComoConseguiraAcesso': 'Usarei bicicletas compartilhadas (Bike Itaú, Bike Bradesco)', 'other': ''},
	'AutorizouColetaInfo': {'SQ001': 'Não'},
	'AceitouTermo': {'SQ001': 'Sim'},
	'Nome': {'Nome': 'testando o formulario'},
	'DataNascimento': {'DataNascimento': '2023-08-01 00:00:00'},
	'Sexo': {'Sexo': 'Homem'},
	'Raca': {'Raca': 'Outros', 'other': 'testando outros'},
	'RG': {'RG': 123456789},
	'CPF': {'CPF': 12345678909},
	'PossuiBilheteUnico': {'PossuiBilheteUnico': 'Sim'},
	'NumBilheteUnico': {'NumBilheteUnico': 12345678910},
	'Email': {'Email': 'testador@email.c'},
	'Telefone': {'Telefone': 11987654321},
	'PoderaInstalarApp': {'PoderaInstalarApp': ''},
	'Endereco': {
		'cep': ['01234-900', '01311-100', '', '', ''],
		'logradouro': ['Praça Charles Miller', 'Avenida Paulista', '', '', ''],
		'numero': [0, 611, '', '', ''],
		'complemento': ['', '', '', '', ''],
		'bairro': ['Pacaembu', 'Bela Vista', '', '', ''],
		'cidade': ['São Paulo', 'São Paulo', '', '', ''],
		'estado': ['SP', 'SP', '', '', ''],
		'nome': ['ime da usp do ime de são paulo da usp do ime de usp de uma cidade', '', '', '']
	},
	'QuantidadePessoas': {'QuantidadePessoas': 99},
	'RendaMensalFamiliar': {'RendaMensalFamiliar': 'Acima de R$ 39.600'},
	'EstudaRegularmente': {'EstudaRegularmente': '', 'other': ''},
	'GrauInstrucao': {'GrauInstrucao': 'Fundamental II completo / Médio incompleto'},
	'CondicaoAtividade': {'CondicaoAtividade': 'Faz atividades esporádicas e ganhos são avulsos'},
	'VinculoEmpregaticio': {'VinculoEmpregaticio': 'Nenhum'},
	'RendaMensalIndividuo': {'RendaMensalIndividuo': 'R$ 0 a R$ 1.320'},
	'QtdViagens': {
		'QtdViagensAPe': 'de 1 a 5',
		'QtdViagensTransPub': '21 ou mais',
		'QtdViagensBike': 'de 11 a 15',
		'QtdViagensTraPubBike': 'de 16 a 20',
		'QtdViagensOnibus': 'de 6 a 10',
		'QtdViagensTremMetro': 0,
		'QtdViagensCombTraPub': 0,
		'QtdViagensCarroApp': 0,
		'QtdViagensMoto': '',
		'QtdViagensCarMotoTrP': 'de 6 a 10',
		'QtdViagensOutro': '21 ou mais'
	},
	'ObjetivoLocal': {'ObjetivoLocal': ['Outros', '', '', ''], 'other': ['adsada', '', '', '']},
	'FrequenciaTrajeto': {'FrequenciaTrajeto': ['Não faço esse trajeto e nem farei durante o projeto piloto', '', '']},
	'DuracaoTrajeto': {'DuracaoTrajeto': ['', '', '']},
	'ComoFazTrajeto': {'ComoFazTrajeto': ['', '', ''], 'other': ['', '', '']},
	'TipoEstacao': {'TipoEstacao': ['', 'Metrô ou Trem', 'Terminais de ônibus', 'Metrô ou Trem', 'Metrô ou Trem']},
	'EstacaoOnibus': {'EstacaoOnibus': ['', '', 'Term. Penha', '', '']},
	'EstacaoMetro': {'EstacaoMetro': ['', 'PALMEIRAS-BARRA FUNDA', '', 'AYRTON SENNA-JARDIM SÃO PAULO', 'SÉ']},
	'RealizariaTrajeto220': {'RealizariaTrajeto220': ''},
	'PqNaoIncentivo220': {'PqNaoIncentivo220': '', 'other': ''},
	'QtdViagensIncenti220': {'QtdViagensIncenti220': ''},
	'RealizariaTrajeto440': {'RealizariaTrajeto440': 'Sim'},
	'PqNaoIncentivo440': {'PqNaoIncentivo440': '', 'other': ''},
	'QtdViagensIncenti440': {'QtdViagensIncenti440': 99},
	'NaoGarantiaSelecao': {'SQ001': 'Sim'},
	'NecessidadeInternet': {'SQ001': 'Não'},
	'DeclaracaoVeracidade': {'SQ001': 'Não'}
}
resposta_mock = converte_resposta_forms(resposta_mock_crua)
resposta_mock_pessoa = converte_resposta_pessoa(resposta_mock_crua)