import sys
import string
import random
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from servidor.bikespserver.bd import insercoes

def escolhe_nome_aleatorio(N=10):
	'''
		Escolhe um nome aleatoriamente
	'''
	nome = ''.join(random.choice(string.ascii_uppercase) for _ in range(N))
	return nome

def escolhe_documento_aleatorio(N=12):
	'''
		Escolhe um documento aleatoriamente
	'''
	documento = ''.join(random.choice(string.digits) for _ in range(N))
	return documento

def escolhe_email_aleatorio(N=10):
	'''
		Escolhe um email aleatoriamente
	'''
	return escolhe_nome_aleatorio(N) + "@email.com"

def aux_adiciona_pessoa(cur):
	dados = {
		"Nome": escolhe_nome_aleatorio(10),
		"DataNascimento": "01/01/2000",
		"Genero": "",
		"Raca": "",
		"RG": escolhe_documento_aleatorio(10),
		"CPF": escolhe_documento_aleatorio(10),
		"Email": escolhe_email_aleatorio(10),
		"Telefone": "",
		"Rua": "",
		"Numero": "",
		"Complemento": "",
		"Bairro": "",
		"CEP": "",
		"Cidade": "",
		"Estado": "",
		"NumBilheteUnico": escolhe_documento_aleatorio(10)
	}
	insercoes.insere_PESSOA(dados, cur)
	return dados

def aux_adiciona_bilhete_unico(cur,id_pessoa=1,bilhete_unico=escolhe_documento_aleatorio(10)):
	dados_bilhete_unico = {
		"NumBilheteUnico":bilhete_unico,
		"idPessoa":id_pessoa,
		"dataInicio":"2023-09-25 15:33:32.987674-03:00",
		"dataFim":None,
		"ativo":True,
		"concedido":0,
		"aguardandoEnvio":0,
		"aguardandoResposta":0
	}
	insercoes.insere_BILHETEUNICO(dados_bilhete_unico,cur)
	return dados_bilhete_unico
	
def aux_atribui_grupo_pesquisa(cur,remuneracao=1,id_pessoa=1,id_grupo=1):
	"""
		Função auxiliar - insere um grupo pesquisa e o adiciona
		como grupo de certo usuario
	"""
	grupo = {
		"idGrupo":id_grupo,
		"remuneracao":remuneracao
	}
	insercoes.insere_GRUPOPESQUISA(grupo,cur)
	if not isinstance(id_pessoa,list):
		id_pessoa = [id_pessoa]

	for i in id_pessoa:
		insercoes.atualiza_grupo_pessoa(grupo["idGrupo"],i,cur)
	

def aux_adiciona_locs_user(cur, ids_localizacoes=[1,2], id_pessoa=1, tipos=["qualquer","qualquer"]):
	'''
		Função auxiliar - liga localizações
		a um usuario
	'''
	for i in range(0,len(ids_localizacoes)):
		id_localizacao = ids_localizacoes[i]
		try:
			tipo = tipos[i]
		except IndexError:
			tipo = "qualquer"

		apelido_localizacao = {
			"idPessoa":id_pessoa,
			"idLocalizacao":id_localizacao,
			"apelido": escolhe_nome_aleatorio(5)
		}
		if tipo == "estacao":
			insercoes.insere_PESSOAESTACAO(apelido_localizacao,cur)
		else:
			insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao,cur)

def aux_adiciona_sessao(cur,token="token",id_pessoa=1,data_validade="2042/07/04 09:45:00 PM GMT-3"):
	sessao_dict = {
		"idPessoa": id_pessoa,
		"token": token, 
		"dataValidade": data_validade
	}

	insercoes.insere_SESSAO(sessao_dict,cur)
