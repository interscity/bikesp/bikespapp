import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD

def aux_adiciona_viagem(
	cur,
	id_usuario=1,
	data_inicio=0,
	duracao=3000,
	id_origem=1,
	id_destino=2,
	deslocamento=1000,
	status="Aprovado",
	remuneracao=0.0
):
	viagem_recebida = Viagem.from_json(
		{'dataInicio': data_inicio,
		'duracao': duracao,
		'origem': id_origem,
		'destino': id_destino,
		'trajeto': ""})
	viagem = ViagemBD(id_usuario,viagem_recebida,deslocamento,id_origem,id_destino,status,remuneracao)	
	insercoes.insere_VIAGEM(viagem,cur)
