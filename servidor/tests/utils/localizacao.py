import string
import random
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from servidor.bikespserver.bd import insercoes

def escolhe_rua_aleatoria(N=5):
	'''
		Escolhe um nome para a rua aleatoriamente
	'''
	nome = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))
	return "Rua " + nome

def aux_adiciona_localizacoes(cur,coordenadas=[[0,0]],tipos=["qualquer"]):
	"""
			Função auxiliar - adiciona uma lista de localizações
	"""
	if not isinstance(coordenadas[0],list):
		coordenadas = [coordenadas]


	info_locs = []
	for i in range(0,len(coordenadas)):
		coordenada = coordenadas[i]
		try:
			tipo = tipos[i]
		except IndexError:
			tipo = "qualquer"

		loc1 = {
			"coordenadas":coordenada,
			"endereco":escolhe_rua_aleatoria(),
			"tipoLocalizacao":tipo
		}
		insercoes.insere_LOCALIZACAO(loc1,cur)
		info_locs.append(loc1)
	return info_locs

def aux_adiciona_par_valido(cur, id_localizacao_1=1, id_localizacao_2=2, distancia=10, trajeto={}):
	id_ponto1 = min(id_localizacao_1,id_localizacao_2)
	id_ponto2 = max(id_localizacao_1,id_localizacao_2)	
	insercoes.insere_PARVALIDO({"idPonto1":id_ponto1,"idPonto2":id_ponto2,"distancia":distancia,"trajeto":{}},cur)