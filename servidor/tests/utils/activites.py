import sys
import random as rd

from pathlib import Path
from datetime import datetime, timedelta

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.viagem.validaviagem.consts import FORMATO_DATA_ACTIVITIES
# from servidor.tests.utils.trajeto import cria_trajeto


class ActivitiesBuilder:

	def __init__(self,num_pontos, data_inicio="Fri Dec 15 08:45:41 GMT-03:00 2023", delta_t=5) -> None:
		self.num_pontos = num_pontos
		self.data_inicio = data_inicio
		self.delta_t = delta_t

		self.subtrajeto_bike = []
		self.subtrajeto_veiculo = []
		self.subtrajeto_parado = []

	def _gera_subtrajeto(self,tipo, quantos, confianca=[100]) -> None:
		quantos = int(quantos)
		if len(confianca) < quantos:
			confianca = quantos*confianca
		
		novo_subtrajeto = []

		for i in range(quantos):
			novo_subtrajeto.append({"type" : tipo, "confidence" : confianca[i]})

		return novo_subtrajeto

	def set_subtrajeto_bike(self, quantos, confianca=[100]) -> None:
		self.subtrajeto_bike = self._gera_subtrajeto("NA_BICICLETA",quantos,confianca)

	def set_subtrajeto_veiculo(self, quantos, confianca=[100]) -> None:
		self.subtrajeto_veiculo = self._gera_subtrajeto("EM_VEICULO",quantos,confianca)

	def set_subtrajeto_parado(self, quantos, confianca=[100]) -> None:
		self.subtrajeto_parado = self._gera_subtrajeto("PARADO",quantos,confianca)

	def gera_lista_medicoes(self) -> list:
		lista_atividades = self.subtrajeto_bike + self.subtrajeto_parado + self.subtrajeto_veiculo

		if len(lista_atividades) < self.num_pontos:
			lista_atividades = lista_atividades + \
				[{"type" : "DESCONHECIDO", "confidence" : 100}] * (self.num_pontos - len(lista_atividades))

		rd.seed(1312)
		rd.shuffle(lista_atividades,)

		t = self.data_inicio

		amostras = []

		for atividade in lista_atividades:
			amostra = atividade.copy()
			if(t[-8]) == ":":
				t_fiel = t
			else:
				t_fiel = t[:-7] + ":" + t[-7:]  
			amostra["date"] = t_fiel
			t = datetime.strftime(datetime.strptime(t,FORMATO_DATA_ACTIVITIES) + timedelta(seconds=self.delta_t),
			FORMATO_DATA_ACTIVITIES)

			amostras.append(amostra)

		return amostras

