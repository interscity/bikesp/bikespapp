import sys
from pathlib import Path
import subprocess

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#####################################################################
#			     	Configuração do BD de Testes                    #
#####################################################################

def reseta_bd_teste():
    """
		Limpa o banco de testes e cria as tabelas vazias
    """
    subprocess.run(["devbox", "run", "reset-banco-teste"], check=True)
