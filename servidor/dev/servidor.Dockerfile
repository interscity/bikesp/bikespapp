# Etapa de build do frontend vite
FROM node:18-alpine AS builder

COPY ./vite /vite
WORKDIR /vite
RUN npm install && npm run build

# Etapa final
FROM python:3.10-slim-bookworm AS runtime

ENV USER=bikesp
ARG UID

USER root:root

# Cria usuário não root e instala dependências
RUN addgroup --gid 1000 ${USER} && adduser --shell /bin/bash --uid ${UID} --gid 1000 --disabled-password --gecos "" ${USER} && \
	apt-get update && apt-get install -y --no-install-recommends \
	gcc \
	libc6-dev \
	libffi-dev \
	libssl-dev \
	python3-dev \
	make \
	build-essential && \
	apt-get clean && rm -rf /var/lib/apt/lists/*

# Cria diretórios
RUN mkdir -p \
	/servidor \
	/content \
	/vite && \
	chown ${USER}:${USER} -R \
	/servidor \
	/content \
	/vite

WORKDIR /servidor

# Prepara PATH do usuário
USER $USER:$USER
ENV PATH="/scripts:/home/${USER}/.local/bin:${PATH}"

# Build da etapa anterior
COPY --chown=${USER}:${USER} --from=builder /vite/dist /vite/dist

# Dependências do Python
COPY --chown=${USER}:${USER} ./requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
