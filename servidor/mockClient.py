import requests
import re
import sys
import json

from pathlib import Path

sys.path.append(str(Path(__file__).absolute()))

#pylint: disable=wrong-import-position

from bikespserver.validaviagem.valida_viagem import valida_viagem_bike
from servidor.bikespserver.classes.viagem import Viagem

from servidor.tests.utils.trajeto import cria_trajeto_str

from servidor.bikespserver.validaviagem.consts import NUM_MIN_AMOSTRAS_TRAJETO

req_data_antiga = {
	"user": "ArthurTesteRequests",
	"viagem" : re.sub("[\n\t ]+", "",
"""{
	"dataInicio": 1231312312,
	"duracao": 51,
	"origem": "trabalho",
	"destino": "destino",
	"trajeto": [
		{
			"Data" : 132123,
			"Posicao" : {
				"latitude" : -45.12312,
				"longitude" : -48.0012
			},
			"Precisao" : 15.231,
			"Velocidade" : 0.01
		},
		{
			"Data" : 1321278,
			"Posicao" : {
				"latitude" : -45.124,
				"longitude" : -48.010
			},
			"Precisao" : 19.531,
			"Velocidade" : 15.01
		}
	]
}""")
}

req_data = {
	"user": "ArthurTesteRequests",
	"viagem" : re.sub("[\n\t ]+", "",
"""{
	"dataInicio": 1231312312,
	"duracao": 51,
	"origem": "trabalho",
	"destino": "destino",
	"trajeto": %s }""" %(cria_trajeto_str(
		[15]*(NUM_MIN_AMOSTRAS_TRAJETO + 5),
		[5]*(NUM_MIN_AMOSTRAS_TRAJETO + 5))))
}


# resp = requests.post("http://localhost:5000/api/regViagem/", data=req_data)

# viagemObjeto = Viagem.from_json(json.loads(req_data["viagem"]))

# print(valida_viagem_bike(viagemObjeto))
print(req_data_antiga)
print("\n\n\n\n")
print(req_data)