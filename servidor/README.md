# Servidor BikeSP

## Executando o servidor

O servidor é gerido pelo devbox. Portanto dependências e configurações são
gerenciadas automaticamente, desde que você esteja num `devbox shell` ou usando
`direnv`.

Para iniciar o servidor, existem duas opções:

```sh
devbox run servidor # Inicia o servidor em primeiro plano
devbox services start servidor # Inicia o servidor em segundo plano
```

A segunda opção é a utilizada pelo `devbox run app`. Em ambos os casos, o banco
é iniciado automaticamente.

## Executando manualmente

- (desenvolvimento) flask --app bikespserver run --reload --debugger --host=0.0.0.0 --port=8000
- (testes) flask --app bikespserver run --debug --host=0.0.0.0 --port=8000 (--debug utiliza o BD de teste)
- (produção) gunicorn -w 2 'bikespserver:create_app()' --access-logfile {caminho para log do servidor}

## Variáveis de ambiente necessárias

As seguintes variáveis de ambiente são usadas no servidor:

```sh
-- SERVER CONFIG --
IGNORA_USER_AUTH_SALVANDO_VIAGEM=
SECRET_KEY_FLASK=
CHAVE_INTEGRIDADE=
ENCRIPT_PK=""
INICIO_PILOTO_TIMESTAMP= (unix epoch secs)
MIN_VERSAO_APP="0.x.y"

-- INTERFACE ADM
ADM_USER=
ADM_PASS=

-- INFO BD PROD --
BD_NOME=""
BD_USUARIO=""
BD_SENHA=""
BD_HOST=""
BD_PORT=""

-- INFO BD TESTES --
BD_NOME_TESTE=""
BD_USUARIO_TESTE=""
BD_SENHA_TESTE=""

-- ETL CONFIG --
CAMINHO_RESPOSTAS_FORMS=""
TOMTOM_API_KEY=""
SENHA_GOOGLE=""

-- ENVIO DE EMAIL --
MAIL_USERNAME=""
MAIL_PASSWORD=""
SMOKE_TESTS_RECIPIENTS="" (separados por vírgula, sem espaço)
SCHEME="http" / "https", default = https

-- VALIDATION CONFIG --
INVALIDA_EMULADORES="True"

-- EXPORTAÇÃO DO LIMESURVEY --
USUARIO_LIMESURVEY=""
SENHA_LIMESURVEY=""
ID_SURVEY="612292"
```

Nem todas são necessárias para o funcionamento do servidor em desenvolvimento,
mas todas são necessárias para o funcionamento em produção.

## Desenvolvendo Páginas e Componentes para o Front

1. Vá ao diretório `servidor/vite/` e rode `npm install`. Biliotecas adicionais devem ser adicionadas por aqui
2. Volte ao diretório `servidor/` e rode `flask --app bikespserver vite build` para compilar os componentes react.
3. Se for adicionar suporte a mais uma tela, incremente o ifelse do `servidor/vite/main.js`

## Para iniciar o banco

Se desejar iniciar o banco manualmente, use:

```sh
devbox services start banco
```

## Para adicionar um grupo de pesquisa

```sh
python etl/respostas_forms/set_grupo_pesquisa.py -o {insercao/remocao} \
    -g {id do grupo} -v {valor da remuneração (por km)}
```

## Para realizar as inserções vindas do formulário

```sh
python etl/respostas_forms/exporta_insere.py -csv {caminho para o csv}
```

## Para apagar o banco e recriar as tabelas

```sh
devbox run clean --db # Apaga todos dados do banco
devbox run setup # Dentre outras coisas, cria as tabelas
```

## Ambiente de Produção

A implantação do backend do BikeSP é toda orquestrada via `docker compose`. Para
colocar o sistema em produção, deve-se popular o arquivo `deploy/.env` seguindo
o modelo `deploy/.env.demo` e o arquivo `deploy/.servidor.env` seguindo o modelo
`deploy/.servidor.env.demo`. Além disso, certifique-se que todos arquivos extras
necessários estão nos seus devidos locais (no momento da escrita desse texto, se
trata apenas do `.serviceAccountKey.json`, `deploy/.driveServiceAccountKey.json`,
`deploy/.bd_senha`, `deploy/.backupkey`, `deploy/.backupkey.pub` e do `dados_google.json`). Importante lembrar que a implantação pode
ser afetada por duas variáveis de ambiente do seu shell:'

- `SERVER_PORT`: define em qual porta expor o serviço do gunicorn (padrão: 8000)
- `BD_PORT`: define em qual porta expor serviço do postgres (padrão: 5432)

Note que isso não afeta o funcionamento do contêiner, apenas como o ambiente
externo pode interagir com esses serviços. Além disso, é necessário ter os
seguintes arquivos prontos:

- `.serviceAccountKey.json`: é o arquivo produzido pelo Firebase com credenciais que nos permitem enviar notificações push para os usuários
- `deploy/.driveServiceAccountKey.json`: é o arquivo de credenciais do Google Drive que permite ao contêiner enviar backups do banco de dados para o Drive
- `etl/respostas_forms/dados_google.json`: contém as credenciais do usuário que a google no BikeSP (eles precisam de um usuário para poder aprovar o app)
- `deploy/.bd_senha`: arquivo de segredo usado apenas pelo contêiner do banco com a senha a ser usada pelo Postgres na criação do cluster
- `deploy/.backupkey`: chave privada usada para descriptografar os backups do banco
- `deploy/.backupkey.pub`: chave pública usada para criptografar os backups do banco

A composição conta com 4 serviços:

### `servidor`

Executa o servidor flask usando o `gunicorn`. A imagem responsável por ele cuida
de fazer o build do conteúdo da pasta `vite/` e depois configurar tudo necessário
para o funcionamento do servidor em python usando as dependências do
`requirements.txt`. As variáveis de ambiente que ele precisa são povoadas com os
arquivos `deploy/.env` e `deploy/.servidor.env`

Conta com utilitários: `insert` e `etl` que são scripts
que facilitam a manipulação do banco de dados e a inserção de novos usuários.
Todos contam com páginas de ajuda. Os 3 primeiros estão inclusos na PATH do
contêiner, enquanto o último representa uma coleção de scripts python que podem
ser executados com `python caminho/para/o/script.py`

Utiliza também dois volumes:

- `logs`: armazena os logs emitidos pelo servidor
- `content`: guarda os arquivos `.csv` produzidos pelo `insert`

Durante a execução monta os arquivos secretos necessários para o funcionamento
do servidor (`.serviceAccountKey.json` e `dados_google.json`)

### `banco`

O serviço para executar o banco de dados Postgres. Depende apenas dos arquivos
`bikespdb/config/tables.sql` e `deploy/.bd_senha` e das variáveis de ambiente do
arquivo `deploy/.env` e `deploy/.pg.env` (note que esse arquivo não precisa estar
no .gitignore pois não contém nenhum dado sensível, apenas instrui à imagem do
Postgres quais variáveis usar para configurar o banco de dados).

> **ATENÇÃO**: É de suma importância que o conteúdo do arquivo `deploy/.bd_senha`
> seja **idêntico** ao conteúdo da variável de ambiente `BD_SENHA`. Caso
> contrário o servidor não terá acesso ao banco de dados.

Seus dados ficam armazenados no volume `data`

Usa um `docker secret` `bd_senha` para armazenar a senha do banco.

### `logs`

Um serviço simples que executa o [lnav](https://lnav.org/) para renderizar arquivos
de log. Ele depende do volume `logs`.

### `psql`

Um serviço com um cliente `psql` instalado para facilitar interação rápida com
o banco de dados. Ele apenas depende das variáveis de ambiente, dos arquivos
`deploy/.env`, `deploy/.backupkey`, `deploy/.backupkey.pub` e `deploy/.driveServiceAccountKey.json`. Conta com o utilitário
`connect` para conectar diretamente ao banco e scripts `dump`, `restore`, `backup`
e `recover` para lidar com backups do banco.

É impossível iniciar esse serviço sem antes gerar as chaves de criptografia.

Utiliza o volume `content` para armazenar os backups do banco localmente. Pode
enviar os backups para o Google Drive usando o script `backup`. Os backups ficam
armazenados na pasta `backups` do Drive.

## Como Fazer o Deploy

1. Crie as chaves de criptografia para os backups com `./deploy/scripts/keygen -p ./deploy`
2. Certifique-se que os arquivos `.env` estejam corretamente populados e os demais arquivos de segredos estão em seus devidos locais.
3. Execute `docker compose build` para construir as imagens dos contêineres (use a flag `--no-cache` se notar que o build não está atualizando as coisas necessárias)
4. Configure `BD_PORT` e `SERVER_PORT` se necessário
5. Execute `docker compose up -d`
6. Verifique se está tudo bem olhando os logs com `docker compose logs`

Parabéns, o servidor está em produção

## Lendo logs

Os logs emitidos pelo servidor ficam armazenados no volume `bikesp-logs`.
Mas podem ser facilmente lidos usando o serviço `logs`

```sh
docker compose exec logs ls # Lista os logs disponíveis
docker compose exec logs lnav logV46 # Abre o log `logV46` no lnav
docker compose exec logs lnav . # Abre todos os logs no lnav
```

Existem 3 tipos de logs:

- `log`: saída do logger do flask (reporta requests recebidas)
- `stdout`: saída padrão do gunicorn (reporta stacktraces)
- `backup`: saída do sistema de backup (reporta sucessos ou falhas dos backups)

É recomendado que você leia a documentação do [lnav](https://lnav.org/) para aprender
a usar o programa.

## Acessando o Banco

Use o serviço `psql` para fazer interações com o banco de dados. Ele possui um cliente
`psql` instalado e alguns scripts utilitários para facilitar a conexão.

```sh
docker compose exec psql connect # Se conecta ao banco de dados
```

## Backup e Recuperação do Banco

Existem 4 scripts disponíves no serviço `psql` para lidar com isso:

- `dump`: cria um dump criptografado do banco de dados
- `restore`: restaura o banco de dados a partir de um dump criptografado
- `backup`: faz um `dump` e envia para o Google Drive
- `recover`: faz download de um dump salvo no Google Drive e usa `restore` para restaurar o banco

Os dois primeiros scripts são usados para lidar com backups locais, enquanto os
seguintes usam o Google Drive para armazenar os backups. Possivelmente você não vai querer usar o `dump` e `restore` diretamente, mas sim o `backup` e `recover`.

```sh
bikespapp/servidor $ docker compose exec psql backup # Cria um backup no Drive
bikespapp/servidor $ docker compose exec psql recover -l # Lista os backups disponíveis
bikespapp/servidor $ docker compose exec psql recover backup-YYYY-mm-dd-HH-MM-SS.sql.bz2.enc # Restaura um backup pelo seu nome
```

Para o funcionamento desses scripts é necessário criar as chaves de criptografia no hospedeiro da composição (as chaves só são montadas no contêiner na hora de executar). Para isso, use o script `keygen`:

```sh
bikespapp/servidor $ ./deploy/scripts/keygen -p ./deploy
```

Para se autenticar com o Google Drive é necessário ter o arquivo `deploy/.driveServiceAccountKey.json` com as credenciais da conta de serviço que usamos para fazer backup no Google Drive.

> É impossível subir o serviço `psql` sem as chaves de criptografia ou credenciais de conta de serviço.

> Caso perca essas chaves de criptografia perdemos a chance de recuperar qualquer backup. Faça algumas cópias dessa chave por segurança.

Usamos o `rclone` por trás dos panos para fazer o envio dos backups para a pasta `backups`.

Na configuração do `rclone`, o remoto foi chamado de `gdrive` e está configurado
para que os arquivos sejam acessíveis apenas pela conta de serviço que está
enviando os backups.

```sh
docker compose exec psql rclone --help
```

Para se ter backups automáticos, basta criar um cronjob no hospedeiro dos contêineres que execute:

```sh
docker compose -f caminho/até/docker-compose.yaml exec psql backup
```

## Inserindo Novos Usuários

Existe o script chamado `insert` na $PATH do **servidor** que irá realizar todo
procedimento para você. Apenas atente-se às variáveis de ambiente:

- `USUARIO_LIMESURVEY`
- `SENHA_LIMESURVEY`
- `ID_SURVEY` (por padrão 612292)

```sh
docker compose exec servidor insert
```

Isso vai exportar os dados do formulário (612292), salvar na pasta `/content`
como um `.csv` chamado `survey-YYYY-mm-dd-HH-MM-SS.csv` e realizar a inserção.
Esse script é apenas um wrapper do script `servidor/etl/respostas_forms/exporta_insere.py`

## Usando Demais Scripts ETL

A pasta `/servidor/etl` contém scripts python que podem ser usados para muitas
operações. Todos eles possuem uma página de ajuda (`--help`). Basta executar
os scripts usando python:

```sh
python /servidor/etl/respostas_forms/exporta_insere.py --help
```

## Verificando Consumo de Recursos

O `docker compose` fornece o comando `stats` que mostra como está a utilização de
recursos pelos serviços.

```sh
docker compose stats
```
