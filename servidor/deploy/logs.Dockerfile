FROM alpine:3.16

ARG VERSION=0.12.3
ARG PLATFORM="linux-musl-x86_64"

RUN wget https://github.com/tstack/lnav/releases/download/v${VERSION}/lnav-${VERSION}-${PLATFORM}.zip && \
	unzip lnav-${VERSION}-${PLATFORM}.zip && \
	cp lnav-${VERSION}/lnav /bin/ && \
	rm -rf lnav-${VERSION}-${PLATFORM}.zip lnav-${VERSION}-${PLATFORM}

COPY scripts/collect /bin/collect

RUN chmod +x /bin/collect

ENV USER=bikesp
ARG UID

RUN adduser -D -s /bin/sh -u ${UID} ${USER} ${USER}
RUN mkdir -p /logs && \
	chown -R ${USER}:${USER} /logs
USER ${USER}:${USER}

WORKDIR /logs

ENV TERM=xterm-256color
ENTRYPOINT ["sh"]
