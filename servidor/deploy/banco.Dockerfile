FROM postgres:15

RUN sed -i 's/^# *\(en_US.UTF-8\|pt_BR.UTF-8\)/\1/' /etc/locale.gen && locale-gen

ENV LANG=pt_BR.utf8
ENV TZ="America/Sao_Paulo"

COPY config/tables.sql /docker-entrypoint-initdb.d