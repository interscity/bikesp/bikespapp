FROM alpine/psql:15.5

WORKDIR /

RUN apk add --no-cache \
	tzdata \
	openssl \
	rclone

COPY scripts/restore /bin/restore
COPY scripts/dump /bin/dump
COPY scripts/connect /bin/connect
COPY scripts/backup /bin/backup
COPY scripts/recover /bin/recover
COPY scripts/init.psql /bin/init.psql

RUN chmod +x /bin/restore && \
	chmod +x /bin/dump && \
	chmod +x /bin/connect && \
	chmod +x /bin/backup && \
	chmod +x /bin/recover && \
	chmod +x /bin/init.psql

ENV USER=bikesp
ARG UID
RUN adduser -D -s /bin/sh -u ${UID} ${USER} ${USER}
RUN mkdir /content /logs && \
	chown -R ${USER}:${USER} /content /logs
USER ${USER}:${USER}


ENV TERM=xterm-256color
ENV TZ=America/Sao_Paulo
ENTRYPOINT ["init.psql"]
