"""
	Módulo para encapsular lógica de uso do FCM para envio de notificações push.
"""
import logging
import os

from firebase_admin import credentials, initialize_app, messaging, exceptions

DEVBOX_PROJECT_ROOT = os.getenv("DEVBOX_PROJECT_ROOT", "/")
CERT_PATH = os.path.join(DEVBOX_PROJECT_ROOT, "servidor",
                         ".serviceAccountKey.json")
CERT = credentials.Certificate(CERT_PATH)

app = initialize_app(CERT)


def __monta_mensagem(token: str,
                     titulo: str,
                     mensagem: str | None = None,
                     subtitulo: str | None = None) -> messaging.Message:
	"""
		Constroi objeto de mensagem do FCM
	"""
	data = {"title": titulo}
	if subtitulo:
		data["subtitle"] = subtitulo
	if mensagem:
		data["body"] = mensagem

	return messaging.Message(data=data, token=token)


def envia_notificacoes(tokens: list[str],
                       titulo: str,
                       mensagem: str | None = None,
                       subtitulo: str | None = None):
	"""
		Envia notificações para os tokens fornecidos usando o FCM.
	"""
	falhas = 0

	for token in tokens:
		message = __monta_mensagem(token, titulo, mensagem, subtitulo)

		try:
			response = messaging.send(message, app=app)
			if not response:
				logging.getLogger("bikespserver").error(
				    "Erro ao enviar notificação, resposta vazia do FCM")
				falhas += 1
		except exceptions.FirebaseError as error:
			logging.getLogger("bikespserver").error(
			    "Erro ao enviar notificação: %s", error)
			falhas += 1

	if falhas == 1:
		return ({
		    "estado": "Erro",
		    "erro": "Erro ao enviar uma notificação"
		}, 500)

	if falhas > 1:
		return ({
		    "estado": "Erro",
		    "erro": f"Erro ao enviar {falhas} notificações"
		}, 500)

	return ({"estado": "Notificações enviadas"}, 200)
