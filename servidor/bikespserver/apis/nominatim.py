'''
	Gerencia a API Nominatim:
		https://nominatim.org/release-docs/latest/api/Overview/
	utilizando a interface do geopy:
		https://geopy.readthedocs.io/en/stable/#nominatim
'''

from typing import Any, List
from geopy.geocoders import Nominatim


class NominatimAPI():  # pylint: disable=too-few-public-methods
	'''
		Classe responsável pela API Nominatim
	'''

	def __init__(self):
		'''
			Inicializa o localizador da API
		'''
		self.locator = Nominatim(user_agent="geocoder")
		self.COORD_INVALIDA = [-1, -1]  # pylint: disable=invalid-name

	def __busca_dict(self, dict_busca: Any) -> Any:
		'''
			Realiza a busca utilizando um dicionário estruturado com cada elemento
			do endereço (rua, número, bairro, cep, cidade, estado, país)
		'''
		try:
			localizacao = self.locator.geocode(dict_busca, addressdetails=True)
		except (Exception,):  # pylint: disable=broad-exception-caught
			localizacao = None
		return localizacao

	def __busca_str(self, str_busca: Any) -> Any:
		'''
			Realiza a busca utilizando apenas uma string
		'''
		try:
			localizacao = self.locator.geocode(str_busca)
		except (Exception,):  # pylint: disable=broad-exception-caught
			localizacao = None
		return localizacao

	def get_coordenadas(self, busca: dict | str) -> List[float]:
		'''
			Devolve as coordenadas de determinada localização
		'''
		if isinstance(busca, str):
			localizacao = self.__busca_str(busca)
		elif isinstance(busca, dict):
			localizacao = self.__busca_dict(busca)
		else:
			raise ValueError("Formato da busca é inválido")
		if localizacao is None:
			return self.COORD_INVALIDA
		return [localizacao.latitude, localizacao.longitude]
