'''
	Gerencia a API TomTom:
		https://developer.tomtom.com 
'''
import sys
import os
from pathlib import Path
from typing import List
import requests

from simplification.cutil import simplify_coords  #pylint: disable=no-name-in-module

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#import servidor.config  # pylint: disable=unused-import disable=wrong-import-position


class TomTomAPI:  # pylint: disable=too-few-public-methods
	'''	
		Classe responsável pela API TomTom
	'''

	def __init__(self):
		'''
			Define URLs e busca a chave da API no .env
		'''
		self.COORD_INVALIDA = [-1, -1]  # pylint: disable=invalid-name
		self.url_base = "https://api.tomtom.com/"
		self.url_rota = self.url_base + "routing/1/calculateRoute"
		self.url_geocode = self.url_base + "search/2/geocode/"
		self.url_struct_geocode = self.url_base + "search/2/structuredGeocode.json"
		self.api_key = os.getenv("TOMTOM_API_KEY")

	def __cria_url_calcula_rota(self,
	                            coordenadas: List[float],
	                            transporte="bicycle") -> str:
		'''
			Monta a URL para buscar uma rota
		'''
		lat1, lon1, lat2, lon2 = coordenadas
		url = self.url_rota + "/" + str(lat1) + "," + str(lon1) + ":" + str(
		    lat2) + "," + str(lon2) + "/json?travelMode=" + str(
		        transporte) + "&key=" + str(self.api_key)
		return url

	def __cria_url_struct_geocode(self, struct: dict) -> str:
		'''
			Monta a URL para buscar uma coordenada dado uma estrutura
			de endereço
			Parâmetros uteis:
				- countryCode
				- countrySubdivision
				- streetNumber
				- streetName
				- municipality
				- countryTertiarySubdivision
				- municipalitySubdivision
				- postalCode
		'''
		if "countryCode" not in struct or struct["countryCode"] == "":
			struct["countryCode"] = "BR"
		if "countrySubdivision" not in struct or struct[
		    "countrySubdivision"] == "":
			struct["countrySubdivision"] = "São Paulo"
		str_parametros = ""
		for key in struct:
			if str(struct[key]) == "":
				continue
			str_parametros += key + "=" + str(struct[key]) + "&"

		str_parametros = str_parametros[0:-1]
		url = self.url_struct_geocode + "?key=" + self.api_key + "&" + str_parametros
		return url

	def __cria_url_geocode(self, endereco: str) -> str:
		'''
			Monta a URL para buscar uma coordenada dado uma string
			de endereço
		'''
		url = self.url_geocode + str(endereco) + ".json?key=" + self.api_key
		return url

	def __realiza_request(self, url: str, timeout=15):
		'''
			Realiza a requisição e trata possíveis erros
		'''
		try:
			resp = requests.get(url=url,
			                    headers={"accept": "*/*"},
			                    timeout=timeout)
			resp.raise_for_status()
		except requests.exceptions.HTTPError as err:
			raise err
		except requests.exceptions.ConnectionError as err:
			raise err
		except requests.exceptions.Timeout as err:
			raise err
		except requests.exceptions.RequestException as err:
			raise err
		return resp

	def __get_distancia_rota(self, rota: dict) -> int:
		'''
			Devolve a distância de determinada rota
		'''
		dist = int(rota["routes"][0]["summary"]["lengthInMeters"])
		return dist

	def __get_trajeto_rota(self, rota: dict) -> dict:
		lista_pontos = []
		for coords in rota["routes"][0]["legs"][0]["points"]:
			lista_pontos.append([coords["latitude"], coords["longitude"]])
		pontos_simplificados = simplify_coords(lista_pontos, 0.00001)
		return {"posicoes": pontos_simplificados}

	def __get_struct_geocode(self, struct: dict) -> List[float] | None:
		url = self.__cria_url_struct_geocode(struct)
		resp = self.__realiza_request(url)
		resp_json = resp.json()
		if len(resp_json["results"]) == 0:
			return None
		resp_json["results"].sort(key=lambda x: x["matchConfidence"]["score"],
		                          reverse=True)
		localizacao = resp_json["results"][0]["position"]
		# Pega o primeiro resultado que for no estado de São Paulo
		for i in range(0, len(resp_json["results"])):
			loc = resp_json["results"][i]
			if "address" in loc and "countrySubdivision" in loc[
			    "address"] and loc["address"]["countrySubdivisionCode"] == "SP":
				localizacao = loc["position"]
				break
		return [localizacao["lat"], localizacao["lon"]]

	def __get_geocode(self, endereco: str) -> List[float] | None:
		url = self.__cria_url_geocode(endereco)
		resp = self.__realiza_request(url)
		resp_json = resp.json()
		if len(resp_json["results"]) == 0:
			return None
		localizacao = resp_json["results"][0]["position"]
		return [localizacao["lat"], localizacao["lon"]]

	def calcula_rota(self,
	                 coordenadas: List[float],
	                 transporte="bicycle") -> dict:
		'''
			Devolve o objeto rota, contendo diversas informações acerca da rota
				- distância, duração, pontos, etc.
		'''
		url = self.__cria_url_calcula_rota(coordenadas, transporte=transporte)
		try:
			resp = self.__realiza_request(url)
		except requests.exceptions.HTTPError:
			url = self.__cria_url_calcula_rota(coordenadas, transporte="car")
			resp = self.__realiza_request(url)
		rota = resp.json()
		distancia = self.__get_distancia_rota(rota)
		trajeto = self.__get_trajeto_rota(rota)
		rota = {"distancia": distancia, "trajeto": trajeto}
		return rota

	def get_coordenadas(self, endereco: str | dict):
		'''
			Devolve a coordenada de um endereço, seja ele
			estruturado ou não.
			https://developer.tomtom.com/geocoding-api/documentation/structured-geocode
		'''
		if isinstance(endereco, str):
			localizacao = self.__get_geocode(endereco)
		elif isinstance(endereco, dict):
			localizacao = self.__get_struct_geocode(endereco)
		else:
			raise ValueError("Formato da busca é inválido")
		if localizacao is None:
			return self.COORD_INVALIDA
		return localizacao
