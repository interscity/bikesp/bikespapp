'''
	Módulo responsável por gerenciar uma contestacão
'''

import sys
from pathlib import Path

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import contestacao_existe,dump_viagem, \
  get_pessoa_info_pelo_id_pessoa, grupo_pesquisa_existe, par_valido_existe

from servidor.bikespserver.bd.atualizacoes import atualiza_contestacao
from servidor.bikespserver.remuneracao.insere_remuneracao import insere_remuneracao_para_enviar
from servidor.bikespserver.viagem.validaviagem.reprocessa import cria_dict_viagem
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.utils.parsers import parse_dinheiro_float

#pylint: enable=wrong-import-position


# pylint: disable=too-many-locals,too-many-return-statements
def gerencia_contestacao(id_viagem: str | int,
                         aprovar: bool,
                         resposta: str,
                         cur: psycopg2.extensions.cursor,
                         valor=None) -> tuple[bool, str, float]:
	'''
		Atualiza a remuneração de uma viagem, caso a contestação
		seja aprovada
	'''
	sucesso = True
	status = "SUCESSO"
	remuneracao = 0.0
	existe = contestacao_existe(id_viagem, cur)

	if existe is None:
		return (False, "CONSTESTAÇÃO INVÁLIDA", remuneracao)

	id_viagem, _, _, aprovada, _ = existe

	if aprovada is not None:
		return (False, "CONTESTACAÇÃO JÁ GERENCIADA", remuneracao)

	viagem_dict = cria_dict_viagem(id_viagem, dump_viagem(id_viagem, cur))
	viagem_obj = Viagem.from_json(viagem_dict)
	viagem_obj.id_viagem = id_viagem

	id_pessoa = viagem_obj.id_usuario
	_, _, id_grupo, _ = get_pessoa_info_pelo_id_pessoa(id_pessoa, cur)

	if aprovar:
		if valor is None:
			if viagem_obj.id_origem is None or viagem_obj.id_destino is None:
				return (
				    False,
				    "VALOR FALTANDO PARA VIAGEM COM ORIGEM OU DESTINO DESCONHECIDO",
				    remuneracao)
			if viagem_obj.id_origem == viagem_obj.id_destino:
				return (False,
				        "VALOR FALTANDO PARA VIAGEM COM ORIGEM == DESTINO",
				        remuneracao)
			if id_grupo is None:
				return (False, "PESSOA SEM GRUPO PESQUISA", remuneracao)

			# Determina valor da viagem
			_, remuneracao_grupo = grupo_pesquisa_existe(id_grupo, cur)
			remuneracao_grupo = parse_dinheiro_float(remuneracao_grupo)
			_, _, distancia, _, = par_valido_existe(viagem_obj.id_origem,
			                                        viagem_obj.id_destino, cur)

			if distancia < 1000:
				return (False, "DISTANCIA MENOR QUE 1KM", remuneracao)

			remuneracao = (min(8000, distancia) / 1000) * remuneracao_grupo
		else:
			remuneracao = float(valor)

		# Atualiza viagem existente
		viagem_obj.status = "Aprovado"
		viagem_obj.motivo_reprovacao = "APROVADO"
		viagem_obj.remuneracao = remuneracao
		viagem_obj.salvar_no_bd(cur)

		# Atualiza bilheteunico
		insere_remuneracao_para_enviar(id_pessoa, remuneracao, cur)

	atualiza_contestacao(id_viagem, aprovar, resposta, cur)

	return (sucesso, status, remuneracao)
