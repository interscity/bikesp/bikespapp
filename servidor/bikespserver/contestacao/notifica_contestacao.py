'''
	Módulo responsável por notificar o resultado de uma contestação
	(aprovada ou reprovada)
'''

import sys
import os
from pathlib import Path
from datetime import datetime

import psycopg2
#import config  # pylint: disable=unused-import

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import contestacao_existe,\
 get_viagem_info, get_pessoa_info_pelo_id_pessoa
from servidor.bikespserver.email.envia_email import envia_email
#pylint: enable=wrong-import-position


def notifica_contestacao(id_viagem: str | int,
                         cur: psycopg2.extensions.cursor,
                         debug=False):
	'''
		Envia um email ao usuário informando o status da sua contestãção
	'''
	existe = contestacao_existe(id_viagem, cur)
	if existe is None:
		print("Viagem não contestada")
		return

	_, _, _, aprovada, resposta = existe
	if aprovada is None:
		print("Contestação não analisada")
		return

	viagem_info = get_viagem_info(id_viagem, cur)
	id_pessoa = viagem_info[0]
	data_viagem = viagem_info[1]

	pessoa_info = get_pessoa_info_pelo_id_pessoa(id_pessoa, cur)
	if pessoa_info is None:
		print("Pessoa inexistente")
		return

	email_pessoa = pessoa_info[1]
	nome_pessoa = pessoa_info[3]
	notifica_pessoa(email_pessoa,
	                nome_pessoa,
	                resposta,
	                aprovada,
	                id_viagem,
	                data_viagem,
	                debug=debug)


#pylint: disable=too-many-arguments
def notifica_pessoa(email_pessoa: str,
                    nome_pessoa: str,
                    resposta: str,
                    aprovada: bool,
                    id_viagem: str | int,
                    data_viagem: datetime,
                    debug=False):
	'''
		Notifica uma pessoa que sua remuneração foi alterada
	'''
	recebedor = [email_pessoa]

	print("Notificando " + email_pessoa)
	assunto = "[Piloto Bike SP] Contestação "
	if aprovada:
		assunto += "aprovada"
	else:
		assunto += "reprovada"

	data = data_viagem.strftime("%d/%m/%Y")
	body = (
	    f"Olá {nome_pessoa},<br><br>" +
	    f"Informamos que a sua contestação sobre a viagem <b>#{id_viagem}</b>, do dia {data} foi "
	    "<b>{status}</b>.<br><br>".format(
	        status="aprovada" if aprovada else 'reprovada') +
	    f'{f"Resposta da equipe do Bike SP: <br><i>{resposta}</i><br><br>" if (resposta is not None and resposta != "") else "" }'  #pylint: disable=line-too-long
	    "Atenciosamente,<br>"
	    "Equipe Piloto Bike SP")

	# pylint: disable=duplicate-code
	if not debug:
		user_mail = os.getenv("MAIL_USERNAME")
		senha_mail = os.getenv("MAIL_PASSWORD")
		envia_email(user_mail, senha_mail, recebedor, assunto, body)

	return {"recebedor": recebedor, "assunto": assunto, "body": body}
	# pylint: enable=duplicate-code
