# Dashboard de admin em HTMX

## Acesso ao dashboard

Para ter acesso ao dashboard, é necessário que o usuário tenha o campo 'admin' no banco com valor `TRUE`, para isso, basta rodar os seguintes comandos:

(Atente-se para o idpessoa, se foram adicionados mais usuários, esse pode não ser seu id)

```bash
devbox shell
devbox services start banco
psql -h localhost -U bikespuser -d bikespbd
UPDATE auth_bikesp.USUARIO SET admin = TRUE WHERE idpessoa = 1;
\q
devbox services stop banco
```

### Acesso a partir de banco de dados antigo

Se é a sua primeira vez acessando o painel para desenvolvimento e você configurou o projeto localmente antes do merge de autenticação do painel, é preciso adicionar uma nova coluna à tabela `auth_bikesp.USUARIO` antes de conceder-se acesso com os comandos acima:

``` bash
devbox shell
devbox services start banco
psql -h localhost -U bikespuser -d bikespbd
ALTER TABLE auth_bikesp.USUARIO
	ADD COLUMN admin BOOLEAN DEFAULT FALSE;
\q
devbox services stop banco
```

## Intro HTMX

Para a construção da dashboard de admin, utilizamos uma biblioteca chamada [HTMX](https://htmx.org/), que adiciona novas funcionalidade diretamente no HTML (através de novos atributos), permitindo a construção de interfaces de usuário dinâmicas e modernas sem a necessidade de instalar uma biblioteca complexa de JavaScript e lidar com várias dependências simultaneamente.

HTMX é uma biblioteca do lado do cliente muito simples e poderosa. Vale a pena dar uma olhada rápida na [documentação](https://htmx.org/docs/).

## Instalação do HTMX

A biblioteca HTMX é invocada através de uma tag `<script>` no header da página. Basta inserí-la no template desejado. No painel de administrador, esta tag se encontra no layout `base.html`, utilizado como layout fundamental para todas as páginas do painel. Ou seja, o HTMX está ativado em todas as páginas da dashboard de administração.

O arquivo do script minificado é servido pelo nosso servidor como `static`. Desta forma, não dependemos do acesso ao CDN.

```html
<script src="{{ url_for ('bikesp.static', filename='htmx.min.js')}}"></script>
<script src="{{ url_for ('bikesp.static', filename='response-targets.js')}}"></script>
```

## Dependências

Há apenas uma dependência: a extensão [response-targets](https://htmx.org/extensions/response-targets/) do htmx, que permite trocar o alvo das transclusões geradas pelo htmx em caso de erro na resposta do servidor. Por padrão, o HTMX só efetiva a ação de `swap` se o servidor devolver status 200. Esta extensão permite definir um comportamento específico em caso de erro nas respostas às requisições.

## Estrutura

A estrutura do front-end é simples.

### Templates

Utilizamos o motor de templates já embutido no Flask: [Jinja2](https://jinja.palletsprojects.com/en/stable/templates/)

Os templates para a dashboard de admin estão localizados em `servidor/bikespserver/templates/admin`, divididos nas seguintes pastas:
- `layouts`: estruturas periféricas comuns a diferentes páginas, como estilos, scripts, menus de navegação etc;
- `pages`: as páginas de navegação propriamente, como signin, home etc;
- `partials`: componentes em HTML que podem ser reutilizados ou devolvidos para swap do HTMX.

O template é renderizado no código em Flask através da função `render_template`, em que se passa o `path` para o template, bem como os parâmetros necessários para o template específico.

### Estilos

Para a estilização, utilizamos CSS puro. Nenhuma biblioteca de estilos foi utilizada.

Foi aplicado um `reset` para normalizar os estilos do CSS para garantir o mesmo comportamento em diferentes navegadores. O reset está contido em: `servidor/bikespserver/static/reset.css`. 

Os estilos globais estão contidos em `servidor/bikespserver/static/base.css`. Aqui foram adicionados apenas algumas definições gerais.

Para estilos específicos em cada componente, em vez de guardarmos as regras de CSS em arquivos separados e também para evitar que estas regras impactem de forma indesejada outros componentes, utilizamos a diretiva [`@scope`](https://developer.mozilla.org/en-US/docs/Web/CSS/@scope) dentro de uma tag `<style>` como primeira "filha" do elemento HTML que se deseja estilizar, dentro dos próprios templates. Esta diretiva ainda se encontra em estado experimental no Firefox, portanto deve ser ativada acessando `about:config` e definindo a regra `layout.css.at-scope.enabled` como `true`. Em breve estará habilitada por padrão. Se não está acostumado com esta diretiva, recomenda-se que olhe a documentação do MDN.

### Scripts

O uso de HTMX reduz consideravelmente o uso de JavaScript personalizado. Pode-se aplicar scripts de duas maneiras: global ou localizada.

Para inserir scripts globalmente, basta criar um arquivo de extensão `.js` na pasta `servidor/bikespserver/static`, e incluí-lo no HTML através de uma tag `<script>`.

Porém, caso queira manter o princípio de localidade de comportamento ([LoC](https://htmx.org/essays/locality-of-behaviour/)), pode-se optar por escrever scripts "inline", dentro dos próprios templates, desde que não manipulem estruturas externas ao template em que estão contidos.

## Observações

Cabe explicar neste documento o funcionamento de algumas partes específicas do código.

### Funcionamento do toaster

O funcionamento do toaster se dá através de dois componentes: `partials/toaster.html` e `partials/toast.html`.

O toaster é o `div` que serve como contêiner para os toasts, que são as mensagens de sucesso ou erro.

Dentro do toaster, há um script inline que define que todo elemento que for inserido no toaster por um [out-of-band swap](https://htmx.org/attributes/hx-swap-oob/) irá desaparecer e em seguida será removido do DOM.

```html
<script>
	htmx.on("htmx:oobAfterSwap", (evt) => {
		const toaster = htmx.find('#toaster')
		const toastFadeTime = 2000

		if (evt.detail.target == toaster) {
			const lastChild = htmx.find('#toaster > *:last-child')

			// // Fade toast out
			setTimeout(() => {
				lastChild.classList.add('fade-out')
			}, toastFadeTime)

			// Remove toast from DOM
			setTimeout(() => {
				htmx.remove(lastChild, toastFadeTime + 1000)
			})
		}
	})
</script>
```

O toast, por sua vez, tem o atributo `hx-swap-oob="beforeend:#toaster"`. Isto significa que, sempre que o componente de toast estiver contido em uma resposta do servidor, o cliente irá inserí-lo como último filho do elemento de id `toaster`. Ou seja, será inserido dentro do toaster através de um out-of-band swap. Se não estiver acostumado com este conceito, sugiro consultar a [documentação](https://htmx.org/docs/#oob_swaps)
