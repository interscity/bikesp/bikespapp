'''
	Main do servidor
'''
import os
import sys
import logging
from datetime import datetime, timedelta
from typing import Any
from threading import Thread
from pathlib import Path
import io
import smtplib
import json

from werkzeug.middleware.proxy_fix import ProxyFix
from flask import (Flask, request, abort, render_template, make_response,
                   redirect, jsonify, Response, Blueprint)
from flask.logging import default_handler

from flask_jwt_extended import JWTManager
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mail import Mail
from flask_vite import Vite
from flask_basicauth import BasicAuth

import psycopg2.extras
#import config

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.endpoints.registra_viagem import registra_viagem
from servidor.bikespserver.autenticacao.verifica_admin import verifica_admin
from servidor.bikespserver.endpoints.contesta_viagem import contesta_viagem
from servidor.bikespserver.endpoints.cadastra_usuario import cadastra_usuario
from servidor.bikespserver.endpoints.checa_sessao import checa_sessao
from servidor.bikespserver.endpoints.login import login
from servidor.bikespserver.endpoints.logout import logout
from servidor.bikespserver.endpoints.sincroniza_viagens import sincroniza_viagens
from servidor.bikespserver.endpoints.sincroniza_localizacoes import sincroniza_localizacoes
from servidor.bikespserver.endpoints.viagens_faltantes import viagens_faltantes
from servidor.bikespserver.endpoints.esqueceu_senha import esqueceu_senha, \
 redefine_senha, redefine_senha_get
from servidor.bikespserver.endpoints.troca_bu import requisita_troca_bu, redefine_bu
from servidor.bikespserver.endpoints.gera_extrato import gera_extrato
from servidor.bikespserver.endpoints.envia_pares_validos import envia_pares_validos
from servidor.bikespserver.endpoints.exibe_bonus_usuario import exibe_bonus_usuario
from servidor.bikespserver.endpoints.exibe_contestacoes_usuario import exibe_contestacoes_usuario
from servidor.bikespserver.endpoints.valida_localizacoes import valida_localizacoes
from servidor.bikespserver.endpoints.altera_localizacao import checa_alteracao_localizacao, \
 altera_localizacao
from servidor.bikespserver.endpoints.notificacoes_push import registra_token_notificacao
from servidor.bikespserver.viagem.visualiza_viagem import visualiza_viagem
from servidor.bikespserver.autenticacao import gerencia_sessao
from servidor.bikespserver.feedback.gerencia_feedback_viagem import registra_feedback_viagem
from servidor.bikespserver.feedback.gerencia_feedback_viagem_perdida \
 import registra_feedback_viagem_perdida
from servidor.bikespserver.contestacao.gerencia_contestacao import gerencia_contestacao
from servidor.bikespserver.utils.validadores import valida_integridade_request
from servidor.bikespserver.utils import log
from servidor.tests.utils.bd import reseta_bd_teste

from servidor.bikespserver.decorators.valida_request import valida_request
from servidor.bikespserver.decorators.valida_token import valida_token, valida_token_admin
from servidor.bikespserver.apis.fcm import envia_notificacoes
from servidor.bikespserver.email.envia_email import envia_email_flask

from servidor.bikespserver.gerenciamento.bonus.cria_bonus import cria_bonus
from servidor.bikespserver.gerenciamento.bonus.concede_bonus import concede_bonus
from servidor.bikespserver.bd.consultas import get_pessoas, get_pessoas_pagina, \
 get_bonus, get_bonus_pagina, get_contestacoes_pagina, get_contestacao, get_viagens_pagina, \
 get_trajeto_viagem, get_token_notificacao, get_email_por_id, get_info_pessoas_coortes, \
 get_tokens_notificacao_todos, get_tokens_notificacao_coorte, get_todas_localizacoes_pessoa

from servidor.etl.envio_sptrans.gera_csv_envio import gera_df_envio
from servidor.bikespserver.utils.seguranca import encripta
from servidor.bikespserver.utils.insercao import inserir_usuarios
#pylint: enable=wrong-import-position


def create_app(testing=False, test_config=None):  #pylint: disable=too-many-locals,too-many-statements
	'''
		Inicializa o servidor flask
	'''

	# create and configure the app
	bprint = Blueprint("bikesp",
	                   __name__,
	                   template_folder="templates",
	                   static_folder="static",
	                   url_prefix=os.getenv("SERVER_PREFIX", ""))

	app = Flask(__name__,
	            instance_relative_config=True,
	            static_url_path=os.getenv("SERVER_PREFIX", "") + "/static")

	# Configura o logger do app
	log_handler = logging.StreamHandler()
	log_handler.setLevel(logging.INFO)
	log_handler.setFormatter(log.CustomFormatter())

	app.logger.setLevel(logging.INFO)
	app.logger.removeHandler(default_handler)
	app.logger.addHandler(log_handler)

	app.config.from_mapping(SECRET_KEY=os.getenv("SECRET_KEY_FLASK"),
	                        BD_HOST=os.getenv("BD_HOST"),
	                        BD_NOME=os.getenv("BD_NOME"),
	                        BD_USUARIO=os.getenv("BD_USUARIO"),
	                        BD_SENHA=os.getenv("BD_SENHA"),
	                        BD_PORT=os.getenv("BD_PORT", "5432"),
	                        BASIC_AUTH_USERNAME=os.getenv("ADM_USER"),
	                        BASIC_AUTH_PASSWORD=os.getenv("ADM_PASS"),
	                        PREFERRED_URL_SCHEME=os.getenv("SCHEME", "http"),
	                        TEMPLATES_AUTO_RELOAD=True)

	app.config['MAIL_SERVER'] = 'smtp.gmail.com'
	app.config['MAIL_PORT'] = 465
	app.config['MAIL_USE_TLS'] = False
	app.config['MAIL_USE_SSL'] = True
	app.config['MAIL_USERNAME'] = os.getenv("MAIL_USERNAME")
	app.config['MAIL_DEFAULT_SENDER'] = os.getenv("MAIL_USERNAME")
	app.config['MAIL_PASSWORD'] = os.getenv("MAIL_PASSWORD")

	app.config['MAX_CONTENT_LENGTH'] = 500000

	if app.debug:
		print("UTILIZANDO BD DE TESTE")
		app.config.update({
		    "BD_HOST": os.getenv("BD_HOST"),
		    "BD_NOME": os.getenv("BD_NOME_TESTE"),
		    "BD_USUARIO": os.getenv("BD_USUARIO_TESTE"),
		    "BD_SENHA": os.getenv("BD_SENHA_TESTE"),
		})
		os.environ["INICIO_PILOTO_TIMESTAMP"] = "1000"
		os.environ["MIN_VERSAO_APP"] = ""

	if not app.debug:
		app.wsgi_app = ProxyFix(app.wsgi_app,
		                        x_for=1,
		                        x_proto=1,
		                        x_host=1,
		                        x_prefix=1)

	if test_config is None:
		# load the instance config, if it exists, when not testing
		app.config.from_pyfile('config.py', silent=True)
	else:
		# load the test config if passed in
		app.config.from_mapping(test_config)

	limiter = create_limiter(app, testing)

	# ensure the instance folder exists
	try:
		os.makedirs(app.instance_path)
	except OSError:
		pass

	jwt = JWTManager(app)  # pylint: disable=unused-variable

	flask_mail = Mail(app)
	vite = Vite(app)  # pylint: disable=unused-variable

	basic_auth = BasicAuth(app)

	# Checa se integridade da request é valida antes de processá-la
	@bprint.before_request
	def checa_validacao_integridade():
		teste = False
		if ("TESTING" in app.config and app.config["TESTING"]) or app.debug:
			teste = True

		if not teste and request.endpoint in ["_registra_viagem"]:
			if not valida_integridade_request(request.form,
			                                  os.getenv("CHAVE_INTEGRIDADE")):
				return ({'estado': "Erro", 'erro': "Corrompido"}, 400)

		return None

	@limiter.request_filter
	def ip_whitelist():
		return request.remote_addr == "127.0.0.1"

	################## ROTAS DO SERVIDOR ##################

	# Índíce
	@bprint.route('/')
	@limiter.exempt
	def menu():
		return 'Bem vindo à página principal. Nada para você por aqui'

	# Endpoint para registrar viagens
	@bprint.route('/api/regViagem/', methods=['POST'])
	@valida_request(["user", "token", "viagem"], 500000)
	@limiter.limit("30 per day;5/second")
	@valida_token
	def _registra_viagem(con, cur, info_pessoa):
		return close_connection(registra_viagem(con, cur, info_pessoa), con,
		                        cur)

	# Endpoint para cadastrar usuários
	@bprint.route('/api/cadastrar/', methods=['POST'])
	@valida_request(["bu", "cpf", "email", "senha"], 200)
	def _cadastra_usuario():
		return cadastra_usuario()

	# Endpoint para efetuar o login de um usuario
	@bprint.route('/api/login/', methods=['POST'])
	@valida_request(["email", "senha"], 200)
	def _login_usuario():
		return login()

	@bprint.route('/admin/signin', methods=['GET', 'POST'])
	def _admin_signin():  # pylint: disable=unused-argument
		if request.method == 'GET':
			return render_template('admin/pages/signin.html')

		# if request.method == 'POST':
		request.form = request.form.copy()
		request.form['senha'] = encripta(request.form['senha'],
		                                 os.getenv("ENCRIPT_PK"))
		admin, id_pessoa = verifica_admin()

		if not admin:
			if id_pessoa is not None:
				app.logger.warning(
				    "Usuário não autorizado %s tentou acessar o painel de administração",
				    id_pessoa)
			return render_template('admin/partials/toast.html',
			                       message='usuário não autorizado',
			                       type="error"), 401

		result, status_code = login()

		if result['estado'] == 'Erro':
			return render_template('admin/partials/toast.html',
			                       message=result['erro'],
			                       type="error"), status_code

		rendered_template = render_template(
		    'admin/partials/toast.html',
		    message=
		    "Autenticado com sucesso. Redirecionando para o painel de administração...",
		    type='success')
		response = make_response(rendered_template, status_code)
		response.headers['HX-Redirect'] = '/admin/dashboard'
		response.set_cookie('token', result['token'])
		response.set_cookie('user', result['CPF'])
		return response

	@bprint.route('/admin/signout', methods=['GET'])
	def _admin_signout():
		response = make_response(redirect('/admin/signin'))
		response.set_cookie('token', '', expires=0)
		response.set_cookie('user', '', expires=0)
		return response

	@bprint.route('/admin/dashboard', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/layouts/dashboard.html')

	@bprint.route('/admin/dashboard/inicio', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_inicio(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/inicio.html')

	@bprint.route('/admin/dashboard/bonus', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_bonus(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/bonus.html')

	@bprint.route('/admin/dashboard/remuneration', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_remuneration(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/remuneration.html')

	@bprint.route('/admin/dashboard/relatorios', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_relatorios(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/relatorios.html')

	@bprint.route('/admin/dashboard/viagens', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_viagens(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/viagens.html')

	@bprint.route('/admin/dashboard/notificacoes', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_notificacoes(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/notificacoes.html')

	@bprint.route('/admin/dashboard/contestacao', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_contestacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/contestacao.html')

	@bprint.route('/admin/dashboard/localizacao', methods=['GET'])
	@valida_token_admin
	def _admin_dashboard_localizacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/localizacao.html')

	@bprint.route('/admin/dashboard/insercaousuarios', methods=["GET", "POST"])
	@valida_token_admin
	def _admin_dashboard_insercao_usuarios(con, cur, info_pessoa):  # pylint: disable=unused-argument
		return render_template('admin/pages/insercaousuarios.html')

	@bprint.route('/admin/insercao_usuarios', methods=["POST"])
	@valida_token_admin
	def _admin_insercao_usuarios(con, cur, info_pessoa):  # pylint: disable=unused-argument
		retorno = []
		erro = []
		thread = Thread(target=inserir_usuarios, args=(app, retorno, erro))
		thread.start()
		thread.join()
		message = retorno[0]
		toast_type = "success" if erro[0] is False else "error"
		return render_template('admin/partials/toast.html',
		                       message=message,
		                       type=toast_type)

	@bprint.route('/admin/feedback_insercao', methods=["GET"])
	@valida_token_admin
	def _admin_feedback_insercao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		message = "Usuários novos estão sendo inseridos!"
		return render_template('admin/partials/toast.html',
		                       message=message,
		                       type="success")

	@bprint.route('/admin/cria_bonus', methods=['POST'])
	@valida_token_admin
	def _admin_cria_bonus(con, cur, info_pessoa):  # pylint: disable=unused-argument
		data = request.form

		if data.get('date-start'):
			data_inicio = data.get('date-start')
		else:
			data_inicio = datetime.now()

		if data.get('date-end'):
			data_fim = data.get('date-end')
		else:
			data_fim = None

		dict_insercao_bonus = {
		    "nome": data.get('name'),
		    "valor": float(data.get('amount', 0.0)),
		    "ativo": bool(data.get('active')),
		    "visivel": bool(data.get('visible')),
		    "descricao": data.get('description'),
		    "data_inicio": data_inicio,
		    "data_fim": data_fim
		}

		[sucesso, status, id_novo_bonus] = cria_bonus(dict_insercao_bonus, cur)

		if sucesso:
			con.commit()
			message = "Bônus \"" + data.get(
			    'name',
			    "") + "\" (id: " + str(id_novo_bonus) + ") criado com sucesso."
			app.logger.info("%s", message)
			rendered_toast = render_template('admin/partials/toast.html',
			                                 message=message,
			                                 type="success")
			rendered_bonus_update = render_template(
			    'admin/partials/bonus_table_update.html')
			response = make_response(rendered_bonus_update + rendered_toast,
			                         200)
		else:
			message = "Erro ao criar bônus \"" + data.get('name',
			                                              "") + "\": " + status
			app.logger.error("%s", message)
			response = make_response(
			    render_template('admin/partials/toast.html',
			                    message=message,
			                    type="error"), 400)
		con.close()

		return response

	@bprint.route('/admin/concede_bonus', methods=['POST'])
	@valida_token_admin
	def _admin_concede_bonus(con, cur, info_pessoa):  # pylint: disable=unused-argument
		data = request.form

		if data.get('date'):
			data_concessao = data.get('date')
		else:
			data_concessao = datetime.now()

		dict_insercao_pessoabonus = {
		    "idPessoa": data.get('pessoa-id'),
		    "idBonus": data.get('bonus-id'),
		    "dataConcessao": data_concessao,
		}

		[sucesso, status] = concede_bonus(dict_insercao_pessoabonus, cur)

		if sucesso:
			con.commit()
			message = "Bônus de id " + data.get(
			    'bonus-id', "") + " concedido a pessoa de id " + data.get(
			        'pessoa-id', "") + " com sucesso."
			app.logger.info("%s", message)
			response = make_response(
			    render_template('admin/partials/toast.html',
			                    message=message,
			                    type="success"), 200)
		else:
			message = "Erro ao conceder bônus: " + status
			app.logger.error("%s", message)
			response = make_response(
			    render_template('admin/partials/toast.html',
			                    message=message,
			                    type="error"), 400)

		con.close()

		return response

	@bprint.route('/admin/getpessoas', methods=['GET'])
	@valida_token_admin
	def _admin_getpessoas(con, cur, info_pessoa):  # pylint: disable=unused-argument
		pagina = int(request.args.get('pagina', 0))

		busca = request.args.get('busca', "")

		cursor = con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		limite = 5
		pessoas = get_pessoas_pagina(pagina, cursor, limite=limite,
		                             busca=busca) or []
		ultima_pagina = len(pessoas) < limite
		return render_template('admin/partials/people_table.html',
		                       headers=["ID", "Nome", "E-mail", "CPF"],
		                       pessoas=pessoas,
		                       pagina=pagina,
		                       ultima_pagina=ultima_pagina)

	@bprint.route('/admin/getbonus', methods=['GET'])
	@valida_token_admin
	def _admin_getbonus(con, cur, info_pessoa):  # pylint: disable=unused-argument
		pagina = int(request.args.get('pagina', 0))

		busca = request.args.get('busca', "")

		cursor = con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		limite = 5
		bonus = get_bonus_pagina(pagina, cursor, limite=limite,
		                         busca=busca) or []
		ultima_pagina = len(bonus) < limite
		return render_template('admin/partials/bonus_table.html',
		                       headers=[
		                           "ID", "Nome", "Valor", "Ativo?", "Visível?",
		                           "Descrição", "Data de início",
		                           "Data de término"
		                       ],
		                       bonus=bonus,
		                       pagina=pagina,
		                       ultima_pagina=ultima_pagina)

	@bprint.route("/admin/gerar_historico", methods=["GET", "POST"])
	def _admin_gerar_historico():

		try:

			df = gera_df_envio()
			csv_buffer = io.StringIO()
			df.to_csv(csv_buffer, index=False)
			csv_buffer.seek(0)

			return Response(csv_buffer.getvalue(),
			                mimetype="text/csv",
			                headers={
			                    "Content-Disposition":
			                        "attachment;filename=historico.csv"
			                })
		except Exception as exception:  # pylint: disable=broad-exception-caught
			return jsonify({"error": str(exception)}), 500

	@bprint.route('/admin/getcontestacao', methods=['GET'])
	@valida_token_admin
	def _admin_getcontestacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		id_viagem = request.args.get('idviagem', "")
		cursor = con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		contestacao = get_contestacao(id_viagem, cursor)

		# Ajustes para mostrar os dados (data, aprovada)
		contestacao['data'] = contestacao['data'].strftime("%d/%m/%y")
		contestacao[
		    'aprovada'] = 'Sim' if contestacao['aprovada'] is True else (
		        'Não' if contestacao['aprovada'] is False else 'Pendente')
		contestacao['resposta'] = contestacao['resposta'] if contestacao[
		    'resposta'] is not None else ""
		return render_template('admin/partials/contestacao_detalhe.html',
		                       contestacao=contestacao)

	@bprint.route('/admin/listar_contestacoes_pendentes', methods=['GET'])
	@valida_token_admin
	def _admin_listar_contestacoes_pendentes(con, cur, info_pessoa):  # pylint: disable=unused-argument
		pagina = int(request.args.get('pagina', 0))
		busca = request.args.get('busca', "")

		cursor = con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		limite = 5
		contestacoes = get_contestacoes_pagina(
		    pagina, cursor, limite=limite, busca=busca) or []
		ultima_pagina = len(contestacoes) < limite

		for contestacao in contestacoes:
			contestacao['data'] = contestacao['data'].strftime('%d/%m/%y')
			contestacao[
			    'aprovada'] = 'Sim' if contestacao['aprovada'] is True else (
			        'Não' if contestacao['aprovada'] is False else 'Pendente')

		return render_template(
		    'admin/partials/contestacoes_table.html',
		    headers=["ID", "Data", "Justificativa", "Aprovada"],
		    contestacoes=contestacoes,
		    pagina=pagina,
		    ultima_pagina=ultima_pagina)

	@bprint.route('/admin/get_botao_contestacao', methods=['GET'])
	@valida_token_admin
	def _admin_get_botao_contestacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		id_viagem = request.args.get('idviagem')
		resposta = request.args.get('resposta')
		aprovar_str = request.args.get('aprovar')
		aprovar_bool = aprovar_str == 'true'
		return render_template('admin/partials/botao_contestacao.html',
		                       aprovar_str=aprovar_str,
		                       aprovar_bool=aprovar_bool,
		                       id_viagem=id_viagem,
		                       resposta=resposta)

	@bprint.route('/admin/gerencia_contestacao', methods=['POST'])
	@valida_token_admin
	def _admin_gerencia_contestacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		data = request.form
		id_viagem = data.get('idviagem')
		aprovar = data.get('aprovar') == 'true'
		resposta = data.get('resposta')

		sucesso, status, remuneracao = gerencia_contestacao(id_viagem=id_viagem,
		                                                    aprovar=aprovar,
		                                                    resposta=resposta,
		                                                    cur=cur)

		if sucesso:
			con.commit()
			if aprovar:
				message = f"Contestação da viagem {id_viagem} aprovada com remuneração de {remuneracao:.2f}."
			else:
				message = f"Contestação da viagem {id_viagem} foi reprovada."
			app.logger.info("%s", message)
			rendered_toast = render_template('admin/partials/toast.html',
			                                 message=message,
			                                 type="success")

			contestacao = get_contestacao(
			    id_viagem,
			    con.cursor(cursor_factory=psycopg2.extras.RealDictCursor))
			# Ajustes para mostrar os dados (data, aprovada)
			contestacao['data'] = contestacao['data'].strftime("%d/%m/%y")
			contestacao[
			    'aprovada'] = 'Sim' if contestacao['aprovada'] is True else (
			        'Não' if contestacao['aprovada'] is False else 'Pendente')
			rendered_contestacao_update = render_template(
			    'admin/partials/contestacao_table_update.html',
			    contestacao=contestacao)
			response = make_response(
			    rendered_contestacao_update + rendered_toast, 200)
		else:
			message = f"Erro ao gerenciar contestação: {status}"
			app.logger.error("%s", message)
			response = make_response(
			    render_template('admin/partials/toast.html',
			                    message=message,
			                    type="error"), 400)

		con.close()
		return response

	@bprint.route('/admin/getviagens', methods=['GET'])
	@valida_token_admin
	def _admin_getviagens(con, cur, info_pessoa):  # pylint: disable=unused-argument
		pagina = int(request.args.get('pagina', 0))
		busca = request.args.get('busca', "")

		cursor = con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		limite = 5

		viagens = get_viagens_pagina(pagina, cursor, limite=limite,
		                             busca=busca) or []
		ultima_pagina = len(viagens) < limite
		return render_template('admin/partials/viagem_table.html',
		                       headers=[
		                           "ID Viagem", "ID Usuário", "Data",
		                           "Deslocamento", "ID Origem", "ID Destino",
		                           "Status", "Remuneracao"
		                       ],
		                       viagens=viagens,
		                       pagina=pagina,
		                       ultima_pagina=ultima_pagina)

	@bprint.route('/admin/gera_mapa', methods=['GET'])
	@valida_token_admin
	def _admin_gera_mapa(con, cur, info_pessoa):  # pylint: disable=unused-argument
		id_viagem = request.args.get('idviagem')
		trajeto = get_trajeto_viagem(id_viagem, cur)[0]
		trajeto_json = json.dumps(trajeto)
		return render_template('admin/partials/mapa_viagem.html',
		                       trajeto=trajeto_json,
		                       id=id_viagem)

	@bprint.route('/admin/feedbacknotificacao', methods=["GET"])
	@valida_token_admin
	def _admin_feedback_notificacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		message = "As notificações estão sendo processadas"
		return render_template('admin/partials/toast.html',
		                       message=message,
		                       type="success")

	@bprint.route("/admin/cria_notificacao", methods=['POST'])
	@valida_token_admin
	def _admin_cria_notificacao(con, cur, info_pessoa):  # pylint: disable=unused-argument, too-many-locals
		data = request.form

		mensagem = data.get("mensagem")
		titulo = data.get("titulo", "Notificação")
		recipient_type = data.get("recipient_type")
		ids = list(data.get("ids").split(","))
		push = data.get("push") == "true"
		email = data.get("email") == "true"

		if not all(id.isdigit() for id in ids):
			message = "Todos os elementos de 'ids' devem ser números"
			return make_response(
			    render_template('admin/partials/toast.html',
			                    message=message,
			                    type="error"), 400)

		if not push and not email:
			message = "É necessário selecionar um tipo de envio"
			return make_response(
			    render_template('admin/partials/toast.html',
			                    message=message,
			                    type="error"), 400)

		destinatarios_push = []
		destinatarios_email = []

		if recipient_type == "all":
			ids = ""
			destinatarios_push.extend(get_tokens_notificacao_todos(cur) or [])
			destinatarios_email.extend(
			    [pessoa[7] for pessoa in get_pessoas(cur)] or [])

		elif recipient_type == "group":
			destinatarios_push.extend(
			    get_tokens_notificacao_coorte(ids, cur) or [])
			emails = [
			    pessoa[1]
			    for pessoa in get_info_pessoas_coortes(ids, cur) or []
			]
			destinatarios_email.extend(emails)

		elif recipient_type == "user":
			lista_push = get_token_notificacao(ids, cur) or []
			lista_push = [lista_push
			             ] if not isinstance(lista_push, list) else lista_push
			destinatarios_push.extend(lista_push)
			destinatarios_email.extend(get_email_por_id(ids, cur) or [])

		if push and destinatarios_push:
			status = envia_notificacoes(destinatarios_push, titulo, mensagem)[1]
			if status != 200:
				app.logger.error("Erro ao enviar notificação push.")
				return make_response(
				    render_template('admin/partials/toast.html',
				                    message="Erro ao enviar notificação push.",
				                    type="error"), 400)

		if email and destinatarios_email:
			envia_email_flask(flask_mail=flask_mail,
			                  assunto=titulo,
			                  html=mensagem,
			                  enviador=os.getenv("MAIL_USERNAME"),
			                  recebedores=destinatarios_email)

		modalidades = "Push e Email" if push and email else (
		    "Push" if push else "Email")
		if (destinatarios_push or destinatarios_email):
			message = f"Notificação enviada com sucesso! Modalidade(s): {modalidades}."
		else:
			message = "Nenhum destinatário válido para envio de notificação."

		app.logger.info("%s | Título: %s | Destinatários(%s): %s ", message,
		                titulo, recipient_type, ids)

		return make_response(
		    render_template('admin/partials/toast.html',
		                    message=message,
		                    type="success"), 200)

	@bprint.route('/admin/get_localizacao', methods=['GET'])
	@valida_token_admin
	def _admin_get_localizacao(con, cur, info_pessoa):  # pylint: disable=unused-argument
		busca = request.args.get('busca', "")
		if not busca:
			return make_response('', 204)

		cursor = con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		localizacoes = get_todas_localizacoes_pessoa(busca, cursor)
		resposta = dias = None

		if localizacoes:
			retorno = checa_alteracao_localizacao(cursor, [busca])
			resposta = retorno[0]['resposta']
			dias = None if resposta else retorno[0]['dias']

		return render_template(
		    'admin/partials/localizacao_table.html',
		    headers=["ID Localizacao", "Endereço", "Coordenadas", "Tipo"],
		    localizacoes=localizacoes,
		    resposta=resposta,
		    dias=dias)

	# Endpoint para testar se uma sessão é válida
	@bprint.route('/api/checaSess/', methods=['POST'])
	@valida_request(["email", "token"], 500)
	def _checa_sessao():
		return checa_sessao()

	# Endpoint para efetuar o logout de um usuario
	@bprint.route('/api/logout/', methods=['POST'])
	@valida_request(["email", "token"], 500)
	def _logout_usuario():
		return logout()

	@bprint.route('/api/contestaViagem/', methods=['POST'])
	@limiter.limit("50 per day")
	@valida_request(
	    ["user", "token", "contestacao"],
	    1000,
	    tipo="json",
	    chaves_json=["contestacao"],
	    chaves_aninhadas_obrigatorias=[["data", "idViagem", "justificativa"]])
	@valida_token
	def __contesta_viagem(con, cur, info_pessoa):
		return close_connection(contesta_viagem(con, cur, info_pessoa), con,
		                        cur)

	@bprint.route('/api/sincronizaViagens/', methods=['POST'])
	@limiter.limit("100 per day", override_defaults=True)
	@valida_request(["user", "token"], 500)
	@valida_token
	def __sincroniza_viagens(con, cur, info_pessoa):
		return close_connection(sincroniza_viagens(cur, info_pessoa), con, cur)

	@bprint.route('/api/viagensFaltantes/', methods=['POST'])
	@limiter.limit("500 per day", override_defaults=True)
	@valida_request(["user", "token", "idsViagens"], 10000)
	@valida_token
	def __viagens_faltantes(con, cur, info_pessoa):
		return close_connection(viagens_faltantes(cur, info_pessoa), con, cur)

	@bprint.route('/adm/visualizaViagem/<int:id_viagem>', methods=['GET'])
	@basic_auth.required
	def _visualiza_viagem(id_viagem):
		return visualiza_viagem(id_viagem)

	@bprint.route("/api/geraExtrato/", methods=['POST'])
	@valida_request(["user", "token"], 501)
	@valida_token
	def __gera_extrato(con, cur, info_pessoa):
		return close_connection(gera_extrato(cur, info_pessoa), con, cur)

	@bprint.route("/api/recebeParesValidos/", methods=['POST'])
	@valida_request(["user", "token"], 500)
	@valida_token
	def __pares_validos(con, cur, info_pessoa):
		return close_connection(envia_pares_validos(cur, info_pessoa), con, cur)

	@bprint.route("/api/exibeBonus/", methods=['POST'])
	@valida_request(["user", "token"], 500)
	@valida_token
	def __exibe_bonus_usuario(con, cur, info_pessoa):
		return close_connection(exibe_bonus_usuario(cur, info_pessoa), con, cur)

	@bprint.route("/api/exibeContestacoes/", methods=['POST'])
	@valida_request(["user", "token"], 500)
	@valida_token
	def __exibe_contestacoes_usuario(con, cur, info_pessoa):
		return close_connection(exibe_contestacoes_usuario(cur, info_pessoa),
		                        con, cur)

	@bprint.route('/api/sincronizaLocs/', methods=['POST'])
	@valida_request(["user", "token"], 500)
	@valida_token
	def __sincroniza_localizacoes(con, cur, info_pessoa):
		return close_connection(sincroniza_localizacoes(cur, info_pessoa), con,
		                        cur)

	@bprint.route('/api/validaLocs/', methods=['POST'])
	def __valida_localizacoes():
		return valida_localizacoes()

	@bprint.route('/api/alteraLocalizacao/checa/', methods=['POST'])
	@valida_request(["user", "token"], 500)
	@valida_token
	def __checa_alteracao_localizacao(con, cur, info_pessoa):
		return close_connection(checa_alteracao_localizacao(cur, info_pessoa),
		                        con, cur)

	@bprint.route('/api/alteraLocalizacao/', methods=['POST'])
	@valida_request(
	    ["user", "token", "id_localizacao", "novo_endereco", "novo_apelido"],
	    1000)
	@valida_token
	def __altera_localizacao(con, cur, info_pessoa):
		return close_connection(altera_localizacao(con, cur, info_pessoa), con,
		                        cur)

	# Registra um token de push notifications para um usuário
	@bprint.route("/api/notificacoes/regToken/", methods=['POST'])
	@valida_request(["user", "token", "tokenNotif"], 700)
	@valida_token
	def __registra_push_token(con, cur, info_pessoa):
		return close_connection(
		    registra_token_notificacao(con, cur, info_pessoa), con, cur)

	@bprint.route("/esqueceuSenha/")
	@limiter.limit("1/minute;5 per day",
	               override_defaults=True,
	               deduct_when=lambda response: response.status_code == 200)
	@valida_request(["email"], 500, tipo="args")
	def __esqueceu_senha():
		testing = "TESTING" in app.config and app.config["TESTING"]
		return esqueceu_senha(flask_mail, testing=testing)

	@bprint.route("/esqueceuSenha/redefinirSenha/", methods=['GET', 'POST'])
	@valida_request(["nova_senha", "token", "confirma_senha"],
	                600,
	                methods=["POST"])
	def __redefinir_senha():
		testing = "TESTING" in app.config and app.config["TESTING"]

		if request.method == "GET":
			return redefine_senha_get()
		if testing:
			return redefine_senha(testing=testing)

		resposta = redefine_senha(testing=testing)[0]
		if resposta["estado"] == "Erro":
			return render_template('tela_resultado.html',
			                       titulo="Redefinindo Senha",
			                       msgErro="Erro: " + resposta["erro"])

		return render_template('tela_resultado.html',
		                       titulo="Redefinindo Senha",
		                       msgErro="",
		                       msgSucesso="Senha Redefinida com Sucesso!")

	@bprint.route("/trocaBU/")
	@limiter.limit("1/minute;5 per day",
	               override_defaults=True,
	               deduct_when=lambda response: response.status_code == 200)
	@valida_request(["token", "cpf"], 500, tipo="args")
	def __requisita_troca_bu():
		testing = "TESTING" in app.config and app.config["TESTING"]
		return requisita_troca_bu(flask_mail, testing=testing)

	@bprint.route("/trocaBU/redefinirBU/", methods=['GET', 'POST'])
	@valida_request(["cpf", "token", "novo_bu"], 500, methods=["POST"])
	def __redefinir_bu():
		if request.method == "GET":
			parametros = request.args
			if "token" not in parametros:
				abort(404)
			token = parametros["token"]
			return render_template('troca_bu.html', token=token)

		if "TESTING" in app.config and app.config["TESTING"]:
			return redefine_bu()

		resposta = redefine_bu()[0]
		if resposta["estado"] == "Erro":
			return render_template('tela_resultado.html',
			                       titulo="Troca de Bilhete Único",
			                       msgErro="Erro: " + resposta["erro"])

		return render_template('tela_resultado.html',
		                       titulo="Troca de Bilhete Único",
		                       msgErro="",
		                       msgSucesso="Bilhete Único Trocado com Sucesso!")

	@bprint.route("/feedbackViagem/", methods=['GET', 'POST'])
	@limiter.limit("1200/minute;1200000000 per day",
	               override_defaults=True,
	               deduct_when=lambda response: response.status_code == 200)
	@valida_request(["cpf", "token", "idViagem"], 10000, methods=["POST"])
	def __feedback_viagem():
		if request.method == "GET":
			parametros = request.args
			if "token" not in parametros or "idViagem" not in parametros:
				abort(404)
			token = parametros["token"]
			id_viagem = parametros["idViagem"]
			return render_template('feedback_viagem.html',
			                       token=token,
			                       idViagem=id_viagem)

		if "TESTING" in app.config and app.config["TESTING"]:
			return registra_feedback_viagem()

		resposta = registra_feedback_viagem()[0]
		if resposta["estado"] == "Erro":
			return render_template('tela_resultado.html',
			                       titulo="Feedback: Última Viagem",
			                       msgErro="Erro: " + resposta["erro"])

		return render_template('tela_resultado.html',
		                       titulo="Feedback: Última Viagem",
		                       msgErro="",
		                       msgSucesso="Obrigado pelo feedback!")

	@bprint.route("/feedbackViagemPerdida/", methods=['GET', 'POST'])
	@valida_request(["cpf", "token"], 10000, methods=["POST"])
	def __feedback_viagem_perdida():
		if request.method == "GET":
			parametros = request.args
			if "token" not in parametros:
				abort(404)
			token = parametros["token"]
			return render_template('feedback_viagem_perdida.html', token=token)

		if "TESTING" in app.config and app.config["TESTING"]:
			return registra_feedback_viagem_perdida()

		resposta = registra_feedback_viagem_perdida()[0]
		if resposta["estado"] == "Erro":
			return render_template('tela_resultado.html',
			                       titulo="Feedback: Viagem Perdida",
			                       msgErro="Erro: " + resposta["erro"])

		return render_template('tela_resultado.html',
		                       titulo="Feedback: Viagem Perdida",
		                       msgErro="",
		                       msgSucesso="Obrigado pelo feedback!")

	# Reseta o bd e faz o seu setup (apenas disponível em debug)
	@bprint.route("/resetaBD/")
	def _reseta_bd():
		if not app.debug or app.config["BD_NOME"] != os.getenv("BD_NOME_TESTE"):
			abort(404)

		if "reset" in request.args and request.args["reset"] == "1":
			reseta_bd_teste()
			return 'BD TESTE RESETADO'

		return "BD TESTE NAO RESETADO"

	@bprint.errorhandler(413)
	def _request_too_large(error):  # pylint: disable=unused-argument
		return jsonify({'estado': "Erro", 'erro': "Dados em excesso"}), 413

	app.register_blueprint(bprint)
	return app


def create_limiter(app, testing: bool):
	'''
		Cria limitador de requisições para os endpoints
	'''
	limiter = Limiter(
	    get_remote_address,
	    app=app,
	    default_limits=["200 per day", "50 per hour"],
	    storage_uri="memory://",
	)
	if testing:
		limiter.enabled = False
	return limiter


def close_connection(response, con, cur):
	'''
		Função usada para fechar a conexão e o cursor
		do bd antes de enviar a resposta de um endpoint para
		o usuário	
	'''
	cur.close()
	con.close()
	return response
