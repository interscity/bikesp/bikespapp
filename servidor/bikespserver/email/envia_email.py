'''
	Módulo responsável por enviar um email utilizando a biblioteca flask_email
'''
import os
import smtplib
from email.mime.text import MIMEText
from flask_mail import Message


def envia_email_flask(flask_mail,
                      assunto="",
                      html="",
                      enviador="",
                      recebedores=None):
	'''
		Envia um email pelo Flask
	'''
	msg = Message(assunto, sender=enviador, recipients=recebedores)
	msg.html = html
	flask_mail.send(msg)


def envia_email(enviador, senha, recebedores, assunto, body):
	'''
		Envia um email pelo SMTPlib
	'''
	msg = MIMEText(body, "html")
	msg['Subject'] = assunto
	msg['From'] = enviador
	msg['To'] = ', '.join(recebedores)
	with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp_server:
		smtp_server.login(enviador, senha)
		print(msg)
		smtp_server.send_message(msg)


def envia_email_interno(assunto, body,\
       mail_user=os.getenv("MAIL_USERNAME"),\
       mail_pass=os.getenv("MAIL_PASSWORD")):
	'''
		Envia um email que não deve ser encaminhado para a lista de email bikesp@ime.usp.br.
		A função adiciona no corpo do email a palavra chave MENSAGEM_INTERNA,
		o que impede o encaminhamento do email pelo filtro que configuramos no Gmail.
	'''
	novo_body = f"MENSAGEM_INTERNA <br><br> {body}"

	envia_email(mail_user, mail_pass, [mail_user], assunto, novo_body)
