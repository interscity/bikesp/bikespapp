'''
	Módulo que contém funções relacionadas a segurança
'''

import hashlib
import base64
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad


def encripta(valor: str, chave: str) -> str:
	'''
		Encripta um valor utilizando o algortimo AES, dada uma chave
	'''
	data = pad(valor.encode(), 16)
	cipher = AES.new(chave.encode('utf-8'), AES.MODE_ECB)
	return base64.b64encode(cipher.encrypt(data)).decode("utf-8")


def hash_MD5(to_hash: str):  #pylint: disable=invalid-name
	'''
		Aplica o algoritmo de hash MD5 em determinado valor
	'''
	return hashlib.md5(to_hash.encode('utf-8'))
