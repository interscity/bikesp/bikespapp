"""
	Módulo contendo funções usadas para validar e testar entradas
	e objetos tratados pelo servidor.
"""

import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.utils.seguranca import hash_MD5
#pylint: enable=wrong-import-position


def valida_tamanhos_campos_form(form: dict, len_max: int):
	"""
		Valida se todos os campos de um dado formulário tem tamanho
		menor ou igual a lenMax
	"""
	return busca_excedente_campos_form(form, len_max)[0] is None


def busca_excedente_campos_form(form: dict, len_max: int):
	"""
		Verifica se algum campo do form tem tamanho maior que len_max
		Se sim retorna o campo e o tamanho
	"""
	for chave in form:
		try:
			if len(str(form[chave])) > len_max:
				return chave, len(str(form[chave]))
			if isinstance(form[chave], dict):
				chave, tamanho = busca_excedente_campos_form(
				    form[chave], len_max)
				if chave:
					return chave, tamanho
		except ValueError:
			pass

	return None, 0


def valida_chaves_form(form: dict, chaves_esperadas: list):
	'''
		Checa se chaves esperadas estão no dicionário
	'''
	for chave_esperada in chaves_esperadas:
		if chave_esperada not in form:
			return False
	return True


def valida_integridade_request(request_dict, chave):
	'''
		Valida se campo 'integridade' da request corresponde
		com o conteúdo. Passo a passo:
		- Junte todos os pares chave+valor do dicionário em uma lista
		- Ordene a lista
		- Concatene cada elemento da lista em uma única string
		- Adicione a chave antes dessa string
		- Aplique o algoritmo de md5
	'''
	if "integridade" not in request_dict:
		return False
	pares_key_value = []
	for key in request_dict:
		if key == "integridade":
			continue
		pares_key_value.append(str(key + request_dict[key]))

	pares_key_value.sort()
	texto = "".join(pares_key_value)

	to_hash = chave + texto

	hashed = hash_MD5(to_hash)

	if hashed.hexdigest() != request_dict["integridade"]:
		return False

	return True
