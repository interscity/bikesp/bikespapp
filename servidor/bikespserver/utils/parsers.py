'''
	Script contendo funções úteis para parsear diferentes tipos de valores
	A maioria é utilizada para parsear as respostas do formulário
'''
import string
import re
import time
from datetime import datetime
import json
from typing import Any
from unidecode import unidecode

#### Funções para o processamento de string ####

#pylint: disable=missing-function-docstring


def remove_pontuacao(stri: str) -> str:
	return str(stri).translate(str.maketrans('', '',
	                                         string.punctuation)).strip()


def remove_acentuacao(stri: str) -> str:
	return unidecode(str(stri))


def remove_quebras(stri: str) -> str:
	return str(stri).replace("\n", " ").replace("\t", "").replace("\r", " ")


def apenas_digitos(stri: str) -> str:
	return re.sub(r'[^\d]', '', str(stri))


def troca_ponto_virgula(stri: str) -> str:
	stri = stri.replace("|", "")
	stri = stri.strip()
	ponto_sep = "|ponto|"
	virgula_sep = "|virgula|"
	stri = stri.replace(",", ponto_sep)
	stri = stri.replace(".", virgula_sep)
	stri = stri.replace(ponto_sep, ".").replace(virgula_sep, ",")
	return stri


#### Parsers genéricos ####


def parse_data(data: str) -> str:
	if data == "":
		data = "1900-01-01 00:00:00"
	return data


def parse_bool(booleano: str) -> bool:
	if (booleano in ["Sim", "sim", "s", "True", "true", True]):
		return True
	return False


def parse_coordenadas(coordenadas: Any) -> str:
	if isinstance(coordenadas, dict):
		if "lat" in coordenadas:
			lat = coordenadas["lat"]
		elif "latitude" in coordenadas:
			lat = coordenadas["latitude"]
		if "lng" in coordenadas:
			lng = coordenadas["lng"]
		elif "lon" in coordenadas:
			lng = coordenadas["lon"]
		elif "long" in coordenadas:
			lng = coordenadas["long"]
		elif "longitude" in coordenadas:
			lng = coordenadas["longitude"]
		coordenadas = [lat, lng]
	if isinstance(coordenadas, list):
		coordenadas = str(coordenadas[0]) + "," + str(coordenadas[1])
	coordenadas = coordenadas.replace("(", "").replace(")", "").replace(
	    "[", "").replace("]", "")
	return coordenadas


def postgres_point_para_lista(postgres_point: str) -> list:
	retira_parenteses = postgres_point.replace("(", "").replace(")", "")
	lista = retira_parenteses.split(",")
	return [float(lista[0]), float(lista[1])]


# pylint: disable=too-many-arguments
def parse_endereco(rua: str,
                   numero: str,
                   complemento="",
                   bairro="",
                   cep="",
                   cidade="",
                   estado="") -> str:
	rua = str(rua).replace("|", "").strip()
	numero = str(numero).replace("|", "").strip()
	complemento = str(complemento).replace("|", "").strip()
	bairro = str(bairro).replace("|", "").strip()
	cep = apenas_digitos(cep)
	cidade = str(cidade).replace("|", "").strip()
	if estado.lower().strip() in ["são paulo", "sao paulo", "sp"]:
		estado = "SP"
	else:
		estado = str(estado).replace("|", "").strip()

	endereco = rua[0:170] + "|" + numero[0:15] + "|" + complemento[
	    0:15] + "|" + bairro[0:50] + "|" + cep[0:8] + "|" + cidade[
	        0:30] + "|" + estado[0:30]
	return endereco


# pylint: enable=too-many-arguments


def parse_integer(i: Any) -> Any:
	try:
		i = str(int(float(i)))
	except:  # pylint: disable=bare-except
		i = None
	return i


def parse_range(rng: str, tipo="integer") -> str:
	rng = str(rng)
	if rng in ["", "0.0"]:
		rng = "0"
	if tipo == "integer":
		pass
	elif tipo == "money":
		rng = rng.replace("R$", "").strip()

	if "Acima" in rng:
		valor1, valor2 = [rng.replace("Acima de", ""), ""]
	elif "ou mais" in rng:
		valor1, valor2 = [rng.replace("ou mais", ""), ""]
	else:
		if rng == "0":
			valor1, valor2 = ["0", "0"]
		else:
			valor1, valor2 = rng.split("a")
		valor1 = valor1.replace("de", "")
		valor2 = valor2.replace("de", "")
	valor1 = troca_ponto_virgula(valor1).replace(",", "")
	valor2 = troca_ponto_virgula(valor2).replace(",", "")
	rng = "[" + valor1 + "," + valor2 + ")"
	return rng


def parse_dinheiro_float(dinheiro: str | None) -> float:
	'''
		Recebe uma string do tipo X$XSepXX e devolve
		o float correspondente
	'''
	if dinheiro is None:
		return 0.0
	if isinstance(dinheiro, (float, int)) or "$" not in dinheiro:
		raise TypeError(
		    "Float passado para parse_dinheiro_float em vez de money")

	dinheiro = dinheiro.strip()
	negativo = -1 if dinheiro[0] == "-" else 1
	dinheiro = str(dinheiro).split("$")[1].strip()

	cents = dinheiro[-2:]
	if not cents.isdigit():
		raise ValueError(
		    f"{cents} é a parte dos centavos do dinheiro, isso é inválido")

	reais = dinheiro[:-3].replace(".", "").replace(",", "")
	if not reais.isdigit():
		raise ValueError(
		    f"{reais} é a parte inteira do dinheiro, isso é inválido")

	return (int(reais) + int(cents) / 100) * negativo


def parse_data_js(data_entrada: str | int, as_time=True):
	'''
		Recebe uma string de um horário vindo do js e converte para um
	objeto Date do python. 
	'''

	if isinstance(data_entrada, int):
		return data_entrada
	if isinstance(data_entrada, float):
		return int(data_entrada)

	crua = data_entrada
	crua = re.sub(r"[\s:]", "", crua)

	limpa = crua[:10] + " " + crua[10:12] + ":" + crua[12:14] + ":" + crua[
	    14:16] + " " + crua[16:-5] + " " + crua[-5:]

	data = datetime.strptime(limpa, '%Y-%m-%d %H:%M:%S %Z %z')

	if as_time:
		return time.mktime(data.timetuple())

	return data


#### Parsers específicos do formulário ####


def parse_nome(nome: str) -> str:
	return str(nome)[0:50]


def parse_bilhete_unico(bilhete_unico: str) -> str:
	return apenas_digitos(bilhete_unico)[0:15]


def parse_documento(documento: str) -> str:
	return apenas_digitos(documento)[0:15]


# pylint: disable=no-else-return
def parse_genero(genero: str) -> str:
	if str(genero).lower() in ["masculino", "m", "homem"]:
		return "M"
	elif str(genero).lower() in ["feminino", "f", "mulher"]:
		return "F"
	elif remove_acentuacao(str(genero).lower()) in ["nao binario", "nb"]:
		return "NB"
	else:
		return "NA"


# pylint: enable=no-else-return


def parse_telefone(telefone: str) -> str:
	return apenas_digitos(telefone).strip()[0:15].strip()


def parse_raca(raca: str) -> str:
	return str(raca).strip()[0:10].strip()


def parse_email(email: str) -> str:
	return remove_quebras(str(email).lower()[0:64]).replace(" ", "")


def parse_bairro(bairro: str) -> str:
	return str(bairro).strip()[0:50].strip()


def parse_cidade(cidade: str) -> str:
	return str(cidade).strip()[0:30].strip()


def parse_estado(estado: str) -> str:
	if estado.lower().strip() in ["são paulo", "sao paulo", "sp"]:
		return "SP"
	return str(estado).strip()[0:30].strip()


def parse_cep(cep: str) -> str:
	cep = apenas_digitos(cep)
	return cep[0:8]


def parse_sis_op(sis: str) -> str:
	return str(sis).strip()[0:10].strip()


def parse_grau_instrucao(grau: str) -> str:
	return str(grau).strip()[0:50].strip()


def parse_vinculo_empregaticio(vinculo: str) -> str:
	return str(vinculo).strip()[0:50].strip()


def parse_condicao_atividade(condicao: str) -> str:
	return str(condicao).strip()[0:50].strip()


def parse_apelido(apelido: str) -> str:
	return str(apelido).strip()[0:28].strip()


def parse_pq_nao_incentivo(pq_nao_incentivo: str) -> str:
	return str(pq_nao_incentivo).strip()[0:300].strip()


def parse_frequencia_trajeto(frequencia: str) -> str:
	return str(frequencia).strip()[0:300].strip()


def parse_como_faz_trajeto(como_faz: str) -> str:
	return str(como_faz).strip()[0:300].strip()


def parse_possibilidade_bike(possibilidade: str) -> str:
	return str(possibilidade).strip()[0:300].strip()


### Demais Parsers ###


def json_para_geoJSON(trajeto_js):  #pylint: disable=invalid-name

	trajeto_js = re.sub(r"'", '"', str(trajeto_js))
	trajeto_js = json.loads(trajeto_js)

	coordenadas = list(
	    map(
	        lambda ponto:
	        [ponto['Posicao']['longitude'], ponto['Posicao']['latitude']],
	        trajeto_js))

	obj_line_string = {
	    "type": "Feature",
	    "properties": {},
	    "geometry": {
	        "type": "LineString",
	        "coordinates": coordenadas,
	    }
	}

	lista_pontos = list(
	    map(
	        lambda ponto: {
	            "type": "Feature",
	            "properties": {
	                "Precisao": ponto['Precisao'],
	                "Data": ponto['Data'],
	            },
	            "geometry": {
	                "coordinates": [
	                    ponto['Posicao']['longitude'], ponto['Posicao'][
	                        'latitude']
	                ],
	                "type": "Point"
	            },
	            # "id": 0
	        },
	        trajeto_js))

	output = {
	    "type": "FeatureCollection",
	    "features": [obj_line_string] + lista_pontos
	}

	return json.dumps(output)


#pylint: enable=missing-function-docstring
