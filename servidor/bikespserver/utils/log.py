'''
    Utilitários de log para o BikeSP
'''
import logging


class CustomFormatter(logging.Formatter):
	'''
		Define o formatter padrão do bikesp com suporte a cores e informação de debug	
 	'''
	wcolor = "\x1b[33;20m"
	ecolor = "\x1b[31;20m"
	ccolor = "\x1b[31;1m"
	reset = "\x1b[0m"

	fmt = "[%(asctime)s] %(levelname)s: %(message)s"
	dfmt = fmt + " (%(filename)s:%(lineno)d)"
	wfmt = wcolor + fmt + " (%(filename)s:%(lineno)d)" + reset
	efmt = ecolor + fmt + " (%(filename)s:%(lineno)d)" + reset
	cfmt = ccolor + fmt + " (%(filename)s:%(lineno)d)" + reset
	datefmt = "%d/%b/%Y %H:%M:%S"

	FORMATS = {
	    logging.DEBUG: logging.Formatter(dfmt, datefmt),
	    logging.INFO: logging.Formatter(fmt, datefmt),
	    logging.WARNING: logging.Formatter(wfmt, datefmt),
	    logging.ERROR: logging.Formatter(efmt, datefmt),
	    logging.CRITICAL: logging.Formatter(cfmt, datefmt),
	}

	def format(self, record):
		formatter = self.FORMATS.get(record.levelno)
		return formatter.format(record)
