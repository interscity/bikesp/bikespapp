'''
	Script com funções úteis relacionadas à coordenadas
'''
import math
import random
import sys
from pathlib import Path
from typing import Tuple, List, Any, Optional

import pytz

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

# pylint: disable=wrong-import-position
from servidor.bikespserver.apis import tomtom
from servidor.bikespserver.apis import nominatim

from servidor.bikespserver.utils.parsers import parse_coordenadas
from servidor.bikespserver.utils.parsers import remove_pontuacao
# pylint: enable=wrong-import-position

fuso_horario = pytz.timezone('America/Sao_Paulo')

RAIO_MAXIMO_TOLERANCIA = 240

random.seed(188)


def coordenada_valida(coordenada: List[Any]) -> bool:
	'''
		Checa validez de certa coordenada. Caso a distância
		entre a coordenada e o marco 0 de São Paulo seja maior
		que determinado valor, ela não é válida
	'''
	distancia_maxima_marco_0_sp = 60000
	marco_0_sp = [-23.5504533, -46.6339112]

	if coordenada == [-1, -1]:
		return False

	dist_marco_0 = distancia_haversine(marco_0_sp, coordenada)
	if dist_marco_0 > distancia_maxima_marco_0_sp:
		return False

	return True


def get_coordenada_endereco(endereco: str) -> List[Any]:
	'''
		Recebe um endereço e devolve a coordenada
		(lat,long) coorespondente a ele
	'''
	split_endereco = endereco.split("|")

	rua = split_endereco[0]

	sem_numero = False

	try:
		numero = str(math.floor(float(split_endereco[1])))
		if numero == "0":
			sem_numero = True
	except ValueError:
		numero = split_endereco[1]
		sem_numero = True

	if len(split_endereco) >= 4:
		bairro = split_endereco[3]
	else:
		bairro = ""

	cep = ""  # type: str|int
	if len(split_endereco) >= 5:
		split_endereco[4] = remove_pontuacao(split_endereco[4])
		if split_endereco[4].isdigit():
			if int(split_endereco[4][0]) != 0:
				cep = int(split_endereco[4])
			else:
				cep = split_endereco[4]

	if len(split_endereco) >= 6:
		cidade = split_endereco[5]
	else:
		cidade = "São Paulo"

	if len(split_endereco) >= 7:
		estado = split_endereco[6]
	else:
		estado = "São Paulo"

	estado = converte_sigla_estado(estado)

	if sem_numero:
		api = tomtom.TomTomAPI()

		string_busca = str(rua) + " " + str(cep) + " " + str(
		    bairro) + " " + str(cidade) + " " + str(estado) + " Brasil"

		coords = api.get_coordenadas(string_busca)
		if not coordenada_valida(coords):
			api = nominatim.NominatimAPI()
			coords = api.get_coordenadas(string_busca)
	else:
		dict_busca = {
		    "countryCode": "BR",
		    "countrySubdivision": estado,
		    "municipality": cidade,
		    "municipalitySubdivision": bairro,
		    "streetNumber": numero,
		    "streetName": rua,
		    "postalCode": cep,
		}
		api = tomtom.TomTomAPI()

		coords = api.get_coordenadas(dict_busca)

	if not coordenada_valida(coords):
		coords = api.COORD_INVALIDA
	return coords


def get_coordenada_estacao(estacao: str) -> List[Any]:
	'''
		Recebe uma localização do tipo estação e devolve
		a coordenada (lat,long) correspondente a ela
	'''
	if "term." in estacao.lower():
		estacao = estacao.lower().replace("term.", "terminal")

	nome_linha = estacao.lower()

	transporte = ""
	if "metrô" in estacao.lower() or "metro" in estacao.lower():
		transporte = "Estação de metrô "
	elif "cptm" in estacao.lower():
		transporte = "Estação de trem "
	elif "onibus" in estacao or "ônibus " in estacao:
		transporte = "Ponto de ônibus "
	elif "terminal" in estacao.lower() or "term." in estacao.lower():
		transporte = ""
	else:
		transporte = "Estação "

	str_busca = transporte + nome_linha + ", São Paulo, Brasil"

	if "sé" in estacao.lower():
		str_busca = "Praça da sé, São Paulo, Brasil"
	if "pinheiros" in estacao.lower():
		str_busca = "Estação Pinheiros, São Paulo, Brasil"
	if "ayrton senna" in estacao.lower():
		str_busca = "R. Pedro Madureira, 585 - Jardim São Paulo, São Paulo - SP, 02044-140"
	if "term" in estacao.lower() and "barra funda" in estacao.lower():
		str_busca = "Estação Palmeiras-Barra Funda, São Paulo, Brasil"
	api = nominatim.NominatimAPI()
	coords = api.get_coordenadas(str_busca)
	if not coordenada_valida(coords):
		coords = api.COORD_INVALIDA

	return coords


def get_coordenada_aleatoria() -> List[Any]:
	'''
		Devolve uma coordenada aleatória em uma área
		interna de São Paulo
	'''
	quadrado = [[-23.49203, -46.7217], [-23.49203, -46.48892],
	            [-23.65125, -46.7217], [-23.65125, -46.48892]]

	random_lat = random.random()
	random_long = random.random()

	latitude = round(
	    quadrado[0][0] + (quadrado[2][0] - quadrado[0][0]) * random_lat, 6)
	longitude = round(
	    quadrado[1][1] + (quadrado[0][1] - quadrado[1][1]) * random_long, 6)

	return [latitude, longitude]


def ponto_proximo_localizacao_usuario(
        ponto: list | dict | str,
        todas_localizacoes_possiveis: list) -> Optional[Any]:
	'''
		Devolve o id da localizacao mais próxima a determinada coordenada,
		caso a distancia seja maior que um raio maximo, devolve None
	'''
	lat1, lon1 = converte_ponto(ponto)
	distancias_ponto_localizacoes = {}
	for localizacao in todas_localizacoes_possiveis:
		id_localizacao = localizacao[0]
		coordenadas = parse_coordenadas(localizacao[1])
		lat2, lon2 = converte_ponto(coordenadas)
		distancia_hav = distancia_haversine([lat1, lon1], [lat2, lon2])
		distancias_ponto_localizacoes[id_localizacao] = distancia_hav
	if distancias_ponto_localizacoes:
		id_localizacao, dist_minima = min(distancias_ponto_localizacoes.items(),
		                                  key=lambda x: x[1])
		if dist_minima <= RAIO_MAXIMO_TOLERANCIA:
			return [id_localizacao, dist_minima]
	return None


def converte_ponto(point1: List | dict | str) -> Tuple[float, float]:
	'''
		Recebe um ponto em formato de lista, dicionário ou string
		e devolve 2 variáveis correspondentes a latitude e
		longitude
	'''
	if isinstance(point1, list):
		lat1 = point1[0]
		lon1 = point1[1]
	elif isinstance(point1, dict):
		lat1 = point1["latitude"]
		lon1 = point1["longitude"]
	else:
		lat1 = float(point1.split(",")[0])
		lon1 = float(point1.split(",")[1])
	return lat1, lon1


def converte_p1_p2(point1: List | dict,
                   point2: List | dict) -> Tuple[float, float, float, float]:
	'''
		Recebe dois pontos em formato de lista ou dicionário
		e devolve 4 variáveis correspondentes a cada latitude/longitude
	'''
	lat1, lon1 = converte_ponto(point1)
	lat2, lon2 = converte_ponto(point2)
	return lat1, lon1, lat2, lon2


# pylint: disable=invalid-name disable=non-ascii-name
def distancia_haversine(point1: List | dict, point2: List | dict) -> float:
	'''
		Calcula a distância entre dois pontos utilizando
		a fórmula de haversine
	'''

	lat1, lon1, lat2, lon2 = converte_p1_p2(point1, point2)

	R = 6371e3
	φ1 = lat1 * math.pi / 180
	φ2 = lat2 * math.pi / 810
	Δφ = (lat2 - lat1) * math.pi / 180
	Δλ = (lon2 - lon1) * math.pi / 180

	a = math.sin(Δφ / 2) * math.sin(Δφ / 2) + math.cos(φ1) * math.cos(
	    φ2) * math.sin(Δλ / 2) * math.sin(Δλ / 2)
	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
	return R * c


# pylint: enable=invalid-name enable=non-ascii-name


def get_rota_info(point1: List | dict,
                  point2: List | dict,
                  transporte="bicycle") -> dict:
	'''
		Calcula a distância entre dois pontos, considerando a rota
		utilizando certo meio de transporte
	'''
	if distancia_haversine(point1, point2) < 1:
		return {"distancia": 0, "trajeto": [point1]}

	lat1, lon1, lat2, lon2 = converte_p1_p2(point1, point2)

	api = tomtom.TomTomAPI()
	rota_info = api.calcula_rota([lat1, lon1, lat2, lon2],
	                             transporte=transporte)
	return rota_info


def get_rota_aleatoria() -> dict:
	'''
		Devolve uma rota aletória
	'''
	rota_info = {"distancia": 7000, "trajeto": {}}
	return rota_info


def converte_sigla_estado(estado: str):
	'''
		Recebe um estado e converte ele para
		seu nome completo, caso seja uma sigla
	'''
	siglas = {
	    "AC": "Acre",
	    "AL": "Alagoas",
	    "AP": "Amapá",
	    "AM": "Amazonas",
	    "BA": "Bahia",
	    "CE": "Ceará",
	    "DF": "Distrito Federal",
	    "ES": "Espírito Santo",
	    "GO": "Goiás",
	    "MA": "Maranhão",
	    "MT": "Mato Grosso",
	    "MS": "Mato Grosso do Sul",
	    "MG": "Minas Gerais",
	    "PA": "Pará",
	    "PB": "Paraíba",
	    "PR": "Paraná",
	    "PE": "Pernambuco",
	    "PI": "Piauí",
	    "RJ": "Rio de Janeiro",
	    "RN": "Rio Grande do Norte",
	    "RS": "Rio Grande do Sul",
	    "RO": "Rondônia",
	    "RR": "Roraima",
	    "SC": "Santa Catarina",
	    "SP": "São Paulo",
	    "SE": "Sergipe",
	    "TO": "Tocantins"
	}

	if str(estado).upper() in siglas:
		return siglas[str(estado).upper()]
	return estado


if __name__ == "__main__":
	print(
	    get_coordenada_endereco(
	        "Praça Charles Miller|s/n||Pacaembu|01234010 |São Paulo|SP"))
