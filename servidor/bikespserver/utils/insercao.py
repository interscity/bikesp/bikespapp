'''
    Wrapper do script exporta_insere.py
	escrito para o endpoint de inserção de usuários
	pelo painel de admin
'''
import os
from pathlib import Path
from subprocess import Popen, PIPE


def inserir_usuarios(app, retorno, erro):
	'''
	    Roda o shell script de inserir usuários, retornos e erros são retornados em listas
	'''
	script_path = Path(
	    __file__).parent.parent.parent / "etl/respostas_forms/exporta_insere.py"
	out = "/tmp/nao_precisa_do.csv"
	id_survey = os.getenv("ID_SURVEY", "612292")

	process = Popen(  # pylint: disable=R1732
	    ["python", str(script_path), "--survey", id_survey, "-o", out],
	    stdin=PIPE,
	    stdout=PIPE,
	    stderr=PIPE,
	    universal_newlines=True)

	stdout, stderr = process.communicate(input="S\n")
	app.logger.info("Output: %s", stdout)

	mensagens_erro = [
	    "Connection to api.tomtom.com timed out",
	    "HTTPSConnectionPool(host='api.tomtom.com', port=443): Read timed out."
	]

	if any(erro in stderr for erro in mensagens_erro):
		mensagem = "Faltam usuários a serem inseridos, tente novamente"
		app.logger.error("%s", mensagem)
		retorno.append(mensagem)
		erro.append(True)
	elif process.returncode == 1:
		mensagem = "Houve um erro ao inserir os usuários"
		app.logger.error("%s", mensagem)
		retorno.append(mensagem)
		erro.append(True)
	else:
		mensagem = "Todos os usuários foram inseridos!"
		app.logger.info("Todos os usuários foram inseridos!")
		retorno.append(mensagem)
		erro.append(False)
