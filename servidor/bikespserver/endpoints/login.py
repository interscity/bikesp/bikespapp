'''
	Módulo responsável por gerenciar o login de usuários
'''

import logging
import os
import sys
from pathlib import Path
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.autenticacao.gerencia_sessao import gera_sessao,\
   salva_sessao, checa_sessao_existente, reseta_sessoes

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_por_email
from servidor.bikespserver.bd.consultas import usuario_senha_existe
from servidor.bikespserver.bd.consultas import get_todas_localizacoes_pessoa
from servidor.bikespserver.bd.consultas import pessoa_validou_locs

from servidor.bikespserver.utils.parsers import parse_email
#pylint: enable=wrong-import-position


def login():
	"""
		Controlador responsável por tratar a tentativa de login de um usuário
	"""
	con = conecta()
	cur = con.cursor()
	logger = logging.getLogger("bikespserver")

	email = parse_email(request.form['email'])
	id_pessoa = get_pessoa_por_email(email, cur)

	if id_pessoa is None:
		logger.debug("Usuário %s não cadastrado", email)
		return ({
		    'estado': "Erro",
		    'erro': 'Par Usuario / Senha Nao Cadastrado'
		}, 401)

	# Checar aqui se existe mais de uma pessoa com mesmo email?

	cpf = id_pessoa[1]
	id_pessoa = id_pessoa[0]

	resultado = usuario_senha_existe(id_pessoa, request.form['senha'], cur)
	if not resultado:
		logger.debug("Senha incorreta para usuário %s", email)
		return ({
		    'estado': "Erro",
		    'erro': 'Par Usuario / Senha Nao Cadastrado'
		}, 401)

	versao_app_minima = os.getenv("MIN_VERSAO_APP", "")
	versao_app_cliente = ""

	if 'versao' in request.form:
		versao_app_cliente = request.form['versao']

	if versao_app_cliente < versao_app_minima:
		return ({
		    'estado':
		        "Erro",
		    'erro':
		        'Seu aplicativo está desatualizado.\n Atualize-o para continuar.'
		}, 403)

	if checa_sessao_existente(id_pessoa):
		## O if existe unicamente para permitir que depois seja feito alguma
		# edição ao fluxo caso alguma outra sessão ja existisse para o usuário
		reseta_sessoes(id_pessoa, cur)
		con.commit()

	# Criar sessão
	nova_sessao = gera_sessao(str(cpf))

	salva_sessao(id_pessoa, nova_sessao)

	# Busca locais da pessoa
	locs = get_todas_localizacoes_pessoa(id_pessoa, cur)

	# Busca se a pessoa validou seus locais
	validou_locs = pessoa_validou_locs(id_pessoa, cur)

	con.commit()
	cur.close()
	con.close()

	logger.debug("Usuário %s logado", email)
	return (
	    {
	        'estado': "Usuário Logado",
	        'CPF': cpf,
	        'token': nova_sessao['token'],
	        'validade': nova_sessao['dataValidade'],
	        'validouLocs': validou_locs,
	        'locs': locs,  # Não é mais útil para versões novas do app
	    },
	    200)
