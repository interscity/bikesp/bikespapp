'''
	Módulo responsável por receber do usuário quais das suas localizações
	estão com coordenadas incorretas
'''
import sys
import json

from pathlib import Path
from threading import Thread

from flask import request, current_app

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.atualizacoes import atualiza_validacao_locs_pessoa
from servidor.bikespserver.bd.consultas import get_pessoa_info, \
 pessoa_validou_locs, id_localizacao_existe
from servidor.bikespserver.email.envia_email import envia_email_interno
from servidor.bikespserver.autenticacao.gerencia_sessao import token_valido
from servidor.bikespserver.utils.validadores import valida_chaves_form
from servidor.bikespserver.utils.validadores import valida_tamanhos_campos_form
from servidor.bikespserver.utils.parsers import parse_coordenadas

#pylint: enable=wrong-import-position


# pylint: disable=too-many-return-statements
def valida_localizacoes():
	'''
		Função que processa a validação das localizações feitas
		pelo usuário
	'''
	#pylint: disable=duplicate-code
	if not valida_chaves_form(request.form, ["user", "token", "locsErradas"]):
		return ({'estado': "Erro", 'erro': "Requisição inválida"}, 400)
	if not valida_tamanhos_campos_form(request.form, 501):
		return ({'estado': "Erro", 'erro': 'Dados em excesso'}, 413)

	con = conecta()
	cur = con.cursor()

	usuario = request.form['user']
	info_pessoa = get_pessoa_info(usuario, cur)

	if info_pessoa is None or not token_valido(info_pessoa[0],
	                                           request.form['token'], cur):
		return ({'estado': "Erro", 'erro': "Erro de Autenticacao"}, 401)

	id_pessoa = info_pessoa[0]
	#pylint: enable=duplicate-code

	if pessoa_validou_locs(id_pessoa, cur):
		return ({'estado': "Erro", 'erro': "Validação já realizada"}, 400)

	try:
		locs_erradas = json.loads(request.form["locsErradas"])
	except json.decoder.JSONDecodeError:
		return ({'estado': "Erro", 'erro': "Requisição inválida"}, 400)

	report_locs_erradas = "N° de localizações erradas: " + str(
	    len(locs_erradas)) + "<br>"

	for loc in locs_erradas:
		id_localizacao = loc["idLocalizacao"]
		info_loc = id_localizacao_existe(id_localizacao, cur)
		if info_loc is None:
			return ({'estado': "Erro", 'erro': "Localização inexistente"}, 400)

		coordenadas = parse_coordenadas(loc["coordenadas"])
		endereco_bd, coordenadas_bd, tipo_localizacao_bd = info_loc

		report_locs_erradas += "<br>ID da localização: <b>" + str(
		    id_localizacao) + "</b><br>"
		report_locs_erradas += "   Coordenadas no mapa: " + str(
		    coordenadas) + "<br>"
		report_locs_erradas += "   Coordenadas no bd: " + str(
		    coordenadas_bd) + "<br>"
		report_locs_erradas += "   Endereço no bd: " + str(endereco_bd) + "<br>"
		report_locs_erradas += "   Tipo da localização no bd: " + str(
		    tipo_localizacao_bd) + "<br><br>"

	if current_app.debug or (
	    "TESTING" in current_app.config and current_app.config["TESTING"]
	) or current_app.config["PREFERRED_URL_SCHEME"] == "http":
		pass
	else:
		if len(locs_erradas) > 0:
			gerencia_email_report(id_pessoa, report_locs_erradas)

	atualiza_validacao_locs_pessoa(id_pessoa, True, cur)
	cur.close()
	con.commit()
	con.close()
	return ({'validado': True}, 200)


# pylint: enable=too-many-return-statements


def gerencia_email_report(id_pessoa: str | int, report: str):
	'''
		Envia o email para os desenvolvedores com o resport
	'''
	with current_app.app_context():
		senha_email = current_app.config["MAIL_PASSWORD"]
		enviador = current_app.config["MAIL_DEFAULT_SENDER"]
		assunto_email = "Bike SP - Validação de localizações"
		mensagem = (
		    f"Validação de localizações da pessoa <b>{id_pessoa}</b>, <br><br>"
		    f"{report}"
		    "<br><br>Equipe de Desenvolvimento do Bike SP")

		Thread(
		    target=envia_email_interno,
		    args=(assunto_email, mensagem, enviador, senha_email),
		).start()
