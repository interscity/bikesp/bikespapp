'''
	Módulo responsável por gerar um extrato
	dos bilhetes únicos e dos créditos do usuário
'''
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.consultas import get_bilhetes_unicos_pessoa, \
 get_historico_envio_pessoa
#pylint: enable=wrong-import-position


def gera_extrato(cur, info_pessoa):
	'''
		Devolve uma lista contendo o histórico de bilhete unico e 
		outra com o histórico de créditos do usuário
	'''
	id_pessoa = info_pessoa[0]

	# bilheteUnico,dataInicio,dataFim,ativo,concedido,aguardandoEnvio
	bilhetes_unicos_pessoa = get_bilhetes_unicos_pessoa(id_pessoa, cur)

	# bilheteUnico,dataEnvio,valor,confirmado
	creditos = get_historico_envio_pessoa(id_pessoa, cur)

	bilhetes_unicos_filtrados = []
	for i in bilhetes_unicos_pessoa:
		bilhetes_unicos_filtrados.append([i[0], i[2], i[3], i[4], i[5], i[6]])

	return {"bilheteUnico": bilhetes_unicos_filtrados, "creditos": creditos}
