'''
	Módulo responsável por enviar todas as
	informações de uma lista de viagens de um usuário
	ao aplicativo
'''
import sys
import json

from pathlib import Path

from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_todas_infos_lista_viagens

#pylint: enable=wrong-import-position


def viagens_faltantes(cur, info_pessoa):
	'''
		Função que devolve informação completa de uma lista de viagens
		de um usuário
	'''
	id_usuario = info_pessoa[0]

	try:
		lista_viagens_faltantes = json.loads(request.form["idsViagens"])
		if not isinstance(lista_viagens_faltantes, list):
			raise ValueError
		lista_viagens_faltantes = [int(i) for i in lista_viagens_faltantes]
	except ValueError:
		return ({'estado': "Erro", 'erro': "Requisição inválida"}, 400)

	info_viagens_faltantes = get_todas_infos_lista_viagens(
	    lista_viagens_faltantes, id_usuario, cur)

	return ({
	    "CPF": request.form['user'],
	    "viagensFaltantes": info_viagens_faltantes
	}, 200)
