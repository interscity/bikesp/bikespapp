'''
	Módulo responsável por gerenciar o logout de usuários
'''

import sys
from pathlib import Path
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.autenticacao.gerencia_sessao import token_valido, remove_sessao

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_por_email, usuario_token_existe

#pylint: enable=wrong-import-position


def logout():
	"""
		Controlador responsável por tratar a tentativa de logout de um usuário
	"""

	#pylint: disable=duplicate-code
	con = conecta()
	cur = con.cursor()

	id_pessoa = get_pessoa_por_email(request.form['email'], cur)

	if id_pessoa is None:
		return ({'estado': "Erro", 'erro': 'Sessao Invalida'}, 403)

	#pylint: enable=duplicate-code

	id_pessoa = id_pessoa[0]
	token = request.form['token']

	busca_token = usuario_token_existe(id_pessoa, token, cur)

	if busca_token is None:
		return ({'estado': "Erro", 'erro': 'Sessao Invalida'}, 403)

	if not token_valido(id_pessoa, token, cur):
		# Pode ser válido devolver algo pra avisar q já estava expirado
		pass

	remove_sessao(id_pessoa, {'token': token})

	con.commit()
	cur.close()
	con.close()

	return ({'estado': "Usuário Deslogado"}, 200)
