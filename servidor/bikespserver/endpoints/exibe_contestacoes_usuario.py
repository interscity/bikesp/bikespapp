'''
	Módulo responsável por devolver ao usuário
	todas as suas contestações
'''
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.consultas import get_contestacoes_usuario
#pylint: enable=wrong-import-position


def exibe_contestacoes_usuario(cur, info_pessoa):
	'''
		Devolve uma lista contendo as contestações
		de um usuário
	'''
	id_pessoa = info_pessoa[0]

	# idViagem,CONTESTACAO.data,justificativa,aprovada,resposta,VIAGEM.remuneracao
	contestacoes_usuario = get_contestacoes_usuario(id_pessoa, cur)

	return {"contestacoes": contestacoes_usuario}
