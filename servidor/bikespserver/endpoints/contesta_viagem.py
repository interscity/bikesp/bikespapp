'''
	Módulo responsável por tratar o recebimento de uma contestação de viagem
'''
import sys
import json
from datetime import datetime, timedelta

from pathlib import Path
from flask import request

import pytz
import dateutil.parser

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_viagem_info
from servidor.bikespserver.bd.consultas import contestacao_existe
from servidor.bikespserver.bd.insercoes import insere_CONTESTACAO

#pylint: enable=wrong-import-position

fuso_horario = pytz.timezone('America/Sao_Paulo')


#pylint: disable=too-many-return-statements disable=too-many-branches
def contesta_viagem(con, cur, info_pessoa):
	'''
		Trata o recebimento de uma contestação de viagem
	'''
	contestacao = request.json['contestacao']
	if isinstance(contestacao, str):
		contestacao = json.loads(contestacao)

	id_usuario = info_pessoa[0]
	id_viagem = contestacao["idViagem"]
	data_contestacao = contestacao["data"]

	try:
		data_contestacao = dateutil.parser.isoparse(data_contestacao)
		data_contestacao = data_contestacao.astimezone(fuso_horario)
		agora = datetime.now(tz=fuso_horario)
		if (data_contestacao - agora) > timedelta(minutes=10):
			return ({'estado': "Erro", 'erro': "Data inválida"}, 400)
	except:  #pylint: disable=bare-except
		return ({'estado': "Erro", 'erro': "Data inválida"}, 400)

	justificativa = contestacao["justificativa"][0:300]

	if justificativa == "":
		return ({'estado': "Erro", 'erro': "Justificativa vazia"}, 400)

	info_viagem = get_viagem_info(id_viagem, cur)

	if info_viagem is None or info_viagem[0] != id_usuario:
		return ({'estado': "Erro", 'erro': "Problemas com a viagem"}, 400)

	if contestacao_existe(str(id_viagem), cur) is not None:
		return ({'estado': "Erro", 'erro': "Viagem já contestada"}, 400)

	contestacao = {
	    "idViagem": id_viagem,
	    "data": data_contestacao,
	    "justificativa": justificativa
	}
	insere_CONTESTACAO(contestacao, cur)

	con.commit()

	return ({'estado': "Contestação enviada"}, 200)
