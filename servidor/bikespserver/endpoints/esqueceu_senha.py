'''
	Módulo responsável por gerenciar a lógica
	do "esqueci minha senha", enviando um email de redefinição
	e lidando com a redefinição propriamente
'''
import sys
from pathlib import Path
from flask import request, url_for, current_app, render_template, abort

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_por_email, usuario_existe
from servidor.bikespserver.email.envia_email import envia_email_flask
from servidor.bikespserver.autenticacao.gerencia_token_redefinicao_senha import salva_token, \
                    gera_token_redefinicao, token_redefinicao_valido, remove_token_redefinicao
from servidor.bikespserver.autenticacao.altera_senha import altera_senha
#pylint: enable=wrong-import-position


def esqueceu_senha(flask_mail, testing=False):
	'''
		Cria um token para redefinição de senha
		e envia um link para redefinição ao email do usuário
	'''
	email_usuario = request.args["email"]
	# print(email_usuario)
	con = conecta()
	cur = con.cursor()

	# Checa se email é valido
	pessoa_info = get_pessoa_por_email(email_usuario, cur)
	if pessoa_info is None:
		return ({
		    'estado': "Erro",
		    "erro": "Pessoa não cadastrada na pesquisa"
		}, 400)

	# Checa se pessoa está cadastrada
	if not usuario_existe(pessoa_info[0], cur):
		return ({
		    'estado': "Erro",
		    "erro": "Usuário não se cadastrou no aplicativo"
		}, 400)

	token_redefinicao = gera_token_redefinicao(email_usuario)

	# Adiciona token na tabela de tokens de redefinição.
	# Se já tiver algum token lá, substituir
	salva_token(pessoa_info[0], token_redefinicao, cur)
	con.commit()

	base_url = url_for('bikesp.__esqueceu_senha',
	                   _external=True,
	                   _scheme=current_app.config["PREFERRED_URL_SCHEME"])

	link_redefinicao = base_url + "redefinirSenha/?token=" + token_redefinicao[
	    "token"]

	if testing:
		return ({"linkRedefinicao": link_redefinicao}, 200)

	mensagem = ("Olá, <br><br>"
	            "Redefina sua senha.\n Acesse o link: "
	            f'<a href="{link_redefinicao}">Redefina sua senha</a><br><br>'
	            "Atenciosamente,<br>"
	            "Equipe Piloto Bike SP")

	envia_email_flask(flask_mail,
	                  assunto="[Piloto Bike SP] Redefinição de senha",
	                  html=mensagem,
	                  enviador=flask_mail.default_sender,
	                  recebedores=[email_usuario])

	return ("Link enviado", 200)


def redefine_senha(testing=False):
	'''
		Recebe uma nova senha e um token e 
		redefine a senha do usuário caso o token seja válido
	'''

	nova_senha = request.form["nova_senha"]
	confirma_senha = request.form["confirma_senha"]
	token = request.form["token"]

	if nova_senha != confirma_senha:
		return ({'estado': "Erro", "erro": "Senhas diferem"}, 400)

	# Confirma se token existe e está na data de validade
	con = conecta()
	cur = con.cursor()
	id_pessoa = token_redefinicao_valido(token, cur)

	if id_pessoa is False:
		return ({
		    'estado': "Erro",
		    "erro": "Link de redefinição de senha expirado."
		}, 400)

	# Altera a senha (removendo junto as sessões existentes)
	if not testing:
		altera_senha(id_pessoa, nova_senha, cur)
	else:
		altera_senha(id_pessoa, nova_senha, cur, chave_cripto="")

	# Reseta tokens de redefinição de senha para usuário
	remove_token_redefinicao(id_pessoa, cur)

	con.commit()
	cur.close()
	con.close()

	return ({"estado": "Sucesso"}, 200)


def redefine_senha_get():
	'''
		Lida com uma resquisiçao GET para a página
		de redefinição de senha. Faz a checagem do token
		antes de carregar o formulário.
	'''
	parametros = request.args
	if "token" not in parametros:
		abort(404)
	token = parametros["token"]
	con = conecta()
	cur = con.cursor()
	id_pessoa = token_redefinicao_valido(token, cur)
	cur.close()
	con.close()
	if id_pessoa is False:
		return render_template('tela_resultado.html',
		                       titulo="Redefinindo Senha",
		                       msgErro="Link de redefinição de senha expirado")
	return render_template('redefinicao_senha.html', token=token)
