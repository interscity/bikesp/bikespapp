'''
	Módulo responsável por gerenciar a lógica
	de trocar o bilhete unico, enviando um email de redefinição
	e lidando com a redefinição propriamente
'''
import sys
from pathlib import Path
from datetime import datetime, timedelta

from flask import request, url_for, current_app

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.insercoes import insere_BILHETEUNICO
from servidor.bikespserver.bd.atualizacoes import atualiza_bilhete_unico
from servidor.bikespserver.bd.consultas import get_pessoa_info, usuario_existe,\
       get_ultima_troca_bu, get_bilhetes_unicos_pessoa,\
       bilhete_unico_existe
from servidor.bikespserver.email.envia_email import envia_email_flask
from servidor.bikespserver.autenticacao.gerencia_sessao import token_valido
from servidor.bikespserver.utils.coords import fuso_horario
from servidor.bikespserver.remuneracao.insere_remuneracao import cria_dict_bilhete_unico
from servidor.bikespserver.utils.parsers import parse_dinheiro_float, parse_bilhete_unico
#pylint: enable=wrong-import-position


def requisita_troca_bu(flask_mail, testing=False):  #pylint: disable=too-many-return-statements
	'''
		Envia um link para redefinição ao email do usuário
	'''
	cpf_usuario = request.args["cpf"]
	token = request.args["token"]

	con = conecta()
	cur = con.cursor()

	#pylint: disable=duplicate-code
	pessoa_info = get_pessoa_info(cpf_usuario, cur)
	if pessoa_info is None:
		return ({
		    'estado': "Erro",
		    "erro": "Pessoa não cadastrada na pesquisa"
		}, 400)

	# Checa se pessoa está cadastrada
	if not usuario_existe(pessoa_info[0], cur):
		return ({
		    'estado': "Erro",
		    "erro": "Usuário não se cadastrou no aplicativo"
		}, 400)
	#pylint: enable=duplicate-code

	email_usuario = pessoa_info[1]
	if email_usuario is None:
		return ({
		    "estado": "Erro",
		    "erro": "Email cadastrado é inválido. Contate os administradores"
		}, 400)

	# Checa validade do token
	if not token_valido(pessoa_info[0], token, cur):
		return ({"estado": "Erro", "erro": "Token inválido"}, 400)

	# Checa última redefinição
	ultima_redefinicao = get_ultima_troca_bu(pessoa_info[0], cur)
	if ultima_redefinicao is not None and not pode_trocar_novamente(
	    ultima_redefinicao[1]):
		return ({
		    "estado": "Erro",
		    "erro": "Bilhete único trocado há pouco tempo"
		}, 400)

	base_url = url_for('bikesp.__requisita_troca_bu',
	                   _external=True,
	                   _scheme=current_app.config["PREFERRED_URL_SCHEME"])
	link_redefinicao = base_url + "redefinirBU/?token=" + token

	if testing:
		return ({"linkRedefinicao": link_redefinicao}, 200)

	mensagem = (
	    "Olá, <br><br>"
	    "Redefina seu bilhete único.\n Acesse o link: "
	    f'<a href="{link_redefinicao}">Redefina seu bilhete único</a><br><br>'
	    "Atenciosamente,<br>"
	    "Equipe Piloto Bike SP")

	envia_email_flask(flask_mail,
	                  assunto="[Piloto Bike SP] Redefinição do bilhete único",
	                  html=mensagem,
	                  enviador=flask_mail.default_sender,
	                  recebedores=[email_usuario])

	return ("Link enviado", 200)


def pode_trocar_novamente(ultima_redefinicao: datetime, limite=30) -> bool:
	'''
		Devolve true se última troca foi há menos de 30 dias
		e false caso contrário
	'''
	if ultima_redefinicao is None:
		return True
	data_redefinicao = fuso_horario.localize(ultima_redefinicao)
	data_minima = data_redefinicao + timedelta(days=limite)
	agora = datetime.now(tz=fuso_horario)
	if agora < data_minima:
		return False
	return True


def redefine_bu():  #pylint: disable=too-many-return-statements
	'''
		Recebe um novo bilhete único e um token e 
		troca o bilhete caso o token seja válido e
		ele não tenha trocado nos últimos 30 dias
	'''

	cpf_usuario = request.form["cpf"]
	novo_bu = request.form["novo_bu"]
	token = request.form["token"]

	if parse_bilhete_unico(novo_bu) != novo_bu:
		return ({
		    'estado': "Erro",
		    "erro": "Bilhete Único inválido. Confira se o digitou corretamente"
		}, 400)

	# Confirma se token existe e está na data de validade
	con = conecta()
	cur = con.cursor()

	pessoa_info = get_pessoa_info(cpf_usuario, cur)

	if pessoa_info is None:
		return ({'estado': "Erro", "erro": "CPF não encontrado"}, 400)

	# Checa se pessoa está cadastrada pylint: disable = duplicate-code
	if not usuario_existe(pessoa_info[0], cur):
		return ({
		    'estado': "Erro",
		    "erro": "Usuário não se cadastrou no aplicativo"
		}, 400)

	if not token_valido(pessoa_info[0], token, cur):
		return ({'estado': "Erro", "erro": "Requisição inválida"}, 400)

	# Checa última redefinição
	ultima_redefinicao = get_ultima_troca_bu(pessoa_info[0], cur)
	if ultima_redefinicao is not None and not pode_trocar_novamente(
	    ultima_redefinicao[1]):
		return ({
		    "estado": "Erro",
		    "erro": "Bilhete único trocado há pouco tempo"
		}, 400)

	if bilhete_unico_existe(novo_bu, cur) is not None:
		return ({"estado": "Erro", "erro": "Bilhete único inválido"}, 400)

	adiciona_novo_bu(pessoa_info[0], novo_bu, cur)

	con.commit()
	cur.close()
	con.close()

	return ({"estado": "Sucesso"}, 200)


def adiciona_novo_bu(id_pessoa, bilhete_unico, cur):
	'''
		Adiciona propriamente uma nova entrada na tabela
		BILHETEUNICO
	'''

	# Desativa todos os bilhetes unicos ativos do usuario
	# e transporta valor aguardandoEnvio de outros bilhetes unicos para o novo
	bilhetes_unicos = get_bilhetes_unicos_pessoa(id_pessoa, cur)
	soma_aguardando_envio = 0
	for bilhete in bilhetes_unicos:
		num = bilhete[0]
		data_inicio = bilhete[2]
		data_fim = bilhete[3]
		ativo = bilhete[4]
		concedido = parse_dinheiro_float(bilhete[5])
		aguardando_envio = parse_dinheiro_float(bilhete[6])
		aguardando_resposta = parse_dinheiro_float(bilhete[7])

		soma_aguardando_envio += aguardando_envio

		# Se bilhete já está inativo, com dataFim e sem aguardando envio, não atualizar
		if not ativo and data_fim is not None and aguardando_envio == 0.0:
			continue

		if ativo:
			ativo = False
		if data_fim is None:
			data_fim = datetime.now(tz=fuso_horario)

		att_bu_dict = cria_dict_bilhete_unico(num, id_pessoa, data_inicio,
		                                      data_fim, ativo, concedido, 0,
		                                      aguardando_resposta)
		atualiza_bilhete_unico(att_bu_dict, cur)

	# Adiciona nova entrada na tabela
	novo_bu_dict = cria_dict_bilhete_unico(bilhete_unico, id_pessoa,
	                                       datetime.now(tz=fuso_horario), None,
	                                       True, 0, soma_aguardando_envio, 0)
	insere_BILHETEUNICO(novo_bu_dict, cur)
