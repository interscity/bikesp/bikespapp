'''
	Módulo responsável por gerenciar o cadastro de usuários
'''

import logging
import sys
from pathlib import Path
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_info, usuario_existe,\
          get_pessoa_bilhete_unico
from servidor.bikespserver.bd.insercoes import insere_USUARIO

from servidor.bikespserver.utils.parsers import parse_email

#pylint: enable=wrong-import-position


def cadastra_usuario():
	"""
		Controlador responsável por tratar a tentativa do cadastro de um novo
		usuário
	"""

	cpf = request.form["cpf"]
	con = conecta()
	cur = con.cursor()

	pessoa = get_pessoa_info(cpf, cur)

	id_pessoa = get_pessoa_bilhete_unico(request.form["bu"], cur)

	cpf_fail = pessoa is None
	email_fail = pessoa is None or pessoa[1] != parse_email(
	    request.form["email"])
	bu_fail = id_pessoa is None

	if cpf_fail or email_fail or bu_fail:
		logging.getLogger("bikespserver").debug(
		    "Tentativa de cadastro:\n\tCPF: %s[%s]\n\tEmail: %s[%s]\n\tBU: %s[%s]\n",
		    request.form["cpf"], "!" if cpf_fail else " ",
		    parse_email(request.form["email"]), "!" if email_fail else " ",
		    request.form["bu"], "!" if bu_fail else " ")

		return ({
		    'estado': 'Erro',
		    'erro': 'Aguarde a habilitação do seu perfil pelos desenvolvedores'
		}, 401)

	id_pessoa = id_pessoa[0]

	if usuario_existe(id_pessoa, cur):
		return ({'estado': 'Erro', 'erro': 'Usuário Já Cadastrado'}, 403)

	insere_USUARIO({"idPessoa": id_pessoa, "senha": request.form['senha']}, cur)

	con.commit()
	cur.close()

	return ({'estado': "Usuário Cadastrado"}, 200)
