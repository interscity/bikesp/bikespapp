'''
	Módulo resposável por realizar a alteração de localizações no BD,
	gerencia o endpoint /api/alteraLocalizacao
'''

import sys
from pathlib import Path
from datetime import date
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.atualizacoes import atualiza_validacao_locs_pessoa
from servidor.bikespserver.bd.consultas import alteracao_localizacao_existe
from servidor.bikespserver.gerenciamento.atualiza_localizacao_pessoa import \
 atualiza_localizacao_pessoa, atualiza_apelido_localizacao_pessoa
from servidor.bikespserver.bd.insercoes import insere_ALTERACAOLOCALIZACAO

#pylint: enable=wrong-import-position


def checa_alteracao_localizacao(cur, info_pessoa):
	'''
		Checa se o usuário realizou a última alteração de uma
		localização a mais de 30 dias. Caso não, retorna False
		e quantos dias faltam até poder realizar a alteração.
	'''
	id_pessoa = info_pessoa[0]

	resposta = alteracao_localizacao_existe(id_pessoa, cur)

	if resposta is None or (date.today() - resposta[0]).days > 30:
		return ({'resposta': True}, 200)

	return ({
	    'resposta': False,
	    'dias': 31 - int((date.today() - resposta[0]).days)
	}, 200)


def altera_localizacao(con, cur, info_pessoa):
	'''
		Realiza a alteração do endereço e/ou do apelido de
		uma localização no BD para um usuário
	'''
	id_pessoa = info_pessoa[0]
	id_localizacao = request.form["id_localizacao"]
	endereco = request.form["novo_endereco"]
	apelido = request.form["novo_apelido"]

	# Verifica se usuário tem permissão para alteração
	(resposta, status) = checa_alteracao_localizacao(cur, info_pessoa)
	if not resposta['resposta']:
		return ({'estado': 'Erro', 'erro': 'Alteração não autorizada.'}, 403)

	# Realiza mudança do apelido
	(sucesso,
	 status) = atualiza_apelido_localizacao_pessoa(id_pessoa, id_localizacao,
	                                               apelido, cur)

	if not sucesso:
		return ({'estado': 'Erro', 'erro': status}, 403)

	# Realiza mudança do endereço
	(sucesso, status) = atualiza_localizacao_pessoa(id_pessoa, id_localizacao,
	                                                endereco, cur)

	if not sucesso:
		return ({'estado': 'Erro', 'erro': status}, 403)

	# Se o endereço foi alterado, marca localizações como não validadas
	if status != "Localização não alterada":
		atualiza_validacao_locs_pessoa(id_pessoa, False, cur)

	# Salva que a alteração foi realizada pelo usuário
	cur = insere_ALTERACAOLOCALIZACAO(id_pessoa, cur)

	con.commit()
	return ({'estado': 'Sucesso'}, 200)
