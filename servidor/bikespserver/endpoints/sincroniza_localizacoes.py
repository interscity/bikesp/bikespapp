'''
	Módulo responsável por enviar as localizações de um usuário ao aplicativo
'''
import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_todas_localizacoes_pessoa

#pylint: enable=wrong-import-position


def sincroniza_localizacoes(cur, info_pessoa):
	'''
		Função que devolve as localizacoes de um usuário
	'''

	id_pessoa = info_pessoa[0]

	# idLocalizacao,coordenadas,tipoLocalizacao,apelido
	# para estações o apelido é substituido pelo nome da estação
	locs = get_todas_localizacoes_pessoa(id_pessoa, cur)
	return ({'locs': locs}, 200)
