'''
	Módulo responsável por devolver ao usuário
	todos os bônus (que ele tenha cumprido ou não)
'''
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.consultas import get_bonus_usuario
#pylint: enable=wrong-import-position


def exibe_bonus_usuario(cur, info_pessoa):
	'''
		Devolve uma lista contendo o todos os bônus visíveis. Devolve
		também, para cada bônus, se o usuário o completou 
	'''
	id_pessoa = info_pessoa[0]

	# idBonus,nome,valor,ativo,descricao,dataInicio,dataFim,dataConcessao
	bonus_usuario = get_bonus_usuario(id_pessoa, cur)

	return {"bonusUsuario": bonus_usuario}
