'''
	Módulo responsável por enviar as informações de pares válidos
	(distância e trajeto ideal) para o usuário
'''
import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_todas_localizacoes_pessoa, \
 dump_pares_localizacoes

#pylint: enable=wrong-import-position


def envia_pares_validos(cur, info_pessoa):
	'''
		Função que devolve informações básicas das viagens de um usuário
	'''

	id_usuario = info_pessoa[0]

	locs_usuario = get_todas_localizacoes_pessoa(id_usuario, cur)
	pares_validos = {}
	lista_ids_localizacoes = [loc[0] for loc in locs_usuario]
	dump_pares_validos = dump_pares_localizacoes(lista_ids_localizacoes, cur)
	for par_valido in dump_pares_validos:
		id_ponto1 = par_valido[0]
		id_ponto2 = par_valido[1]

		if id_ponto1 not in lista_ids_localizacoes or id_ponto2 not in lista_ids_localizacoes:
			continue

		distancia = par_valido[2]
		trajeto = par_valido[3]
		key_dict = str(id_ponto1) + "&" + str(id_ponto2)

		if key_dict in pares_validos:
			continue
		pares_validos[key_dict] = {"distancia": distancia, "trajeto": trajeto}

	return ({'paresValidos': pares_validos}, 200)
