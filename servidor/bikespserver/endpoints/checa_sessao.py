'''
	Módulo responsável por gerenciar o endpoint /checaSess/ , que checa 
	se uma sessão de usuário ainda é válida
'''

import os
import sys
from pathlib import Path
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.autenticacao.gerencia_sessao import token_valido

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_por_email

#pylint: enable=wrong-import-position


def checa_sessao():
	"""
		Controlador responsável por tratar o teste de uma sessão de cliente
	"""

	#pylint: disable=duplicate-code
	con = conecta()
	cur = con.cursor()

	id_pessoa = get_pessoa_por_email(request.form['email'], cur)

	if id_pessoa is None:
		return ({'estado': "Erro", 'erro': 'Sessao Invalida'}, 403)

	id_pessoa = id_pessoa[0]
	token = request.form['token']

	if not token_valido(id_pessoa, token, cur):
		return ({'estado': "Erro", 'erro': 'Sessao Invalida'}, 403)

	versao_app_minima = os.getenv("MIN_VERSAO_APP", "")

	versao_app_cliente = ""
	if 'versao' in request.form:
		versao_app_cliente = request.form['versao']

	if versao_app_cliente < versao_app_minima:
		return ({'estado': "Erro", 'erro': 'App desatualizado'}, 403)

	#pylint: enable=duplicate-code

	cur.close()
	con.close()

	return ({'estado': "OK"}, 200)
