'''
	Módulo responsável por gerenciar a requisição contendo a viagem
'''
import json
import sys

from pathlib import Path

from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.viagem import Viagem

from servidor.bikespserver.feedback.gerencia_feedback_viagem import requisita_feedback_viagem

from servidor.bikespserver.viagem.validaviagem.valida_viagem \
 import valida_viagem_bike, viagem_duplicada

from servidor.bikespserver.remuneracao.insere_remuneracao import insere_remuneracao_para_enviar

#pylint: enable=wrong-import-position


def registra_viagem(con, cur, info_pessoa):
	"""
		Controlador que trata o recebimento de uma viagem pela API
	"""
	usuario = request.form['user']

	id_usuario = info_pessoa[0]
	id_grupo_pesquisa = info_pessoa[2]

	viagem_request = Viagem.from_json(json.loads(request.form['viagem']))

	if viagem_duplicada(viagem_request, id_usuario, cur):
		return ({"estado": "Erro", "erro": "Viagem duplicada"}, 400)

	dict_resposta = valida_viagem_bike(viagem_request, id_usuario,
	                                   id_grupo_pesquisa, cur)

	insere_remuneracao_para_enviar(id_usuario, dict_resposta["remuneracao"],
	                               cur)
	con.commit()

	# Responder request
	dict_resposta['CPF'] = usuario
	requisita_feedback_viagem(id_usuario, dict_resposta["idViagem"],
	                          dict_resposta["motivoStatus"], cur)
	con.commit()
	return (dict_resposta, 200)
