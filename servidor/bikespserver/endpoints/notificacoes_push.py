"""
    Módulo resposável por lidar com as notificações push a serem enviadas para o aplicativo
"""
import sys
from pathlib import Path
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.insercoes import insere_TOKENNOTIFICACAO

#pylint: enable=wrong-import-position


def registra_token_notificacao(con, cur, info_pessoa):
	"""
        Registra o token do usuário para envio de notificações push.
    """
	token_notif = request.form["tokenNotif"]

	id_usuario = info_pessoa[0]

	insere_TOKENNOTIFICACAO({"idPessoa": id_usuario, "token": token_notif}, cur)
	con.commit()
	return ({"tokenNotif": token_notif}, 200)
