'''
	Módulo responsável por enviar as viagens de um usuário ao aplicativo
'''
import sys

from pathlib import Path

from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_viagens_pessoa

#pylint: enable=wrong-import-position


def sincroniza_viagens(cur, info_pessoa):
	'''
		Função que devolve informações básicas das viagens de um usuário
	'''
	id_usuario = info_pessoa[0]

	viagens = get_viagens_pessoa(id_usuario, cur)
	return ({'CPF': request.form['user'], 'viagens': viagens}, 200)
