'''
	Módulo responsável por consultar uma viagem a respeito
	de seu trajeto
'''

import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_trajeto_viagem
#pylint: enable=wrong-import-position


def consulta_trajeto(id_viagem, force_prod=False):
	'''
		Devolve a string representando um trajeto,
		dado um idViagem
	'''
	con = conecta(force_prod)
	cur = con.cursor()

	trajeto = get_trajeto_viagem(id_viagem, cur)

	if trajeto is None:
		trajeto = "{}"
	else:
		trajeto = str(trajeto[0]).replace("'", '"')

	return trajeto
