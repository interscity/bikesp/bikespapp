"""
	Módulo responsável por exibir a página que visualiza um viagem
"""

import urllib.parse
import sys
from pathlib import Path
# from flask import render_template

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import dump_viagem

from servidor.bikespserver.utils.parsers import json_para_geoJSON
#pylint: enable=wrong-import-position


def visualiza_viagem(id_viagem):
	"""
		Controlador responsável por tratar uma requisição ao endpoint que visualiza uma viagem
	"""

	con = conecta()
	cur = con.cursor()

	viagem_bd = dump_viagem(id_viagem, cur)
	trajeto = str(viagem_bd[9])

	# print(trajeto)

	geo_json = json_para_geoJSON(trajeto)

	dados = urllib.parse.quote(geo_json)

	comeco = "http://geojson.io/#data=data:application/json,"

	con.close()

	return "<a href=" + comeco + dados + ">Clique aqui</a>"


#render_template('visualiza_trajeto.html', token=comeco)
