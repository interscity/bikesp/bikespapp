"""
Módulo com constantes utilizadas na validação das viagens
"""

NUM_MAX_VIAGENS_REMUNERADAS_DIA = 2
NUM_MAX_VIAGENS_REMUNERADAS_MES = 50

PORCAO_ORIGEM = 0.3  # os 30% iniciais de um trajeto podem ser considerados origem
PORCAO_DESTINO = 0.3  # os 30% finais  de um trajeto podem ser considerados destino

TEMPO_MAXIMO_DENTRO_RAIO = 120

# --- Validação por Activities ---

FORMATO_DATA_ACTIVITIES = "%a %b %d %H:%M:%S %Z%z %Y"
NUM_MIN_ACITVITIES_KM = 5

CONFIANCA_MINIMA = 85
TEMPO_MAX_SEM_ACTIVITIES = 60 * 5  # em segundos

PCNTG_ACTIVITIES_REPROVACAO = 0.55
PCNTG_ACTIVITIES_APROVACAO = 0.60

# --- Validação por GPS ---

NUM_MIN_AMOSTRAS_KM = 5
NUM_MIN_AMOSTRAS_TRAJETO = 15  # Deixei para não quebrar testes

PRECISAO_MINIMA = 250

LIMITE_INF_80_PCNTIL = 2.0
LIMITE_SUP_80_PCNTIL = 12.5

COEF_ATENUACAO_TAMANHO = 0.97
DIV_ATENUACAO = 150

TEMPO_MAX_SEM_AMOSTRA = 60 * 2  # em segundos

# O valor abaixo não pode ser menor que app/utils - TIMEOUT_REVIVER_TRAJETO_ABSURDO
DELTAT_AMOSTRA_MUITO_ANTIGA = 60 * 60 * 5  # segundos;

NUM_MAX_PAUSAS = 3
