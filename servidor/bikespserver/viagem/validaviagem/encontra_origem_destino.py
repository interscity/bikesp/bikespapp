"""
Arquivo para o código da função que extrai os ids de origem e destino
de um dado trajeto
"""

import sys
import math

from pathlib import Path

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.viagem import Viagem

from servidor.bikespserver.bd.consultas import get_todas_localizacoes_pessoa

from servidor.bikespserver.utils.coords import ponto_proximo_localizacao_usuario

from servidor.bikespserver.viagem.validaviagem.consts import PORCAO_ORIGEM,\
            PORCAO_DESTINO, LIMITE_INF_80_PCNTIL,\
            TEMPO_MAXIMO_DENTRO_RAIO

from servidor.bikespserver.classes.deslocamento_atomico import DeslocamentoAtomico

#pylint: enable=wrong-import-position


#pylint: disable=too-many-locals
def encontra_origem_destino(viagem_req: Viagem, id_usuario: int | str,
                            cur: psycopg2.extensions.cursor):
	'''
		Encontra a origem e o destino de uma viagem recebida de um certo usuário.
	Atualiza as propriedades da instância da classe Viagem correspondente.
	'''
	id_origem = None
	idx_origem = -1
	data_origem = None

	id_destino = None
	idx_destino = -1

	viagem_req.trajeto.sort(key=lambda x: x["Data"])

	locais_usuario = get_todas_localizacoes_pessoa(id_usuario, cur)
	for idx_medicao in range(0,
	                         math.ceil(len(viagem_req.trajeto) * PORCAO_ORIGEM),
	                         1):

		medicao = viagem_req.trajeto[idx_medicao]
		ponto = medicao["Posicao"]
		resultado = ponto_proximo_localizacao_usuario(ponto, locais_usuario)

		if resultado is not None and (idx_origem == -1 or
		                              resultado[0] == id_origem):
			id_origem = resultado[0]
			idx_origem = idx_medicao
			data_origem = medicao["Data"]

	for idx_medicao in range(
	    0, math.ceil(len(viagem_req.trajeto) * PORCAO_DESTINO)):

		medicao = list(reversed(viagem_req.trajeto))[idx_medicao]
		ponto = medicao["Posicao"]
		resultado = ponto_proximo_localizacao_usuario(ponto, locais_usuario)

		if resultado is not None and (data_origem is None or
		                              data_origem <= medicao["Data"]):
			id_destino = resultado[0]
			# print("Destino primeiro:", len(viagem_req.trajeto) - idx_medicao - 1)
			idx_destino = len(viagem_req.trajeto) - idx_medicao - 1
			break

	if idx_origem != -1 and idx_destino != -1 and id_origem != id_destino:

		local_destino = list(
		    filter(lambda x: x[0] == id_destino, locais_usuario))

		assert len(local_destino) == 1

		## Achar índice do primeiro ponto do trajeto que é dessa localização

		tempo_no_raio = 0
		for inc in range(idx_destino - idx_origem):
			medicao = viagem_req.trajeto[idx_origem + inc]
			ponto = medicao["Posicao"]

			resultado = ponto_proximo_localizacao_usuario(ponto, local_destino)
			if resultado is not None and (data_origem is None or
			                              data_origem < medicao["Data"]):
				if inc != 0:
					ultima_medicao = viagem_req.trajeto[idx_origem + inc - 1]
					finaliza_busca, tempo_no_raio = finaliza_busca_idx_destino(
					    medicao, ultima_medicao, tempo_no_raio)
					if idx_destino != -1 and finaliza_busca:
						break

				idx_destino = idx_origem + inc

	# print("Origem:",idx_origem,"Destino:",idx_destino)

	viagem_req.associa_origem_destino(id_origem, idx_origem, id_destino,
	                                  idx_destino, cur)


#pylint: enable=too-many-locals


def finaliza_busca_idx_destino(medicao_atual: dict, ultima_medicao: dict,
                               tempo_no_raio: int) -> list:
	'''
		Se a velocidade entre medições for pequena ou o tempo dentro do raio for grande
		a busca pelo idx_destino é finalizada
	'''

	deslocamento_atomico = DeslocamentoAtomico(ultima_medicao, medicao_atual)
	tempo_no_raio += deslocamento_atomico.delta_t
	velocidade = deslocamento_atomico.vel
	if (velocidade < LIMITE_INF_80_PCNTIL) or (tempo_no_raio
	                                           > TEMPO_MAXIMO_DENTRO_RAIO):
		return [True, tempo_no_raio]
	return [False, tempo_no_raio]
