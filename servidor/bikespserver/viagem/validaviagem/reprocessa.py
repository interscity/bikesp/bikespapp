'''
    Responsável por reprocessar uma viagem armazenada no BD
'''

import sys

from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import dump_viagem, get_pessoa_info_pelo_id_pessoa
from servidor.bikespserver.classes.viagem import Viagem
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.viagem.validaviagem.valida_viagem import valida_viagem_bike
from servidor.bikespserver.utils.parsers import parse_dinheiro_float

from servidor.bikespserver.remuneracao.insere_remuneracao import insere_remuneracao_para_enviar

#pylint: enable=wrong-import-position


def reprocessa_viagem(id_viagem: str | int, con) -> list:
	'''
        Recebe o ID de uma viagem e a reprocessa
    '''
	estado = "SUCESSO"

	cur = con.cursor()

	infos_viagem_antes = dump_viagem(id_viagem, cur)
	if infos_viagem_antes is None:
		estado = "VIAGEM_INEXISTENTE"
		return [estado, [{}, {}]]

	viagem_antes = cria_dict_viagem(id_viagem, infos_viagem_antes)
	viagem_antes_request = Viagem.from_json(viagem_antes)

	viagem_antes_request.id_viagem = id_viagem

	id_pessoa = viagem_antes["idPessoa"]
	id_grupo_pesquisa = get_pessoa_info_pelo_id_pessoa(id_pessoa, cur)[2]

	dict_resposta = valida_viagem_bike(viagem_antes_request, id_pessoa,
	                                   id_grupo_pesquisa, cur)
	remuneracao_antes = parse_dinheiro_float(viagem_antes["remuneracao"])
	remuneracao_depois = float(dict_resposta["remuneracao"])

	if remuneracao_antes < remuneracao_depois:
		insere_remuneracao_para_enviar(id_pessoa,
		                               remuneracao_depois - remuneracao_antes,
		                               cur)
		estado = "SUCESSO"
	elif remuneracao_antes == remuneracao_depois:
		estado = "SUCESSO"
	else:
		estado = "REMUNERAÇÃO_NOVA_MENOR"

	infos_viagem_depois = dump_viagem(id_viagem, cur)
	viagem_depois = cria_dict_viagem(id_viagem, infos_viagem_depois)

	cur.close()
	return [estado, [viagem_antes, viagem_depois]]


class SimulaReq():  # pylint: disable=too-few-public-methods
	'''
		Classe criada apenas para simular uma requisição
	'''

	def __init__(self, data_inicio, trajeto):
		self.data_inicio = data_inicio
		self.trajeto = trajeto


def cria_dict_viagem(id_viagem: str | int, viagem_tupla: tuple) -> dict:  # pylint: disable=too-many-locals
	'''
        Recebe uma tupla da viagem vinda do servidor
        e devolve um dict para representá-la
    '''

	id_viagem = viagem_tupla[0]
	id_pessoa = viagem_tupla[1]
	datetime_data_inicio = viagem_tupla[2]
	data_inicio = datetime.timestamp(datetime_data_inicio)
	deslocamento = viagem_tupla[3]
	id_origem = viagem_tupla[4]
	id_destino = viagem_tupla[5]
	status = viagem_tupla[6]
	remuneracao = viagem_tupla[7]
	motivo_status = viagem_tupla[8]
	trajeto = viagem_tupla[9]
	activity_trip = viagem_tupla[10]
	metadados = viagem_tupla[11]

	assert len(viagem_tupla) == 12

	viagem_bd = ViagemBD(id_pessoa, SimulaReq(data_inicio, trajeto),
	                     deslocamento, id_origem, id_destino, status,
	                     remuneracao, motivo_status, activity_trip, metadados)

	duracao = viagem_bd.get_duracao_viagem() if len(trajeto) != 0 else 0

	return {
	    "idViagem": id_viagem,
	    "idPessoa": id_pessoa,
	    "dataInicio": data_inicio,
	    "duracao": duracao,
	    "deslocamento": deslocamento,
	    "origem": id_origem,
	    "destino": id_destino,
	    "status": status,
	    "remuneracao": remuneracao,
	    "motivoStatus": motivo_status,
	    "trajeto": trajeto,
	    "activityRecognitionTrip": activity_trip,
	    "metadados": metadados
	}
