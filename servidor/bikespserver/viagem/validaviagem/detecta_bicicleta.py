"""
Arquivo para o código principal da detecção do meio de transporte
do trajeto.
"""

import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.deslocamento_atomico import DeslocamentoAtomico

from servidor.bikespserver.viagem.validaviagem.consts import LIMITE_INF_80_PCNTIL
from servidor.bikespserver.viagem.validaviagem.consts import LIMITE_SUP_80_PCNTIL
from servidor.bikespserver.viagem.validaviagem.consts import COEF_ATENUACAO_TAMANHO, DIV_ATENUACAO

#pylint: enable=wrong-import-position


def detecta_bicicleta(trajeto: list[dict]):
	"""
		Função responsável por checar se uma sequência de pontos corresponde
	a um trajeto feito de bicicleta. Devolve False caso considera que a viagem
	não tenha sido feita de bicicleta ou se os dados são insuficientes
	"""

	try:
		deslocamentos, tempo_total = cria_deslcoamentos_atomicos(trajeto)
	except (ValueError, ZeroDivisionError) as err:
		print(err)
		return False

	num_amostras = len(deslocamentos) + 1
	quantif_amostras = num_amostras / DIV_ATENUACAO

	atenuacao = COEF_ATENUACAO_TAMANHO**(quantif_amostras)

	restricoes = [
	    Restricao(4 / 5, '<=', LIMITE_SUP_80_PCNTIL * atenuacao,
	              "Viagem muito rápida"),
	    Restricao(4 / 5, '>', LIMITE_INF_80_PCNTIL * atenuacao,
	              "Viagem muito devagar")
	]

	return analisa_vel_percentis(restricoes, deslocamentos, tempo_total)


def cria_deslcoamentos_atomicos(
        trajeto: list[dict]) -> tuple[list[DeslocamentoAtomico], float]:
	"""
		Função auxiliar. Dada uma lista com dicionários correspondendo
	a cada um dos pontos do trajeto, devolve uma lista de objetos da classe
	DeslocamentoAtomico correspondente e a soma do tempo decorrido entre
	os pares de amostras.
	"""

	deslocamentos = []
	soma_tempo = 0
	i = 0
	while i < len(trajeto) - 1:
		# Não adicionar no soma_tempo nem no deslocamento deslocamento entre pausas
		if "DataInicio" in trajeto[i + 1].keys():
			i += 2
			continue
		if "DataInicio" in trajeto[i].keys():
			i += 1
			continue

		novo_desc = DeslocamentoAtomico(trajeto[i], trajeto[i + 1])
		deslocamentos.append(novo_desc)
		soma_tempo += novo_desc.delta_t

		i += 1

	return deslocamentos, soma_tempo


class Restricao:
	"""
		Classe responsável por representar uma restrição que
	deve ser satisfeita para que uma viagem seja aprovada como de 
	bicicleta.
	"""

	def __init__(self, pcntg, comp, limite, msg_falha):
		self.pcntg = pcntg
		self.comp = comp
		self.limite = limite
		self.msg_falha = msg_falha

	def get_pcntg(self):
		"""
			Devolve o percentil relativo à aplicação 
		da restrição
		"""
		return self.pcntg

	def vale_para(self, valor):
		"""
			Devolve um true caso a restricao seja
		satisfeita para o valor dado.
		"""
		match self.comp:
			case '<':
				return valor < self.limite
			case '>':
				return valor > self.limite
			case '<=':
				return valor <= self.limite
			case '>=':
				return valor >= self.limite

	def log_falha(self):
		"""
			Imprime a mensagem de log (falha) da 
		restricao
		"""
		print(self.msg_falha)

	def __str__(self):
		return "val " + self.comp + str(self.limite)


def analisa_vel_percentis(restricoes: list[Restricao], deslocamentos,
                          tempo_total) -> bool:
	"""
		Analisa os valores dos percentis de velocidades médias
	dos deslocamentos atômicos dados com base nas restrições dadas.
	"""
	restricoes.sort(key=lambda x: x.pcntg)
	deslocamentos.sort(key=lambda x: x.vel)

	ind_deslocamento = 0

	soma_tempo = 0

	for restricao in restricoes:

		while soma_tempo < (restricao.get_pcntg() * tempo_total):
			soma_tempo += deslocamentos[ind_deslocamento].delta_t
			ind_deslocamento += 1

		ind_deslocamento = min(ind_deslocamento, len(deslocamentos) - 1)

		if not restricao.vale_para(deslocamentos[ind_deslocamento].vel):
			restricao.log_falha()
			return False

	return True
