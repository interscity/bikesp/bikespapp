"""
Arquivo principal do Módulo. Deve invocar as funções dos demais arquivos
do módulo para avaliar (aceitar ou rejeitar) uma viagem.
"""

import os
import sys

from pathlib import Path
from datetime import datetime

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.viagem import Viagem

from servidor.bikespserver.bd.consultas import grupo_pesquisa_existe
from servidor.bikespserver.bd.consultas import get_viagens_pessoa
from servidor.bikespserver.bd.consultas import viagem_existe

from servidor.bikespserver.utils.coords import fuso_horario

from servidor.bikespserver.utils import parsers

from servidor.bikespserver.viagem.validaviagem.consts import (
    PRECISAO_MINIMA, NUM_MIN_AMOSTRAS_KM, NUM_MAX_VIAGENS_REMUNERADAS_DIA,
    NUM_MAX_VIAGENS_REMUNERADAS_MES, TEMPO_MAX_SEM_AMOSTRA,
    DELTAT_AMOSTRA_MUITO_ANTIGA, NUM_MAX_PAUSAS)
from servidor.bikespserver.viagem.validaviagem.detecta_bicicleta import detecta_bicicleta
from servidor.bikespserver.viagem.validaviagem.encontra_origem_destino \
 import encontra_origem_destino
from servidor.bikespserver.viagem.validaviagem.valida_por_activities import valida_por_activities
#pylint: enable=wrong-import-position


def valida_viagem_bike(viagem: Viagem, id_usuario: int | str,
                       id_grupo_pesquisa: int | str,
                       cur: psycopg2.extensions.cursor) -> dict:
	"""
	Dados um objeto Viagem, as informações do usuário que a realizou e
	um cursor de conexão com o BD, decide se a viagem deve ser remunerada e gera
	um dict com as informações correspondentes à análise.
	"""

	viagem.associa_usuario(id_usuario)

	viagem.checa_num_max_pausas(NUM_MAX_PAUSAS)

	encontra_origem_destino(viagem, id_usuario, cur)

	if os.getenv("INVALIDA_EMULADORES", "True").lower() != "false":
		viagem.detecta_emulador()

	ja_validada_por_activities = valida_por_activities(viagem)
	if (not viagem.status == "Reprovado" and
	    not ja_validada_por_activities) and reprova_por_gps(viagem):
		viagem.reprova("NAO_BICICLETA")

	elif atingiu_limite_viagens(id_usuario, viagem.data_inicio, cur):
		viagem.reprova("LIMITE_VIAGENS_EXCEDIDO")

	elif viagem_antecipa_piloto(viagem.data_inicio):
		viagem.reprova("PILOTO_NAO_INICIADO")
	else:
		# Aprovada
		remuneracao_grupo = 0.0
		if id_grupo_pesquisa is not None:
			remuneracao_grupo = parsers.parse_dinheiro_float(
			    grupo_pesquisa_existe(id_grupo_pesquisa, cur)[1])

		viagem.calcula_remuneracao(remuneracao_grupo)

	id_viagem = viagem.salvar_no_bd(cur)

	return {
	    'estadoViagem': viagem.status,
	    'remuneracao': viagem.remuneracao,
	    'trajetoIdeal': viagem.trajeto_ideal,
	    'distanciaIdeal': viagem.deslocamento,
	    'motivoStatus': viagem.motivo_reprovacao,
	    'idOrigem': viagem.id_origem,
	    'idDestino': viagem.id_destino,
	    'idViagem': id_viagem,
	}


def reprova_por_gps(viagem: Viagem):
	"""
		Devolve True caso a viagem seria reprovada usando as medições do GPS
	"""
	viagem.checa_num_min_pontos(NUM_MIN_AMOSTRAS_KM)

	nucleo_limpo = viagem.extrai_nucleo_trajeto_utilizavel(
	    PRECISAO_MINIMA, NUM_MIN_AMOSTRAS_KM, TEMPO_MAX_SEM_AMOSTRA,
	    DELTAT_AMOSTRA_MUITO_ANTIGA)

	if not viagem.status == "Reprovado" and not detecta_bicicleta(nucleo_limpo):
		viagem.reprova("NAO_BICICLETA")
		return True

	return False


def atingiu_limite_viagens(id_pessoa: int | str,
                           data_viagem_timestamp: int | float,
                           cur: psycopg2.extensions.cursor) -> bool:
	"""
		Checa se usuário já atingiu o limite de viagens (remuneradas) a partir da data da viagem
	"""
	limite_diario = NUM_MAX_VIAGENS_REMUNERADAS_DIA
	limite_mensal = NUM_MAX_VIAGENS_REMUNERADAS_MES

	data_viagem = datetime.fromtimestamp(data_viagem_timestamp, tz=fuso_horario)
	ontem_viagem = datetime(data_viagem.year, data_viagem.month,
	                        data_viagem.day, 0, 0)
	inicio_mes = datetime(data_viagem.year, data_viagem.month, 1, 0, 0)

	viagens_24hrs = get_viagens_pessoa(
	    id_pessoa, cur, intervalo_data=[ontem_viagem, data_viagem])
	if len([
	    viagem for viagem in viagens_24hrs
	    if parsers.parse_dinheiro_float(viagem[2]) > 0
	]) >= limite_diario:
		return True

	viagens_mes = get_viagens_pessoa(id_pessoa,
	                                 cur,
	                                 intervalo_data=[inicio_mes, data_viagem])
	if len([
	    viagem for viagem in viagens_mes
	    if parsers.parse_dinheiro_float(viagem[2]) > 0
	]) >= limite_mensal:
		return True

	return False


def viagem_antecipa_piloto(data_viagem_timestamp):
	"""
		Devolve true caso a viagem de data passada tenha sido feita antes do início
	do período de testes do piloto (marcado na variável do ENV "INICIO_PILOTO_TIMESTAMP")
	"""
	data_viagem = datetime.fromtimestamp(data_viagem_timestamp, tz=fuso_horario)
	inicio_piloto = datetime.fromtimestamp(int(
	    os.getenv("INICIO_PILOTO_TIMESTAMP", "1000")),
	                                       tz=fuso_horario)
	return data_viagem < inicio_piloto


def viagem_duplicada(viagem: Viagem, id_usuario: int | str,
                     cur: psycopg2.extensions.cursor) -> bool:
	'''
		Devolve true caso viagem já esteja no BD
	'''
	data_viagem = datetime.fromtimestamp(viagem.data_inicio, tz=fuso_horario)
	if viagem_existe(id_usuario, data_viagem, cur) is None:
		return False
	return True
