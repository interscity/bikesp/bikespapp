"""
	Módulo responsável pela validação de uma viagem usando o google / android activities
"""

import sys

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.viagem import Viagem

# from servidor.bikespserver.viagem.validaviagem.consts import PRECISAO_MINIMA
from servidor.bikespserver.viagem.validaviagem.consts import NUM_MIN_ACITVITIES_KM
from servidor.bikespserver.viagem.validaviagem.consts import CONFIANCA_MINIMA
from servidor.bikespserver.viagem.validaviagem.consts import PCNTG_ACTIVITIES_REPROVACAO
from servidor.bikespserver.viagem.validaviagem.consts import PCNTG_ACTIVITIES_APROVACAO
# from servidor.bikespserver.viagem.validaviagem.consts import DELTAT_AMOSTRA_MUITO_ANTIGA

#pylint: enable=wrong-import-position


def valida_por_activities(viagem: Viagem) -> bool:
	"""
		Tenta valdiar a viagem usando os 'activities' da google.
	Devolve true se o houve evidência suficiente para aprovar (ou reprovar)
	 a viagem, e false caso contrário - ou seja, caso o resultado seja 
	inconclusivo.
	"""

	lista_activities = viagem.extrai_nucleo_activities_utilizavel(
	    CONFIANCA_MINIMA)

	# print("Lista de activities: ", lista_activities)

	if not viagem.checa_num_min_activities(NUM_MIN_ACITVITIES_KM,
	                                       trip=lista_activities):
		return False

	num_total = len(lista_activities)
	num_aparicoes = {
	    "A_PE": 0,
	    "NA_BICICLETA": 0,
	    "EM_VEICULO": 0,
	    "CORRENDO": 0,
	    "PARADO": 0,
	    "CAMINHANDO": 0,
	    "DESCONHECIDO": 0,
	}

	for amostra in lista_activities:
		num_aparicoes[amostra["type"]] += 1

	if num_aparicoes["EM_VEICULO"] / num_total >= PCNTG_ACTIVITIES_REPROVACAO:
		# print("Reprovando Viagem por Activities")
		viagem.reprova("NAO_BICICLETA")
		return True

	if num_aparicoes["NA_BICICLETA"] / num_total >= PCNTG_ACTIVITIES_APROVACAO:
		# print("Aprovando Viagem por Activities")
		return True

	return False
