'''
	Remoções no banco de dados
'''

import sys
from pathlib import Path
from typing import Any, Optional
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import consultas

#pylint: enable=wrong-import-position


# pylint: disable=invalid-name
def remove_GRUPOPESQUISA(id_grupo: str,
                         cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Remove linha da relação GRUPOPESQUISA dado um idGrupo
	'''
	resultado_resp_grupo = consultas.grupo_pesquisa_existe(id_grupo, cur)

	if resultado_resp_grupo is None:
		return cur

	cur.execute("""
		DELETE FROM GRUPOPESQUISA
		WHERE idGrupo = %s;
	""", [id_grupo])

	return cur


def remove_SESSAO(id_pessoa: str | int, token: str,
                  cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Remove linha da relação SESSAO data um idPessoa e um token
	'''
	resultado_resp_sessao = consultas.usuario_token_existe(
	    id_pessoa, token, cur)
	if resultado_resp_sessao is None:
		return cur
	cur.execute(
	    """
		DELETE FROM auth_bikesp.SESSAO
		WHERE idPessoa = %s AND token = %s;
	""", [id_pessoa, token])
	return cur


def remove_TOKENREDEFINICAOSENHA(
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Remove token de redefinição de senha de certo usuário
	'''
	existencia_token = consultas.pessoa_possui_token_redefinicao(id_pessoa, cur)
	if existencia_token is None:
		return cur
	cur.execute(
	    """
		DELETE FROM auth_bikesp.TOKENREDEFINICAOSENHA
		WHERE idPessoa = %s;
		""", [id_pessoa])
	return cur


def remove_APELIDOLOCALIZACAO(id_pessoa: str | int, id_localizacao: str | int,
                              cur: psycopg2.extensions.cursor):
	'''
		Remove entrada da tabela APELIDOLOCALIZACAO
	'''
	cur.execute(
	    """
		DELETE FROM APELIDOLOCALIZACAO
		WHERE idPessoa = %s AND idLocalizacao = %s;
		""", [id_pessoa, id_localizacao])


def remove_PESSOAESTACAO(id_pessoa: str | int, id_localizacao: str | int,
                         cur: psycopg2.extensions.cursor):
	'''
		Remove entrada da tabela PESSOAESTACAO
	'''
	cur.execute(
	    """
		DELETE FROM PESSOAESTACAO
		WHERE idPessoa = %s AND idLocalizacao = %s;
		""", [id_pessoa, id_localizacao])


def remove_PARVALIDO(id_ponto1: str | int, id_ponto2: str | int,
                     cur: psycopg2.extensions.cursor):
	'''
		Remove entrada da tabela PARVALIDO
	'''
	cur.execute(
	    """
		DELETE FROM PARVALIDO
		WHERE (idPonto1 = %s AND idPonto2 = %s) OR
		(idPonto1 = %s AND idPonto2 = %s)
		""", [id_ponto1, id_ponto2, id_ponto2, id_ponto1])


def remove_LOCALIZACAO(id_localizacao: str | int,
                       cur: psycopg2.extensions.cursor) -> bool:
	'''
		Remove entrada da tabela LOCALIZACAO
		Não apaga localização se houver alguma foreign key associada
		(por exemplo, nas tabelas APELIDOLOCALIZACAO e PESSOAESTACAO)
	'''
	try:
		cur.execute(
		    """
			DELETE FROM LOCALIZACAO
			WHERE idLocalizacao = %s;
			""", [str(id_localizacao)])
	except:  # pylint: disable=bare-except
		return False

	if cur.rowcount == 0:
		return False
	return True
