'''
	Script responsável por inserir elementos no bd
'''
import sys
import json
from pathlib import Path
from typing import List, Any, Tuple, Optional
from datetime import date
import psycopg2
import psycopg2.extensions

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.utils import parsers

#pylint: enable=wrong-import-position


# Função auxiliar às inserções
def gera_query(insercoes: dict) -> Tuple[List[str], List[str], List[Any]]:
	'''
		Gera variáveis necessárias para cada query, dado
		dicionário com atributos a serem inseridos/atualizados
	'''
	nomes_atributos = list(insercoes.keys())
	tipos_atributos = []  # type: List[str]
	valores_atributos = []  # type: List[Any]
	for nome in nomes_atributos:
		tipos_atributos.append(insercoes[nome]["tipo"])
		if insercoes[nome]["valor"] == '':
			valores_atributos.append(None)
		else:
			valores_atributos.append(insercoes[nome]["valor"])
	return nomes_atributos, tipos_atributos, valores_atributos


def insere_GRUPOPESQUISA(  # pylint: disable=invalid-name
        grupo: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela GRUPOPESQUISA
	'''

	resultado_resp_grupo = consultas.grupo_pesquisa_existe(
	    grupo["idGrupo"], cur)

	insercoes = {
	    "remuneracao": {
	        "tipo": "%s",
	        "valor": grupo["remuneracao"]
	    },
	    "idGrupo": {
	        "tipo": "%s",
	        "valor": grupo["idGrupo"]
	    },
	}

	nomes_atributos, tipos_atributos, valores_atributos = gera_query(insercoes)

	nomes_atributos_string = "(" + ",".join(nomes_atributos) + ")"
	tipos_atributos_string = "(" + ",".join(tipos_atributos) + ")"

	if resultado_resp_grupo is not None:
		if resultado_resp_grupo[1] != grupo["remuneracao"]:
			id_grupo = resultado_resp_grupo[0]
			tipo_id_grupo = "%s"
			cur.execute(
			    """
				UPDATE GRUPOPESQUISA
				SET """ + nomes_atributos_string + """=""" + tipos_atributos_string + """
				WHERE idGrupo=""" + tipo_id_grupo + """;
			""", valores_atributos + [str(id_grupo)])
	else:
		cur.execute(
		    """
	 		INSERT INTO GRUPOPESQUISA""" + nomes_atributos_string + """
	 		VALUES """ + tipos_atributos_string + """;
	 	""", valores_atributos)

	return cur


def insere_LOCALIZACAO(  # pylint: disable=invalid-name
        localizacao: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela LOCALIZACAO
	'''
	coordenadas = parsers.parse_coordenadas(localizacao["coordenadas"])
	resultado_resp_localizacao = consultas.localizacao_existe(
	    coordenadas, localizacao["endereco"], cur)
	if resultado_resp_localizacao is not None:
		return cur

	insercoes = {
	    "endereco": {
	        "tipo": "%s",
	        "valor": localizacao["endereco"]
	    },
	    "coordenadas": {
	        "tipo": "%s",
	        "valor": coordenadas
	    },
	    "tipoLocalizacao": {
	        "tipo": "%s",
	        "valor": localizacao["tipoLocalizacao"]
	    }
	}

	nomes_atributos, tipos_atributos, valores_atributos = gera_query(insercoes)

	nomes_atributos_string = "(" + ",".join(nomes_atributos) + ")"
	tipos_atributos_string = "(" + ",".join(tipos_atributos) + ")"

	cur.execute(
	    """
 		INSERT INTO LOCALIZACAO""" + nomes_atributos_string + """
		VALUES """ + tipos_atributos_string + """;
	    """, valores_atributos)
	return cur


def insere_PESSOAESTACAO(  # pylint: disable=invalid-name
        pessoa_estacao: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela PESSOAESTACAO
	'''

	pessoa = str(pessoa_estacao["idPessoa"])
	estacao = str(pessoa_estacao["idLocalizacao"])
	if consultas.pessoa_estacao_existe(pessoa, estacao, cur) is not None:
		return cur

	cur.execute(
	    """
		INSERT INTO PESSOAESTACAO(idPessoa,idLocalizacao)
		VALUES (%s,%s);
	""", [pessoa, estacao])
	return cur


def insere_APELIDOLOCALIZACAO(  # pylint: disable=invalid-name
        apelido_localizacao: dict,
        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela APELIDOLOCALIZACAO
	'''
	id_pessoa = str(apelido_localizacao["idPessoa"])
	id_localizacao = str(apelido_localizacao["idLocalizacao"])

	resp_apelido_loc_existe = consultas.apelido_localizacao_existe(
	    id_pessoa, id_localizacao, cur)

	insercoes = {
	    "apelido": {
	        "tipo": "%s",
	        "valor": parsers.parse_apelido(apelido_localizacao["apelido"])
	    }
	}

	tipo_id_pessoa = "%s"
	tipo_id_localizacao = "%s"

	nomes_atributos, tipos_atributos, valores_atributos = gera_query(insercoes)

	nomes_atributos_string = "(" + ",".join(nomes_atributos + ["idPessoa"] +
	                                        ["idLocalizacao"]) + ")"
	tipos_atributos_string = "(" + ",".join(tipos_atributos + [tipo_id_pessoa] +
	                                        [tipo_id_localizacao]) + ")"

	if resp_apelido_loc_existe is not None:
		if resp_apelido_loc_existe[2] != apelido_localizacao["apelido"]:
			cur.execute(
			    """
				UPDATE APELIDOLOCALIZACAO SET """ + nomes_atributos_string + """=""" +
			    tipos_atributos_string + """
				WHERE idPessoa=""" + tipo_id_pessoa + """ AND idLocalizacao=""" +
			    tipo_id_localizacao + """;
			""", valores_atributos + [id_pessoa] + [id_localizacao] + [id_pessoa] +
			    [id_localizacao])
	else:
		cur.execute(
		    """
			INSERT INTO APELIDOLOCALIZACAO""" + nomes_atributos_string + """
			VALUES """ + tipos_atributos_string + """;
		""", valores_atributos + [id_pessoa] + [id_localizacao])

	return cur


def insere_PARVALIDO(  # pylint: disable=invalid-name
        par_valido: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela PARVALIDO
	'''

	id_ponto1 = par_valido["idPonto1"]
	id_ponto2 = par_valido["idPonto2"]

	if int(id_ponto1) > int(id_ponto2):
		id_ponto1, id_ponto2 = id_ponto2, id_ponto1

	resp_par_valido_existe = consultas.par_valido_existe(
	    id_ponto1, id_ponto2, cur)

	tipo_id_ponto = "%s"

	insercoes = {
	    "distancia": {
	        "tipo": "%s",
	        "valor": par_valido["distancia"]
	    },
	    "trajeto": {
	        "tipo": "%s",
	        "valor": json.dumps(par_valido["trajeto"])
	    }
	}

	nomes_atributos, tipos_atributos, valores_atributos = gera_query(insercoes)

	nomes_atributos_string = "(" + ",".join(nomes_atributos + ["idPonto1"] +
	                                        ["idPonto2"]) + ")"
	tipos_atributos_string = "(" + ",".join(tipos_atributos + [tipo_id_ponto] +
	                                        [tipo_id_ponto]) + ")"

	if resp_par_valido_existe is not None:
		cur.execute(
		    """
			UPDATE PARVALIDO SET """ + nomes_atributos_string + """=""" +
		    tipos_atributos_string + """
			WHERE idPonto1=""" + tipo_id_ponto + """ AND idPonto2=""" + tipo_id_ponto +
		    """;
		""", valores_atributos + [str(id_ponto1)] + [str(id_ponto2)] +
		    [str(id_ponto1)] + [id_ponto2])
	else:
		cur.execute(
		    """
			INSERT INTO PARVALIDO""" + nomes_atributos_string + """
			VALUES """ + tipos_atributos_string + """;
		""", valores_atributos + [str(id_ponto1)] + [str(id_ponto2)])

	return cur


def insere_ALTERACAOLOCALIZACAO(  # pylint: disable=invalid-name
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere e atualiza entradas da tabela ALTERALOCALIZACAO
	'''

	alteracao_localizacao_ultima = consultas.alteracao_localizacao_existe(
	    id_pessoa, cur)

	hoje = date.today()

	if alteracao_localizacao_ultima is not None:
		cur.execute(
		    """
			UPDATE ALTERACAOLOCALIZACAO SET data=%s
			WHERE idPessoa=%s;
			""", [str(hoje), str(id_pessoa)])
	else:
		cur.execute(
		    """
			INSERT INTO ALTERACAOLOCALIZACAO (idPessoa,data) VALUES (%s,%s);
			""", [str(id_pessoa), str(hoje)])

	return cur


def insere_VIAGEM(viagem: ViagemBD, cur) -> psycopg2.extensions.cursor:  # pylint: disable=invalid-name
	"""
	Dada uma viagem na forma de objeto viagem BD, salva
	a viagem no BD com o cursor dado.
	"""

	atributos = [
	    att for att in dir(viagem)
	    if not att.startswith('__') and not callable(getattr(viagem, att))
	]
	tipos_valores = list(map(lambda x: '%s', atributos))
	valores = list(map(lambda x: getattr(viagem, x), atributos))

	nomes_atributos_string = "(" + ",".join(atributos) + ")"
	tipos_atributos_string = "(" + ",".join(tipos_valores) + ")"
	cur.execute(
	    """
	 		INSERT INTO VIAGEM""" + nomes_atributos_string + """
	 		VALUES """ + tipos_atributos_string + """;
	 	""", valores)
	return cur


def insere_PESSOA(  # pylint: disable=invalid-name disable=too-many-locals
        resposta: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela PESSOA
	'''
	cpf = parsers.parse_documento(resposta["CPF"])
	if consultas.get_pessoa_id(cpf, cur) is not None:
		return cur

	nome = parsers.parse_nome(resposta["Nome"])
	data_nascimento = parsers.parse_data(resposta["DataNascimento"])
	genero = parsers.parse_genero(resposta["Genero"])
	# bilhete_unico = parsers.parse_bilhete_unico(resposta["NumBilheteUnico"])
	raca = parsers.parse_raca(resposta["Raca"])
	rg = parsers.remove_pontuacao(resposta["RG"])[0:15]  # pylint: disable=invalid-name
	email = parsers.parse_email(resposta["Email"])
	telefone = parsers.parse_telefone(resposta["Telefone"])
	endereco = parsers.parse_endereco(
	    resposta["Rua"],
	    resposta["Numero"],
	    complemento=resposta["Complemento"],
	    bairro=parsers.parse_bairro(resposta["Bairro"]),
	    cep=parsers.parse_cep(resposta["CEP"]),
	    cidade=parsers.parse_cidade(resposta["Cidade"]),
	    estado=parsers.parse_estado(resposta["Estado"]))
	cur.execute(
	    """
		INSERT INTO PESSOA(nome,dataNascimento,genero,raca,rg,cpf,email,telefone,endereco)
	 	VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)
	""", [nome, data_nascimento, genero, raca, rg, cpf, email, telefone, endereco])

	return cur


def insere_RESPOSTA_FORMS(  # pylint: disable=invalid-name disable=too-many-locals
        resposta: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela RESPOSTAFROMS
	'''
	resultado_pessoa = consultas.get_pessoa_id(resposta["CPF"], cur)
	try:
		id_pessoa = resultado_pessoa[0]
	except:  # pylint: disable=raise-missing-from disable=bad-option-value disable=broad-exception-raised
		raise Exception("[ERRO]: Algo de errado com a inserção de",
		                list(resposta.values()))

	resultado_resp_forms = consultas.resposta_forms_existe(id_pessoa, cur)

	chave = {"idPessoa": {"tipo": "%s", "valor": id_pessoa}}

	insercoes = {
	    "dataResposta": {
	        "tipo": "%s",
	        "valor": parsers.parse_data(resposta["DataResposta"])
	    },
	    "sabeAndarDeBike": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["SabeAndarDeBike"])
	    },
	    "temProblemaFisico": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["TemProblemaFisico"])
	    },
	    "temBikePropria": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["TemBikePropria"])
	    },
	    "comoConseguiraAcessoBike": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.remove_quebras(resposta["ComoConseguiraAcessoBike"])
	    },
	    "aceitouTermoConsentimento": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["AceitouTermoConsentimento"])
	    },
	    "autorizouColetaInfo": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["AutorizouColetaInfo"])
	    },
	    "possuiBilheteUnico": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["PossuiBilheteUnico"])
	    },
	    "poderaInstalarApp": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["PoderaInstalarApp"])
	    },
	    "qtdPessoasApartamento": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_integer(resposta["QuantidadePessoasApartamento"])
	    },
	    "rendaMensalFamiliar": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_range(resposta["RendaMensalFamiliar"],
	                                tipo="money")
	    },
	    "estudaRegularmente": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["EstudaRegularmente"])
	    },
	    "grauInstrucao": {
	        "tipo": "%s",
	        "valor": parsers.parse_grau_instrucao(resposta["GrauInstrucao"])
	    },
	    "condicaoAtividade": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_condicao_atividade(resposta["CondicaoAtividade"])
	    },
	    "vinculoEmpregaticio": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_vinculo_empregaticio(
	                resposta["VinculoEmpregaticio"])
	    },
	    "rendaMensalIndividual": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_range(resposta["RendaMensalIndividual"],
	                                tipo="money")
	    },
	    "qtdViagensAPe": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensAPe"])
	    },
	    "qtdViagensTransPub": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensTransPub"])
	    },
	    "qtdViagensBike": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensBike"])
	    },
	    "qtdViagensTransPubBike": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensTransPubBike"])
	    },
	    "qtdViagensOnibus": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensOnibus"])
	    },
	    "qtdViagensTremMetro": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensTremMetro"])
	    },
	    "qtdViagensCombTransPub": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensCombTransPub"])
	    },
	    "qtdViagensCarroAplicativo": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensCarroAplicativo"])
	    },
	    "qtdViagensMoto": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensMoto"])
	    },
	    "qtdViagensCarroMotoTransPub": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_range(resposta["QtdViagensCarroMotoTransPub"])
	    },
	    "qtdViagensOutro": {
	        "tipo": "%s",
	        "valor": parsers.parse_range(resposta["QtdViagensOutro"])
	    },
	    "realizariaTrajetos220": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["RealizariaTrajetos220"])
	    },
	    "pqNaoIncentivo220": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_pq_nao_incentivo(resposta["PqNaoIncentivo220"])
	    },
	    "qtdViagensIncentivo220": {
	        "tipo": "%s",
	        "valor": parsers.parse_integer(resposta["QtdViagensIncentivo220"])
	    },
	    "realizariaTrajetos440": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["RealizariaTrajetos440"])
	    },
	    "pqNaoIncentivo440": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_pq_nao_incentivo(resposta["PqNaoIncentivo440"])
	    },
	    "qtdViagensIncentivo440": {
	        "tipo": "%s",
	        "valor": parsers.parse_integer(resposta["QtdViagensIncentivo440"])
	    },
	    "cienciaGarantiaNaoSelecao": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["CienciaNaoGarantiaSelecao"])
	    },
	    "cienciaNecessidadeInternet": {
	        "tipo": "%s",
	        "valor": parsers.parse_bool(resposta["CienciaNecessidadeInternet"])
	    },
	    "declaracaoRespostasVeridicas": {
	        "tipo":
	            "%s",
	        "valor":
	            parsers.parse_bool(resposta["DeclaracaoRespostasVeridicas"])
	    }
	}

	nomes_atributos, tipos_atributos, valores_atributos = gera_query(insercoes)

	nomes_atributos_string_insert = "(" + ",".join(nomes_atributos +
	                                               ["idPessoa"]) + ")"
	tipos_atributos_string_insert = "(" + ",".join(
	    tipos_atributos + [chave["idPessoa"]["tipo"]]) + ")"
	valores_atributos_insert = valores_atributos + [chave["idPessoa"]["valor"]]

	nomes_atributos_string_update = "(" + ",".join(nomes_atributos) + ")"
	tipos_atributos_string_update = "(" + ",".join(tipos_atributos) + ")"
	valores_atributos_update = valores_atributos

	if resultado_resp_forms is not None:
		cur.execute(
		    """
			UPDATE RESPOSTAFORMS SET """ + nomes_atributos_string_update + """=""" +
		    tipos_atributos_string_update + """
			WHERE idPessoa=""" + chave["idPessoa"]["tipo"] + """;
		""", valores_atributos_update + [chave["idPessoa"]["valor"]])
	else:
		cur.execute(
		    """
	 		INSERT INTO RESPOSTAFORMS""" + nomes_atributos_string_insert + """
	 		VALUES """ + tipos_atributos_string_insert + """;
	 	""", valores_atributos_insert)

	return cur


def insere_USUARIO(  # pylint: disable=invalid-name
        usuario: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela USUARIO
	'''
	existencia_usuario = consultas.usuario_existe(usuario["idPessoa"], cur)
	if not existencia_usuario:
		cur.execute(
		    """
			INSERT INTO auth_bikesp.USUARIO (idPessoa,senhaHash)
			VALUES (%s,crypt(%s,gen_salt('bf',8)));

		""", [usuario["idPessoa"], usuario["senha"]])
	else:
		cur.execute(
		    """	
			UPDATE auth_bikesp.USUARIO
			SET senhaHash = crypt(%s,gen_salt('bf',8))
			WHERE idPessoa = %s;
		""", [usuario["senha"], usuario["idPessoa"]])
	return cur


def insere_SESSAO(  # pylint: disable=invalid-name
        sessao: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela SESSAO
	'''
	cur.execute(
	    """
		INSERT INTO auth_bikesp.SESSAO (idPessoa,token,dataValidade)
		VALUES (%s,%s,%s);

	""", [sessao["idPessoa"], sessao["token"], sessao["dataValidade"]])
	return cur


def insere_TOKENREDEFINICAOSENHA(  # pylint: disable=invalid-name
        sessao: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela TOKENREDEFINICAOSENHA
	'''
	token_existente = consultas.pessoa_possui_token_redefinicao(
	    sessao["idPessoa"], cur)
	if token_existente is None:
		cur.execute(
		    """
			INSERT INTO auth_bikesp.TOKENREDEFINICAOSENHA (idPessoa,token,dataValidade)
			VALUES (%s,%s,%s);

		""", [sessao["idPessoa"], sessao["token"], sessao["dataValidade"]])
	else:
		cur.execute(
		    """
				UPDATE auth_bikesp.TOKENREDEFINICAOSENHA SET (token,dataValidade)=(%s,%s)
				WHERE idPessoa = %s;
			""", [sessao["token"], sessao["dataValidade"], sessao["idPessoa"]])
	return cur


def insere_TOKENNOTIFICACAO(  # pylint: disable=invalid-name
        token_notif: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	"""
		Insere na tabela TOKENNOTIFICACAO
    """
	existe = consultas.get_token_notificacao(token_notif["idPessoa"], cur)

	if existe is None:
		cur.execute(
		    """
				INSERT INTO auth_bikesp.TOKENNOTIFICACAO (idPessoa,token)
				VALUES (%s,%s);
			""", [str(token_notif["idPessoa"]), token_notif["token"]])
	else:
		cur.execute(
		    """
			UPDATE auth_bikesp.TOKENNOTIFICACAO SET
			token = %s
			WHERE idPessoa = %s
  		""", [token_notif["token"], token_notif["idPessoa"]])

	return cur


def insere_CONTESTACAO(  # pylint: disable=invalid-name
        contestacao: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela CONTESTACAO
	'''
	cur.execute(
	    """
			INSERT INTO CONTESTACAO (idViagem,data,justificativa)
			VALUES (%s,%s,%s);
		""", [
	        str(contestacao["idViagem"]), contestacao["data"],
	        contestacao["justificativa"]
	    ])

	return cur


def insere_RESPOSTA_TRAJETO(  # pylint: disable=invalid-name
        resposta_trajeto: dict,
        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela RESPOSTATRAJETO
	'''

	id_pessoa = str(resposta_trajeto["idPessoa"])
	apelido1 = parsers.parse_apelido(resposta_trajeto["apelido1"])
	apelido2 = parsers.parse_apelido(resposta_trajeto["apelido2"])
	frequencia = parsers.parse_frequencia_trajeto(
	    resposta_trajeto["frequencia"])
	duracao = parsers.parse_integer(resposta_trajeto["duracao"])
	como_faz = parsers.parse_como_faz_trajeto(resposta_trajeto["comoFaz"])
	bike = parsers.parse_possibilidade_bike(
	    resposta_trajeto["possibilidadeBike"])

	resultado_resp_trajeto = consultas.resposta_trajeto_existe(
	    id_pessoa, apelido1, apelido2, cur)

	if resultado_resp_trajeto is not None:
		cur.execute(
		    """
			UPDATE RESPOSTATRAJETO SET (frequencia,duracao,comoFaz,
				possibilidadeBike)=(%s,%s,%s,%s)
			WHERE idPessoa = %s AND apelido1 = %s AND apelido2 = %s;
		""", [frequencia, duracao, como_faz, bike, id_pessoa, apelido1, apelido2])
	else:

		cur.execute(
		    """

				INSERT INTO RESPOSTATRAJETO (idPessoa,apelido1,apelido2,
					frequencia,duracao,comoFaz,possibilidadeBike)
				VALUES (%s,%s,%s,%s,%s,%s,%s);
			""", [id_pessoa, apelido1, apelido2, frequencia, duracao, como_faz, bike])
	return cur


def insere_BILHETEUNICO(  # pylint: disable=invalid-name
        dict_insercao: dict, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere um bilhete unico na tabela BILHETEUNICO
	'''
	bilhete_unico = parsers.parse_bilhete_unico(
	    dict_insercao["NumBilheteUnico"])

	info_bilhete_unico = consultas.bilhete_unico_existe(bilhete_unico, cur)

	id_pessoa = dict_insercao["idPessoa"]
	data_inicio = dict_insercao["dataInicio"]
	data_fim = dict_insercao["dataFim"]
	ativo = dict_insercao["ativo"]
	concedido = dict_insercao["concedido"]
	aguardando_envio = dict_insercao["aguardandoEnvio"]
	aguardando_resposta = dict_insercao["aguardandoResposta"]

	if info_bilhete_unico is None:
		cur.execute(
		    """
				INSERT INTO BILHETEUNICO (bilheteUnico,idPessoa,dataInicio,dataFim,ativo,
					concedido,aguardandoEnvio,aguardandoResposta)
				VALUES (%s,%s,%s,%s,%s,%s,%s,%s);

			""", [
		        bilhete_unico, id_pessoa, data_inicio, data_fim, ativo,
		        concedido, aguardando_envio, aguardando_resposta
		    ])
	return cur


def insere_HISTORICOENVIOSPTRANS(  # pylint: disable=invalid-name
        dict_insercao, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Insere na tabela HISTORICOENVIOSPTRANS
	'''

	bilhete_unico = parsers.parse_bilhete_unico(
	    dict_insercao["NumBilheteUnico"])

	data_envio = dict_insercao["DataEnvio"]
	valor = dict_insercao["Valor"]
	confirmado = dict_insercao["Confirmado"]
	observacao = dict_insercao["Observacao"]

	cur.execute(
	    """
		INSERT INTO HISTORICOENVIOSPTRANS (bilheteUnico,dataEnvio,valor,confirmado,observacao)
		VALUES (%s,%s,%s,%s,%s)
	""", [bilhete_unico, data_envio, valor, confirmado, observacao])
	return cur


def insere_FEEDBACKVIAGEM(  # pylint: disable=invalid-name
        feedback_dict, cur: psycopg2.extensions.cursor) -> None:
	'''
		Insere na tabela FEEDBACKVIAGEM
	'''

	id_viagem = feedback_dict["idViagem"]
	respondido = feedback_dict["respondido"]
	token_feedback = feedback_dict["tokenFeedback"]
	motivo_original = feedback_dict["motivoOriginal"]
	outros_apps = feedback_dict["outrosApps"]
	feedback_geral = feedback_dict["feedbackGeral"]
	respostas_dinamicas = feedback_dict["respostasDinamicas"]

	cur.execute(
	    """
		INSERT INTO FEEDBACKVIAGEM (idViagem,respondido,tokenFeedback,
			motivoOriginal,outrosApps,feedbackGeral,respostasDinamicas)
		VALUES (%s,%s,%s,%s,%s,%s,%s)
		""", [
	        id_viagem, respondido, token_feedback, motivo_original, outros_apps,
	        feedback_geral, respostas_dinamicas
	    ])


def insere_FEEDBACKVIAGEMPERDIDA(  # pylint: disable=invalid-name
        feedback_dict, cur: psycopg2.extensions.cursor) -> None:
	'''
		Insere na tabela FEEDBACKVIAGEMPERDIDA
	'''

	id_pessoa = feedback_dict["idPessoa"]
	data_feedback = feedback_dict["dataFeedback"]
	quando_perdeu = feedback_dict["quandoPerdeu"]
	algo_diferente = feedback_dict["algoDiferente"]
	ver_app = feedback_dict["verApp"]
	feedback_geral = feedback_dict["feedbackGeral"]
	respostas_dinamicas = feedback_dict["respostasDinamicas"]

	cur.execute(
	    """
		INSERT INTO FEEDBACKVIAGEMPERDIDA (idPessoa,dataFeedback,quandoPerdeu,algoDiferente,
			verApp,feedbackGeral,respostasDinamicas)
		VALUES (%s,%s,%s,%s,%s,%s,%s)
		""", [
	        id_pessoa, data_feedback, quando_perdeu, algo_diferente, ver_app,
	        feedback_geral, respostas_dinamicas
	    ])


def insere_BONUS(bonus_dict: dict, cur: psycopg2.extensions.cursor) -> int:  # pylint: disable=invalid-name
	'''
		Insere na tabela BONUS
	'''
	nome = bonus_dict["nome"]
	valor = bonus_dict["valor"]
	ativo = bonus_dict["ativo"]
	visivel = bonus_dict["visivel"]
	descricao = bonus_dict["descricao"]
	data_inicio = bonus_dict["data_inicio"]
	data_fim = bonus_dict["data_fim"]

	cur.execute(
	    """	
			INSERT INTO BONUS (nome,valor,ativo,visivel,descricao,dataInicio,dataFim)
			VALUES (%s,%s,%s,%s,%s,%s,%s)
			RETURNING idBonus;
		""", [nome, valor, ativo, visivel, descricao, data_inicio, data_fim])
	resposta = cur.fetchall()[0]
	novo_id_bonus = -1
	if resposta is not None:
		novo_id_bonus = int(list(resposta)[0])
	return novo_id_bonus


def insere_PESSOABONUS(  # pylint: disable=invalid-name
        pessoa_bonus_dict: dict, cur: psycopg2.extensions.cursor) -> None:
	'''
		Insere na tabela PESSOABONUS
	'''
	pessoa = pessoa_bonus_dict["idPessoa"]
	bonus = pessoa_bonus_dict["idBonus"]
	data_concessao = pessoa_bonus_dict["dataConcessao"]
	cur.execute(
	    """	
			INSERT INTO PESSOABONUS (idPessoa,idBonus,dataConcessao)
			VALUES (%s,%s,%s)
		""", [pessoa, bonus, data_concessao])


def atualiza_grupo_pessoa(id_grupo: str | int | None, id_pessoa: str | int,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Atualiza o grupo de pesquisa de uma pessoa
	'''
	cur.execute(
	    """
			UPDATE PESSOA SET idGrupo=%s
			WHERE idPessoa = %s;
		""", [id_grupo, id_pessoa])

	return cur
