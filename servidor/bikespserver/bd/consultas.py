'''
	Consultas no banco de dados (não alteram seu estado)
'''
import sys

from pathlib import Path
from typing import Tuple, Any, Optional
from datetime import datetime, date, timedelta

import psycopg2
import psycopg2.extensions
from psycopg2 import sql

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.utils import coords
from servidor.bikespserver.utils import parsers
#pylint: enable=wrong-import-position

#### Chegagem da existência de elementos no bd ####

## Se contem elemento, devolve o resultado da query de busca, caso contrário devolve None ##


def get_pessoa_info(cpf: str, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca pessoa pelo cpf no bd, e devolve idPessoa,
		email,idGrupo
	'''
	cpf = parsers.parse_documento(cpf)

	cur.execute(
	    """
		SELECT idPessoa,email,idGrupo 
		FROM PESSOA
		WHERE cpf = %s;
	""", [cpf])

	resultado = cur.fetchone()
	return resultado


def get_pessoa_info_pelo_id_pessoa(
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca pessoa por seu id e devolve cpf,email e idGrupo
	'''
	cur.execute(
	    """
		SELECT cpf,email,idGrupo,nome
		FROM PESSOA
		WHERE idPessoa = %s;
	""", [id_pessoa])
	resultado = cur.fetchone()
	return resultado


def get_pessoa_bilhete_unico(bilhete_unico: str,
                             cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve o id de uma pessoa, dado um bilhete unico
	'''
	bilhete_unico = parsers.parse_bilhete_unico(bilhete_unico)
	cur.execute(
	    """
		SELECT idPessoa
		FROM BILHETEUNICO
		WHERE bilheteUnico = %s;
	""", [bilhete_unico])
	resultado = cur.fetchone()
	return resultado


def get_viagem_info(id_viagem: str | int,
                    cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca viagem pelo id e devolve: idPessoa, data,
		deslocamento, idOrigem, idDestino, status, remunercao
	'''
	cur.execute(
	    """
		SELECT idPessoa,data,deslocamento,idOrigem,idDestino,status,remuneracao
		FROM VIAGEM
		WHERE idViagem = %s;
	""", [str(id_viagem)])

	resultado = cur.fetchone()
	return resultado


def get_localizacao_info(id_localizacao: str | int,
                         cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as informações de uma localização
	'''
	cur.execute(
	    """
		SELECT endereco,coordenadas,tipoLocalizacao
		FROM LOCALIZACAO
		WHERE idLocalizacao = %s;
	""", [id_localizacao])
	resultado = cur.fetchone()
	return resultado


def get_trajeto_viagem(id_viagem: str | int,
                       cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca o trajeto de uma viagem
	'''
	cur.execute(
	    """
			SELECT trajeto
			FROM VIAGEM
			WHERE idViagem = %s;
		""", [str(id_viagem)])

	resultado = cur.fetchone()
	return resultado


def get_viagens_pessoa(
    id_pessoa: int | str,
    cur: psycopg2.extensions.cursor,
    intervalo_data: Tuple[datetime, datetime] = (datetime(1900, 1,
                                                          1), datetime.max)
) -> Optional[Any]:
	'''
		Devolve todas as viagens de uma pessoa. Opcionalmente,
		é possível passar um itervalo de data
	'''
	data_inicio = intervalo_data[0].strftime("%Y-%m-%d %H:%M:%S")
	data_fim = intervalo_data[1].strftime("%Y-%m-%d %H:%M:%S")
	cur.execute(
	    """
			SELECT idViagem,data,remuneracao,status,motivoStatus,idOrigem,idDestino
			FROM VIAGEM
			WHERE idPessoa = %s AND data >= %s
			AND data <= %s;
		""", [str(id_pessoa), data_inicio, data_fim])
	resultado = cur.fetchall()
	return resultado


def get_todas_infos_viagem(id_viagem: int | str,
                           cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as informações de uma viagem, incluindo trajeto e distância ideal, se existirem
	'''
	cur.execute(
	    """
		SELECT idPessoa,data,idorigem,iddestino,remuneracao,status,
			VIAGEM.trajeto,NULL as trajetoIdeal,NULL as distancia,motivoStatus
		FROM VIAGEM
		WHERE idViagem = %s AND ((idOrigem,idDestino) NOT IN (
			(SELECT idPonto1,idPonto2
			FROM PARVALIDO)
			UNION
			(SELECT idPonto2,idPonto1
				FROM PARVALIDO)
			))
		UNION (
		SELECT idPessoa,data,idorigem,iddestino,remuneracao,status,
			VIAGEM.trajeto,PARVALIDO.trajeto as trajetoIdeal,distancia,motivoStatus
  		FROM VIAGEM,PARVALIDO
  		WHERE idViagem = %s AND ((idPonto1 = idorigem and idPonto2 = idDestino) OR
  			(idPonto1 = idDestino and idPonto2 = idOrigem))
  		);
	""", [str(id_viagem), str(id_viagem)])
	resultado = cur.fetchone()
	return resultado


def get_todas_infos_lista_viagens(
        lista_viagens: list, id_pessoa: int | str,
        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as informações de uma lista de viagens de
		determinado usuário
	'''
	cur.execute(
	    """
		SELECT idViagem,idPessoa,data,deslocamento,
			idOrigem,idDestino,remuneracao,status,motivoStatus,trajeto
		FROM VIAGEM
		WHERE idViagem = ANY(%s) AND idPessoa = %s
		""", [lista_viagens, id_pessoa])
	resultado = cur.fetchall()
	return resultado


def viagem_existe(id_pessoa: int | str, data: str,
                  cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca viagem por data e id_pessoa
	'''
	cur.execute(
	    """
		SELECT idViagem
		FROM VIAGEM
		WHERE data = %s AND idPessoa = %s;
	""", [str(data), str(id_pessoa)])
	resultado = cur.fetchone()
	return resultado


def resposta_trajeto_existe(id_pessoa, apelido1, apelido2,
                            cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca resposta trajeto por suas chaves primárias
	'''
	cur.execute(
	    """
			SELECT * FROM RESPOSTATRAJETO
			WHERE idPessoa = %s AND apelido1 = %s AND apelido2 = %s;
		""", [str(id_pessoa), str(apelido1),
	    str(apelido2)])
	resultado = cur.fetchone()
	return resultado


def get_pessoa_id(cpf: str, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve o id de uma pessoa, dado seu cpf
	'''
	cpf = parsers.parse_documento(cpf)
	cur.execute("""
		SELECT idPessoa 
		FROM PESSOA
		WHERE cpf = %s;
	""", [cpf])

	resultado = cur.fetchone()
	return resultado


def get_pessoa_por_email(email: str,
                         cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca pessoa pelo email cadastrado no bd.Devolve idPessoa, BU e cpf.
	'''
	email = parsers.parse_email(email)

	cur.execute(
	    """
		SELECT idPessoa, cpf 
		FROM PESSOA
		WHERE email = %s;
	""", [email])

	resultado = cur.fetchone()
	return resultado


def usuario_existe(id_pessoa: int | str,
                   cur: psycopg2.extensions.cursor) -> bool:
	'''
		Busca por um usuário
	'''
	cur.execute(
	    """
		SELECT * FROM auth_bikesp.USUARIO
		WHERE idPessoa = %s;
	""", [id_pessoa])

	resultado = cur.fetchone()
	return resultado is not None


def usuario_senha_existe(id_pessoa: int | str, senha: str,
                         cur: psycopg2.extensions.cursor) -> bool:
	'''
		Busca por um par usuário-senha
	'''
	cur.execute(
	    """
		SELECT * FROM auth_bikesp.USUARIO
		WHERE senhaHash = crypt(%s ,senhaHash) AND idPessoa = %s;
	""", [senha, id_pessoa])

	resultado = cur.fetchone()
	return resultado is not None


def usuario_token_existe(id_pessoa: int | str, token: str,
                         cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca por par usuário-token na tabela SESSAO
	'''
	cur.execute(
	    """
		SELECT * FROM auth_bikesp.SESSAO
		WHERE token = %s AND idPessoa = %s;

	""", [token, id_pessoa])
	resultado = cur.fetchone()
	return resultado


def grupo_pesquisa_existe(id_grupo: str,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca grupo de pesquisa no bd
	'''
	cur.execute("""
		SELECT * FROM GRUPOPESQUISA
		WHERE idGrupo = %s;
	""", [id_grupo])
	resultado = cur.fetchone()
	return resultado


def resposta_forms_existe(id_pessoa: str,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca resposta do formulário por id de Pessoa no bd
	'''
	cur.execute("""
		SELECT * FROM RESPOSTAFORMS
		WHERE idPessoa = %s;
	""", [id_pessoa])
	resultado = cur.fetchone()
	return resultado


def id_localizacao_existe(id_localizacao: str | int,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca localização por id no bd
	'''
	cur.execute(
	    """
		SELECT endereco,coordenadas,tipoLocalizacao
		FROM LOCALIZACAO
		WHERE idLocalizacao = %s;

	""", [id_localizacao])
	resultado = cur.fetchone()
	return resultado


def localizacao_existe(coordenadas: str | list, endereco: str,
                       cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca localização por coordenadas no bd
	'''
	coordenadas = parsers.parse_coordenadas(coordenadas)
	cur.execute(
	    """
		SELECT idLocalizacao FROM LOCALIZACAO
		WHERE coordenadas ~= point(%s) and UPPER(endereco) = UPPER(%s);
	""", [coordenadas, endereco])
	resultado = cur.fetchone()
	return resultado


def pessoa_estacao_existe(pessoa: str, estacao: str,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca dupla pessoa-estação no bd
	'''
	cur.execute(
	    """
		SELECT idPessoa,idLocalizacao FROM PESSOAESTACAO
		WHERE idPessoa = %s AND idLocalizacao = %s;
	""", [pessoa, estacao])
	resultado = cur.fetchone()
	return resultado


def get_residencia_pessoa(id_pessoa: str | int,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca pela residência de uma pessoa
	'''
	cur.execute(
	    """
		SELECT idLocalizacao
		FROM APELIDOLOCALIZACAO
		WHERE apelido = 'Residência' and idPessoa = %s;
		""", [id_pessoa])
	resultado = cur.fetchall()
	return resultado


def apelido_localizacao_existe(
        pessoa: str, qualquer: str,
        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca dupla pessoa-localizacao em APELIDOLOCALIZACAO
	'''
	cur.execute(
	    """
		SELECT idPessoa,idLocalizacao,apelido FROM APELIDOLOCALIZACAO
		WHERE idPessoa = %s AND idLocalizacao = %s;
	""", [pessoa, qualquer])
	resultado = cur.fetchone()
	return resultado


def alteracao_localizacao_existe(
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca por data de última alteração de uma localização feita
		por um usuário, registrado em ALTERACAOLOCALIZACAO
	'''
	cur.execute(
	    """
		SELECT data FROM ALTERACAOLOCALIZACAO WHERE idPessoa=%s;
		""", [str(id_pessoa)])
	resultado = cur.fetchone()
	return resultado


def par_valido_existe(id_ponto1: str, id_ponto2: str,
                      cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca por par de pontos em PARVALIDO
	'''
	cur.execute(
	    """
		SELECT * FROM PARVALIDO
		WHERE (idPonto1 = %s AND idPonto2 = %s) OR (idPonto1 = %s AND idPonto2 = %s)
	""", [id_ponto1, id_ponto2, id_ponto2, id_ponto1])
	resultado = cur.fetchone()
	return resultado


def localizacao_pessoa_associada_existe(
        id_localizacao: str | int, cur: psycopg2.extensions.cursor) -> bool:
	'''
		Devolve se uma localização possui associação com alguma pessoa,
		ou seja, se o ID da loc aparece nas tabelas APELIDOLOCALIZACAO
		ou PESSOAESTACAO.
	'''
	info_loc = id_localizacao_existe(id_localizacao, cur)
	if info_loc is None:
		return False

	tabela = "apelidolocalizacao"
	if info_loc[2].lower() == "estacao":
		tabela = "pessoaestacao"

	cur.execute(
	    sql.SQL("""
		SELECT EXISTS
		(SELECT idPessoa FROM {}
		WHERE idLocalizacao = %s);
		""").format(sql.Identifier(tabela)), [str(id_localizacao)])
	resultado = cur.fetchone()

	return resultado is not None and resultado[0]


def localizacao_viagem_associada_existe(
        id_localizacao: str | int, cur: psycopg2.extensions.cursor) -> bool:
	'''
		Devolve se uma localização possui associação com alguma viagem,
		ou seja, se o ID da loc aparece na tabela VIAGEM.
	'''
	cur.execute(
	    """
		SELECT EXISTS
		(SELECT idViagem FROM VIAGEM
		WHERE idOrigem = %s OR idDestino = %s);
		""", [str(id_localizacao), str(id_localizacao)])
	resultado = cur.fetchone()

	return resultado is not None and resultado[0]


def get_token_notificacao(
        ids: str | int | list[str | int],
        cur: psycopg2.extensions.cursor) -> None | str | list[str]:
	"""
    	Obtém o token de notificação de usuários
		Se a entrada for um único ID, será retornado o token como string
		Se a entrada for uma lista de IDS, será retornada uma lista de strings 
    """
	if not isinstance(ids, list):
		ids = [ids]

	placeholders = ','.join(['%s'] * len(ids))

	query = f"""
			SELECT (token) FROM auth_bikesp.TOKENNOTIFICACAO
			WHERE idPessoa IN({placeholders});
			"""

	cur.execute(query, ids)
	if len(ids) == 1:
		resultado = cur.fetchone()
		if resultado is None:
			return None
		return resultado[0]

	resultados = cur.fetchall()
	if resultados is None:
		return None
	return [row[0] for row in resultados]


def get_tokens_notificacao_coorte(ids_coortes: str | int | list[str | int],
                                  cur: psycopg2.extensions.cursor) -> list[str]:
	"""
		Obtém o token de notificação de todos os usuários de coortes 
    """
	if not isinstance(ids_coortes, list):
		ids_coortes = [ids_coortes]

	placeholders = ', '.join(['%s'] * len(ids_coortes))

	query = f"""
			SELECT t.token
			FROM auth_bikesp.TOKENNOTIFICACAO t
			JOIN PESSOA p ON t.idPessoa = p.idPessoa
			WHERE p.idGrupo IN({placeholders});	
			"""

	cur.execute(query, ids_coortes)
	return [row[0] for row in cur.fetchall()]


def get_tokens_notificacao_todos(cur: psycopg2.extensions.cursor) -> list[str]:
	"""
        Obtém todos os tokens de notificação registrados
    """
	cur.execute("""
        SELECT token
        FROM auth_bikesp.TOKENNOTIFICACAO
    """)
	return [row[0] for row in cur.fetchall()]


def contestacao_existe(id_viagem: str | int,
                       cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca contestação pelo id_viagem
	'''
	cur.execute("""
			SELECT  * FROM CONTESTACAO
			WHERE idViagem = %s;
		""", [str(id_viagem)])
	resultado = cur.fetchone()
	return resultado


def bilhete_unico_existe(bilhete_unico: str,
                         cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca por bilhete unico na tabela BILHETEUNICO
	'''
	bilhete_unico = parsers.parse_bilhete_unico(bilhete_unico)
	cur.execute(
	    """
		SELECT * FROM BILHETEUNICO
		WHERE bilheteUnico = %s;
		""", [bilhete_unico])
	resultado = cur.fetchone()
	return resultado


def pessoa_possui_token_redefinicao(
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca tokens de redefinicao por usuário
	'''
	cur.execute(
	    """
			SELECT * FROM auth_bikesp.TOKENREDEFINICAOSENHA
			WHERE idPessoa = %s;
		""", [str(id_pessoa)])
	resultado = cur.fetchone()
	return resultado


def token_redefinicao_existe(token: str,
                             cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca certo token na tabela TOKENREDEFINICAOSENHA
	'''
	cur.execute(
	    """
			SELECT idPessoa,dataValidade FROM auth_bikesp.TOKENREDEFINICAOSENHA
			WHERE token = %s;
		""", [str(token)])
	resultado = cur.fetchone()
	return resultado


def bonus_existe(nome: str, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Busca um bonus pelo nome na tabela BONUS
	'''
	cur.execute("""
		SELECT idBonus
		FROM BONUS
		WHERE nome = %s;
	""", [str(nome)])
	resultado = cur.fetchone()
	return resultado


#### Consultas gerais ####


def get_bilhetes_unicos_pessoa(
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos os bilhetes unicos de uma pessoa
	'''
	cur.execute("""
			SELECT * FROM BILHETEUNICO
			WHERE idPessoa = %s;
		""", [id_pessoa])
	resultado = cur.fetchall()
	return resultado


def get_bilhete_ativo_pessoa(id_pessoa: str | int,
                             cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve o bilhete único ativo da pessoa
	'''
	cur.execute(
	    """
			SELECT * FROM BILHETEUNICO
			WHERE idPessoa = %s AND ativo = 't';
		""", [id_pessoa])
	resultado = cur.fetchone()
	return resultado


def get_historico_envio_pessoa(
        id_pessoa: str | int, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todo o histórico de envio de certa pessoa
	'''
	cur.execute(
	    """
			SELECT BILHETEUNICO.bilheteUnico,dataEnvio,valor,confirmado,observacao
			FROM BILHETEUNICO
			JOIN HISTORICOENVIOSPTRANS
			ON BILHETEUNICO.bilheteUnico = HISTORICOENVIOSPTRANS.bilheteUnico
			WHERE idPessoa = %s
			ORDER BY dataEnvio DESC;
		""", [id_pessoa])
	resultado = cur.fetchall()
	return resultado


def get_coordenada_localizacao(
        indice: str, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve a coordenada dado o id de uma localização
	'''
	cur.execute(
	    """
		SELECT coordenadas FROM LOCALIZACAO
		WHERE idLocalizacao = %s
	""", [indice])
	resultado = cur.fetchone()
	return resultado


def aux_devolve_pontos(indice1: str, indice2: str,
                       cur: psycopg2.extensions.cursor) -> Tuple[dict, dict]:
	'''
		Devolve as coordenadas dados dois índices
		em um dicionário
	'''
	pontos = []
	for i in [indice1, indice2]:
		resultado = get_coordenada_localizacao(i, cur)
		if resultado is None:
			raise IndexError(
			    "Não foi possível encontrar coordenada para a localização:", i)
		coordenada = resultado[0]
		pontos.append(coordenada)

	ponto1 = parsers.parse_coordenadas(pontos[0]).split(",")
	ponto2 = parsers.parse_coordenadas(pontos[1]).split(",")

	p1 = {"latitude": float(ponto1[0]), "longitude": float(ponto1[1])}  # pylint: disable=invalid-name
	p2 = {"latitude": float(ponto2[0]), "longitude": float(ponto2[1])}  # pylint: disable=invalid-name
	return p1, p2


def get_rota_localizacoes(indice1: str,
                          indice2: str,
                          cur: psycopg2.extensions.cursor,
                          force_prod=False) -> dict:
	'''
		Devolve a rota entre duas localizações
	'''
	ponto1, ponto2 = aux_devolve_pontos(indice1, indice2, cur)
	if not force_prod:
		return coords.get_rota_aleatoria()
	return coords.get_rota_info(ponto1, ponto2)


def calcula_distancia_localizacoes_haversine(
        indice1: str, indice2: str, cur: psycopg2.extensions.cursor) -> float:
	'''
		Dados dois índices de localizações contidas no bd,
		devolve a distância em metros entre essas localizações
		utilizando a fórmula de harversine
	'''
	ponto1, ponto2 = aux_devolve_pontos(indice1, indice2, cur)
	return coords.distancia_haversine(ponto1, ponto2)


def get_todas_localizacoes_pessoa(id_pessoa: int | str,
                                  cur: psycopg2.extensions.cursor) -> list:
	'''
		Devolve todas as localizacoes válidas para uma pessoa
		Cada localizacao contém seu id, suas coordenadas e seu tipo
	'''
	resultado_estacoes = []
	resultado_qualquer = []
	# nota: aqui usamos "endereco as apelido" e depois "endereco" novamente porque as estações não
	# tem um apelido, e o endereço é apenas o nome delas.
	cur.execute(
	    """
		SELECT LOCALIZACAO.idLocalizacao,coordenadas,tipoLocalizacao,endereco as apelido,endereco
		FROM PESSOAESTACAO,LOCALIZACAO
		WHERE idPessoa = %s AND LOCALIZACAO.idLocalizacao = PESSOAESTACAO.idLocalizacao;
	""", [id_pessoa])
	resultado_estacoes = cur.fetchall()
	cur.execute(
	    """
		SELECT LOCALIZACAO.idLocalizacao,coordenadas,tipoLocalizacao,apelido,endereco
		FROM APELIDOLOCALIZACAO,LOCALIZACAO
		WHERE idPessoa = %s AND LOCALIZACAO.idLocalizacao = APELIDOLOCALIZACAO.idLocalizacao
	""", [id_pessoa])
	resultado_qualquer = cur.fetchall()
	resultado_final = []

	if len(resultado_estacoes) == 0:
		resultado_final = resultado_qualquer
	if len(resultado_qualquer) == 0:
		resultado_final = resultado_estacoes
	if len(resultado_qualquer) != 0 and len(resultado_estacoes) != 0:
		resultado_final = resultado_qualquer + resultado_estacoes
	return resultado_final


def get_sessoes_usuario(id_pessoa: int | str,
                        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as sessões de determinado usuário
	'''
	cur.execute(
	    """
		SELECT * FROM auth_bikesp.SESSAO
		WHERE idPessoa = %s;
	""", [id_pessoa])
	resultado = cur.fetchall()
	return resultado


def get_grupos_pesquisa(cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos os grupos existentes
	'''
	cur.execute("""
			SELECT * FROM GRUPOPESQUISA;
		""")
	resultado = cur.fetchall()
	return resultado


def dump_bilhetes_ativos(cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos os bilhetes unicos ativos,
		juntamente com a remuneração a ser enviada
	'''
	cur.execute("""
		SELECT bilheteUnico,aguardandoEnvio FROM BILHETEUNICO
		WHERE ativo = 't';
		""")
	resultado = cur.fetchall()
	return resultado


def get_ultima_troca_bu(id_pessoa: str | int,
                        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve data dá última troca de bilhete único
		de um usuário
	'''
	cur.execute(
	    """
		SELECT bilheteUnico,dataFim 
		FROM BILHETEUNICO
		WHERE idPessoa = %s
		ORDER BY dataFim desc NULLS LAST
		LIMIT 1;
	""", [id_pessoa])
	resultado = cur.fetchone()
	return resultado


def get_pares_localizacao(id_localizacao: str | int,
                          cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos os PARVALIDO que contenham
		certa localização
	'''
	cur.execute(
	    """
			SELECT idPonto1,idPonto2
			FROM PARVALIDO
			WHERE idPonto1 = %s OR idPonto2 = %s;
		""", [id_localizacao, id_localizacao])
	resultado = cur.fetchall()
	return resultado


def dump_pares_localizacoes(lista_localizacoes: list,
                            cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as informações de todos os pares validos de uma
		lista de localizações
	'''
	cur.execute(
	    """
			SELECT *
			FROM PARVALIDO
			WHERE idPonto1 = ANY(%s) OR idPonto2 = ANY(%s);
		""", [lista_localizacoes, lista_localizacoes])
	resultado = cur.fetchall()
	return resultado


def dump_viagem(id_viagem: str | int,
                cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''	
	Performa um SELECT * em uma viagem
	'''

	cur.execute("""
		SELECT * FROM VIAGEM WHERE idViagem = %s;
	""", [id_viagem])
	resultado = cur.fetchone()
	return resultado


def get_pessoas_para_notificar(
        data: datetime, cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as pessoas pertencentes a um grupo
		de pesquisa que tiveram sua remuneração alterada desde 'data'
	'''
	cur.execute(
	    """
		SELECT idPessoa,nome,email,PESSOA.idGrupo,data,remuneracao_atual
		FROM 
		(
			SELECT DISTINCT ON (idGrupo) idGrupo,data,remuneracao_atual
			FROM HISTORICOGPESQUISA
			WHERE data >= %s
			ORDER BY idGrupo, data DESC NULLS LAST
		) AS GRUPO,PESSOA
		WHERE GRUPO.idGrupo = PESSOA.idGrupo;
	""", [data])
	resultado = cur.fetchall()
	return resultado


def get_pessoas_com_ambas_localizacoes(
        id_ponto1: str | int, id_ponto2: str | int,
        cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve uma lista de pessoas
		que possuem um par de localizações
		como válido
		(consulta ajuda a saber se parvalido pode
		ser removido)
	'''
	cur.execute(
	    """
		SELECT idPessoa
		FROM (
			SELECT idPessoa,COUNT(idLocalizacao) as aparicoes
			FROM (
				SELECT idLocalizacao,idPessoa
				FROM APELIDOLOCALIZACAO
				UNION
				(
					SELECT idLocalizacao,idPessoa
					FROM PESSOAESTACAO
				)
			) AS TODAS_LOCS
			WHERE idLocalizacao in (%s,%s)
			GROUP BY idPessoa
		) AS APARICOES
		WHERE aparicoes > 1
	""", [id_ponto1, id_ponto2])
	resultado = cur.fetchall()
	return resultado


def get_feedback_viagem_info(id_viagem: int,
                             cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as infos de um feedback
	'''
	cur.execute(
	    """
			SELECT tokenFeedback,respondido,motivoOriginal
			FROM FEEDBACKVIAGEM
			WHERE idViagem = %s;
		""", [id_viagem])
	resultado = cur.fetchone()
	return resultado


def atingiu_limite_diario_viagens_perdidas(id_pessoa: int | str,
                                           cur: psycopg2.extensions.cursor,
                                           limite=3) -> bool:
	'''
		Devolve true se pessoa já atingiu o limite de feedbacks
		de viagens perdidas em um dia
	'''
	dia_hoje = date.today()
	cur.execute(
	    """
			SELECT COUNT(dataFeedback)
			FROM FEEDBACKVIAGEMPERDIDA
			WHERE dataFeedback > %s AND idPessoa = %s;
		""", [dia_hoje, id_pessoa])
	resultado = cur.fetchone()
	if resultado is None:
		return False
	return resultado[0] >= limite


def dump_bonus(id_bonus: int | str,
               cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as informações de um bônus
	'''
	cur.execute(
	    """
		SELECT nome,valor,ativo,visivel,descricao,dataInicio,dataFim
		FROM BONUS
		WHERE idBonus = %s;
	""", [id_bonus])
	resultado = cur.fetchone()
	return resultado


def get_bonus_usuario(id_pessoa: int | str,
                      cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos os bônus, incluindo a informação específica para aquele usuário
	'''
	cur.execute(
	    """
		SELECT *
		FROM (
			SELECT idBonus,nome,valor,ativo,descricao,dataInicio,dataFim,NULL
			FROM BONUS
			WHERE visivel = 't' AND idBonus NOT IN 
				(SELECT idBonus FROM PESSOABONUS WHERE idPessoa = %s)
			UNION (
				SELECT idBonus,nome,valor,ativo,descricao,dataInicio,dataFim,dataConcessao
				FROM BONUS
				NATURAL JOIN PESSOABONUS
				WHERE visivel = 't' AND idPessoa = %s
			)
		) as pessoa_bonus
		ORDER BY idBonus asc;
		""", [id_pessoa, id_pessoa])
	resultado = cur.fetchall()
	return resultado


def get_contestacoes_usuario(id_pessoa: int | str,
                             cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todas as contestações de um usuário
	'''
	cur.execute(
	    """
			SELECT CONTESTACAO.idViagem,CONTESTACAO.data,
				justificativa,aprovada,resposta,VIAGEM.remuneracao
			FROM CONTESTACAO
			JOIN VIAGEM
			ON CONTESTACAO.idViagem = VIAGEM.idViagem
			WHERE VIAGEM.idPessoa = %s
			ORDER BY CONTESTACAO.data desc;
		""", [id_pessoa])
	resultado = cur.fetchall()
	return resultado


def pessoa_eh_admin(id_pessoa: int | str,
                    cur: psycopg2.extensions.cursor) -> bool:
	'''
		Devolve true se campo admin é true
		na tabela PESSOA
	'''
	cur.execute(
	    """
		SELECT admin
		FROM auth_bikesp.USUARIO WHERE idPessoa = %s;
	""", [id_pessoa])
	resultado = cur.fetchone()
	return resultado is not None and resultado[0]


def pessoa_validou_locs(id_pessoa: int | str,
                        cur: psycopg2.extensions.cursor) -> bool:
	'''
		Devolve true se campo validouLocs é true
		na tabela PESSOA
	'''
	cur.execute(
	    """
		SELECT validouLocais
		FROM PESSOA WHERE idPessoa = %s;
	""", [id_pessoa])
	resultado = cur.fetchone()
	return resultado is not None and resultado[0]


def get_contestacao(id_viagem: int | str,
                    cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
	Devolve uma contestação de acordo com o id da viagem
	'''
	cur.execute("""
		SELECT * FROM CONTESTACAO WHERE idviagem = %s
		""", [id_viagem])
	resultado = cur.fetchone()
	return resultado


def get_bonus(cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos os bonus existentes
	'''
	cur.execute("""
			SELECT * FROM BONUS;
		""")
	resultado = cur.fetchall()
	return resultado


def get_bonus_pagina(pagina: int | str,
                     cur: psycopg2.extensions.cursor,
                     limite=5,
                     busca="") -> Optional[Any]:
	'''
		Devolve todos os bonus existentes paginado
	'''
	if len(busca) > 0:
		busca_regex = '%' + busca + '%'
		cur.execute(
		    """
			SELECT * FROM BONUS WHERE nome ILIKE %s LIMIT %s OFFSET (%s * %s);
			""", [busca_regex, limite, pagina, limite])
	else:
		cur.execute(
		    """
			SELECT * FROM BONUS LIMIT %s OFFSET (%s * %s);
			""", [limite, pagina, limite])
	resultado = cur.fetchall()
	return resultado


def get_pessoas(cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve todos as pessoas existentes
	'''
	cur.execute("""
			SELECT * FROM PESSOA;
		""")
	resultado = cur.fetchall()
	return resultado


def get_pessoas_pagina(pagina: int | str,
                       cur: psycopg2.extensions.cursor,
                       limite=5,
                       busca="") -> Optional[Any]:
	'''
		Devolve todos as pessoas existentes paginado
	'''
	if len(busca) > 0:
		busca_regex = '%' + busca + '%'
		cur.execute(
		    """
			SELECT * FROM PESSOA WHERE nome ILIKE %s LIMIT %s OFFSET (%s * %s);
			""", [busca_regex, limite, pagina, limite])
	else:
		cur.execute(
		    """
			SELECT * FROM PESSOA LIMIT %s OFFSET (%s * %s);
		""", [limite, pagina, limite])
	resultado = cur.fetchall()
	return resultado


def aux_busca_contestacao_data(cur: psycopg2.extensions.cursor,
                               data_inicial: datetime, data_final: datetime,
                               limite: int, pagina: int | str):
	'''
	Busca contestacoes dentro de um período de tempo
	'''
	cur.execute(
	    """
        SELECT * FROM CONTESTACAO WHERE data >= %s AND data < %s LIMIT %s OFFSET (%s * %s);
        """, [data_inicial, data_final, limite, pagina, limite])


def aux_busca_contestacao_id(cur: psycopg2.extensions.cursor,
                             idviagem: int | str):
	'''
	Busca contestação por idviagem
	'''
	cur.execute(
	    """
        SELECT * FROM CONTESTACAO WHERE idviagem = %s;
        """, [idviagem])


def aux_busca_contestacao_status(cur: psycopg2.extensions.cursor, status: str,
                                 limite: int, pagina: int | str):
	'''
	Busca contestacoes com um status específico de aprovação
	'''
	if status is None:
		cur.execute(
		    """
            SELECT * FROM CONTESTACAO WHERE aprovada IS NULL LIMIT %s OFFSET (%s * %s);
            """, [limite, pagina, limite])
	else:
		cur.execute(
		    """
            SELECT * FROM CONTESTACAO WHERE aprovada = %s LIMIT %s OFFSET (%s * %s);
            """, [status, limite, pagina, limite])


def aux_busca_contestacao_justificativa(cur: psycopg2.extensions.cursor,
                                        busca_regex: str, limite: int,
                                        pagina: int | str):
	'''
	Busca contestacoes de acordo com uma string presente na justificativa
	'''
	cur.execute(
	    """
        SELECT * FROM CONTESTACAO WHERE justificativa ILIKE %s LIMIT %s OFFSET (%s * %s);
        """, [busca_regex, limite, pagina, limite])


def get_contestacoes_pagina(pagina: int | str,
                            cur: psycopg2.extensions.cursor,
                            limite=5,
                            busca="") -> Optional[Any]:
	'''
    Devolve contestacoes de forma paginada
    '''
	if len(busca) > 0:
		# Tenta busca por data, então ID, então justificativa
		try:
			busca_data = datetime.strptime(busca, '%d/%m/%y')
			prox_dia = busca_data + timedelta(days=1)
			aux_busca_contestacao_data(cur, busca_data, prox_dia, limite,
			                           pagina)
		except ValueError:
			if busca.isdigit():
				aux_busca_contestacao_id(cur, busca)
			elif busca in ('Sim', 'Não', 'Pendente'):
				busca = True if busca == "Sim" else (
				    False if busca == "Não" else None)
				aux_busca_contestacao_status(cur, busca, limite, pagina)
			else:
				busca_regex = '%' + busca + '%'
				aux_busca_contestacao_justificativa(cur, busca_regex, limite,
				                                    pagina)
	else:
		cur.execute(
		    """
            SELECT * FROM CONTESTACAO LIMIT %s OFFSET (%s * %s);
            """, [limite, pagina, limite])
	resultado = cur.fetchall()
	return resultado


def get_viagens_pagina(pagina: int | str,
                       cur: psycopg2.extensions.cursor,
                       limite=5,
                       busca="") -> Optional[Any]:
	'''
        Devolve todas as viagens existentes paginado
    '''
	if len(busca) > 0:
		cur.execute(
		    """
            SELECT * FROM VIAGEM WHERE idPessoa = %s LIMIT %s OFFSET (%s * %s);
            """, [busca, limite, pagina, limite])
	else:
		cur.execute(
		    """
            SELECT * FROM VIAGEM LIMIT %s OFFSET (%s * %s);
            """, [limite, pagina, limite])
	resultado = cur.fetchall()
	return resultado


def get_email_por_id(ids: int | str | list[int | str],
                     cur: psycopg2.extensions.cursor) -> Optional[list[str]]:
	"""
    Retorna os e-mails correspondentes a uma lista de IDs.
	"""
	if not isinstance(ids, list):
		ids = [ids]

	# Create a string with the appropriate number of placeholders
	placeholders = ', '.join(['%s'] * len(ids))

	query = f"""
        SELECT email FROM PESSOA 
        WHERE idPessoa IN ({placeholders});
    """

	cur.execute(query, ids)

	resultado = cur.fetchall()
	if not resultado:
		return None

	return [row[0] for row in resultado]


def get_info_pessoas_coortes(ids_coortes: str | int | list[str | int],
                             cur: psycopg2.extensions.cursor) -> Optional[Any]:
	'''
		Devolve id e email das pessoas pertencentes aos coortes passados
	'''
	if not isinstance(ids_coortes, list):
		ids_coortes = [ids_coortes]

	placeholders = ', '.join(['%s'] * len(ids_coortes))

	query = f"""
		SELECT idpessoa, email FROM PESSOA
		WHERE idGrupo IN({placeholders});
	"""

	cur.execute(query, ids_coortes)

	resultado = cur.fetchall()
	if not resultado:
		return None

	return resultado
