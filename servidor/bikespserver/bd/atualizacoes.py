'''
	Atualizações no banco de dados
	(OBS: algumas atualizações também podem estar em insercoes.py)
'''
import sys
from pathlib import Path
from typing import Tuple
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd import consultas
from servidor.bikespserver.classes.viagem_bd import ViagemBD
from servidor.bikespserver.utils import parsers

#pylint: enable=wrong-import-position


def coordenada_localizacao(id_localizacao: str | int, coordenada: Tuple[float,
                                                                        float],
                           cur: psycopg2.extensions.cursor) -> None:
	'''
		Atualiza o atributo coordenada de uma localização
	'''
	latitude = coordenada[0]
	longitude = coordenada[1]
	cur.execute(
	    """
			UPDATE LOCALIZACAO SET coordenadas = POINT(%s,%s)
			WHERE idLocalizacao = %s;
		""", [latitude, longitude, id_localizacao])


def atualiza_viagem_existente(id_viagem: str | int, viagem: ViagemBD,
                              cur: psycopg2.extensions.cursor) -> None:
	'''
		Dada uma viagem na forma de objeto viagem BD, atualiza
		a viagem existente no BD
	'''
	atributos = [
	    att for att in dir(viagem)
	    if not att.startswith('__') and not callable(getattr(viagem, att))
	]
	tipos_valores = list(map(lambda x: '%s', atributos))
	valores = list(map(lambda x: getattr(viagem, x), atributos))
	valores.append(id_viagem)

	nomes_atributos_string = "(" + ",".join(atributos) + ")"
	tipos_atributos_string = "(" + ",".join(tipos_valores) + ")"
	cur.execute(
	    """
	 		UPDATE VIAGEM SET """ + nomes_atributos_string + """ =
	 		""" + tipos_atributos_string + """
	 		WHERE idViagem = %s;
	 	""", valores)


def atualiza_contestacao(id_viagem: int | str, aprovar: bool, resposta: str,
                         cur: psycopg2.extensions.cursor) -> None:
	'''
		Atualiza o status de uma contestação
	'''
	cur.execute(
	    """
			UPDATE CONTESTACAO SET 
			(aprovada,resposta) = (%s,%s)
			WHERE idViagem = %s;
		""", [aprovar, resposta, id_viagem])


def atualiza_bilhete_unico(dict_atualizacao: dict,
                           cur: psycopg2.extensions.cursor) -> None:
	'''
		Atualiza uma entrada da tabela BILHETEUNICO
	'''
	bilhete_unico = parsers.parse_bilhete_unico(
	    dict_atualizacao["NumBilheteUnico"])

	info_bilhete_unico = consultas.bilhete_unico_existe(bilhete_unico, cur)

	id_pessoa = dict_atualizacao["idPessoa"]
	data_inicio = dict_atualizacao["dataInicio"]
	data_fim = dict_atualizacao["dataFim"]
	ativo = dict_atualizacao["ativo"]
	concedido = dict_atualizacao["concedido"]
	aguardando_envio = dict_atualizacao["aguardandoEnvio"]
	aguardando_resposta = dict_atualizacao["aguardandoResposta"]

	if info_bilhete_unico is not None:
		cur.execute(
		    """
			UPDATE BILHETEUNICO SET (idPessoa,dataInicio,dataFim,ativo,
				concedido,aguardandoEnvio,aguardandoResposta)=(%s,%s,%s,%s,%s,%s,%s)
			WHERE bilheteUnico = %s;
			""", [
		        id_pessoa, data_inicio, data_fim, ativo, concedido,
		        aguardando_envio, aguardando_resposta, bilhete_unico
		    ])


# pylint: disable=invalid-name
def atualiza_endereco_PESSOA(id_pessoa: str | int, endereco: dict | str,
                             cur: psycopg2.extensions.cursor) -> None:
	'''
		Atualiza o atributo endereco da tabela PESSOA
		Parâmetro 'endereco' pode ser tanto um dicionário com os campos adequados
		quanto a string final já formatada para o BD (use o parser).
	'''
	if isinstance(endereco, dict):
		rua = endereco.get("rua", "")
		numero = endereco.get("numero", "")
		bairro = endereco.get("bairro", "")
		cep = endereco.get("cep", "")
		cidade = endereco.get("cidade", "")
		estado = endereco.get("estado", "")

		endereco = parsers.parse_endereco(rua,
		                                  numero,
		                                  bairro=parsers.parse_bairro(bairro),
		                                  cep=parsers.parse_cep(cep),
		                                  cidade=parsers.parse_cidade(cidade),
		                                  estado=parsers.parse_estado(estado))

	cur.execute(
	    """
		UPDATE PESSOA set endereco = %s
		WHERE idPessoa = %s;
		""", [endereco, id_pessoa])


# pylint: enable=invalid-name
def atualiza_feedback_viagem(dict_respostas: dict, id_viagem: int,
                             cur: psycopg2.extensions.cursor) -> None:
	'''
		Atualiza feedback de uma viagem com as respostas do usuário
	'''
	respondido = dict_respostas["respondido"]
	outros_apps = dict_respostas["outrosApps"]
	tempo_clima = dict_respostas["tempoClima"]
	feedback_geral = dict_respostas["feedbackGeral"]
	respostas_dinamicas = dict_respostas["respostasDinamicas"]

	cur.execute(
	    """
			UPDATE FEEDBACKVIAGEM
			SET (respondido,outrosApps,tempoClima,feedbackGeral,respostasDinamicas)=(%s,%s,%s,%s,%s)
			WHERE idViagem = %s;
		""", [
	        respondido, outros_apps, tempo_clima, feedback_geral,
	        respostas_dinamicas, id_viagem
	    ])


def atualiza_validacao_locs_pessoa(id_pessoa: str | int, validou: bool,
                                   cur: psycopg2.extensions.cursor) -> None:
	'''
		Seta o atributo validouLocais de uma pessoa como true ou false
	'''
	cur.execute("UPDATE PESSOA SET validouLocais = %s WHERE idPessoa = %s;",
	            [("t" if validou else "f"), id_pessoa])
