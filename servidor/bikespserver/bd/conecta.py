'''
    Cria a conexão com o banco de dados
'''
import os
import psycopg2

from flask import current_app


def conecta(force_bd_prod=False,
            force_bd_dev=False) -> psycopg2.extensions.connection:
	'''
        Realiza a conexão com o banco de dados
    '''

	if force_bd_prod:
		host = os.getenv("BD_HOST")
		database = os.getenv("BD_NOME")
		user = os.getenv("BD_USUARIO")
		password = os.getenv("BD_SENHA")
		port = os.getenv("BD_PORT", "5432")
	elif force_bd_dev:
		host = os.getenv("BD_HOST")
		database = os.getenv("BD_NOME_TESTE")
		user = os.getenv("BD_USUARIO_TESTE")
		password = os.getenv("BD_SENHA_TESTE")
		port = os.getenv("BD_PORT", "5432")
	else:
		try:
			# print("Vou pegar do app")
			host = current_app.config["BD_HOST"]
			database = current_app.config["BD_NOME"]
			user = current_app.config["BD_USUARIO"]
			password = current_app.config["BD_SENHA"]
			port = current_app.config["BD_PORT"]
		except RuntimeError:
			# print("Vou pegar do env")
			host = os.getenv("BD_HOST")
			database = os.getenv("BD_NOME_TESTE")
			user = os.getenv("BD_USUARIO_TESTE")
			password = os.getenv("BD_SENHA_TESTE")
			port = os.getenv("BD_PORT", "5432")

	# print("Vou conectar a ", host, database,user,password)

	con = psycopg2.connect(host=host,
	                       database=database,
	                       user=user,
	                       password=password,
	                       port=port)
	return con
