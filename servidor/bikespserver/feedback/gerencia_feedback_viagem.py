'''
	Módulo responsável por gerenciar os feedbacks de viagens
'''
import sys
import json

from pathlib import Path
from threading import Thread

from flask import request, current_app, url_for

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_info, get_pessoa_info_pelo_id_pessoa, \
 get_feedback_viagem_info, get_viagem_info
from servidor.bikespserver.bd.atualizacoes import atualiza_feedback_viagem
from servidor.bikespserver.bd.insercoes import insere_FEEDBACKVIAGEM

from servidor.bikespserver.autenticacao.gerencia_sessao import gera_sessao

from servidor.bikespserver.email.envia_email import envia_email

from servidor.bikespserver.remuneracao.insere_remuneracao import insere_remuneracao_para_enviar

from servidor.bikespserver.feedback.consts import REMUNERACAO_POR_FEEDBACK, \
  PERGUNTAS_FEEDBACK_FIXAS, TAMANHO_MAXIMO_RESPOSTA_DINAMICA

#pylint: enable=wrong-import-position


def requisita_feedback_viagem(id_usuario: int | str, id_viagem: int | str,
                              motivo_status: str,
                              cur: psycopg2.extensions.cursor):
	'''
		Cria entrada na tabela FEEDBACKVIAGEM e dispara email
		ao usuário para ele responder o formulário de feedback
	'''
	if id_viagem is None:
		return "Viagem inexistente"

	token_feedback = gera_sessao((str(id_viagem) + str(id_usuario)))["token"]

	feedback_vazio = cria_dict_feedback(id_viagem, False, token_feedback,
	                                    motivo_status, {},
	                                    PERGUNTAS_FEEDBACK_FIXAS)

	insere_FEEDBACKVIAGEM(feedback_vazio, cur)

	base_url = url_for('bikesp.__feedback_viagem',
	                   _external=True,
	                   _scheme=current_app.config["PREFERRED_URL_SCHEME"])

	link_feedback = base_url + "/?token=" + token_feedback + "&idViagem=" + str(
	    id_viagem)

	email_pessoa = get_pessoa_info_pelo_id_pessoa(id_usuario, cur)[1]
	data_viagem = get_viagem_info(id_viagem, cur)[1]

	# Não envia o email se estivermos apenas testando
	if current_app.debug or (
	    "TESTING" in current_app.config and current_app.config["TESTING"]
	) or current_app.config["PREFERRED_URL_SCHEME"] == "http":
		return link_feedback

	with current_app.app_context():
		senha_email = current_app.config["MAIL_PASSWORD"]
		enviador = current_app.config["MAIL_DEFAULT_SENDER"]
		assunto_email = f"[Piloto Bike SP] Feedback da viagem #{str(id_viagem)} ({str(data_viagem)})"
		mensagem = (
		    "Olá, <br><br>"
		    f"Deixe seu feedback sobre a viagem #{str(id_viagem)} do dia {str(data_viagem)}.<br>"
		    f'Acesse o link: <a href="{link_feedback}">Feedback</a><br>'
		    'Caso queira deixar um feedback sobre uma viagem que não pôde ser registrada,'
		    'acesse o link contido na tela Fale Conosco do aplicativo.<br><br>'
		    "Atenciosamente,<br>"
		    "Equipe Piloto Bike SP")
		recebedores = [email_pessoa]

		Thread(
		    target=envia_email,
		    args=(enviador, senha_email, recebedores, assunto_email, mensagem),
		).start()

	return "Link enviado"


def registra_feedback_viagem():  #pylint: disable = too-many-return-statements
	'''
		Atualiza entrada da tabela FEEDBACKVIAGEM
		com a resposta do usuário
	'''
	cpf = request.form["cpf"]
	token = request.form["token"]
	id_viagem = request.form["idViagem"]

	con = conecta()
	cur = con.cursor()

	pessoa_info = get_pessoa_info(cpf, cur)
	if pessoa_info is None:
		return ({"estado": "Erro", "erro": "CPF inválido"}, 400)

	id_pessoa = int(pessoa_info[0])
	viagem_info = get_viagem_info(id_viagem, cur)

	feedback_existe = get_feedback_viagem_info(id_viagem, cur)

	if viagem_info is None or id_pessoa != viagem_info[
	    0] or feedback_existe is None or token != feedback_existe[0]:
		return ({"estado": "Erro", "erro": "Requisição inválida"}, 400)

	if feedback_existe[1]:
		return ({"estado": "Erro", "erro": "Feedback já respondido"}, 400)

	dict_respostas = armazena_respostas(request.form, PERGUNTAS_FEEDBACK_FIXAS)

	# Formulário não pode ser inteiro vazio
	# pylint: disable = duplicate-code
	if len(dict_respostas.keys()) == 0:
		return ({
		    "estado": "Erro",
		    "erro": "O formulário não pode ser vazio"
		}, 400)
	# pylint: enable = duplicate-code

	insere_remuneracao_para_enviar(id_pessoa, REMUNERACAO_POR_FEEDBACK, cur)

	dict_atualizacao = cria_dict_feedback(id_viagem, True, None, None,
	                                      dict_respostas,
	                                      PERGUNTAS_FEEDBACK_FIXAS)

	atualiza_feedback_viagem(dict_atualizacao, id_viagem, cur)

	con.commit()
	cur.close()
	con.close()
	return ({"estado": "Sucesso"}, 200)


# pylint: disable=too-many-arguments
def cria_dict_feedback(id_viagem: int | str, respondido: bool, token: str,
                       motivo: str, respostas: dict,
                       perguntas_fixas: dict) -> dict:
	'''
		Cria um dicíonário que representa uma entrada da tabela FEEDBACKVIAGEM
		ou da tabela FEEDBACKVIAGEMPERDIDA
	'''
	feedback_dict = {
	    "idViagem": id_viagem,
	    "tokenFeedback": token,
	    "motivoOriginal": motivo,
	    "respondido": respondido,
	}  # type: dict[str, (int|str|None)]

	for pergunta in perguntas_fixas:
		if pergunta not in respostas:
			feedback_dict[pergunta] = None
		else:
			feedback_dict[pergunta] = respostas[pergunta]

	feedback_dict["respostasDinamicas"] = None
	if "respostasDinamicas" in respostas:
		feedback_dict["respostasDinamicas"] = json.dumps(
		    respostas["respostasDinamicas"])

	return feedback_dict


def armazena_respostas(forms: dict, respostas_fixas: dict) -> dict:
	'''
		Recebe o resultado do forms e armazena
		as respostas em um dicionário.
		Respostas dinâmicas ficam em uma chave separada
	'''
	dict_respostas = {}  # type: dict[str, (dict[str,str]|int|str|None)]

	respostas_dinamicas = {}
	for chave in forms:
		valor = forms[chave]
		chave_split = chave.split(":")
		if len(chave_split) < 2:
			continue
		tipo, id_pergunta = chave_split
		if id_pergunta in respostas_fixas and tipo in ["F", "f"
		                                              ] and valor != '':
			dict_respostas[id_pergunta] = str(
			    valor)[0:respostas_fixas[id_pergunta]["tamanhoMaximo"]]
		if tipo in ["D", "d"]:
			respostas_dinamicas[id_pergunta] = str(
			    valor)[0:TAMANHO_MAXIMO_RESPOSTA_DINAMICA]

	if len(respostas_dinamicas.keys()) != 0:
		dict_respostas["respostasDinamicas"] = dict(respostas_dinamicas)

	return dict_respostas
