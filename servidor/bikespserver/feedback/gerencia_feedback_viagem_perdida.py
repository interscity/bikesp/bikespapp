'''
	Módulo responsável por gerenciar os feedbacks de viagens perdidas
'''

import sys
import json

from pathlib import Path
from datetime import datetime

from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta

from servidor.bikespserver.bd.consultas import get_pessoa_info, \
 atingiu_limite_diario_viagens_perdidas

from servidor.bikespserver.bd.insercoes import insere_FEEDBACKVIAGEMPERDIDA

from servidor.bikespserver.autenticacao.gerencia_sessao import token_valido

from servidor.bikespserver.remuneracao.insere_remuneracao import insere_remuneracao_para_enviar

from servidor.bikespserver.feedback.consts import REMUNERACAO_POR_FEEDBACK, \
 PERGUNTAS_FEEDBACK_PERDIDAS_FIXAS

from servidor.bikespserver.feedback.gerencia_feedback_viagem import armazena_respostas
#pylint: enable=wrong-import-position


def registra_feedback_viagem_perdida(
):  #pylint: disable = too-many-return-statements
	'''
		Adiciona uma entrada na tabela FEEDBACKVIAGEMPERDIDA
		contendo informações sobre uma viagem perdida
	'''
	con = conecta()
	cur = con.cursor()
	cpf = request.form["cpf"]
	token = request.form["token"]
	pessoa_info = get_pessoa_info(cpf, cur)

	if pessoa_info is None:
		return ({"estado": "Erro", "erro": "CPF inválido"}, 400)

	id_pessoa = int(pessoa_info[0])
	if not token_valido(id_pessoa, token, cur):
		return ({"estado": "Erro", "erro": "Problema de autenticação"}, 400)

	if atingiu_limite_diario_viagens_perdidas(id_pessoa, cur, limite=2):
		return ({
		    "estado":
		        "Erro",
		    "erro":
		        "Você atingiu o limite diário de feedbacks de viagens perdidas"
		}, 400)

	insere_remuneracao_para_enviar(id_pessoa, REMUNERACAO_POR_FEEDBACK, cur)

	dict_respostas = armazena_respostas(request.form,
	                                    PERGUNTAS_FEEDBACK_PERDIDAS_FIXAS)

	# Formulário não pode ser completamente vazio
	if len(dict_respostas.keys()) == 0:
		return ({
		    "estado": "Erro",
		    "erro": "O formulário não pode ser vazio"
		}, 400)

	dict_feedback_perdida = cria_dict_feedback(
	    id_pessoa, dict_respostas, PERGUNTAS_FEEDBACK_PERDIDAS_FIXAS)

	insere_FEEDBACKVIAGEMPERDIDA(dict_feedback_perdida, cur)

	con.commit()
	cur.close()
	con.close()

	return ({"estado": "Sucesso"}, 200)


def cria_dict_feedback(id_pessoa: int | str, respostas: dict,
                       perguntas_fixas: dict) -> dict:
	'''
		Cria um dicíonário que representa uma entrada da tabela FEEDBACKVIAGEM
	'''
	dict_feedback_perdida = {
	    "idPessoa": id_pessoa,
	    "dataFeedback": datetime.now(),
	}  # type: dict[str, (datetime|int|str|None)]

	for pergunta in perguntas_fixas:
		if pergunta not in respostas:
			dict_feedback_perdida[pergunta] = None
		else:
			dict_feedback_perdida[pergunta] = respostas[pergunta]

	dict_feedback_perdida["respostasDinamicas"] = None
	if "respostasDinamicas" in respostas:
		dict_feedback_perdida["respostasDinamicas"] = json.dumps(
		    respostas["respostasDinamicas"])

	return dict_feedback_perdida
