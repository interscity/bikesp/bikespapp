'''
	Módulo com constantes usadas no gerenciamento do feedback
'''

REMUNERACAO_POR_FEEDBACK = 1.65

TAMANHO_MAXIMO_RESPOSTA_DINAMICA = 1000

PERGUNTAS_FEEDBACK_FIXAS = {
    "tempoClima": {
        "tamanhoMaximo": 30
    },
    "outrosApps": {
        "tamanhoMaximo": 200
    },
    "feedbackGeral": {
        "tamanhoMaximo": 1000
    }
}

PERGUNTAS_FEEDBACK_PERDIDAS_FIXAS = {
    "quandoPerdeu": {
        "tamanhoMaximo": 1000
    },
    "algoDiferente": {
        "tamanhoMaximo": 1000
    },
    "verApp": {
        "tamanhoMaximo": 20
    },
    "feedbackGeral": {
        "tamanhoMaximo": 1000
    },
}
