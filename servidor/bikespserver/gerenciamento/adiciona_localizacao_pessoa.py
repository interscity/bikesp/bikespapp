'''
	Módulo responsável por adicionar uma localização a certa pessoa
'''

import sys
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.insercoes import insere_LOCALIZACAO, \
 insere_APELIDOLOCALIZACAO,insere_PESSOAESTACAO
from servidor.bikespserver.bd.consultas import get_localizacao_info, \
 get_todas_localizacoes_pessoa, localizacao_existe
from servidor.bikespserver.gerenciamento.registra_resposta import insere_pares_validos
from servidor.bikespserver.utils.coords import coordenada_valida

#pylint: enable=wrong-import-position


# pylint: disable=too-many-arguments
def adiciona_localizacao_pessoa(id_pessoa: str | int,
                                id_localizacao: str | int | None,
                                tipo_localizacao: str,
                                apelido: str | None,
                                endereco: str | None,
                                coordenada: list | None,
                                cur: psycopg2.extensions.cursor,
                                force_prod=True):
	'''
		Adiciona uma localização a um usuário
		(ela pode ser preexistente ou não)
	'''
	sucesso = True
	status = "Sucesso"

	if tipo_localizacao == "qualquer" and apelido is None:
		sucesso = False
		status = "Apelido faltando para a localização"
	elif id_localizacao is not None:
		id_localizacao = int(id_localizacao)
		# Localização já existe
		info_loc = get_localizacao_info(id_localizacao, cur)
		if info_loc is None:
			sucesso = False
			status = "A localização informada não existe"
		else:
			if tipo_localizacao is not None and tipo_localizacao != info_loc[2]:
				sucesso = False
				status = "Tipo informado não condiz com tipo da localização"
			else:
				endereco, coordenada, tipo_localizacao = info_loc
	else:
		if coordenada is None or not coordenada_valida(coordenada):
			sucesso = False
			status = "A coordenada informada é inválida"
		else:
			insere_LOCALIZACAO(
			    {
			        "endereco": endereco,
			        "coordenadas": coordenada,
			        "tipoLocalizacao": tipo_localizacao
			    }, cur)
			id_localizacao = int(
			    localizacao_existe(coordenada, endereco, cur)[0])

	if not sucesso:
		return [sucesso, status]

	[sucesso, status] = liga_localizacao_locs_antigas(id_pessoa, id_localizacao,
	                                                  cur, force_prod)
	if not sucesso:
		return [sucesso, status]

	liga_localizacao_pessoa(id_pessoa, id_localizacao, tipo_localizacao,
	                        apelido, cur)

	return [sucesso, status]


def liga_localizacao_locs_antigas(id_pessoa: str | int,
                                  id_localizacao: str | int | None,
                                  cur: psycopg2.extensions.cursor,
                                  force_prod=True):
	'''
		Liga localização nova às localizações antigas da pessoa
	'''
	sucesso = True
	status = "Sucesso"
	localizacoes_pessoa = get_todas_localizacoes_pessoa(id_pessoa, cur)
	for localizacao in localizacoes_pessoa:
		id_loc_velha = int(localizacao[0])
		if id_localizacao == int(id_loc_velha):
			sucesso = False
			status = "Pessoa já está ligada a essa localização"
			return [sucesso, status]
		insere_pares_validos([id_localizacao, id_loc_velha],
		                     cur,
		                     force_prod=force_prod)
	return [sucesso, status]


def liga_localizacao_pessoa(id_pessoa: str | int,
                            id_localizacao: str | int | None,
                            tipo_localizacao: str, apelido: str | None,
                            cur: psycopg2.extensions.cursor):
	'''
		Liga localização nova à pessoa
		(tabela APELIDOLOCALIZACAO ou PESSOAESTACAO)
	'''
	if tipo_localizacao == "qualquer":
		insere_APELIDOLOCALIZACAO(
		    {
		        "idPessoa": id_pessoa,
		        "idLocalizacao": id_localizacao,
		        "apelido": apelido
		    }, cur)
	else:
		insere_PESSOAESTACAO(
		    {
		        "idPessoa": id_pessoa,
		        "idLocalizacao": id_localizacao
		    }, cur)
