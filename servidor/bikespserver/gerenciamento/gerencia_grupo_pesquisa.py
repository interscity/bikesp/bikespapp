'''
	Gerencia os grupos de pesquisa
'''
import sys
from pathlib import Path

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd import remocoes
from servidor.bikespserver.bd import insercoes
#pylint: enable=wrong-import-position


def insere_grupo_pesquisa(id_grupo: str, valor: str | float,
                          con: psycopg2.extensions.connection) -> None:
	'''
		Comunica-se com o bd para inserir/atualizar determinado grupo
	'''
	cur = con.cursor()
	grupo = {"idGrupo": id_grupo, "remuneracao": float(valor)}
	cur = insercoes.insere_GRUPOPESQUISA(grupo, cur)
	con.commit()
	cur.close()


def remove_grupo_pesquisa(id_grupo: str,
                          con: psycopg2.extensions.connection) -> None:
	'''
		Comunica-se com o bd para remover determinado grupo
	'''
	cur = con.cursor()
	cur = remocoes.remove_GRUPOPESQUISA(id_grupo, cur)
	con.commit()
	cur.close()
