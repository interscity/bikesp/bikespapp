'''
	Notifica usuários que foram inseridos no BD
'''
import sys
import os
from pathlib import Path

#import config  # pylint: disable=unused-import

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.email.envia_email import envia_email
#pylint: enable=wrong-import-position


def notifica_pessoas_inseridas(email_usuarios: list, debug=False):
	'''
		Notifica por email as pessoas que foram inseridas no BD
	'''
	for email in email_usuarios:
		if email == "googleman@google.com":
			continue
		notifica_pessoa(email, debug=debug)
		print(email)


#pylint: disable=too-many-arguments
def notifica_pessoa(email_pessoa: str, debug=False):
	'''
		Notifica uma pessoa que foi inserida no BD
	'''
	print("Notificando " + email_pessoa)

	recebedor = [email_pessoa]

	assunto = "[Bike SP] Acesso ao aplicativo liberado"

	body = (
	    "Olá,<br><br>"
	    "Obrigado por ter respondido ao questionário! Agora, "
	    "você já pode instalar o aplicativo, se cadastrar, e fazer a viagem de validação.<br><br>"
	    "<h3><b>PASSO A PASSO</b></h3> <br>"
	    "<b>INSTALAÇÃO DO APLICATIVO</b> - O aplicativo está disponível "
	    "na página da Google Play Store com o nome “Bike SP”. "
	    "Você pode acessar a página por "
	    '<a href="https://play.google.com/store/apps/details?id=com.bikesp&pli=1">'
	    'esse link</a>; <br><br>'
	    "<b>VIAGEM DE VALIDAÇÃO</b> - Após se cadastrar e logar no aplicativo, "
	    "registre uma primeira viagem para que possamos confirmar que tudo deu "
	    "certo no seu processo de cadastro, "
	    "login, e registro de viagens. <b>Essa viagem não precisa necessariamente "
	    "ser feita de bicicleta "
	    "e ela não será remunerada</b>. Basta iniciar uma viagem no aplicativo em um local válido "
	    "(ou seja, previamente cadastrado) e finalizar a viagem em um local válido também. "
	    "Para essa viagem, você pode optar por iniciar a viagem em um local cadastrado e finalizar a "
	    "viagem logo em seguida nesse mesmo local. Nesse caso, espere pelo menos 1 "
	    "minuto antes de finalizar a viagem.<br><br>"
	    "Atenciosamente,<br>"
	    "Equipe Piloto Bike SP")

	if not debug:
		user_mail = os.getenv("MAIL_USERNAME")
		senha_mail = os.getenv("MAIL_PASSWORD")
		envia_email(user_mail, senha_mail, recebedor, assunto, body)
	return {"recebedor": recebedor, "assunto": assunto, "body": body}


#pylint: enable=too-many-arguments
