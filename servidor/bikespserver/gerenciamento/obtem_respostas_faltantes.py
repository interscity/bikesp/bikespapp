'''
	Obtém as respotas faltando no BD
'''
import sys
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_pessoa_info

#pylint: enable=wrong-import-position


def obtem_respostas_faltantes(lista_respostas: list,
                              con: psycopg2.extensions.connection) -> list:
	'''
		Dada uma lista de respostas, devolve
		as que não estão inseridas no BD
		(Utiliza o CPF como chave da busca)
	'''
	respostas_faltantes = []
	cur = con.cursor()
	for resposta in lista_respostas:
		if get_pessoa_info(resposta["CPF"], cur) is None:
			respostas_faltantes.append(resposta)
	return respostas_faltantes
