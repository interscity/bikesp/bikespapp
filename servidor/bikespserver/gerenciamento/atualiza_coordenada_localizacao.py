'''
	Módulo responsável por gerenciar a atualização de uma coordenada
	no BD
'''

import sys
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.atualizacoes import coordenada_localizacao as att_coord_loc
from servidor.bikespserver.bd.consultas import get_coordenada_localizacao, get_pares_localizacao

from servidor.bikespserver.gerenciamento.registra_resposta import insere_pares_validos
from servidor.bikespserver.utils.coords import coordenada_valida
#pylint: enable=wrong-import-position


def atualiza_coordenada_localizacao(id_localizacao: str | int,
                                    coordenada: tuple,
                                    cur: psycopg2.extensions.cursor,
                                    force_prod=True):
	'''
		Atualiza a coordenada na tabela LOCALIZACAO
		e os trajetos/distancias na tabela PARVALIDO
	'''

	# Checa existência da localização
	coordenada_velha = get_coordenada_localizacao(id_localizacao, cur)
	if coordenada_velha is None:
		return "ID INEXISTENTE"

	# Checa validade da coordenada
	if not coordenada_valida(list(coordenada)):
		return "COORDENDA INVÁLIDA"

	# Atualiza na tabela LOCALIZACAO
	att_coord_loc(id_localizacao, coordenada, cur)

	# Para cada par valido que envolve essa localizacao
	# Chama a api do tomtom para o par
	# Atualiza tabela PARVALIDO
	lista_pares = get_pares_localizacao(id_localizacao, cur)
	for par in lista_pares:
		if int(par[0]) == int(id_localizacao):
			outra_loc = par[1]
		else:
			outra_loc = par[0]
		print("Atualizando localizações:",
		      str(id_localizacao) + ", " + str(outra_loc))
		insere_pares_validos([id_localizacao, outra_loc],
		                     cur,
		                     force_prod=force_prod)

	return "SUCESSO"
