'''
	Remove a relação entre um usuário e uma localização.
'''

import sys
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.remocoes import remove_APELIDOLOCALIZACAO, remove_PESSOAESTACAO

#pylint: enable=wrong-import-position


# pylint: disable=too-many-arguments
def desassocia_localizacao_pessoa(id_pessoa: str | int,
                                  id_localizacao: str | int | None,
                                  tipo_localizacao: str,
                                  cur: psycopg2.extensions.cursor):
	'''
		Desassocia a localização da pessoa
		(tabela APELIDOLOCALIZACAO ou PESSOAESTACAO)
	'''
	if tipo_localizacao == "qualquer":
		remove_APELIDOLOCALIZACAO(id_pessoa, id_localizacao, cur)
	else:
		remove_PESSOAESTACAO(id_pessoa, id_localizacao, cur)
