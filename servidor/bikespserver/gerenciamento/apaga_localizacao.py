'''
	Módulo responsável por apagar uma localização do BD.
'''

import sys
import logging
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.consultas import get_localizacao_info, \
  localizacao_pessoa_associada_existe, localizacao_viagem_associada_existe
from servidor.bikespserver.bd.remocoes import remove_LOCALIZACAO
#pylint: enable=wrong-import-position


def apaga_localizacao(id_localizacao: str | int,
                      cur: psycopg2.extensions.cursor) -> tuple[bool, str]:
	'''
		Apaga uma localização do BD de maneira segura, garantindo
		que não esteja associada a nenhum usuário ou viagem.
		Pares válidos associados serão apagados juntos (CASCADE).
	'''
	sucesso = True
	status = "Sucesso"

	# Verifica se localização existe
	info_loc = get_localizacao_info(id_localizacao, cur)
	if info_loc is None:
		sucesso = False
		status = "A localização informada não existe."
		return (sucesso, status)

	# Verifica se existem usuários associados à localização
	if localizacao_pessoa_associada_existe(id_localizacao, cur):
		sucesso = False
		status = "A localização está associada a usuários."
		return (sucesso, status)

	# Verifica se existem viagens associadas à localização
	if localizacao_viagem_associada_existe(id_localizacao, cur):
		sucesso = False
		status = "A localização está associada a viagens."
		return (sucesso, status)

	# Apaga localização
	sucesso = remove_LOCALIZACAO(id_localizacao, cur)

	if sucesso is False:
		status = "Não foi possível apagar localização do BD."
		logging.getLogger("bikespserver").info(
		    "Tentativa de apagar localização %s falhou.\n", str(id_localizacao))
	else:
		logging.getLogger("bikespserver").info(
		    "Localização %s apagada do banco de dados.\n", str(id_localizacao))

	return (sucesso, status)
