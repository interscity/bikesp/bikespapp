'''
	Notifica usuários de grupos de pesquisa
'''
import sys
import os
from pathlib import Path
from datetime import datetime
from dateutil.relativedelta import relativedelta
import psycopg2

#import config  # pylint: disable=unused-import

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.consultas import get_pessoas_para_notificar
from servidor.bikespserver.email.envia_email import envia_email
#pylint: enable=wrong-import-position


def pessoas_para_notificar(con: psycopg2.extensions.connection,
                           data_inicio: datetime) -> dict:
	'''
		Devolve dicionário com todas as pessoas a serem notificadas,
		divididas por grupos de pesquisa
	'''
	usuarios = {}

	cur = con.cursor()

	resultado = get_pessoas_para_notificar(data_inicio, cur)

	for pessoa in resultado:
		id_pessoa = pessoa[0]
		nome = pessoa[1]
		email = pessoa[2]
		id_grupo = pessoa[3]
		data = pessoa[4]
		remuneracao = pessoa[5]
		if id_grupo not in usuarios:
			usuarios[id_grupo] = {
			    "data": data,
			    "remuneracao": remuneracao,
			    "pessoas": []
			}
		usuarios[id_grupo]["pessoas"].append({
		    "idPessoa": id_pessoa,
		    "nome": nome,
		    "email": email
		})

	return usuarios


#pylint: disable=too-many-arguments
def notifica_pessoa(nome_pessoa: str,
                    email_pessoa: str,
                    data: datetime,
                    remuneracao: str,
                    data_fim="",
                    debug=False):
	'''
		Notifica uma pessoa que sua remuneração foi alterada
	'''
	print("Notificando " + email_pessoa)
	assunto = "[Piloto Bike SP] Alteração na remuneração"
	data_inicio = data.strftime("%d/%m/%Y")
	if data_fim == "":
		data_fim = (data + relativedelta(months=1)).strftime("%d/%m/%Y")
	else:
		data_fim = data_fim.strftime("%d/%m/%Y")
	body = (
	    f"Olá {nome_pessoa},<br><br>"
	    f"A partir do dia {data_inicio} sua remuneração no projeto piloto "
	    f"Bike SP passa a ser de {remuneracao} por km. Ela permanecerá "
	    f"com esse valor até o dia {data_fim}.<br><br>"
	    "Em caso de dúvidas acesse o nosso "
	    '<a href="https://interscity.org/bikesp/piloto/perguntas-frequentes/">FAQ</a>'
	    " ou entre em contato conosco pelo email bikesp@ime.usp.br<br><br>"
	    "Atenciosamente,<br>"
	    "Equipe Piloto Bike SP")

	user_mail = os.getenv("MAIL_USERNAME")
	senha_mail = os.getenv("MAIL_PASSWORD")
	recebedor = [email_pessoa]
	if not debug:
		envia_email(user_mail, senha_mail, recebedor, assunto, body)

	return {"recebedor": recebedor, "assunto": assunto, "body": body}


#pylint: enable=too-many-arguments
