'''
	Módulo responsável por conceder um bonus a alguem
'''

import sys
from pathlib import Path
from datetime import datetime

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import dump_bonus, get_pessoa_info_pelo_id_pessoa
from servidor.bikespserver.bd.insercoes import insere_PESSOABONUS
from servidor.bikespserver.remuneracao.insere_remuneracao import insere_remuneracao_para_enviar
from servidor.bikespserver.utils.parsers import parse_dinheiro_float
#pylint: enable=wrong-import-position


def concede_bonus(insercao_pessoa_bonus: dict,
                  cur: psycopg2.extensions.cursor) -> tuple[bool, str]:
	'''
		Concede certo bônus a um usuário
	'''
	sucesso = True
	status = "Sucesso"

	id_pessoa = insercao_pessoa_bonus["idPessoa"]
	id_bonus = insercao_pessoa_bonus["idBonus"]

	bonus_info = dump_bonus(id_bonus, cur)

	if bonus_info is None:
		status = "Bônus inexistente"
		sucesso = False

	if get_pessoa_info_pelo_id_pessoa(id_pessoa, cur) is None:
		status = "Pessoa inexistente"
		sucesso = False

	if not sucesso:
		return (sucesso, status)

	if not bonus_info[2]:
		status = "Bônus inativo"
		sucesso = False
	if bonus_info[6] is not None and bonus_info[6] < datetime.now():
		status = "Bônus já terminou"
		sucesso = False

	if sucesso:
		remuneracao = parse_dinheiro_float(bonus_info[1])
		insere_PESSOABONUS(insercao_pessoa_bonus, cur)
		insere_remuneracao_para_enviar(id_pessoa, remuneracao, cur)

	return (sucesso, status)
