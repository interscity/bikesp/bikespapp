'''
	Módulo responsável por criar um bonus no bd
'''

import sys
from pathlib import Path

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.insercoes import insere_BONUS
from servidor.bikespserver.bd.consultas import bonus_existe

#pylint: enable=wrong-import-position


def cria_bonus(insercao_bonus: dict,
               cur: psycopg2.extensions.cursor) -> tuple[bool, str, int]:
	'''
		Cria um novo bônus no bd
	'''
	id_novo_bonus = 0
	sucesso = True
	status = "Sucesso"

	if bonus_existe(insercao_bonus["nome"], cur):
		sucesso = False
		status = "Bônus já existe"
		return (sucesso, status, id_novo_bonus)

	id_novo_bonus = insere_BONUS(insercao_bonus, cur)
	return (sucesso, status, id_novo_bonus)
