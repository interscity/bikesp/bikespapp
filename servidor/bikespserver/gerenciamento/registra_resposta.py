'''
	Gerencia as etapas para registrar uma resposta do formulário no banco de dados
'''
import sys
import random

from pathlib import Path
from datetime import datetime
from typing import Optional, Any, Tuple

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.utils import parsers
from servidor.bikespserver.utils import coords
from servidor.bikespserver.utils.coords import fuso_horario
#pylint: enable=wrong-import-position


def insere_pares_validos(indices_localizacoes: list,
                         cur: psycopg2.extensions.cursor,
                         force_prod=False) -> psycopg2.extensions.cursor:
	'''
		Gerencia a inserção de pares validos no bd
	'''
	indices_localizacoes = list(dict.fromkeys(indices_localizacoes))
	for i in range(0, len(indices_localizacoes)):  # pylint: disable=consider-using-enumerate
		for j in range(i + 1, len(indices_localizacoes)):
			indice1 = indices_localizacoes[i]
			indice2 = indices_localizacoes[j]
			rota = consultas.get_rota_localizacoes(indice1, indice2, cur,
			                                       force_prod)

			if rota["distancia"] == 0:
				continue

			par_valido = {
			    "idPonto1": indice1,
			    "idPonto2": indice2,
			    "distancia": rota["distancia"],
			    "trajeto": rota["trajeto"]
			}
			cur = insercoes.insere_PARVALIDO(par_valido, cur)

	return cur


def insere_localizacao_qualquer(
        qualquer: dict,
        id_pessoa: str,
        cur: psycopg2.extensions.cursor,
        force_prod=False) -> Tuple[psycopg2.extensions.cursor, Optional[Any]]:
	'''
		Gerencia a inserção de uma localização do tipo qualquer no bd
	'''

	endereco = parsers.parse_endereco(qualquer["rua"],
	                                  qualquer["numero"],
	                                  complemento=qualquer["complemento"],
	                                  cep=qualquer["cep"],
	                                  cidade=qualquer["cidade"],
	                                  estado=qualquer["estado"],
	                                  bairro=qualquer["bairro"])
	if force_prod:
		coordenadas = coords.get_coordenada_endereco(endereco)
	else:
		coordenadas = coords.get_coordenada_aleatoria()

	print(endereco, coordenadas)
	if coordenadas == [-1, -1]:
		print("[AVISO]: Localização", qualquer, "inválida", file=sys.stderr)
		return cur, None

	localizacao = {
	    "endereco": endereco,
	    "coordenadas": coordenadas,
	    "tipoLocalizacao": "qualquer"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao, cur)

	id_localizacao = consultas.localizacao_existe(
	    parsers.parse_coordenadas(coordenadas), endereco, cur)
	if id_localizacao is None:
		return cur, None

	id_localizacao = id_localizacao[0]

	apelido = qualquer["apelido"]
	apelido_localizacao = {
	    "idPessoa": id_pessoa,
	    "idLocalizacao": id_localizacao,
	    "apelido": apelido
	}

	cur = insercoes.insere_APELIDOLOCALIZACAO(apelido_localizacao, cur)

	return cur, id_localizacao


def insere_localizacao_estacao(
        estacao: dict,
        id_pessoa: str,
        cur: psycopg2.extensions.cursor,
        force_prod=False) -> Tuple[psycopg2.extensions.cursor, Optional[Any]]:
	'''
		Gerencia a inserção de uma localização do tipo estação no bd
	'''
	if force_prod:
		coordenadas = coords.get_coordenada_estacao(estacao)
	else:
		coordenadas = coords.get_coordenada_aleatoria()
	print(estacao, coordenadas)
	if coordenadas == [-1, -1]:
		print("[AVISO]: Estação", estacao, "inválida", file=sys.stderr)
		return cur, None

	localizacao = {
	    "endereco": estacao,
	    "coordenadas": coordenadas,
	    "tipoLocalizacao": "estacao"
	}
	cur = insercoes.insere_LOCALIZACAO(localizacao, cur)

	id_localizacao = consultas.localizacao_existe(
	    parsers.parse_coordenadas(coordenadas), estacao, cur)
	if id_localizacao is None:
		return cur, None
	id_localizacao = id_localizacao[0]

	pessoa_estacao = {"idPessoa": id_pessoa, "idLocalizacao": id_localizacao}

	cur = insercoes.insere_PESSOAESTACAO(pessoa_estacao, cur)
	return cur, id_localizacao


def insere_localizacoes(resposta: dict,
                        cur: psycopg2.extensions.cursor,
                        force_prod=False) -> psycopg2.extensions.cursor:
	'''
		Gerencia a inserção de uma sequência de localizações no bd
	'''

	id_pessoa = consultas.get_pessoa_id(resposta["CPF"], cur)[0]

	localizacoes = {"qualquer": [], "estacao": []}  # type: dict

	for i in range(0, len(resposta["Endereco"]["nome"])):
		endereco = resposta["Endereco"]
		apelido = endereco["nome"][i]

		if apelido == '':
			if endereco["cep"][i] == '':
				continue
			apelido = "Sem nome"

		qualquer = {
		    "apelido": apelido,
		    "cep": endereco["cep"][i],
		    "rua": endereco["logradouro"][i],
		    "numero": endereco["numero"][i],
		    "complemento": endereco["complemento"][i],
		    "bairro": endereco["bairro"][i],
		    "cidade": endereco["cidade"][i],
		    "estado": endereco["estado"][i],
		}

		localizacoes["qualquer"].append(qualquer)

	for transporte in [
	    resposta["EstacaoOnibus"]["EstacaoOnibus"],
	    resposta["EstacaoMetro"]["EstacaoMetro"]
	]:
		for estacao in transporte:
			if str(estacao) == "":
				continue
			localizacoes["estacao"].append(estacao)

	indices_localizacoes = []
	for localizacao in localizacoes["qualquer"]:
		cur, id_localizacao = insere_localizacao_qualquer(
		    localizacao, id_pessoa, cur, force_prod)
		if id_localizacao is not None:
			indices_localizacoes.append(id_localizacao)

	for localizacao in localizacoes["estacao"]:
		cur, id_localizacao = insere_localizacao_estacao(
		    localizacao, id_pessoa, cur, force_prod)
		if id_localizacao is not None:
			indices_localizacoes.append(id_localizacao)

	cur = insere_pares_validos(indices_localizacoes, cur, force_prod)

	return cur


def converte_resposta_pessoa(resposta: dict) -> dict:
	'''
		Converte a resposta para uma estrutura
		inserível
	'''
	raca = considera_other("Raca", resposta)
	pessoa_dict = {
	    "NumBilheteUnico": resposta["NumBilheteUnico"]["NumBilheteUnico"],
	    "Nome": resposta["Nome"]["Nome"],
	    "DataNascimento": resposta["DataNascimento"]["DataNascimento"],
	    "Genero": resposta["Sexo"]["Sexo"],
	    "Raca": raca,
	    "RG": resposta["RG"]["RG"],
	    "CPF": resposta["CPF"]["CPF"],
	    "Email": resposta["Email"]["Email"],
	    "Telefone": resposta["Telefone"]["Telefone"],
	    "Rua": resposta["Endereco"]["logradouro"][0],
	    "Numero": resposta["Endereco"]["numero"][0],
	    "Complemento": resposta["Endereco"]["complemento"][0],
	    "Bairro": resposta["Endereco"]["bairro"][0],
	    "CEP": resposta["Endereco"]["cep"][0],
	    "Cidade": resposta["Endereco"]["cidade"][0],
	    "Estado": resposta["Endereco"]["estado"][0],
	}
	return pessoa_dict


def insere_pessoa(
        resposta: dict,
        cur: psycopg2.extensions.cursor) -> psycopg2.extensions.cursor:
	'''
		Gerencia a inserção de uma pessoa,
		incluindo suas informações e seu grupo
	'''
	id_grupo = decide_grupo_pesquisa(cur)

	pessoa_dict = converte_resposta_pessoa(resposta)
	cur = insercoes.insere_PESSOA(pessoa_dict, cur)
	id_pessoa = consultas.get_pessoa_id(resposta["CPF"], cur)[0]
	cur = atribui_grupo_pesquisa(id_grupo, id_pessoa, cur)
	return cur


def considera_other(chave: str, item: dict):
	'''
		Considera possibilidade de "other" em respostas.
		"other" é o campo que o usuário responde caso
		nenhuma das alternativas corresponda a sua resposta
	'''
	if chave not in item:
		return None
	if "other" not in item[chave]:
		return item[chave][chave]
	if item[chave]["other"] == "":
		return item[chave][chave]
	return item[chave]["other"]


def considera_vazio_zero(valor: Any) -> Any:
	'''
		Devolve a string 0 se o valor é igual
		a ''	
	'''
	if str(valor) == '':
		return "0"
	return valor


def converte_resposta_forms(resposta: dict) -> dict:
	'''
		Converte uma resposta para uma estrutura
		inserível
	'''
	como_conseguira_acesso = considera_other("ComoConseguiraAcesso", resposta)
	estuda_regularmente = considera_other("EstudaRegularmente", resposta)
	pq_nao_incentivo_220 = considera_other("PqNaoIncentivo220", resposta)
	pq_nao_incentivo_440 = considera_other("PqNaoIncentivo440", resposta)

	forms_dict = {
	    "DataResposta":
	        resposta["DataResposta"]["DataResposta"],
	    "CPF":
	        resposta["CPF"]["CPF"],
	    "SabeAndarDeBike":
	        resposta["SabeAndarDeBike"]["SabeAndarDeBike"],
	    "TemProblemaFisico":
	        resposta["TemProblemaFisico"]["TemProblemaFisico"],
	    "TemBikePropria":
	        resposta["TemBikePropria"]["TemBikePropria"],
	    "ComoConseguiraAcessoBike":
	        como_conseguira_acesso,
	    "AceitouTermoConsentimento":
	        resposta["AceitouTermo"]["SQ001"],
	    "AutorizouColetaInfo":
	        resposta["AutorizouColetaInfo"]["SQ001"],
	    "PossuiBilheteUnico":
	        resposta["PossuiBilheteUnico"]["PossuiBilheteUnico"],
	    "PoderaInstalarApp":
	        resposta["PoderaInstalarApp"]["PoderaInstalarApp"],
	    "QuantidadePessoasApartamento":
	        resposta["QuantidadePessoas"]["QuantidadePessoas"],
	    "RendaMensalFamiliar":
	        resposta["RendaMensalFamiliar"]["RendaMensalFamiliar"],
	    "EstudaRegularmente":
	        estuda_regularmente,
	    "GrauInstrucao":
	        resposta["GrauInstrucao"]["GrauInstrucao"],
	    "CondicaoAtividade":
	        resposta["CondicaoAtividade"]["CondicaoAtividade"],
	    "VinculoEmpregaticio":
	        resposta["VinculoEmpregaticio"]["VinculoEmpregaticio"],
	    "RendaMensalIndividual":
	        resposta["RendaMensalIndividuo"]["RendaMensalIndividuo"],
	    "QtdViagensAPe":
	        resposta["QtdViagens"]["QtdViagensAPe"],
	    "QtdViagensTransPub":
	        resposta["QtdViagens"]["QtdViagensTransPub"],
	    "QtdViagensBike":
	        resposta["QtdViagens"]["QtdViagensBike"],
	    "QtdViagensTransPubBike":
	        resposta["QtdViagens"]["QtdViagensTraPubBike"],
	    "QtdViagensOnibus":
	        resposta['QtdViagens']["QtdViagensOnibus"],
	    "QtdViagensTremMetro":
	        resposta["QtdViagens"]["QtdViagensTremMetro"],
	    "QtdViagensCombTransPub":
	        resposta["QtdViagens"]["QtdViagensCombTraPub"],
	    "QtdViagensCarroAplicativo":
	        resposta["QtdViagens"]["QtdViagensCarroApp"],
	    "QtdViagensMoto":
	        resposta["QtdViagens"]["QtdViagensMoto"],
	    "QtdViagensCarroMotoTransPub":
	        resposta["QtdViagens"]["QtdViagensCarMotoTrP"],
	    "QtdViagensOutro":
	        resposta["QtdViagens"]["QtdViagensOutro"],
	    "RealizariaTrajetos220":
	        resposta["RealizariaTrajeto220"]["RealizariaTrajeto220"],
	    "PqNaoIncentivo220":
	        pq_nao_incentivo_220,
	    "QtdViagensIncentivo220":
	        considera_vazio_zero(
	            resposta["QtdViagensIncenti220"]["QtdViagensIncenti220"]),
	    "PqNaoIncentivo440":
	        pq_nao_incentivo_440,
	    "QtdViagensIncentivo440":
	        considera_vazio_zero(
	            resposta["QtdViagensIncenti440"]["QtdViagensIncenti440"]),
	    "RealizariaTrajetos440":
	        resposta["RealizariaTrajeto440"]["RealizariaTrajeto440"],
	    "CienciaNaoGarantiaSelecao":
	        resposta["NaoGarantiaSelecao"]["SQ001"],
	    "CienciaNecessidadeInternet":
	        resposta["NecessidadeInternet"]["SQ001"],
	    "DeclaracaoRespostasVeridicas":
	        resposta["DeclaracaoVeracidade"]["SQ001"],
	}
	return forms_dict


def insere_respostaforms(
        resposta: dict,
        cur: psycopg2.extensions.cursor) -> psycopg2.extensions.cursor:
	'''
		Converte a resposta para uma estrutura inserível
		e chama a inserção
	'''
	resposta_forms_dict = converte_resposta_forms(resposta)
	cur = insercoes.insere_RESPOSTA_FORMS(resposta_forms_dict, cur)
	return cur


def insere_respostatrajeto(
        resposta: dict,
        cur: psycopg2.extensions.cursor) -> psycopg2.extensions.cursor:
	'''
		Converte a resposta dos trajetos para uma estrutura inserível
		e chama a inserção
	'''
	resposta_trajeto_dict = {}

	id_pessoa = consultas.get_pessoa_id(resposta["CPF"]["CPF"], cur)[0]

	combinacoes = [[0, 1], [0, 2], [1, 2]]

	for i in range(0, len(resposta["FrequenciaTrajeto"]["FrequenciaTrajeto"])):
		combinacao = combinacoes[i]
		frequencia = resposta["FrequenciaTrajeto"]["FrequenciaTrajeto"][i]
		duracao = resposta["DuracaoTrajeto"]["DuracaoTrajeto"][i]
		como_faz = resposta["ComoFazTrajeto"]["ComoFazTrajeto"][i]
		possibilidade_bike = resposta["PossibilidadeBike"]["PossibilidadeBike"][
		    i]
		resposta_trajeto_dict = {
		    "idPessoa": id_pessoa,
		    "apelido1": resposta["Endereco"]["nome"][combinacao[0]],
		    "apelido2": resposta["Endereco"]["nome"][combinacao[1]],
		    "frequencia": frequencia,
		    "duracao": duracao,
		    "comoFaz": como_faz,
		    "possibilidadeBike": possibilidade_bike,
		}
		cur = insercoes.insere_RESPOSTA_TRAJETO(resposta_trajeto_dict, cur)
	return cur


def insere_bilhete_unico(
        resposta: dict,
        cur: psycopg2.extensions.cursor) -> psycopg2.extensions.cursor:
	'''
		Insere o bilhete unico da resposta
		na tabela BILHETEUNICO
	'''
	dict_pessoa = converte_resposta_pessoa(resposta)
	bilhete_unico = dict_pessoa["NumBilheteUnico"]
	cpf = dict_pessoa["CPF"]

	id_pessoa = consultas.get_pessoa_id(cpf, cur)

	dict_bilhete_unico = {
	    "NumBilheteUnico": bilhete_unico,
	    "idPessoa": id_pessoa,
	    "dataInicio": datetime.now(tz=fuso_horario),
	    "dataFim": None,
	    "ativo": True,
	    "concedido": 0,
	    "aguardandoEnvio": 0,
	    "aguardandoResposta": 0
	}
	cur = insercoes.insere_BILHETEUNICO(dict_bilhete_unico, cur)
	return cur


def registra_resposta(resposta: dict,
                      con: psycopg2.extensions.connection,
                      force_prod=False) -> None:
	'''
		Gerencia a inserção de cada resposta do formulário
	'''
	cur = con.cursor()

	if len(consultas.get_grupos_pesquisa(cur)) == 0:
		print("[AVISO]: Nenhum grupo de pesquisa disponível", file=sys.stderr)
	resposta["Endereco"]["nome"].insert(0, "Residência")
	print("Inserindo ", resposta["Nome"]["Nome"])
	cur = insere_pessoa(resposta, cur)
	cur = insere_bilhete_unico(resposta, cur)
	cur = insere_respostaforms(resposta, cur)
	cur = insere_localizacoes(resposta, cur, force_prod)
	cur = insere_respostatrajeto(resposta, cur)
	con.commit()
	cur.close()


def atribui_grupo_pesquisa(
        id_grupo: str | int | None, id_pessoa: str | int,
        cur: psycopg2.extensions.cursor) -> psycopg2.extensions.cursor:
	'''
		Atribui um grupo a uma pessoa
	'''
	cur = insercoes.atualiza_grupo_pessoa(id_grupo, id_pessoa, cur)
	return cur


def decide_grupo_pesquisa(cur: psycopg2.extensions.cursor) -> int | None:
	'''
		Decide o grupo de uma pessoa aleatoriamente
	'''
	grupos_pesquisa = consultas.get_grupos_pesquisa(cur)
	id_grupos = []
	for grupo in grupos_pesquisa:
		id_grupos.append(grupo[0])

	if len(id_grupos) == 0:
		return None

	indice_aleatorio = random.randint(0, len(id_grupos) - 1)
	grupo_escolhido = grupos_pesquisa[indice_aleatorio][0]
	return grupo_escolhido
