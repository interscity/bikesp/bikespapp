'''
	Módulo responsável por atualizar endereço e apelido
	de uma localização no BD para um usuário
'''

import sys
import logging
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.atualizacoes import atualiza_endereco_PESSOA
from servidor.bikespserver.bd.consultas import get_localizacao_info, \
     apelido_localizacao_existe, get_residencia_pessoa, localizacao_existe
from servidor.bikespserver.bd.insercoes import insere_APELIDOLOCALIZACAO
from servidor.bikespserver.bd.remocoes import remove_PARVALIDO
from servidor.bikespserver.gerenciamento.adiciona_localizacao_pessoa \
     import adiciona_localizacao_pessoa
from servidor.bikespserver.gerenciamento.remove_localizacao_pessoa \
     import desassocia_localizacao_pessoa
from servidor.bikespserver.gerenciamento.apaga_localizacao import apaga_localizacao
from servidor.bikespserver.utils import parsers
from servidor.bikespserver.utils.coords import get_coordenada_aleatoria, get_coordenada_endereco
#pylint: enable=wrong-import-position


def checa_residencia_unica(id_pessoa: int | str,
                           cur: psycopg2.extensions.cursor) -> tuple[bool, str]:
	'''
		Verifica se usuário possui uma única residência
	'''
	sucesso = True
	status = "Sucesso"

	residencia_pessoa = get_residencia_pessoa(id_pessoa, cur)

	if residencia_pessoa is None or len(residencia_pessoa) == 0:
		sucesso = False
		status = "Usuário não possui residência."
	elif len(residencia_pessoa) > 1:
		sucesso = False
		status = "Usuário possui mais de uma residência cadastrada."

	return (sucesso, status)


def checa_permissao_alterar_localizacao(
        id_pessoa: int | str, id_localizacao,
        cur: psycopg2.extensions.cursor) -> dict:
	'''
		Função auxiliar para verificar se a localização pode ser
		alterada pelo usuário.
	'''
	# Verifica se a localização existe
	info_loc = get_localizacao_info(id_localizacao, cur)
	if info_loc is None:
		return {"resultado": False, "erro": "Localização não existe."}

	# Verifica se é uma estação
	if info_loc[2].lower() == "estacao":
		return {"resultado": False, "erro": "Localização é uma estação."}

	# Verifica se há vínculo entre a localização e o usuário
	apelido = apelido_localizacao_existe(str(id_pessoa), str(id_localizacao),
	                                     cur)
	if apelido is None:
		return {
		    "resultado": False,
		    "erro": "Localização não está vinculada ao usuário."
		}

	return {"resultado": True, "info_loc": info_loc, "apelido": apelido[2]}


def atualiza_localizacao_pessoa(id_pessoa: str | int,
                                id_localizacao: str | int,
                                endereco: dict | str,
                                cur: psycopg2.extensions.cursor,
                                force_prod: bool = True) -> tuple[bool, str]:
	'''
		Atualiza o endereço de uma localização para um usuário no BD.
		Mantém o apelido da localização antiga.
		Parâmetro 'endereco' pode ser tanto um dicionário com os campos adequados
		quanto a string final já formatada para o BD (use o parser).
	'''
	sucesso = True
	status = "Sucesso"

	# Verifica se há permissão para alterar localização
	permissao = checa_permissao_alterar_localizacao(id_pessoa, id_localizacao,
	                                                cur)
	if not permissao["resultado"]:
		sucesso = False
		status = "A localização informada não pode ser modificada."
		logging.getLogger("bikespserver").error(
		    "Solicitação de alteração da localização %s pelo usuário %s não pôde ser concluída (%s).",
		    str(id_localizacao), str(id_pessoa), permissao["erro"])
		return (sucesso, status)

	info_loc = permissao["info_loc"]
	apelido = permissao["apelido"]

	# Adequa formato do endereço para string
	if isinstance(endereco, dict):
		dict_localizacao = endereco
		endereco = parsers.parse_endereco(
		    rua=dict_localizacao.get("rua", ""),
		    numero=dict_localizacao.get("numero", ""),
		    complemento=dict_localizacao.get("complemento", ""),
		    bairro=dict_localizacao.get("bairro", ""),
		    cep=dict_localizacao.get("cep", ""),
		    cidade=dict_localizacao.get("cidade", ""),
		    estado=dict_localizacao.get("estado", ""))

	# Se o endereço é o mesmo, não altera
	if endereco == info_loc[0]:
		status = "Localização não alterada"
		return (sucesso, status)

	# Trata caso de uma residência
	eh_residencia = False
	if apelido == "Residência":
		eh_residencia = True

		sucesso, status = checa_residencia_unica(id_pessoa, cur)
		if not sucesso:
			logging.getLogger("bikespserver").error(
			    "Usuário %s possui inconsistência no número de residências.",
			    str(id_pessoa))
			return (sucesso, status)

	# Gerando coordenadas
	if force_prod:
		coordenada = get_coordenada_endereco(endereco)
	else:
		coordenada = get_coordenada_aleatoria()

	# Adiciona nova localização
	sucesso, status = adiciona_localizacao_pessoa(id_pessoa, None, "qualquer",
	                                              apelido, endereco, coordenada,
	                                              cur, force_prod)

	if sucesso:
		coordenada_str = parsers.parse_coordenadas(coordenada)

		id_nova_localizacao = localizacao_existe(coordenada_str, endereco, cur)
		if id_nova_localizacao is None:
			logging.getLogger("bikespserver").error(
			    "Solicitação de alteração da localização %s pelo usuário %s \
				não pôde ser concluída (ID da nova localização não encontrado).",
			    str(id_localizacao), str(id_pessoa))
			sucesso = False
			status = "Erro ao atualizar localização."
			return (sucesso, status)

		id_nova_localizacao = id_nova_localizacao[0]

		# Desassocia localização antiga
		desassocia_localizacao_pessoa(id_pessoa, id_localizacao, "qualquer",
		                              cur)

		# Remove par válido entre nova e antiga localização
		remove_PARVALIDO(id_localizacao, id_nova_localizacao, cur)

		# Atualiza residência na tabela PESSOA
		if eh_residencia:
			atualiza_endereco_PESSOA(id_pessoa, endereco, cur)

		# Apaga a localização se não houverem associações
		apaga_localizacao(id_localizacao, cur)

		logging.getLogger("bikespserver").info(
		    "Usuário %s alterou localização %s para %s.", str(id_pessoa),
		    str(id_localizacao), str(id_nova_localizacao))

	return (sucesso, status)


def atualiza_apelido_localizacao_pessoa(
        id_pessoa: str | int, id_localizacao: str | int, novo_apelido: str,
        cur: psycopg2.extensions.cursor) -> tuple[bool, str]:
	'''
	   Atualiza o apelido de uma localização para um usuário.
	'''
	sucesso = True
	status = "Sucesso"

	# Verifica se há permissão para alterar localização
	permissao = checa_permissao_alterar_localizacao(id_pessoa, id_localizacao,
	                                                cur)
	if not permissao["resultado"]:
		sucesso = False
		status = "A localização informada não pode ser modificada."
		logging.getLogger("bikespserver").error(
		    "Solicitação de alteração de apelido da localização %s \
			para usuário %s não pôde ser concluída (%s).", str(id_localizacao),
		    str(id_pessoa), permissao["erro"])
		return (sucesso, status)

	apelido_antigo = permissao["apelido"]

	# Se apelido é o mesmo, não altera
	if apelido_antigo == novo_apelido:
		return (sucesso, status)

	# Verifica se é uma residência
	if apelido_antigo == "Residência" or novo_apelido == "Residência":
		sucesso = False
		status = "Não é possível alterar o apelido de uma residência."
		logging.getLogger("bikespserver").error(
		    "Solicitação de alteração de apelido da localização %s para usuário %s \
			não pôde ser concluída (Não é possível alterar o apelido de uma residência).",
		    str(id_localizacao), str(id_pessoa))
		return (sucesso, status)

	# Atualiza apelido
	apelido_localizacao = {
	    'idPessoa': id_pessoa,
	    'idLocalizacao': id_localizacao,
	    'apelido': novo_apelido
	}
	insere_APELIDOLOCALIZACAO(apelido_localizacao, cur)

	logging.getLogger("bikespserver").info(
	    "Apelido de localização %s de usuário %s alterado.",
	    str(id_localizacao), str(id_pessoa))

	return (sucesso, status)
