"""
	Módulo que contém a classe DeslocamentoAtomico
"""

import sys
from pathlib import Path

sys.path.append(
    str(Path(__file__).absolute().parent.parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.utils.coords import distancia_haversine
from servidor.bikespserver.utils.parsers import parse_data_js

#pylint: enable=wrong-import-position


class DeslocamentoAtomico:  # pylint: disable=too-many-instance-attributes,too-few-public-methods
	"""
		Classe responsável por reunir as informações relacionadas a um deslocamento
	atômico, ou seja, aquele correspondente ao ir de uma amostra à próxima.
	"""

	def __init__(self, amostra1, amostra2):
		self.delta_s = distancia_haversine(amostra1['Posicao'],
		                                   amostra2['Posicao'])

		try:
			self.delta_t = amostra2["Data"] - amostra1["Data"]
		except TypeError as _:

			secs1 = parse_data_js(amostra1["Data"])
			secs2 = parse_data_js(amostra2["Data"])
			self.delta_t = secs2 - secs1

		if self.delta_t == 0:
			self.delta_t = 1

		self.vel = self.delta_s / self.delta_t

		if self.vel < 0:
			raise ValueError(
			    "DeslocamentoAtomico aplicado para pontos em ordem cronológica errada"
			)
