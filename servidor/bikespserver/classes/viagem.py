"""
	Módulo que contém a classe Viagem
"""
import sys
import math
import json
import re

from pathlib import Path

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.classes.viagem_bd import ViagemBD

from servidor.bikespserver.utils.parsers import parse_data_js

from servidor.bikespserver.bd.consultas import viagem_existe
from servidor.bikespserver.bd.consultas import par_valido_existe

from servidor.bikespserver.bd.insercoes import insere_VIAGEM

from servidor.bikespserver.bd.atualizacoes import atualiza_viagem_existente

#pylint: enable=wrong-import-position


class Viagem:  # pylint: disable=too-many-instance-attributes
	"""
		Representação de uma viagem a ser tratada pelo servidor
	"""

	def __init__(self):
		self.data_inicio = None
		self.duracao = None
		self.apelido_origem = None
		self.apelido_destino = None
		self.trajeto = None
		self.activity_recognition_trip = None
		self.id_usuario = None

		self.status = "Aprovado"
		self.motivo_reprovacao = "APROVADO"

		self.id_origem = None
		self.idx_origem = None
		self.id_destino = None
		self.idx_destino = None
		self.indice_destino_trajeto = None

		self.trajeto_ideal = []

		self.remuneracao = 0
		self.deslocamento = 0

		self.metadados = {}
		self.pausas = []

		# Atribuido caso a viagem já tenha sido salva no BD
		self.id_viagem = None

	def carrega_estado_inicial(self, json_input: dict):
		"""
			Dado um json com os valores dos atributos iniciais
		da instância, atualiza os valores desta instância.
		"""
		self.data_inicio = json_input['dataInicio']
		self.duracao = json_input['duracao']
		self.trajeto = json_input['trajeto']

		if "idPessoa" in json_input:
			self.id_usuario = json_input["idPessoa"]
		if "origem" in json_input:
			self.id_origem = json_input["origem"]
		if "destino" in json_input:
			self.id_destino = json_input["destino"]
		if 'activityRecognitionTrip' in json_input:
			self.activity_recognition_trip = json_input[
			    'activityRecognitionTrip']
		else:
			self.activity_recognition_trip = []

		if 'metadados' in json_input and json_input['metadados'] is not None:
			self.metadados = json_input['metadados']
		else:
			self.metadados = {}

		if "pausas" in json_input:
			pausas = json_input["pausas"]

			texto_pausas = re.sub(r"'", '"', str(pausas))
			self.metadados["pausas"] = pausas
			self.pausas = json.loads(texto_pausas)
			self.pausas.sort(key=lambda x: x["DataInicio"])

	@staticmethod
	def from_json(json_input: dict):
		"""
			Dado um json vindo da API, devolve um objeto
		da classe Viagem
		"""

		nova_viagem = Viagem()
		nova_viagem.carrega_estado_inicial(json_input)

		return nova_viagem

	def associa_usuario(self, id_usuario: int | str):
		"""
			Associa esta viagem ao id do usuário que a criou
		"""

		self.id_usuario = id_usuario

	def reprova(self, motivo: str, force: bool = False):
		"""
			Reprova a viagem pelo motivo dado. Se o parâmetro force
		for especificado, altera o motivo da reprovação anterior.
		"""

		if force or self.status != "Reprovado":
			self.status = "Reprovado"
			self.motivo_reprovacao = motivo
			self.remuneracao = 0.00

	def calcula_remuneracao(self, remuneracao_km: float):
		"""
			Calcula remuneração final para uma viagem válida dado o 
		deslocamento de um trajeto e uma remuneração por km do grupo de pesquisa
		do usuário que a realizou
		"""
		if self.status != "Reprovado":
			deslocamento_contado = self.deslocamento
			if deslocamento_contado > 8000:
				deslocamento_contado = 8000
			elif deslocamento_contado < 1000:
				self.reprova("DESLOCAMENTO PEQUENO")
				deslocamento_contado = 0
			self.remuneracao = remuneracao_km * deslocamento_contado / 1000

	def checa_num_max_pausas(self, num_max_pausas: int):
		"""
			Reprova essa viagem se ela tiver mais que num_max_pausas pausas
		"""
		if len(self.pausas) > num_max_pausas:
			self.reprova("VIAGEM_INVALIDA")

	def detecta_emulador(self):
		"""
			Reprova esta viagem se o campo de metadados tiver chave 'emulador: true'
		"""
		if self.metadados in ("{}", {}):
			return

		if "emu" in self.metadados:
			if self.metadados["emu"] is True:
				self.reprova("VIAGEM_INVALIDA")

	def associa_origem_destino(  # pylint: disable=too-many-arguments
	        self, id_origem: int | str | None, idx_origem: int,
	        id_destino: int | str | None, idx_destino: int,
	        cur: psycopg2.extensions.cursor):
		"""
			Dados os ids dos pontos de origem e destino da viagem, 
		atualiza o estado interno da viagem.
		"""

		self.id_origem = id_origem
		self.idx_origem = idx_origem

		self.id_destino = id_destino
		self.idx_destino = idx_destino

		par_invalido, motivo = par_origem_destino_invalidos(
		    id_origem, id_destino)
		if par_invalido:
			self.reprova(motivo)
		else:
			par_valido = par_valido_existe(id_origem, id_destino, cur)
			if par_valido is not None:
				self.deslocamento = par_valido[2]
				self.trajeto_ideal = par_valido[3]

	def checa_num_min_pontos(self, num_pontos_km) -> bool:
		"""
			Checa se a instância do objeto viagem tem um número mínimo
		de pontos por km, e a reprova se esse número não for
		atingido e se a viagem já não estiver reprovada.

			Devolve um booleano que simboliza a satisfação da condição
		"""

		if len(self.trajeto) < math.floor(
		    (self.deslocamento / 1000) * num_pontos_km):
			self.reprova("POUCOS_PONTOS")
			return False
		return True

	def checa_num_min_activities(self, num_pontos_km, trip=None) -> bool:
		"""
			Checa se o trip passado tem um número de amostras por Km 
		ou, caso passada um trip vazio, faz o mesmo teste sobre a trip
		da instância do objeto viagem. Devolve um booleano que 
		simboliza se o trip possuí mais do que o mínimo de pontos
		"""

		if trip is None:
			trip = self.activity_recognition_trip

		if trip is None or len(trip) == 0:
			return False

		return len(trip) >= math.floor(
		    (self.deslocamento / 1000) * num_pontos_km)

	def extrai_nucleo_trajeto_utilizavel(self, precisao_minima, num_pontos_km,
	                                     max_sem_amostra, deltat_outliers):
		"""
			Devolve o 'núcleo' do trajeto sem amostras imprecisas e com
		amostras ordenadas cronologicamente.
		"""

		if self.status == "Reprovado":
			return []

		try:
			trajeto = self.get_nucleo_trajeto()

			trajeto_novo = list(
			    filter(lambda x: x["Precisao"] <= precisao_minima, trajeto))
			if trajeto_novo[-1] != trajeto[-1]:
				trajeto_novo.append(trajeto[-1])
			if trajeto_novo[0] != trajeto[0]:
				trajeto_novo = [trajeto[0]] + trajeto_novo

			trajeto_novo = self._remove_amostras_absurdas(
			    trajeto_novo, deltat_outliers)
			trajeto_novo.sort(key=lambda x: x["Data"])
			trajeto_novo = self._insere_pausas_em_trajeto(trajeto_novo)
			i = 0
			while i < len(trajeto_novo) - 1:
				proximo = trajeto_novo[i + 1]
				if "DataInicio" in proximo.keys():
					i += 2
					continue
				if "DataInicio" in trajeto_novo[i].keys():
					i += 1
					continue
				ts_prox = parse_data_js(proximo["Data"])
				deltat_agora = abs(ts_prox -
				                   parse_data_js(trajeto_novo[i]["Data"]))
				# print("i:", i, ", deltat:", deltat_agora)
				if deltat_agora > max_sem_amostra:
					# print("Muito tempo sem amostra")
					self.reprova("TEMPO_SEM_AMOSTRA")
					return []
				i += 1

			if len(trajeto_novo) < math.floor(
			    (self.deslocamento / 1000) * num_pontos_km):
				# print("Muito pequena sem pontos imprecisos")
				self.reprova("POUCOS_PONTOS")
				return []

			return trajeto_novo

		except TypeError as erro:
			print("Erro tratanto trajeto de uma viagem:", erro)
			self.reprova("POUCOS_PONTOS")
			return []

	def _remove_amostras_absurdas(self, trajeto, deltat_outliers):
		"""
			Remove amostras com oriundas de momentos muito distantes da viagem,
		comumente causadas por problemas de caching de posições do gps.
		"""
		i = 0
		while i < len(trajeto) - 1:
			ts_prox = parse_data_js(trajeto[i + 1]["Data"])
			deltat_agora = abs(ts_prox - parse_data_js(trajeto[i]["Data"]))
			# print("i:", i, ", deltat:", deltat_agora)
			if (deltat_agora > deltat_outliers and i >= len(trajeto) - 2):
				trajeto = trajeto[:-1]
				print("Tirando um ponto com timestamp absurdo")
			elif (deltat_agora > deltat_outliers and i == 0):
				trajeto = trajeto[1:]
				i -= 1
				print("Tirando um ponto com timestamp absurdo")
			elif (deltat_agora > deltat_outliers and
			      abs(parse_data_js(trajeto[i + 2]["Data"]) - ts_prox)
			      > deltat_outliers):
				trajeto = trajeto[:i + 1] + trajeto[i + 2:]
				print("Tirando um ponto com timestamp absurdo")
			i += 1

		return trajeto

	def _insere_pausas_em_trajeto(self, trajeto):
		"""
		Devolve um trajeto com marcações em suas pausas.
		"""
		for pausa in self.pausas:
			pausa["DataInicio"] = parse_data_js(pausa["DataInicio"])
			if "DataFim" in pausa.keys():
				pausa["DataFim"] = parse_data_js(pausa["DataFim"])

		trajeto.sort(key=lambda x: x["Data"])

		id_pausas, id_trajeto = (0, 0)
		novo_trajeto = []

		while id_trajeto < len(trajeto) and id_pausas < len(self.pausas):
			t_trajeto = parse_data_js(trajeto[id_trajeto]["Data"])
			t_pausa = self.pausas[id_pausas]["DataInicio"]
			if t_trajeto > t_pausa:
				if id_trajeto > 0:
					novo_trajeto.append(self.pausas[id_pausas])
				id_pausas += 1
			else:
				novo_trajeto.append(trajeto[id_trajeto])
				id_trajeto += 1

		while id_trajeto < len(trajeto):
			novo_trajeto.append(trajeto[id_trajeto])
			id_trajeto += 1

		return novo_trajeto

	def extrai_nucleo_activities_utilizavel(self, confianca_minima):
		"""
			Devolve a porção da lista de activities que possuem uma confianca minima
		"""

		activities = self.activity_recognition_trip

		if activities is None:
			return []

		try:
			activities_novo = list(
			    filter(
			        lambda x: x["confidence"] >= confianca_minima or x["type"]
			        == "DESCONHECIDO", activities))

			return activities_novo

		except TypeError as erro:
			print("Erro tratanto activities de uma viagem:", erro)
			return []

	def get_nucleo_trajeto(self):
		"""
			Devolve o 'núcleo' do trajeto - a subseção relativa ao deslocamento
		entre a origem e o primeiro ponto dentro do alcance do destino.
		"""

		return self.trajeto[self.idx_origem:self.idx_destino + 1]

	def salvar_no_bd(self, cur: psycopg2.extensions.cursor) -> int | None:
		"""
		Cria entrada relacionada à Viagem no BD e, caso ela seja válida,
		devolve o id do objeto criado.
		"""

		viagem_a_salvar = ViagemBD.de_viagem(self)
		if self.id_viagem is None:
			cur = insere_VIAGEM(viagem_a_salvar, cur)
		else:
			atualiza_viagem_existente(self.id_viagem, viagem_a_salvar, cur)

		resultado = viagem_existe(self.id_usuario, viagem_a_salvar.data, cur)
		if resultado is not None:
			self.id_viagem = resultado[0]
			return resultado[0]

		return resultado


def par_origem_destino_invalidos(
        id_origem: int | str | None,
        id_destino: int | str | None) -> tuple[bool, str]:
	"""
	Função que checa se um par (id_origem, id_destino) é inválido, e dá o motivo 
	"""

	invalidos = True

	if id_origem is None and id_destino is not None:
		motivo_status = "ORIGEM_DESCONHECIDA"
	elif id_destino is None and id_origem is not None:
		motivo_status = "DESTINO_DESCONHECIDO"
	elif id_destino is None and id_origem is None:
		motivo_status = "ORIGEM_DESTINO_DESCONHECIDOS"
	elif id_origem == id_destino:
		motivo_status = "ORIGEM_IGUAL_DESTINO"
	else:
		invalidos = False
		motivo_status = "APROVADO"

	return (invalidos, motivo_status)
