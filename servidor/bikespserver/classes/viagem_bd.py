"""
	Módulo que contém a classe ViagemBD
"""

import json

from datetime import datetime

import pytz

fuso_horario = pytz.timezone('America/Sao_Paulo')


class ViagemBD:  # pylint: disable=too-many-instance-attributes,too-few-public-methods
	"""
		Representação de uma viagem a ser salva no BD
	"""

	def __init__(  # pylint: disable=too-many-arguments
	        self,
	        id_usuario,
	        viagem_req,
	        deslocamento,
	        id_origem,
	        id_destino,
	        status,
	        remuneracao,
	        motivo_status="APROVADO",
	        activity_recognition_trip=None,
	        metadados=None):

		#pylint: disable=invalid-name

		#idPessoa integer NOT NULL,
		self.idPessoa = id_usuario

		#data date NOT NULL, # como?
		self.data = str(
		    datetime.fromtimestamp(viagem_req.data_inicio, tz=fuso_horario))
		#deslocamento double precision NOT NULL CHECK (deslocamento > 0),
		self.deslocamento = deslocamento

		# idOrigem integer NOT NULL,
		# idDestino integer NOT NULL,
		self.idOrigem = id_origem
		self.idDestino = id_destino
		self.motivoStatus = motivo_status

		# activities recognition trip
		if activity_recognition_trip is None:
			self.activityRecognitionTrip = json.dumps([])
		else:
			self.activityRecognitionTrip = json.dumps(activity_recognition_trip)

		#pylint: enable=invalid-name

		#status varchar(30) NOT NULL CHECK (status in ('emAnalise', 'reprovado','aprovado')),
		self.status = status

		#remuneracao money,
		self.remuneracao = remuneracao

		# trajeto jsonb NOT NULL,
		self.trajeto = json.dumps(viagem_req.trajeto)

		# 'metadados' pode ser nulo,
		if metadados is None:
			self.metadados = "{}"
		else:
			self.metadados = json.dumps(metadados)

	def get_duracao_viagem(self):
		'''
			Por meio da data da viagem e 
			de seu trajeto, é possível devolver a duração da viagem
		'''

		def get_data_formatada(data: str):
			rev_data = data[::-1]
			i = len(data) - rev_data.index(':') - 1
			data_formatada = data[:i] + data[i + 1:]
			return data_formatada

		trajeto_json = json.loads(self.trajeto)
		primeira_data = get_data_formatada(self.data)
		primeira_data = datetime.strptime(primeira_data, "%Y-%m-%d %H:%M:%S%z")

		ultima_data = get_data_formatada(trajeto_json[-1]["Data"])

		ultima_data = datetime.strptime(ultima_data, "%Y-%m-%d %H:%M:%S %Z%z")

		duracao = datetime.timestamp(ultima_data) - datetime.timestamp(
		    primeira_data)
		return duracao

	@staticmethod
	def de_viagem(viagem):
		"""
			Cria um novo objeto ViagemBD a partir de uma Viagem
		"""

		return ViagemBD(viagem.id_usuario, viagem, viagem.deslocamento,
		                viagem.id_origem, viagem.id_destino, viagem.status,
		                viagem.remuneracao, viagem.motivo_reprovacao,
		                viagem.activity_recognition_trip, viagem.metadados)
