'''
	Decorator responsável por checar as chaves e o tamanho de uma request 
'''
import logging
import sys
import json
from pathlib import Path

from flask import request
from werkzeug.exceptions import BadRequest

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.utils.validadores import busca_excedente_campos_form, valida_chaves_form

#pylint: enable=wrong-import-position


# pylint: disable=too-many-arguments
def valida_request(chaves_obrigatorias: list,
                   tamanho_maximo_request: int,
                   methods=None,
                   tipo="form",
                   chaves_json=None,
                   chaves_aninhadas_obrigatorias=None):
	'''
		Esse decorator valida o formato de uma request.
		chaves_obrigatorias: qual chave a request deve ter
		tamanho_maximo_request: qual o tamanho maximo de cada campo da request
		methods: quais métodos devem passar pela verificação. Default = ["GET","POST"]
		tipo: tipo da request. form,json ou args. Default = form
		chaves_json: para o tipo json, quais chaves tem atributos aninhados
		chaves_aninhadas_obrigatorias: para cada chaves_json, quais atributos cada uma deve ter
	'''
	logger = logging.getLogger("bikespserver")
	if methods is None:
		methods = ["GET", "POST"]

	def decorated_function(function):

		# pylint: disable=too-many-return-statements
		def wrapped(*args, **kwargs):
			if ("GET" not in methods and
			    request.method == "GET") or ("POST" not in methods and
			                                 request.method == "POST"):
				return function(*args, **kwargs)

			try:
				if tipo == "form":
					requisicao = request.form
				elif tipo == "args":
					requisicao = request.args
				elif tipo == "json":
					requisicao = request.json
				else:
					requisicao = None
			except (AttributeError, BadRequest) as err:
				logger.error("Erro ao acessar dados da request: %s", str(err))

				logger.debug("\nRequest:: %s - %s\n%s\n", request.method,
				             request.url, request.get_data(as_text=True))

				return ({
				    'estado': "Erro",
				    'erro': "Formato de requisição inválido"
				}, 400)

			if requisicao is None:
				logger.error("Tipo de requisição inválido %s", tipo)
				logger.debug("\nRequest:: %s - %s\n%s\n", request.method,
				             request.url, request.get_data(as_text=True))
				return ({
				    'estado': "Erro",
				    'erro': "Formato de requisição inválido"
				}, 400)

			if not valida_chaves_form(requisicao, chaves_obrigatorias):
				logger.warning("Request necessitava de chaves %s",
				               chaves_obrigatorias)
				logger.debug("\nRequest:: %s - %s\n%s\n", request.method,
				             request.url, request.get_data(as_text=True))
				return ({'estado': "Erro", 'erro': "Requisição inválida"}, 400)

			chave, tamanho = busca_excedente_campos_form(
			    requisicao, tamanho_maximo_request)
			if chave is not None:
				logger.debug(
				    "Campo %s de tamanho %d excedeu o tamanho máximo (%d)",
				    chave, tamanho, tamanho_maximo_request)
				return ({'estado': "Erro", 'erro': 'Dados em excesso'}, 413)

			if tipo == "json":
				if chaves_json is None or chaves_aninhadas_obrigatorias is None:
					logger.error(
					    "Chaves json e chaves aninhadas obrigatórias não foram passadas"
					)
					return ({
					    'estado': "Erro",
					    'erro': "Erro interno de validação de requisição"
					}, 500)

				for i, chave_json in enumerate(chaves_json):
					chave_convertida = requisicao[chave_json]
					if isinstance(chave_convertida, str):
						chave_convertida = json.loads(chave_convertida)
					if not valida_chaves_form(chave_convertida,
					                          chaves_aninhadas_obrigatorias[i]):
						logger.warning(
						    "Request necessitava de chaves '%s.[%s]'",
						    chave_json,
						    ",".join(chaves_aninhadas_obrigatorias[i]))
						logger.debug("\nRequest:: %s - %s\n%s\n",
						             request.method, request.url,
						             request.get_data(as_text=True))
						return ({
						    'estado': "Erro",
						    'erro': "Dados em falta"
						}, 400)

			return function(*args, **kwargs)

		wrapped.__name__ = function.__name__
		return wrapped

	return decorated_function
