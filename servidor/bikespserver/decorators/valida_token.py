'''
	Decorator responsável por validar o token do usuário
	e devolver as informações básicas dele e um cursor do bd 
'''
import logging
import sys
from pathlib import Path
from functools import wraps

from flask import request, redirect, make_response

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_info
from servidor.bikespserver.autenticacao.gerencia_sessao import token_valido

#pylint: enable=wrong-import-position


# pylint: disable=too-many-arguments
def valida_token(function):
	'''
		Esse decorator valida o token de um usuário
		e devolve informações sobre ele e um cursor do bd
	'''

	@wraps(function)
	def wrapped(*args, **kwargs):
		con = conecta()
		cur = con.cursor()

		if len(request.form.keys()) != 0:
			usuario = request.form['user']
			token = request.form['token']
		elif len(request.args.keys()) != 0:
			usuario = request.args["user"]
			token = request.args['token']
		else:
			usuario = request.json["user"]
			token = request.json['token']

		info_pessoa = get_pessoa_info(usuario, cur)

		if info_pessoa is None or not token_valido(info_pessoa[0], token, cur):
			if info_pessoa is None:
				logging.getLogger("bikespserver").debug(
				    "Tentativa de autenticação com usuário não cadastrado %s",
				    usuario)
			return ({'estado': "Erro", 'erro': "Erro de Autenticacao"}, 401)

		kwargs['con'] = con
		kwargs['cur'] = cur
		kwargs['info_pessoa'] = info_pessoa
		return function(*args, **kwargs)

	wrapped.__name__ = function.__name__
	return wrapped


def valida_token_admin(function):
	'''
		Esse decorator valida o token de um usuário
		No caso de sucesso, devolve informações sobre ele
		e um cursor do bd, caso contrário redireciona à
		tela de login do painel de administração
	'''

	@wraps(function)
	def wrapped(*args, **kwargs):
		con = conecta()
		cur = con.cursor()

		token = request.cookies.get('token', '')
		usuario = request.cookies.get('user', '')

		info_pessoa = get_pessoa_info(usuario, cur)

		if info_pessoa is None or not token_valido(info_pessoa[0], token, cur):
			if info_pessoa is None:
				logging.getLogger("bikespserver").debug(
				    "Tentativa de autenticação com usuário não cadastrado %s",
				    usuario)

			response = make_response(redirect('/admin/signin'))
			response.set_cookie('token', '', expires=0)
			response.set_cookie('usuario', '', expires=0)
			return response

		kwargs['con'] = con
		kwargs['cur'] = cur
		kwargs['info_pessoa'] = info_pessoa
		return function(*args, **kwargs)

	wrapped.__name__ = function.__name__
	return wrapped
