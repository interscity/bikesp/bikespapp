'''
	Gerenciador de sessões de usuário.
'''
import sys
from datetime import timedelta
from datetime import datetime
from pathlib import Path
import psycopg2

from flask_jwt_extended import create_access_token

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd import insercoes
from servidor.bikespserver.bd import remocoes
from servidor.bikespserver.bd import consultas
from servidor.bikespserver.utils.coords import fuso_horario
#pylint: enable=wrong-import-position


def token_valido(id_pessoa: int | str, token: str,
                 cur: psycopg2.extensions.cursor) -> bool:
	'''
		Checa validade de certo token para determinado idPessoa
	'''
	resultado = consultas.usuario_token_existe(str(id_pessoa), token, cur)
	if resultado is None:
		return False
	data_validade = fuso_horario.localize(resultado[2])
	agora = datetime.now(tz=fuso_horario)
	if agora <= data_validade:
		return True
	return False


def checa_sessao_existente(id_pessoa: str | int):
	"""
		Dado o identificador de uma pessoa, checa se já
	existe alguma sessão existente e válida aberta a ela.
	Caso encontre alguma sessão expirada, a remove do BD.
	"""

	con = conecta()
	cur = con.cursor()
	res = consultas.get_sessoes_usuario(id_pessoa, cur)

	if res is None or len(res) < 1:
		cur.close()
		con.close()
		return False

	res = res[0]

	if not token_valido(id_pessoa, res[1], cur):
		remove_sessao(id_pessoa, {'token': res[1]})
		cur.close()
		con.close()
		return False

	cur.close()
	con.close()
	return True


def gera_sessao(identificacao: str,
                validade_dias=365,
                validade_horas=0,
                validade_minutos=0) -> dict:
	'''
		Dado uma identificação, devolve uma sessão
		com token e dataValidade
	'''
	token = _cria_token_acesso(identificacao)
	data_validade = _cria_data_validade(validade_dias, validade_horas,
	                                    validade_minutos)
	sessao = {'token': token, 'dataValidade': data_validade}
	return sessao


def _cria_token_acesso(identificacao: str) -> dict:
	'''
		Utiliza o módulo create_access_token do
		flask_jwt_extended para criar um token de acesso
	'''
	json = {"id": identificacao}
	token = create_access_token(identity=json)
	return token


def _cria_data_validade(dias: int, horas: int, minutos: int) -> str:
	'''
		Devolve uma data de validade a partir da data atual
	'''
	agora = datetime.now(tz=fuso_horario)
	validade = agora + timedelta(days=dias, hours=horas, minutes=minutos)
	return validade.isoformat()


def salva_sessao(id_pessoa: int | str, json_sessao: dict) -> None:
	'''
		Salva determinada sessão de um usuário
	'''
	con = conecta()
	cur = con.cursor()
	sessao = {
	    "idPessoa": str(id_pessoa),
	    "token": json_sessao["token"],
	    "dataValidade": json_sessao["dataValidade"]
	}
	cur = insercoes.insere_SESSAO(sessao, cur)
	con.commit()
	cur.close()
	con.close()


def remove_sessao(id_pessoa: int | str, json_sessao: dict) -> None:
	'''
		Remove determinada sessão de um usuário
	'''
	con = conecta()
	cur = con.cursor()
	cur = remocoes.remove_SESSAO(str(id_pessoa), str(json_sessao["token"]), cur)
	con.commit()
	cur.close()
	con.close()


def reseta_sessoes(id_pessoa: int | str,
                   cur: psycopg2.extensions.cursor) -> None:
	'''
		Remove todas as sessão de um usuário
	'''
	id_pessoa = str(id_pessoa)
	sessoes = consultas.get_sessoes_usuario(id_pessoa, cur)
	for sessao in sessoes:
		token = sessao[1]
		cur = remocoes.remove_SESSAO(id_pessoa, token, cur)
