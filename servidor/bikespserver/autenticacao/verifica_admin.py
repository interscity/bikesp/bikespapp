'''
	Módulo responsável verificar status de admin para signin no painel
'''

import sys
from pathlib import Path
from flask import request

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import get_pessoa_por_email
from servidor.bikespserver.bd.consultas import pessoa_eh_admin
from servidor.bikespserver.utils.parsers import parse_email
#pylint: enable=wrong-import-position


def verifica_admin():
	"""
		Função que liga o endpoint admin/signin à consulta de status admin no banco de dados
	"""
	con = conecta()
	cur = con.cursor()

	pessoa = get_pessoa_por_email(parse_email(request.form['email']), cur)
	if pessoa is None:
		cur.close()
		con.close()
		return False, None

	id_pessoa = pessoa[0]
	response = pessoa_eh_admin(id_pessoa, cur)
	cur.close()
	con.close()
	return (response, id_pessoa)
