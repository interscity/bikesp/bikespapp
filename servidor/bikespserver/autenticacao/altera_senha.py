'''
	Módulo responsável por alterar a senha de determinado usuário
'''

import sys
import os
from pathlib import Path
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.insercoes import insere_USUARIO
from servidor.bikespserver.autenticacao.gerencia_sessao import reseta_sessoes
from servidor.bikespserver.utils.seguranca import encripta
#pylint: enable=wrong-import-position


def altera_senha(
    id_pessoa: int | str,
    nova_senha: str,
    cur: psycopg2.extensions.cursor,
    chave_cripto=os.getenv("ENCRIPT_PK")) -> None:
	'''
		Altera a senha e remove todas as sessões existentes
	'''
	# Remove todas as seções existentes da pessoa
	reseta_sessoes(id_pessoa, cur)

	# Encripta a senha da mesma forma que o aplicativo faria
	if chave_cripto != "":
		senha = encripta(nova_senha, chave_cripto)
	else:
		senha = nova_senha

	usuario = {"idPessoa": id_pessoa, "senha": senha}

	# Update tabela USUARIO com a nova senha
	insere_USUARIO(usuario, cur)
