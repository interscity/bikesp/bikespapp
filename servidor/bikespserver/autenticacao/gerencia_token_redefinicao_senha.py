'''
	Módulo responsável por gerenciar os tokens de redefinição de senha
'''
import sys
from pathlib import Path
from datetime import datetime
import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.insercoes import insere_TOKENREDEFINICAOSENHA
from servidor.bikespserver.bd.remocoes import remove_TOKENREDEFINICAOSENHA
from servidor.bikespserver.bd.consultas import token_redefinicao_existe
from servidor.bikespserver.autenticacao.gerencia_sessao import gera_sessao
from servidor.bikespserver.autenticacao.gerencia_sessao import fuso_horario
#pylint: enable=wrong-import-position


def gera_token_redefinicao(identificacao: str) -> dict:
	'''
		Gera o token de redefinição (com data de validade)
	'''
	return gera_sessao(identificacao, validade_dias=0, validade_horas=1)


def token_redefinicao_valido(token: str,
                             cur: psycopg2.extensions.cursor) -> bool | int:
	'''
		Devolve false caso token não seja válido e o id_pessoa caso contrário
	'''
	resultado = token_redefinicao_existe(token, cur)
	if resultado is None:
		return False

	data_validade = fuso_horario.localize(resultado[1])
	agora = datetime.now(tz=fuso_horario)
	if agora <= data_validade:
		return resultado[0]
	return False


def salva_token(id_pessoa: str | int, token: dict,
                cur: psycopg2.extensions.cursor) -> None:
	'''
		Salva o token de redefinição de senha no bd
	'''
	token_obj = {
	    "idPessoa": id_pessoa,
	    "token": token["token"],
	    "dataValidade": token["dataValidade"]
	}

	insere_TOKENREDEFINICAOSENHA(token_obj, cur)


def remove_token_redefinicao(id_pessoa,
                             cur: psycopg2.extensions.cursor) -> None:
	'''
		Remove o token de redefinição de senha
		de certo usuário
	'''
	remove_TOKENREDEFINICAOSENHA(id_pessoa, cur)
