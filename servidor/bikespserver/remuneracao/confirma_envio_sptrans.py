'''
	Módulo recebe um bilheteUnico e um valor
	e adiciona essa entrada na tabela HISTORICOENVIOSPTRANS
	Além disso, move esse valor do atributo aguardandoEnvio para concedido
'''

import sys
from pathlib import Path
from datetime import datetime

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import bilhete_unico_existe
from servidor.bikespserver.bd.insercoes import insere_HISTORICOENVIOSPTRANS
from servidor.bikespserver.remuneracao.insere_remuneracao \
 import transporta_aguardando_envio_concedido
#pylint: enable=wrong-import-position


def confirma_envio_sptrans(
        bilhete_unico: str, valor: float, cur: psycopg2.extensions.cursor
) -> tuple[bool, str, tuple[float, float]]:
	'''
		Confirma um envio de crédito para a SPTrans
	'''
	sucesso = True
	status = "SUCESSO"

	if valor <= 0:
		return (False, "VALOR <= 0", (0, 0))

	if bilhete_unico_existe(bilhete_unico, cur) is None:
		return (False, "BILHETE UNICO INEXISTENTE", (0, 0))

	[concedido, aguardando_envio
	] = transporta_aguardando_envio_concedido(bilhete_unico, valor, cur)

	entrada_envio = {
	    "NumBilheteUnico": bilhete_unico,
	    "DataEnvio": datetime.now(),
	    "Valor": valor,
	    "Confirmado": True,
	    "Observacao": None
	}

	insere_HISTORICOENVIOSPTRANS(entrada_envio, cur)

	return (sucesso, status, (concedido, aguardando_envio))
