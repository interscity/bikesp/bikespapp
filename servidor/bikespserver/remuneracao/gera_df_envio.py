'''
	Módulo responsável por gerar um csv a ser enviado para SPTrans
	contendo os bilhetes únicos e remunerações a serem inseridas
'''

import sys
from pathlib import Path

import pandas as pd

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import dump_bilhetes_ativos
from servidor.bikespserver.utils.parsers import parse_dinheiro_float
#pylint: enable=wrong-import-position


def gera_df_envio(force_prod=False) -> pd.DataFrame:
	'''
		Gera o arquivo csv no formato desejável pela SPTrans
	'''
	con = conecta(force_prod)
	cur = con.cursor()

	# Dump dos bilhetes únicos ativos
	dump_bilhetes = dump_bilhetes_ativos(cur)

	# Constrói o dataframe no formato correto
	lista_envio = []
	for bilhete in dump_bilhetes:
		numero = bilhete[0]
		valor = parse_dinheiro_float(bilhete[1])

		# Restrição SPTrans: valor >= 10
		if valor < 10:
			continue

		lista_envio.append({"id": numero, "codigo": "691", "valor": valor})

	df = pd.DataFrame(lista_envio)

	cur.close()
	con.close()

	return df
