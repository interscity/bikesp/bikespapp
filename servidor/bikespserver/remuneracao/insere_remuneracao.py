'''
	Módulo responsável por gerenciar as remunerações do usuário
'''
import sys

from pathlib import Path

import psycopg2

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.consultas import get_bilhete_ativo_pessoa, bilhete_unico_existe
from servidor.bikespserver.bd.atualizacoes import atualiza_bilhete_unico

from servidor.bikespserver.utils.parsers import parse_dinheiro_float

#pylint: enable=wrong-import-position


def insere_remuneracao_para_enviar(id_pessoa: int | str, remuneracao: float,
                                   cur: psycopg2.extensions.cursor):
	'''
		Adiciona remuneração em aguardandoEnvio
		para certa pessoa
	'''
	bilhete_ativo = get_bilhete_ativo_pessoa(id_pessoa, cur)
	if bilhete_ativo is None:
		return
	bilhete_unico = bilhete_ativo[0]
	data_inicio = bilhete_ativo[2]
	data_fim = bilhete_ativo[3]
	ativo = bilhete_ativo[4]
	concedido = parse_dinheiro_float(bilhete_ativo[5])
	aguardando_envio = parse_dinheiro_float(bilhete_ativo[6]) + remuneracao
	aguardando_resposta = parse_dinheiro_float(bilhete_ativo[7])

	# envia remuneração para coluna aguardandoEnvio
	dict_bilhete_unico = cria_dict_bilhete_unico(bilhete_unico, id_pessoa,
	                                             data_inicio, data_fim, ativo,
	                                             concedido, aguardando_envio,
	                                             aguardando_resposta)
	atualiza_bilhete_unico(dict_bilhete_unico, cur)


def transporta_aguardando_envio_concedido(
        bilhete_unico: str, valor: float,
        cur: psycopg2.extensions.cursor) -> tuple[float, float]:
	'''
		Transporta um valor de aguardando envio para concedido
	'''
	bilhete_info = bilhete_unico_existe(bilhete_unico, cur)

	id_pessoa = bilhete_info[1]
	data_inicio = bilhete_info[2]
	data_fim = bilhete_info[3]
	ativo = bilhete_info[4]
	concedido = parse_dinheiro_float(bilhete_info[5]) + valor
	aguardando_envio = parse_dinheiro_float(bilhete_info[6]) - valor
	aguardando_resposta = parse_dinheiro_float(bilhete_info[7])

	dict_bilhete_unico = cria_dict_bilhete_unico(bilhete_unico, id_pessoa,
	                                             data_inicio, data_fim, ativo,
	                                             concedido, aguardando_envio,
	                                             aguardando_resposta)
	atualiza_bilhete_unico(dict_bilhete_unico, cur)
	return (concedido, aguardando_envio)


#pylint: disable=too-many-arguments
def cria_dict_bilhete_unico(bilhete_unico, id_pessoa, data_inicio, data_fim,
                            ativo, concedido, aguardando_envio,
                            aguardando_resposta) -> dict:
	'''
		Cria dicionário para inserir na tabela BILHETEUNICO
	'''
	dict_bilhete_unico = {
	    "NumBilheteUnico": bilhete_unico,
	    "idPessoa": id_pessoa,
	    "dataInicio": data_inicio,
	    "dataFim": data_fim,
	    "ativo": ativo,
	    "concedido": concedido,
	    "aguardandoEnvio": aguardando_envio,
	    "aguardandoResposta": aguardando_resposta
	}
	return dict_bilhete_unico
