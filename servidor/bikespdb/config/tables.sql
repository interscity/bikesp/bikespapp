CREATE SCHEMA auth_bikesp;
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE DOMAIN tipo_trajeto AS jsonb;

CREATE TABLE GRUPOPESQUISA(
    idGrupo int PRIMARY KEY,
    remuneracao money NOT NULL
);

CREATE TABLE HISTORICOGPESQUISA(
    idGrupo integer NOT NULL,
    data timestamp NOT NULL,
    remuneracao_anterior money,
    remuneracao_atual money,
    operacao varchar(15) NOT NULL CHECK (operacao in ('remoção','inserção','atualização')),
    CONSTRAINT pk_historicogpesquisa PRIMARY KEY (idGrupo,data)
);

CREATE TABLE PESSOA (
    idPessoa serial PRIMARY KEY,
    nome varchar(50) NOT NULL,
    dataNascimento date NOT NULL,
    genero varchar(2) NOT NULL,
    raca varchar(10) NOT NULL,
    rg varchar(15) NOT NULL,
    cpf varchar(15) UNIQUE NOT NULL,
    email varchar(64) UNIQUE NOT NULL,
    endereco varchar(300) NOT NULL,
    telefone varchar(15) NOT NULL,
    idGrupo integer,
    validouLocais bool NOT NULL DEFAULT 'f',
    -- idGrupo integer NOT NULL,
    CONSTRAINT fk_pessoa_grupo FOREIGN KEY (idGrupo) REFERENCES GRUPOPESQUISA (idGrupo)
);

CREATE TABLE auth_bikesp.USUARIO (
    idPessoa integer PRIMARY KEY,
    senhaHash varchar(500) NOT NULL,
    admin bool NOT NULL DEFAULT 'f',
    CONSTRAINT fk_usuario_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE auth_bikesp.SESSAO (
    idPessoa integer PRIMARY KEY,
    token varchar(500) UNIQUE NOT NULL,
    dataValidade timestamp NOT NULL,
    CONSTRAINT fk_sessao_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE auth_bikesp.TOKENREDEFINICAOSENHA(
    idPessoa integer,
    token varchar(500) UNIQUE NOT NULL,
    dataValidade timestamp NOT NULL,
    CONSTRAINT pk_tokenredefinicao PRIMARY KEY (idPessoa,token),
    CONSTRAINT fk_tokenredefinicao_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)

);

CREATE TABLE auth_bikesp.TOKENNOTIFICACAO (
    idPessoa integer NOT NULL,
    token varchar(500) UNIQUE NOT NULL,
    CONSTRAINT pk_tokennotificacao PRIMARY KEY (idPessoa, token),
    CONSTRAINT fk_tokennotificacao_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE RESPOSTAFORMS (
    idPessoa integer PRIMARY KEY,
    dataResposta timestamp NOT NULL,
    sabeAndarDeBike bool ,
    temProblemaFisico bool ,
    temBikePropria bool ,
    comoConseguiraAcessoBike text,
    aceitouTermoConsentimento bool ,
    autorizouColetaInfo bool ,
    possuiBilheteUnico bool ,
    poderaInstalarApp bool ,
    qtdPessoasApartamento integer ,
    rendaMensalFamiliar numrange ,
    estudaRegularmente bool ,
    grauInstrucao varchar(50),
    condicaoAtividade varchar(50),
    vinculoEmpregaticio varchar(50),
    rendaMensalIndividual numrange ,
    qtdViagensAPe int4range ,
    qtdViagensTransPub int4range ,
    qtdViagensBike int4range ,
    qtdViagensTransPubBike int4range ,
    qtdViagensOnibus int4range ,
    qtdViagensTremMetro int4range ,
    qtdViagensCombTransPub int4range ,
    qtdViagensCarroAplicativo int4range ,
    qtdViagensMoto int4range ,
    qtdViagensCarroMotoTransPub int4range ,
    qtdViagensOutro int4range ,
    realizariaTrajetos220 bool ,
    pqNaoIncentivo220 varchar(300),
    qtdViagensIncentivo220 integer ,
    realizariaTrajetos440 bool ,
    pqNaoIncentivo440 varchar(300),
    qtdViagensIncentivo440 integer ,
    cienciaGarantiaNaoSelecao bool ,
    cienciaNecessidadeInternet bool ,
    declaracaoRespostasVeridicas bool ,
    CONSTRAINT fk_respostaforms_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE LOCALIZACAO (
    idLocalizacao bigserial PRIMARY KEY,
    endereco varchar(320) NOT NULL,
    coordenadas point NOT NULL,
    tipoLocalizacao varchar(30) NOT NULL CHECK (tipoLocalizacao IN ('qualquer','estacao'))
);

CREATE TABLE PARVALIDO (
    idPonto1 integer NOT NULL,
    idPonto2 integer NOT NULL,
    distancia double precision NOT NULL CHECK (distancia > 0),
    trajeto tipo_trajeto NOT NULL,

    CONSTRAINT pk_parvalido PRIMARY KEY (idPonto1,idPonto2),
    CONSTRAINT fk_parvalido_localizacao1 FOREIGN KEY (idPonto1) REFERENCES LOCALIZACAO (idLocalizacao) ON DELETE CASCADE,
    CONSTRAINT fk_parvalido_localizacao2 FOREIGN KEY (idPonto2) REFERENCES LOCALIZACAO (idLocalizacao) ON DELETE CASCADE,
    CONSTRAINT chk_parvalido CHECK (idPonto1 <> idPonto2)

);

CREATE TABLE PESSOAESTACAO(
    idPessoa integer NOT NULL,
    idLocalizacao integer NOT NULL,

    CONSTRAINT pk_pessoaestacao PRIMARY KEY (idPessoa,idLocalizacao),
    CONSTRAINT fk_pessoaestacao_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa),
    CONSTRAINT fk_pessoaestacao_estacao FOREIGN KEY (idLocalizacao) REFERENCES LOCALIZACAO (idLocalizacao)

);

CREATE TABLE APELIDOLOCALIZACAO(
    idPessoa integer NOT NULL,
    idLocalizacao integer NOT NULL,
    apelido varchar(28) NOT NULL,

    CONSTRAINT pk_apelidolocalizacao PRIMARY KEY (idPessoa,idLocalizacao),
    CONSTRAINT fk_apelidolocalizacao_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa),
    CONSTRAINT fk_apelidolocalizacao_localizacao FOREIGN KEY (idLocalizacao) REFERENCES LOCALIZACAO (idLocalizacao) 
);

CREATE TABLE RESPOSTATRAJETO(
    idPessoa integer NOT NULL,
    apelido1 varchar(28) NOT NULL,
    apelido2 varchar(28) NOT NULL,
    frequencia varchar(300),
    duracao integer,
    comoFaz varchar(300),
    possibilidadeBike varchar(300),

    CONSTRAINT pk_respostatrajeto PRIMARY KEY (idPessoa,apelido1,apelido2),
    CONSTRAINT fk_respostatrajeto_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA(idPessoa)
);

CREATE TABLE ALTERACAOLOCALIZACAO(
    idPessoa integer NOT NULL PRIMARY KEY,
    data date NOT NULL DEFAULT '1900-01-01',

    CONSTRAINT fk_alteracaolocalizacao FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE VIAGEM(
    idViagem serial PRIMARY KEY,
    idPessoa integer NOT NULL,
    data timestamp NOT NULL,
    deslocamento double precision NOT NULL CHECK (deslocamento >= 0),
    idOrigem integer,
    idDestino integer,
    status varchar(30) NOT NULL CHECK (status in ('EmAnalise', 'Reprovado','Aprovado')),
    remuneracao money,
    motivoStatus varchar(100) NOT NULL,
    trajeto tipo_trajeto NOT NULL,
    activityRecognitionTrip jsonb,
    metadados jsonb,

    CONSTRAINT fk_viagem_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa),
    CONSTRAINT fk_viagem_localizacao_origem FOREIGN KEY (idOrigem) REFERENCES LOCALIZACAO (idLocalizacao),
    CONSTRAINT fk_viagem_localizacao_destino FOREIGN KEY (idDestino) REFERENCES LOCALIZACAO (idLocalizacao)

    -- CONSTRAINT chk_origem_destino CHECK (idOrigem <> idDestino)
);

CREATE TABLE CONTESTACAO(
    idViagem integer NOT NULL PRIMARY KEY,
    data timestamp NOT NULL,
    justificativa varchar(300) NOT NULL,
    aprovada bool,
    resposta varchar(1000),

    CONSTRAINT fk_contestacao_viagem FOREIGN KEY (idViagem) REFERENCES VIAGEM (idViagem)
);

CREATE TABLE BILHETEUNICO(
    bilheteUnico varchar(15) NOT NULL PRIMARY KEY,
    idPessoa integer NOT NULL,
    dataInicio timestamp NOT NULL,
    dataFim timestamp,
    ativo bool NOT NULL,
    concedido money NOT NULL CHECK (concedido >= money(0.0)),
    aguardandoEnvio money NOT NULL CHECK (aguardandoEnvio >= money(0.0)),
    aguardandoResposta money NOT NULL CHECK (aguardandoResposta >= money(0.0)),

    CONSTRAINT fk_bilhetesunicos_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE HISTORICOENVIOSPTRANS(
    bilheteUnico varchar(15) NOT NULL,
    dataEnvio timestamp NOT NULL,
    valor money NOT NULL CHECK (valor >= money(0.0)),
    confirmado bool NOT NULL,
    observacao varchar(300),

    CONSTRAINT pk_historicoenviosptrans PRIMARY KEY (bilheteUnico,dataEnvio),
    CONSTRAINT fk_historicoenviosptrans_bilhetesunicos FOREIGN KEY (bilheteUnico) REFERENCES BILHETEUNICO (bilheteUnico)
);

CREATE TABLE FEEDBACKVIAGEM(
    idViagem integer NOT NULL PRIMARY KEY,
    tokenFeedback varchar(500) UNIQUE NOT NULL,
    respondido bool NOT NULL,
    motivoOriginal varchar(100) NOT NULL,
    /* PERGUNTAS AO USUARIO */
    tempoClima varchar(50),
    outrosApps varchar(200),
    feedbackGeral varchar(1000),
    respostasDinamicas jsonb,

    CONSTRAINT fk_feedbackviagem_viagem FOREIGN KEY (idViagem) REFERENCES VIAGEM (idViagem)
);

CREATE TABLE FEEDBACKVIAGEMPERDIDA(
    idPessoa integer NOT NULL,
    dataFeedback timestamp NOT NULL,
    /* PERGUNTAS AO USUARIO */
    quandoPerdeu varchar(1000),
    algoDiferente varchar(1000),
    verApp varchar(20),
    feedbackGeral varchar(1000),
    respostasDinamicas jsonb,

    CONSTRAINT pk_feedbackviagemperdida PRIMARY KEY (idPessoa,dataFeedback),
    CONSTRAINT fk_feedbackviagemperdida_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa)
);

CREATE TABLE BONUS(
    idBonus serial NOT NULL PRIMARY KEY,
    nome varchar(100) NOT NULL UNIQUE,
    valor money NOT NULL DEFAULT 0.0,
    ativo bool NOT NULL,
    visivel bool NOT NULL,
    descricao varchar(1000),
    dataInicio timestamp NOT NULL,
    dataFim timestamp
);

CREATE TABLE PESSOABONUS(
    idPessoa integer NOT NULL,
    idBonus integer NOT NULL,
    dataConcessao timestamp,
    CONSTRAINT pk_pessoabonus PRIMARY KEY (idPessoa,idBonus),
    CONSTRAINT fk_pessoabonus_pessoa FOREIGN KEY (idPessoa) REFERENCES PESSOA (idPessoa),
    CONSTRAINT fk_pessoabonus_bonus FOREIGN KEY (idBonus) REFERENCES BONUS (idBonus)
);

CREATE OR REPLACE FUNCTION historico_grupo_pesquisa_function()
RETURNS trigger AS $historico_grupo_pesquisa_function$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO HISTORICOGPESQUISA (idGrupo,data,remuneracao_atual,operacao)
        VALUES (NEW.idGrupo,LOCALTIMESTAMP,NEW.remuneracao,'inserção');
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO HISTORICOGPESQUISA (idGrupo,data,remuneracao_anterior,remuneracao_atual,operacao)
        VALUES (NEW.idGrupo,LOCALTIMESTAMP,OLD.remuneracao,NEW.remuneracao,'atualização');
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        INSERT INTO HISTORICOGPESQUISA (idGrupo,data,operacao)
        VALUES (OLD.idGrupo,LOCALTIMESTAMP,'remoção');
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$historico_grupo_pesquisa_function$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_historico_grupo_pesquisa
AFTER INSERT OR UPDATE OR DELETE ON GRUPOPESQUISA
    FOR EACH ROW EXECUTE PROCEDURE historico_grupo_pesquisa_function();