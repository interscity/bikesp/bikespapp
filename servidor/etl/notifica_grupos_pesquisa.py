'''
	Notifica os usuários dos grupos de pesquisa que tiveram
	a remuneração alterada desde uma certa data
'''

import sys
import argparse
from datetime import datetime
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.gerenciamento.notifica_grupos_pesquisa \
	import pessoas_para_notificar,notifica_pessoa
from servidor.bikespserver.bd.conecta import conecta

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, Utiliza o BD de teste')
	arg_parser.add_argument("-dt","--date",
		action='store',required=True,
		type=lambda d: datetime.strptime(d, "%d/%m/%Y").date(),
		help='(DD/MM/YYYY). Alterações nos grupos a partir desta data serão consideradas')

	args = arg_parser.parse_args()

	return args


def main():
	'''
		Main
	'''
	args = parse_args()

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)

	usuarios = pessoas_para_notificar(con,args.date)

	con.close()

	lista_deve_notificar = []

	for grupo,valor in usuarios.items():
		data_alteracao = valor["data"]
		remuneracao_atual = valor["remuneracao"]
		pessoas = valor["pessoas"]
		print("Notificação grupo " + str(grupo) + ":")
		print("\tData da alteração:",data_alteracao)
		print("\tRemuneração atual:",remuneracao_atual)
		print("\tQuantidade de pessoas:",len(pessoas))
		quer_ver_pessoas = ""

		while quer_ver_pessoas not in ("S","N"):
			quer_ver_pessoas = input("Gostaria de ver quem são as pessoas? (S/N): ")

		quer_ver_pessoas = bool(quer_ver_pessoas == "S")

		if quer_ver_pessoas:
			print("Exibindo pessoas:")
			for pessoa in pessoas:
				print("\tID Pessoa: " + str(pessoa["idPessoa"]) + ", email: " + pessoa["email"])

		quer_notificar = ""
		while quer_notificar not in ("S","N"):
			quer_notificar = input("Confirmar notificação para essas " + str(len(pessoas)) + \
				" pessoas do grupo " + str(grupo) + "? (S/N): ")

		quer_notificar = bool(quer_notificar == "S")

		if quer_notificar:
			for pessoa in pessoas:
				email = pessoa["email"]
				nome = pessoa["nome"]
				lista_deve_notificar.append({"nome":nome,"email":email,"data_alteracao":data_alteracao,
					"remuneracao":remuneracao_atual})

	# Envio de email é feito para todas as pessoas de uma vez
	for pessoa in lista_deve_notificar:
		notifica_pessoa(pessoa["nome"],pessoa["email"],pessoa["data_alteracao"],
			pessoa["remuneracao"],debug=force_dev)

# pylint: disable=duplicate-code
if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		pass
