'''
	Gera um arquivo csv contendo uma lista
	de remunerações por bilhete único
'''
import argparse
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.remuneracao.gera_df_envio import gera_df_envio

#pylint: enable=wrong-import-position


def parse_args():
	'''
		Realiza o parseamento dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-o","--output",
		action='store',required=True,help='Caminho de saída para o CSV')
	arg_parser.add_argument("-p","--production",
		action='store_true',required=False,help='Force production')

	args = arg_parser.parse_args()

	return args

def main():
	'''
		Main
	'''
	args = parse_args()

	output_file = args.output

	force_prod = bool(args.production)

	df = gera_df_envio(force_prod=force_prod)

	# Constrói o csv no formato correto
	df.to_csv(output_file,index=False,header=None)


if __name__ == "__main__":
	main()
