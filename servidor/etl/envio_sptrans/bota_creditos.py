'''
	Dado um csv_envio, insere os envios na tabela HISTORICOENVIOS
	e transporte o valor do aguardandoEnvio para o concedido
'''
import sys
import os
from pathlib import Path
import argparse

import pandas as pd

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.remuneracao.confirma_envio_sptrans \
	import confirma_envio_sptrans

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("--csv",
		action='store',required=True,help='Caminho do CSV de envio')

	args = arg_parser.parse_args()

	if not os.path.exists(args.csv):
		arg_parser.error("CSV não encontrado")

	return args

def converte_df_lista(df) -> list:
	'''
		Converte um df para um array
	'''
	lista = []
	for _, linha in df.iterrows():
		lista.append({"bilheteUnico":linha["bilheteUnico"],"valor":linha["valor"]})
	return lista

def main():
	'''
		Main
	'''
	args = parse_args()

	csv_path = args.csv

	df = pd.read_csv(csv_path,
		names=['bilheteUnico', 'codigoSPTrans','valor'],
		dtype={"bilheteUnico":str,"codigoSPTrans":int,"valor":float}
	)

	lista_insercoes = converte_df_lista(df)

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	sucesso_total = True

	for insercao in lista_insercoes:
		print("BilheteUnico: " + str(insercao["bilheteUnico"]) + \
			" | Valor: " + str(insercao["valor"]))

		[sucesso,status,[concedido,aguardando_envio]] = confirma_envio_sptrans(
			insercao["bilheteUnico"],insercao["valor"],cur)
		if not sucesso:
			print("\tERRO: " + status)
		else:
			print("\tSUCESSO: Concedido = " + str(concedido) + \
			" | AguardandoEnvio = " + str(aguardando_envio))

		sucesso_total = sucesso_total and sucesso

	if sucesso_total:
		con.commit()
	else:
		print("NÃO FOI POSSÍVEL COMMITAR")

	con.close()


if __name__ == "__main__":
	main()
