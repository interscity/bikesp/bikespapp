'''
	Aprova ou reprova uma contestação
'''
import sys
import argparse

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.contestacao.gerencia_contestacao import gerencia_contestacao
from servidor.bikespserver.contestacao.notifica_contestacao import notifica_contestacao
#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-id","--viagem",
		action='store',required=True,help='Id da viagem contestada a ser gerenciada')
	arg_parser.add_argument("-v","--valor",
		action='store',required=False,
		help='Valor da remuneração da nova viagem. Se vazio, usa o padrão (distância*grupo)')
	arg_parser.add_argument("--notificar",
		action='store_true',required=False,
		help='Caso deseje enviar um email automático para o usuário')
	arg_parser.add_argument("-m","--resposta",
		action='store',required=True,
		help='Resposta ao usuário sobre essa contestação')

	grupo_aprovar = arg_parser.add_argument_group('Gerenciamento da aprovação')
	aprovacao = grupo_aprovar.add_mutually_exclusive_group()
	aprovacao.add_argument("-a","--aprovar",
		action='store_true',required=False,help='Aprovar a contestação')
	aprovacao.add_argument("-r","--reprovar",
		action='store_true',required=False,help='Reprovar a contestação')

	args = arg_parser.parse_args()

	if not args.viagem.isdigit():
		arg_parser.error("ID da viagem inválido")

	try:
		if args.valor is not None:
			float(args.valor)
	except ValueError:
		arg_parser.error("Valor da nova remuneração inválido.")

	if (not args.aprovar and not args.reprovar) or (args.aprovar and args.reprovar):
		arg_parser.error("Viagem deve ser aprovada OU reprovada.")

	if args.reprovar and args.valor is not None:
		arg_parser.error("Valor só é permitido com contestações aprovadas")

	if len(args.resposta) > 1000:
		arg_parser.error("Resposta deve ter no máximo 1000 caracteres")

	return args

def input_confirma(debug):
	'''
		Aguarda a confirmação do usuário
		para seguir o fluxo
	'''
	if debug:
		return
	confirma = ""
	while confirma not in ["S","N"]:
		confirma = input("Confirma (S/N): ")
	if confirma == "N":
		sys.exit()

def main():
	'''
		Main
	'''
	args = parse_args()

	id_viagem = args.viagem
	aprovar = args.aprovar
	valor = args.valor
	notificar = bool(args.notificar)
	resposta = args.resposta
	force_dev = bool(args.debug)

	if not notificar:
		print("ATENÇÃO: USUÁRIO NÃO SERÁ NOTIFICADO")

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	[sucesso,status,remuneracao] = gerencia_contestacao(id_viagem,aprovar,resposta,cur,valor=valor)

	if not sucesso:
		print("ERRO:",status)
		sys.exit()

	if aprovar and not args.debug:
		print("Atualização: \n\tViagem: " + str(id_viagem) + \
		"\n\tRemuneração: " + str(remuneracao) + \
		"\n\tResposta: " + resposta)
	if notificar and not args.debug:
		print()
		print("Notificando usuário...")
		print("\tAprovar:",aprovar)
		print("\tResposta:",resposta)
		print()
		input_confirma(args.debug)
		notifica_contestacao(id_viagem,cur)
	else:
		input_confirma(args.debug)

	con.commit()
	con.close()

if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		pass
