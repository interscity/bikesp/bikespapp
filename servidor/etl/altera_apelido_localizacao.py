'''
	Atualiza o apelido de uma localizacao para uma pessoa
'''
import sys
import argparse
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.atualiza_localizacao_pessoa \
	import atualiza_apelido_localizacao_pessoa

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-p","--pessoa",
		action='store',required=True,help='ID da pessoa')
	arg_parser.add_argument("-l", "--localizacao",
		action='store',required=True,help='ID da localizacao')
	arg_parser.add_argument("-a", "--apelido",
		action='store',required=True,help='Novo apelido da localizacao')

	args = arg_parser.parse_args()

	if not args.pessoa.isdigit():
		arg_parser.error("ID da pessoa inválido")

	if not args.localizacao.isdigit():
		arg_parser.error("ID da localizacao inválido")

	return args


def main():
	'''
		Main
	'''
	args = parse_args()

	id_pessoa = args.pessoa
	id_localizacao = args.localizacao
	apelido = args.apelido

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	[sucesso,status] = atualiza_apelido_localizacao_pessoa(id_pessoa,id_localizacao,apelido,cur)

	print(status)

	if sucesso:
		con.commit()
	con.close()

if __name__ == "__main__":
	main()
