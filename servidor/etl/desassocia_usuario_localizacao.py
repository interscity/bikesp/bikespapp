'''
	Remove a relação entre um usuário e uma localização.
'''

import sys
import argparse

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.bd.consultas import apelido_localizacao_existe, pessoa_estacao_existe
from servidor.bikespserver.gerenciamento.remove_localizacao_pessoa \
	import desassocia_localizacao_pessoa

#pylint: enable=wrong-import-position


def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-p","--pessoa",
		action='store',required=True,help='ID da pessoa que receberá a localização')
	arg_parser.add_argument("-l","--localizacao",
		action='store',required=True,help='ID da localização')

	arg_parser.add_argument("-t","--tipo",
		action='store',required=True, default="qualquer",
		choices=["qualquer","estacao"], help='Tipo da localização')

	args = arg_parser.parse_args()

	if not args.localizacao.isdigit():
		arg_parser.error("ID da localização inválido")

	if not args.pessoa.isdigit():
		arg_parser.error("ID da pessoa inválido")

	return args

def main():
	'''
		Main
	'''
	args = parse_args()
	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	id_localizacao = args.localizacao
	id_pessoa = args.pessoa
	tipo_localizacao = args.tipo

	if tipo_localizacao == "qualquer" and \
		not apelido_localizacao_existe(id_pessoa, id_localizacao, cur):
		print("Não há associação entre o usuário",id_pessoa, "e a localização", id_localizacao)
		return

	if tipo_localizacao =="estacao" and \
		not pessoa_estacao_existe(id_pessoa, id_localizacao, cur):
		print("Não há associação entre o usuário",id_pessoa, "e a estação", id_localizacao)
		return

	desassocia_localizacao_pessoa(id_pessoa, id_localizacao, tipo_localizacao, cur)

	con.commit()
	con.close()

	print("Associação entre", id_pessoa, "e", id_localizacao, "foi removida")

if __name__ == "__main__":
	main()
