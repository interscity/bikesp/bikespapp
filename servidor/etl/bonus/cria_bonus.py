'''
	Cria um bônus no banco de dados
'''
import sys
import argparse

from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.bonus.cria_bonus import cria_bonus

#pylint: enable=wrong-import-position

def data_valida(data: str) -> datetime:
	'''
		Checa se uma data é válida
	'''
	try:
		return datetime.strptime(data, "%Y-%m-%d %H:%M:%S")
	except ValueError as exc:
		raise argparse.ArgumentTypeError(
			f"data inválida: {data!r}. formato YYY-MM-DD HH:mm:SS") from exc

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-n","--nome",
		action='store',required=True,help='Título do bônus')
	arg_parser.add_argument("-v","--valor",
		action='store',required=True,help='Valor do bônus')
	arg_parser.add_argument("--inativo",
		action='store_true',required=False,default=False,help='Bônus está inativo')
	arg_parser.add_argument("--invisivel",
		action='store_true',required=False,default=False,help='Bônus não estará visível aos usuários')
	arg_parser.add_argument("-desc","--descricao",
		action='store',required=False,help='Descrição do Bônus')
	arg_parser.add_argument("-i","--inicio",
		action='store',required=False,default=datetime.now(),
		type=data_valida,help='Data de início do bônus - formato YYY-MM-DD HH:mm:SS ')
	arg_parser.add_argument("-f","--fim",
		action='store',required=False,type=data_valida,help='Data do fim do bônus')

	args = arg_parser.parse_args()

	try:
		float(args.valor)
	except ValueError:
		arg_parser.error("Valor do bônus é inválido")

	return args

def main():
	'''
		Main
	'''
	args = parse_args()

	nome = args.nome
	valor = float(args.valor)
	ativo = not bool(args.inativo)
	visivel = not bool(args.invisivel)
	descricao = args.descricao
	data_inicio = args.inicio
	data_fim = args.fim

	dict_insercao_bonus = {
		"nome":nome,
		"valor":valor,
		"ativo":ativo,
		"visivel": visivel,
		"descricao": descricao,
		"data_inicio": data_inicio,
		"data_fim": data_fim
	}

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	[sucesso,status,id_novo_bonus] = cria_bonus(dict_insercao_bonus,cur)

	if sucesso:
		con.commit()
		print("SUCESSO - Bônus criado com o id: " + str(id_novo_bonus))
	else:
		print("ERRO AO CRIAR BÔNUS:" + status)
	con.close()


if __name__ == "__main__":
	main()
