'''
	Concede um bônus a uma pessoa
'''
import sys
import argparse

from pathlib import Path
from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.bonus.concede_bonus import concede_bonus
from servidor.etl.bonus.cria_bonus import data_valida

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-p","--pessoa",
		action='store',required=True,help='ID da pessoa')
	arg_parser.add_argument("-b","--bonus",
		action='store',required=True,help='ID do bônus')
	arg_parser.add_argument("--data",
		action='store',required=False,default=datetime.now(),
		type=data_valida,help='Data de concessão do bônus - formato YYY-MM-DD HH:mm:SS ')

	args = arg_parser.parse_args()

	return args

def main():
	'''
		Main
	'''
	args = parse_args()

	bonus = args.bonus
	pessoa = args.pessoa
	data_concessao = args.data

	dict_insercao_pessoabonus = {
		"idPessoa":pessoa,
		"idBonus":bonus,
		"dataConcessao":data_concessao
	}

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	[sucesso,status] = concede_bonus(dict_insercao_pessoabonus,cur)

	if sucesso:
		con.commit()
		print("SUCESSO - Bônus concedido")
	else:
		print("ERRO AO CONCEDER BÔNUS:" + status)

	con.close()


if __name__ == "__main__":
	main()
