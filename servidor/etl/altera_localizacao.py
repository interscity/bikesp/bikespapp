'''
	Atualiza o endereço de uma localizacao de uma pessoa
'''
import sys
import argparse
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.atualiza_localizacao_pessoa \
	import atualiza_localizacao_pessoa, checa_residencia_unica
from servidor.bikespserver.bd.consultas import get_residencia_pessoa

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()
	grupo = arg_parser.add_mutually_exclusive_group(required=True)

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-p","--pessoa",
		action='store',required=True,help='ID da pessoa')
	grupo.add_argument("-l", "--localizacao",
		action='store',help='ID da localizacao')
	grupo.add_argument("-r", "--residencia",
		action='store_true',help='Atualiza a residencia da pessoa')

	arg_parser.add_argument("--cep",
		action='store',required=True,help='CEP do novo endereço')
	arg_parser.add_argument("--rua",
		action='store',required=True,help='Rua do novo endereço')
	arg_parser.add_argument("--numero",
		action='store',required=True,help='Número do novo endereço')
	arg_parser.add_argument("--complemento",
		action='store',required=False,help='Complemento do novo endereço')
	arg_parser.add_argument("--bairro",
		action='store',required=True,help='Bairro do novo endereço')
	arg_parser.add_argument("--cidade",
		action='store',required=False,help='Cidade do novo endereço')
	arg_parser.add_argument("--estado",
		action='store',required=False,help='Estado do novo endereço')

	args = arg_parser.parse_args()

	if not args.pessoa.isdigit():
		arg_parser.error("ID da pessoa inválido")

	if args.localizacao is not None and not args.localizacao.isdigit():
		arg_parser.error("ID da localizacao inválido")

	if args.complemento is None:
		args.complemento = ""
	if args.cidade is None:
		args.cidade = "São Paulo"
	if args.estado is None:
		args.estado = "São Paulo"

	return args


def main():
	'''
		Main
	'''
	args = parse_args()

	id_pessoa = args.pessoa

	dict_localizacao = {
		"cep": args.cep,
		"rua": args.rua,
		"numero": args.numero,
		"complememento": args.complemento,
		"bairro": args.bairro,
		"cidade": args.cidade,
		"estado": args.estado
	}

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	id_localizacao = None
	if bool(args.residencia) is True:
		[sucesso, status] = checa_residencia_unica(id_pessoa,cur)

		if not sucesso:
			print(status)
			con.close()
			return

		id_localizacao = get_residencia_pessoa(id_pessoa,cur)
		assert id_localizacao is not None
		id_localizacao = id_localizacao[0][0]
	else:
		id_localizacao = args.localizacao

	[sucesso,status] = atualiza_localizacao_pessoa(id_pessoa,id_localizacao,
	                                               dict_localizacao,cur,
	                                               force_prod = not force_dev)

	print(status)

	if sucesso:
		con.commit()
	con.close()

if __name__ == "__main__":
	main()
