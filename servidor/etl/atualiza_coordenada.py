'''
	Atualiza as coordenadas de uma localização,
	e os seus respectivos trajetos
'''
import sys
import argparse

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.atualiza_coordenada_localizacao \
	import atualiza_coordenada_localizacao

#pylint: enable=wrong-import-position


def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-id","--localizacao",
		action='store',required=True,help='ID da localização')
	arg_parser.add_argument("-lat","--latitude",
		action='store',required=True,help='Latitude da nova localização')
	arg_parser.add_argument("-lon","--longitude",
		action='store',required=True,help='Longitude da nova localização')

	args = arg_parser.parse_args()

	if not args.localizacao.isdigit():
		arg_parser.error("ID da localização inválida")

	try:
		if float(args.latitude) < -90 or float(args.latitude) > 90:
			raise ValueError
	except ValueError:
		arg_parser.error("Valor de latitude inválido")
	try:
		if float(args.longitude) < -90 or float(args.longitude) > 90:
			raise ValueError
	except ValueError:
		arg_parser.error("Valor de longitude inválido")

	return args

def main():
	'''
		Main
	'''
	args = parse_args()

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()
	id_localizacao = args.localizacao
	coordenada = (float(args.latitude),float(args.longitude))

	sucesso = atualiza_coordenada_localizacao(id_localizacao,coordenada,cur,force_prod=not force_dev)

	print(sucesso)

	con.commit()
	con.close()

if __name__ == "__main__":
	main()
