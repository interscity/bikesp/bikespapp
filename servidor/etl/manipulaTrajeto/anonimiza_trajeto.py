"""
Módulo responsável por anonimizar (dessociar de uma posição no espaço) um dado trajeto.
"""

import sys
import argparse
import json
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.utils.parsers import parse_data_js
from servidor.bikespserver.viagem.consulta_trajeto import consulta_trajeto

#pylint: enable=wrong-import-position

#pylint: disable=invalid-name,disable=bare-except
def anonimiza_trajeto(trajeto):
	"""
		Anonimiza um dado trajeto ao deslocar suas posições e remover timestamp.
	"""
	if len(trajeto) == 0:
		return []

	primeira_pos = trajeto[0]["Posicao"]
	if trajeto[0]["Data"] is int:
		t0 = trajeto[0]["Data"]
	else:
		t0 = parse_data_js(trajeto[0]["Data"])

	novo_trajeto = []

	for ponto in trajeto:
		try:
			t = ponto["Data"] - t0
		except:
			t = parse_data_js(ponto["Data"]) - t0

		novo_ponto = \
			{"Data": t,
			"Posicao" : {
					"latitude" : -(ponto["Posicao"]["latitude"] - primeira_pos["latitude"]),
					"longitude" : -(ponto["Posicao"]["longitude"] - primeira_pos["longitude"])
				},
			"Precisao": ponto["Precisao"]
		}

		if "Velocidade" in ponto:
			novo_ponto["Velocidade"] = ponto["Velocidade"]


		novo_trajeto.append(novo_ponto)

	return novo_trajeto
#pylint: enable=invalid-name,enable=bare-except

def parse_args():
	"""
		Realiza o parseamento dos argumentos
	"""

	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-t","--txt",
		action='store',required=False,help='Caminho do .txt com o trajeto.')
	arg_parser.add_argument("-id","--id",
		action='store',required=False,help='Id da viagem no bd de produção')
	arg_parser.add_argument("-o","--output",
		action='store',required=False,help='Arquivo de saída.')
	arg_parser.add_argument("-p","--prod",
		action='store_true',required=False,help='Forçar produção')

	return arg_parser.parse_args()


if __name__ == "__main__":
	args = parse_args()

	if args.txt is None and args.id is None:
		print("""[ERRO]: Indique o caminho de um txt com o trajeto da viagem
		 (-t), ou o id da viagem no bd de produção (-id).\n
		 Use a flag -p para forçar o ambiente de produção""")
		sys.exit()


	args.prod = bool(args.prod)

	if args.txt is None:
		#busca viagem no bd
		trajeto_original = json.loads(consulta_trajeto(args.id,force_prod=args.prod))

	elif args.id is None:
		# carrega trajeto do txt
		with open(args.txt,"r", encoding="utf-8") as arq_entrada:
			trajeto_js = arq_entrada.read()
			trajeto_original = json.loads(trajeto_js)

	trajeto_anonimizado = anonimiza_trajeto(trajeto_original)
	saida = json.dumps(trajeto_anonimizado)

	if args.output is None:
		print(saida)
	else:
		with open(args.output,"w+", encoding="utf-8") as arq_saida:
			arq_saida.write(saida)
			print("Trajeto salvo em " + args.output)
