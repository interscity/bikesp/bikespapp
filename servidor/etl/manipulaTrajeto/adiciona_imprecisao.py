"""
Módulo responsável por adicionar imprecisão artificial às medições de um trajeto.
"""

import sys
import argparse
import math
import json
import random as rd
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.viagem.consulta_trajeto import consulta_trajeto

#pylint: enable=wrong-import-position


#pylint: disable=invalid-name
def adiciona_imprecisao(trajeto, max_imprecisao, min_imprecisao=0.0):
	"""
		Dado um trajeto, adiciona imprecisão distribuída aleatoriamente 
	(distr. normal) de modo que fiquem com raio de imprecisão de min_imprecisao 
	até max_imprecisao metros.
	"""
	novo_trajeto = []
	max_imprecisao = float(max_imprecisao)

	for ponto in trajeto:
		novo_ponto = ponto.copy()
		lat_original = ponto["Posicao"]["latitude"]
		lng_original = ponto["Posicao"]["longitude"]
		imprecisao_original = ponto["Precisao"]

		if imprecisao_original >= max_imprecisao:
			lat_nova = lat_original
			lng_nova = lng_original
		else:
			deslocamento = abs(rd.normalvariate(mu=0,sigma = (max_imprecisao-min_imprecisao)/3))
			if deslocamento + imprecisao_original >= max_imprecisao:
				deslocamento = max_imprecisao - imprecisao_original

			# escolher ângulo uniformemente distribuído entre [0, 2pi]
			angulo = rd.uniform(0.0,2*math.pi)

			dLng = deslocamento*math.sin(angulo)/111111
			dLat = deslocamento*math.cos(angulo)/111111

			# mover amostra de acordo com sen e cos desse angulo, usando o deslocamento sorteado

			lat_nova = lat_original + dLat
			lng_nova = lng_original + dLng

		novo_ponto["Posicao"] =\
		{
			"latitude" : lat_nova,
			"longitude" : lng_nova
		}

		novo_trajeto.append(novo_ponto)

	return novo_trajeto
#pylint: enable=invalid-name

def parse_args():
	"""
		Realiza o parseamento dos argumentos
	"""

	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-min","--min",
		action='store',required=False,help='Imprecisão Mínima das amostras (padrão = 0).')
	arg_parser.add_argument("-t","--txt",
		action='store',required=False,help='Caminho do .txt com o trajeto.')
	arg_parser.add_argument("-id","--id",
		action='store',required=False,help='Id da viagem no bd de produção.')
	arg_parser.add_argument("-max","--max",
		action='store',required=True,help='Imprecisão Máxima das amostras.')
	arg_parser.add_argument("-o","--output",
		action='store',required=False,help='Arquivo de saída.')
	arg_parser.add_argument("-p","--prod",
		action='store_true',required=False,help='Forçar produção.')

	return arg_parser.parse_args()

if __name__ == "__main__":
	args = parse_args()

	if args.prod is None:
		args.prod = False

	if args.min is None:
		args.min = 0

	if args.txt is None and args.id is None:
		print("""[ERRO]: Indique o caminho de um txt com o trajeto da viagem (-t),
			ou o id da viagem no bd de produção (-id)""")
		sys.exit()

	if args.txt is None:
		#busca viagem no bd
		trajeto_original = json.loads(consulta_trajeto(args.id,force_prod=args.prod))

	elif args.id is None:
		# carrega trajeto do txt
		with open(args.txt,"r",encoding="utf-8") as arq_entrada:
			trajeto_js = arq_entrada.read()
			trajeto_original = json.loads(trajeto_js)


	trajeto_deslocado = adiciona_imprecisao(trajeto_original,args.max,args.min)
	saida = json.dumps(trajeto_deslocado)

	if args.output is None:
		print(saida)
	else:
		with  open(args.output,"w+",encoding="utf-8") as arq_saida:
			arq_saida.write(saida)
			print("Trajeto salvo em " + args.output)
