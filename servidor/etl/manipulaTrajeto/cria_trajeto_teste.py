"""
Módulo responsável por criar um trajeto para teste a partir de um trajeto existente no bd.
"""

import sys
import argparse
import json
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.viagem.consulta_trajeto import consulta_trajeto

from servidor.etl.manipulaTrajeto.anonimiza_trajeto import anonimiza_trajeto
from servidor.etl.manipulaTrajeto.adiciona_imprecisao import adiciona_imprecisao

#pylint: enable=wrong-import-position


def cria_trajeto_teste(trajeto, min_imprecisao, max_imprecisao):
	"""
		Anonimiza um trajeto e adiciona imprecisao para criar um trajeto de teste.
	"""

	trajeto_anonimizado = anonimiza_trajeto(trajeto)

	trajeto_impreciso = adiciona_imprecisao(trajeto_anonimizado,max_imprecisao,min_imprecisao)

	return trajeto_impreciso


def parse_args():
	"""
		Realiza o parseamento dos argumentos
	"""

	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-max","--max",
		action='store',required=True,help='Imprecisão Máxima das amostras')
	arg_parser.add_argument("-min","--min",
		action='store',required=False,help='Imprecisão Mínima das amostras (padrão = 0)')
	arg_parser.add_argument("-t","--txt",
		action='store',required=False,help='Caminho do .txt com o trajeto')
	arg_parser.add_argument("-id","--id",
		action='store',required=False,help='Id da viagem no bd de produção')
	arg_parser.add_argument("-o","--output",
		action='store',required=False,help='Arquivo de saída')

	return arg_parser.parse_args()


if __name__ == "__main__":
	args = parse_args()

	if args.min is None:
		args.min = 0

	if args.id is None and args.txt is None:
		print("""[ERRO]: Indique o caminho de um txt com o trajeto da viagem (-t),
		ou o id da viagem no bd de produção (-id)""")
		sys.exit()

	if args.txt is None:
		#busca viagem no bd
		trajeto_original = json.loads(consulta_trajeto(args.id,force_prod=True))

	elif args.id is None:
		# carrega trajeto do txt
		with open(args.txt,"r",encoding="utf-8") as arq_entrada:
			trajeto_js = arq_entrada.read()
			trajeto_original = json.loads(trajeto_js)

	trajeto_novo = cria_trajeto_teste(trajeto_original,args.max,args.min)
	saida = json.dumps(trajeto_novo)

	if args.output is None:
		print(saida)
	else:
		with open(args.output,"w+",encoding="utf-8") as arq_saida:
			arq_saida.write(saida)
			print("Trajeto salvo em " + args.output)
