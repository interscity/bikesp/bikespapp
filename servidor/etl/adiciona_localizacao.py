'''
	Adiciona uma nova localização para certo usuário.
	Se a localização já estiver contida na tabela LOCALIZACAO,
	ela apenas será ligada ao usuário
	(pela PESSOAESTACAO ou APELIDOLOCALIZACAO)
	Para adicionar uma residência, utilize o sacript atualiza_residencia.py
'''
import sys
import argparse

from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.adiciona_localizacao_pessoa \
	import adiciona_localizacao_pessoa

#pylint: enable=wrong-import-position


def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-p","--pessoa",
		action='store',required=True,help='ID da pessoa que receberá a localização')
	arg_parser.add_argument("-l","--localizacao",
		action='store',required=False,help='ID da localização (caso já exista)')

	arg_parser.add_argument("-t","--tipo",
		action='store',required=False,default="qualquer",
		choices=["qualquer","estacao"],help='Tipo da localização')

	arg_parser.add_argument("-e","--endereco",
		action='store',required=False,help='Endereço da localização (vai direto para o campo endereco)')
	arg_parser.add_argument("-lat","--latitude",
		action='store',required=False,help='Latitude da nova localização')
	arg_parser.add_argument("-lon","--longitude",
		action='store',required=False,help='Longitude da nova localização')
	arg_parser.add_argument("-a","--apelido",
		action='store',required=False,help='Apelido da nova localização')

	args = arg_parser.parse_args()

	if args.apelido == "Residência":
		arg_parser.error("Para adicionar uma nova residência, utilize o script atualiza_residencia.py")

	if args.localizacao is not None and not args.localizacao.isdigit():
		arg_parser.error("ID da localização inválida")

	if not args.pessoa.isdigit():
		arg_parser.error("ID da pessoa inválida")

	if args.latitude is not None and args.longitude is not None:
		try:
			args.latitude = float(args.latitude)
			if args.latitude < -90 or args.latitude > 90:
				raise ValueError
		except ValueError:
			arg_parser.error("Valor de latitude inválido")
		try:
			args.longitude = float(args.longitude)
			if args.longitude < -90 or args.longitude > 90:
				raise ValueError
		except ValueError:
			arg_parser.error("Valor de longitude inválido")

	return args

# pylint: disable=duplicate-code
def main():
	'''
		Main
	'''
	args = parse_args()
	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	cur = con.cursor()

	id_localizacao = args.localizacao
	id_pessoa = args.pessoa
	tipo_localizacao = args.tipo
	apelido = args.apelido
	endereco = args.endereco
	latitude = args.latitude
	longitude = args.longitude

	coordenada = [latitude,longitude]

	[sucesso,status] = adiciona_localizacao_pessoa(
		id_pessoa,
		id_localizacao,
		tipo_localizacao,
		apelido,
		endereco,
		coordenada,
		cur,
		force_prod=not force_dev
	)

	print(status)
	if sucesso:
		con.commit()
	con.close()

if __name__ == "__main__":
	main()
