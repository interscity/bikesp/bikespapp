'''
	Script responsável por inserir as respostas do formulário no banco de dados
	Para ver o usage: python insere_repostas.py --help 
'''

import sys
import os
import argparse
import textwrap
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.etl.respostas_forms import parse_formulario
#import servidor.config  # pylint: disable=unused-import
from servidor.bikespserver.gerenciamento import registra_resposta
from servidor.etl.respostas_forms.insere_usuario_google import insere_usuario_google

#pylint: enable=wrong-import-position

def insere_respostas(lista_respostas:list,force_prod:bool,con_inicial="") -> None:  # pylint: disable=missing-function-docstring
	if con_inicial != "":
		con = con_inicial
	else:
		con = conecta(force_prod)

	for resp in lista_respostas:
		registra_resposta.registra_resposta(resp, con,force_prod=force_prod)

	if con_inicial == "":
		con.close()

def parse_args():
	'''
		Realiza o parseamento dos argumentos
	'''
	arg_parser = argparse.ArgumentParser(epilog=textwrap.dedent(
		'''\
         Se ambos argumentos forem omitidos
         o script buscará o valor da variável de ambiente
         CAMINHO_RESPOSTAS_FORMS e prod será = True.
         Se apenas a flag --csv for utiliza, prod será = False.
         '''))

	arg_parser.add_argument("-csv","--csv",
		action='store',required=False,help='Caminho do CSV')
	arg_parser.add_argument("--nao-confirmar",
		action='store_true',required=False,help='Confirmar inserções')
	arg_parser.add_argument("-p","--prod",
		action='store_true',required=False,help='Forçar produção')
	arg_parser.add_argument("-r","--range",
		action='store',required=False,help='Range no formato [X,Y], que inclui X e Y')

	args = arg_parser.parse_args()

	if args.csv is None:
		args.csv = os.path.join(
			str(Path(__file__).absolute().parent.parent),
			str(os.getenv("CAMINHO_RESPOSTAS_FORMS")))
		args.prod = True

	args.prod = bool(args.prod)

	return args

def parse_range(rng):
	'''
		Parse um range no formato X,Y
	'''
	inicio, fim = rng.split(",")
	inicio = int(inicio[1:])
	fim = int(fim[:-1])
	return inicio,fim


def main():
	'''
		Main
	'''
	args = parse_args()

	csv_path = args.csv
	force_prod = args.prod
	rng = args.range
	confirmar = not bool(args.nao_confirmar)

	lista_respostas = parse_formulario.parse(csv_path,force_prod=force_prod)
	if rng is not None:
		try:
			inicio, fim = parse_range(rng)
			lista_respostas = lista_respostas[inicio:fim+1]
		except (IndexError, ValueError):
			print("[ERRO]: Problema com o range",rng)
			sys.exit()

	if confirmar:
		nomes_usuarios = []
		for i in lista_respostas:
			nomes_usuarios.append(i["Nome"]["Nome"])
		print("Serão inseridos " + str(len(nomes_usuarios)) + " usuários.")
		for nome in nomes_usuarios:
			print("\t" + nome)
		confirma = input("Confirma? S/N: ")
		if confirma not in ("S","s"):
			return

	insere_respostas(lista_respostas,force_prod)
	if force_prod:
		insere_usuario_google(force_prod)


if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		pass
