'''
	Insere o usuário correspondente ao google
'''

import sys
import json
import os
from pathlib import Path

#import config #pylint: disable=unused-import

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position
from servidor.bikespserver.bd.insercoes import insere_USUARIO
from servidor.bikespserver.bd.consultas import get_pessoa_info,get_pessoa_bilhete_unico
from servidor.bikespserver.bd.consultas import usuario_existe
from servidor.bikespserver.bd.conecta import conecta

#pylint: enable=wrong-import-position

def insere_usuario_google(force_prod:bool,con_inicial=""):
	'''
		Insere o usuário default do google no banco de dados
	'''
	if con_inicial == "":
		con = conecta(force_prod)
	else:
		con = con_inicial

	cur = con.cursor()

	with open(
		str(Path(__file__).absolute().parent) + "/dados_google.json",
		"r",
		encoding="utf-8") as arquivo_google:

		dados_google = json.loads(arquivo_google.read())

	pessoa = get_pessoa_info(dados_google["CPF"], cur)
	id_pessoa = get_pessoa_bilhete_unico(dados_google["NumBilheteUnico"],cur)

	if pessoa is None or pessoa[1] != dados_google["Email"] \
		or id_pessoa is None or id_pessoa[0] != pessoa[0]:

		raise ValueError("Pessoa Google não cadastrada")

	if usuario_existe(pessoa[0], cur):
		return

	insere_USUARIO({"idPessoa": pessoa[0], "senha": os.getenv("SENHA_GOOGLE")}, cur)
	cur.close()
	if con_inicial == "":
		con.commit()
		con.close()
	return
