'''
	Atualizações nos grupos de pesquisa
'''
import sys
from pathlib import Path

import argparse

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.gerenciamento import gerencia_grupo_pesquisa
from servidor.bikespserver.bd.conecta import conecta

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, insere no BD de teste')
	arg_parser.add_argument("-o","--operacao",
		action='store',choices=['remocao','insercao'],required=True,
		help='Tipo de operação a ser realizada')
	arg_parser.add_argument("-g","--grupo",
		action='store',required=True,help='Id do grupo alvo da operação')
	arg_parser.add_argument("-v","--valor",
		action='store',required=False,
		help='Valor atualizado da remuneração (válido apenas para operações de alteração e inserção)')

	args = arg_parser.parse_args()

	if (args.operacao == "remocao" and args.valor is not None) or \
		(args.operacao == "insercao" and args.valor is None):
		arg_parser.error("Valor é apenas válido para a operação de inserção")
	if not args.grupo.isdigit():
		arg_parser.error("Id do grupo inválido")
	if args.valor is not None:
		try:
			float(args.valor)
		except ValueError:
			arg_parser.error("Valor inválido")
	return args

def remove_grupo(id_grupo:str,con) -> None:
	'''
		Remove grupo de pesquisa
	'''
	gerencia_grupo_pesquisa.remove_grupo_pesquisa(id_grupo,con)

def insere_grupo(id_grupo:str,valor:str|float,con) -> None:
	'''
		Insere grupo de pesquisa
	'''
	gerencia_grupo_pesquisa.insere_grupo_pesquisa(id_grupo,valor,con)


def main():
	'''
		Main
	'''
	args = parse_args()

	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)

	if args.operacao == "remocao":
		remove_grupo(args.grupo,con)
	else:
		insere_grupo(args.grupo,args.valor,con)

	con.close()

if __name__ == "__main__":
	main()
