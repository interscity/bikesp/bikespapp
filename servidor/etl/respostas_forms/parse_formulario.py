'''
	Este script recebe um arquivo csv com as respostas do formulário
	e devolve uma lista de dicionários contendo cada uma das respostas
'''
import sys
import re
import json
import os

from pathlib import Path
from typing import List

import pandas as pd
import numpy as np

# Lista de colunas que são repetidas no csv
colunas_repetidas = [
	"Endereco","ObjetivoLocal","FrequenciaTrajeto","DuracaoTrajeto",
	"ComoFazTrajeto","PossibilidadeBike","TipoEstacao","EstacaoOnibus",
	"EstacaoMetro"
]

# Colunas a serem removidas
colunas_remover = [
	"EstacaoMaisUm","MaisUm","lastpage","startlanguage","seed"
]


def print_usage() -> None:
	'''
		Exibe as instruções para rodar o script
	'''
	print("USAGE: python parse_formulario.py {arquivo contendo as repostas}")
	sys.exit()

def coluna_repetida(nome_coluna):
	'''
		Checa se uma coluna está na lista
		de colunas repetidas
	'''
	for coluna in colunas_repetidas:
		if coluna in nome_coluna or nome_coluna in coluna:
			return coluna
	return False

def coluna_remover(nome_coluna):
	'''
		Checa se uma coluna esta na lista
		de colunas a serem removidas
	'''
	for coluna in colunas_remover:
		if coluna in nome_coluna or nome_coluna in coluna:
			return coluna
	return False

def cria_return_list(dict_interesse: dict): #pylint: disable=too-many-branches
	'''
		Converte o dict com as respostas
		para uma lista
	'''
	return_list = []
	for resposta in dict_interesse:
		resposta_dict = {}
		for chave in resposta:
			if "|" in chave:
				atributo = chave.split("|")[0]
				if "[" in chave:
					nome_chave = atributo.split("[")[0]
					nome_atributo = atributo.split("[")[1][0:-1]
					if nome_chave not in resposta_dict:
						resposta_dict[nome_chave] = {}
					if nome_atributo not in resposta_dict[nome_chave]:
						resposta_dict[nome_chave][nome_atributo] = []
					resposta_dict[nome_chave][nome_atributo].append(resposta[chave])
				else:
					nome_chave = atributo
					if nome_chave not in resposta_dict:
						resposta_dict[nome_chave] = {nome_chave:[]}
					resposta_dict[nome_chave][nome_chave].append(resposta[chave])
			else:
				atributo = chave
				if "[" in chave:
					nome_chave = atributo.split("[")[0]
					nome_atributo = atributo.split("[")[1][0:-1]
					if nome_chave not in resposta_dict:
						resposta_dict[nome_chave] = {}
					resposta_dict[nome_chave][nome_atributo] = resposta[chave]
				else:
					resposta_dict[atributo] = {atributo:resposta[chave]}
		if resposta_dict["DataResposta"]["DataResposta"] != "":
			for idx_numero in range(0,len(resposta_dict["Endereco"]["numero"])):
				try:
					resposta_dict["Endereco"]["numero"][idx_numero] = \
						int(resposta_dict["Endereco"]["numero"][idx_numero])
				except ValueError:
					resposta_dict["Endereco"]["numero"][idx_numero] = 0
			return_list.append(resposta_dict)
	return return_list

def adiciona_pessoa_google(df):
	'''
		Adiciona uma pessoa correspondente ao google (usuário teste)
	'''
	with open(
		str(Path(__file__).absolute().parent) + "/dados_google.json",
		"r",encoding="utf-8") as arquivo_google:
		dados_google = json.loads(arquivo_google.read())

	df = pd.concat([df, pd.DataFrame([dados_google])], ignore_index=True)
	return df

def checa_dados_diferentes(lista_respostas : list) -> None:
	'''
		Checa se existem dados repetidos
		sendo inseridos
	'''
	emails = []
	bilhetes_unicos = []
	cpfs = []
	for resposta in lista_respostas:
		if resposta["Email"] in emails:
			raise ValueError("Email repetido:",resposta["Email"])
		if resposta["CPF"] in cpfs:
			raise ValueError("CPF repetido:",resposta["CPF"])
		if resposta["NumBilheteUnico"] in bilhetes_unicos:
			raise ValueError("Bilhete único repetido:",resposta["NumBilheteUnico"])
		emails.append(resposta["Email"])
		cpfs.append(resposta["CPF"])
		bilhetes_unicos.append(resposta["NumBilheteUnico"])


def cria_tipos_notaveis():
	'''
		Cria dicionario com os tipos notáveis
		(principalmente para representar documentos)
	'''
	tipos_notaveis = {
		'CPF': str,
		'RG':str,
		'NumBilheteUnico':str,
		'Telefone':str
	}
	for i in range(1,6):
		key = "Endereco" + str(i) + "[cep]"
		tipos_notaveis[key] = str
	return tipos_notaveis

def parse(csv_path:str,df="",force_prod=False) -> List:
	'''
		Função que realiza o parsing do csv
	'''

	tipos_notaveis = cria_tipos_notaveis()

	if not isinstance(df,pd.DataFrame):
		df = pd.read_csv(csv_path,dtype=tipos_notaveis)

	df.rename(columns={"submitdate":"DataResposta","id":"IdResposta"},inplace=True)

	for nome_coluna in df.columns:
		busca_chave = re.search(r"\[",nome_coluna)
		if coluna_remover(nome_coluna):
			df.drop(nome_coluna,axis=1,inplace=True)
			continue
		if busca_chave is not None and coluna_repetida(nome_coluna):
			nome_base = coluna_repetida(nome_coluna)
			numero_repeticao = nome_coluna.replace(nome_base,"").split("[")[0]
			nome_atributo = nome_coluna.replace(nome_base,"").split("[")[1][0:-1]
			novo_nome = nome_base + "[" + nome_atributo + "]" + "|" + numero_repeticao
			df.rename(columns={nome_coluna:novo_nome},inplace=True)
			nome_coluna = novo_nome
		if busca_chave is None and coluna_repetida(nome_coluna):
			nome_base = coluna_repetida(nome_coluna)
			numero_repeticao = nome_coluna.replace(nome_base,"").split("[")[0]
			novo_nome = nome_base + "|" + numero_repeticao
			df.rename(columns={nome_coluna:novo_nome},inplace=True)

	df.replace(np.nan,'',regex=True,inplace=True)

	if force_prod:
		df = adiciona_pessoa_google(df)

	dict_interesse = df.to_dict("records")

	return_list = cria_return_list(dict_interesse)
	checa_dados_diferentes(return_list)
	return return_list

if __name__ == "__main__":
	parse(os.getenv('CAMINHO_RESPOSTAS_FORMS'))
