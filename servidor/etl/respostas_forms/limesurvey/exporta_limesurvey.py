'''
	Exporta um formulário do LimeSurvey
	Atenção: o limesurvey limita 3 requests de login seguidas
	com cooldown de 10 minutos.
'''
from urllib.parse import unquote

import requests
from bs4 import BeautifulSoup

DELIMITADOR_CSV = ","

headers = {
	"Host":"survey.ccsl.ime.usp.br",
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0",
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
	"Accept-Language": "pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3",
	"Accept-Encoding": "gzip, deflate, br",
	"Origin":"https://survey.ccsl.ime.usp.br",
}

url_login = "https://survey.ccsl.ime.usp.br/limesurvey/index.php/admin/authentication/sa/login/" # pylint: disable=invalid-name

def salva_string(csv_texto: str,caminho_saida: str):
	'''
		Salva uma string em um arquivo
	'''
	print("Salvando em " + caminho_saida)
	with open(caminho_saida,"w",encoding="utf-8") as arquivo:
		arquivo.write(csv_texto)

def exporta_limesurvey(usuario,senha,survey_id,caminho_saida):
	'''
		Dados usuário,senha,id do formulário e um caminho,
		exporta esse formulário e o salva nesse caminho
	'''
	scraper = LimesurveyScraper(usuario,senha,survey_id,caminho_saida)
	login = scraper.faz_login()
	if not login:
		return False
	scraper.exporta_formulario()
	return True
class LimesurveyScraper():
	'''
		Classe responsável por raspar a instância do
		LimeSurvey contendo as respostas do formulário
		de cadastro
	'''
	def __init__(self,usuario:str,senha:str,survey_id: str,caminho_saida: str):
		self.usuario = usuario
		self.senha = senha
		self.survey_id = survey_id
		if caminho_saida is None:
			caminho_saida = "/tmp/exportLimeSurvey.csv"
		self.caminho_saida = caminho_saida

		self.sessao = requests.Session()
		self.sessao.headers.update(headers)
		self.cookies = {}

		self.url_export = "https://survey.ccsl.ime.usp.br/" + \
			"limesurvey/index.php/admin/export/sa/exportresults/surveyid/" + \
			self.survey_id


	def get_cookie(self,cookie):
		'''
			Pega o valor de um cookie
		'''
		if cookie in self.cookies:
			return self.cookies[cookie]
		return None

	def set_cookie(self,cookie,valor):
		'''
			Seta o valor de um cookie
		'''
		if valor is None:
			del self.cookies[cookie]
		else:
			self.cookies[cookie] = valor

	def set_headers_post_login(self):
		'''
			Seta os headers necessários
			para fazer um POST na URL de login
		'''
		headers_login = self.set_headers_get_login()
		headers_login["Content-Type"] = "application/x-www-form-urlencoded"
		return headers_login

	def set_headers_get_login(self):
		'''
			Seta os headers necessários
			para fazer um GET na URL de login
		'''
		headers_login = dict(headers)
		headers_login["Cookie"] = ""
		for key in self.cookies:
			headers_login["Cookie"] += key + "=" + self.get_cookie(key) + "; "
		headers_login["Referer"] = url_login
		return headers_login

	def set_headers_post_export(self,):
		'''
			Seta os headers necessários
			para fazer um POST na URL de export
		'''
		headers_export = dict(headers)
		headers_export["Cookie"] = ""
		for key in self.cookies:
			headers_export["Cookie"] += key + "=" + self.get_cookie(key) + "; "
		headers_export["Content-Type"] = "application/x-www-form-urlencoded"
		headers_export["Referer"] = self.url_export
		return headers_export

	def faz_login(self) -> dict:
		'''
			Faz as requisições necessárias para logar
			no LimeSurvey
		'''
		r = self.sessao.get(
			url_login,
			headers=headers
		)
		assert r.status_code == 200
		self.set_cookie(
			"LS-AMGLYMZNZGYCSWIE",
			unquote(r.cookies["LS-AMGLYMZNZGYCSWIE"])
		)
		self.set_cookie(
			"YII_CSRF_TOKEN",
			unquote(r.cookies["YII_CSRF_TOKEN"])
		)

		payload = {
			"YII_CSRF_TOKEN":self.get_cookie("YII_CSRF_TOKEN"),
			"authMethod":"Authdb",
			"user":self.usuario,
			"password":self.senha,
			"loginlang":"pt-BR",
			"action":"login",
			"width":"905",
			"login_submit":"login"
		}

		headers_post_login = self.set_headers_post_login()

		r = self.sessao.post(
			url_login,
			data=payload,
			headers=headers_post_login,
			cookies=self.cookies,
			allow_redirects=False
		)
		assert r.status_code == 302
		self.set_cookie(
			"LS-AMGLYMZNZGYCSWIE",
			unquote(r.cookies["LS-AMGLYMZNZGYCSWIE"])
		)
		self.set_cookie(
			"YII_CSRF_TOKEN",
			None
		)

		headers_get_login = self.set_headers_get_login()
		r = self.sessao.get(
			url_login,
			headers=headers_get_login,
			cookies=self.cookies
		)
		assert r.status_code == 200
		self.set_cookie(
			"YII_CSRF_TOKEN",
			unquote(r.cookies["YII_CSRF_TOKEN"])
		)

		if "incorreto" in r.text:
			print("SENHA INCORRETA")
			return {}
		if "excedeu" in r.text:
			print("EXCEDEU LIMITE")
			return {}

		print("Login concluído")
		return self.cookies

	def coleta_info_formulario(self) -> dict:
		'''
			Requisita informações básicas
			do formulário (quantas respostas e
			quais são as colunas)
		'''
		info = {
			"export_from":"",
			"export_to":"",
			"colselect[]":[]
		}
		r = self.sessao.get(
			self.url_export,
			headers=headers,
			cookies=self.cookies
		)
		assert r.status_code == 200
		soup = BeautifulSoup(r.text,'html.parser')

		export_from_html = soup.find(
			'input',
			class_="form-control",
			attrs={'id':"export_from"}
		)
		if export_from_html is not None:
			info["export_from"] = export_from_html["min"]
			info["export_to"] = export_from_html["max"]
			col_select_html = soup.find(
				'select',
				class_="form-control",
				attrs={'id':"colselect"}
			)
			for select in col_select_html.find_all('option'):
				id_coluna = select["data-fieldname"]
				info["colselect[]"].append(id_coluna)
		return info
	def exporta_formulario(self) -> str:
		'''
			Faz as requisições necessárias
			para exportar o formulário do LimeSurvey
		'''
		info = self.coleta_info_formulario()
		payload = {
			"YII_CSRF_TOKEN":self.get_cookie("YII_CSRF_TOKEN"),
			"type":"csv",
			"csvfieldseparator":DELIMITADOR_CSV,
			"completionstate":"complete",
			"exportlang":"pt-BR",
			"export_from":info["export_from"],
			"export_to":info["export_to"],
			"answers":"long",
			"convertyto":"1",
			"convertnto":"2",
			"maskequations":"Y",
			"headstyle":"code",
			"striphtmlcode":"1",
			"headspacetounderscores":"0",
			"abbreviatedtext":"0",
			"emcode":"0",
			"abbreviatedtextto":"15",
			"codetextseparator":".+",
			"sid":self.survey_id,
			"colselect[]":info["colselect[]"]
		}
		headers_post_export = self.set_headers_post_export()
		r = self.sessao.post(
			self.url_export,
			data=payload,
			headers=headers_post_export,
			cookies=self.cookies
		)
		if r.status_code != 200:
			print("Falha no POST de exportação")
			return ""
		csv_exportado = r.text
		salva_string(csv_exportado,self.caminho_saida)
		return csv_exportado
