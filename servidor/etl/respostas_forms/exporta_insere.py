'''
	Script responsável por exportar as respostas do lime survey e inserir os usuários
	que não estão inseridos.
'''

import sys
import os
import argparse

from pathlib import Path
#import config # pylint: disable=unused-import


sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.gerenciamento.obtem_respostas_faltantes import obtem_respostas_faltantes
from servidor.bikespserver.gerenciamento.notifica_pessoas_inseridas \
	import notifica_pessoas_inseridas
from servidor.etl.respostas_forms import parse_formulario
from servidor.etl.respostas_forms.limesurvey.exporta_limesurvey import exporta_limesurvey
from servidor.etl.respostas_forms.insere_repostas import insere_respostas
from servidor.etl.respostas_forms.insere_usuario_google import insere_usuario_google
#pylint: enable=wrong-import-position

def parse_args():
	'''
		Realiza o parseamento dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, Utiliza o BD de teste')
	arg_parser.add_argument("--notificar",
		action='store_true',required=False,
		help='Caso queira enviar email aos usuários avisando que foram inseridos')
	arg_parser.add_argument("-csv","--csv",
		action='store',required=False,help='Caminho do CSV')
	arg_parser.add_argument("-o","--output",
		action='store',required=False,help='Caminho de saída do formulário')
	arg_parser.add_argument("-u","--usuario",
		action='store',required=False,help='Usuário do LimeSurvey')
	arg_parser.add_argument("-s","--senha",
		action='store',required=False,help='Senha do LimeSurvey')
	arg_parser.add_argument("--survey",
		action='store',required=False,help='ID do formulário do LimeSurvey')

	args = arg_parser.parse_args()

	com_csv = args.csv is not None

	if not com_csv and args.senha is None:
		args.senha = os.getenv("SENHA_LIMESURVEY")
	if not com_csv and args.usuario is None:
		args.usuario = os.getenv("USUARIO_LIMESURVEY")

	sem_csv = (args.output is not None or args.survey is not None \
		or args.senha is not None or args.usuario is not None)

	if not (not com_csv or not sem_csv):
		raise argparse.ArgumentError(None,
			"--csv é mutualmente exclusivo com --survey,--usuario,--senha e --output")

	if not com_csv and not sem_csv:
		raise argparse.ArgumentError(args.csv,
			"Forneça um CSV ou a tupla id_survey,usuario,senha,output")

	if sem_csv and (args.output is None or args.survey is None):
		raise argparse.ArgumentError(args.csv,
			"Forneça todos os argumentos necessários (output,id_survey)")		
	return args

def input_confirma():
	'''
		Confirma uma ação do usuário
	'''
	confirma = input("Confirma? S/N: ")
	if confirma in ("S","s"):
		return True
	return False

def confirma_notificacoes(email_usuarios: list)-> bool:
	'''
		Função que mostra aos usuários quem será notificado
		e pede sua confirmação
	'''
	print("Serão notificados " + str(len(email_usuarios)) + " usuários.")
	for email in email_usuarios:
		print("\t" + email)
	return input_confirma()

def confirma_insercoes(nomes_usuarios: list)-> bool:
	'''
		Função que mostra ao usuário quem será inserido
		e pede sua confirmação
	'''
	print("Serão inseridos " + str(len(nomes_usuarios)) + " usuários.")
	for nome in nomes_usuarios:
		print("\t" + nome)

	return input_confirma()

def main():
	'''
		Main
	'''
	args = parse_args()

	csv_path = args.csv
	usuario = args.usuario
	senha = args.senha
	caminho_saida_formulario = args.output
	survey_id = args.survey
	notificar = bool(args.notificar)
	force_dev = bool(args.debug)

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)
	if csv_path is None:
		sucesso_exportacao = exporta_limesurvey(usuario,senha,survey_id,caminho_saida_formulario)
		if not sucesso_exportacao:
			return
		csv_path = caminho_saida_formulario

	lista_respostas = parse_formulario.parse(csv_path,force_prod=not force_dev)

	respostas_faltantes = obtem_respostas_faltantes(lista_respostas,con)

	nomes_usuarios = [resposta['Nome']["Nome"] for resposta in respostas_faltantes]
	email_usuarios = [resposta['Email']["Email"] for resposta in respostas_faltantes]

	confirma = confirma_insercoes(nomes_usuarios)
	if confirma:
		insere_respostas(respostas_faltantes,not force_dev,con_inicial=con)
		if "GOOGLE TESTER" in nomes_usuarios and not force_dev:
			insere_usuario_google(not force_dev,con)
		if notificar and not force_dev:
			confirma = confirma_notificacoes(email_usuarios)
			if confirma:
				print("Notificando usuários...")
				notifica_pessoas_inseridas(email_usuarios)
			else:
				print("ABORTANDO NOTIFICAÇÃO")
				con.close()
				return
		con.commit()
		con.close()
	else:
		print("ABORTANDO INSERÇÃO")
		con.close()

#pylint: disable=duplicate-code
if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		pass
