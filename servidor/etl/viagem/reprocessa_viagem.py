'''
	Reprocessa uma viagem salva no BD
'''
import sys
from pathlib import Path

import argparse

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))

#pylint: disable=wrong-import-position

from servidor.bikespserver.bd.conecta import conecta
from servidor.bikespserver.viagem.validaviagem.reprocessa import reprocessa_viagem

#pylint: enable=wrong-import-position

def parse_args() -> argparse.Namespace:
	'''
		Parsing dos argumentos
	'''
	arg_parser = argparse.ArgumentParser()

	arg_parser.add_argument("-d","--debug",
		action='store_true',required=False,help='Debug mode, utiliza apenas o BD de teste')
	arg_parser.add_argument("-u","--update",
		action='store_true',required=False,help='Fazer a atualização no BD')
	arg_parser.add_argument("-id","--viagem",
		action='store',required=True,help='ID da viagem a ser reprocessada')


	args = arg_parser.parse_args()

	args.update = bool(args.update)

	return args

def main():
	'''
		Main
	'''
	args = parse_args()

	force_dev = bool(args.debug)

	id_viagem = args.viagem

	con = conecta(force_bd_prod=not force_dev,force_bd_dev=force_dev)

	[estado,[viagem_antes,viagem_depois]] = reprocessa_viagem(id_viagem,con)
	for chave,valor in viagem_antes.items():
		if str(valor) != str(viagem_depois[chave]):
			print("Atualização:", chave)
			print("\t" + str(valor),"---->",str(viagem_depois[chave]))
	print("Estado:",estado)

	if args.update:
		if estado == "SUCESSO":
			con.commit()
		else:
			print("A viagem não foi atualizada")

	con.close()

if __name__ == "__main__":
	main()
