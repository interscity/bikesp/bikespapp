'''
	Gera um geojson de um trajeto
'''
import sys
import json
import re

def json_para_geoJSON(trajeto_js): #pylint: disable=invalid-name
	'''
		Converte um trajeto json
		para o geojson correspondente
	'''
	trajeto_js = re.sub(r'""', '"', str(trajeto_js))
	trajeto_js = json.loads(trajeto_js)

	coordenadas = list(
		map(lambda ponto :
			[ponto['Posicao']['longitude'],ponto['Posicao']['latitude']],
			trajeto_js)
	)

	output = {
		"type": "FeatureCollection",
		"features": [
			{
				"type": "Feature",
				"properties": {},
				"geometry": {
					"type": "LineString",
					"coordinates": coordenadas,
				}
			}
		]
	}

	return json.dumps(output)

if __name__ == "__main__":

	if len(sys.argv) < 2:
		print("Uso:\n\tpython",sys.argv[0],"<nome_arquivo_entrada> ")
		sys.exit(1)

	ARQ_NAME = sys.argv[1]

	with open(ARQ_NAME,"r",encoding="utf-8") as arq:

		lido = arq.read()

		print(json_para_geoJSON(lido))
