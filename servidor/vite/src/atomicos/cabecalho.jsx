import React,{useEffect, useState} from "react";

function Cabecalho(props){

	const [windowWidth, setWindowWidth] = useState(window.innerWidth);

	const tamanhoFonte = windowWidth > 600 ? "20pt" : "5vw"

	const textoCabecalho = props.texto

	useEffect(() => {
		function handleResize() {
			setWindowWidth(window.innerWidth);
		}

		window.addEventListener('resize', handleResize);
		return () => window.removeEventListener('resize', handleResize);
	}, []);

	return (
		  <header style={{
			backgroundColor: "#f2eeda",
			display: "flex",
			justifyContent: "center",
			height: "10vh",
			borderBottom: "solid 1vh #c7a80c"
		  }}>
			<h1 style={{fontSize: tamanhoFonte, color: "#000000"}}>{textoCabecalho}</h1>
		  </header>
	);
}

export {Cabecalho};