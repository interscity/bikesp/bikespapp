import React, {useState, findDOMNode} from "react"

function ItemForms (props){

	const rotulo = props.rotulo;
	const prefixo = props.comPrefixo ? (props.fixa ? "F:" : "D:") : "";
	const idItem = prefixo + props.idItem;
	const tipo = props.tipo;

	const valPlaceholder = props.placeholder;
	const tamanhoMaximo = props.maxlength;

	const vertical = props.textoAcima;

	let objEntrada;

	if(tipo == "texto"){
		objEntrada = <input type="text" maxlength={tamanhoMaximo} id={idItem} name={idItem} placeholder={valPlaceholder}/>
	}else if(tipo == "textao"){
		objEntrada = <textarea id={idItem} maxlength={tamanhoMaximo} name={idItem} placeholder={valPlaceholder} 
			style={{
			width:"80%"
		}}/>
	}else if(tipo == "escolha"){
		let escolhas = props.escolhas;
		let opcoes = [];

		for(let i in escolhas){
			let escolha = escolhas[i]
			let idEscolha = idItem + "." + escolha;
			opcoes.push(
				<div style={{width: "30%"}}className="flexBetween">
					<label for={idEscolha}>{escolha}:</label>
					<input type="radio" id={idEscolha} name={idItem} value={escolha}></input>
				</div>
			)
		}

		objEntrada = opcoes

	}else if(tipo == "multipla-escolha"){
		objEntrada = <ItemMultiplaEscolha idItem={idItem} escolhas={props.escolhas}/>
	}

	return(
		<div className={vertical ? "flexColAround" : "flexAround"} style={{
			width:"90%"
		}}>
			{tipo=="escolha" ?
				<h4 style={{
					width:"95%",
					textAlign: "center",
					margin:"0.5em", 
					wordWrap: "break-word"}}>{rotulo}</h4>
			:
				<label style={{
					width:"95%",
					textAlign: "center",
					margin:"0.5em", 
					wordWrap: "break-word"}}
				for={idItem}>{rotulo}</label>
			}
			{objEntrada}
				
		</div>
	)

}

function ItemMultiplaEscolha(props){

	const idItem = props.idItem;
	const escolhas = props.escolhas;

	const [escolhidas,setEscolhidas] = useState([]);

	let atualizarEscolhas = (colocou,escolha) => {
		if(colocou){
			escolhidas.push(escolha);
			setEscolhidas(escolhidas);
		}else{
			for(let i in escolhidas){
				if(escolhidas[i] == escolha){
					escolhidas.splice(i,1);
					setEscolhidas(escolhidas);
				}
			}
		}
		document.getElementById(idItem).value = "[" + String(escolhidas) + "]";
		console.log("Novas Escolhidas:", String(escolhidas));
	}

			
	let objSaida = []
		
	objSaida.push(
		<input type="text" id={idItem} name={idItem} style={{display: 'none'}}></input>
	)

	for (let i in escolhas){
		let escolha = escolhas[i]
		let idOpcao = idItem + "." +  escolha
		objSaida.push(
			<div style={{width: "30%"}}className="flexBetween">
				<label for={idOpcao}> {escolha}: </label>
				<input type="checkbox" id={idOpcao} name={idOpcao} value={escolha}
				onChange={(evento) => {
					atualizarEscolhas(evento.target.checked,escolha)}}/>
			</div>
		)
	}

	return objSaida

}

ItemForms.defaultProps = {
	textoAcima: false,
	tipo: "texto",
	placeholder: "",
	maxlength: 100,
	escolhas:[],
	comPrefixo: true,
	fixa: true,
}

export {ItemForms}
