import React from "react";

function Header (props) {

    const componenteConteudo = props.texto ?
        () => (
            <header>
                {props.texto}
            </header>
        ) :
        () => (
            <header>
                <span class="material-symbols-outlined" id="header__icon">
                    pedal_bike
                    </span>
                <span class="header__title">BikeSP</span>
            </header>
        )

    return componenteConteudo()
    
}

export {Header};