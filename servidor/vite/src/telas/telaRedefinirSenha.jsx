import React, { useState } from "react";
import { Header } from "../atomicos/header";

export const TelaRedefinirSenha = (props) => {
	let senha = '';
	let confirmaSenha = '';

	function verificaSenha () {
		if (senha != confirmaSenha)
			alert("As senhas não coincidem.");
	}
	
    return (
        <div>
            <Header />
            <div className="redefinir__container">
                <form
                    id="form"
                    method="POST"
                    encType="multipart/form-data"
                    action={acaoSubmit}
                >
                    <h1 className="redefinir__title">Redefinição de senha:</h1>
                    <label className="redefinir__label" htmlFor="nova_senha">Nova Senha:</label>
                    <input
                        className="redefinir__input"
                        type="password"
                        id="nova_senha"
                        name="nova_senha"
                        onChange={(e) => {
							senha = e.target.value;
                        }}
						onBlur={(e) => {
							if (e.target.value.length < 8)
								alert("A senha precisa ter ao menos 8 caracteres.");
						}}
                    />
                    <label className="redefinir__label" htmlFor="confirma_senha">Confirma Senha:</label>
                    <input
                        className="redefinir__input"
                        type="password"
                        id="confirma_senha"
                        name="confirma_senha"
                        onChange={(e) => {
							confirmaSenha = e.target.value;
                        }}
						onBlur={verificaSenha}
                    />
					<input type="hidden" name="token" value={token}/>
                    <input type="submit" id="submit" value="Redefinir"/>		
                </form>
            </div>
        </div>
    );
}
