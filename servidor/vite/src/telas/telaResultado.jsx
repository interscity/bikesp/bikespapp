import React, { useState } from "react";

import { Header } from "../atomicos/header";

function TelaResultado(props){

	const fracasso = erro != "";

	return (
		<div style={{
			backgroundColor: "#f6f7e9"
		}}>
			<Header texto={titulo}/>
			<div className="flexCenter" style={{
				height: "89vh",
				width: "100vw"
			}}>
				{fracasso ?
					<h2 style={{color:"#7f0e0f"}}>{erro}</h2>
				:
					<h2>{sucesso}</h2>
				}
			</div>
		</div>
	);
}

export {TelaResultado}