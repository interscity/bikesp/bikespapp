import React, { useState } from "react";

import { Header } from "../atomicos/header";
import { ItemForms } from "../atomicos/itemForms";

function TelaFeedbackViagem(props){

	const textoViagem = idViagem ? "(#" + idViagem + ")" : "";

	return (
		  <div style={{
			backgroundColor: "#f6f7e9"
		  }}>
			<Header texto={"Feedback: Última Viagem " + textoViagem}/>

		  	<div className="flexCenter" style={{
				height: "89vh",
				width: "100vw"
			}}>

				<form id="form" 
					className="flexColAround"
					style={{
						minHeight: "70%",
						minWidth: "50vw"
					}}
					action={acaoSubmit} method="POST" enctype="multipart/form-data">

					<ItemForms idItem="cpf" comPrefixo={false} maxlength={20} rotulo="Digite seu CPF:"  textoAcima={true} placeholder="123.456.789-01"/> 

					<ItemForms idItem="tempoClima" textoAcima={true} tipo="escolha"
					rotulo="Como você classificaria as condições climáticas durante sua viagem?"
					escolhas={["Chuvoso","Nublado","Ensolarado"]}
					/>

					<ItemForms idItem="outrosApps" maxlength={200} textoAcima={true} tipo="textao"
					rotulo="Você estava usando outros aplicativos durante essa viagem? (Se sim, quais?):"/>

					<ItemForms idItem="feedbackGeral" maxlength={1000} textoAcima={true} tipo="textao"
					rotulo="Fique à vontade para registrar comentários gerais sobre a viagem ou o app:"/>

					<ItemForms idItem="modoAviao" tipo="escolha" fixa={false} maxlength={1000} textoAcima={true}
					rotulo="Você fez essa viagem com o modo avião ligado?"
					escolhas={["Sim","Não"]}
					/>
					
					<input type="hidden" name="token" value={token}/>
					<input type="hidden" name="idViagem" value={idViagem}/>

					<input type="submit" id="submit" value="Enviar"/>

				</form>

			</div>

		  </div>
	);
}

export {TelaFeedbackViagem};