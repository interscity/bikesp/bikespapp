import React from 'react';
import { Header } from '../atomicos/header';

const TelaTrocaBU = () => {
	return (
		<div>
			<Header />
			<div className="troca__container">
				<form
					id="form"
					method="POST"
					encType="multipart/form-data"
					action={acaoSubmit}
				>
					<h1 className="troca__title"> Troca de bilhete único: </h1>
					<div className="troca__description-container">
						<span className="troca__description">
							Lembre-se: Esta operação não poderá ser desfeita e não é permitido reutilizar um bilhete único antigo. Além disso, não nos responsabilizamos por trocas indevidas.
						</span>
					</div>
					<label className="troca__label" htmlFor="cpf">CPF:</label>
						<input
							className="troca__input"
							type="text"
							id="cpf"
							name="cpf"
						/>
					<label className="paginaTrocaBUtroca__label" htmlFor="novo-bu">Novo bilhete único:</label>
						<input
							className="troca__input"
							type="text"
							id="novo_bu"
							name="novo_bu"
						/>
					<input type="hidden" name="token" value={token}/>
					<input type="submit" id="submit" value="Trocar" />
				</form>
			</div>
		</div>  
	);
};

export { TelaTrocaBU };
