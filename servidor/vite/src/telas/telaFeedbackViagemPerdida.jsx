import React, { useState } from "react";

import { Header } from "../atomicos/header";
import { ItemForms } from "../atomicos/itemForms";

function TelaFeedbackViagemPerdida(props){

	return (
		  <div style={{
			backgroundColor: "#f6f7e9"
		  }}>

			<Header texto="Feedback: Viagem Perdida"/>

		  	<div className="flexCenter" style={{
				height: "89vh",
				width: "100vw"
			}}>

				<form id="form" 
					className="flexColAround"
					style={{
						minHeight: "70%",
						minWidth: "50vw"
					}}

					action={acaoSubmit} method="POST" enctype="multipart/form-data">

					<ItemForms idItem="cpf" comPrefixo={false} maxlength={20} rotulo="Digite seu CPF:"  textoAcima={true} placeholder="123.456.789-01"/> 

					<ItemForms idItem="quandoPerdeu"  maxlength={1000} textoAcima={true} tipo="textao"
					rotulo="Descreva como descobriu que sua viagem foi perdida:"/>

					<ItemForms idItem="algoDiferente"  maxlength={1000} textoAcima={true} tipo="textao"
					rotulo="Você lembra de ter feito algo diferente nesta viagem?:"
					/>

					<ItemForms idItem="verApp" textoAcima={true}  maxlength={20} rotulo="Qual a sua versão do app?"
					placeholder="v0.x.y"/>

					<ItemForms idItem="feedbackGeral"  maxlength={1000} textoAcima={true} tipo="textao"
					rotulo="Fique à vontade para registrar comentários gerais sobre a viagem ou o app:"/>

					<input type="hidden" name="token" value={token}/>

					<input type="submit" id="submit" value="Enviar"/>

				</form>

			</div>

		  </div>

	);
}

export {TelaFeedbackViagemPerdida};