import "./src/styles.css";
import { Testo } from "./src/teste";
import { TelaAdmin } from "./src/telas/telaAdmin";
import { TelaFeedbackViagem } from "./src/telas/telaFeedbackViagem";
import { TelaFeedbackViagemPerdida } from "./src/telas/telaFeedbackViagemPerdida";
import { TelaResultado } from "./src/telas/telaResultado";

import { createRoot } from 'react-dom/client';
import {TelaRedefinirSenha} from "./src/telas/telaRedefinirSenha";
import { TelaTrocaBU } from "./src/telas/telaTrocaBU";

// // Clear the existing HTML content
// document.body.innerHTML = '<div id="app"></div>';

// Render your React component instead
let rootElement = document.getElementById('app');
if(rootElement != null){
	let tela = rootElement.classList[0];
	const root = createRoot(rootElement);
	if(tela == "paginaAdm"){
		root.render(TelaAdmin());
	}else if(tela == "paginaFeedbackViagem"){
		root.render(TelaFeedbackViagem());
	}else if(tela == "paginaFeedbackViagemPerdida"){
		root.render(TelaFeedbackViagemPerdida());
	}else if(tela == "paginaResultado"){
		root.render(TelaResultado());
	}else if (tela == "paginaRedefinirSenha") {
		root.render(TelaRedefinirSenha());
	} else if (tela == "paginaTrocaBU") {
		root.render(TelaTrocaBU());
	}else{
		root.render(Testo());
	}
}
