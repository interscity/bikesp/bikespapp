package com.bikesp.geoLocator.cores.vanilla;

import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Handler;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.util.Log;

import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

import com.bikesp.geoLocator.GeoLocator;
import com.bikesp.geoLocator.cores.GeolocatorCore;
import com.bikesp.geoLocator.cores.vanilla.PrecisionTweaker;


public class VanillaCore extends GeolocatorCore implements LocationListener {
	
	private Executor executor;
	public LocationManager lmngr;
	private PrecisionTweaker precisionTweaker;

	protected List<String> preferedProviders = new ArrayList<String>();

	private long trackingIntervalTimeout;
	private float trackingDistanceTolerance;

	private final String FALLBACK_PROVIDER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ? LocationManager.FUSED_PROVIDER : LocationManager.GPS_PROVIDER;
	private String activeProvider = "";

	public static boolean waitingForSinglePos = false;
	private boolean currentlyTracking = false;

	public VanillaCore(GeoLocator parent){
		super(parent);

		if (Build.VERSION.SDK_INT >= 31){
			preferedProviders.add(LocationManager.FUSED_PROVIDER);
		}

		preferedProviders.add(LocationManager.GPS_PROVIDER);

		if (Build.VERSION.SDK_INT >= 29 && ActivityCompat.checkSelfPermission(parent, android.Manifest.permission.ACCESS_FINE_LOCATION) == android.content.pm.PackageManager.PERMISSION_GRANTED) {
			preferedProviders.add(LocationManager.PASSIVE_PROVIDER);
		}

		preferedProviders.add(LocationManager.NETWORK_PROVIDER);

	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	public void start(Looper looper){
		this.looper = looper;
		this.executor = ContextCompat.getMainExecutor(this.parent);

		// Obtain location manager
		this.lmngr = (LocationManager)this.parent.getSystemService(this.parent.LOCATION_SERVICE);

		// START PRECISION TWEAKER
		precisionTweaker = new PrecisionTweaker(new Handler(looper),this.executor,this);

		precisionTweaker.run();
	}

	public void stop(){
		this.precisionTweaker.stop();
		this.looper.quit();
	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	public void beginTracking(long intervalTimeout, float distanceTolerance){
		this.trackingIntervalTimeout = intervalTimeout;
		this.trackingDistanceTolerance = distanceTolerance;
		updateLocationUpdates(this.getPreferredProvider());
	}

	public void stopTracking(){
		this.lmngr.removeUpdates(this);
	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	public void getUrgentNewPosition(){
		VanillaCore.waitingForSinglePos = true;
		for(String provider : this.preferedProviders){
			subscribeToSinglePosition(provider);
		}
	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	public void getSinglePosition(){
		if (!VanillaCore.waitingForSinglePos){
			VanillaCore.waitingForSinglePos = true;
			subscribeToSinglePosition(this.getPreferredProvider());
		}
	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	private void subscribeToSinglePosition(String requestedProvider){
		if (Build.VERSION.SDK_INT >= 30){
			this.lmngr.getCurrentLocation(requestedProvider,null,
			ContextCompat.getMainExecutor(this.parent),
			new Consumer<Location>() {
			@Override
			public void accept(Location location) {
				if(VanillaCore.waitingForSinglePos){
					VanillaCore.this.parent.registerSingleNewPos(location);
					VanillaCore.waitingForSinglePos = false;
				}
			}});
		}else{
			this.lmngr.requestSingleUpdate(requestedProvider,
			new LocationListener() {
				@Override
				public void onLocationChanged(Location location) {
					if(VanillaCore.waitingForSinglePos){
						VanillaCore.this.parent.registerSingleNewPos(location);
						VanillaCore.waitingForSinglePos = false;
					}
				}
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras){
					return;
				}

				@Override
   				public void onProviderDisabled(String provider) {
					return;
				}

				@Override
   				public void onProviderEnabled(String provider) {
					return;
				}
	
			}
			,null);
		}
	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	private void updateLocationUpdates(String provider){
		this.lmngr.removeUpdates(this);

		Log.d("NATIVE GEOLOCATION", "Updating provider to " + provider);

		this.activeProvider = provider;

		this.lmngr.requestLocationUpdates(provider,
		this.trackingIntervalTimeout,
		this.trackingDistanceTolerance,
		this);
	}

	private String getPreferredProvider(){

		for (String thisProvider : preferedProviders) {
			if(this.lmngr.isProviderEnabled(thisProvider)){
				Log.d("NATIVE GEOLOCATION", "Provider " + thisProvider + " is enabled");
				this.parent.updateGPSState(true);
				return thisProvider;
			}
		}

		//  If none of the knwon providers are enabled / avaliable, 
		// try using any of the avaliable ones. If that fails, return
		// the FALLBACK_PROVIDER

		List<String> providers = this.lmngr.getProviders(true);

		if(providers.isEmpty()){
			Log.w("NATIVE GEOLOCATION", "NO GPS ENABLED FOR USER TRACKING");
			this.parent.updateGPSState(false);
			return this.FALLBACK_PROVIDER;
		}else{
			return providers.get(0);
		}

	}

	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
	public void updatePreferences(String nextProvider) {
		// String nextProvider = getBestEvaluated();

		if(nextProvider == null){
			return;
		}

		int i = 0;
		
		for(i = 0; i < preferedProviders.size(); i++){
			if(preferedProviders.get(i) == nextProvider){
				break;
			}
		}

		if(i == 0){
			return;
		}

		for(int j = i-1; j >= 0; j-=1){
			preferedProviders.set(j+1, preferedProviders.get(j));
		}

		preferedProviders.set(0, nextProvider);

		updateLocationUpdates(getPreferredProvider());
	}

	// Inherited from Location Listener:

	@Override
    public void onLocationChanged(Location loc) {
    	// Log.d("NATIVE GEOLOCATION", "Treating new loc sample: " + loc);
    	this.parent.registerNewPosition(loc);
	}

	@Override
	@RequiresPermission(anyOf = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
    public void onProviderDisabled(String provider) {
    	
		Log.d("NATIVE GEOLOCATION", "Provider Disabled");

		if(!provider.equals(this.activeProvider)){
			return;
		}

		String nextProvider = this.getPreferredProvider();

		if(!nextProvider.equals(this.FALLBACK_PROVIDER)){
			updateLocationUpdates(nextProvider);
		}

		if(!this.lmngr.isProviderEnabled(nextProvider)){
			this.parent.registerNewPosition(null);
		}
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		return;
	}

	public void enablePowerSavings(){};

	public void disablePowerSavings(){};

}
