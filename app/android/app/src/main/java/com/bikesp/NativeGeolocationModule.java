package com.bikesp;

import android.os.Build;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.location.Location;
import android.location.LocationManager;
import android.os.PowerManager;
import android.provider.Settings;

import android.Manifest;

import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.modules.permissions.PermissionsModule;
import com.facebook.react.bridge.JavaOnlyArray;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.bridge.PromiseImpl;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.ArrayList;
import java.util.Date;
import java.lang.Math;
import java.util.Map;
import java.util.HashMap;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

import com.bikesp.geoLocator.GeoLocator;

// Traduzir tudo pro inglês!!!

public class NativeGeolocationModule extends ReactContextBaseJavaModule {

   //private static final GeoLocator geoLocator = new GeoLocator();
   private Callback callbackAviso;

   public static ReactApplicationContext reactContext;

   private static boolean gpsAvaliable = true;

   private static ArrayList<Callback> singlePosCallbacks = new ArrayList<Callback>();

   NativeGeolocationModule(ReactApplicationContext context) {
      super(context);
      this.reactContext = context;
   }

   @Override
   public String getName() {
      return "NativeGeolocation";
   }

   @ReactMethod(isBlockingSynchronousMethod = true)
   public boolean isIgnoringBatteryOptimizations(){
      // Log.d("NativeGeolocation + GEOLOCATOR NATIVO :","is gps enabled? " + NativeGeolocationModule.gpsAvaliable);
      PowerManager pm = (PowerManager) this.reactContext.getSystemService(Context.POWER_SERVICE);
      return pm.isIgnoringBatteryOptimizations(this.reactContext.getPackageName());
   }

   @ReactMethod
   public void promptBatteryOptimizations(){
      // Log.d("NativeGeolocation + GEOLOCATOR NATIVO :","is gps enabled? " + NativeGeolocationModule.gpsAvaliable);
      Intent promptBatteryIntent = new Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);//, Uri.parse("package:"+this.reactContext.getPackageName()));
      promptBatteryIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

      this.reactContext.startActivity(promptBatteryIntent);
   }

   @ReactMethod(isBlockingSynchronousMethod = true)
   public boolean isGPSAvaliable(){
      // Log.d("NativeGeolocation + GEOLOCATOR NATIVO :","is gps enabled? " + NativeGeolocationModule.gpsAvaliable);
      return NativeGeolocationModule.gpsAvaliable;
   }

   @ReactMethod(isBlockingSynchronousMethod = true)
   public boolean isCurrentlyTracking(){
      // Log.d("NativeGeolocation + GEOLOCATOR NATIVO :","is gps enabled? " + NativeGeolocationModule.gpsAvaliable);
      return GeoLocator.getIsCurrentlyTracking();
   }

   @ReactMethod
   public void trackPosition(int refreshTimeout, Float distanceTolerance){
      // Começa a captura

      //Enviar mensagem ao broadcast receiver do GeoLocator
      Intent trackIntent = new Intent(this.reactContext,GeoLocator.class);
      trackIntent.setAction("bikeSPApp.intent.BEGIN_TRACKING");
      trackIntent.putExtra("com.bikesp.geoLocator.timeout", (long) refreshTimeout);
      trackIntent.putExtra("com.bikesp.geoLocator.distTol", distanceTolerance);
      this.reactContext.startForegroundService(trackIntent);
      Log.d("NATIVE GEOLOCATION", "Mandei track intent para" + "com.app.geoLocator.GeoLocator");
   }

   @ReactMethod
   public void getSinglePosition(Callback resultCallback) {
      NativeGeolocationModule.singlePosCallbacks.add(resultCallback);

      Intent getSinglePosIntent = new Intent(this.reactContext,GeoLocator.class);
      getSinglePosIntent.setAction("bikeSPApp.intent.GPS_NEW_LOC");
      this.reactContext.startService(getSinglePosIntent);
      Log.d("NATIVE GEOLOCATION", "Sent new loc request to" + "com.app.geoLocator.GeoLocator");
   }

   @ReactMethod
   public void getUrgentPosition(Integer staleTimeout, Callback resultCallback) {
       if (ActivityCompat.checkSelfPermission(reactContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(reactContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("NativeGeolocation + GEOLOCATOR NATIVO :", "Permissão de localização recusada");
           return;
       }

       Location latest = this.getLatestPosition(staleTimeout);

      if(latest == null){
         NativeGeolocationModule.singlePosCallbacks.add(resultCallback);

         Intent getSinglePosIntent = new Intent(this.reactContext,GeoLocator.class);
         getSinglePosIntent.setAction("bikeSPApp.intent.GPS_URGENT_NEW_LOC");
         this.reactContext.startService(getSinglePosIntent);
         Log.d("NativeGeolocation + GEOLOCATOR NATIVO :", "Sent urgent new loc request to" + "com.app.geoLocator.GeoLocator");
      }else{
         resultCallback.invoke(locationToMap(latest));
      }
   }

   @ReactMethod(isBlockingSynchronousMethod = true)
   public WritableMap getLastKnownPosition(Integer staleTimout){
       if (ActivityCompat.checkSelfPermission(reactContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(reactContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return locationToMap(null);
       }

       return locationToMap(this.getLatestPosition(staleTimout));
   }


   @RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
   private Location getLatestPosition(Integer staleTimout){
      String[] providers = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) // FUSED_PROVIDER só disponível a partir do SDK 31
              ? new String[]{LocationManager.FUSED_PROVIDER, LocationManager.NETWORK_PROVIDER, LocationManager.PASSIVE_PROVIDER, LocationManager.GPS_PROVIDER}
              : new String[]{LocationManager.NETWORK_PROVIDER, LocationManager.PASSIVE_PROVIDER, LocationManager.GPS_PROVIDER};

      LocationManager locationManager = (LocationManager) this.reactContext.getSystemService(Context.LOCATION_SERVICE);

      Location location = null;

      Date now = new Date();

      for(int i = 0; i < providers.length; i+=1){

         Location newLocation = locationManager.getLastKnownLocation(providers[i]);

         if(newLocation != null && (location == null || (newLocation.getTime() > location.getTime() && newLocation.getAccuracy() <= 100.0))){
            if(staleTimout == null || Math.abs(now.getTime() - newLocation.getTime()) <= staleTimout*1000){
               location = newLocation;
            }
         }

      }
   
      return location;
   }

   @ReactMethod
   public void stopTracking(){
      // Encerra a captura
      Intent stopTrackIntent = new Intent(this.reactContext,GeoLocator.class);
      stopTrackIntent.setAction("bikeSPApp.intent.STOP_TRACKING");
      this.reactContext.startForegroundService(stopTrackIntent);
      Log.d("NATIVE GEOLOCATION", "Mandei stopTrack intent para" + "com.app.geoLocator.GeoLocator");
   }

   @ReactMethod
   public void changeGoogleServicesCoreStatus(boolean newStatus){
      Intent changeCoreIntent = new Intent(this.reactContext,GeoLocator.class);
      changeCoreIntent.setAction("bikeSPApp.intent.CHANGE_CORE");
      changeCoreIntent.putExtra("com.bikesp.geoLocator.newCoreStatus", (boolean) newStatus);
      this.reactContext.startService(changeCoreIntent);
   }

   @ReactMethod
   public void enablePowerSavings(){
		Intent enableSavingsIntent = new Intent(this.reactContext,GeoLocator.class);
      enableSavingsIntent.setAction("bikeSPApp.intent.ENABLE_SAVINGS");
      this.reactContext.startService(enableSavingsIntent);
	}

	@ReactMethod
	public void disablePowerSavings(){
		Intent disableSavingsIntent = new Intent(this.reactContext,GeoLocator.class);
      disableSavingsIntent.setAction("bikeSPApp.intent.DISABLE_SAVINGS");
      this.reactContext.startService(disableSavingsIntent);
	}

   @ReactMethod
   public void addListener(String eventName){

   }
   @ReactMethod
   public void removeListeners(Integer count){
      
   }

   // Auxiliar
   private static void emiteEvento(String nomeEvento, WritableMap params) {
      if(NativeGeolocationModule.reactContext != null){
         NativeGeolocationModule.reactContext
         .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
         .emit(nomeEvento, params);
      }
   }

   public static void registerSingleNewPos(Location loc){
      for(int i = NativeGeolocationModule.singlePosCallbacks.size() -1; i >= 0; i--){
         Callback callback = NativeGeolocationModule.singlePosCallbacks.get(i);
         
         callback.invoke(locationToMap(loc));

         NativeGeolocationModule.singlePosCallbacks.remove(i);
      }
   }

   public static void registraNovoLoc(Location loc){
      NativeGeolocationModule.emiteEvento("NewLocation",locationToMap(loc));
   }

   public static WritableMap locationToMap(Location loc){
      WritableMap params = Arguments.createMap();

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); 

      if (loc == null){
         params.putDouble("lng",-1);
         params.putDouble("lat",-1);
         params.putDouble("acc",-1);
         params.putDouble("spd",-1);
         params.putString("timeStamp",sdf.format(new java.util.Date()));
         return params;
      }

      params.putDouble("lng", loc.getLongitude());
      params.putDouble("lat", loc.getLatitude());

      Date date = new java.util.Date(loc.getTime()); 
      // the format of your date
      sdf.setTimeZone(TimeZone.getTimeZone("GMT-3")); 
      String formattedDate = sdf.format(date);

      if(loc.hasAccuracy()){
         params.putDouble("acc",loc.getAccuracy());
      }else{
         params.putDouble("acc",-1.0);
      }

      params.putDouble("spd", loc.getSpeed());

      params.putString("timeStamp", formattedDate);

      return params;
   }

   public static void updateGPSState(boolean newState){
      NativeGeolocationModule.gpsAvaliable = newState;
   }

}