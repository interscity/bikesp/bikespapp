package com.bikesp.geoLocator.cores.vanilla;

import android.Manifest;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;

import java.util.concurrent.Executor;
import java.util.function.Consumer;

import com.bikesp.geoLocator.cores.vanilla.VanillaCore;

public class PrecisionTweaker implements Runnable{

	private final long precisionTweakInterval = 1000 * 60 * 1;

	private class ProviderScore {
		private short waitingTicks = 0;
		private int points = 0;

		private String provider;

		public ProviderScore(String provider){
			this.provider = provider;
		}

		public int getScore(){
			return this.points;
		}

		@RequiresPermission(anyOf = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION})
		public void reevaluate(LocationManager lmngr, Executor executor){

			points = 0;
			if(!lmngr.isProviderEnabled(this.provider)){
				points = -1;
				waitingTicks = 0;
			}else if(waitingTicks == 0){
				Location lastLoc = lmngr.getLastKnownLocation(this.provider);
				waitingTicks = 1;
				if (Build.VERSION.SDK_INT >= 30){
					lmngr.getCurrentLocation(this.provider,null,
						executor,
						new Consumer<Location>() {
						@Override
						public void accept(Location newLoc) {
							points = nextScore(lastLoc, newLoc);
							points = (int)Math.round(points/Math.max(5*waitingTicks, 5));
							waitingTicks = 0;
						}});
				}else{
					lmngr.requestSingleUpdate(this.provider,
					new LocationListener() {
						@Override
						public void onLocationChanged(Location newLoc) {
							points = nextScore(lastLoc, newLoc);
							points = (int)Math.round(points/Math.max(waitingTicks, 1));
							waitingTicks = 0;
						}
						@Override
						public void onStatusChanged(String provider, int status, Bundle extras){
							return;
						}
						@Override
						public void onProviderDisabled(String provider) {
							return;
						}
		
						@Override
						public void onProviderEnabled(String provider) {
							return;
						}
		 
					}
					,null);
				}
			}else{
				waitingTicks += 1;
			}
		}

		private int nextScore(Location oldLoc, Location newLoc){
			if(newLoc == null){
				return -1;
			}

			if(oldLoc != null && (oldLoc.getLongitude() == newLoc.getLongitude() || 
				oldLoc.getLatitude() == newLoc.getLatitude())){
				return 1;
			}

			if(newLoc.hasAccuracy()){
				return (int)Math.round(10000.00 - (newLoc.getAccuracy()*newLoc.getAccuracy()) );
			}else{
				return 2;
			}
			
		}
	}

	private LocationManager lmngr;

	@Nullable
	private ProviderScore fused;
	private ProviderScore passive;
	private ProviderScore network;
	private ProviderScore gps;

	private VanillaCore parent;
	private Executor executor;
	private Handler taskHandler;

	public PrecisionTweaker(Handler handler, Executor executor, VanillaCore parent){
		this.parent = parent;
		this.lmngr = parent.lmngr;
		this.executor = executor;
		this.taskHandler = handler;

		fused = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ? new ProviderScore(LocationManager.FUSED_PROVIDER) : null;
		passive = new ProviderScore(LocationManager.PASSIVE_PROVIDER);
		network = new ProviderScore(LocationManager.NETWORK_PROVIDER);
		gps = new ProviderScore(LocationManager.GPS_PROVIDER);
	}

	private String getBestEvaluated(){
		int fusedPoints = fused != null ? fused.getScore() : 0;
		int passivePoints = passive.getScore();
		int networkPoints = network.getScore();
		int gpsPoints = gps.getScore();

		if(fusedPoints > passivePoints && fusedPoints > networkPoints && fusedPoints > gpsPoints && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
			return LocationManager.FUSED_PROVIDER;
		}else if(networkPoints > fusedPoints && networkPoints > passivePoints && networkPoints > gpsPoints){
			return LocationManager.NETWORK_PROVIDER;
		}else if(passivePoints > fusedPoints && passivePoints > networkPoints && passivePoints > gpsPoints){
			return LocationManager.PASSIVE_PROVIDER;
		}else if(gpsPoints > fusedPoints && gpsPoints > networkPoints && gpsPoints > passivePoints){
			return LocationManager.GPS_PROVIDER;
		}else{
			return null;
		}
	}

	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION})
	private void tweakPrecision() {
		// Log.d("NATIVE GEOLOCATION", "Tweaking Precision");

		this.parent.updatePreferences(getBestEvaluated());

		if (fused != null) fused.reevaluate(this.lmngr, this.executor);
		passive.reevaluate(this.lmngr, this.executor);
		network.reevaluate(this.lmngr, this.executor);
		gps.reevaluate(this.lmngr, this.executor);

	}

	@Override
	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION})
	public void run() {
		tweakPrecision();
		this.taskHandler.postDelayed(this, precisionTweakInterval);
	}

	public void stop() {
		this.taskHandler.removeCallbacks(this);
	}

};