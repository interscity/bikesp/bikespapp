package com.bikesp.activityRecognition;

import java.io.Serializable;
import java.util.Date;
import com.google.android.gms.location.DetectedActivity;
public class ActivityRecognitionData implements Serializable {

    private Date dateOfActivity;
    private String typeOfActivity;
    private int confidence;

    public ActivityRecognitionData(int typeOfActivity, int confidence) {
        this.confidence = confidence;
        this.typeOfActivity = getNameOfActivity(typeOfActivity);
        dateOfActivity = new Date();
    }

    private String getNameOfActivity(int type) {
        switch (type) {
            case DetectedActivity.IN_VEHICLE:
                return "EM_VEICULO";
            case DetectedActivity.ON_BICYCLE:
                return "NA_BICICLETA";
            case DetectedActivity.ON_FOOT:
                return "A_PE";
            case DetectedActivity.RUNNING:
                return "CORRENDO";
            case DetectedActivity.STILL:
                return "PARADO";
            case DetectedActivity.WALKING:
                return "CAMINHANDO";
            default:
                return "DESCONHECIDO";
        }
    }

    public Date getDate() {
        return dateOfActivity;
    }

    public String getTypeOfActivity() {
        return typeOfActivity;
    }

    public int getConfidenceLevel() {
        return confidence;
    }
}
