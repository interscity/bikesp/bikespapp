package com.bikesp.activityRecognition;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.DetectedActivity;
import android.widget.Toast;
import android.util.Log;
public class ActivityTransitionReceiver extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {
        // Verifica se a intent possui um resultado e extrai os dados do resultado,
        // escrevendo tudo em um arquivo no final.
        Log.d("Activity Recognition", "Atividade detectada");
        ActivityRecognitionManager manager = ActivityRecognitionManager.getInstance();
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            DetectedActivity activity = result.getMostProbableActivity();
            manager.addActivityDataToTrip(activity);
            Log.d("Activity Recognition", "Atividade:" + activity.toString());

        }
    }    
}
