package com.bikesp.activityRecognition;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionRequest;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.DetectedActivity;

import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;

import com.bikesp.R;

/**
 * Classe para gerenciar o Activities Recognition.
 * Essa classe é uma singleton. Ela é instanciada logo quando a aplicação se inicia.
 * Para iniciar o rastreamento das atividades o usuário deve chamar o metodo startTracking().
 * Para encerrar o rastreamento o método endTracking() deve ser chamado. Note que é essencial que um
 * rastreamento iniciado seja encerrado para liberar recursos do celular e delimitar as informações de cada viagem.
 * 
 * O método getLastTrip retorna a ultima viagem feita. Se nenhuma viagem foi feita antes durante a execução do app
 * o método retorna null.
 * O método getCurrentTrip retorna a viagem atual se o método startTracking foi chamado. Se ele não foi chamado
 * uma lista vazia é retornada.
 */
public class ActivityRecognitionManager {

    public static final String ACTION_USER_RECOGNITION = "bikeSPApp.intent.ACTIVITY_TRANSITION";
    public static final int UPDATE_INTERVAL = 5000;
    private static ActivityRecognitionManager instance;
    private List<ActivityTransition> transitions;
    private List<ActivityRecognitionData> trip;
    private List<ActivityRecognitionData> lastTrip;
    private boolean isTracking;
    private Context context;
    private ActivityTransitionRequest request;
    private ActivityRecognitionData lastMeasure;
    private PendingIntent pendingIntent;

    private ActivityRecognitionManager(Context context) {
        transitions = new ArrayList<ActivityTransition>();
        this.context = context;
        isTracking = false;
        configDefaultTransitions();
        trip = null;
        lastMeasure = null;
        Intent intent = new Intent(ACTION_USER_RECOGNITION);

        // TODO: Precisa de versão 33 do Android
        context.registerReceiver(new ActivityTransitionReceiver(), new IntentFilter(ACTION_USER_RECOGNITION), Context.RECEIVER_NOT_EXPORTED);

        pendingIntent = PendingIntent.getBroadcast(
                context,
                0,
                intent,
                PendingIntent.FLAG_IMMUTABLE
        );

        request = new ActivityTransitionRequest(transitions);
    }

    public static ActivityRecognitionManager initManager(Context context) {
        if (instance == null) {
            synchronized (ActivityRecognitionManager.class) {
                if(instance == null){
                    instance = new ActivityRecognitionManager(context);
                }
            }
        }

        return instance;
    }

    public static ActivityRecognitionManager getInstance() {
        if (instance == null) return null;
        return instance;
    }

    private void configDefaultTransitions() {
        transitions.add(
                new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.ON_BICYCLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build());

        transitions.add(
                new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.ON_BICYCLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());

        transitions.add(
                new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());

        transitions.add(
            new ActivityTransition.Builder()
            .setActivityType(DetectedActivity.WALKING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build());

        transitions.add(
                new ActivityTransition.Builder()
                .setActivityType(DetectedActivity.STILL)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build());

        transitions.add(
            new ActivityTransition.Builder()
            .setActivityType(DetectedActivity.STILL)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build());
    }
    /**
     * Método para iniciar o rastreamento. Após chamar este método,
     * lembre-se de chamar o método endTracking para encerrar o rastreamento.
     */
    @RequiresPermission(Manifest.permission.ACTIVITY_RECOGNITION)
    public synchronized void startTracking() {
        if (isTracking) return;
        Log.d("ActivityRecog", "Iniciando");

        isTracking = true;
        lastMeasure = null;
        trip = new ArrayList<ActivityRecognitionData>();

        ActivityRecognition.getClient(context).requestActivityUpdates(UPDATE_INTERVAL, pendingIntent)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            public void onSuccess(Void aVoid) {
                Log.d("ActivityRecog", "Transitions Api was successfully registered.");
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            public void onFailure(Exception e) {
                Log.d("ActivityRecog", "Transitions could not be registered: " + e);
            }
        });
    }

    /**
     * Método para encerrar o rastreamento.
     */
    @RequiresPermission(Manifest.permission.ACTIVITY_RECOGNITION)
    public synchronized void endTracking() {
        if (!isTracking) return;
        Log.d("ActivityRecog", "Finalizando");
        isTracking = false;
        lastTrip = trip;

        Task<Void> task = ActivityRecognition.getClient(context).removeActivityTransitionUpdates(pendingIntent);

        task.addOnSuccessListener(
            new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void result) {
                    trip = null;
                }
            }
        );

        task.addOnFailureListener(
            new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.e("ActivityRecog", e.getMessage());
                    trip = null;
                }
            }
        );
    }

    public synchronized boolean isTracking(){
        return isTracking;
    }

    public synchronized void addActivityDataToTrip(DetectedActivity activity) {
        ActivityRecognitionData measure = new ActivityRecognitionData(activity.getType(), activity.getConfidence());
        if (lastMeasure != null) {
            if (( dateDiff(measure.getDate(), lastMeasure.getDate()) < UPDATE_INTERVAL) && (measure.getTypeOfActivity() == lastMeasure.getTypeOfActivity())) {
                lastMeasure = measure;
                return;
            }
        }
        lastMeasure = measure;

        if (trip != null){
            trip.add(measure);
        }
    }
    
    /*
     * Retorna a diferença entre duas datas. Nesse caso, d1 deve ser a data
     * mais nova.
     */
    private long dateDiff(Date d1, Date d2) {
        return d1.getTime() - d2.getTime();
    }

    /**
     * Método para pegar a ultima viagem feita. Se nenhuma viagem foi feita durante a execução
     * do programa o método retorna null. Se há uma viagem para retornar, o método retorna
     * uma lista de ActivityRecognitionData.
     */
    public List<ActivityRecognitionData> getLastTrip() {
        return lastTrip;
    }
    /**
     * Método para pegar a viagem em andamento. Se não há nenhuma viagem em andamento, retorna null.
     */
    public synchronized List<ActivityRecognitionData> getCurrentTrip() {
        if (trip == null){
            return new ArrayList<>();
        }
        List<ActivityRecognitionData> copy = new ArrayList<>(trip);
        return copy;
    }

    public ActivityRecognitionData getLastMeasure() {
        return lastMeasure;
    }
    
}
