package com.bikesp.geoLocator.cores.googleservices;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.CurrentLocationRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Granularity;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnCompleteListener;

import java.util.concurrent.Executor;

import com.bikesp.geoLocator.GeoLocator;
import com.bikesp.geoLocator.cores.GeolocatorCore;


public class GoogleServicesCore extends GeolocatorCore implements LocationListener  {
	
	private long locationsPerBatch = 12;

	protected FusedLocationProviderClient fusedLocationClient;
	private Context context;
	private Executor executor;

	protected long trackingIntervalTimeout;
	protected float trackingDistanceTolerance;
	
	public GoogleServicesCore(GeoLocator parent){
		super(parent);
		this.context = parent;
	}

	public void start(Looper looper){
		this.looper = looper;
		this.executor = ContextCompat.getMainExecutor(this.parent);

		fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.context);
	}

	public void stop(){
		this.looper.quit();
	}

	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION})
	public void beginTracking(long intervalTimeout, float distanceTolerance){

		this.trackingIntervalTimeout = intervalTimeout;
		this.trackingDistanceTolerance = distanceTolerance;

		LocationRequest locationRequest;

		if (this.parent.isPowerSavingOn()){
			locationRequest = new LocationRequest.Builder(intervalTimeout)
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
				.setMaxUpdateDelayMillis(locationsPerBatch*intervalTimeout).build();
		}else{
			locationRequest = new LocationRequest.Builder(intervalTimeout)
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).build();
		}

        this.fusedLocationClient.requestLocationUpdates(locationRequest,(LocationListener) this,this.looper);
	}

	public void stopTracking(){
		this.fusedLocationClient.flushLocations();
		GoogleServicesCore.this.fusedLocationClient.removeLocationUpdates(GoogleServicesCore.this);
	}

	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	private void resetCurrentTrackingSession(){
		this.fusedLocationClient.flushLocations().addOnCompleteListener(this.executor, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> result) {
				new java.util.Timer().schedule(
					new java.util.TimerTask() {
						@Override
						public void run() {
							GoogleServicesCore.this.fusedLocationClient.removeLocationUpdates(GoogleServicesCore.this);
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
								Toast.makeText(context, "Permissão de localização recusada", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            GoogleServicesCore.this.beginTracking(GoogleServicesCore.this.trackingIntervalTimeout,GoogleServicesCore.this.trackingDistanceTolerance);
						}
					}, 
					500 
				);
				
            }
		});
	}

	@Override
    public void onLocationChanged(Location loc) {
    	// Log.d("NATIVE GEOLOCATION", "Treating new loc sample: " + loc);
    	this.parent.registerNewPosition(loc);
	}

	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public void getUrgentNewPosition(){
		CurrentLocationRequest request = new CurrentLocationRequest.Builder()
		.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
		.setMaxUpdateAgeMillis(10)
		.setGranularity(Granularity.GRANULARITY_FINE)
		.build();

		fusedLocationClient.getCurrentLocation(request,null)
        .addOnSuccessListener(this.executor, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
				GoogleServicesCore.this.parent.registerSingleNewPos(location);
            }
        });
	}

	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public void getSinglePosition(){
		this.getUrgentNewPosition();
	}


	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public void enablePowerSavings(){
		if(this.parent.getIsCurrentlyTracking()){
			this.resetCurrentTrackingSession();
		}
	};


	@RequiresPermission(anyOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public void disablePowerSavings(){
		if(this.parent.getIsCurrentlyTracking()){
			this.resetCurrentTrackingSession();
		}
	};

	public FusedLocationProviderClient getClient(){
		return this.fusedLocationClient;
	}

}
