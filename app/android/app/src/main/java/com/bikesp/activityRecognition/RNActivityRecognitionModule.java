package com.bikesp.activityRecognition;


import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableArray;

import java.util.List;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

public class RNActivityRecognitionModule extends ReactContextBaseJavaModule {

    ActivityRecognitionManager manager;

    public RNActivityRecognitionModule(ReactApplicationContext context) {
        super(context);
        manager = ActivityRecognitionManager.getInstance();
    }

    @NonNull
    @Override
    public String getName() {
        return "RNActivityRecognitionModule";
    }
    
    @ReactMethod
    public void startTracking() {

        if (ActivityCompat.checkSelfPermission(getReactApplicationContext(), Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Toast.makeText(getReactApplicationContext(), "Permissão do ActivityRecognition recusada", Toast.LENGTH_SHORT).show();
            }
            Log.w("ActivityRecognition", "Permissão do ActivityRecognition recusada, falha ao iniciar o tracking");
            return;
        }
        manager.startTracking();
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public void endTracking() {
        if (ActivityCompat.checkSelfPermission(getReactApplicationContext(), Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Toast.makeText(getReactApplicationContext(), "Permissão do ActivityRecognition recusada", Toast.LENGTH_SHORT).show();
            }
            Log.w("ActivityRecognition", "Permissão do ActivityRecognition recusada, falha ao parar o tracking");
            return;
        }
        manager.endTracking();
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public boolean isTracking() {
        return manager.isTracking();
    }

   @ReactMethod(isBlockingSynchronousMethod = true)
    public WritableArray getLastTrip() {
        List<ActivityRecognitionData> trip = manager.getLastTrip();
        return arrayListToWritableArray(trip);
    }

    
    @ReactMethod(isBlockingSynchronousMethod = true)
    public WritableArray getCurrentTrip() {
        List<ActivityRecognitionData> trip = manager.getCurrentTrip();
        return arrayListToWritableArray(trip);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public WritableMap getLastMeasure() {
        ActivityRecognitionData data = manager.getLastMeasure();
        return acitivityRecogDataToWritableMap(data);
    }

    private WritableMap acitivityRecogDataToWritableMap(ActivityRecognitionData data) {
        WritableNativeMap m = new WritableNativeMap();
        m.putString("date", data.getDate().toString());
        m.putString("type", data.getTypeOfActivity());
        m.putInt("confidence", data.getConfidenceLevel());

        return m;
    }
    private WritableArray arrayListToWritableArray(List<ActivityRecognitionData> trip) {
        WritableArray tripToSendToReact = Arguments.createArray();
        if (trip != null) {
            for (ActivityRecognitionData ac : trip) {
                WritableMap m = acitivityRecogDataToWritableMap(ac);
                tripToSendToReact.pushMap(m);
            }
        }

        return tripToSendToReact;
    }
}
