package com.bikesp.geoLocator;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.app.Service;
import android.app.PendingIntent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;

import android.os.Build;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Looper;
import android.os.Process;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.bikesp.R;
import com.bikesp.MainActivity;
import com.bikesp.NativeGeolocationModule;

import com.bikesp.geoLocator.cores.GeolocatorCore;
import com.bikesp.geoLocator.cores.vanilla.VanillaCore;
import com.bikesp.geoLocator.cores.googleservices.GoogleServicesCore;

public class GeoLocator extends Service{
	
	private static boolean IS_GOOGLE_SERVICES_DEFAULT = true;

	public LocationManager lmngr;

	private static ArrayList<Integer> threadIds = new ArrayList<Integer>();

	private GeolocatorCore core;

	private static boolean powerSavingOn = true;

	private static boolean currentlyTracking = false;
	private static long trackingIntervalTimeout;
	private static float trackingDistanceTolerance;

	public GeoLocator() {
	    super();

		if(IS_GOOGLE_SERVICES_DEFAULT && isGooglePlayServicesAvailable(NativeGeolocationModule.reactContext)){
			this.core = new GoogleServicesCore(this);
		}else{
			this.core = new VanillaCore(this);
		}
		
	    Log.d("NATIVE GEOLOCATION", "Geolocator Class Instantiated");
	}

	private Notification createFSNotification(){
		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent =
		        PendingIntent.getActivity(this, 0, notificationIntent,
		                PendingIntent.FLAG_IMMUTABLE);
		String NOTIFICATION_CHANNEL_ID = "com.app.notifsInternas";
	    String channelName = "NotificacoesInternas";

	    NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
	    chan.setLightColor(Color.GREEN);
	    chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
		chan.setImportance(NotificationManager.IMPORTANCE_MIN);
	    NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    assert manager != null;
	    manager.createNotificationChannel(chan);

		Log.d("NATIVE GEOLOCATION", "Creating Notification");

		Notification notification =
		          new Notification.Builder(this,NOTIFICATION_CHANNEL_ID)
		    .setContentTitle(getText(R.string.foreground_notif_title))
		    .setContentText(getText(R.string.foreground_notif_text))
		    .setSmallIcon(R.drawable.iconenotif)
		    .setContentIntent(pendingIntent)
            .setCategory(Notification.CATEGORY_SERVICE)
		    //.setTicker(getText(R.string.ticker_text))
		    .build();

		return notification;

	}

	@Override
	public void onCreate() {
		// Start up the thread running the service. Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block. We also make it
		// background priority so CPU-intensive work doesn't disrupt our UI.
		HandlerThread thread = new HandlerThread("ServiceStartArguments",
		        Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		core.start(thread.getLooper());

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		// Message msg = serviceHandler.obtainMessage();
		// msg.arg1 = startId;

		if (intent == null){
			return START_STICKY;
		}

		if (intent.getAction() == "bikeSPApp.intent.BEGIN_TRACKING"){
			GeoLocator.trackingIntervalTimeout = intent.getLongExtra("com.bikesp.geoLocator.timeout",0);
			GeoLocator.trackingDistanceTolerance = intent.getFloatExtra("com.bikesp.geoLocator.distTol",3);

			GeoLocator.threadIds.add(startId);
			GeoLocator.this.beginTracking(GeoLocator.trackingIntervalTimeout,GeoLocator.trackingDistanceTolerance);

			// msg.arg2 = 1;	
		}
		else if (intent.getAction() == "bikeSPApp.intent.STOP_TRACKING"){
			GeoLocator.this.stopTracking();
			// msg.arg2 = 0;
		}else if(intent.getAction() == "bikeSPApp.intent.GPS_NEW_LOC"){
			this.core.getSinglePosition();
			
			// msg.arg2 = 3;
		}else if(intent.getAction() == "bikeSPApp.intent.GPS_URGENT_NEW_LOC"){
			this.core.getUrgentNewPosition();
		}
		else if(intent.getAction() == "android.location.MODE_CHANGED"){
			// msg.arg2 = 2;
		}else if(intent.getAction() == "bikeSPApp.intent.CHANGE_CORE"){
			boolean newGoogleCoreStatus = intent.getBooleanExtra("com.bikesp.geoLocator.newCoreStatus",IS_GOOGLE_SERVICES_DEFAULT);

			if(newGoogleCoreStatus){
				enableGoogleServicesCore();
			}else{
				enableVanillaCore();
			}
		}else if(intent.getAction() == "bikeSPApp.intent.ENABLE_SAVINGS"){
			this.enablePowerSavings();
		}else if(intent.getAction() == "bikeSPApp.intent.DISABLE_SAVINGS"){
			this.disablePowerSavings();
		}else{
			Log.d("NATIVE GEOLOCATION", "intent:" + intent);
		}
		// serviceHandler.sendMessage(msg);
		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
	  // We don't provide binding, so return null
	  return null;
	}


	@Override
	public void onTaskRemoved(Intent i){
		super.onTaskRemoved(i);
		stopSelf();
	}

	@Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        Log.d("NATIVE GEOLOCATION", "Destroying service");
        this.stopTracking();
		this.core.stop();
    }

	private boolean isGooglePlayServicesAvailable(Context context){
		if(context == null){
			return true;
		}
		GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
		return resultCode == ConnectionResult.SUCCESS;  
	}

	private void enableGoogleServicesCore(){
		if(this.core instanceof GoogleServicesCore){
			Log.d("NATIVE GEOLOCATION", "Core wont change, google services already enabled");
		}else if(!isGooglePlayServicesAvailable(this)){
			Log.d("NATIVE GEOLOCATION", "Core wont change, google services cant be enabled");
		}else{
			if(currentlyTracking){
				core.stopTracking();
			}
			core.stop();

			this.core = new GoogleServicesCore(this);

			HandlerThread thread = new HandlerThread("ServiceStartArguments",
		        Process.THREAD_PRIORITY_BACKGROUND);
			thread.start();

			core.start(thread.getLooper());

			if(currentlyTracking){
				core.beginTracking(GeoLocator.trackingIntervalTimeout,GeoLocator.trackingDistanceTolerance);
			}
			Log.d("NATIVE GEOLOCATION", "Changing core to Google Play Services");
		}
	}

	private void enableVanillaCore(){
		if(this.core instanceof VanillaCore){
			Log.d("NATIVE GEOLOCATION", "Core wont change, vanilla core already enabled");
		}else{
			if(currentlyTracking){
				core.stopTracking();
			}
			core.stop();

			this.core = new VanillaCore(this);

			HandlerThread thread = new HandlerThread("ServiceStartArguments",
		        Process.THREAD_PRIORITY_BACKGROUND);
			thread.start();

			core.start(thread.getLooper());

			if(currentlyTracking){
				core.beginTracking(GeoLocator.trackingIntervalTimeout,GeoLocator.trackingDistanceTolerance);
			}
			Log.d("NATIVE GEOLOCATION", "Changing core to Vanilla");
		}
	}

	public void beginTracking(long intervalTimeout, float distanceTolerance) {
		Log.d("NATIVE GEOLOCATION", "Beginning User Tracking");

		// START FOREGROUND SERVICE
		Notification fsNotification = this.createFSNotification();
		Log.d("NATIVE GEOLOCATION", "Notification Created");
		// Notification ID cannot be 0.

		// Inicia o serviço de forma segura
		// Para versões superiores a TIRAMISU, é necessário passar o tipo de serviço
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
			startForeground(1, fsNotification, ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION);
		} else {
			startForeground(1, fsNotification);
		}

		Log.d("NATIVE GEOLOCATION", "Service Created");

		this.currentlyTracking = true;
		this.core.beginTracking(intervalTimeout,distanceTolerance);
	}

	public void stopTracking() {
		Log.d("NATIVE GEOLOCATION", "Stopping User Tracking");
		this.currentlyTracking = false;
		
		this.core.stopTracking();

		stopForeground(true);
		Log.d("NATIVE GEOLOCATION","Alive threads:" + GeoLocator.threadIds.size());
		// Log.d("NATIVE GEOLOCATION","Alive thread:" + this.threadIds.get(0));

		for(int i = GeoLocator.threadIds.size() -1; i >= 0; i--){
			Integer threadId = GeoLocator.threadIds.get(i);
			stopSelf(threadId);
			GeoLocator.threadIds.remove(i);
		}

		stopSelf();

		Log.d("NATIVE GEOLOCATION","Alive threads:" + GeoLocator.threadIds.size());

	}

	public void registerSingleNewPos(Location newLoc){
		NativeGeolocationModule.registerSingleNewPos(newLoc);
	}

	public void registerNewPosition(Location newLoc){
		NativeGeolocationModule.registraNovoLoc(newLoc);
	}

	public void updateGPSState(boolean newState){
		NativeGeolocationModule.updateGPSState(newState);
	}

	public static boolean getIsCurrentlyTracking(){
		return currentlyTracking;
	}

	public boolean isPowerSavingOn(){
		return powerSavingOn;
	}

	public void enablePowerSavings(){
		if(powerSavingOn){
			return;
		}
		powerSavingOn = true;
		this.core.enablePowerSavings();
	}

	public void disablePowerSavings(){
		if(!powerSavingOn){
			return;
		}
		powerSavingOn = false;
		this.core.disablePowerSavings();
	}
}
