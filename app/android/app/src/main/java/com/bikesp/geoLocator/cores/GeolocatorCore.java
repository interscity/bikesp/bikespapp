package com.bikesp.geoLocator.cores;

import android.os.Looper;

import com.bikesp.geoLocator.GeoLocator;

public abstract class GeolocatorCore {
	
	public final GeoLocator parent;

	protected Looper looper;

	public GeolocatorCore(GeoLocator parent){
		this.parent = parent;
	}

	abstract public void start(Looper looper);

	abstract public void stop();

	abstract public void beginTracking(long intervalTimeout, float distanceTolerance);

	abstract public void stopTracking();

	abstract public void getUrgentNewPosition();

	abstract public void getSinglePosition();

	abstract public void enablePowerSavings();

	abstract public void disablePowerSavings();
}
