# BikeSP - App

## Instalação

Para este aplicativo, é apenas necessário instalar o Node JS >= 14 e o Android Studio, porém, recomendamos seguir as etapas descritas em [React Native Tutorial](https://reactnative.dev/docs/environment-setup)

### Setup do Ambiente de Desenvolvimento

É necessário povoar o diretório app/ com um arquivo .env
As chaves necessárias no arquivo .env são (atualmente):

```
REACT_APP_PK_MAPBOX (chave pública da API do Mapbox)
REACT_APP_SK_MAPBOX (chave privada da API do Mapbox)
SERVER_HOST=http://x (endereço do servidor)
ENCRIPT_PK= (chave de criptografia constante) [PODE OMITIR]
CHAVE_INTEGRIDADE= (chave para hash de integridade)
```

### Setup do dispositivo móvel

Para visualizar esse aplicativo, é necessário um Android (ou um emulador). As instruções para a configuração correta do dispostivo estão contidas em [Running On Device](https://reactnative.dev/docs/running-on-device)


## Execução

Após instalar e configurar tudo e estando em bikespapp/app, basta rodar:

`npx react-native start` 

e em outro terminal

`npx react-native run-android`

## Testando (Testes E2E)


### Debug
Depois de atualizar as dependências com `npm install`, faça o _build_ da versão de testes:

`npx detox build --configuration android.att.debug` (Para executar no dispositivo conectado)

ou

`npx detox build --configuration android.emu.debug` (Para executar no emulador)

---

Para executar a bateria de testes, rode em um terminal

`SERVER_HOST='http://'$(hostname -I | awk '{print $1}')':59118' npx react-native start --reset-cache` 

E em outro rode:

`npx detox test --configuration android.att.debug` (Para executar no dispositivo conectado)

ou

`npx detox test --configuration android.emu.debug` (Para executar no emulador)

### Release

Se desejar fazer o teste com a versão de release em vez da debug, rode os mesmos comandos substituindo `.debug` por `.release`. Nesse caso, também é necessário que o .env do aplicativo esteja contendo o servidor de testes. Para isso, substitua a entrada SERVER_HOST para a saída deste comando: 

`echo 'http://'$(hostname -I | awk '{print $1}')':59118'`
