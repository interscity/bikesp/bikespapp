/**
 * @format
 */

import React, { useContext, useEffect } from "react";
import { AppRegistry } from "react-native";
import App from "./src/App";
import {name as appName} from "./app.json";
import { PaperProvider } from "react-native-paper";
import notifee from "@notifee/react-native";
import messaging from "@react-native-firebase/messaging";
import { contextoGerenciadorNotifis } from "./src/contextos/contextoGerenciadorNotifs";

export default function Main() {
	const gNotif = useContext(contextoGerenciadorNotifis);

	useEffect(() => {
		messaging().onMessage(async (remoteMessage) => {
			console.log("Mensagem FCM em primeiro plano: ", remoteMessage.data.title);
			await gNotif.mostraNotificacaoRemota(remoteMessage.data);
		});
		
		messaging().setBackgroundMessageHandler(async (remoteMessage) => {
			console.log("Mensagem FCM em segundo plano: ", remoteMessage.data.title);
			await gNotif.mostraNotificacaoRemota(remoteMessage.data);
		});
		
		notifee.onBackgroundEvent(async (data) => {
			console.log("Evento em segundo plano: ", JSON.stringify(data));
			
		});

		messaging().registerDeviceForRemoteMessages().then(async () => {
			console.log("Dispositivo registrado para mensagens remotas!");
			return await messaging().getToken();
		}).then((token) => {
			console.log("Token FCM: ", token);
		}).catch((error) => {
			console.log("Erro ao registrar dispositivo para notificações no FCM:\n", error);
		});
	});

	return (
		<PaperProvider>
			<App />
		</PaperProvider>
	);
}

AppRegistry.registerComponent(appName, () => Main);
