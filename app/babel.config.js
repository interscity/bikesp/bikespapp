module.exports = {
	presets: ["module:metro-react-native-babel-preset"],
	plugins: [
		[
			"module:react-native-dotenv",
			{
				moduleName: "@env",
				path: "../.env",
				allowList: [
					"SERVER_HOST",
					"REACT_APP_PK_MAPBOX",
					"CHAVE_INTEGRIDADE",
					"ENCRIPT_PK"
				],
				safe: false,
				allowUndefined: false,
			},
		],
	],
};
