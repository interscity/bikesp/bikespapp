import "react-native";
import React from "react";
import fetch from "node-fetch";
import {FLASK_ADDR} from "../testSetup/testUtils.js"

import {endpointCadastro,endpointLogin,
		endpointLogout,endpointRegistraViagem,
		endpointContestaViagem,endpointChecaSessao,
		endpointRegTokenNotificao} from "../../src/gerenciadores/conexaoServidor.js";


it ("server up",async ()=> {
	let status = 0;
	await fetch(FLASK_ADDR)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).toBe(200);
});


it ("endpoint inexistente down",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/endpointQueNaoExiste"
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).toBe(404);
});


it ("cadastro up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/" + endpointCadastro;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});

it ("login up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/" + endpointLogin;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});

it ("logout up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/" + endpointLogout;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});

it ("registraViagem up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/" + endpointRegistraViagem;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});

it ("contestaViagem up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/" + endpointContestaViagem;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});

it ("checaSessao up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + "/" + endpointChecaSessao;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});

it ("registraTokenNotif up",async ()=> {
	let status = 0;
	let url = FLASK_ADDR + endpointRegTokenNotificao;
	await fetch(url)
		.then(
			(response) => {
				status = response.status;
		});

	expect(status).not.toBe(404);
});