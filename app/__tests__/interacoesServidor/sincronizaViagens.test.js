import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";
import {produzTrajeto} from "../testSetup/testUtils.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";

import {DistanciaHaversine} from "../../src/classes/utils.js";

it ("sincronização correta",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let [respViagem,token] = await enviaViagem(new Date(2023,8,30,7));
	expect(respViagem[1].motivoStatus).toBe("APROVADO");
	let idViagem = respViagem[1].idViagem;

	let respostaSincroniza = await conexao.sincronizaViagens(CPF,token);
	expect(respostaSincroniza[0]).toBe(true);
	expect(respostaSincroniza[1].viagens).not.toBe(null);
	expect(respostaSincroniza[1]["viagens"].length).toBe(1);
		expect(respostaSincroniza[1]["viagens"][0].length).toBe(7);
	let respostaFaltante = await conexao.viagensFaltantes(CPF,token,["1"]);
	console.log(respostaFaltante[0])
	expect(respostaFaltante[0]).toBe(true);
	expect(respostaFaltante[1]["viagensFaltantes"]).not.toBe(null);
	expect(respostaFaltante[1]["viagensFaltantes"].length).toBe(1);
	expect(respostaFaltante[1]["viagensFaltantes"][0].length).toBe(10);

}, MAX_JEST_TIMEOUT);



async function enviaViagem(dataInicio){
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let velocidadeMetrosSegundo = 30 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = 50;

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	return [resposta,token];
}