import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";


it ("cadastro incorreto não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let respostaErro = await conexao.tentaCadastro("email","cpf","bu","senha");
	expect(respostaErro[0]).toBe(false);
	respostaErro = await conexao.tentaCadastro("testador@email.c","12345678910","bu","senha");
	expect(respostaErro[0]).toBe(false);
	respostaErro = await conexao.tentaCadastro("email","12345678910","12345678910","senha");
	expect(respostaErro[0]).toBe(false);
},MAX_JEST_TIMEOUT);

it ("cadastro correto funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let respostaAcerto = await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	expect(respostaAcerto[0]).toBe(true);
},MAX_JEST_TIMEOUT);

it ("cadastro repetido não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let respostaAcerto = await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	let respostaErro = await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	expect(respostaErro[0]).toBe(false);
	respostaErro = await conexao.tentaCadastro(EMAIL,CPF,BU,"outraSenha");
	expect(respostaErro[0]).toBe(false);
	respostaErro = await conexao.tentaCadastro("email",CPF,BU,"outraSenha");
	expect(respostaErro[0]).toBe(false);
},MAX_JEST_TIMEOUT);

it ("cadastro com maiúscula funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let respostaAcerto = await conexao.tentaCadastro("TEstador@EMAIL.c",CPF,BU,SENHA);
	expect(respostaAcerto[0]).toBe(true);
},MAX_JEST_TIMEOUT);