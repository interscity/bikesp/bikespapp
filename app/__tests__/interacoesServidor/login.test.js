import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";

it ("login repetido funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	let token1 = respostaAcerto[1]["token"]
	respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	let token2 = respostaAcerto[1]["token"];
	expect(token1).not.toBe(token2);
	expect(respostaAcerto[1]["CPF"]).toBe(CPF);
},MAX_JEST_TIMEOUT);


it ("login funcionando após primeiro erro",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaErro = await conexao.tentaLogin(EMAIL,"senhaerrada");
	expect(respostaErro[0]).toBe(false);
	let respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	
},MAX_JEST_TIMEOUT);
it ("login com senha incorreta não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaErro = await conexao.tentaLogin(EMAIL,"senhaerrada");
	expect(respostaErro[0]).toBe(false);
	respostaErro = await conexao.tentaLogin("email",SENHA);
	expect(respostaErro[0]).toBe(false);

},MAX_JEST_TIMEOUT);

it ("login funcionando de primeira",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);	
},MAX_JEST_TIMEOUT);


it ("login email maiúsculo funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
},MAX_JEST_TIMEOUT);

it ("login email maiúsculo com cadastro maiúsculo",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
	
	respostaAcerto = await conexao.tentaLogin(EMAIL,SENHA);
	expect(respostaAcerto[0]).toBe(true);
},MAX_JEST_TIMEOUT);
