import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";



it ("validaSessao token errado não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	
	let respostaErro = await conexao.validaSessao(EMAIL,"tokenErrado");
	let teveErro = respostaErro[0];
	let mensagemErro = respostaErro[1];
	expect(teveErro).toBe(false);
	expect(mensagemErro).toBe("Sessao Invalida")
},MAX_JEST_TIMEOUT);

it ("validaSessao token certo funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];
	
	let respostaAcerto = await conexao.validaSessao(EMAIL,token);
	expect(respostaAcerto[0]).toBe(true);
},MAX_JEST_TIMEOUT);


it ("validaSessao email errado não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];
	
	let respostaErro = await conexao.validaSessao("email",token);
	let teveErro = respostaErro[0];
	let mensagemErro = respostaErro[1];
	expect(teveErro).toBe(false);
	expect(mensagemErro).toBe("Sessao Invalida")
},MAX_JEST_TIMEOUT);

it ("validaSessao após logout não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];
	await conexao.logout(EMAIL,token);
	let respostaErro = await conexao.validaSessao(EMAIL,token);
	let teveErro = respostaErro[0];
	let mensagemErro = respostaErro[1];
	expect(teveErro).toBe(false);
	expect(mensagemErro).toBe("Sessao Invalida")
},MAX_JEST_TIMEOUT);