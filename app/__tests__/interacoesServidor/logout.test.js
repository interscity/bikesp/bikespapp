import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";

it ("logout sem estar logado",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);

	let respostaErro = await conexao.logout(EMAIL,"token");
	expect(respostaErro[0]).toBe(false);
},MAX_JEST_TIMEOUT);

it ("logout funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let respostaAcerto = await conexao.logout(EMAIL,token);
	expect(respostaAcerto[0]).toBe(true);
},MAX_JEST_TIMEOUT);


it ("logout token errado não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	
	let respostaErro = await conexao.logout(EMAIL,"tokenErrado");
	expect(respostaErro[0]).toBe(false);
},MAX_JEST_TIMEOUT);

it ("logout email errado não funcionando",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];
	
	let respostaErro = await conexao.logout("email",token);
	expect(respostaErro[0]).toBe(false);
},MAX_JEST_TIMEOUT);