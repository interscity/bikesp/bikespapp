import {FLASK_ADDR, TEST_FLASK_PORT, MAX_JEST_TIMEOUT} from "../testSetup/testUtils.js"

import conexao from "../../src/gerenciadores/conexaoServidor.js";

it ("ping server up",async ()=> {
	conexao.SERVER_HOST = FLASK_ADDR;
	
	let resposta = await conexao.ping();
	expect(resposta[0]).toBe(true);
},MAX_JEST_TIMEOUT);

it ("ping server down",async ()=> {
	conexao.SERVER_HOST = "http://loremipsum:" + TEST_FLASK_PORT;

	let resposta = await conexao.ping();
	expect(resposta[0]).toBe(false);
},MAX_JEST_TIMEOUT);
