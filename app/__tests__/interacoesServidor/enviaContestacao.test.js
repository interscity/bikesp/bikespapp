import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";
import {produzTrajeto} from "../testSetup/testUtils.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";

import moment from 'moment';

import {Trajeto} from "../../src/classes/trajeto.js";
import {DistanciaHaversine} from "../../src/classes/utils.js";

it ("contestação correta",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let [respViagem,token] = await enviaViagemRapida();
	expect(respViagem[1].motivoStatus).toBe("NAO_BICICLETA");
	let idViagem = respViagem[1].idViagem;
	let objContestacao = {
		"idViagem":idViagem,
		"justificativa":"ola marilene",
		"data":new Date()
	};
	let respostaContestacao = await conexao.enviaContestacao(CPF,token,objContestacao);
	expect(respostaContestacao[0]).toBe(true);
}, MAX_JEST_TIMEOUT);

it ("contestação com justificativa vazia",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let [respViagem,token] = await enviaViagemRapida();
	expect(respViagem[1].motivoStatus).toBe("NAO_BICICLETA");
	let idViagem = respViagem[1].idViagem;
	let objContestacao = {
		"idViagem":idViagem,
		"justificativa":"",
		"data":new Date()
	};
	let respostaContestacao = await conexao.enviaContestacao(CPF,token,objContestacao);
	expect(respostaContestacao[0]).toBe(false);
}, MAX_JEST_TIMEOUT);

it ("contestação com idViagem errado",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let [respViagem,token] = await enviaViagemRapida();
	expect(respViagem[1].motivoStatus).toBe("NAO_BICICLETA");
	let idViagem = respViagem[1].idViagem;
	let objContestacao = {
		"idViagem":-1,
		"justificativa":"ola marilene",
		"data":new Date()
	};
	let respostaContestacao = await conexao.enviaContestacao(CPF,token,objContestacao);
	expect(respostaContestacao[0]).toBe(false);
}, MAX_JEST_TIMEOUT);

it ("contestação data inválida",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	let [respViagem,token] = await enviaViagemRapida();
	expect(respViagem[1].motivoStatus).toBe("NAO_BICICLETA");
	let idViagem = respViagem[1].idViagem;
	let objContestacao = {
		"idViagem":idViagem,
		"justificativa":"ola marilene",
		"data":""
	};
	let respostaContestacao = await conexao.enviaContestacao(CPF,token,objContestacao);
	expect(respostaContestacao[0]).toBe(false);
}, MAX_JEST_TIMEOUT);




async function enviaViagemRapida(){
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(2023,8,30,7);
	let velocidadeMetrosSegundo = 300 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = 50;

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	return [resposta,token];
}