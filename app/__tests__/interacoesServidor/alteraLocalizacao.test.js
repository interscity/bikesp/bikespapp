import {BU, CPF, EMAIL, FLASK_ADDR, MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils";
import resetaBD from "../testSetup/resetaBD";

import conexao from "../../src/gerenciadores/conexaoServidor.js";

it ("altera endereco e apelido", async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin = await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	// Verifica se é permitido alterar localização
	let respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1]["resposta"]).toBe(true);

	// Solicita alteração
	let respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"2","Avenida Paulista|610||Bela Vista|01311-100|São Paulo|SP","novo apelido");
	expect(respostaAlteracao[0]).toBe(true);

	// Verifica novamente se é permitido alterar localização
	respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1]["resposta"]).toBe(false);
	expect(respostaCheca[1]["dias"]).toBe(31);

	// Tenta alterar novamente
	respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"2","Avenida Paulista|611||Bela Vista|01311-100|São Paulo|SP","mais novo apelido");
	expect(respostaAlteracao[0]).toBe(false);
	expect(respostaAlteracao[1]).toBe("Alteração não autorizada.");
}, MAX_JEST_TIMEOUT);

it ("altera residência", async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin = await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	// Verifica se é permitido alterar localização
	let respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1]["resposta"]).toBe(true);

	// Solicita alteração
	let respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"1","Avenida Paulista|610||Bela Vista|01311-100|São Paulo|SP","Residência");
	expect(respostaAlteracao[0]).toBe(true);

	// Verifica novamente se é permitido alterar localização
	respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1]["resposta"]).toBe(false);
	expect(respostaCheca[1]["dias"]).toBe(31)

	// Tenta alterar novamente
	respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"1","Avenida Paulista|611||Bela Vista|01311-100|São Paulo|SP","Residência");
	expect(respostaAlteracao[0]).toBe(false);
	expect(respostaAlteracao[1]).toBe("Alteração não autorizada.");
}, MAX_JEST_TIMEOUT)

it ("altera apenas apelido", async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin = await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	// Verifica se é permitido alterar localização
	let respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1]["resposta"]).toBe(true);

	// Solicita alteração
	let respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"2","Rua do Matão|1010||Butantã|5508090|São Paulo|SP","novo apelido");
	expect(respostaAlteracao[0]).toBe(true);

	// Verifica novamente se é permitido alterar localização
	respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1]["resposta"]).toBe(false);
	expect(respostaCheca[1]["dias"]).toBe(31);

	// Tenta alterar novamente
	respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"2","Rua do Matão|1010||Butantã|5508090|São Paulo|SP","mais novo apelido");
	expect(respostaAlteracao[0]).toBe(false);
	expect(respostaAlteracao[1]).toBe("Alteração não autorizada.");
}, MAX_JEST_TIMEOUT)

it ("altera endereco com id errado", async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin = await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	// Verifica se é permitido alterar localização
	let respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1].resposta).toBe(true);

	// Solicita alteração
	let respostaAlteracao = await conexao.alteraLocalizacao(CPF,token,"27","Avenida Paulista|610||Bela Vista|01311-100|São Paulo|SP","novo apelido");
	expect(respostaAlteracao[0]).toBe(false);
	expect(respostaAlteracao[1]).toBe("A localização informada não pode ser modificada.");

	// Verifica novamente se é permitido alterar localização
	respostaCheca = await conexao.checaAlteraLocalizacao(CPF,token);
	expect(respostaCheca[0]).toBe(true);
	expect(respostaCheca[1].resposta).toBe(true);
}, MAX_JEST_TIMEOUT);
