import {BU, CPF, EMAIL, FLASK_ADDR,MAX_JEST_TIMEOUT, SENHA} from "../testSetup/testUtils.js";
import {produzTrajeto} from "../testSetup/testUtils.js";
import resetaBD from "../testSetup/resetaBD.js";

import conexao from "../../src/gerenciadores/conexaoServidor.js";

import {DistanciaHaversine} from "../../src/classes/utils.js";

it ("viagem enviada com poucos pontos",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(2023,8,30,7);
	let velocidadeMetrosSegundo = 30 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = 5;

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("POUCOS_PONTOS");

},MAX_JEST_TIMEOUT);

it ("viagem com pontos desconhecidos",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let dataInicio = new Date(2023,8,30,7);
	let duracaoSegundos = 30 * 60;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto([0,0],[-24,-45],dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("ORIGEM_DESTINO_DESCONHECIDOS");

},MAX_JEST_TIMEOUT);

it ("viagem com origem desconhecida",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);

	let dataInicio = new Date(2023,8,30,7);
	let duracaoSegundos = 30 * 60;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto([-23,-46],posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("ORIGEM_DESCONHECIDA");

},MAX_JEST_TIMEOUT);

it ("viagem com destino desconhecido",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);

	let dataInicio = new Date(2023,8,30,7);
	let duracaoSegundos = 30 * 60;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,[-23,-46],dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("DESTINO_DESCONHECIDO");

},MAX_JEST_TIMEOUT);


it ("viagem rápida",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(2023,8,30,7);
	let velocidadeMetrosSegundo = 300 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = Math.max(duracaoSegundos/5, 120);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("NAO_BICICLETA");

},MAX_JEST_TIMEOUT);


it ("viagem devagar",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(2023,8,30,7);
	let velocidadeMetrosSegundo = 5 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("NAO_BICICLETA");

},MAX_JEST_TIMEOUT);


it ("viagem com origem igual a destino",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);

	let dataInicio = new Date(2023,8,30,7);
	let duracaoSegundos = 30 * 60;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoInicial,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("ORIGEM_IGUAL_DESTINO");

},MAX_JEST_TIMEOUT);


it ("viagem antecipa piloto",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(1970,0,0,0,0,5);
	let velocidadeMetrosSegundo = 30 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].estadoViagem).toBe("Reprovado");
	expect(resposta[1].motivoStatus).toBe("PILOTO_NAO_INICIADO");

},MAX_JEST_TIMEOUT);


it ("viagem de bike correta",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(2023,8,30,7);
	let velocidadeMetrosSegundo = 30 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].estadoViagem).toBe("Aprovado");
	expect(resposta[1].motivoStatus).toBe("APROVADO");

},MAX_JEST_TIMEOUT);


it ("limite de viagens atingido",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let velocidadeMetrosSegundo = 30 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,new Date(2023,8,30,7),duracaoSegundos,qtdAmostras);
	let resposta = await conexao.enviaViagem(CPF,token,{
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	});
	expect(resposta[1].motivoStatus).toBe("APROVADO");
	objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,new Date(2023,8,30,9),duracaoSegundos,qtdAmostras);
	resposta = await conexao.enviaViagem(CPF,token,{
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	});
	expect(resposta[1].motivoStatus).toBe("APROVADO");

	objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,new Date(2023,8,30,12),duracaoSegundos,qtdAmostras);
	resposta = await conexao.enviaViagem(CPF,token,{
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	});
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("LIMITE_VIAGENS_EXCEDIDO");
},MAX_JEST_TIMEOUT);


it ("inexistência viagem de validação",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);
	let posicaoFinal = respostaLogin[1]["locs"][1][1].slice(1,-1).split(",").map(Number);
	let distancia = DistanciaHaversine({"Posicao":{"latitude":posicaoInicial[0],"longitude":posicaoInicial[1]}},{"Posicao":{"latitude":posicaoFinal[0],"longitude":posicaoFinal[1]}});

	let dataInicio = new Date(2023,12,30,7);
	let velocidadeMetrosSegundo = 30 / 3.6;
	let duracaoSegundos = distancia / velocidadeMetrosSegundo;
	let qtdAmostras = Math.max(duracaoSegundos/20, 30);

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].estadoViagem).toBe("Aprovado");
	expect(resposta[1].motivoStatus).toBe("APROVADO");

	objTrajeto = produzTrajeto(posicaoInicial,posicaoFinal,new Date(2023,12,30,12),duracaoSegundos,qtdAmostras);
	resposta = await conexao.enviaViagem(CPF,token,{
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	});
	expect(resposta[0]).toBe(true);
	expect(resposta[1].estadoViagem).toBe("Aprovado");
	expect(resposta[1].motivoStatus).toBe("APROVADO");


},MAX_JEST_TIMEOUT);

it ("viagem com origem igual a destino e poucos pontos",async ()=> {
	await resetaBD();
	conexao.SERVER_HOST = FLASK_ADDR;
	await conexao.tentaCadastro(EMAIL,CPF,BU,SENHA);
	let respostaLogin =	await conexao.tentaLogin(EMAIL,SENHA);
	let token = respostaLogin[1]["token"];

	let posicaoInicial = respostaLogin[1]["locs"][0][1].slice(1,-1).split(",").map(Number);

	let dataInicio = new Date(2023,8,30,7);
	let duracaoSegundos = 30 * 60;
	let qtdAmostras = 1;

	let objTrajeto = produzTrajeto(posicaoInicial,posicaoInicial,dataInicio,duracaoSegundos,qtdAmostras);
	
	let objViagem = {
		dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
		duracao: duracaoSegundos,
		trajeto: objTrajeto.posicoes
	};

	let resposta = await conexao.enviaViagem(CPF,token,objViagem);
	expect(resposta[0]).toBe(true);
	expect(resposta[1].motivoStatus).toBe("ORIGEM_IGUAL_DESTINO");

},MAX_JEST_TIMEOUT);