import { encripta } from "../../../../src/classes/utilsSeguranca.js";

it("encripta de 'senha' usando a chave 'af32da2879cdf8a3' é '11e8hWmZrwGu5nLa0GOeMQ=='",() =>{
	input = "senha"
	expect(
		encripta(input,"af32da2879cdf8a3")
	).toStrictEqual(
		"11e8hWmZrwGu5nLa0GOeMQ=="
	);
});


it("encripta de 'admin' usando a chave 'af32da2879cdf8a3' é 'Ix5+j0f/Tnpn009iMI6/FQ=='",() =>{
	input = "admin"
	expect(
		encripta(input,"af32da2879cdf8a3")
	).toStrictEqual(
		"Ix5+j0f/Tnpn009iMI6/FQ=="
	);
});