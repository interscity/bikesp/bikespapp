import { geraValorIntegridade } from "../../../../src/classes/utilsSeguranca.js";


it("geraValorIntegridade chaveolatestatestaola é igual a 232f15ee7437211eb928438a2f73f72c ",() =>{
	dict_1 = {
		"testa":"ola",
		"ola":"testa"
	}
	expect(
		geraValorIntegridade(dict_1,"chave")
	).toStrictEqual(
		"232f15ee7437211eb928438a2f73f72c"
	);
});
