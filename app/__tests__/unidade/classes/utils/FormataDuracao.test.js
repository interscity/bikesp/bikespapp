import { FormataDuracao } from "../../../../src/classes/utils";

it("FormataDuracao(0) é 0 s",() =>{
	expect(FormataDuracao(0)).toBe("0 s");
});
it("FormataDuracao(60000) é 1 min",() =>{
	expect(FormataDuracao(60000)).toBe("1 min");
});
it("FormataDuracao(10000) é 1 s",() =>{
	expect(FormataDuracao(1000)).toBe("1 s");
});

it("FormataDuracao(72) é 0 s",() =>{
	expect(FormataDuracao(72)).toBe("0 s");
});
it("FormataDuracao(14789874) é 246 mins",() =>{
	expect(FormataDuracao(14789874)).toBe("246 mins");
});
it("FormataDuracao(147898.74) é 2 mins",() =>{
	expect(FormataDuracao(147898.74)).toBe("2 mins");
});

it("FormataDuracao(-10000) é ",() =>{
	expect(FormataDuracao(-10000)).toBe("");
});
