import { LocalizacaoMaisProxima } from "../../../../src/classes/utils";

it("LocalizacaoMaisProxima",() =>{
	let listaLocs = [
		[1,[0,0]],
	];
	expect(LocalizacaoMaisProxima(listaLocs,[0,0])).toStrictEqual({"distancia":0,"localizacao":listaLocs[0],"id":1,"posicaoAtual":[0,0]});
	listaLocs.push([2,[1,1]]);
	expect(LocalizacaoMaisProxima(listaLocs,[0,0])).toStrictEqual({"distancia":0,"localizacao":listaLocs[0],"id":1,"posicaoAtual":[0,0]});
	expect(LocalizacaoMaisProxima(listaLocs,[1,1])).toStrictEqual({"distancia":0,"localizacao":listaLocs[1],"id":2,"posicaoAtual":[1,1]});
	expect(LocalizacaoMaisProxima(listaLocs,[0.7,0.7])["id"]).toStrictEqual(2);
});
