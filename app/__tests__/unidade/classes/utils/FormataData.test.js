import {FormataData} from "../../../../src/classes/utils.js";

it("FormataData(new Date(2001,1,1)) é \"01/01",() =>{
	expect(FormataData(new Date(2001,0,1))).toBe("01/01");
});

it("FormataData(new Date(2023,5,1)) é \"01/05",() =>{
	expect(FormataData(new Date(2001,4,1))).toBe("01/05");
});

it("FormataData(new Date(2001,12,3,8,20)) é \"03/12",() =>{
	expect(FormataData(new Date(2001,11,3,8,20))).toBe("03/12");
});

it("FormataData(new Date(2001,12,3,8,20),'DD/MM/YY HH') é \"03/12 08",() =>{
	expect(FormataData(new Date(2001,11,3,8,20),"DD/MM/YY HH")).toBe("03/12/01 08");
});

it("FormataData(new Date(2001,12,3,8,20),'DD/MM/YY HH:mm:ss') é \"03/12/01 08:20:00",() =>{
	expect(FormataData(new Date(2001,11,3,8,20),"DD/MM/YY HH:mm:ss")).toBe("03/12/01 08:20:00");
});