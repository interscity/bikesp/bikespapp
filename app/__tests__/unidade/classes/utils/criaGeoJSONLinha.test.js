import { criaGeoJSONLinha } from "../../../../src/classes/utils";


it("criaGeoJSONLinha([0,0]) é linhaGeoJSON com as coordenadas [0,0]",() =>{
	expect(criaGeoJSONLinha([0,0])).toStrictEqual({"features": [{"geometry": {"coordinates": [0, 0], "type": "LineString"}, "properties": {}, "type": "Feature"}], "type": "FeatureCollection"});
});

it("criaGeoJSONLinha([-123.4433, 23.0002]) é linhaGeoJSON com as coordenadas [-123.4433, 23.0002]",() =>{
	expect(criaGeoJSONLinha([-123.4433, 23.0002])).toStrictEqual({"features": [{"geometry": {"coordinates": [-123.4433, 23.0002], "type": "LineString"}, "properties": {}, "type": "Feature"}], "type": "FeatureCollection"});
});
