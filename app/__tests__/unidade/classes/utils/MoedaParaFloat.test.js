import {MoedaParaFloat} from "../../../../src/classes/utils.js";

it("MoedaParaFloat(R$ 0,00) é 0 ",() =>{
	expect(MoedaParaFloat("R$ 0,00")).toBe(0);
});

it("MoedaParaFloat(R$ 1,00) é 1",() =>{
	expect(MoedaParaFloat("R$ 1,00")).toBe(1);
});

it("MoedaParaFloat(R$ 1,50) é 1.5",() =>{
	expect(MoedaParaFloat("R$ 1,50")).toBe(1.5);
});

it("MoedaParaFloat(R$ 10,09) é 10.09",() =>{
	expect(MoedaParaFloat("R$ 10,09")).toBe(10.09);
});

it("MoedaParaFloat(R$ 100.000,00) é 100000 ",() =>{
	expect(MoedaParaFloat("R$ 100.000,00")).toBe(100000);
});

it("MoedaParaFloat(-R$ 1,00) é -1",() =>{
	expect(MoedaParaFloat("-R$ 1,00")).toBe(-1);
});
