import {FormataMoeda} from "../../../../src/classes/utils.js";

it("FormataMoeda(0) é R$ 0,00",() =>{
	expect(FormataMoeda(0)).toBe("R$ 0,00");
});

it("FormataMoeda(1) é R$ 1,00",() =>{
	expect(FormataMoeda(1)).toBe("R$ 1,00");
});

it("FormataMoeda(1.5) é R$ 1,50",() =>{
	expect(FormataMoeda(1.5)).toBe("R$ 1,50");
});

it("FormataMoeda(10.09) é R$ 10,09",() =>{
	expect(FormataMoeda(10.09)).toBe("R$ 10,09");
});

it("FormataMoeda(0.0005) é R$ 0,00",() =>{
	expect(FormataMoeda(0.0005)).toBe("R$ 0,00");
});

it("FormataMoeda(100000) é R$ 100.000,00",() =>{
	expect(FormataMoeda(100000)).toBe("R$ 100.000,00");
});

it("FormataMoeda(-1) é -R$ 1,00",() =>{
	expect(FormataMoeda(-1)).toBe("-R$ 1,00");
});
