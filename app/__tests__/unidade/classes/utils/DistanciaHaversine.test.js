import { DistanciaHaversine } from "../../../../src/classes/utils";

it("DistanciaHaversine({Data: 2023-07-04T13:29:11.890Z, Posicao: {latitude: -23.5587813, longitude: -46.7315117}},{Data: 2023-07-04T13:29:16.924Z, Posicao: {latitude: -23.5587712, longitude: -46.7314966}}) é 1.9052821396581527:",() =>{
	expect(DistanciaHaversine({"Data": '2023-07-04T13:29:11.890Z', "Posicao": {"latitude": -23.5587813, "longitude": -46.7315117}},{"Data": '2023-07-04T13:29:16.924Z', "Posicao": {"latitude": -23.5587712, "longitude": -46.7314966}})).toStrictEqual(1.9052821396581527);
});

it("DistanciaHaversine({Data: 2023-07-04T13:32:52.103Z, Posicao: {latitude: -23.5587855, longitude: -46.7314986}},{Data: 2023-07-04T13:33:03.304Z, Posicao: {latitude: -23.5587888, longitude: -46.7314978}}) é 0.37589409618227043:",() =>{
	expect(DistanciaHaversine({"Data": '2023-07-04T13:32:52.103Z', "Posicao": {"latitude": -23.5587855, "longitude": -46.7314986}},{"Data": '2023-07-04T13:33:03.304Z', "Posicao": {"latitude": -23.5587888, "longitude": -46.7314978}})).toStrictEqual(0.37589409618227043);
});

it("DistanciaHaversine({latitude: -23.5587855, longitude: -46.7314986},{latitude: -23.5587888, longitude: -46.7314978} é 0.37589409618227043:",() =>{
	expect(DistanciaHaversine({"latitude": -23.5587855, "longitude": -46.7314986},{"latitude": -23.5587888, "longitude": -46.7314978})).toStrictEqual(0.37589409618227043);
});

it("DistanciaHaversine([-23.5587855, -46.7314986],[-23.5587888, -46.7314978] é 0.37589409618227043:",() =>{
	expect(DistanciaHaversine([-23.5587855, -46.7314986],[-23.5587888, -46.7314978])).toStrictEqual(0.37589409618227043);
});

it("DistanciaHaversine([-23.5587855, -46.7314986],{latitude: -23.5587888, longitude: -46.7314978} é 0.37589409618227043:",() =>{
	expect(DistanciaHaversine([-23.5587855, -46.7314986],{latitude: -23.5587888, longitude: -46.7314978})).toStrictEqual(0.37589409618227043);
});
