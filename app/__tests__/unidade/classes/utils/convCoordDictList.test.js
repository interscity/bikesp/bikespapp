import { convCoordDictList } from "../../../../src/classes/utils";

it("convCoordDictList({latitude : -23.5588282,longitude :-46.7315275})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({latitude : -23.5588282,longitude :-46.7315275})).toStrictEqual([-46.7315275,-23.5588282]);
});

it("convCoordDictList({latitude :0,longitude :0})) é [0,0]:",() =>{
	expect(convCoordDictList({latitude : 0,longitude :0})).toStrictEqual([0,0]);
});

it("convCoordDictList({latitude:458,longitude :-31.0000054})) é [-31.0000054,458]:",() =>{
	expect(convCoordDictList({latitude:458,longitude :-31.0000054})).toStrictEqual([-31.0000054,458]);
});

it("convCoordDictList({lat : -23.5588282,longitude :-46.7315275})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({lat : -23.5588282,longitude :-46.7315275})).toStrictEqual([-46.7315275,-23.5588282]);
});

it("convCoordDictList({lat : -23.5588282,long :-46.7315275})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({lat : -23.5588282,long :-46.7315275})).toStrictEqual([-46.7315275,-23.5588282]);
});

it("convCoordDictList({lat : -23.5588282,lng :-46.7315275})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({lat : -23.5588282,lng :-46.7315275})).toStrictEqual([-46.7315275,-23.5588282]);
});

it("convCoordDictList({Posicao:{lat : -23.5588282,lng :-46.7315275}})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({Posicao:{lat : -23.5588282,lng :-46.7315275}})).toStrictEqual([-46.7315275,-23.5588282]);
});

it("convCoordDictList({Posicao:{latitude : -23.5588282,lng :-46.7315275}})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({Posicao:{latitude : -23.5588282,lng :-46.7315275}})).toStrictEqual([-46.7315275,-23.5588282]);
});

it("convCoordDictList({Posicao:{latitude : -23.5588282,long :-46.7315275}})) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList({Posicao:{latitude : -23.5588282,long :-46.7315275}})).toStrictEqual([-46.7315275,-23.5588282]);
});


it("convCoordDictList([-23.5588282,-46.7315275]) é [-23.5588282,-46.7315275]:",() =>{
	expect(convCoordDictList([-23.5588282,-46.7315275])).toStrictEqual([-23.5588282,-46.7315275]);
});

it("convCoordDictList([-46.7315275,-23.5588282]) é [-46.7315275,-23.5588282]:",() =>{
	expect(convCoordDictList([-46.7315275,-23.5588282])).toStrictEqual([-46.7315275,-23.5588282]);
});