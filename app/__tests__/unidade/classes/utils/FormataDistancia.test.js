import { FormataDistancia } from "../../../../src/classes/utils";

it("FormataDistancia(0) é 0 m",() =>{
	expect(FormataDistancia(0)).toBe("0 m");
});
it("FormataDistancia(0.08) é 0 m",() =>{
	expect(FormataDistancia(0.08)).toBe("0 m");
});
it("FormataDistancia(1.01) é 1 m",() =>{
	expect(FormataDistancia(1.01)).toBe("1 m");
});
it("FormataDistancia(567) é 567 m",() =>{
	expect(FormataDistancia(567)).toBe("567 m");
});
it("FormataDistancia(115.32) é 115 m",() =>{
	expect(FormataDistancia(115.32)).toBe("115 m");
});
it("FormataDistancia(567.999) é 568 m" ,() =>{
	expect(FormataDistancia(567.999)).toBe("568 m");
});
it("FormataDistancia(567.01) é 567 m" ,() =>{
	expect(FormataDistancia(567.01)).toBe("567 m");
});

it("FormataDistancia(1100) é 1.1 km",() =>{
	expect(FormataDistancia(1100)).toBe("1.1 km");
});
it("FormataDistancia(1150) é 1.15 km",() =>{
	expect(FormataDistancia(1150)).toBe("1.15 km");
});
it("FormataDistancia(1158) é 1.16 km",() =>{
	expect(FormataDistancia(1158)).toBe("1.16 km");
});
it("FormataDistancia(140000) é 14 km",() =>{
	expect(FormataDistancia(14000)).toBe("14 km");
});
it("FormataDistancia(1115.32) é 1.12 km",() =>{
	expect(FormataDistancia(1115.32)).toBe("1.12 km");
});
it("FormataDistancia(1000.01) é 1 km",() =>{
	expect(FormataDistancia(1000.01)).toBe("1 km");
});
it("FormataDistancia(1999.99) é 2 km",() =>{
	expect(FormataDistancia(1999.99)).toBe("2 km");
});

it("FormataDistancia(-10) é ",() =>{
	expect(FormataDistancia(-10)).toBe("");
});