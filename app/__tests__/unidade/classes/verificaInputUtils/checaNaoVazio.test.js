import { checaNaoVazio } from "../../../../src/classes/verificaInputUtils";

it(" '' é vazia ",() => {
	expect(checaNaoVazio('',() => {}, () => {})).toEqual(false);
});

it(" ' ' é vazia ",() => {
	expect(checaNaoVazio(" ",() => {}, () => {})).toEqual(false);
});

it(" '	' é vazia ",() => {
	expect(checaNaoVazio("	",() => {}, () => {})).toEqual(false);
});

it(" 'a' não é vazia ",() => {
	expect(checaNaoVazio('a',() => {}, () => {})).toEqual(true);
});