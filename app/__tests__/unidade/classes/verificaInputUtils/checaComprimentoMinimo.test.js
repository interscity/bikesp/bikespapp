import { checaComprimentoMínimo } from "../../../../src/classes/verificaInputUtils";

it(" '' não tem comprimento 6",() => {
	expect(checaComprimentoMínimo('',6,() => {}, () => {})).toEqual(false);
});

it(" '' não tem comprimento 1",() => {
	expect(checaComprimentoMínimo('',1,() => {}, () => {})).toEqual(false);
});

it(" '1' tem comprimento 1",() => {
	expect(checaComprimentoMínimo('1',1,() => {}, () => {})).toEqual(true);
});

it(" 'abaca' tem comprimento 4",() => {
	expect(checaComprimentoMínimo('abaca',1,() => {}, () => {})).toEqual(true);
});