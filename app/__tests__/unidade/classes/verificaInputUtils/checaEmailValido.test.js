import { checaEmailValido } from "../../../../src/classes/verificaInputUtils";

it(" 'adsasda' não tem @",() => {
	expect(checaEmailValido('adsasda',() => {}, () => {})).toEqual(false);
});

it(" 'ad	a@sda' tem espaço",() => {
	expect(checaEmailValido('ad	a@sda',() => {}, () => {})).toEqual(false);
});

it(" 'adsa@sda' é válido",() => {
	expect(checaEmailValido('adsa@sda',() => {}, () => {})).toEqual(true);
});