import { exec } from "child_process";

export default async function resetaBD() {
	await new Promise((resolve, reject) => {
		exec("devbox run repop-banco-teste", (error, stdout, stderr) => {
			if (error) {
				reject(error);
				return;
			}
			resolve(stdout ? stdout : stderr);
		});
	});
}
