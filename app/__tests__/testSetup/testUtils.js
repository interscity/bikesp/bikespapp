import moment from "moment";
import {Trajeto} from "../../src/classes/trajeto.js";

import { SERVER_PORT_TESTE } from "@env";

export const TEST_FLASK_PORT = Number(SERVER_PORT_TESTE);
export const FLASK_ADDR = "http://127.0.0.1:" + TEST_FLASK_PORT;
export const MAX_JEST_TIMEOUT = 100 * 1000;

// Credenciais de teste
export const EMAIL = "testador@email.c";
export const CPF = "12345678909";
export const BU = "12345678910";
export const SENHA = "senha";

export function produzTrajeto(posicaoInicial,posicaoFinal,dataInicio,duracaoSegundos,qtdAmostras){
	let objTrajeto = new Trajeto();
	objTrajeto.inicio = dataInicio;
	objTrajeto.fim = new Date(objTrajeto.inicio.getTime() + 1000 * duracaoSegundos);

	let posicoes = [];

	let taxaAmostra = duracaoSegundos/qtdAmostras; // 1 amostra a cada X segundos

	let deslocamentoLat = (posicaoFinal[0] - posicaoInicial[0])/(duracaoSegundos);
	let deslocamentoLong = (posicaoFinal[1] - posicaoInicial[1])/(duracaoSegundos);
	for (let i = 0; i <= qtdAmostras;i++){
		let latitude = posicaoInicial[0] + i * deslocamentoLat * taxaAmostra;
		let longitude = posicaoInicial[1] + i * deslocamentoLong * taxaAmostra;
		let data = moment(new Date(objTrajeto.inicio.getTime() + 1000 * i*taxaAmostra));
		data = data.format("Y-MM-DD HH:mm:ss") + " GMT-03:00";
		posicoes.push(
			{
				"Data":data,
				"Posicao":{"latitude":latitude,"longitude":longitude},
				"Precisao": 20,
				"Velocidade": 0
			}
		);
	}
	objTrajeto.posicoes = posicoes;

	return objTrajeto;
}