import {NativeEventEmitter, NativeModules, PermissionsAndroid, Platform} from "react-native";
var {NativeGeolocation} = NativeModules;

import waterfall from "async/waterfall";

// Traduzir para o inglês: Está fora do escopo do BikeSP
class WrappedNativeGeolocation{

	constructor(){
		this.eventEmitter = new NativeEventEmitter(NativeGeolocation);
		this.eventEmitter.removeAllListeners("NewLocation");
		this.escutaNovaLoc = this.eventEmitter.addListener("NewLocation", () => {});
	}

	retrievePermissions(callbackAfterRetrieved, failureCallback = () => {}){
		waterfall([
			async function(callback) {
				let resCoarse = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
				callback(null, resCoarse);
			},
			async function(resCoarse, callback) {
				let resFine = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
				callback(null, resCoarse, resFine);
			},
			async function(resCoarse, resFine, callback) {
				if(Platform.Version >= 29){
					let resBackground = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION);
					callback(null, resCoarse, resFine, resBackground);
				}else{
					callback(null, resCoarse, resFine, true);
				}
			}
		], function (err, resCoarse, resFine, resBackground) {
			if(err == null){
				callbackAfterRetrieved(resCoarse, resFine, resBackground);
			}else{
				failureCallback(err);
			}
		});
	}

	requestPermissions(permissions, successCallback, failureCallback = () => {}){
		let neededCallbacks = [];
		permissions = permissions.map((a) => a.toUpperCase());

		if(permissions.includes("FINE")){
			neededCallbacks.push(async function (callback){
				try{
					let val = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
					callback(null, val == "granted");
				}catch (err) {
					callback(err, null);
				}
				
			});
		}
		else if(permissions.includes("COARSE")){
			neededCallbacks.push(async function (callback){
				try{
					let val = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
					callback(null, val == "granted");
				}catch (err) {
					callback(err, null);
				}
				
			});
		}

		if(permissions.includes("BACKGROUND")){
			neededCallbacks.push(async function (perm, callback){
				// console.log("callback: ", callback, " perm: ", perm)
				if (Platform.Version >= 29){
					try{
						let val = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION);
						callback(null, perm , val == "granted");
					}catch (err) {
						callback(err, perm, null);
					}					
				}
				else{
					callback(null,perm,true);
				}

			});
		}

		waterfall(neededCallbacks,
			function (err, perm, back){
				// console.log("err,: ", err, " perm: ", perm," back: ", back)
				if(err == null){
					if(permissions.includes("BACKGROUND")){
						successCallback(perm,back);
					}else{
						successCallback(perm);
					}
				}else{
					failureCallback(err);
				}
			});
	}
	
	isGPSAvaliable(){
		// const v = NativeGeolocation.isGPSAvaliable()
		// console.log("Is gps enabled?",v)
		return NativeGeolocation.isGPSAvaliable();
	}

	isIgnoringBatteryOptimizations(){
		// const v = NativeGeolocation.isGPSAvaliable()
		// console.log("Is gps enabled?",v)
		return NativeGeolocation.isIgnoringBatteryOptimizations();
	}

	promptBatteryOptimizationsSettings(){
		NativeGeolocation.promptBatteryOptimizations();
	}

	//Stale timout: #secs which make a single gps sample stale
	getLastKnownPosition(positiveCallback, negativeCallback, staleTimeout=null){
		this.retrievePermissions((c,f) => {
			if(c || f){
				let lastMeasurement = NativeGeolocation.getLastKnownPosition(staleTimeout);
				if(lastMeasurement.lat == -1){
					console.log("Failure accessing last know pos");
					negativeCallback();
				}else{
					positiveCallback(lastMeasurement);
				}
			}else{
				throw new Error("NativeGeolocation: Attempted to get last known position without necessary permissions");
			}
		});
		
	}

	/**
	 * Tries to get the user's current position with urgency.
	 * First, the method looks for the most recent cached gps sample, 
	 * and if none are found within the last <staleTimeout> seconds, 
	 * fires a request for a new position from every available provider.
	 * @param {callback} positiveCallback 
	 * @param {callback} negativeCallback 
	 * @param {number} staleTimeout (s) 
	 * @param {number} timeout (ms)
	 */
	getUrgentPosition(positiveCallback, negativeCallback, staleTimeout, timeout=null){
		this.retrievePermissions((c,f) => {
			if(c || f){

				let positionRequestInfo = new LocationRequestInfo();

				if(timeout != null){
					setTimeout(() => {
						if(positionRequestInfo.isComplete()){
							return; 
						}
						positionRequestInfo.stale = true;
						negativeCallback();
					},timeout);
				}
				NativeGeolocation.getUrgentPosition(staleTimeout,
					(measurement) => {
						positionRequestInfo.completed = true;
						if(measurement.lat == -1){
							console.log("Failure accessing urgent pos");
							negativeCallback();
						}else if(!positionRequestInfo.isStale()){
							positiveCallback(measurement);
						}
					});
			}else{
				throw new Error("NativeGeolocation: Attempted to get urgent position without necessary permissions");
			}
		});
	}

	getSinglePosition(positiveCallback, negativeCallback, timeout=null){
		this.retrievePermissions((c,f) => {
			if(c || f){

				let positionRequestInfo = new LocationRequestInfo();

				if(timeout != null){
					setTimeout(() => {
						if(positionRequestInfo.isComplete()){
							return; 
						}
						positionRequestInfo.stale = true;
						negativeCallback();
					},timeout);
				}

				NativeGeolocation.getSinglePosition(
					(measurement) => {
						positionRequestInfo.completed = true;
						if(measurement.lat == -1){
							console.log("Failure accessing current position");
							negativeCallback();
						}else if(!positionRequestInfo.isStale()){
							positiveCallback(measurement);
						}
					}
				);
			}else{
				throw new Error("NativeGeolocation: Attempted to get present position without necessary permissions");
			}
		});
	}

	trackPosition(positiveCallback, negativeCallback, refreshTimeout=0, distanceTolerance=3, forceSingleListener=false){
		this.retrievePermissions((c,f) => {
			if(c || f){
				if(forceSingleListener){
					this.eventEmitter.removeAllListeners("NewLocation");
				}
				NativeGeolocation.trackPosition(refreshTimeout, distanceTolerance);
				this.eventEmitter.addListener("NewLocation", (event) => {
					if(event.lat == -1){
						negativeCallback();
					}else{
						positiveCallback(event);
					}
				});
			}else{
				throw new Error("NativeGeolocation: Attempted to begin tracking without necessary permissions");
			}
		});
	}

	stopTracking(){
		this.eventEmitter.removeAllListeners("NewLocation");
		NativeGeolocation.stopTracking();
	}

	/**
	 * Updates the Geolocator module's core. The caller may switch between
	 * the Google Play Services core, or the vanilla core depending on the
	 * boolean value passed.
	 * @param {boolean} enableGooglePlayServices 
	 */
	updateCore(enableGooglePlayServices){
		NativeGeolocation.changeGoogleServicesCoreStatus(enableGooglePlayServices);
	}

	/**
	 * Enables power saving features, such as batched locations delivery. 
	 * Power saving features are set to on by default;
	 */
	enablePowerSavings(){
		NativeGeolocation.enablePowerSavings();
	}

	/**
	 * Disables power saving features, such as batched locations delivery. 
	 * Power saving features are set to on by default;
	 */
	disablePowerSavings(){
		NativeGeolocation.disablePowerSavings();
	}

	/**
	 * Returns whether the module is currently tracking the user's position
	 */
	isCurrentlyTracking(){
		return NativeGeolocation.isCurrentlyTracking();
	}

}

/**
 * Auxiliar Class, retains info regarding a single location request
 */
class LocationRequestInfo {

	constructor(){
		this.stale = false;
		this.completed = false;
	}

	isStale(){
		return this.stale;
	}

	isComplete(){
		return this.completed;
	}

}

const instanciaNativeGeolocation = new WrappedNativeGeolocation();

export default instanciaNativeGeolocation;