import { NativeModules } from "react-native";

var { RNActivityRecognitionModule } = NativeModules;

class ActivityRecognitionWrapper {

	constructor(){
		this.enabled = true;
	}

	enable(){
		this.enabled = true;
	}

	disable(){
		if (this.isTracking()){
			throw new Error("ActivityRecognition: Attempted to disable the module while tracking");
		} 
		this.enabled = false;
	}

	isEnabled(){
		return this.enabled;
	}

	isTracking(){
		return RNActivityRecognitionModule.isTracking();
	}

	startTracking() {
		if (this.enabled) RNActivityRecognitionModule.startTracking();
	}

	endTracking() {
		if (this.enabled) RNActivityRecognitionModule.endTracking();
	}

	getLastTrip() {
		return this.enabled ? RNActivityRecognitionModule.getLastTrip() : [];
	}

	getCurrentTrip() {
		return this.enabled ? RNActivityRecognitionModule.getCurrentTrip(): [];
	}

	getLastMeasure() {
		return this.enabled ? RNActivityRecognitionModule.getLastMeasure(): {};
	}
}

const activityRecognition = new ActivityRecognitionWrapper();

export default activityRecognition;