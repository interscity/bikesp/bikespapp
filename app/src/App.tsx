import React, {useEffect, useContext} from "react";
import {StatusBar, AppState, PermissionsAndroid} from "react-native";

import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";

import {TelaHistoricoViagens} from "./componentes/telas/telaHistoricoViagens";
import {TelaHome} from "./componentes/telas/telaHome";
import {TelaEmViagem} from "./componentes/telas/telaEmViagem";
import {TelaFaleConosco} from "./componentes/telas/telaFaleConosco";
import {TelaDetalhesViagem} from "./componentes/telas/telaDetalhesViagem";
import {TelaCadastro} from "./componentes/telas/telaCadastro";
import {TelaLogin} from "./componentes/telas/telaLogin";
import {TelaCarregandoDados} from "./componentes/telas/telaCarregandoDados";
import {TelaRotaViagem} from "./componentes/telas/telaRotaViagem";
import {TelaContestar} from "./componentes/telas/telaContestar";
import {TelaLocaisCadastrados} from "./componentes/telas/telaLocaisCadastrados.js";
import {TelaEditaLocalizacao} from "./componentes/telas/telaEditaLocalizacao";
import {TelaExtrato} from "./componentes/telas/telaExtrato.js";
import {TelaConfiguracoes} from "./componentes/telas/telaConfiguracoes.js";
import {TelaBonus} from "./componentes/telas/telaBonus.js";
import {TelaContestacoes} from "./componentes/telas/telaContestacoes.js";
import {TelaRoot} from "./componentes/telas/telaRoot";

import {contextoGerenciadorLocal} from "./contextos/contextoGerenciadorLocal.js";
import {contextoGerenciadorNotifis} from "./contextos/contextoGerenciadorNotifs";
import {contextoTrajeto} from "./contextos/contextoTrajeto";
import {contextoLocalizador} from "./contextos/contextoLocalizador";
import {tentaRetomarTrajeto} from "./acoes/tentaRetomarTrajeto.js";
import {TelaTrocaBU} from "./componentes/telas/telaTrocaBU";
import {TelaEsqueceuSenha} from "./componentes/telas/telaEsqueceuSenha";
import {TelaExibePonto} from "./componentes/telas/telaExibePonto";
import {TelaMaisInfo} from "./componentes/telas/telaMaisInfo.js";
import {TelaInstrucionalCadastro} from "./componentes/telas/telaInstrucionalInicial.js";
import {TelaInstrucionalHome} from "./componentes/telas/telaInstrucionalHome.js";

import {check, request, PERMISSIONS, RESULTS} from "react-native-permissions";
import {containeres} from "./estilos/containeres.js";

import {Trajeto} from "./classes/trajeto";
import {Viagem} from "./classes/viagem";
import {Local} from "./classes/local";
import {TelaValidacaoLocalizacoes} from "./componentes/telas/telaValidacaoLocalizacoes";


/**
 * Define os parâmetros que vão estar disponíveis no `route.params` através do `useRoute`
 */
export type AppParamList = {
	EditaLocalizacao:  {local: Local};
	LocaisCadastrados: undefined;
	ExibePonto: {local: Local};
	FaleConosco: {telaOrigem: AppScreens, textoOrigem: string} | undefined;
	InstrucionalHome:{revisando: boolean} | undefined;
	MaisInfo: {voltarPara: AppScreens};
	RotaViagem: {trajeto: Trajeto, apelidoOrigem: string, apelidoDestino: string};
	DetalhesViagem: {viagem: Viagem | undefined | null, idViagem: string | number | null}; // TODO: melhorar tipagem do id viagem
	Contestar: {idViagem: string | number | null};
	Limbo: undefined;
	InstrucoesCadastro: undefined;
	InstrucoesHome: undefined;
	Home: undefined;
	HistoricoViagens: undefined;
	EmViagem: undefined;
	Login: undefined;
	Cadastro: undefined;
	CarregandoDados: undefined;
	TrocaBU: undefined;
	EsqueceuSenha: undefined;
	Extrato: undefined;
	Config: undefined;
	Bonus: undefined;
	Contestacoes: undefined;
	Root: undefined;
	ValidacaoLocalizacoes: undefined;
};

/**
 * Define as telas diponíveis no aplicativo para permitir navegação type safe.
 */
export type AppScreens = keyof AppParamList;

const Pilha = createNativeStackNavigator<AppParamList>();

function App() {
	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const gNotifs = useContext(contextoGerenciadorNotifis);
	const trajeto = useContext(contextoTrajeto);
	const localizador = useContext(contextoLocalizador);

	const verificarPermissoes = async () => {
		const vibratePermission = await check(PERMISSIONS.ANDROID.VIBRATE);
		const bootPermission = await check(
			PERMISSIONS.ANDROID.RECEIVE_BOOT_COMPLETED,
		);

		if (
			vibratePermission !== RESULTS.GRANTED ||
      bootPermission !== RESULTS.GRANTED
		) {
			pedirPermissoes();
		}
	};

	const pedirPermissoes = async () => {
		try {
			await request(PERMISSIONS.ANDROID.VIBRATE);
			await request(PERMISSIONS.ANDROID.RECEIVE_BOOT_COMPLETED);
			await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
			);
		} catch (error) {
			console.error("Erro ao solicitar permissões:", error);
		}
	};

	useEffect(() => {
		verificarPermissoes();
		gNotifs.criaCanalDeNotificacoes();
		AppState.addEventListener("change", proxEstado => {
			if (proxEstado != "active") {
				localizador.ligaOtimizacoesEnergia();

				gDadosLocais.salvaDadosDisco();
			} else {
				setTimeout(localizador.desligaOtimizacoesEnergia, 500);

				if (gDadosLocais.carregouDisco) {
					if (gDadosLocais.getLocalmenteMem("Logado") == "t") {
						tentaRetomarTrajeto(
							trajeto,
							localizador,
							gDadosLocais,
							gNotifs,
							// eslint-disable-next-line @typescript-eslint/no-empty-function
							() => {},
						);
					}
				}
			}
		});
		return () => {
			if (
				gDadosLocais.getLocalmenteMem("EmViagem") == "t" &&
        trajeto.inicio !== null
			) {
				trajeto.salvaActivityParcial();
				gDadosLocais.setLocalmenteMem("TrajetoAntigo", trajeto.paraJSON());
			} else {
				gDadosLocais.setLocalmenteMem("TrajetoAntigo", "{}");
			}
			gDadosLocais.salvaDadosDisco();
		};
	}, []);

	return (
		<NavigationContainer>
			<StatusBar
				barStyle="dark-content"
				backgroundColor={containeres.tela.backgroundColor}
			/>
			<Pilha.Navigator>
				<Pilha.Screen
					name="Limbo"
					component={TelaCarregandoDados}
					options={{headerShown: false}}
				/>
				<Pilha.Screen 
					name="Root"
					component={TelaRoot}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="InstrucoesCadastro"
					component={TelaInstrucionalCadastro}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="InstrucoesHome"
					component={TelaInstrucionalHome}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Home"
					component={TelaHome}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="HistoricoViagens"
					component={TelaHistoricoViagens}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="LocaisCadastrados"
					component={TelaLocaisCadastrados}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="EmViagem"
					component={TelaEmViagem}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="FaleConosco"
					component={TelaFaleConosco}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="DetalhesViagem"
					component={TelaDetalhesViagem}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Login"
					component={TelaLogin}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Cadastro"
					component={TelaCadastro}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="CarregandoDados"
					component={TelaCarregandoDados}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="RotaViagem"
					component={TelaRotaViagem}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Contestar"
					component={TelaContestar}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="TrocaBU"
					component={TelaTrocaBU}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="EsqueceuSenha"
					component={TelaEsqueceuSenha}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="ExibePonto"
					component={TelaExibePonto}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Extrato"
					component={TelaExtrato}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="MaisInfo"
					component={TelaMaisInfo}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Config"
					component={TelaConfiguracoes}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Bonus"
					component={TelaBonus}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="Contestacoes"
					component={TelaContestacoes}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="EditaLocalizacao"
					component={TelaEditaLocalizacao}
					options={{headerShown: false}}
				/>
				<Pilha.Screen
					name="ValidacaoLocalizacoes"
					component={TelaValidacaoLocalizacoes}
					options={{headerShown: false}}
				/>
			</Pilha.Navigator>
		</NavigationContainer>
	);
}

export default App;
