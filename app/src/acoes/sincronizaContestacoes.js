import interfaceConexao from "../gerenciadores/conexaoServidor.js";

async function sincronizaContestacoes(gerenciadorLocal){
	console.log("SINCRONIZANDO CONTESTACOES");

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let contestacoes = [];

	let resultado = await interfaceConexao.exibeContestacoes(cpf,token);
	if (!resultado[0]){
		return [false,[]];
	}

	contestacoes = resultado[1]["contestacoes"];

	gerenciadorLocal.setLocalmenteMem("Contestacoes",JSON.stringify(contestacoes));

	return [true,contestacoes];
}


export {sincronizaContestacoes};