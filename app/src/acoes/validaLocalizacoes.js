import interfaceConexao from "../gerenciadores/conexaoServidor.js";

async function validaLocalizacoes(gerenciadorLocal,locsErradas){
	console.log("VALIDANDO LOCALIZAÇÕES");

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");
	let resultado = await interfaceConexao.validaLocalizacoes(cpf,token,locsErradas);
	if (!resultado[0]){
		return false;
	}
	gerenciadorLocal.setLocalmenteMem("ValidouLocs","t");
	return true;
}

export {validaLocalizacoes};
