import DeviceInfo from "react-native-device-info";
import waterfall from "async/waterfall";

import {bytesParaMb} from "../classes/utils.js";

/**
 * Função auxiliar que devolve os metadados a serem enviados juntos a cada viagem.
 */
async function coletaMetadados(callbackFinal, localizador, usouGooglePlayServices){

	await waterfall([
		async function(callback) {
			let resultadoAPI = await DeviceInfo.getApiLevel();
			callback(null, resultadoAPI);
		},
		async function(nivelAPI, callback) {
			let resultadoDevice = await DeviceInfo.getModel();
			callback(null, nivelAPI, resultadoDevice);
		},
		async function(nivelAPI, modelo, callback) {
			let resultadoBateria = await DeviceInfo.getPowerState();
			if (resultadoBateria.batteryLevel != undefined){
				resultadoBateria.batteryLevel = parseFloat(resultadoBateria.batteryLevel.toFixed(2));
			}
			callback(null, nivelAPI, modelo, resultadoBateria);
		},
		async function(nivelAPI, modelo, bateria, callback) {
			let modoAviao = await DeviceInfo.isAirplaneMode();
			callback(null, nivelAPI, modelo, bateria, modoAviao);
		},
		async function(nivelAPI, modelo, bateria, aviao, callback) {
			let emulador = await DeviceInfo.isEmulator();
			callback(null, nivelAPI, modelo, bateria, aviao, emulador);
		},
		async function(nivelAPI, modelo, bateria, aviao, emulador, callback) {
			let memoriaMaxima = bytesParaMb(await DeviceInfo.getMaxMemory());
			let memoriaUsada = bytesParaMb(await DeviceInfo.getUsedMemory());
			let memoriaTotal = bytesParaMb(await DeviceInfo.getTotalMemory());
			let memoriaInfo = {
				"memoriaTotal": memoriaTotal,
				"memoriaMaxima": memoriaMaxima,
				"memoriaUsada": memoriaUsada
			};
			callback(null, nivelAPI, modelo, bateria, aviao, emulador, memoriaInfo);
		},	
	], function (err, nivelAPI, modelo, infoBateria, modoAviao, ehEmulador, memoriaInfo) {

		infoBateria["optimization"] = localizador.estaComOtimizacaoBateria();

		const vApp = DeviceInfo.getVersion();
		const marca = DeviceInfo.getBrand();
		let objMetadados = {
			"vApp" : vApp,
			"vAPI" : nivelAPI,
			"marca" : marca,
			"modelo" : modelo,
			"bateria" : infoBateria,
			"aviao" : modoAviao,
			"emu" : ehEmulador,
			"memoria":memoriaInfo,
			"GooglePS": usouGooglePlayServices,
		};
		
		if(err != null){
			console.log("Erro coletando metadados");
			objMetadados.erro=String(err);
		}

		callbackFinal(objMetadados);
	});
}

export {coletaMetadados};