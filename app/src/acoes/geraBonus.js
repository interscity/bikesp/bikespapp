import interfaceConexao from "../gerenciadores/conexaoServidor.js";

async function geraBonus(gerenciadorLocal){
	console.log("GERANDO BONUS");

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let bonus = [];

	let resultado = await interfaceConexao.exibeBonus(cpf,token);
	if (!resultado[0]){
		return [false,[]];
	}

	bonus = resultado[1]["bonusUsuario"];

	gerenciadorLocal.setLocalmenteMem("Bonus",JSON.stringify(bonus));

	return [true,bonus];
}


export {geraBonus};