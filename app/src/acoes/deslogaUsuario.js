import { 
	Alert,
} from "react-native";

import interfaceConexao from "../gerenciadores/conexaoServidor.js";

import {reenviaViagens} from "../acoes/reenviaViagens.js";

const alertLoginExpirado = () => {
	Alert.alert(
		"Login expirado",
		"Realize o login novamente",
		[{text: "Ok"}],
		{cancelable: false}
	);
};

async function deslogaUsuario(gerenciadorLocal,loginExpirado,navigation=null){

	console.log("Deslogando");	

	await reenviaViagens(gerenciadorLocal);
	
	interfaceConexao.logout(gerenciadorLocal.getLocalmenteMem("NomeUsuario"),gerenciadorLocal.getLocalmenteMem("TokenUser"));
	
	gerenciadorLocal.setLocalmenteMem("Logado","f");
	gerenciadorLocal.setLocalmenteMem("NomeUsuario","");
	gerenciadorLocal.setLocalmenteMem("TokenUser","");
	gerenciadorLocal.setLocalmenteMem("ValidadeToken","");
	gerenciadorLocal.setLocalmenteMem("EmViagem", "f");
	await gerenciadorLocal.salvaDadosDisco();

	if (loginExpirado){
		alertLoginExpirado();
	}

	if(navigation != null){
		navigation.replace("Login");
	
		// Limpando stack
		navigation.reset({index: 0, routes: [{name: "Login"}],});
	}

}

export {deslogaUsuario};