import interfaceConexao from "../gerenciadores/conexaoServidor.js";
import {Viagem} from "../classes/viagem.js";
import {Trajeto} from "../classes/trajeto.js";
import {MoedaParaFloat} from "../classes/utils.js";

import {geraExtrato} from "./geraExtrato.js";

async function sincronizaViagens(gerenciadorLocal,callbackProgresso){
	console.log("SINCRONIZANDO...");
	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let resultado = await interfaceConexao.sincronizaViagens(cpf,token);
	if (!resultado[0]){
		return [false,[]];
	}
	await geraExtrato(gerenciadorLocal);
	
	let viagensServidor = resultado[1]["viagens"];
	let viagensAplicativo = gerenciadorLocal.getListaViagens();

	let viagensFaltantesApp = [];
	for (let i = 0; i < viagensServidor.length;i++){
		let viagemServidor = viagensServidor[i];
		let viagemPresenteApp = false;
		for (let j = 0; j < viagensAplicativo.length;j++){
			let viagemApp = viagensAplicativo[j];
			if (viagemApp.idViagem == viagemServidor[0]){
				viagemPresenteApp = true;
				let remuneracao = MoedaParaFloat(viagemServidor[2]);
				viagensAplicativo[j].remuneracao = remuneracao;
				viagensAplicativo[j].statusRemuneracao = viagemServidor[3];
				viagensAplicativo[j].motivoStatus = viagemServidor[4];
			}
		}
		if (!viagemPresenteApp) {
			let idViagem = viagemServidor[0];
			viagensFaltantesApp.push(idViagem);


		}
		
		if (callbackProgresso != undefined) callbackProgresso(100*i/viagensServidor.length);
	}

	let novasViagensAplicativo = [];

	let respostaViagensFaltantes = await interfaceConexao.viagensFaltantes(cpf,token,viagensFaltantesApp);
	if (respostaViagensFaltantes[0]){
		let viagensFaltantes = respostaViagensFaltantes[1]["viagensFaltantes"];
		for (let i = 0; i < viagensFaltantes.length;i++){
			let novaViagem = criaViagemFaltante(viagensFaltantes[i]);
			novasViagensAplicativo.push(novaViagem);
		}
	}

	for (let i = 0; i < novasViagensAplicativo.length;i++){
		viagensAplicativo.push(novasViagensAplicativo[i]);
	}

	viagensAplicativo.sort((a,b) => (new Date(b.data) - new Date(a.data)));

	gerenciadorLocal.resetaViagens();
	for (let i = 0; i < viagensAplicativo.length;i++){
		gerenciadorLocal.salvaViagem(viagensAplicativo[i]);
	}
	return [true,viagensAplicativo];
}


function criaViagemFaltante(viagemFaltante){
	let novaViagem;
	let idViagem = viagemFaltante[0];
	let dataInicio = viagemFaltante[2];
	let idOrigem = viagemFaltante[4];
	let idDestino = viagemFaltante[5];

	let remuneracao = MoedaParaFloat(viagemFaltante[6]);
	let statusRemuneracao = viagemFaltante[7];
	let statusEnvio = "Enviado";
	let trajetoServidor = viagemFaltante[9];
	let motivoStatus = viagemFaltante[8];
	

	let trajeto = montaTrajetoRecebido(trajetoServidor, dataInicio, idOrigem, idDestino);

	novaViagem = new Viagem(idViagem,idOrigem,idDestino,remuneracao,statusRemuneracao,statusEnvio,trajeto,motivoStatus);
	return novaViagem;
}

function montaTrajetoRecebido(trajetoRecebido, dataInicio, idOrigem, idDestino) {
	let trajetoParaSalvar = new Trajeto();
	trajetoParaSalvar.id_origem = idOrigem;
	trajetoParaSalvar.id_destino = idDestino;
	trajetoParaSalvar.inicio = dataBDToDate(dataInicio);
	// TODO: Lidar melhor com trajetos vazios, usar 0 da forma atual é um paliativo
	let duracaoSegundos = Math.abs(dataTrajetoToDate(trajetoRecebido[trajetoRecebido.length-1]?.Data ?? 0) - dataTrajetoToDate(trajetoRecebido[0]?.Data ?? 0))/1000;
	trajetoParaSalvar.fim = encontraFim(trajetoParaSalvar.inicio,duracaoSegundos);
	return trajetoParaSalvar;
}

function encontraFim(inicio,duracaoSegundos){
	let fim = new Date(inicio);
	fim.setSeconds(fim.getSeconds()+duracaoSegundos);
	return fim;
}

function dataBDToDate(dataBD){
	let data = new Date(dataBD);
	data.setHours(data.getHours()+3);
	return data;
}

function dataTrajetoToDate(dataTrajeto){
	try {
		let data = dataTrajeto.split(" ")[0];
		let horario = dataTrajeto.split(" ")[1];
		let fuso = dataTrajeto.split(" ")[2];
		let fusoHora = Number(fuso.split(":")[0].split("-")[1]);
		let fusoMinuto = Number(fuso.split(":")[1]);
		let dataResultante = new Date(Date.UTC(data.split("-")[0],data.split("-")[1] - 1,data.split("-")[2],horario.split(":")[0],horario.split(":")[1],horario.split(":")[2]));
		dataResultante.setHours(dataResultante.getHours()+fusoHora);
		dataResultante.setMinutes(dataResultante.getMinutes()+fusoMinuto);
		return dataResultante;      
	}
	catch {
		let new_date = new Date(dataTrajeto);
		if (String(new_date) == "Invalid Date") return new Date(0);
		return new_date;
	}

}

export {sincronizaViagens};
export {dataTrajetoToDate};