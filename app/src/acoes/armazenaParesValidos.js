import interfaceConexao from "../gerenciadores/conexaoServidor.js";

async function armazenaParesValidos(gerenciadorLocal){

	console.log("OBTENDO PARES VALIDOS");

	let paresValidos = [];

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let resultado = await interfaceConexao.requisitaParesValidos(cpf,token);
	if (!resultado[0]){
		return [false,paresValidos];
	}

	paresValidos = resultado[1];

	gerenciadorLocal.setLocalmenteMem("ParesValidos",JSON.stringify(paresValidos["paresValidos"]));

	return [true,paresValidos];
}



export {armazenaParesValidos};