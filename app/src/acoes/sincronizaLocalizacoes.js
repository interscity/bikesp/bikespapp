import interfaceConexao from "../gerenciadores/conexaoServidor.js";

async function sincronizaLocalizacoes(gerenciadorLocal){
	console.log("SINCRONIZANDO LOCALIZAÇÕES");

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let locs = {};

	let resultado = await interfaceConexao.sincronizaLocalizacoes(cpf,token);
	if (!resultado[0]){
		return [false,{}];
	}

	locs = montaStringLocais(resultado[1]["locs"]);

	gerenciadorLocal.setLocalmenteMem("Locais",locs);

	return [true,JSON.parse(locs)];
}

function montaStringLocais(listaLocs){
	let stringLocs = "[";

	for(const locI in listaLocs){
		if(locI > 0){
			stringLocs += ",";
		}
		let elemento = listaLocs[locI];

		let idLocal = elemento[0];
		let coordenada = elemento[1];
		let tipoLocal = elemento[2];
		let nomeLocal = elemento[3];
		let endereco = elemento[4];

		if (tipoLocal == "estacao"){
			nomeLocal = embelezaEstacao(nomeLocal);
		}

		let elementoString = String(idLocal) + "," + String(coordenada).replace("(","[").replace(")","]") + "," + "\"" + tipoLocal + "\"" + "," + "\"" + nomeLocal + "\"" + "," + "\"" + endereco + "\"";

		stringLocs += "["  + elementoString + "]";
	}

	stringLocs += "]";
	return stringLocs;
}

function embelezaEstacao(nomeEstacao){
	let nomeEstacaoSplit = nomeEstacao.split(" ");
	for (let i = 0; i < nomeEstacaoSplit.length;i++){
		nomeEstacaoSplit[i] = nomeEstacaoSplit[i].toLowerCase();
		nomeEstacaoSplit[i] = nomeEstacaoSplit[i].charAt(0).toUpperCase() + nomeEstacaoSplit[i].slice(1); 
	}
	nomeEstacao = nomeEstacaoSplit.join(" ");
	return nomeEstacao;
}

export {sincronizaLocalizacoes};

