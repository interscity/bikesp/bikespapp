import JailMonkey from "jail-monkey";

export async function verificaRoot(): Promise<boolean> {
	const isDebuggedMode = await JailMonkey.isDebuggedMode();
	const isJailBroken =
        JailMonkey.isOnExternalStorage() ||
        JailMonkey.isJailBroken() ||
        JailMonkey.trustFall() ||
        isDebuggedMode ||
        JailMonkey.canMockLocation();

	return (!__DEV__ && isJailBroken);
}