/**
 * Função auxiliar para tentar retomar o trajeto antigo (da memória) ao se
 * reiniciar o app
 * @param {Trajeto} trajeto 
 * @param {GeoLocation} localizador 
 * @param {GerenciadorDadosLocais} gDadosLocais 
 * @param {CallableFunction} callback 
 */
export function tentaRetomarTrajeto(trajeto,localizador,gDadosLocais,gerenciadorNotificacoes,callback){
	//Carregar estado do trajeto / viagem anterior
	// console.log("Estava em viagem?", gDadosLocais.getLocalmenteMem("EmViagem"));

	if(gDadosLocais.getLocalmenteMem("EmViagem") == "t"){
		//Checar permissões, se não não retomo
		localizador.descobrePermissoes((c,f) => {
			if(!f){
				gDadosLocais.setLocalmenteMem("EmViagem","f");
				gDadosLocais.setLocalmenteMem("TrajetoAntigo","{}");
				callback();
				return;
			}
			
			const callbackFracasso = () => {
				gerenciadorNotificacoes.criarNotifUltimaViagemPerdida();
				gDadosLocais.setLocalmenteMem("EmViagem","f");
				gDadosLocais.setLocalmenteMem("TrajetoAntigo","{}");
				callback();
			};

			trajeto.atribuiLocalizador(localizador);
			trajeto.retomaTrajeto(gDadosLocais.getLocalmenteMem("TrajetoAntigo"),callback,callbackFracasso);
		});

		
	}else{
		callback();
	}
}