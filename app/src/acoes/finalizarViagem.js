import { ToastAndroid } from "react-native";

import { Viagem } from "../classes/viagem.js";

import { montaTrajetoParaSalvar, calculaDistHaversineTrajeto } from "../classes/utils.js";

import interfaceConexao from "../gerenciadores/conexaoServidor.js";

import {coletaMetadados} from "./coletaMetadados.js";

import {deslogaUsuario} from "./deslogaUsuario.js";

import {geraExtrato} from "./geraExtrato.js";

export async function finalizarViagem(idDestino,trajeto,gerenciadorLocal,navigation, callbackPosTermino=()=>{}){

	gerenciadorLocal.setLocalmenteMem("EmViagem", "f");

	trajeto.finalizarTrajeto(idDestino);

	const usouGoogleServices = gerenciadorLocal.getLocalmenteMem("UsandoGoogleServices");

	let idOrigem = trajeto.id_origem;

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	montaObjViagem(trajeto,idOrigem,idDestino,usouGoogleServices, async (objViagem) => {

		let metadados = objViagem.metadados;
		let viagem_salva = criaViagemIntermediaria(gerenciadorLocal,idOrigem,idDestino,trajeto,metadados);

		let resultado = await interfaceConexao.enviaViagem(cpf,token,objViagem);
		
		await geraExtrato(gerenciadorLocal);
		if(resultado[0] == true){
			atualizaViagemRequestSucesso(viagem_salva,resultado[1]);
			callbackPosTermino();
			navigation.replace("HistoricoViagens");
		}
		else if(resultado[1] == "Problema de Conexão"){
			ToastAndroid.show(resultado[1], ToastAndroid.LONG);
			atualizaViagemRequestSemSucesso(viagem_salva,"NaoEnviado",resultado[1]);
			callbackPosTermino();
			navigation.replace("HistoricoViagens");
		}
		else if (resultado[1] == "Viagem duplicada"){
			callbackPosTermino();
			navigation.replace("HistoricoViagens");
		}
		else {
			atualizaViagemRequestSemSucesso(viagem_salva,"RespostaRejeitada",resultado[1]);
			callbackPosTermino();

			let textoToast = resultado[1];
			if (resultado[1] == "Erro de Autenticacao"){
				textoToast = "Problema de Autenticação";
			}
			ToastAndroid.show(textoToast, ToastAndroid.LONG);

			if (resultado[1] == "Erro de Autenticacao"){
				await deslogaUsuario(gerenciadorLocal,true,navigation);
			}
			else{
				navigation.replace("HistoricoViagens");
			}
		}

	}); 
}

function criaViagemIntermediaria(gerenciadorLocal,idOrigem,idDestino,trajeto,metadados){
	let remuneracao = 0.0;
	let statusEnvio = "Problema de Conexão";
	let statusRemuneracao = "EmAnalise";
	let trajetoParaSalvar = trajeto.copia();

	let idViagem = null;
	let viagem = new Viagem(idViagem,idOrigem,idDestino,remuneracao,statusRemuneracao,statusEnvio,trajetoParaSalvar,statusEnvio);

	viagem.distancia = calculaDistHaversineTrajeto(trajeto);
	viagem.metadados = metadados;

	gerenciadorLocal.salvaViagem(viagem);

	return viagem;
}

function atualizaViagemRequestSucesso(viagem,respostaJson){		
	viagem.remuneracao = respostaJson.remuneracao;
	viagem.statusRemuneracao = respostaJson.estadoViagem;
	viagem.statusEnvio = "RespostaRecebida";
	viagem.idViagem = respostaJson.idViagem;
	viagem.motivoStatus = respostaJson.motivoStatus;
	viagem.idOrigem = respostaJson.idOrigem;
	viagem.idDestino = respostaJson.idDestino;
	viagem.trajeto.id_origem = respostaJson.idOrigem;
	viagem.trajeto.id_destino = respostaJson.idDestino;
	
	viagem.trajeto = montaTrajetoParaSalvar(viagem.trajeto);

}

function atualizaViagemRequestSemSucesso(viagem,novoStatusEnvio,motivoStatus){
	viagem.statusEnvio = novoStatusEnvio;
	viagem.statusRemuneracao = (novoStatusEnvio == "RespostaRejeitada") ? "Reprovado" : "EmAnalise";
	viagem.trajeto = (novoStatusEnvio == "RespostaRejeitada") ? montaTrajetoParaSalvar(viagem.trajeto) : viagem.trajeto.copia();
	viagem.motivoStatus = motivoStatus;
}

async function montaObjViagem(objTrajeto,idOrigem,idDestino, usouGServices, callbackContinuar){
	const localizador = objTrajeto.getLocalizador();
	coletaMetadados((objMetadados) => {

		let objViagem = {
			dataInicio: Math.floor(objTrajeto.inicio.getTime() /1000),
			duracao: objTrajeto.fim - new Date(objTrajeto.inicio),
			origem: idOrigem,
			destino: idDestino,
			trajeto: objTrajeto.posicoes,
			pausas: objTrajeto.pausas,
			activityRecognitionTrip: objTrajeto.activityRecognitionTrip,
			metadados: objMetadados
		};

		// console.log("Obj viagem a ser enviado: ", objViagem)

		callbackContinuar(objViagem);

	}, localizador, usouGServices);
	
}