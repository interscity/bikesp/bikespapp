import messaging from "@react-native-firebase/messaging";
import interfaceConexao from "../gerenciadores/conexaoServidor.js";
import { GerenciadorDadosLocais } from "../gerenciadores/gerenciadorDadosLocais";

export async function atualizaTokenNotificacao(gerenciadorLocal: GerenciadorDadosLocais) {
	const cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	const tokenUser = gerenciadorLocal.getLocalmenteMem("TokenUser");
	const tokenNotificacao: [boolean, any] = await messaging()
		.getToken()
		.then(token => [true, token], reason => [false, reason]);
 
	if (tokenNotificacao[0] == false) {
		return tokenNotificacao;
	}

	console.log("REGISTRANDO TOKEN PARA NOTIFICAÇÕES PUSH");
 
	return await interfaceConexao.atualizaTokenNotificacao(cpf, tokenUser, tokenNotificacao[1] as string);
}