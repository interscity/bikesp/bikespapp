import interfaceConexao from "../gerenciadores/conexaoServidor.js";

export async function reenviaViagens(gDadosLocais){
	let viagensNaoEnviadas = gDadosLocais.getViagensNaoEnviadas();
	for(let viagem of viagensNaoEnviadas){
		await viagem.reenvia(gDadosLocais,interfaceConexao);
		if (viagem.statusEnvio == "ViagemDuplicada"){
			gDadosLocais.removeViagem(viagem);
		}
	}
}