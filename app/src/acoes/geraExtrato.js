import interfaceConexao from "../gerenciadores/conexaoServidor.js";

async function geraExtrato(gerenciadorLocal){
	console.log("GERANDO EXTRATO");

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let extrato = {};

	let resultado = await interfaceConexao.requisitaExtrato(cpf,token);
	if (!resultado[0]){
		return [false,{}];
	}

	extrato = resultado[1];

	gerenciadorLocal.setLocalmenteMem("Extrato",JSON.stringify(extrato));

	return [true,extrato];
}


export {geraExtrato};