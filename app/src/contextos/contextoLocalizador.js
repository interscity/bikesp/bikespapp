import { createContext } from "react";
import { GeoLocation } from "../gerenciadores/geoLocation.js";
 
const contextoLocalizador = createContext(new GeoLocation());
export { contextoLocalizador };

