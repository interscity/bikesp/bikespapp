import { createContext } from "react";
import {Trajeto} from "../classes/trajeto.js";
import activityRecognition from "../modulos/AcitivityRecognition.js";

export const contextoTrajeto = createContext(new Trajeto(activityRecognition));