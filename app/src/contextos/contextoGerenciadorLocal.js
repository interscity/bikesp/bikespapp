import { createContext } from "react";
import { GerenciadorDadosLocais } from "../gerenciadores/gerenciadorDadosLocais.js";

export const contextoGerenciadorLocal = createContext(new GerenciadorDadosLocais());
