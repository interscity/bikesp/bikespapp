import { createContext } from "react";
import { GerenciadorNotificacoes } from "../gerenciadores/notificacoes";

export const contextoGerenciadorNotifis = createContext(new GerenciadorNotificacoes());
