import { montaTrajetoParaSalvar, calculaDistHaversineTrajeto } from "../classes/utils.js";

class Viagem {
	constructor(idViagem=null,idOrigem="",idDestino="",remuneracao=0.0,statusRemuneracao="Reprovado",statusEnvio="NaoEnviado",trajeto=[],motivoStatus="APROVADA"){
		this.idViagem = idViagem;
		this.trajeto = trajeto;
		this.data = new Date(this.trajeto.inicio);
		this.duracao = this.trajeto.fim - this.data - this.trajeto.duracaoPausa;

		this.distancia = calculaDistHaversineTrajeto(trajeto);

		this.idOrigem = idOrigem;
		this.idDestino = idDestino;

		this.remuneracao = remuneracao;
		this.statusRemuneracao = statusRemuneracao; // Deveríamos trocar isso pra um ENUM, eu acho
		this.statusEnvio = statusEnvio;
		this.motivoStatus = motivoStatus;
	}	

	/**
	 * Dado um objeto genérico, carrega o objeto viagem correspondente
	 * @param {Object} obj Objeto com as chaves da viagem
	 */
	static aPartirDeJSON(obj){

		// console.log("Objeto q vou parsear:", obj);

		let idViagem = obj.idViagem;
		let data = new Date(obj.data);
		let distancia = obj.distancia;
		let duracao = obj.duracao;
		let idOrigem = obj.idOrigem;
		let idDestino = obj.idDestino;
		let remuneracao = parseFloat(obj.remuneracao);
		let statusRemuneracao = obj.statusRemuneracao;
		let statusEnvio = obj.statusEnvio;
		let trajeto = obj.trajeto;
		let motivoStatus = obj.motivoStatus;
		let metadados = obj.metadados;

		let novaViagem = new Viagem(idViagem,idOrigem,idDestino,remuneracao,statusRemuneracao,statusEnvio,trajeto,motivoStatus);

		novaViagem.data = data;
		novaViagem.duracao = duracao;
		novaViagem.distancia = distancia;
		novaViagem.metadados = metadados;
		// console.log("Objeto parseado:",novaViagem);

		return novaViagem;

	}

	/**
	 * Devolve json com objeto Viagem
	 * @returns 
	 */
	paraJSON(){

		let trajetoASalvar = this.trajeto;

		if(["RespostaRejeitada","RespostaRecebida","ViagemDuplicada","Enviado"].includes(this.statusEnvio)){
			trajetoASalvar = montaTrajetoParaSalvar(this.trajeto);
		}

		let viagemDic = {
			"idViagem":this.idViagem,
			"data":this.data,
			"duracao":this.duracao,
			"distancia":this.distancia,
			"idOrigem":this.idOrigem,
			"idDestino":this.idDestino,
			"remuneracao":this.remuneracao,	
			"statusRemuneracao":this.statusRemuneracao,
			"statusEnvio":this.statusEnvio,
			"trajeto":trajetoASalvar,
			"motivoStatus":this.motivoStatus,
			"metadados":this.metadados,
		};

		return JSON.stringify(viagemDic);
	}

	async reenvia(gerenciadorLocal,interfaceConexao){

		// console.log('VOu reenviar uma viagem usando',gerenciadorLocal,interfaceConexao);

		if(this.statusEnvio != "NaoEnviado"){
			console.error("Tentei Reenviar viagem já enviada");
			return;
		}

		let cpf = String(gerenciadorLocal.getLocalmenteMem("CPF"));
		let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

		let objViagem = {
			idViagem:this.idViagem,
			dataInicio:  Math.floor(this.data.getTime() /1000),
			duracao: this.duracao,
			idOrigem: this.idOrigem,
			idDestino: this.idDestino,
			trajeto: this.trajeto.posicoes,
			pausas: this.trajeto.pausas,
			activityRecognitionTrip:this.trajeto.activityRecognitionTrip,
			metadados: this.metadados
		};

		let resultado = await interfaceConexao.enviaViagem(cpf,token,objViagem);

		if(resultado[0]){
			let respostaJson = resultado[1];
			this.remuneracao = respostaJson.remuneracao;
			this.statusRemuneracao = respostaJson.estadoViagem;
			this.statusEnvio = "RespostaRecebida";
			this.idViagem = respostaJson.idViagem;
			this.motivoStatus = respostaJson.motivoStatus;
			this.idOrigem = respostaJson.idOrigem;
			this.idDestino = respostaJson.idDestino;
			this.trajeto.id_origem = respostaJson.idOrigem;
			this.trajeto.id_destino = respostaJson.idDestino;
			console.log("Atualizei :) ");
		}
		else if(resultado[1] == "Problema de Conexão"){
			console.log("Não consegui reenviar viagem");
		}
		else if (resultado[1] == "Viagem duplicada"){
			console.log("Viagem duplicada");
			this.statusEnvio = "ViagemDuplicada";
			this.statusRemuneracao = "Reprovado";
			this.motivoStatus = "VIAGEM_DUPLICADA";
		}
		else{
			this.statusEnvio = "RespostaRejeitada";
			this.statusRemuneracao = "Reprovado";
			this.motivoStatus = resultado[1];
		}

	}

}

export {Viagem};