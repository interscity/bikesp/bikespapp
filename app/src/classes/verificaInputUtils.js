
/**
 * Função que checa se um dado texto não é vazio ou só tem espaços em branco
 * @param {string} texto Texto a ser testado
 * @param {CallableFunction} callbackOk Callback invocado quando condição é satisfeita
 * @param {CallableFunction} callbackErro Callback invocado quando condição não é satisfeita
 * @returns booleana dizendo se condição foi satisfeita
 */
function checaNaoVazio(texto, callbackOk, callbackErro){

	let textoLimpo = texto.replaceAll(/\s/g, "");
	if(textoLimpo.length == 0){
		callbackErro();
		return false;
	}else{
		callbackOk();
		return true;
	}

}


/**
 * Função que checa se um dado texto tem um comprimento mínimo
 * @param {string} texto Texto a ser testado
 * @param {number} minComprimento Comprimento mínimo aceito
 * @param {CallableFunction} callbackOk Callback invocado quando condição é satisfeita
 * @param {CallableFunction} callbackErro Callback invocado quando condição não é satisfeita
 * @returns booleana dizendo se condição foi satisfeita
 */
function checaComprimentoMínimo(texto, minComprimento, callbackOk, callbackErro){

	if(texto.length < minComprimento){
		callbackErro();
		return false;
	}else{
		callbackOk();
		return true;
	}

}

/**
 * Funcção que checa se um dado texto corresponde a um email válido
 * @param {string} texto Texto a ser testado
 * @param {CallableFunction} callbackOk Callback invocado quando condição é satisfeita
 * @param {CallableFunction} callbackErro Callback invocado quando condição não é
 * satisfeita, passando com parâmetro a mensagem de erro correspondente.
 * @returns booleana dizendo se condição foi satisfeita
 */
function checaEmailValido(texto, callbackOk, callbackErro){
	if(!texto.includes("@")){
		callbackErro("Email deve conter @");
		return false;
	}else if(/\s/g.test(texto)){
		callbackErro("Email não pode conter espaços");
		return false;
	}else{
		callbackOk();
		return true;
	}
}

/**
 * Realiza a verificação de um CPF, chamando um callback de sucesso ou erro
 * @param {string} texto o CPF na forma de texto
 * @param {*} callbackOk callback a ser chamado se o CPF for válido
 * @param {*} callbackErro callback a ser chamado se o CPF for inválido
 * @returns {boolean} se o CPF é válido
 */
function checaCPFValido(texto,callbackOk,callbackErro){
	texto = texto.replace(/[-.]/g,"");
	// CPF tem que ter 11 dígitos
	if (texto.length != 11 && !/^\d+$/.test(texto)) { callbackErro(); return false; }

	const digitos = texto.split("").map(x => parseInt(x));
	const verificador2 = digitos.pop();
	const verificador1 = digitos.pop();
	
	/**
	 * Dado um critério calculado a partir dos dígitos do CPF e um digito verificador, 
	 * aplica a regra para determinar se o digito verificador é válido
	 * @param {number} criterio calculado a partir dos dígitos do CPF 
	 * @param {number} verificador obtido do CPF inserido
	 * @returns {boolean} Se o digito verificador encontrado no CPF atende ao critério calculado
	 */
	function checaDigitoVerificador(criterio, verificador) {
		return criterio < 2 && verificador == 0 || criterio >= 2 && verificador == 11 - criterio;
	}

	// Verifica primeiro dígito
	const criterio1 = digitos.map((digito, index) => digito * (10 - index))
		.reduce((acc, curr) => acc + curr, 0) % 11;
	
	if (!checaDigitoVerificador(criterio1, verificador1)) { callbackErro(); return false; }

	// Verifica segundo dígito
	digitos.push(verificador1);
	const criterio2 = digitos.map((digito, index) => digito * (11 - index))
		.reduce((acc, curr) => acc + curr, 0) % 11;
 
	if (!checaDigitoVerificador(criterio2, verificador2)) { callbackErro(); return false; }

	callbackOk();
	return true;
}

/**
 * Dado um número de bilhete único na forma de string, verifica se ele é válido e executa uma callback de sucesso ou erro
 * @param {string} texto número do bilhete único
 * @param {*} callbackOk 
 * @param {*} callbackErro 
 * @returns {boolean} Se o bilhete único é valido
 */
function checaBUValido(texto,callbackOk,callbackErro){
	if (!/^\d+$/.test(texto)) { callbackErro(); return false; }

	callbackOk();
	return true;
}

export {checaNaoVazio,checaComprimentoMínimo,checaEmailValido,checaCPFValido,checaBUValido};