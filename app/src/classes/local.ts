/**
 * Descreve o formato de uma localização cadastrada pelo usuário, retornada pelo backend e disponibilizada no Gerenciador de Dados Locais
 */
export type Local = [id: number, coordenada: [latitude: number, longitude: number], tipo: string, apelido: string, endereco: string];