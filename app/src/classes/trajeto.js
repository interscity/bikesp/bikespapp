import moment from "moment";

import {convCoordDictList, criaGeoJSONLinha,LocalizacaoMaisProxima, DistanciaHaversine,MAX_DIST_REVIVER_TRAJETO,RAIO_MAXIMO_TOLERANCIA, TIMEOUT_REVIVER_TRAJETO, TIMEOUT_REVIVER_TRAJETO_ABSURDO, TIMEOUT_POS_VELHA, MAX_SOMA_TEMPO_PAUSA, MAX_DIST_PAUSA} from "./utils.js";

class Trajeto {

	constructor(activityRecognition){
		this.posicoes = [];
		this.inicio = null;
		this.fim = null;

		this.pausas = [];
		this.inicioPausa = null;
		this.fimPausa = null;
		this.duracaoPausa = 0;

		this.emAndamento = false;
		this.emPausa = false;

		this.id_origem = null;
		this.id_destino = null;
		this.localizador = null;
		this.podeMudarEstado = false;

		this.callbacksAndamento = [];
		this.callbacksPausa = [];

		this.activityRecognition = activityRecognition;
		this.activityRecognitionTrip = [];
		this.activityRecognitionTripParcial = [];

		this.updatingActivityRecognitionTrip = false;
	}

	atribuiLocalizador(localizador){
		this.localizador = localizador;
	}

	getLocalizador(){
		return this.localizador;
	}

	atribuiActivitiesRecognition(activityRecognition){
		this.activityRecognition = activityRecognition;
	}

	estendeActivityRecognitionTrip(trip){
		if (this.activityRecognitionTrip == []){
			this.activityRecognitionTrip = trip;
		}
		else{
			this.activityRecognitionTrip.push(...trip);
		}
	}

	salvaActivityParcial(){
		if(this.updatingActivityRecognitionTrip){
			return;
		}
		this.updatingActivityRecognitionTrip = true;
		let newActivitiesTrip = this.activityRecognition.getCurrentTrip();
		this.activityRecognitionTripParcial = newActivitiesTrip;
		this.updatingActivityRecognitionTrip = false;
	}

	recebePosicoes(){
		this.localizador.adicionaCallbackSucesso((p) => { 
			{ 
				this.adicionaPosicao(p);
			}
		},"posicaoAtualTrajeto");
	}

	paraRecebimento(){
		this.localizador.adicionaCallbackSucesso(() => { 
		},"posicaoAtualTrajeto");		
	}

	adicionaPosicao(amostra){
		if (!this.emAndamento) return;

		if(this.posicoes.length > 0){
			let ultimaPos = this.posicoes[this.posicoes.length-1]; 
			if(ultimaPos.Data === amostra.data){
				// console.log("Não adicionei pois são iguais:",ultimaPos," vs ",amostra)
				return;
			}
		}

		this.salvaActivityParcial();
		console.log("activityRecognitionTripParcial:",this.activityRecognitionTripParcial);

		let objPos = {
			"latitude": amostra.latitude,
			"longitude": amostra.longitude, 
		};
		let novaPosicao = {
			"Data":amostra.data,
			"Posicao":objPos,
			"Precisao": amostra.acuracia,
		};

		// Não adiciona o marco 0 de São Paulo no trajeto
		if (objPos.latitude == this.localizador.estadoMarco0.latitude &&
		objPos.longitude == this.localizador.estadoMarco0.longitude	&& novaPosicao.Data == this.localizador.estadoMarco0.data) return;	

		this.posicoes.push(novaPosicao);
	}

	iniciarTrajeto(id_origem,primeira_amostra){
		if (this.localizador == null) throw new Error("[ERRO]: Faltou atribuir um localizador ao trajeto");
		if (this.activityRecognition == null) throw new Error("[ERRO]: Faltou atribuir o activityRecognition ao trajeto");
		if (this.emAndamento) throw new Error("[ERRO]: Já existe um trajeto em andamento");
		if (!this.localizador.emMedicao) this.localizador.iniciarMedicao();
		console.log("INICIEI TRAJETO");
		this.emAndamento = true;
		this.emPausa = false;
		this.inicio = new Date();
		this.atualizaCallbacksAndamento();
		this.id_origem = id_origem;
		this.id_destino = null;
		this.fim = null;
		this.posicoes = [];
		
		this.pausas = [];
		this.inicioPausa = null;
		this.fimPausa = null;
		this.duracaoPausa = 0;

		// Activity recognition
		console.log("Activity Habilitado:",this.activityRecognition.isEnabled());
		this.activityRecognitionTrip = [];
		this.activityRecognitionTripParcial = [];
		this.activityRecognition.startTracking();
		
		this.adicionaPosicao(primeira_amostra);
		this.recebePosicoes();
	}	

	pausaTrajeto(motivo) {
		if (this.emAndamento) {
			
			// Parando medições
			this.estendeActivityRecognitionTrip(this.activityRecognition.getCurrentTrip());
			this.salvaActivityParcial();
			this.activityRecognition.endTracking();
			
			this.paraRecebimento();

			this.emPausa = true;
			this.atualizaCallbacksPausa();
			this.inicioPausa = new Date();

			let data = moment(this.inicioPausa).format("Y-MM-DD HH:mm:ss") + " GMT-03:00";
			if (this.posicoes.length == 0){
				this.adicionaPosicao(this.localizador.getEstadoAtual());
			}
			let posAtual = this.posicoes[this.posicoes.length - 1];

			let novaPausa = {
				"DataInicio": data,
				"Motivo": motivo,
				"AmostraRecente": posAtual
			};
	
			this.pausas.push(novaPausa);
		}
	}

	async continuaTrajeto(gerenciadorLocal) {
		const callbackFracasso = () => {
			this.atualizaCallbacksPausa();
			this.finalizarTrajeto(-1);
			gerenciadorLocal.setLocalmenteMem("EmViagem", "f");
		};

		if (this.emAndamento) {

			this.emPausa = false;
			let fimDaPausa = new Date();

			// POSSO RETOMAR ESSA VIAGEM? 
			const deltaT =  Math.floor((this.duracaoPausa + (fimDaPausa - this.inicioPausa))/1000);

			// console.log("Delta t da viagem:", deltaT);

			if(deltaT  > MAX_SOMA_TEMPO_PAUSA * 60){
				callbackFracasso();
				return {ok: true, recuperou: false, motivo: "Pausa muito longa"};
			}

			let podeVoltarPorDistancia = await this.distanciaVoltaPausaOk();
			if (!podeVoltarPorDistancia){
				callbackFracasso();
				return {ok: true, recuperou: false, motivo: "Usuário longe de onde iniciou a pausa"};
			}

			console.log("CONTINUEI A VIAGEM");
			
			// Retomando medições
			this.recebePosicoes();
			this.activityRecognition.startTracking();
			
			this.atualizaCallbacksPausa();
			this.fimPausa = fimDaPausa;
			this.duracaoPausa = this.duracaoPausa + (this.fimPausa - this.inicioPausa);

			let data = moment(fimDaPausa).format("Y-MM-DD HH:mm:ss") + " GMT-03:00";

			let pausaAntiga = this.pausas[this.pausas.length-1];

			let novaPausa = {
				"DataInicio": pausaAntiga["DataInicio"],
				"Motivo": pausaAntiga["Motivo"],
				"DataFim" : data,
				"AmostraRecente" : pausaAntiga["AmostraRecente"],
			};		

			this.pausas[this.pausas.length-1] = novaPausa;

			return {ok: true, recuperou: true};
		}

		return {ok: false};
	}

	async distanciaVoltaPausaOk(){
		if(this.pausas.length < 1){
			throw new Error("[ERRO]: Tentou voltar de pausa inexistente");
		}
		const ultimoPontoTrajeto = this.pausas[this.pausas.length-1]["AmostraRecente"]["Posicao"];

		console.log("Ponto do começo da pausa para retomar: ", ultimoPontoTrajeto);

		let posAtual = this.localizador.getEstadoAtual();

		if(Math.floor((moment() - moment(posAtual.data, "YYYY-MM-DD HH:mm:ss Z"))/1000) > TIMEOUT_POS_VELHA){

			let promessa = new Promise((resolver) => {
				this.localizador.pegarPosicaoE(() => {resolver();},() => {resolver();},TIMEOUT_POS_VELHA);
			});

			await promessa;

			posAtual = this.localizador.getEstadoAtual();
			console.log("Posicao era velha");
		}

		const pos1 = [ultimoPontoTrajeto.latitude, ultimoPontoTrajeto.longitude];
		const pos2 = [posAtual.latitude, posAtual.longitude];

		const deltaS = DistanciaHaversine(pos1,pos2);

		if(deltaS > MAX_DIST_PAUSA){
			return false;
		}
		console.log("Distancia volta OK",deltaS);
		return true;
	}

	finalizarTrajeto(id_destino){
		if (this.localizador == null) throw new Error("[ERRO]: Faltou atribuir um localizador ao trajeto");
		if (!this.emAndamento) throw new Error("[ERRO]: Não existe nenhum trajeto em andamento");
		if (this.localizador.emMedicao) this.localizador.finalizarMedicao();
		console.log("FINALIZEI TRAJETO");
		this.emAndamento = false;
		this.emPausa = false;
		this.atualizaCallbacksAndamento();
		this.fim = new Date();
		this.id_destino = id_destino;
		this.paraRecebimento();
		
		// Activity Recognition
		this.estendeActivityRecognitionTrip(this.activityRecognition.getCurrentTrip());
		this.activityRecognition.endTracking();
	}

	checaPodeMudarEstado(locsPossiveis,coordenadaAtual){
		// PodeMudarEstado é true se a localizacao mais próxima está dentro do raio
		// e se a localização final é diferente da inicial
		let locMaisProxima = LocalizacaoMaisProxima(locsPossiveis,coordenadaAtual);
		let	podeMudarEstado = false;
		if (locMaisProxima["distancia"] <= RAIO_MAXIMO_TOLERANCIA){
			podeMudarEstado = true;
			if (this.emAndamento && locMaisProxima["id"] == this.id_origem){
				podeMudarEstado = false;
			}
		}
		this.podeMudarEstado = podeMudarEstado;
		return podeMudarEstado;
	} 

	/**
	 * Devolve uma cópia deste trajeto em uma nova instância da classe
	 */
	copia(){
		let novo = new Trajeto();
		novo.posicoes = JSON.parse(JSON.stringify(this.posicoes));
		novo.inicio = this.inicio;
		novo.fim = this.fim;
		novo.emAndamento = this.emAndamento;

		novo.duracaoPausa = this.duracaoPausa;
		novo.pausas = JSON.parse(JSON.stringify(this.pausas));

		novo.id_origem = this.id_origem;
		novo.id_destino = this.id_destino;

		novo.localizador = this.localizador;

		novo.podeMudarEstado = this.podeMudarEstado;
		novo.callbacksAndamento = this.callbacksAndamento;

		novo.activityRecognitionTrip = JSON.parse(JSON.stringify(this.activityRecognitionTrip));
		novo.activityRecognitionTripParcial = JSON.parse(JSON.stringify(this.activityRecognitionTripParcial));


		return novo;
	}

	/**
	 * Dado o JSON de um trajeto antigo, retoma o estado ao do trajeto anterior
	 * se ele não for 'antigo' de mais, conforme definido na constante 
	 * TIMEOUT_REVIVER_TRAJETO do utils.
	 * 
	 * Devolve um booleano indicando se o trajeto foi recuperado ou não.
	 */
	retomaTrajeto(jsonTrajeto, callbackSucesso, callbackErro){

		const medicaoAberta = this.localizador.estouRastreando();

		if(medicaoAberta){
			callbackSucesso();
			return;
		}

		let naoRetomarTrajeto = () => {
			console.log("NÃO RETOMEI");
			this.emAndamento = false;
			this.posicoes = [];
			this.inicioPausa = null;
			this.fimPausa = null;
			this.duracaoPausa = 0;
			this.emPausa = false;
			this.pausas = [];
			this.atualizaCallbacksAndamento();
			this.activityRecognition.endTracking();
			this.activityRecognitionTripParcial = [];
			this.activityRecognitionTrip = [];
			callbackErro();
		};

		let retomarTrajeto = () => {
			this.emAndamento = true;

			this.inicio = new Date(objAntigo.inicio);
			if (objAntigo.fim != null) {
				this.fim = new Date(objAntigo.fim);
			}

			this.id_origem = objAntigo.id_origem;
			this.id_destino = objAntigo.id_destino;

			this.podeMudarEstado = objAntigo.podeMudarEstado;
			
			// this.callbacksAndamento = [];

			this.atualizaCallbacksAndamento();
			this.recebePosicoes();
			if (!this.emPausa){
				this.localizador.iniciarMedicao();
				this.activityRecognition.startTracking();
				this.activityRecognitionTrip = this.activityRecognitionTripParcial;
			}
			else{
				this.posicoes = objAntigo.posicoes;
			}

			console.log("RETOMEI TRAJETO");

			callbackSucesso();
		};

		let objAntigo;
		try {
			objAntigo = JSON.parse(jsonTrajeto);
			if(objAntigo.posicoes.length < 1){
				console.log("Posicoes antiga era vazia");
				naoRetomarTrajeto();
				return;
			}
		} catch (error) {
			console.log("Não consegui acessar posições do trajeto antigo.\n",error);
			naoRetomarTrajeto();
			return;
		}
		this.emPausa = objAntigo.emPausa;
		this.inicioPausa =  moment(objAntigo.inicioPausa, "YYYY-MM-DD HH:mm:ss Z");
		this.fimPausa = moment(objAntigo.fimPausa, "YYYY-MM-DD HH:mm:ss Z");
		this.duracaoPausa = objAntigo.duracaoPausa;
		this.pausas = objAntigo.pausas;
		
		if(this.emPausa){
			retomarTrajeto();
			return;
		}

		this.localizador.pegarPosicaoE(
			(medicaoAgora) => {

				let ultimoPontoAntigo = objAntigo.posicoes[objAntigo.posicoes.length-1];

				if(this.posicoes.length > 1){
					const ultimoPontoTrajeto = this.posicoes[this.posicoes.length-1];
					// console.log("Achei em mim a antiga:",ultimoPontoTrajeto);
					// console.log("Ultimo ponto antigo: ",ultimoPontoAntigo);
					// console.log("> ?", ultimoPontoTrajeto.Data > ultimoPontoAntigo.Data);
					if(ultimoPontoTrajeto.Data > ultimoPontoAntigo.Data){
						ultimoPontoAntigo = ultimoPontoTrajeto;
					}
				}
			
				console.log("Ultimo ponto antigo: ",ultimoPontoAntigo);

				const deltaS = DistanciaHaversine(
					ultimoPontoAntigo,
					{Posicao:   {latitude: medicaoAgora.lat,
						longitude: medicaoAgora.lng} });	
				
				console.log("Retomando trajeto pausado por",deltaS,"metros");
					

				//"2023-09-07 12:16:29 GMT-03:00"
				const deltaT = Math.floor((moment() - moment(ultimoPontoAntigo.Data, "YYYY-MM-DD HH:mm:ss Z"))/1000);

				console.log("Retomando trajeto pausado por", deltaT,"segundos e",deltaS,"metros");

				if(deltaT > TIMEOUT_REVIVER_TRAJETO_ABSURDO || (deltaT > TIMEOUT_REVIVER_TRAJETO && deltaS > MAX_DIST_REVIVER_TRAJETO) ){
					console.log("Trajeto pausado em ponto muito distante e há muito tempo, não será retomado");
					naoRetomarTrajeto();
					return;
				}

				retomarTrajeto();
			}, naoRetomarTrajeto, 61,15000);

		
	}

	/**
	 * Devolve a representação em JSON usada para salvar um trajeto no disco
	 */
	paraJSON(){
		const objIntermediario = {
			inicio: this.inicio,
			fim: this.fim,

			id_origem: this.id_origem,
			id_destino: this.id_destino,

			inicioPausa: this.inicioPausa,
			fimPausa: this.fimPausa,
			duracaoPausa: this.duracaoPausa,
			emPausa: this.emPausa,
			pausas: this.pausas,

			posicoes: this.posicoes,
			podeMudarEstado: this.podeMudarEstado,

			activityRecognitionTrip: this.activityRecognitionTrip,

			activityRecognitionTripParcial: this.activityRecognitionTripParcial

		};



		const saida = JSON.stringify(objIntermediario);
		
		console.log("Vou salvar: ", saida);

		return saida;
	}

	paraGeoJSON(){
		let coordenadas = this.posicoes.map( (item) => ( convCoordDictList(item.Posicao) ));
		return criaGeoJSONLinha(coordenadas);		
	}

	getDuracaoTrajeto(){
		if (!this.emAndamento || this.inicio == null) return 0;
		if (this.emPausa) return this.inicioPausa - this.inicio - this.duracaoPausa;
		let dataAgora = new Date();
		return dataAgora - this.inicio - this.duracaoPausa;
	}

	getTempoPausaRestante(){
		let dataAgora = new Date();
		let tempoRestante = MAX_SOMA_TEMPO_PAUSA * 60 * 1000 - ((dataAgora - this.inicioPausa) + this.duracaoPausa);
		return tempoRestante;
	}

	adicionaCallbackAndamento(c,id){
		this.callbacksAndamento[id] = c ;
	}

	adicionaCallbackPausa(c, id){
		this.callbacksPausa[id] = c;
	}

	atualizaCallbacksAndamento(){
		for (let id in this.callbacksAndamento) {
			this.callbacksAndamento[id](this.emAndamento);
		}
	}

	atualizaCallbacksPausa(){
		for (let id in this.callbacksPausa) {
			this.callbacksPausa[id](this.emPausa);
		}
	}

}

export {Trajeto};