/*
    Funções utilitárias para o app
*/
import moment from "moment";

import { Trajeto } from "./trajeto";

export const RAIO_MAXIMO_TOLERANCIA = 240;

export const TIMEOUT_REVIVER_TRAJETO = 121;
// O valor abaixo não pode ser maior que servidor/validaviagens/consts - DELTAT_AMOSTRA_MUITO_ANTIGA
export const TIMEOUT_REVIVER_TRAJETO_ABSURDO = 60 * 60 * 4;
export const MAX_DIST_REVIVER_TRAJETO = 100;

export const TIMEOUT_POS_VELHA = 10; // secs
export const MAX_SOMA_TEMPO_PAUSA = 60; // Tempo em minutos 
export const MAX_DIST_PAUSA = 75;
export const NUM_MAX_PAUSAS = 3;
export const ANTECEDENCIA_LEMBRETE_TERMINE_PAUSA = 15; // Tempo em minutos 

/**
 * Formata um número na notação de uma moeda
 * @param {number} valor - valor a ser convertido 
 * @param {String} moeda - nome oficial da moeda a ser formatada
 * @param {String} lingua - linguagem da formatação 
 * @returns - String com o valor formatado
 */
export function FormataMoeda(valor,moeda="BRL",lingua="pt-br"){
	let MoedaFormat= new Intl.NumberFormat(lingua, {
		style: "currency",
		currency: moeda,
	});
	return MoedaFormat.format(valor).replace(/\u00A0/, " "); 
}

/**
 * Recebe um valor no formato de moeda (R$ 10,00) e o transforma em float (10.00)
 * @param {string} valor - valor no formato de moeda
 * @returns - float resultante da conversão
 * 
*/
export function MoedaParaFloat(valor){
	const negativo = (valor.charAt(0) === "-") ? -1 : 1;
	const cents = Number(valor.substring(valor.length-2,valor.length)) / 100;
	const reais = Number(valor.substring(0,valor.length-3).split("$")[1].replace(".","").replace(",",""));
	
	return (reais + cents)*negativo;
}


/**
 * Formata uma data do tipo Date para string
 * @param {Date} data - data a ser formatada
 * @param {String} formato - formato do output 
 * @returns - String com o valor formatado
 */
export function FormataData(data,formato="DD/MM"){
	data = new Date(data);
	let dataFormatada = moment(data).format(formato);
	return dataFormatada;
}

/**
 * Converte um dicionário contendo latidude e longitude
 * de uma coordenada para uma lista, onde o primeiro elemento
 * é a longitude e o segundo a latitude
 * @param {Dict} coordDict - dicionário contendo a coordenada 
 * @returns - Lista com longitude e latitude.
 */
export function convCoordDictList(coordDict){

	let [lat,long] = [-1,-1];

	if (coordDict instanceof Array){
		return coordDict;
	}

	if (coordDict.Posicao != null){
		coordDict = coordDict.Posicao;
	} 
	else if (coordDict.Pos != null){
		coordDict = coordDict.Pos;
	}

	if (coordDict.latitude != null){
		lat = coordDict.latitude;
	}
	else if (coordDict.lat != null){
		lat = coordDict.lat;
	}

	if (coordDict.longitude != null){
		long = coordDict.longitude;
	}
	else if (coordDict.long != null){
		long = coordDict.long;
	}
	else if (coordDict.lon != null){
		long = coordDict.lon;
	}
	else if (coordDict.lng != null){
		long = coordDict.lng;
	}

	return [long,lat];

}

/**
 * Cria uma linha geoJSON com as coordenadas a serem usadas para
 * a geolocalização
 * @param {Array} coordenadas - valores da latitude e longitude a serem passadas para coordinates
 * @returns - Linha no formato geoJSON com as informações das coordenadas corretas
 */
export function criaGeoJSONLinha(coordenadas){
	let rota = {
		"type": "FeatureCollection",
		"features": [
			{
				"type": "Feature",
				"properties": {},
				"geometry": {
					"type": "LineString",
					"coordinates": coordenadas,
				}
			}
		]
	};

	return rota;
}

/**
 * Formata uma distância na unidade de medida certa
 * @param {number} distanciaM - distância a ser convertida
 * @returns - String com a distância formatada
 */
export function FormataDistancia(distanciaM){
	let returnDistancia = "";
	let distanciaEmkm = 0;
	if(distanciaM>1000){
		distanciaEmkm = parseFloat((distanciaM/1000).toFixed(2));
		returnDistancia = distanciaEmkm + " km";
	}
	else if(distanciaM >=0){
		returnDistancia = parseFloat(distanciaM.toFixed(0)) + " m";
	}
	return returnDistancia;
}

/**
 * Formata uma duração para String
 * @param {Date} duracaoMs - tempo a ser convertido
 * @returns - String com a duração formatada
 */
export function FormataDuracao(duracaoMs){
	let returnStringMinutos = "";
	let returnStringSegundos = "";
	let returnString = "";

	if(duracaoMs >= 0){
		let segundos = Math.floor(duracaoMs/1000) ;
		let minutos = Math.floor(segundos/60);

		minutos >= 1 ? segundos = 0 : segundos = segundos - minutos * 60;

		if (minutos > 0){
			returnStringMinutos = minutos + " min";
			if (minutos > 1) returnStringMinutos += "s";
		}

		if (segundos > 0) returnStringSegundos = segundos + " s";

		if (returnStringMinutos == "" && returnStringSegundos == ""){
			returnString = "0 s";
		}
		else if (returnStringSegundos == ""){
			returnString = returnStringMinutos;
		}
		else if (returnStringMinutos == ""){
			returnString = returnStringSegundos;
		}
		else if (returnStringMinutos != "" && returnStringSegundos != ""){
			returnString = returnStringMinutos + " " + returnStringSegundos;
		}
	}
	
	return returnString;
}

/**
 * Cria um polígono que se aproxime do formato de um círculo
 * @param {Array} centro - valores eixo X e eixo Y do centro
 * @param {number} raioKm - valor do raio
 * @returns - GeoJSON com as informações a serem usadas para construir o círculo
 */
export function criaShapeCirculo(centro,raioKm){
	const points = 64; 
	var km = raioKm;

	var ret = [];
	var distanceX = km/(111.320*Math.cos(centro[1]*Math.PI/180));
	var distanceY = km/110.574;

	var theta, x, y;
	for(var i=0; i<points; i++) {
		theta = (i/points)*(2*Math.PI);
		x = distanceX*Math.cos(theta);
		y = distanceY*Math.sin(theta);

		ret.push([centro[0]+x, centro[1]+y]);
	}
	ret.push(ret[0]);
	let shape = {
		"type": "FeatureCollection",
		"features": [
			{
				"type": "Feature",
				"properties": {},
				"geometry": {
					"type": "Polygon",
					"coordinates": [ret],
				}
			}
		]
	};
	
	return shape;    
}


/**
 * Calcula a distância de 2 pontos a partir da latitude e longitude
 * @param {Dict} ponto1 - posição inicial do trajeto
 * @param {Dict} ponto2 - posição final do trajeto
 * @returns - distância em metros
 */
//https://www.movable-type.co.uk/scripts/latlong.html
export function DistanciaHaversine(ponto1,ponto2){

	let [lat1,lon1] = [0,0];
	let [lat2,lon2] = [0,0];

	if (ponto1 instanceof Array){
		lat1 = ponto1[0];
		lon1 = ponto1[1];
	}
	else{
		[lon1,lat1] = convCoordDictList(ponto1);
	}

	if (ponto2 instanceof Array){
		lat2 = ponto2[0];
		lon2 = ponto2[1];		
	}
	else{
		[lon2,lat2] = convCoordDictList(ponto2);

	}

	const R = 6371e3; // metros
	const φ1 = lat1 * Math.PI/180; // φ, λ in radians
	const φ2 = lat2 * Math.PI/180;
	const Δφ = (lat2-lat1) * Math.PI/180;
	const Δλ = (lon2-lon1) * Math.PI/180;

	const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
              Math.cos(φ1) * Math.cos(φ2) *
              Math.sin(Δλ/2) * Math.sin(Δλ/2);
	const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	
	return R * c; // metros
}


/**
 * Dada uma lista de localizações e uma coordenada,
 * devolve a localização mais próxima dessa coordenada
 * @param {Array[Array]} locsPossiveis - lista de localizações
 * @param {Array} coordenadaAtual - coordenada atual [longitude,latitude]
 * @returns - localização mais proxima
 */
export function LocalizacaoMaisProxima(locsPossiveis,coordenadaAtual){
	let locMaisProxima = {"distancia":1000000,"localizacao":{},"id":null};
	for (let i in locsPossiveis){

		let coordenadaLocal = locsPossiveis[i][1];

		let latLongLocal = {"Posicao":{"latitude":coordenadaLocal[0],"longitude":coordenadaLocal[1]}};
		let latLongAtual = {"Posicao":{"latitude":coordenadaAtual[1],"longitude":coordenadaAtual[0]}};
		let distancia = DistanciaHaversine(latLongAtual,latLongLocal);
		if (distancia < locMaisProxima["distancia"]){
			locMaisProxima["distancia"] = distancia;
			locMaisProxima["localizacao"] = locsPossiveis[i];
			locMaisProxima["id"] = locsPossiveis[i][0];
		}
	}
	locMaisProxima["posicaoAtual"] = coordenadaAtual;
	return locMaisProxima;
}

/**
 * Dado um trajeto, cria um trajeto Nulo (sem posições)
 * @param {Trajeto} trajetoEnviado 
 * @returns 
 */
export function montaTrajetoParaSalvar(trajetoEnviado) {
	let trajetoParaSalvar = new Trajeto();
	trajetoParaSalvar.inicio = trajetoEnviado.inicio;
	trajetoParaSalvar.fim = trajetoEnviado.fim;
	trajetoParaSalvar.id_origem = trajetoEnviado.id_origem;
	trajetoParaSalvar.id_destino = trajetoEnviado.id_destino;
	return trajetoParaSalvar;
}

/**
 * Dado um trajeto, calcula a distância da entre os pontos
 * de origem e destino
 * @param {Trajeto} trajeto 
 * @returns 
 */
export function calculaDistHaversineTrajeto(trajeto) {
	let qtdPontosTrajeto = 0;
	if (trajeto.posicoes != null){
		qtdPontosTrajeto = trajeto.posicoes.length;
	}

	let distancia = 0;

	if (qtdPontosTrajeto >= 2){
		distancia = DistanciaHaversine(trajeto.posicoes[0],trajeto.posicoes[qtdPontosTrajeto-1]);	
	}

	return distancia;
}


/**
 * Dada uma string e um tamanhoMaximo,
 * devolve a string limitada por esse tamanho, com '...' no final
 * @param {String} string - string a ser limitada
 * @param {number} tamanhoMaximo - tamanho máximo da string
 * @returns - string limitada
 */
export function LimitaExibicaoString(string,tamanhoMaximo=20){
	if (string.length <= tamanhoMaximo){
		return string;
	}
	return (string.substring(0,tamanhoMaximo-3) + "...");
}

/**
 * Dada um apelido de origem e destino
 * devolve a distancia e o trajetoIdeal desse parvalido
 * @param {String} origem - apelido da origem
 * @param {String} destino - apelido do destino
 * @param {object} gDadosLocais - gerenciador de dados locais
 * @returns - [distancia,trajeto]
 */
export function GetDistanciaIdealTrajetoIdeal(id_origem, id_destino, paresValidos){
	let trajeto = {};
	let distancia = 0;

	let ids_pontos = [Math.min(id_origem,id_destino),Math.max(id_origem,id_destino)];

	let key_str = ids_pontos[0] + "&" + ids_pontos[1];
	if (key_str in paresValidos){
		trajeto = paresValidos[key_str]["trajeto"];
		distancia = paresValidos[key_str]["distancia"];
	}

	return [distancia,trajeto];

}

/**
 * Dado um valor em bytes, faz a conversão para mega bytes
 * e arredonda o valor final para 2 casas decimais
 * @param {Integer} bytes - quantidade de bytes
 * @returns - mb
 */
export function bytesParaMb(bytes){
	let mb = parseFloat((bytes/1e6).toFixed(2));
	return mb;
}


/**
 * Dado um id de uma localização, devolve o apelido
 * correspondente
 * @param {Integer} idLocalizacao - id da localização
 * @param {array} listaLocs - lista de locais
 * @returns - apelido
 */
export function GetApelidoLocalizacaoPorId(idLocalizacao, listaLocs) {
	let apelido = "Desconhecido";
	
	for (let i = 0; i < listaLocs.length; i++){
		let loc = listaLocs[i];
		if (loc[0] == idLocalizacao){
			apelido = loc[3];
			return apelido;
		}
	}	
	return apelido;	
}