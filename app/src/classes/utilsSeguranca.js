/*
	Funções utilitárias de segurança do app
*/

import AES from "crypto-js/aes";
import MD5 from "crypto-js/md5";
import Utf8 from "crypto-js/enc-utf8";
import ECB from "crypto-js/mode-ecb";

/**
 * Encripta uma string utilizando o algoritmo AES
 * @param {string} input - valor a ser encriptado 
 * @param {String} chave - chave de criptografia
 * @returns - String encriptada
 */
export function encripta(input,chave){

	let key = Utf8.parse(chave);
	let encrypted = AES.encrypt(input, key, {mode: ECB});
	return encrypted.toString();
}

/**
 * Gera valor de integridade para um dicionário utilizando o algoritmo MD5.
 * @param {string} dict - dicionário de entrada 
 * @param {String} chave - chave fornecida como 'salt'
 * @returns - hash de integridade
 */
export function geraValorIntegridade(dict,chave){
	/*
		Passo a passo:
		- Junte todos os pares chave+valor do dicionário em uma lista
		- Ordene a lista
		- Concatene cada elemento da lista em uma única string
		- Adicione a chave antes dessa string
		- Aplique o algoritmo de md5
	*/
	let pares_key_valor = [];
	for (const [key,valor] of Object.entries(dict)){
		pares_key_valor.push(String(key) + String(valor));
	}
	pares_key_valor.sort();
	let texto = pares_key_valor.join("");
	let to_hash = chave + texto;
	let integridade = MD5(to_hash).toString();
	return integridade;
}