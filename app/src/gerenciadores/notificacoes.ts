import notifee, {
	AndroidCategory,
	AndroidImportance,
} from "@notifee/react-native";

const ID_CANAL = "bikesp";
const NOME_CANAL = "Bike SP";

// const TEMPO_LEMBRETE_FINALIZAR_VIAGEM = 60 * 60 * 1000; // 1 hora, em milisegundos

const COR_NOTIF = "#f6f7e9";

class GerenciadorNotificacoes {
	private id_viagem_perdida: string | null = null;
	private id_finalizar_viagem: string | null = null;
	private id_finalizar_pausa: string | null = null;
	private id_nao_pode_continuar: string | null = null;

	constructor() {}

	async criaCanalDeNotificacoes() {
		await notifee.createChannel({
			id: ID_CANAL,
			name: NOME_CANAL,
		});
	}

	async criarNotifLembraFinalizarViagem() {
		this.id_finalizar_viagem = await notifee.displayNotification({
			title: "Viagem em andamento",
			body: "Não se esqueça de finalizar sua viagem.",
			android: {
				channelId: ID_CANAL,
				smallIcon: "iconenotif",
				color: COR_NOTIF,
				ongoing: true,
			},
		});
	}

	cancelaLembraFinalizarViagem() {
		if (this.id_finalizar_viagem)
			notifee.cancelNotification(this.id_finalizar_viagem);
	}

	async criarNotifUltimaViagemPerdida() {
		this.id_viagem_perdida = await notifee.displayNotification({
			title: "Viagem não pôde ser retomada",
			body: "Infelizmente a última viagem foi perdida.",
			android: {
				channelId: ID_CANAL,
				smallIcon: "iconenotif",
				color: COR_NOTIF,
			},
		});
	}

	/**
   * @param motivo Motivo a ser exibido na notificação de que a viagem não pôde ser retomada.
   */
	async criaNotifNaoPodeContinuar(motivo: string) {
		this.id_nao_pode_continuar = await notifee.displayNotification({
			title: "Viagem não pôde ser retomada",
			body: "Sua viagem não pôde ser retomada. " + motivo,
			android: {
				channelId: ID_CANAL,
				smallIcon: "iconenotif",
				color: COR_NOTIF,
			},
		});
	}

	/**
   * Cria uma notificação de lembrete de alta prioridade para finalizar a pausa com um temporizador.
   * @param restante tempo restante de pausa (em milisegundos)
   */
	async criaNotifLembraFinalizarPausa(restante: number) {
		console.log(JSON.stringify(new Date(Date.now() + restante)));
		this.id_finalizar_pausa = await notifee.displayNotification({
			title: "Pausa em andamento",
			body: "Não se esqueça de retomar sua viagem em breve.",
			android: {
				channelId: ID_CANAL,
				smallIcon: "iconenotif",
				color: COR_NOTIF,
				ongoing: true,
				showChronometer: true,
				chronometerDirection: "down",
				timestamp: Date.now() + restante,
				importance: AndroidImportance.HIGH,
				category: AndroidCategory.REMINDER,
			},
		});
	}

	cancelaNotifLembraFinalizarPausa() {
		if (this.id_finalizar_pausa)
			notifee.cancelNotification(this.id_finalizar_pausa);
	}

	/**
   * Usada para emitir notificações vindas do Firebase Cloud Messaging.
   * @param notification objeto de notificação a ser exibido
   * @returns o id da notificação emitida
   */
	async mostraNotificacaoRemota(notification: {
    title?: string;
    subtitle?: string;
    body?: string;
  }): Promise<string> {
		const notif = {
			title: notification.title ?? "Bike SP",
			subtitle: notification.subtitle,
			body: notification.body,
			android: {
				channelId: ID_CANAL,
				smallIcon: "iconenotif",
				color: COR_NOTIF,
				importance: AndroidImportance.HIGH,
				pressAction: {id: "default"},
			},
		};
		return await notifee.displayNotification(notif);
	}
}

export {GerenciadorNotificacoes};
