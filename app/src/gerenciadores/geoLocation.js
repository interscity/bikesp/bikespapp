import NativeGeolocation from "../modulos/NativeGeolocation";
import { LaunchArguments } from "react-native-launch-arguments";
const args = LaunchArguments.value();

var intervaloEntreUpdate = 5000;
var mockPosition = false;
var posicoes = [
	{lat:23,lng:23,acc:1, spd: 0, timeStamp: ""},
	{lat:43.0005,lng:23,acc:1, spd: 0, timeStamp: ""},
	{lat:66.0010,lng:33,acc:1, spd: 0, timeStamp: ""},
	{lat:-26.0010,lng:-33,acc:1, spd: 0, timeStamp: ""},
	{lat:-56.0010,lng:-33,acc:1, spd: 0, timeStamp: ""},
	{lat:-76.0010,lng:-23,acc:1, spd: 0, timeStamp: ""}
];

var idx = 0;

if (Object.keys(args).length != 0) {
	mockPosition = args.mockPosition;
	posicoes = args.positions;
	intervaloEntreUpdate = args.interval? args.interval : 5000;
}

class GeoLocation{
	constructor(){
		this.estadoMarco0 = {
			latitude : -23.5503343,
			longitude : -46.6340507,
			acuracia:0,
			data: new Date()			
		};
		this.estadoAtual = {
			latitude : this.estadoMarco0["latitude"],
			longitude: this.estadoMarco0["longitude"],
			acuracia: this.estadoMarco0["acuracia"],
			data: this.estadoMarco0["data"]
		};
		this.callbacksSucesso = {};
		this.callbacksFracasso = {};
		this.emMedicao = false;
	}

	descobrePermissoes(callback){
		NativeGeolocation.retrievePermissions(callback, (e) => {console.log(e);});
	}

	pedePermissoes(callback){
		// isso deve devolver alguma coisa
		// Geolocation.requestAuthorization();

		NativeGeolocation.requestPermissions(["FINE"],(fine) => {
			// console.log("Depois do pedido:");
			// NativeGeolocation.retrievePermissions(() => {
			// console.log("COARSE:",a);
			// console.log("FINE:",b);
			// console.log("BACKGROUND",c);
			// },(e) => {console.log(e)});
			callback(fine);
		},(e) => {console.log(e);});
		
	}

	gpsDisponivel(){
		return NativeGeolocation.isGPSAvaliable();
	}

	estaComOtimizacaoBateria(){
		return !NativeGeolocation.isIgnoringBatteryOptimizations();
	}

	abreConfigOtimizacaoBateria(){
		NativeGeolocation.promptBatteryOptimizationsSettings();
	}

	atualizaNucleo(usarGooglePlayServices){
		NativeGeolocation.updateCore(usarGooglePlayServices);
	}

	pegarPosicaoE(callbackSucesso,callbackErro, timeoutPodre, timeout){
		NativeGeolocation.getUrgentPosition((evento) => {
			callbackSucesso(evento);
			this.sucesso(this,evento);
		}, () => {callbackErro();}, timeoutPodre,timeout);
	}

	iniciarMedicao(){
		console.log("INICIEI MEDIÇÃO");
		// Com o app ressuscitado, a medição parece ainda estar rodando, o que impediria retomarmos
		// um trajeto
		// throw new Error("[ERRO]: Já existe uma medição em andamento.");
		this.emMedicao = true;

		if (mockPosition){
			idx = 0;
			this.sucesso(this,posicoes[0]);
		}else{
			// NativeGeolocation.getSinglePosition((evento) => {
			// 	console.log("Oba chewgou só uma pos: ", evento);
			// }, () => {this.fracasso;});
			NativeGeolocation.getUrgentPosition((evento) => {this.sucesso(this,evento);},() => {this.fracasso();}, 120);
			NativeGeolocation.trackPosition((evento) => {this.sucesso(this,evento);}, () => {this.fracasso();}, intervaloEntreUpdate,0, true);
		}
	}

	finalizarMedicao(){

		if (!this.emMedicao) throw new Error("[ERRO]: Não existe nenhuma medição em andamento.");
		NativeGeolocation.stopTracking();
		console.log("FINALIZEI MEDIÇÃO");
		this.emMedicao = false;
	}

	sucesso(eu,evento){
		if (!this.emMedicao || evento.timeStamp < this.estadoAtual.data) return;
		eu.estadoAtual.latitude = evento.lat;
		eu.estadoAtual.longitude = evento.lng;
		eu.estadoAtual.acuracia = evento.acc;
		eu.estadoAtual.data = evento.timeStamp;
		this.estadoAtual = eu.estadoAtual;
		
		console.log("Medição:",this.estadoAtual);
		// console.log("Accuracy",evento.acc);

		for (let id in this.callbacksSucesso) {
			this.callbacksSucesso[id](this.estadoAtual);
		}

		if (mockPosition){
			setTimeout(function() {
				idx++;
				eu.sucesso(eu,posicoes[idx % posicoes.length]);
			}, intervaloEntreUpdate);
		}

	}

	fracasso(){
		console.log("Fracasso ao tentar medir posição");

		for (let id in this.callbacksFracasso) {
			this.callbacksFracasso[id]();
		}

	}

	getEstadoAtual(){
		return this.estadoAtual;
	}
	getPosicaoAtual(){
		return {latitude : this.estadoAtual.latitude, longitude : this.estadoAtual.longitude};
	}

	adicionaCallbackSucesso(c,id){
		this.callbacksSucesso[id] = c ;
	}

	adicionaCallbackFracasso(c,id){
		this.callbacksFracasso[id] = c ;
	}

	ligaOtimizacoesEnergia(){
		NativeGeolocation.enablePowerSavings();
	}

	desligaOtimizacoesEnergia(){
		NativeGeolocation.disablePowerSavings();
	}

	estouRastreando(){
		return NativeGeolocation.isCurrentlyTracking();
	}
}

export {GeoLocation};