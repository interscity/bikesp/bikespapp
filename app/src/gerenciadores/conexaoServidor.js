import FormData from "form-data";
import fetch from "node-fetch";

import { SERVER_HOST, ENCRIPT_PK, CHAVE_INTEGRIDADE } from "@env";

import {encripta,geraValorIntegridade} from "../classes/utilsSeguranca.js";

export const endpointRaiz = "/";
export const endpointCadastro = "/api/cadastrar/";
export const endpointLogin = "/api/login/";
export const endpointLogout = "/api/logout/";
export const endpointRegistraViagem = "/api/regViagem/";
export const endpointContestaViagem = "/api/contestaViagem/";
export const endpointChecaSessao = "/api/checaSess/";
export const endpointSincronizaViagens = "/api/sincronizaViagens/";
export const endpointSincronizaLocs = "/api/sincronizaLocs/";
export const endpointViagensFaltantes = "/api/viagensFaltantes/";
export const endpointExtrato = "/api/geraExtrato/";
export const endpointParesValidos = "/api/recebeParesValidos/";
export const endpointExibeBonus = "/api/exibeBonus/";
export const endpointExibeContestacoes = "/api/exibeContestacoes/";
export const endpointValidaLocalizacoes = "/api/validaLocs/";
export const endpointAlteraLocalizacao = "/api/alteraLocalizacao/";
export const endpointChecaAlteraLocalizacao = endpointAlteraLocalizacao + "checa/";
export const endpointRegTokenNotificao = "/api/notificacoes/regToken/";
export const endpointEsqueceuSenha = "/esqueceuSenha/";
export const endpointTrocaBU = "/trocaBU/";


function limpaDocumento(stringDocumento){
	return stringDocumento.replace(/[\D-]/g, "");
}

function montaDadosRequest(dados,tipoRequest="form"){

	dados["integridade"] = geraValorIntegridade(dados,CHAVE_INTEGRIDADE);

	if (tipoRequest == "form"){
		return montaFormDadosRequest(dados);
	}
	else if (tipoRequest == "json"){
		return JSON.stringify(dados);
	}
	else{
		throw new Error("Tipo de requisição inválida:",tipoRequest);
	}
}

function montaFormDadosRequest(dictValores){
	let dadosForm = new FormData();

	for(const chave in dictValores){
		dadosForm.append(chave,dictValores[chave]);
	}

	return dadosForm;
}

console.log("SERVER_HOST:",SERVER_HOST);

class InterfaceConexao{

	constructor(SERVER_HOST){
		this.SERVER_HOST = SERVER_HOST;
	}

	/*
		Devolve a resposta de uma requisição para
		determinada URL
	*/
	async enviaRequisicao(url,requisicao){
		const controller = new AbortController();
		const id = setTimeout(() => controller.abort(), 5000);
		requisicao.signal = controller.signal;

		const response = await fetch(url, requisicao).then((resp) => {
			return resp;
		});
		clearTimeout(id);

		return response;
	}

	/* 
		Faz uma requisição para certa URL e invoca o
		callbacks de sucesso (se status code == 200) e
		o callback de erro caso contrário.
	*/
	async gerenciaRequisicao(url,requisicao,callbackSucesso,callbackErro){
		try {

			let respostaServidor = await this.enviaRequisicao(url,requisicao);
			if (respostaServidor.status == 200){
				return await callbackSucesso(respostaServidor);
			}
			return await callbackErro(respostaServidor);

		}
		catch (e) {
			console.log("Erro ao fazer requisição para",url,":",e);
			return [false,"Problema de Conexão"];
		}		
	}

	async callbackErroGenerico(respostaServidor){
		console.log("Requisição não pôde ser enviada para", respostaServidor.url, "(" + respostaServidor.status + ")");
		let respostaApp = traduzStatusParaTexto(respostaServidor.status); // Resposta default de acordo com status code
		try {
			let respJson = await respostaServidor.json();
			respostaApp = respJson.erro;
		}
		catch {/* empty */}
		
		return [false,respostaApp];		
	}

	async callbackSucessoGenerico(respostaServidor) {
		let respJson = await respostaServidor.json();
		return [true,respJson];
	}

	async tentaCadastro(email, cpf, bu, senha){
		let senhaHash = encripta(senha,ENCRIPT_PK);

		let dadosRequest = {
			"email": email,
			"cpf": limpaDocumento(cpf),
			"bu": limpaDocumento(bu),
			"senha": senhaHash,
		};

		dadosRequest = montaDadosRequest(dadosRequest,"form");

		let url = this.SERVER_HOST + endpointCadastro;
		let requisicao = {method: "POST", body: dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async tentaLogin(email, senha,versaoApp=""){

		async function callbackSucessoLogin(respostaServidor){
			let respJson = await respostaServidor.json();
			return [true,
				{
					"token":respJson.token,
					"validadeToken":respJson.validade,
					"bu":respJson.bu,
					"CPF":respJson["CPF"],
					"locs":respJson.locs,
					"validouLocs":respJson.validouLocs
				}]; 
		}

		let senhaHash = encripta(senha,ENCRIPT_PK);

		let dadosRequest = {
			"email": email,
			"senha": senhaHash,
			"versao": versaoApp,
		};

		dadosRequest = montaDadosRequest(dadosRequest,"form");

		let url = this.SERVER_HOST + endpointLogin;
		let requisicao = {method: "POST", body: dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,callbackSucessoLogin,this.callbackErroGenerico);
		return [sucesso,respostaApp];

	}

	async logout(email, token){

		let dadosRequest = {
			"email": email,
			"token": token,
		};

		dadosRequest = montaDadosRequest(dadosRequest,"form");

		let url = this.SERVER_HOST + endpointLogout;
		let requisicao = {method: "POST", body: dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async tentaRedefinirSenha(email) {

		async function callbackSucessoRedefinir(respostaServidor){
			return [true,respostaServidor];
		}

		let url = this.SERVER_HOST + endpointEsqueceuSenha + "?email=" + email;
		let requisicao = {method: "GET"};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao, callbackSucessoRedefinir, this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async requisitaTrocaBU(cpf,token) {

		async function callbackSucessoTrocaBU(respostaServidor){
			return [true,respostaServidor];
		}

		let url = this.SERVER_HOST + endpointTrocaBU + "?cpf=" + cpf + "&token=" + token;
		let requisicao = {method: "GET"};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao, callbackSucessoTrocaBU, this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	/**
	 * Função responsável por testar se uma sessão aina é válida.
	 * DEVOLVE APENAS UM BOOLEANO, QUE É FALSO SOMENTE SE ELA FOR INVALIDADA
	 * PELO SERVIDOR
	 */
	async validaSessao(email, token, versaoApp=""){

		let dadosRequest = {
			"email": email,
			"token": token,
			"versao": versaoApp,
		};

		dadosRequest = montaDadosRequest(dadosRequest,"form");

		let url = this.SERVER_HOST + endpointChecaSessao;
		let requisicao = {method: "POST", body: dadosRequest};
		let respostaRequisicao = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico).then( (resposta) => {
			if(resposta[1] == "App desatualizado"){
				return [false, "Seu aplicativo está desatualizado. Atualize-o para continuar."];
			}else if(resposta[1] == "Sessao Invalida"){
				return [false, resposta[1]];
			}else{
				return [true, resposta[1]];
			}
		} );
		// console.log("Resultado de validar sessao:",respostaRequisicao[0]);
		return respostaRequisicao;
	}

	async enviaViagem(cpf,token,objViagem){
		
		let dadosRequest = {
			"user": cpf,
			"token": token,
			"viagem": JSON.stringify(objViagem)
		};

		dadosRequest = montaDadosRequest(dadosRequest,"form");

		let url = this.SERVER_HOST + endpointRegistraViagem;
		let requisicao = {method: "POST", body: dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async enviaContestacao(cpf,token,objContestacao){

		let dadosRequest = {
			"user":cpf,
			"token":token,
			"contestacao":JSON.stringify(objContestacao)
		};

		dadosRequest = montaDadosRequest(dadosRequest,"json");

		let url = this.SERVER_HOST + endpointContestaViagem;
		let requisicao = {method:"POST",body:dadosRequest,headers: {"Accept": "application/json","Content-Type": "application/json"}};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async sincronizaViagens(cpf,token){
		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointSincronizaViagens;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}


	async viagensFaltantes(cpf,token,listaViagensFaltantes){
		let dadosRequest = {
			"user":cpf,
			"token":token,
			"idsViagens":JSON.stringify(listaViagensFaltantes)
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointViagensFaltantes;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async requisitaExtrato(cpf,token){
		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointExtrato;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async requisitaParesValidos(cpf,token){
		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointParesValidos;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async exibeBonus(cpf,token){
		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointExibeBonus;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async exibeContestacoes(cpf,token){

		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");

		let url = this.SERVER_HOST + endpointExibeContestacoes;

		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}
	
	async sincronizaLocalizacoes(cpf,token){
		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointSincronizaLocs;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async validaLocalizacoes(cpf,token,locsErradas){
		let dadosRequest = {
			"user":cpf,
			"token":token,
			"locsErradas":JSON.stringify(locsErradas)
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointValidaLocalizacoes;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async alteraLocalizacao(cpf, token, idLocalizacao, novoEndereco, novoApelido){
		let dadosRequest = {
			"user":cpf,
			"token":token,
			"id_localizacao":idLocalizacao,
			"novo_endereco":novoEndereco,
			"novo_apelido":novoApelido
		};
		dadosRequest = montaDadosRequest(dadosRequest, "form");
		let url = this.SERVER_HOST + endpointAlteraLocalizacao;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async checaAlteraLocalizacao(cpf, token){
		let dadosRequest = {
			"user":cpf,
			"token":token
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointChecaAlteraLocalizacao;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}

	async atualizaTokenNotificacao(cpf, token, tokenNotificacao){
		let dadosRequest = {
			"user":cpf,
			"token":token,
			"tokenNotif":tokenNotificacao
		};
		dadosRequest = montaDadosRequest(dadosRequest,"form");
		let url = this.SERVER_HOST + endpointRegTokenNotificao;
		let requisicao = {method:"POST",body:dadosRequest};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,this.callbackSucessoGenerico,this.callbackErroGenerico);
		return [sucesso, respostaApp];
	}

	async ping(){
		let url = this.SERVER_HOST + endpointRaiz;
		let requisicao = {method:"GET"};
		let [sucesso,respostaApp] = await this.gerenciaRequisicao(url,requisicao,() => [true, "Requisição feita com sucesso"],this.callbackErroGenerico);
		return [sucesso,respostaApp];
	}
}

function traduzStatusParaTexto(codigo){
	let parseado = "Problema de Conexão";
	switch (codigo){
	case 200:
		parseado = "Requisição feita com sucesso";
		break;
	case 429:
		parseado = "Limite de requisições atingido";
		break;
	case 403:
		parseado = "Problema de autenticação";
		break;
	case 401:
		parseado = "Problema de autenticação";
		break;
	case 400:
		parseado = "Problema com a requisição";
		break;
	default:
		parseado = "Problema de Conexão";
	}
	return parseado;
}

const conexao = new InterfaceConexao(SERVER_HOST);
export default conexao;