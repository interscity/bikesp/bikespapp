import AsyncStorage from "@react-native-async-storage/async-storage";
import { Viagem } from "../classes/viagem";
import { deslogaUsuario } from "../acoes/deslogaUsuario.js";

import { 
	Alert,
} from "react-native";

import {GetDistanciaIdealTrajetoIdeal,MoedaParaFloat} from "../classes/utils.js";

const idUltimoReset = "00";
const chavesReset = ["DESLOGAR","Viagens","ParesValidos"];

class GerenciadorDadosLocais {
	constructor(){
		this.chaves = [
			"Viagens",
			"EmViagem",
			"TrajetoAntigo",
			"NomeUsuario",
			"Logado",
			"TokenUser",
			"ValidadeToken",
			"BilheteUnico",
			"Locais",
			"MostrouInstrucionalCadastro",
			"MostrouInstrucionalHome",
			"PrimeiraExecucao",
			"PrimeiraViagem",
			"UltimoUsuario",
			"CPF",
			"Extrato",
			"UltimaViagemFalha",
			"ParesValidos",
			"UsandoGoogleServices",
			"UsandoActivities",
			"UltimoReset",
			"Bonus",
			"Contestacoes",
			"ValidouLocs",
		];
		this.valoresPadrao = {
			"Viagens" : "[]",
			"EmViagem" : "f",
			"TrajetoAntigo" : "{}",
			"NomeUsuario": "",
			"Logado": "f",
			"TokenUser": "",
			"ValidadeToken": "",
			"BilheteUnico":"",
			"CPF":"",
			"Locais" : "[]",
			"MostrouInstrucionalCadastro" : "f",
			"MostrouInstrucionalHome" : "f",
			"PrimeiraExecucao" : "t",
			"PrimeiraViagem" : "t",
			"UltimoUsuario":"",
			"UltimaViagemFalha" : "",
			"Extrato":"{\"bilheteUnico\":[],\"creditos\":[]}",
			"ParesValidos":"[]",
			"UsandoGoogleServices":"t",
			"UsandoActivities":"t",
			"UltimoReset": "",
			"Bonus":"[]",
			"Contestacoes":"[]",
			"ValidouLocs":"f",
		};
		this.variaveisLocais = {};
		this.viagensLocais = [];
		this.carregouDisco = false;
	}

	getDadosGerais(){
		const ultimosDias = 7;
		let distanciaPercorrida = 0;
		let ganhoTotal = 0;
		let listaViagens = this.getListaViagens();

		let hoje = new Date();

		const paresValidos = JSON.parse(this.getLocalmenteMem("ParesValidos"));


		for (let viagem of listaViagens){
			let dia = new Date(viagem.data);
			let dif = (hoje - dia)/(1000 * 60 * 60 * 24);
			if ( dif > ultimosDias || viagem.statusRemuneracao != "Aprovado") continue;

			let distanciaEtrajeto = GetDistanciaIdealTrajetoIdeal(viagem.idOrigem,viagem.idDestino,paresValidos);

			distanciaPercorrida += distanciaEtrajeto[0];

			ganhoTotal += viagem.remuneracao;
		}

		return {"distanciaPercorrida": distanciaPercorrida, "dias": ultimosDias, "ganhoTotal": ganhoTotal};
	}

	getInfoViagens(){

		let listaViagens = this.getListaViagens();
		let recebidos = 0;
		let areceber = 0;
		for (let viagem of listaViagens){
			if (viagem.statusRemuneracao == "Aprovado"){
				recebidos += viagem.remuneracao;
			}
			else if (viagem.statusRemuneracao == "EmAnalise"){
				areceber += viagem.remuneracao;
			}
		}
		return {"recebidos":recebidos,"areceber":areceber};
	}

	getTotaisRemuneracoes(){
		let extratoLocal = JSON.parse(this.getLocalmenteMem("Extrato"));
		let recebidos = 0;
		let areceber = 0;

		for (let i = 0; i < extratoLocal["bilheteUnico"].length;i++){
			let entrada = extratoLocal["bilheteUnico"][i];
			recebidos += MoedaParaFloat(entrada[4]);
			areceber += MoedaParaFloat(entrada[5]);
		}

		return {"recebidos":recebidos, "areceber":areceber};

	}

	getListaViagens(){
		// Recupera lista de viagens
		let listaViagens = this.viagensLocais;

		listaViagens.sort((a,b) => (new Date(b.data) - new Date(a.data)));
		return listaViagens;
	}

	/**
	 * Devolve as viagens que ainda não foram enviadas ao servidor.
	 * @returns Lista de objetos da classe Viagem
	 */
	getViagensNaoEnviadas(){

		let viagensNaoEnviadas = this.viagensLocais.filter((viagem) => (
			viagem.statusEnvio == "NaoEnviado"
		));

		return viagensNaoEnviadas;

	}

	salvaViagem(viagem){
		if (this.viagensLocais.indexOf(viagem) == -1){
			this.viagensLocais.push(viagem);
		}
	}

	removeViagem(viagem){
		const index = this.viagensLocais.indexOf(viagem);
		if (index != -1){
			this.viagensLocais.splice(index, 1);
		}
	}

	resetaViagens(){
		this.viagensLocais = [];
	}

	resetaLocalmenteMem(chave){
		if (chave == "Viagens"){
			this.resetaViagens();
		}
		this.setLocalmenteMem(chave,this.valoresPadrao[chave]);
	}

	async checaReset(){
		if (this.variaveisLocais["UltimoReset"] < idUltimoReset){
			console.log("Devo resetar dados");

			for (let chaveAResetar of chavesReset) {
				if(chaveAResetar == "DESLOGAR" && this.variaveisLocais["Logado"] == "t"){
					await deslogaUsuario(this,true);
				}else{
					this.resetaLocalmenteMem(chaveAResetar);
				}
			}

			this.setLocalmenteMem("UltimoReset",idUltimoReset);
		}
	}

	/**
	 * Carrega a lista de objetos do tipo Viagem a partir da string lida do disco
	 * @param {String} strLista 
	 */
	carregaListaViagens(strLista){

		let listaViagens = strLista;
		if (listaViagens == null){
			// console.log("Lista de viagens ainda não lida");
			listaViagens = [];
		}
		else{
			let listaObjViagens = JSON.parse(listaViagens);
			listaViagens = [];
			for(const viagemObj of listaObjViagens){
				// console.log("vou fazer o push");
				listaViagens.push(Viagem.aPartirDeJSON(viagemObj));
			}
		}

		this.viagensLocais = listaViagens;
	}

	async carregaDadosDisco(callback,callbackProgresso){
		this.carregouDisco = false;
		let idx = 1;
		for (let i in this.chaves){
			let chave = this.chaves[i];
			
			try {
				this.variaveisLocais[chave] = await this.getLocalmenteDisco(chave);
			}
			catch (erro){
				let stringAlert = "ERRO RECUPERANDO CHAVE: " + chave + "\n" + erro;
				Alert.alert(
					"ERRO",
					stringAlert,
					[{text: "Ok"}]
				);
			}
			
			if (callbackProgresso != undefined) callbackProgresso(100*idx/Object.keys(this.chaves).length);
			idx++;
		}
		
		this.carregaListaViagens(this.variaveisLocais["Viagens"]);

		await this.checaReset();

		this.carregouDisco = true;
		console.log("Acabei de carregar., Lista de viagens: ", this.listaViagens);
		callbackProgresso(100);
		callback();
	}

	async salvaDadosDisco(){
		for (let i in this.chaves){
			let chave = this.chaves[i];
			let valor;
			if (this.variaveisLocais[chave] == null){
				continue;
			}else if(chave == "Viagens"){
				if(this.viagensLocais.length == 0){
					valor = this.variaveisLocais["Viagens"];
				}else{
					let viagensJson = this.viagensLocais.map((v) => v.paraJSON());
					valor = "[" + String(viagensJson) + "]";
				}
			}else{
				valor = this.variaveisLocais[chave];
			}
			await this.setLocalmenteDisco(chave,valor);
		}
	}

	getLocalmenteMem(chave){
		return this.variaveisLocais[chave];
	}

	setLocalmenteMem(chave,valor){
		this.variaveisLocais[chave] = valor;
	}

	removeLocalmenteMem(chave){
		delete this.variaveisLocais[chave];
	}

	async resetaMemDisco(){
		console.log("RESETA MEMÓRIA E DISCO");
		let keysNotToRemove = ["UltimoUsuario","UltimoReset"];
		for (let i in this.chaves){
			let chave = this.chaves[i];
			if (keysNotToRemove.includes(chave)) continue;
			this.variaveisLocais[chave] = this.valoresPadrao[chave];
		}

		this.viagensLocais = [];
		this.carregouDisco = false;

		AsyncStorage.getAllKeys()
			.then(keys => {
				let keysToRemove = [];
				for (let i = 0; i < keys.length;i++){
					if (keysNotToRemove.includes(keys[i])) continue;
					keysToRemove.push(keys[i]); 
				}
				AsyncStorage.multiRemove(keysToRemove);
			});
	}

	async setLocalmenteDisco(chave,valor){
		try {
			await AsyncStorage.setItem(chave,valor);
			// console.log("Salvo com sucesso: " + chave + " = " + valor);
		}
		catch (erro) {
			throw new Error("[ERRO]: Erro salvando localmente - " + erro);
		}

	}
	async getLocalmenteDisco(chave){
		try {
			let valor = await AsyncStorage.getItem(chave);
			if(valor == null){
				// console.log("Achei null no disco pra chave " + chave);
				valor = this.valoresPadrao[chave];
			}
			// console.log(chave + " = " + valor)
			return valor;
		}
		catch (erro) {
			throw new Error("[ERRO]: Erro recuperando dado local - " + erro);
		}
		
	}
	async removeLocalmenteDisco(chave){
		try {
			await AsyncStorage.removeItem(chave);
			// console.log(chave + " removida com sucesso");
		}
		catch (erro){
			throw new Error("[ERRO]: Erro removendo dado local - " + erro);
		}
	}
}

export {GerenciadorDadosLocais};