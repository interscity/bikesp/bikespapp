import { CameraAnimationMode } from "@rnmapbox/maps";

class Camera{
	zoom: number;
	animationDuration: number;
	animationMode: CameraAnimationMode;
	pitch: number;
	minZoomLevel: number;
	maxZoomLevel: number;

	constructor() {
		this.zoom = 15;
		this.animationDuration = 0;
		this.animationMode = "flyTo";
		this.pitch = 20;
		this.minZoomLevel = 10;
		this.maxZoomLevel = 18;
	}
	
	setZoomLevel(valor: number){
		if (valor < this.minZoomLevel) {
			valor = this.minZoomLevel;
		} else if (valor > this.maxZoomLevel) {
			valor = this.maxZoomLevel;
		}

		this.zoom = valor;
	}
	getZoomLevel(){
		return this.zoom;
	}
	getPitch(){
		return this.pitch;
	}
	getMaxZoomLevel(){
		return this.maxZoomLevel;
	}
	getMinZoomLevel(){
		return this.minZoomLevel;
	}
	getAnimationDuration(){
		return this.animationDuration;
	}
	getAnimationMode(){
		return this.animationMode;
	}
}

export {Camera};