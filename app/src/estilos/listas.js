import { StyleSheet } from "react-native";

import { 
	corFundo,
	corFundoAux,
	vh,
	vw, 
	em,
} from "./globais";

const listas = StyleSheet.create({
	itemLista: { 
		backgroundColor: corFundo,
		flexDirection: "row",		
		alignItems: "center",
		height: 7.6 * vh, 
		paddingHorizontal: 0.4*em,
		margin: 0.3*em,
		borderRadius: 0.4 *em,
	},


	itemListaViagens: {
		minWidth: 70 * vw,
		maxWidth: 77 * vw,
		justifyContent: "space-between",
	},

	itemListaLocais: {
		minWidth: 75 * vw,
		maxWidth: 80 * vw,
		height: 8 * vh,
		justifyContent: "space-between",
		width: 0
	},

	itemListaBilheteUnico: {
		height: 15 * vh,
		flexDirection: "row",
		justifyContent: "flex-start"
	},

	itemListaBonus: {
		height: 15 * vh,
		width: 85*vw,
		backgroundColor: corFundo,
		paddingHorizontal: 0.4*em,
		paddingVertical: 0.4*em,
		margin: 0.3*em,
		borderRadius: 0.4 *em,
	},
	itemListaContestacoes: {
		height: 17 * vh,
		width: 85*vw,
		backgroundColor: corFundo,
		paddingHorizontal: 0.4*em,
		margin: 0.3*em,
		borderRadius: 0.4 *em,
	},
	containerListaViagens: {
		alignItems: "center",
		minWidth: 80 * vw,
		maxWidth: 95 * vw,
	},
	containerListaLocais: {
		alignItems: "center",
		minWidth: 80 * vw,
		maxWidth: 95 * vw,
		height: 50 * vh,
	},
	containerListaBonus: {
		alignItems: "center",
		width: 90 * vw,
		height: 75 * vh,
	},
	listaViagens: {
		backgroundColor: corFundoAux,
		maxHeight: 45*vh,
		borderRadius: 0.7 *em,
		minWidth: 80 * vw,
		maxWidth: 90 * vw,
	},
	listaLocais: {
		backgroundColor: corFundoAux,
		borderRadius: 0.7 * em,
		minWidth: 75 * vw,
		maxWidth: 80 * vw,
	},

	listaBonus: {
		backgroundColor: corFundoAux,
		borderRadius: 0.7 * em,
	},
});

export {listas};