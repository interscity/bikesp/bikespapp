import {
	Dimensions,
	StyleSheet
} from "react-native";


const corFundo = "rgb(246, 247, 233)";
const corFundoAux = "rgb(242, 238, 218)";
const corFonte = "#000000";
const corSucesso = "#95f089";
const corErro = "#cc0000";
const corAmareloOuro = "#c7a80c";
const corOuroDesbotado = "#efe5b1";
const vh = Dimensions.get("window").height / 100;
const vw = Dimensions.get("window").width / 100;
const em = 2.35 * vh;

const globais = StyleSheet.create({
	shadowProp:{
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 7,
		},
		shadowOpacity: 0.4,
		shadowRadius: 9.11,

		elevation: 14,
	},
	
	dialogPausa : {
		height:   90 * vh,
	},
	setaIndicaClique : {
		fontSize: em * 1.4,
		fontFamily: "Lato-Regular",
		fontWeight: "bold",
		color:corAmareloOuro
	},
});

/**
 * Função que modifica a intensidade de uma cor. 
 * @param {string} cor informação de cor em hexadecimal ("#ffffff") 
 * @param {number} intensidade percentual de mudança de brilho. Positivo deixa a cor clara. Negativo deixa a cor escura
 * @returns cor em hexadecimal com sua intensidade modificada
 */
function ajustaCor(cor, intensidade) {
	return "#" + 
		cor
			.replace(/^#/, "")
			.replace(/../g, 
				cor => (
					"0" + Math.min(255, Math.max(0, parseInt(cor, 16) + intensidade))
						.toString(16))
					.substring(-2));
}

export {
	globais, 
	corFundo, 
	corFundoAux,
	corFonte,
	corErro,
	corSucesso,
	corAmareloOuro,
	corOuroDesbotado,
	vh,
	vw,
	em,
	ajustaCor
};