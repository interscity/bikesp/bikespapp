import { StyleSheet } from "react-native";

import { 
	corFundo,
	corAmareloOuro,
	vw,
	vh,
	em,
	
} from "./globais";

const containeres = StyleSheet.create({
	containerHome:{
		alignItems: "center",
		minWidth: 80 * vw,
		maxWidth: 95 * vw,
		minHeight: 52 * vh,
		maxHeight: 80 * vh,
		justifyContent: "space-around",
		marginVertical:4*vh,
	},
	containerHorizontal : {
		flexDirection: "row",
		justifyContent: "space-around",
		alignItems: "center",
	},
	containerVertical : {
		justifyContent: "space-around",
		alignItems: "center",
	},
	containerInteracaoViagens : {
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignContent: "center",
		marginBottom: 1.5*vh
	},
	tela : {
		backgroundColor: corFundo,
		height : 100*vh,
		width : 100*vw,
		justifyContent: "space-around",
		alignItems: "center",
		paddingBottom: 0.7*em,
	},
	telaScroll: {
		backgroundColor: corFundo,
		minHeight: 100*vh,
		width : 100*vw,
		justifyContent: "space-around",
		alignItems: "center",
		paddingBottom: 0.7*em,
		overflow: "scroll"
	},
	syncContainer: {
		backgroundColor: corFundo,
		display: "flex",
		height: 4*em,
	},
	syncContainerRight: {		
		backgroundColor: corFundo,
		alignSelf: "flex-end",
		flexDirection:"row",
		justifyContent: "flex-end",
		alignItems: "center",
		height: 3*em,
	},
	containerMapa: {
		height: 67*vh,
		width: 100*vw,
		alignSelf:"center", 
		justifyContent:"center"
	},
	containerBotaoContestar:{
		width: 90*vw,
		height: vh*4, 
		marginTop: 3*vh,  
		borderStyle: "solid",
		borderColor: "#fff",
		flexDirection: "row",
		alignItems:"center", 
		justifyContent:"flex-start"
	},
	containerCircularIcon:{
		borderRadius:100, 
		width: vw*22, 
		height: vw*22,
		alignItems:"center",
		justifyContent:"center"
	},
	containerMenu:{
		width: 85*vw, 
		height: 6*vh, 
		marginTop:3*vh, 
		alignItems:"flex-end", 
		justifyContent:"flex-start"
	},
	cadastroInputContainer: {
		backgroundColor:"#ffffff", 
		borderRadius:8, 
		borderColor: corAmareloOuro, 
		borderWidth:1,
		paddingLeft:0.4*em,
		marginBottom: -0.9*em,
	},
	containerEsqueceuSenha: {
		paddingBottom: 1.5*em,
		paddingLeft: 1*em,
		alignSelf: "flex-start"
	},
	emViagemContainer: {
		paddingBottom: 0.8*em,
		paddingTop: 1.4*em,
		minHeight: 20*vh,
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
	},
	loginContainer: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-start"
	},
	maisInfoItemContainer: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		marginBottom: 0.7*em
	},
	containerPaginacao : {
		flexDirection: "row",
		alignItems: "center",
		marginVertical: 0.8*em,
	},
	calloutCirculoPausa : {
		width: 40*vw,
		height: 10*vh,
		borderRadius: 2*em,
		borderColor: "#000000",
		backgroundColor:"#ffffff", 
		justifyContent: "center",
		alignItems: "center",
	},
	containerConfiguracoes: {
		width: 85*vw,
		flexDirection: "column",
		justifyContent: "space-around",
	},
	containerConfiguracoesAninhado: {
		width: 70*vw,
		marginLeft:2*vw,
		marginTop:1.2*vh,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	headerEmViagem:{
		width: 100*vw,
		height: 18*vh,
		backgroundColor:"#95f089", 
		top:0,
		justifyContent: "space-around",
		alignItems: "center",
		display: "flex",
		flexDirection: "row",
		paddingLeft: 5*vw,
		paddingRight: 5*vw,
	},
	headerEmPausa : {
		width: 100*vw,
		height: 18*vh,
		backgroundColor:"#FFD500", 
		top:0,
		justifyContent: "space-around",
		alignItems: "center",
		display: "flex",
		flexDirection: "row",
		paddingLeft: 5*vw,
		paddingRight: 5*vw,
	},
	popUpPermissoes:{
		opacity:1,
		height: vh*75,
		width:vw*80,
		justifyContent:"space-around",
		backgroundColor:"#decc73",
		paddingHorizontal:15,
		borderRadius:20,
		alignContent:"center",
	},
	popUp:{
		flex: 1,
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		opacity:0.8,
		backgroundColor:"black"
	},
	/** Formatação para o componente `Respiro` */
	respiro: {
		height: 3*em,
		width: 3*em,
	},
	/** Formatação para o componente `Respiro` com a propriedade `pequeno` ativa */
	respiro_pequeno: {
		height: 1.5*em,
		width: 1.5*em,
	},
	/** Formatação para o componente `Respiro` com a propriedade `minusculo` ativa */
	respiro_minusculo: {
		height: 0.5*em,
		width: 0.5*em,
	},
	/** Formatação para o componente `Bloco` */
	bloco: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "flex-start",
		width: "95%",
		gap: 0.5*em,
	},
	bloco_centro: {
		alignItems: "center",
		justifyContent: "center"
	},
	bloco_espacado: {
		alignItems: "center",
		justifyContent: "space-between"
	},
	recuo: {
		paddingLeft: 1.5*em,
	},
	/** Formatação adicional para o componente `Bloco` com a propriedade `horizontal` ativa */
	bloco_horizontal: {
		flexDirection: "row",
		flexWrap: "nowrap"
	},
	/** Formatação para o componente `Regiao` */
	regiao: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "flex-start",
		padding: em,
		gap: 2*em
	}
});

export {containeres};