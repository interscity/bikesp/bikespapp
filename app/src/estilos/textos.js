import { StyleSheet } from "react-native";

import { 
	corAmareloOuro,
	corErro,
	corFonte,
	em
} from "./globais";

const textos = StyleSheet.create({
	/** Formatação base usada pelo componente `Texto` e todos demais elementos de texto simples */
	texto : {
		color : corFonte,
		fontSize: em,
		fontFamily: "Lato-Regular",
		textAlign: "justify",
		alignSelf: "flex-start"
	},
	syncTexto: {
		fontSize: 0.9*em,
		marginRight: 0.5*em,
	},
	/** Formatação adicional à formatação `texto` usada pela propriedade `erro` do componente `Texto` */
	erro: {
		color : corErro,
		fontSize: em,
	},
	/** Formatação adicional à formatação `texto` usada pela propriedade `italico` do componente `Texto` */
	italico:{
		fontSize: em,
		fontFamily: "Lato-Italic",
	},
	/** Formatação usada pela propriedade `negrito` de `Texto` */
	negrito : {
		fontSize: em,
		fontFamily: "Lato-Bold"
	},
	/** Formatação adicional à formatação `texto` usada pela propriedade `redireciona` do componente `Texto` */
	redireciona:{
		color : corAmareloOuro,
		textDecorationLine: "underline"
	},
	/** Formatação usada pelo componente `Titulo` */
	titulo:{
		color : corFonte,
		fontSize: 1.5*em,
		fontFamily: "Lato-Bold",
		textAlign: "center",
		padding: 1.5*em,
	},
	/** Formatação usada pelo componente `Subtitulo` */
	subtitulo : {
		color: corFonte,
		fontSize : 1.2 * em,
		minHeight: 2 * em,
		textAlignVertical: "center",
		fontFamily: "Lato-Bold",
	},
	/** Formatação usada pelo componente `Label` */
	label: {
		color: corFonte,
		alignSelf: "flex-start",
		fontSize: em,
		fontFamily: "Lato-Regular",
		textAlign: "left",
		paddingLeft: em,
		paddingBottom: 0.3*em,
	},
});

export { textos };