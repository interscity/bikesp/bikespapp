import { StyleSheet } from "react-native";

import { 
	corFundoAux,
	corAmareloOuro, 
	corOuroDesbotado,
	vw, 
	em,
} from "./globais";

const botoes = StyleSheet.create({
	/** Formatação geral para botões */
	botao: {
		backgroundColor: corFundoAux,
		borderColor: corOuroDesbotado,
		borderRadius: 1.25*em,
		borderStyle: "solid"
	},
	/** Formatação adicional geral para componentes que podem ser inativados */
	inativo: {
		opacity: 0.5
	},
	/** Formatação adicional geral para componentes que podem ser pressionados */
	pressionado: {
		size: 1.1,
		opacity: 0.9
	},
	
	switchAtivo:{
		track: corAmareloOuro,
		thumb: corAmareloOuro,
	},
	switchInativo:{
		track: corOuroDesbotado,
		thumb: corOuroDesbotado,
	},
	botaoNav : {
		fontFamily: "Lato-Bold",
		backgroundColor: corFundoAux,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		alignSelf: "center",
		minHeight: 2.5 * em,
		maxWidth: 80 * vw,
		paddingHorizontal: 0.9*em,
		borderWidth: 0.125 * em,
		borderColor: corAmareloOuro,
		borderRadius: 1.5*em,
	},
	botaoSync : {
		fontFamily: "Lato-Bold",
		backgroundColor: "#ebe9e1",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		alignSelf: "center",
		minHeight: 1.2 * em,
		maxWidth: 80 * vw,
		paddingHorizontal: 0.9*em,
		borderRadius: 1*em,
	},
	botaoNavHome: {
		paddingHorizontal: em,
		paddingVertical:0.2*em,
		borderWidth: 0.16* em,
		fontSize : 1.5 * em,
		fontFamily: "Lato-Bold",
	},
	botaoFinalizar:{
		backgroundColor:"#de887c",
		borderWidth: 0.125 * em,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		fontSize : 1.5 * em,
		fontFamily: "Lato-Bold",
		minHeight: 2 * em,
		maxWidth: 90 * vw,
		paddingHorizontal: 1.7*em,
		paddingVertical:0.5*em,
		borderRadius: 1.7 *em,
	},
	btnContestar:{
		borderRadius: 25,
		borderWidth: 0.16* em,
		backgroundColor:"#decc73",
		height:2.8*em,
		paddingHorizontal: 0.8*em,
		display: "flex",
		flexDirection: "row",
		alignItems:"center",
		justifyContent:"space-evenly",
	},
	botaoPaginacao : {
		justifyContent: "center",
		alignItems: "center",
		minHeight: 2.0 * em,
		paddingHorizontal: 0.6*em,
		borderColor: corOuroDesbotado,
		borderWidth: 0.1 * em,
		borderRadius: 0.5*em,
		marginHorizontal: 4,
	},
	botaoPaginacaoAtivo : {
		backgroundColor: corFundoAux,
		borderColor: corAmareloOuro,
	},
	botaoPausa : {
		backgroundColor: "#FFD500",
		borderRadius: 1*em,
		padding: 0.2*em,
		marginLeft: 0.5*em,
	},
	botaoContinua : {
		backgroundColor: "#95f089",
		borderRadius: 1*em,
		padding: 0.2*em,
		marginLeft: 0.5*em,
	},
	pressableItemContestacao: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		alignSelf: "center",
		minHeight: 1.4 * em,
		minWidth: 15 * vw,
		borderWidth: 0.05 * em,
		borderColor: "#c4c3bc",
		borderRadius: 0.5*em,		
	},
	touchableOpacityStyle: {
		position: "absolute",
		width: 50,
		height: 50,
		alignItems: "center",
		justifyContent: "center",
		right: 30,
		bottom: 30,
	},
});

export {botoes};