import React from "react";

import PropTypes from "prop-types";

import { PopupGenerico } from "../atomicos/popupGenerico.js";

function PopupNaoFecheApp (props) {

	const callbackAceito = props.callbackAceito;

	return (
		<PopupGenerico texto={
			["Em respeito à sua privacidade, o app não rastreia sua localização depois que você o encerra.",
				"Você pode bloquear a tela do seu celular ou usar outros aplicativos enquanto faz uma viagem, mas caso você encerre o app, sua viagem será perdida!",
			]} 
		callbackAceito={callbackAceito}
		testID={props.testID}/>
	);

}

PopupNaoFecheApp.propTypes = {
	callbackAceito: PropTypes.func,
	testID: PropTypes.string,
};

export {PopupNaoFecheApp};
