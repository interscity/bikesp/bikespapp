import React,{useContext,useState,useEffect} from "react";
import {
	Text,
	View,
	FlatList,
	TouchableOpacity
} from "react-native";
  
import PropTypes from "prop-types";

import {ItemListaViagens} from "../atomicos/itemListaViagens.js";
import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import {sincronizaViagens} from "../../acoes/sincronizaViagens.js"; 
import {reenviaViagens} from "../../acoes/reenviaViagens.js"; 

import { listas } from "../../estilos/listas.js";
import { textos } from "../../estilos/textos.js";

import { BotaoSincronizar } from "../intermediarios/botaoSincronizar.js";
import { containeres } from "../../estilos/containeres.js";
import { botoes } from "../../estilos/botoes.js";

var key_viagem = 0;

function ListaViagens(props){

	let gerenciadorLocal = useContext(contextoGerenciadorLocal);
	
	// Suponho que estarão ordenadas por ordem cronológica reversa
	const [viagens,setViagens] = useState(props.viagens); 
	const itensPorPagina = 10;
	const [totalPaginas,setTotalPaginas] = useState(0);
	const [paginaAtual,setPaginaAtual] = useState(0);
	const [viagensParaRenderizar,setViagensParaRenderizar] = useState([]);

	const SemViagens = () => {
		return (
			<View style={{height: listas.listaViagens.height, alignItems: "center", justifyContent: "center"}}>
				<Text style={[textos.texto]}> Você ainda não fez nenhuma viagem!</Text>
			</View>
		);
	};

	const setaViagens = (viagens) => {
		setViagens(viagens);
		let renderizar = devolveViagensParaRenderizar(viagens,setTotalPaginas,paginaAtual,itensPorPagina);
		setViagensParaRenderizar(renderizar);
	};

	const devolveViagensParaRenderizar = (todasViagens,setTotalPaginas,paginaAtual,itensPorPagina) => {
		setTotalPaginas(Math.ceil(todasViagens.length/itensPorPagina));
		let inicio = itensPorPagina * paginaAtual;
		let fim = inicio + itensPorPagina;
		return todasViagens.slice(inicio,fim);
	};

	useEffect(()=>{
		setViagensParaRenderizar(
			devolveViagensParaRenderizar(viagens,setTotalPaginas,paginaAtual,itensPorPagina)
		);
	},[paginaAtual]);
	
	return (
		<View style={listas.containerListaViagens}>
			<BotaoSincronizar
				orientation="right"
				chaveMemoria={"Extrato"}
				funcaoSincroniza={async ()=>{
					await reenviaViagens(gerenciadorLocal);
					return await sincronizaViagens(gerenciadorLocal);
				}}
				setDados={setaViagens}
			/>
			<FlatList
				style={listas.listaViagens}
				ListEmptyComponent={	
					<SemViagens/>
				}
				data={viagensParaRenderizar}
				keyExtractor={() => {key_viagem += 1; return key_viagem;} }
				renderItem={({item}) => (
					<ItemListaViagens viagem={item}/>
				)}
				persistentScrollbar={true}
			/>
			<View style={containeres.containerPaginacao}>
				<BotoesPaginacao setPaginaAtual={setPaginaAtual} paginaAtual={paginaAtual} totalPaginas={totalPaginas}/>
			</View>
		</View>

	);
}

ListaViagens.propTypes = {
	viagens: PropTypes.array
};


function BotoesPaginacao(props){
	const totalPaginas = props.totalPaginas;
	const paginaAtual = props.paginaAtual;
	const setPaginaAtual = props.setPaginaAtual;

	const numMaxBotoesInicio = 1;
	const numMaxBotoesAntes = 1;
	const numMaxBotoesDepois = 1;
	const numMaxBotoesFim = 1;
	
	let listaNumeroBotoes = [];

	for (let i = 0; i < totalPaginas; i++){
		if (i < numMaxBotoesInicio || i >= totalPaginas-numMaxBotoesFim ||
			(i >= paginaAtual && i - paginaAtual <= numMaxBotoesAntes) || 
			(i <= paginaAtual && paginaAtual - i <= numMaxBotoesDepois)){

			let ultimo = listaNumeroBotoes[listaNumeroBotoes.length-1];
			if (ultimo != "..." && i - ultimo > 1){
				listaNumeroBotoes.push("...");
			}
			listaNumeroBotoes.push(i);
		}
	}

	let componentesBotoes = [];
	for (let i = 0; i < listaNumeroBotoes.length; i++){
		let texto = listaNumeroBotoes[i];
		let reticencias = texto == "...";
		let botao;
		if (!reticencias){
			botao = (<TouchableOpacity
				style={[
					botoes.botaoPaginacao,
					texto == paginaAtual ? botoes.botaoPaginacaoAtivo :  null
				]}
				disabled={texto == paginaAtual || reticencias}
				key={i}
				onPress={() => setPaginaAtual(texto)}
			>
				<Text style={[textos.texto]}> {texto+1} </Text>
			</TouchableOpacity>);
		}
		else{
			botao = (<Text key={i} style={[textos.texto]}> {texto} </Text>);
		}
		componentesBotoes.push(botao);
	}
	return componentesBotoes;

}

BotoesPaginacao.propTypes = {
	totalPaginas: PropTypes.number,
	setPaginaAtual: PropTypes.func,
	paginaAtual: PropTypes.number
};


export {ListaViagens};