import React, { useState, useEffect } from "react";

import {ScrollView, Text } from "react-native";

import { Dialog, Portal, RadioButton, Button, TextInput } from "react-native-paper";

import { MAX_DIST_PAUSA, MAX_SOMA_TEMPO_PAUSA, NUM_MAX_PAUSAS} from "../../classes/utils.js";

import PropTypes from "prop-types";
import { globais } from "../../estilos/globais.js";

/**
 * Objeto que armazena os motivos de pausa. Cada entrada deve ter um campo `label` que é o texto mostrado no radio button 
 * e um campo `descricao` que é o texto mostrado abaixo dos radio buttons (componente MensagemOpcaoPausa) que deve dar 
 * contexto explicando o que a opção significa 
 */
const motivos = {
	"compras": {
		label: "Mercado/Compras",
		descricao: "Tava no caminho e lembrou que precisa comprar algo? Faz uma pausa e vai lá resolver."
	},
	"alimentacao": {
		label: "Alimentação",
		descricao: "Fome? Sede? Faz uma pausa e se vai lá."
	},
	"bicicleta": {
		label: "Problemas na bicicleta",
		descricao: "Bicicleta quebrou? Pneu furou? Faz uma pausa e resolve o problema."
	},
	"outro": {
		label: "Outro motivo",
		descricao: "Conte-nos, se quiser, qual o motivo da sua pausa."
	},
	"naoinformar": {
		label: "Não quero informar",
		descricao: "Tudo bem. Respeitamos a sua privacidade. Não é necessário nos informar tudo o que faz."
	},
};

/**
 * Caixa de diálogo nativa exibida quando o usuário pressiona o botão de pausa. 
 * O objetivo desse diálogo é coletar a razão pela qual o usuário está solicitando a pausa
 * @param {*} props.callbackPausa 
 * @param {*} props.trajeto  
 * @param {*} props.onClose  
 * @param {*} props.open  
 * @param {*} props.setViagemPausada  
 * @returns 
 */
function PausaDialog(props) {
	const [selecao, setSelecao] = useState("outro");
	const [motivo, setMotivo] = useState("");
	const [openRegras, setOpenRegras] = useState(false);
	
	const handleSelecao = (value) => {
		setSelecao(value);
		setMotivo("");
	};

	const handleMotivoChange = (text) => setMotivo(text);

	const handlePausa = () => {
		props.callbackPausa(props.trajeto, selecao, motivo);
		props.setViagemPausada(true);
		props.onClose();
	};
	

	useEffect(() => {},[openRegras]);

	return (
		<Portal>
			{openRegras && <RegrasPausaDialog setOpenRegras={setOpenRegras} openRegras={openRegras}/>}
			{!openRegras && <Dialog style={globais.dialogPausa} visible={props.open} onDismiss={props.onClose}>
				<Dialog.Title>Pausar a Viagem</Dialog.Title>
				<Dialog.Content> 
					<Text>
							Para consultar as regras a respeito das pausas de uma viagem clique {""}
						<Text
							style={[{
								textDecorationLine: "underline", 
								color:"rgb(199, 168, 12)"
							}]} 
							onPress={() => { setOpenRegras(true);}}
						>
							aqui
						</Text>
					</Text>
				</Dialog.Content>
				<Dialog.Title>Motivo da pausa</Dialog.Title>
				<Dialog.ScrollArea style={{paddingHorizontal: 0, paddingBottom: 0}}>
					<ScrollView>
						<Dialog.Content>
							<RadioButton.Group onValueChange={handleSelecao} value={selecao}>
								{
									Object.keys(motivos).map(k => (
										<RadioButton.Item key={k} label={motivos[k].label} value={k} />
									))
								}
							</RadioButton.Group>

							{selecao && <MensagemOpcaoPausa motivo={selecao}/>}
							
							{selecao === "outro" && (
								<TextInput
									label="Motivo"
									value={motivo}
									maxLength={100}
									onChangeText={handleMotivoChange}
								/>
							)}
						</Dialog.Content>
					</ScrollView>
				</Dialog.ScrollArea>
				<Dialog.Actions>
					<Button onPress={props.onClose}>Cancelar</Button>
					<Button onPress={handlePausa}>Pausar Viagem</Button>
				</Dialog.Actions>
			</Dialog>}
		</Portal>
	);
}

PausaDialog.propTypes = {
	callbackPausa: PropTypes.func,
	trajeto: PropTypes.any,
	onClose: PropTypes.func,
	open: PropTypes.bool,
	setViagemPausada: PropTypes.func,
};

/**
 * Diálogo nativo para exibit ao usuário as regras acerca das pausas durante uma viagem
 * @param {*} props.setOpenRegras 
 * @param {*} props.openRegras  
 * @returns 
 */
function RegrasPausaDialog(props) {
	const setOpenRegras = props.setOpenRegras;
	const openRegras = props.openRegras;

	return (
		<Dialog visible={openRegras} onDismiss={() =>{setOpenRegras(false);}}>
			<Dialog.Title>Regras para a pausa</Dialog.Title>
			<Dialog.ScrollArea style={{paddingHorizontal: 0, paddingBottom: 20, paddingTop: 20}}>
				<ScrollView>
					<Dialog.Content>
						<Text>
								O usuário pode realizar até <Text style={{fontWeight: "bold"}}>{NUM_MAX_PAUSAS}</Text> pausas durante cada viagem.  {"\n"} {"\n"}
								As pausas de uma viagem devem somar, no máximo, <Text style={{fontWeight: "bold"}}>{MAX_SOMA_TEMPO_PAUSA}</Text> minutos. Caso o usuário exceda esse limite
								a viagem será reprovada. Se você estiver com as notificações habilitadas, você será notificado quando esse tempo estiver acabando. {"\n"} {"\n"}
								A distância máxima entre o ponto de início da pausa ao ponto de retomada dela deve ser de, no máximo, <Text style={{fontWeight: "bold"}}>{MAX_DIST_PAUSA} </Text>
								metros. Caso o usuário retorne mais distante do que esse limite, a viagem será reprovada.
						</Text>
					</Dialog.Content>
				</ScrollView>
			</Dialog.ScrollArea>
			<Dialog.Actions>
				<Button onPress={() =>{setOpenRegras(false);}}>Entendi</Button>
			</Dialog.Actions>
		</Dialog>
	);
}

RegrasPausaDialog.propTypes = {
	setOpenRegras: PropTypes.func,
	openRegras: PropTypes.bool,
};

/**
 * Texto a ser exibido no diálogo de pausa dando contexto ao usuário sobre o motivo selecionado.
 * Utiliza o campo `descricao` de uma entrada do objeto `motivos` como texto 
 * @param {*} motivo a chave do objeto `motivos` que vai retornar a descrição 
 * @returns 
 */
function MensagemOpcaoPausa({ motivo }) {
	return <Text>{motivos[motivo].descricao}</Text>;
}

MensagemOpcaoPausa.propTypes = {
	/**
	 * a chave do objeto `motivos` que vai retornar a descrição
	 */
	motivo: PropTypes.string.isRequired
};


export default PausaDialog;
