import React from "react";

import PropTypes from "prop-types";

import { PopupGenerico } from "../atomicos/popupGenerico.js";

import constantes from "../../../constantes.json";

function PopupJustificaPermissoes (props) {

	const callbackAceito = props.callbackAceito;

	return (
		<PopupGenerico texto={
			["Seja bem-vindo(a) ao Bike SP!",
				"Este app coleta a sua localização enquanto estiver com ele aberto ou minimizado para que suas viagens possam ser tratadas e recompensadas.",
				"Por favor, dê ao app a permissão para que ele possa acessar sua localização exata.",
				"Opcionalmente, permita também que o app acesse sua atividade física para aumentar as chances que suas viagens sejam aprovadas."
			]} 
		callbackAceito={callbackAceito}
		testID={constantes.justificaPermissoesTestID}/>
	);

}

PopupJustificaPermissoes.propTypes = {
	callbackAceito: PropTypes.func,
};

export {PopupJustificaPermissoes};
