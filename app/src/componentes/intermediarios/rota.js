import React, {useState, useEffect} from "react";
import {View, Image, TouchableOpacity} from "react-native";

import PropTypes from "prop-types";
import ImageView from "react-native-image-viewing";
import {BackHandler} from "react-native";

import {REACT_APP_PK_MAPBOX} from "@env";
import {containeres} from "../../estilos/containeres";
import Texto from "../textos/Texto";
import Bloco from "../conteineres/bloco";
import {em} from "../../estilos/globais";

// eslint-disable-next-line @typescript-eslint/no-var-requires
var polyline = require("@mapbox/polyline");

function Rota(props) {
	let trajeto = props.trajeto;
	let listaPosicoes = getPosicoesTrajeto(trajeto);

	if (listaPosicoes.length < 2) {
		return (
			<Bloco largura={15 * em}>
				<Texto centro erro>
					{" "}
          Não é possível visualizar um trajeto com menos de 2 pontos.
				</Texto>
			</Bloco>
		);
	}
	let inicio = posicaoString(listaPosicoes[0]);
	let fim = posicaoString(listaPosicoes[listaPosicoes.length - 1]);
	let polilinha = criaPolilinha(listaPosicoes);

	const atributosPath = {
		strokeWidth: "5",
		strokeColor: "a3baff",
		strokeOpacity: "1",
	};

	const atributosPinInicio = {
		label: "a",
		color: "ff0000",
	};
	const atributosPinFim = {
		label: "b",
		color: "0000ff",
	};

	const atributosGerais = {
		center: "auto",
		size: "600x600",
	};

	let path =
    criaPin(atributosPinInicio, inicio) +
    "," +
    criaPin(atributosPinFim, fim) +
    "," +
    criaCaminho(atributosPath, polilinha);
	let url =
    "https://api.mapbox.com/styles/v1/mapbox/streets-v12/static/" +
    path +
    "/" +
    atributosGerais["center"] +
    "/" +
    atributosGerais["size"] +
    "?logo=false&access_token=" +
    REACT_APP_PK_MAPBOX;

	return <ImagemRota url={url} />;
}

Rota.propTypes = {
	trajeto: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

function criaCaminho(atributos, polilinha) {
	let grossuraLinha = atributos["strokeWidth"];
	let corLinha = atributos["strokeColor"];
	let opacidadeLinha = atributos["strokeOpacity"];
	let path =
    "path-" +
    grossuraLinha +
    "+" +
    corLinha +
    "-" +
    opacidadeLinha +
    "(" +
    polilinha +
    ")";
	return encodeURIComponent(path);
}

function criaPin(atributos, coordenadas) {
	let label = atributos["label"];
	let cor = atributos["color"];
	let pin = "pin-s-" + label + "+" + cor + "(" + coordenadas + ")";
	return encodeURIComponent(pin);
}

function criaPolilinha(listaCoords) {
	return polyline.encode(listaCoords);
}

function getPosicoesTrajeto(trajeto) {
	// Trajeto do usuário
	let listaPosicoes = [];
	for (let p in trajeto["posicoes"]) {
		// let lat = trajeto["posicoes"][p]["Posicao"]["latitude"];
		// let long = trajeto["posicoes"][p]["Posicao"]["longitude"];
		let lat = trajeto["posicoes"][p][0];
		let long = trajeto["posicoes"][p][1];
		listaPosicoes.push([lat, long]);
	}
	return listaPosicoes;
}

function posicaoString(posicao) {
	return posicao[1] + "," + posicao[0];
}

function ImagemRota(props) {
	const fechaImagem = () => {
		if (isModalVisible) {
			setModalVisible(false);
		}
	};
	const abreImagem = () => {
		if (!isModalVisible) {
			setModalVisible(true);
		}
	};

	useEffect(() => {
		BackHandler.addEventListener("hardwareBackPress", fechaImagem);
		return () =>
			BackHandler.removeEventListener("hardwareBackPress", fechaImagem);
	}, []);

	let width = 300;
	let height = 300;
	let url = props.url;
	const images = [
		{
			uri: url,
		},
	];
	const [isModalVisible, setModalVisible] = useState(false);

	return (
		<View>
			<TouchableOpacity onPress={abreImagem}>
				<Image source={{uri: url}} style={{width: width, height: height}} />
			</TouchableOpacity>

			<ImageView
				key={"imagemRota"}
				backgroundColor={containeres.tela.backgroundColor}
				animationType={"slide"}
				swipeToCloseEnabled={true}
				visible={isModalVisible}
				imageIndex={0}
				onRequestClose={fechaImagem}
				images={images}
			/>
		</View>
	);
}

ImagemRota.propTypes = {
	url: PropTypes.string,
};

export {Rota};
