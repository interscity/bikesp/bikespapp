import React, {useContext,useState } from "react";
import {
	Text,
	Pressable,
	View,
	ActivityIndicator
} from "react-native";

import PropTypes from "prop-types";

import { REACT_APP_PK_MAPBOX } from "@env";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import Mapbox, { Callout, PointAnnotation } from "@rnmapbox/maps";

Mapbox.setAccessToken(REACT_APP_PK_MAPBOX);


import {convCoordDictList,/*criaGeoJSONLinha,*/criaShapeCirculo,RAIO_MAXIMO_TOLERANCIA,MAX_DIST_PAUSA} from "../../classes/utils.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { contextoLocalizador } from "../../contextos/contextoLocalizador.js";
import { contextoTrajeto } from "../../contextos/contextoTrajeto.js";
import { contextoCamera } from "../../contextos/contextoCamera";
import { containeres } from "../../estilos/containeres.js";
import { botoes } from "../../estilos/botoes.js";
import { textos } from "../../estilos/textos.js";
import { OnPressEvent } from "@rnmapbox/maps/lib/typescript/src/types/OnPressEvent.js";
import { Local } from "../../classes/local.js";

export interface Coordinate {
	latitude: number;
	longitude: number;
}

function Mapa({
	locsPossiveis,
	posicaoInicial,
	redimensionaWidth = 1,
	redimensionaHeight = 1
}: { 
	locsPossiveis?: Local[],
	posicaoInicial?: [number, number], 
	redimensionaWidth?: number, 
	redimensionaHeight?: number 
}) {
	const camera = useContext(contextoCamera);

	const [mapaCarregado,setMapaCarregado] = useState(false);
	
	camera.setZoomLevel(15);
	if (locsPossiveis === undefined) {
		const gerenciadorLocal = useContext(contextoGerenciadorLocal);
		locsPossiveis = JSON.parse(gerenciadorLocal.getLocalmenteMem("Locais"));
	}

	return (
		<View>
			<View style={[containeres.containerMapa,{
				width:containeres.containerMapa.width * redimensionaWidth,
				height:containeres.containerMapa.height * redimensionaHeight
			}]}>
				<Mapbox.MapView style={{flex:1,opacity:mapaCarregado?1:0,}}
					logoEnabled={false}
					tintColor={containeres.tela.backgroundColor}
					attributionEnabled={false}
					localizeLabels={true}
					onDidFinishLoadingMap={()=>{setMapaCarregado(true);}}
				>

					<LocaisConhecidos locais={locsPossiveis ?? []}/>
					<CirculoPausa/>
					{posicaoInicial
						? <Camera coordenadas={posicaoInicial}/>
						: <PosAtual/>
					}
				</Mapbox.MapView>	
			</View>	
			{mapaCarregado?null
				:
				<View style={[containeres.containerMapa,{position:"absolute",flex:1}]}>
					<ActivityIndicator size={containeres.containerMapa.height*0.4} color={botoes.botaoNav.borderColor}/>
				</View>

			}
		</View>	
	);

}

function LocaisConhecidos({ locais }: { locais: Local[] }){
	const listaPointAnnotation = [];
	const listaCirculos = [];
	for (const locIdx in locais){

		// let idLocalizacao = locais[locIdx][0];
		const coordenada = locais[locIdx][1];
		const coordenadaMapBox: [number, number] = [coordenada[1],coordenada[0]];
		const tipo = locais[locIdx][2];
		const apelido = locais[locIdx][3];

		listaCirculos.push(
			<Circulo key={"circle" + locIdx} id={"circle" + locIdx} centro={coordenadaMapBox} raio={RAIO_MAXIMO_TOLERANCIA/1000}/>
		);
		listaPointAnnotation.push(
			<LocationPoint 
				key={locIdx}
				coordinate={coordenadaMapBox}
				apelido={apelido}
				tipo={tipo}	
				id={locIdx}		
			/>
		);
	}
	return (
		<View>
			{listaCirculos}
			{listaPointAnnotation}
		</View>

	);

}

LocaisConhecidos.propTypes = {
	locais: PropTypes.array,
};

function LocationPoint({coordinate, apelido, tipo, id}: {coordinate: [number, number], apelido: string, tipo: string, id: string}) {
	
	return (	
		<PointAnnotation
			coordinate={coordinate}
			id={id}
			style={{
				elevation: 3
			}}
		>
			{tipo === "estacao" 
				? <Icon name="bus" size={20} color="#666666" /> 
				: <></>}
			<MarkerPopup nome={apelido}/>
		</PointAnnotation>	
	);
}


LocationPoint.propTypes = {
	apelido: PropTypes.string,
	coordinate: PropTypes.array,
	tipo: PropTypes.string,
	id: PropTypes.string
};

export function MarkerPopup({ nome }: {nome: string}) {
	return (
		<Callout
			title={nome}
			contentStyle={{ 
				borderRadius: 5,
				borderColor: "#ffffff",
			}}/>
	);
}

function PosAtual(){

	const localizador = useContext(contextoLocalizador);

	const [localizacao,setLoc] = useState<[number, number]>(convCoordDictList(localizador.getPosicaoAtual()) as [number, number]);
	localizador.adicionaCallbackSucesso((p: Coordinate) => { 
		{ 
			if (localizacao[0] != p.longitude || localizacao[1] != p.latitude){
				setLoc(convCoordDictList(p) as [number, number]);
			}
		}
	},"posAtual");
	// let rota = localizador.emTrajeto ? localizador.trajeto.paraGeoJSON() : criaGeoJSONLinha([]);

	return (
		<View>
			<PointAnnotation id="posInicial" title="marcadorPosInicial" coordinate={localizacao}>
				<View style={{
					height: 20, 
					width: 20, 
					backgroundColor: "#0000ff" , 
					borderRadius: 40, 
					borderColor: "#fff", 
					borderWidth: 3,
					elevation: 3
				}}/>		
				<Callout title="Você está aqui" />
			</PointAnnotation>
			<Camera coordenadas={localizacao}/>
			{/*<Mapbox.ShapeSource id='shapeSource' shape={rota}>
				<Mapbox.LineLayer id='lineLayer' style={{lineColor:"red",lineWidth:3}} />
			</Mapbox.ShapeSource>*/}
		</View>
	);


}

function Camera({ coordenadas }: { coordenadas: [number, number] }){
	const camera = useContext(contextoCamera);
	const cameraZoom = camera.getZoomLevel();
	
	return (
		<Mapbox.Camera
			zoomLevel={cameraZoom}
			animationMode={camera.getAnimationMode()}
			maxZoomLevel={camera.getMaxZoomLevel()}
			minZoomLevel={camera.getMinZoomLevel()}
			pitch={camera.getPitch()}
			animationDuration={camera.getAnimationDuration()}
			centerCoordinate={coordenadas}
		/>
	);
}

function Circulo({
	centro,
	raio,
	id,
	cor = "#c7a80c",
	onPress
}: {
	centro: [number, number],
	raio: number,
	id: string,
	cor: string,
	onPress?: (event: OnPressEvent) => void
}){
	const shapeCirculo = criaShapeCirculo(centro,raio);
	const opacidade = 0.45;
	const corBorda = "#000000";
	
	const idShape = "shape" + String(id);
	const idPolygon = "polygon" + String(id);
	return (
		<Mapbox.ShapeSource 
			id={idShape}
			shape={shapeCirculo as any /* */}
			onPress={onPress}
		>
			<Mapbox.FillLayer 
				id={idPolygon} 
				layerIndex={10000}
				style={
					{
						fillOpacity:opacidade,
						fillColor:cor,
						fillOutlineColor:corBorda,
					}
				}/>
		</Mapbox.ShapeSource>
	);
}

Circulo.defaultProps = {
	cor: "#c7a80c",
	onPress: ()=>{}
};


function CirculoPausa(){

	const trajeto = useContext(contextoTrajeto);

	const coordsAnnotationDefault: Coordinate = {latitude:0, longitude:0};
	const [intervalo,setIntervalo] = useState<NodeJS.Timeout | undefined>(undefined);
	const [annotation, setAnnotation] = useState<[boolean, Coordinate]>([false,coordsAnnotationDefault]);
	const [emPausa, setEmPausa] = useState(trajeto.emPausa);
	
	trajeto.adicionaCallbackPausa((pausa: boolean) => {
		{
			if (emPausa != pausa){
				setEmPausa(pausa);
			}
		}
	}, "checkEmPausaCirculoMapa");

	if (trajeto.posicoes.length == 0 || trajeto.pausas.length == 0 || !emPausa){
		return (<></>);
	}
	
	const posicaoInicioPausa: Coordinate = trajeto.pausas[trajeto.pausas.length-1]["AmostraRecente"]["Posicao"];
	const centro: [number, number] = [posicaoInicioPausa.longitude,posicaoInicioPausa.latitude];
	const cor = containeres.headerEmPausa.backgroundColor;

	const onPressCirculo = (evento : { coordinates: Coordinate }) => {
		alteraEstadoAnnotation(!annotation[0],evento.coordinates);
	};

	const alteraEstadoAnnotation = (mostra: boolean, coordenadas: Coordinate) => {
		if (mostra){
			setAnnotation([true,coordenadas]);
			clearInterval(intervalo);
			setIntervalo(setInterval(() => {
				setAnnotation([false,coordsAnnotationDefault]);
			}, 8000));		
		}
		else{
			setAnnotation([false,coordsAnnotationDefault]);
			clearInterval(intervalo);
			setIntervalo(undefined);
		}
	};


	const centroAnnotation = [annotation[1].longitude, annotation[1].latitude];

	return (
		<View>
			{annotation[0] && 
				<Mapbox.MarkerView 
					id="annotationCirclePausa"
					coordinate={centroAnnotation}
				>
					<View style={containeres.calloutCirculoPausa}>
						<Pressable onPress={() => alteraEstadoAnnotation(false,coordsAnnotationDefault)}>
							<Text style={[textos.texto,{justifyContent:"space-around"}]}>
								Distância máxima para a pausa ser válida.
							</Text>
						</Pressable>
					</View>
				</Mapbox.MarkerView>
			}
			<Circulo
				key={"circlePausa"}
				id={"circlePausa"}
				onPress={onPressCirculo}
				cor={cor}
				centro={centro}
				raio={MAX_DIST_PAUSA/1000}
			/>
		</View>
	);
}

export {Mapa};

