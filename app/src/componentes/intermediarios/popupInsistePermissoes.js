import React from "react";

import PropTypes from "prop-types";

import { PopupGenerico } from "../atomicos/popupGenerico.js";

function PopupInsistePermissoes (props) {

	const callbackAceito = props.callbackAceito;

	return (
		<PopupGenerico texto={
			["Não foi possível iniciar uma viagem.",
				"Por favor, mude as permissões do app para que ele possa acessar a localização exata e tente novamente",
			]} 
		callbackAceito={callbackAceito}
		mostrarIcone={true}
		testID={props.testID}/>
	);

}

PopupInsistePermissoes.propTypes = {
	callbackAceito: PropTypes.func,
	testID: PropTypes.string,
};

export {PopupInsistePermissoes};
