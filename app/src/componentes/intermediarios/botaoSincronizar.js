import React, { useState } from "react";
import {
	Text,
	View,
	Pressable,
} from "react-native";

import PropTypes from "prop-types";

import { containeres } from "../../estilos/containeres";
import { botoes } from "../../estilos/botoes";
import { textos } from "../../estilos/textos";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function BotaoSincronizar(props){

	let orientation = props.orientation;

	let mensagemCarregamento = props.mensagemCarregamento;
	let mensagemErro = props.mensagemErro;
	let mensagemSucesso = props.mensagemSucesso;

	let funcaoSincroniza = props.funcaoSincroniza;

	let setDados = props.setDados;

	const [gerou,setGerou] = useState([false,false]); // [tentou gerar, geração deu certo] 
	const [carregando,setCarregando] = useState(false); 

	const gera = async () => {
		setCarregando(true);
		let [sincronizou,dados] = await funcaoSincroniza();
		if (!sincronizou) setGerou([true,false]);
		else {
			setGerou([true,true]);
			setDados(dados);
		}
		setCarregando(false);
	};

	const Botao = () => {
		return (
			<View>
				<Pressable
					disabled={carregando ? true:false}
					style={[botoes.botaoSync]}
					onPress={gera}
				>
					<Icon name="sync" size={textos.subtitulo.fontSize*1.2} style={{transform: [{rotateZ: "90deg"}]}} color="#cca404"  />
				</Pressable>
			</View>
		);
	};
	const Mensagens = ()=> {
		return (
			<View>
				{carregando && <MensagemCarregamentoGeracao texto={mensagemCarregamento}/>}
				{!carregando && gerou[0] && !gerou[1] && <MensagemErroGeracao texto={mensagemErro} setGerou={setGerou}/>}
				{!carregando && gerou[0] && gerou[1] && <MensagemSucessoGeracao texto={mensagemSucesso} setGerou={setGerou}/>}
			</View>
		);
	};

	return (
		<View style={[{
			alignSelf:orientation == "center" ? "center" :
				orientation == "right" ? "flex-end":
					null
		}]}>
			{orientation == "center" ?
				<View style={containeres.syncContainer}>
					<Botao/>
					<Mensagens/>
				</View>
				: orientation == "right" ?
					<View style={containeres.syncContainerRight}>
						<Mensagens/>
						<Botao/>
					</View>
					: <> </>
			}
		</View>
	);
}

BotaoSincronizar.defaultProps = {
	orientation:"center",
	mensagemCarregamento: "Carregando...",
	mensagemErro: "Não foi possível sincronizar",
	mensagemSucesso: "Sincronização realizada com sucesso"
};

BotaoSincronizar.propTypes = {
	orientation: PropTypes.string,
	mensagemCarregamento: PropTypes.string,
	mensagemErro: PropTypes.string,
	mensagemSucesso: PropTypes.string,
	funcaoSincroniza: PropTypes.func,
	setDados: PropTypes.func
};


function MensagemCarregamentoGeracao(props){
	let texto = props.texto;
	return (
		<Text style={[textos.syncTexto,{color:"red",textAlign:"center"}]}> {texto}</Text>
	);
}

function MensagemErroGeracao(props){
	let setGerou = props.setGerou;
	let texto = props.texto;
	setTimeout(() => {
		setGerou([false,false]);
	}, 4000);
	return (
		<Text style={[textos.syncTexto,{color:"red",textAlign:"center"}]}> {texto} </Text>
	);

}

function MensagemSucessoGeracao(props){
	let setGerou = props.setGerou;
	let texto = props.texto;
	setTimeout(() => {
		setGerou([false,false]);
	}, 4000);
	return (
		<Text style={[textos.syncTexto,{color:textos.texto.color,textAlign:"center"}]}> {texto} </Text>
	);

}


MensagemCarregamentoGeracao.propTypes = {
	texto: PropTypes.string,
};

MensagemErroGeracao.propTypes = {
	texto: PropTypes.string,
	setGerou: PropTypes.func,
};

MensagemSucessoGeracao.propTypes = {
	texto: PropTypes.string,
	setGerou: PropTypes.func,
};


export {BotaoSincronizar};