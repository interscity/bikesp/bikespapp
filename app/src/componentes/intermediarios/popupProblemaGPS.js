import React from "react";

import PropTypes from "prop-types";

import { PopupGenerico } from "../atomicos/popupGenerico.js";
import {useNavigation} from "@react-navigation/native";

function PopupProblemaGPS (props) {

	const navigation = useNavigation();

	const callbackAceito = props.callbackAceito;
	const callbackRecusa = props.callbackRecusa;

	return (
		<PopupGenerico texto={
			["Sua última viagem foi reprovada devido a problemas no GPS. Isso provavelmente aconteceu devido a alguma configuração do seu celular ou do aplicativo.",
				"Deseja acessar as configurações do app Bike SP e saber mais?"
			]} 
		callbackAceito={() => {callbackAceito();navigation.navigate("Config");}}
		textoAceito={"Sim"}
		callbackRecusa={callbackRecusa}
		textoRecusa={"Não"}
		/>
	);

}

PopupProblemaGPS.propTypes = {
	callbackAceito: PropTypes.func,
	callbackRecusa: PropTypes.func,
};

export {PopupProblemaGPS};
