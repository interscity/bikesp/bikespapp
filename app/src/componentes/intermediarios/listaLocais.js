import React from "react";
import {
	Text,
	View,
	FlatList,
} from "react-native";
  
import PropTypes from "prop-types";

import {ItemListaLocais} from "../atomicos/itemListaLocais.js";

import { textos } from "../../estilos/textos.js";
import { listas } from "../../estilos/listas.js";
function ListaLocais(props){	
	const locais = props.locais;

	return (
		<View style={[
			listas.containerListaLocais,
			{
				flexDirection: "column"
			}	
		]}>
			<View
				style={[
					listas.listaLocais,
					{
						justifyContent: "center"
					}
				]}
			>
				<FlatList
					contentContainerStyle={{
						alignSelf: "baseline",
						maxHeight: locais.length * (1.2*listas.itemListaLocais.height),
					}}
					ListEmptyComponent={
						<View style={{alignItems: "center", justifyContent: "space-evenly"}}>
							<Text style={[textos.texto]}> Nenhum Local Cadastrado. Entre em contato com os desenvolvedores!</Text>
						</View>
					}
					data={locais}
					keyExtractor={(item) => item[3]}
					renderItem={({item}) => (
						<ItemListaLocais local={item}/>
					)}
				/>
			</View>
		</View>

	);
}

ListaLocais.propTypes = {
	locais: PropTypes.array,
};


export {ListaLocais};