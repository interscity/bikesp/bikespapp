import React from "react";
import PropTypes from "prop-types";

import { Text } from "react-native";
import { textos } from "../../estilos/textos";

/**
 * Componente para ser usado em títulos que encapsula um `Text` do `react-native`. 
 * As propriedades passadas são refletidas no componente `Text` interno
 * 
 * @param {*} props esquerda: alinha o subtitulo à esquerda
 * @returns 
 */

export default function Titulo(props) {
	return <Text {...props} style={[textos.titulo, (props.esquerda) ? {textAlign: "left", alignSelf: "flex-start"} : {}, props.style]}>{props.children}</Text>;
}

Titulo.propTypes = {
	/**Alinha o título à esquerda */
	esquerda: PropTypes.bool,
	children: PropTypes.node,
	style: PropTypes.object
};