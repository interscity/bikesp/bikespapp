import React from "react";
import PropTypes from "prop-types";

import { Text } from "react-native";
import { textos } from "../../estilos/textos";

/**
 * Componente geral para ser usado em labels de formulários que encapsula um `Text` do `react-native`. 
 * As propriedades passadas são refletidas no componente `Text` interno
 * @returns 
 */
export default function Label(props) {
	return <Text {...props} style={[textos.label, props.style]}>{props.children}</Text>;
}

Label.propTypes = {
	children: PropTypes.node,
	style: PropTypes.object
};