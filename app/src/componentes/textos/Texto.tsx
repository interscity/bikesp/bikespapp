import React from "react";

import {GestureResponderEvent, Text} from "react-native";
import {textos} from "../../estilos/textos";

export type Props = {
	/**Alinha o texto ao centro */
	centro: boolean;
	/**Aplica formatação itálico ao texto (definido por `textos.italico`) */
	italico: boolean;
	/**Aplica formatação negrito ao texto (definido por `textos.negrito`) */
	negrito: boolean;
	/**Aplica formatação erro ao texto (definido por `textos.erro`) */
	erro: boolean;
	/**Aplica formatação redirecionamento ao texto (definido por `textos.redireciona`) e define o `onPress` do texto*/
	redireciona?: (event: GestureResponderEvent) => void;
	/**Fator multiplicador do tamanho da fonte do texto (padrão: 1)*/
	multiplicadorTamanho?: number;
} & React.ComponentProps<typeof Text>;

/**
 * Componente geral para ser usado em textos de corpo que encapsula um `Text` do `react-native`.
 * As propriedades passadas são refletidas no componente `Text` interno
 */
export default function Texto({
	centro,
	italico,
	negrito,
	erro,
	redireciona,
	multiplicadorTamanho = 1,
	children,
	style,
	...props
}: Props): Element {
	const texto = (
		<Text
			{...props}
			style={[
				textos.texto,
				centro && {textAlign: "center", alignSelf: "center"},
				multiplicadorTamanho !== 1 && {
					fontSize: textos.texto.fontSize * multiplicadorTamanho,
				},
				italico && textos.italico,
				negrito && textos.negrito,
				erro && textos.erro,
				redireciona && textos.redireciona,
				style || {},
			]}
			onPress={redireciona}>
			{children}
		</Text>
	);
	return texto;
}
