import React from "react";
import PropTypes from "prop-types";
import Texto from "./Texto";
import DeviceInfo from "react-native-device-info";

export default function Versao(props) {
	return <Texto italico centro={props.centro}>Versão v{DeviceInfo.getVersion()}</Texto>;
}

Versao.defaultProps = {
	centro: true
};

Versao.propTypes = {
	centro: PropTypes.bool
};
