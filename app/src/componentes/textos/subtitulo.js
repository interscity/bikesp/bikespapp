import React from "react";
import PropTypes from "prop-types";

import { Text } from "react-native";
import { textos } from "../../estilos/textos";

/**
 * Componente para ser usado em subtitulos que encapsula um `Text` do `react-native`. 
 * As propriedades passadas são refletidas no componente `Text` interno
 * 
 * @param {*} props esquerda: alinha o subtitulo à esquerda
 * @returns 
 */
export default function Subtitulo(props) {
	return <Text {...props} style={[textos.subtitulo, (props.esquerda) ? {textAlign: "left"} : {}, props.style]} >{props.children}</Text>;

}

Subtitulo.propTypes = {
	esquerda: PropTypes.bool,
	children: PropTypes.node,
	style: PropTypes.object
};