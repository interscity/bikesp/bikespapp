import React, { useState } from "react";
import { Alert, View, ActivityIndicator } from "react-native";
import { BotaoNav } from "../atomicos/botaoNav";
import { BotaoHome } from "../atomicos/botaoHome";

import { checaNaoVazio, checaEmailValido } from "../../classes/verificaInputUtils.js";

import interfaceConexao from "../../gerenciadores/conexaoServidor.js";
import { Input } from "@rneui/themed";

import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import { listas } from "../../estilos/listas.js";
import { botoes } from "../../estilos/botoes.js";
import Texto from "../textos/Texto";
import Label from "../textos/label.js";
import Tela from "../conteineres/tela.js";
import Titulo from "../textos/titulo.js";
import Regiao from "../conteineres/regiao.js";
import Bloco from "../conteineres/bloco.js";
import { vh } from "../../estilos/globais.js";

/**
 * Tela disponível pelo textp "Esqueceu a senha" na tela de login. Usado para o usuário solicitar a recuperação de senha
 * @param {*} props 
 * @returns 
 */
function TelaEsqueceuSenha () {
	const [email, onChangeEmail] = useState("");
	const [msgErroEmail, setEmailError] = useState("");
	const [msgErroServidor, setErroServidor] = useState("");

	const [aguardandoServidor,setAguardandoServidor] = useState(false);

	function verificaEmail(){
		let emailValido = checaNaoVazio(email,() => {}, () => {setEmailError("Digite seu email");});
		
		if(emailValido){
			emailValido = checaEmailValido(email, () => {}, (x) => {setEmailError(x);});
		}

		if(emailValido){
			setEmailError("");
		}

		return emailValido;
	}

	function verificaInputValido(){

		let emailValido = verificaEmail();

		return emailValido;
	}

	return (
		<Tela>
			<Titulo>Esqueceu a Senha?</Titulo>

			<Regiao altura={75*vh}>
				<Bloco>
					<Label>Insira seu email e receba um link para redefini-la</Label>
					<Input
						containerStyle={{width:containeres.containerMapa.width*0.95}}
						inputContainerStyle={containeres.cadastroInputContainer}
						errorMessage={msgErroEmail}
						placeholder="Email"
						labelStyle={textos.texto}
						autoCapitalize='none'
						value={email}
						onChangeText={onChangeEmail}
					/>
				</Bloco>
				{aguardandoServidor &&
					<View>
						<ActivityIndicator size={containeres.tela.height*0.1} color={botoes.botaoNav.borderColor} />
					</View>
				}
				
				<Texto erro style={{opacity: msgErroServidor == "" ? 0 : 1}}> 
					{msgErroServidor}{"\n"}
				</Texto>
				
				<BotaoHome
					checkCallback={async () => {
						setAguardandoServidor(true);
						setErroServidor("");
						if (verificaInputValido()) {
							// Espera confirmação do servidor
							let resultado = await interfaceConexao.tentaRedefinirSenha(email);
							if (!resultado[0]) {
								setErroServidor(resultado[1]);
								Alert.alert(
									"Houve um problema",
									resultado[1],
									[
										{ text: "Voltar" }
									],
									{ cancelable: false }
								);
							} else {
								Alert.alert(
									"Sucesso",
									`Enviamos um email para ${email} com o link para redefinir a senha. Verifique a caixa de spam.`,
									[
										{ text: "Voltar" }
									],
									{ cancelable: true }
								);
							}
							setAguardandoServidor(false);
							return resultado[0];
						} else {
							setAguardandoServidor(false);
							return false;
						}
					} }
					texto="Enviar email"
					telaDestino={"Login"}
					disabled={aguardandoServidor}
					estiloAux={{ width: listas.itemListaViagens.maxWidth * 0.5}} />


			</Regiao>
			
			<BotaoNav
				texto="Voltar"
				telaDestino="Login"
				returnIcon={true} />
		</Tela>
	);
}

export {TelaEsqueceuSenha};
