import React, { useContext,useState } from "react";

import {
	Text,
	StatusBar,
	TextInput,
	Keyboard,
	Pressable,
	Modal,
	View,
	TouchableWithoutFeedback
} from "react-native";

import PropTypes from "prop-types";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {BotaoNav} from "../atomicos/botaoNav.js";
import { PopupGenerico } from "../atomicos/popupGenerico.js";

import interfaceConexao from "../../gerenciadores/conexaoServidor.js";
import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";

import {sincronizaContestacoes} from "../../acoes/sincronizaContestacoes.js";
import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import { botoes } from "../../estilos/botoes.js";
import Texto from "../textos/Texto";
import { em } from "../../estilos/globais.js";
import Tela from "../conteineres/tela.js";
import Cabecalho from "../conteineres/cabecalho.tsx";
import Bloco from "../conteineres/bloco.js";
import Respiro from "../conteineres/respiro.js";
import RodaPe from "../conteineres/rodape.tsx";
import {useNavigation} from "@react-navigation/native";

function TelaContestar(props){

	let gerenciadorLocal = useContext(contextoGerenciadorLocal);

	const navigation = useNavigation();

	let idViagem = props.route.params.idViagem;

	const [texto, onChangeText] = React.useState("");

	const [popup,setPopupInfo] = useState([false,false,""]); 

	return (
		<Tela>
			<StatusBar
				backgroundColor= {popup[0]? "black": containeres.tela.backgroundColor}
			/>
			{popup[0] && popup[1] && <PopupAcerto setPopupInfo={setPopupInfo}/>}
			{popup[0] && !popup[1] && <PopupErro mensagem={popup[2]} setPopupInfo={setPopupInfo}/>}
			
			<Cabecalho>
				<Text style={[textos.titulo]}> Contestar viagem  </Text>
			</Cabecalho>

			<Pressable
				style={[botoes.botaoNav,]}
				onPress={() => navigation.navigate("Contestacoes")}
			>
				<Icon name="bullhorn" size={1.5*textos.texto.fontSize} color="#decc73" style={{marginRight:0.25*em}} />
				<Texto centro>
					Ver Contestações
				</Texto>
			</Pressable>

			<Respiro pequeno/>

			<Bloco>
				<Text style={[textos.subtitulo]}> Descreva por que a viagem deveria ter sido considerada: </Text>
			
			</Bloco>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
				<TextInput
					editable
					multiline
					numberOfLines={4}
					maxLength={300}
					autoFocus={false}
					onSubmitEditing={Keyboard.dismiss}
					onChangeText={text => onChangeText(text)}

					value={texto}
					style={{
						backgroundColor:"#ffffff", 
						textAlignVertical: "top",
						borderRadius:8, 
						borderColor:botoes.botaoNav.borderColor, 
						borderWidth:1,
						color: "black",
						flex:1,
						height:"100%",
						padding: 10,
						marginVertical: 20
					}}
				/>
			</TouchableWithoutFeedback>
			{ idViagem == null &&
				<View>
					<Text style={[textos.subtitulo]}> Não é possível contestar esta viagem </Text>
				</View>
			}
			{ idViagem != null && 
				<Pressable style = {[botoes.botaoNav, {flexDirection: "column", alignItems:"center"}, {marginBottom: 8}]}
					onPress={() => {enviarContestacao(idViagem,texto,gerenciadorLocal,setPopupInfo);}}>		
					<Texto style={[textos.texto]}>Enviar</Texto>
				</Pressable>
			}

			<RodaPe>
				<BotaoNav
					texto="Voltar"
					voltar={true}
					returnIcon={true}
				/>
			</RodaPe>

		</Tela>	  
	);
}
TelaContestar.propTypes = {
	route: PropTypes.object,
	idViagem: PropTypes.number
};


function PopupErro(props){

	let mensagem = props.mensagem;
	let setPopupInfo = props.setPopupInfo;

	let callbackAceito = () => {setPopupInfo([false,false,""]);};

	return (

		<Modal
			transparent={true}
			animationType = {"slide"}>
			<PopupGenerico texto={[mensagem]}
				callbackAceito={callbackAceito}
				textoAceito={"OK"}
				mostrarIcone={true}
				fatorEncolhimento={0.3}/>
		</Modal>

	);
}

PopupErro.propTypes = {
	setPopupInfo: PropTypes.func,
	mensagem: PropTypes.string
};

function PopupAcerto(props){

	let mensagem = "Contestação enviada com sucesso";
	let navigation = useNavigation();
	let setPopupInfo = props.setPopupInfo;

	let callbackAceito = () => {
		setPopupInfo([false,false,""]);
		navigation.navigate("HistoricoViagens");
	};

	return (
		<Modal
			transparent={true}
			animationType = {"slide"}>						
			<PopupGenerico texto={[mensagem]}
				callbackAceito={callbackAceito}
				textoAceito={"OK"}
				mostrarIcone={true}
				fatorEncolhimento={0.3}/>
		</Modal>
	);
}
PopupAcerto.propTypes = {
	setPopupInfo: PropTypes.func,
};

async function enviarContestacao(idViagem,texto,gerenciadorLocal,setPopupInfo){

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	let data = new Date();
	let objContestacao = montaObjContestacao(idViagem,texto,data);
	
	let resultado = await interfaceConexao.enviaContestacao(cpf,token,objContestacao);
	setPopupInfo([true,resultado[0],resultado[1]]);
	sincronizaContestacoes(gerenciadorLocal);
}

function montaObjContestacao(idViagem,texto,data){
	let objContestacao = {
		"idViagem":idViagem,
		"justificativa":texto,
		"data":data
	};
	return objContestacao;
}




export {TelaContestar};
