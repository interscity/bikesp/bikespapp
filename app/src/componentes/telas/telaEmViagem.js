import React, { useContext, useEffect, useState, useCallback } from "react";
import {
	StatusBar,
	View,
	Alert,
	TouchableHighlight,
	Pressable,
	AppState
} from "react-native";

import PropTypes from "prop-types";

import { BotaoNav } from "../atomicos/botaoNav.js";
import { Mapa } from "../intermediarios/Mapa";
import { ParedeCarregando } from "../atomicos/paredeCarregando.js";
import { HeaderEmViagem  } from "../atomicos/headerEmViagem.js";
import { HeaderInfosViagens } from "../atomicos/headerInfosViagens.js";
import { HeaderGPSIndisponivel } from "../atomicos/headerGPSIndisponivel.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { contextoGerenciadorNotifis } from "../../contextos/contextoGerenciadorNotifs";
import { contextoLocalizador } from "../../contextos/contextoLocalizador.js";
import { contextoTrajeto } from "../../contextos/contextoTrajeto.js";
import {convCoordDictList,LocalizacaoMaisProxima,RAIO_MAXIMO_TOLERANCIA, NUM_MAX_PAUSAS} from "../../classes/utils.js";

import {finalizarViagem} from "../../acoes/finalizarViagem.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { HeaderEmPausa } from "../atomicos/headerEmPausa.js";
import PausaDialog from "../intermediarios/pausaDialog.js";

import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { containeres } from "../../estilos/containeres.js";
import { botoes } from "../../estilos/botoes.js";
import { textos } from "../../estilos/textos.js";
import Texto from "../textos/Texto";
import DeviceInfo from "react-native-device-info";
import Tela from "../conteineres/tela.js";
import { Modal } from "react-native-paper";
import { PopupGenerico } from "../atomicos/popupGenerico.js";

function TelaEmViagem(){

	let gerenciadorLocal = useContext(contextoGerenciadorLocal);
	const locsPossiveis = JSON.parse(gerenciadorLocal.getLocalmenteMem("Locais"));

	const [paredeVisivel, setParedeVisivel] = useState(false);
	const [popupVisivel, setPopupVisivel] = useState(false);
	const trajeto = useContext(contextoTrajeto);

	// Calback que será chamado quando o trajeto for iniciado
	trajeto.adicionaCallbackAndamento((emAndamento) => { 
		{ 
			if (emAndamento === true){
				setPopupVisivel(emAndamento);
			}
		}
	},"popupIncioViagem");

	useFocusEffect(
		useCallback(() => {
			const listener = AppState.addEventListener("change", nextAppState => {
				if (nextAppState === "active" && !trajeto.localizador.emMedicao){
					trajeto.localizador.iniciarMedicao();
				}
				if (nextAppState != "active" && trajeto.localizador.emMedicao && (!trajeto.emAndamento || (trajeto.emAndamento && trajeto.emPausa))){
					trajeto.localizador.finalizarMedicao();
				}
			});
			return () => {listener.remove();};
		}, [])
	);

	return (
		<Tela>
			{paredeVisivel &&
				<ParedeCarregando texto="Tentando enviar viagem"/>}
			
			<GerenciadorHeaders locsPossiveis={locsPossiveis} 
				mostrandoPopup={paredeVisivel}/>
			<Mapa locsPossiveis={locsPossiveis} redimensionaHeight={0.9}/>
			<View style={containeres.emViagemContainer}>
				<InteracoesViagem locsPossiveis={locsPossiveis} callbackAtualizaEsperando={setParedeVisivel}/>
				<BotaoNav 
					texto="Voltar"
					telaDestino="Home"
					returnIcon={true}
				/>
			</View>
			<Modal
				visible = {popupVisivel}
				animationType = {"slide"}>
				<PopupGenerico texto={["Seja consciente e responsável: Pedale com segurança."]}
					callbackAceito={() => {setPopupVisivel(false);}}
					textoAceito={"OK"}
					style={{opacity: 1}}
					mostrarIcone={true}
					fatorEncolhimento={0.6}/>
			</Modal>
		</Tela> 
	);
}

function GerenciadorHeaders(props){
	let locsPossiveis = props.locsPossiveis;
	let localizador = useContext(contextoLocalizador);
	let trajeto = useContext(contextoTrajeto);

	const [emViagem,setEmViagem] = useState(trajeto.emAndamento);
	trajeto.adicionaCallbackAndamento((emAndamento) => { 
		{ 
			if (emViagem != emAndamento){
				setEmViagem(emAndamento);
			}
		}
	},"checkEmViagemHeaders");

	const [emPausa, setEmPausa] = useState(trajeto.emPausa);
	trajeto.adicionaCallbackPausa((pausa) => {
		{
			if (emPausa != pausa){
				setEmPausa(pausa);
			}
		}
	}, "checkEmPausaHeaders");
	
	const [localizacao,setLoc] = useState(convCoordDictList(localizador.getPosicaoAtual()));
	const [gpsDisponivel, setGPSDisponivel] = useState(true);

	localizador.adicionaCallbackSucesso((p) => { 
		{ 
			if (localizacao[0] != p.longitude || localizacao[1] != p.latitude){
				setLoc(convCoordDictList(p));
			}
			setGPSDisponivel(true);
		}
	},"checkPosAtualHeaders");
	
	localizador.adicionaCallbackFracasso(() => {setGPSDisponivel(false);}, "headersFracasso");

	useEffect(
		() => {
			const v = localizador.gpsDisponivel();
			// console.log("Chegou do GPS:",v);
			setGPSDisponivel(v);
		},[]);

	let corStatusBar;

	if(props.mostrandoPopup){
		corStatusBar = "black";
	}else if(! gpsDisponivel){
		corStatusBar = "#c36060";
	}else{
		if(emViagem){
			corStatusBar = "#95f089";
			if (emPausa){
				corStatusBar = "#FFD500";
			}
		} else {
			corStatusBar = "#f4ffe6";
		}
	}
	
	return (
		<View>
			<StatusBar
				backgroundColor= {corStatusBar}
			/>	
			<View>
				{gpsDisponivel ?
					emViagem ? 
						(emPausa? <HeaderEmPausa/> : <HeaderEmViagem/>)
						: (<HeaderInfosViagens locsPossiveis={locsPossiveis} localizacao={localizacao}/>)
					:
					<HeaderGPSIndisponivel/>
				}
			</View>
		</View>
	);
}

GerenciadorHeaders.propTypes = {
	emViagem: PropTypes.bool,
	mostrandoPopup: PropTypes.bool,
	locsPossiveis: PropTypes.array,
};

function InteracaoPausaTrajeto() {
	let trajeto = useContext(contextoTrajeto);
	let gNotifs = useContext(contextoGerenciadorNotifis);
	let gDados = useContext(contextoGerenciadorLocal);
	const [viagemPausada, setViagemPausada] = useState(trajeto.emPausa);
	const [open, setOpen] = useState(false);
	const [podeInteragir, setPodeInteragir] = useState(true);

	const handleInteracao = () => {
		if (!trajeto.emPausa) {
			setOpen(true);
		} else {
			confirmaMudancaEstadoPausa(trajeto,gNotifs,gDados,setViagemPausada);
		}
	};
	
	trajeto.adicionaCallbackPausa((pausa) => {
		if (!pausa && trajeto.pausas.length >= NUM_MAX_PAUSAS){
			setPodeInteragir(false);
		}
	}, "checkEmPausaBotaoPausa");

	const handleCloseDialog = () => {
		setOpen(false);
	};

	function BotaoPausa () {
		return (
			<View style={botoes.botaoPausa}>
				<Icon name="pause" size={textos.titulo.fontSize * 1.3} color="#000" /> 
			</View>
		);
	}

	function BotaoContinua () {
		return (
			<View style={botoes.botaoContinua}>
				<Icon name="play" size={textos.titulo.fontSize * 1.3} color="#000" />
			</View>
		);
	}

	if(!podeInteragir){
		return;
	}

	return (
		<View>
			<Pressable onPress={handleInteracao}>
				{viagemPausada ? <BotaoContinua/> : <BotaoPausa/>}
			</Pressable>
			<PausaDialog open={open} onClose={handleCloseDialog} 
				callbackPausa={(trajeto, selecao, motivo) => mudaEstadoTrajeto(trajeto,selecao,motivo,gNotifs,gDados)} trajeto={trajeto} setViagemPausada={setViagemPausada} />
		</View>
	);
}

function InteracoesViagem(props){
	const navigation = useNavigation();
	let locsPossiveis = props.locsPossiveis;

	let trajeto = useContext(contextoTrajeto);
	let gerenciadorLocal = useContext(contextoGerenciadorLocal);
	let gerenciadorNotifs = useContext(contextoGerenciadorNotifis);
	let localizador = useContext(contextoLocalizador);

	const [localizacao,setLoc] = useState(convCoordDictList(localizador.getPosicaoAtual()));
	const [amostraAtual,setAmostraAtual] = useState(localizador.getEstadoAtual());
	const [gpsDisponivel, setGPSDisponivel] = useState(true);

	const [modoEconomiaBateria, setModoEconomiaBateria] = useState(false);

	useEffect(() => {
		const fetchPowerState = async () => {
			try {
				const powerState = DeviceInfo.getPowerStateSync();
				setModoEconomiaBateria(powerState.lowPowerMode);
			} catch (error) {
				console.error("Erro ao obter o estado da bateria:", error);
			}
		};
	
		fetchPowerState();
	
		const interval = setInterval(fetchPowerState, 500);
	
		return () => clearInterval(interval);
	}, []);

	useEffect(() => {
		console.log(`Pausa: ${trajeto.emPausa}`);
	}, [trajeto.emPausa]);

	localizador.adicionaCallbackSucesso((p) => { 
		{ 
			if (localizacao[0] != p.longitude || localizacao[1] != p.latitude){
				setLoc(convCoordDictList(p));
				setAmostraAtual(p);
			}
			setGPSDisponivel(true);
		}
	},"checkPosAtual");

	localizador.adicionaCallbackFracasso(() => {setGPSDisponivel(false);}, "botaoFracasso");
	
	const [emViagem,setEmViagem] = useState(trajeto.emAndamento);
	trajeto.adicionaCallbackAndamento((emAndamento) => { 
		{ 
			if (emViagem != emAndamento){
				setEmViagem(emAndamento);
			}
		}
	},"checkEmViagemBotao");

	const [emPausa, setEmPausa] = useState(trajeto.emPausa);
	trajeto.adicionaCallbackPausa((statusPausa) => {
		{
			if (emPausa != statusPausa) {
				setEmPausa(statusPausa);
			}
		}
	}, "checkEmPausaBotao");

	const [botaoAtivo,setBotaoAtivo] = useState(true);

	let podeMudarEstado = trajeto.checaPodeMudarEstado(locsPossiveis,localizacao);
	let locMaisProxima = LocalizacaoMaisProxima(locsPossiveis,localizacao);

	locMaisProxima["amostraAtual"] = amostraAtual;

	if (locMaisProxima["distancia"] > RAIO_MAXIMO_TOLERANCIA){
		locMaisProxima["id"] = null;
		locMaisProxima["localizacao"] = {};
	}

	let func_finalizar = () => {
		props.callbackAtualizaEsperando(false);
		// console.log("DEIXEI DE ESPERAR")

	};

	let func_esperar = () => {
		// console.log("ESTOU ESPERANDO")
		props.callbackAtualizaEsperando(true);
		setBotaoAtivo(false);
	};

	return (
		<View style={containeres.containerInteracaoViagens}>
			{!emPausa && <TouchableHighlight
				activeOpacity={0.99}
				underlayColor= {(!gpsDisponivel || emViagem)?  "#A62A2A": "#99CC32"}
				style={[botoes.botaoFinalizar,{
					backgroundColor: (!gpsDisponivel || emViagem)?  "#de887c": "#95f089", 
					borderColor: (!gpsDisponivel || emViagem)?  "#de686c": "#65d069"}]}
				onPress={async () => {
					if(!botaoAtivo){
						return;
					}

					console.log(modoEconomiaBateria);

					if (modoEconomiaBateria){
						Alert.alert(
							"Modo de Economia de Bateria Ativo",
							"Desabilite o modo de economia de bateria para poder iniciar uma viagem",
							[{text: "Ok"}]
						);
					} else if (!podeMudarEstado || !gpsDisponivel) {
						confirmaMudancaEstado(locMaisProxima, trajeto, gerenciadorLocal, navigation, gerenciadorNotifs, func_esperar, func_finalizar);
					}
					else{
						mudaEstadoViagem(locMaisProxima, trajeto, gerenciadorLocal, navigation, gerenciadorNotifs, func_esperar, func_finalizar);
					}
				}}
			>
				<Texto negrito>
					{emViagem? "Finalizar viagem" : "Iniciar Viagem"}
				</Texto>
			</TouchableHighlight>}
			{emViagem && <InteracaoPausaTrajeto/>}
		</View>

	);
}

InteracoesViagem.propTypes = {
	callbackAtualizaEsperando: PropTypes.func,
	locsPossiveis: PropTypes.array,
	navigation:PropTypes.object
};

async function mudaEstadoTrajeto(trajeto, selecao, motivo, gNotifs, gDadosLocais) {
	if (trajeto.emPausa) {
		gNotifs.cancelaNotifLembraFinalizarPausa();
		let resultado = await trajeto.continuaTrajeto(gDadosLocais);
		if(resultado.ok && !resultado.recuperou){
			gNotifs.criaNotifNaoPodeContinuar(resultado.motivo);
		}else if(resultado.ok && resultado.recuperou){
			gNotifs.criarNotifLembraFinalizarViagem();
		}
	} else {
		gNotifs.cancelaLembraFinalizarViagem();

		if (selecao == "outro") {
			trajeto.pausaTrajeto(motivo);
		} else {
			trajeto.pausaTrajeto(selecao);
		}

		const tempoRestante = trajeto.getTempoPausaRestante();

		gNotifs.criaNotifLembraFinalizarPausa(tempoRestante);
	}
	gDadosLocais.setLocalmenteMem("TrajetoAntigo",trajeto.paraJSON());
}

async function mudaEstadoViagem(locMaisProxima,trajeto,gerenciadorLocal,navigation, gerenciadorNotifs, callbackEsperandoFinalizar, callbackFinalizou){

	if (trajeto.emAndamento){
		if(trajeto.emPausa){
			gerenciadorNotifs.cancelaNotifLembraFinalizarPausa();
		}else{
			gerenciadorNotifs.cancelaLembraFinalizarViagem();
		}
		
		callbackEsperandoFinalizar();
		await finalizarViagem(locMaisProxima["id"],trajeto,gerenciadorLocal,navigation, callbackFinalizou);
	}
	else{
		gerenciadorLocal.setLocalmenteMem("EmViagem", "t");
		gerenciadorNotifs.criarNotifLembraFinalizarViagem();
		trajeto.iniciarTrajeto(locMaisProxima["id"],locMaisProxima["amostraAtual"]);
	}
}


function confirmaMudancaEstado(locMaisProxima,trajeto,gerenciadorLocal,navigation, gerenciadorNotifs, callbackEsperandoFinalizar, callbackFinalizou){
	Alert.alert(
		"Tem certeza?",
		"É muito provável que a viagem não seja remunerada",
		[
			{text: "Cancelar"},
			{text: "Confirmar", onPress: () => {mudaEstadoViagem(locMaisProxima,trajeto,gerenciadorLocal,navigation, gerenciadorNotifs,callbackEsperandoFinalizar,callbackFinalizou);}},
		],
		{cancelable: false},
	);
}

async function confirmaMudancaEstadoPausa(trajeto, gNotifs, gDados, setViagemPausada){

	let confirma = () => {
		mudaEstadoTrajeto(trajeto, null, null,gNotifs,gDados);
		setViagemPausada(false);
	};

	let podeVoltar = await trajeto.distanciaVoltaPausaOk();
	if (podeVoltar){
		confirma();
		return;
	}

	Alert.alert(
		"A viagem provavelmente será cancelada",
		"Você está longe de onde iniciou a pausa",
		[
			{text: "Voltar"},
			{text: "Confirmar", onPress: ()=>{confirma();}},
		],
	);
}

export {TelaEmViagem};
