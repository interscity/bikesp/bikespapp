import React, {useContext} from "react";
import {
	Text,
	View,
	Pressable,
	ScrollView,
} from "react-native";

import PropTypes from "prop-types";

import {BotaoNav} from "../atomicos/botaoNav.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {FormataMoeda,FormataData,FormataDuracao,FormataDistancia,LimitaExibicaoString, GetApelidoLocalizacaoPorId, GetDistanciaIdealTrajetoIdeal} from "../../classes/utils.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { textos } from "../../estilos/textos.js";
import { containeres } from "../../estilos/containeres.js";
import { botoes } from "../../estilos/botoes.js";
import { listas } from "../../estilos/listas.js";

import Texto from "../textos/Texto";
import Titulo from "../textos/titulo.js";
import Bloco from "../conteineres/bloco.js";
import { corAmareloOuro, em } from "../../estilos/globais.js";
import Tela from "../conteineres/tela.js";
import {useNavigation} from "@react-navigation/native";

function TelaDetalhesViagem(props){

	const navigation = useNavigation();

	let gDadosLocais = useContext(contextoGerenciadorLocal);

	let viagem = carregaViagem(props.route.params,gDadosLocais);
	const iconeEstadoSize = 3*textos.texto.fontSize;
	const idViagem = viagem.idViagem;
	const data = FormataData(viagem.data,"HH:mm:ss DD/MM/YY");
	const duracao = FormataDuracao(viagem.duracao);
	const remuneracao = FormataMoeda(viagem.remuneracao);
	const estadoRemuneracao = viagem.statusRemuneracao;
	const motivoCodigo = viagem.motivoStatus;

	const idOrigem = viagem.idOrigem;
	const idDestino = viagem.idDestino;

	const listaLocs = JSON.parse(gDadosLocais.getLocalmenteMem("Locais"));
	const paresValidos = JSON.parse(gDadosLocais.getLocalmenteMem("ParesValidos"));

	const apelidoOrigem = GetApelidoLocalizacaoPorId(idOrigem, listaLocs);
	const apelidoDestino = GetApelidoLocalizacaoPorId(idDestino, listaLocs);

	const motivoStatus = traduzCodigoMotivo(motivoCodigo);

	let iconeEstado;
	if(estadoRemuneracao == "Reprovado"){
		iconeEstado = <Icon name="close-circle-outline" 
			size={iconeEstadoSize}
			color="#a34040"
		/>;
	}else if(estadoRemuneracao == "Aprovado"){
		iconeEstado = <Icon name="check-circle-outline" 
			size={iconeEstadoSize}
			color="#65db65"
		/>;
	}

	let temPontoDesconhecido = false;
	let distancia,trajetoIdeal;
	if (apelidoOrigem == "Desconhecido" || apelidoDestino == "Desconhecido"){
		[distancia,trajetoIdeal] = [viagem.distancia,{"posicoes":[]}];
		temPontoDesconhecido = true;
	}
	else{
		[distancia,trajetoIdeal] = GetDistanciaIdealTrajetoIdeal(idOrigem,idDestino,paresValidos);
	}
	
	distancia = FormataDistancia(distancia);

	const textoID = idViagem == null ? "" : "#" + idViagem;

	let estadoRemuneracaoTexto;
	if (estadoRemuneracao == "EmAnalise") {
		estadoRemuneracaoTexto = "Aguardando\nrede";
	} else {
		estadoRemuneracaoTexto = estadoRemuneracao;
	}
	
	let statusFormatado = <>
		<Texto centro negrito> Status: 
			<Texto centro negrito> {estadoRemuneracaoTexto}</Texto>
		</Texto> 
	</>;

	return (
		<Tela style={containeres.telaScroll}>
			<Bloco centro>
				<View style={containeres.containerBotaoContestar}>
					<View
						style={{
							justifyContent: "center",
							flex: 1,
							marginLeft: 10,
							textAlign: "left"
						}}
					>
						<Texto negrito>
							{textoID}
						</Texto>
					</View>
					{estadoRemuneracao != "Aprovado" &&
					<Pressable
						onPress={() =>{navigation.navigate("Contestar",{idViagem:idViagem});}}
					>
						<View style={[botoes.btnContestar,{textAlign: "right"}]}>
							<Icon name="bullhorn" size={textos.texto.fontSize} color="#000000"/>
							<Text style={{color: "#000000",fontWeight: "bold",}}>Contestar</Text>
						</View>
					</Pressable>}
				</View>
				<Icon name="bicycle" size={2*textos.titulo.fontSize} color={corAmareloOuro}/>
				<Titulo>Viagem Encerrada</Titulo>
				<View style={{marginTop:containeres.tela.height*-0.035}}>
					<Texto centro>{data}</Texto>
				</View>
			</Bloco>
			
			<View style={[containeres.containerVertical, {height:containeres.tela.height*0.6,}]}>
				<ScrollView>
					{temPontoDesconhecido ||
					<View style={[{width: listas.listaViagens.maxWidth,height:containeres.tela.height*0.1},
						containeres.containerHorizontal]}
					>
						<View style={[containeres.containerVertical]}>
							<Pressable
								style={[botoes.botaoNav,props.estiloAux]}
								onPress={() => navigation.navigate("RotaViagem",{trajeto:trajetoIdeal,apelidoOrigem:apelidoOrigem,apelidoDestino:apelidoDestino})}
							>
								<Icon name="map-search" size={1.5*textos.texto.fontSize} color="#decc73" style={{marginRight:0.25*em}} />
								<Texto centro>
									Rota de referência
								</Texto>
							</Pressable>
						</View>
					</View>
					}

					<View style={[{width: listas.listaViagens.maxWidth,height:containeres.tela.height*0.17},
						containeres.containerHorizontal]}
					>
						<View style={[containeres.containerVertical,{width: listas.listaViagens.maxWidth/2}]}>
							<Icon name="cash-multiple" size={2.7*textos.texto.fontSize} color="#decc73" />
							<Texto centro negrito>{remuneracao}</Texto>
							<Texto centro>
								Remuneração {"\n"} estimada 
							</Texto>
						</View>
											
						<View style={[containeres.containerVertical,{width: listas.listaViagens.maxWidth/2}]}>
							
							{statusFormatado}
							{motivoCodigo != "APROVADO" || iconeEstado}
							
							{motivoCodigo != "APROVADO" &&
								<View>
									<Texto centro negrito>Motivo:</Texto> 
									<Texto centro>{motivoStatus}</Texto>
								</View>	
							}

						</View>
					</View>

					<View style={[{width: listas.listaViagens.maxWidth, height:containeres.tela.height*0.16, marginTop: containeres.tela.height*0.05},
						containeres.containerHorizontal]}
					>
						<View style={[containeres.containerVertical,{width: listas.listaViagens.maxWidth, marginLeft:0.5*textos.texto.fontSize}]}>
							<Icon name="map-marker-path" size={3*textos.texto.fontSize} color="#decc73" />
							<Texto centro> 
								Viagem de {"\n"}
								<Texto negrito> {LimitaExibicaoString(apelidoOrigem,40)} </Texto>
								até {"\n"}
								<Texto negrito> {LimitaExibicaoString(apelidoDestino,40)} </Texto>
							</Texto>
						</View>

						<View style={[containeres.containerVertical,{width: listas.listaViagens.maxWidth}]}>
							<Icon name="map-marker-distance" size={2.7*textos.texto.fontSize} color="#decc73" />
							<View style={[containeres.containerVertical]}>
								<Texto centro negrito>Distância</Texto>
								<Texto centro negrito> {distancia}</Texto>
							</View>
						</View>
					</View>
					<View style={[{width: listas.listaViagens.maxWidth,height:containeres.tela.height*0.16, marginTop: containeres.tela.height*0.01},
						containeres.containerHorizontal]}
					>
						<Icon name="timer-outline" size={2.7*textos.texto.fontSize} color="#decc73" />
						<View style={[containeres.containerVertical,{marginLeft: -listas.listaViagens.maxWidth*0.7}]}>
							<Texto centro negrito>{duracao}</Texto>
						</View>
					</View> 
				</ScrollView>
			</View>
			<BotaoNav 
				texto="Voltar"
				voltar={true}
				returnIcon={true}
			/>
		</Tela>	  

	);
}


TelaDetalhesViagem.propTypes = {
	route: PropTypes.object,
	estiloAux: PropTypes.object,
};

// Se viagem é passada no props, apenas devolve a viagem.
// Se idViagem é passado, busca essa viagem na lista.
function carregaViagem(routeParams,gDadosLocais){
	let viagem = routeParams.viagem;
	if (viagem != null && viagem != undefined){
		return viagem;
	}
	let idViagem = routeParams.idViagem;
	const listaViagens = gDadosLocais.getListaViagens();
	for (let i = 0; i < listaViagens.length;i++){
		let v = listaViagens[i];
		if (v.idViagem == idViagem){
			return v;
		}
	}
	return null;
}


function traduzCodigoMotivo(motivoCodigo){
	let parseado;
	switch (motivoCodigo){
	case "ORIGEM_DESTINO_DESCONHECIDOS":
		parseado = "Origem e destino desconhecidos";
		break;
	case "ORIGEM_DESCONHECIDA":
		parseado = "Origem desconhecida";
		break;
	case "DESTINO_DESCONHECIDO":
		parseado = "Destino desconhecido";
		break;
	case "NAO_BICICLETA":
		parseado = "A viagem não foi feita de bicicleta";
		break;
	case "ORIGEM_IGUAL_DESTINO":
		parseado = "A origem é a mesma do destino";
		break;
	case "LIMITE_VIAGENS_EXCEDIDO":
		parseado = "Limite de viagens excedido";
		break;
	case "POUCOS_PONTOS":
		parseado = "O total de pontos medidos pelo GPS é insuficiente";
		break;
	case "TEMPO_SEM_AMOSTRA":
		parseado = "O GPS ficou muito tempo sem coletar medições";
		break;
	case "DESLOCAMENTO PEQUENO":
		parseado = "Esse trajeto é menor que 1 km";
		break;
	case "PILOTO_NAO_INICIADO":
		parseado = "O piloto do Bike SP ainda não começou";
		break;
	case "VIAGEM_VALIDACAO":
		parseado = "Viagem de validação concluída com sucesso";
		break;
	case "VIAGEM_DUPLICADA":
		parseado = "Essa viagem já foi enviada";
		break;
	case "VIAGEM_INVALIDA":
		parseado = "Viagem Inválida";
		break;
	case "APROVADO":
		parseado = "A viagem foi aprovada";
		break;
	default:
		parseado = motivoCodigo;
	}
	return parseado;
}

export {TelaDetalhesViagem};
