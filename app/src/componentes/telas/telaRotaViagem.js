import React from "react";

import {
	Text,
	SafeAreaView,
	StatusBar,
	View,
} from "react-native";

import {BotaoNav} from "../atomicos/botaoNav.js";
import PropTypes from "prop-types";

import {Rota} from "../intermediarios/rota.js";
import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import Respiro from "../conteineres/respiro.js";
import Titulo from "../textos/titulo.js";
import Texto from "../textos/Texto";
import Bloco from "../conteineres/bloco.js";
import {useNavigation} from "@react-navigation/native";


function TelaRotaViagem(props){
	
	const navigation = useNavigation();
	let trajeto = props.route.params.trajeto;
	let origem = props.route.params.apelidoOrigem;
	let destino = props.route.params.apelidoDestino;
	return (
		<SafeAreaView style={containeres.tela}>
			<StatusBar
				backgroundColor= {containeres.tela.backgroundColor}
			/>
			<Bloco>
				<Titulo> Rota de Referência </Titulo>
				<Texto negrito centro>{origem}{" a "}{destino}</Texto>
			</Bloco>
			<Rota trajeto={trajeto}/>
			<View style={{width: containeres.containerMenu.width}}>
				<Text style={[textos.subtitulo,{textAlign:"center"}]}> Como a rota foi calculada?</Text>
				<Respiro minusculo/>
				<Text style={[textos.texto,{fontSize: 0.9*textos.texto.fontSize, textAlign:"justify"}]}>
					Esta rota foi pré-calculada baseando-se nos pontos de origem e de destino, e portanto não reflete o trajeto realizado pelo usuário nesta viagem especificamente. {"\n"}
					Se você	acredita que há um erro no cálculo, dirija-se ao{" "}
					<Text style={textos.redireciona}
						onPress={ () => { navigation.navigate("FaleConosco"); }}	
					>
						Fale Conosco</Text>
					{" "}para obter informações de como nos contatar.
				</Text>
			</View>			
			<BotaoNav 
				texto="Voltar"
				voltar={true}
				returnIcon={true}
			/>
		</SafeAreaView>	  
	);
}

TelaRotaViagem.propTypes = {
	route: PropTypes.object,
};

export {TelaRotaViagem};
