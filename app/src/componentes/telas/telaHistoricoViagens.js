import React, {useContext, useCallback} from "react";
import {
	View,
	Pressable
} from "react-native";

import {BotaoNav} from "../atomicos/botaoNav.js";
import {ListaViagens} from "../intermediarios/listaViagens.js";
import {FormataMoeda} from "../../classes/utils.js";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";

import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { textos } from "../../estilos/textos.js";
import { containeres } from "../../estilos/containeres.js";
import { botoes } from "../../estilos/botoes.js";
import Titulo from "../textos/titulo.js";
import Tela from "../conteineres/tela.js";
import Texto from "../textos/Texto";
import Bloco from "../conteineres/bloco.js";
import Respiro from "../conteineres/respiro.js";
import Rodape from "../conteineres/rodape.tsx";
import Cabecalho from "../conteineres/cabecalho.tsx";
import { em } from "../../estilos/globais.js";

function TelaHistoricoViagens(){

	const navigation = useNavigation();

	const gDadosLocais = useContext(contextoGerenciadorLocal);

	let totaisRemuneracoes = gDadosLocais.getTotaisRemuneracoes();

	useFocusEffect(
		useCallback(() => { 
			totaisRemuneracoes = gDadosLocais.getTotaisRemuneracoes();
		}, [])
	);

	const listaViagens = gDadosLocais.getListaViagens();
	return (
		<Tela alinhadoNoTopo centralizado>
			<Cabecalho>
				<Titulo>Histórico de Viagens</Titulo>
			</Cabecalho>	
			<Bloco horizontal>
				<View style={[containeres.containerVertical]}>
					<Icon name="cash-check" size={textos.subtitulo.fontSize*1.2} color="#c7a80c" />
					<Texto centro negrito> Já recebido:{"\n"}
						{FormataMoeda(totaisRemuneracoes.recebidos)}
					</Texto>
				</View>
				<View style={[containeres.containerVertical]}>
					<Icon name="calendar-clock" size={textos.subtitulo.fontSize*1.2} color="#c7a80c"  />
					<Texto centro negrito> A receber:{"\n"}
						{FormataMoeda(totaisRemuneracoes.areceber)}
					</Texto>
				</View>
			</Bloco>

			<Respiro pequeno/>

			<Bloco horizontal>
				<Pressable
					style={[botoes.botaoNav,{marginRight:10}]}
					onPress={() => navigation.navigate("Extrato")}
				>
					<View style={[containeres.containerHorizontal]}>
						<Icon name="piggy-bank-outline" size={1.5*textos.texto.fontSize} color="#decc73" style={{marginRight:0.25*em}} />
						<Texto centro>
								Extrato
						</Texto>
					</View>
				</Pressable>
				<Pressable
					style={[botoes.botaoNav]}
					onPress={() => navigation.navigate("Bonus")}
				>
					<View style={[containeres.containerHorizontal]}>
						<Icon name="diamond-stone" size={1.5*textos.texto.fontSize} color="#decc73" style={{marginRight:0.25*em}} />
						<Texto centro>
								Bônus
						</Texto>
					</View>
				</Pressable>
			</Bloco>

			<View style={{flex:1}}>
				<ListaViagens viagens={listaViagens}/>
			</View>

			<Rodape>	
				<BotaoNav 
					texto="Voltar"
					telaDestino="Home"
					returnIcon={true}
				/>
			</Rodape>
		</Tela>

	);
}

export {TelaHistoricoViagens};
