import React, {useContext,useState} from "react";
import {
	Text,
	Pressable,
} from "react-native";

import {BotaoNav} from "../atomicos/botaoNav.js";
import {ListaLocais} from "../intermediarios/listaLocais.js";
import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import {sincronizaLocalizacoes} from "../../acoes/sincronizaLocalizacoes.js";
import { BotaoSincronizar } from "../intermediarios/botaoSincronizar.js";
import { containeres } from "../../estilos/containeres.js";
import Tela from "../conteineres/tela.js";
import { textos } from "../../estilos/textos.js";
import RodaPe from "../conteineres/rodape.tsx";
import Cabecalho from "../conteineres/cabecalho.tsx";
import {useNavigation} from "@react-navigation/native";

function TelaLocaisCadastrados(){

	const navigation = useNavigation();

	const gDadosLocais = useContext(contextoGerenciadorLocal);

	let locsLocal = JSON.parse(gDadosLocais.getLocalmenteMem("Locais"));
	const [locs,setLocs] = useState(locsLocal); 

	return (
		<Tela alinhadoNoTopo>

			<Cabecalho style={{
				marginTop: 0, 
				alignItems:"center"}}
			>
				<Text style={[textos.titulo,{fontSize:textos.titulo.fontSize*1.15}]}>
					Locais Cadastrados
				</Text>

			</Cabecalho>
			<BotaoSincronizar
				chaveMemoria={"Locais"}
				funcaoSincroniza={async ()=>{return await sincronizaLocalizacoes(gDadosLocais);}}
				setDados={setLocs}
			/>

			<ListaLocais locais={locs}/>


			<RodaPe style={containeres.containerVertical}>	
				<Text style={[textos.texto, textos.italico,{alignSelf:"center"}]}> 
					Não vê algum dos locais que cadastrou? 
				</Text>
				<Pressable onPress={ () => {
					navigation.navigate("FaleConosco");
				}}>
					<Text style={[textos.texto, textos.italico,
						{alignSelf:"center",fontSize:textos.italico.fontSize,
							textDecorationLine: "underline"}]}>
						Fale Conosco</Text>
				</Pressable>
 
				<BotaoNav 
					texto="Voltar"
					returnIcon={true}
					estiloAux={{marginTop:10}}
					voltar={true}
				/>
			</RodaPe>
		</Tela>	 

	);
}

export {TelaLocaisCadastrados};