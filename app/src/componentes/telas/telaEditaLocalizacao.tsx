import React, { useContext, useState} from "react";

import Titulo from "../textos/titulo.js";
import Tela from "../conteineres/tela.js";
import Cabecalho from "../conteineres/cabecalho";
import RodaPe from "../conteineres/rodape";
import Label from "../textos/label.js";

import {containeres} from "../../estilos/containeres.js";
import {textos} from "../../estilos/textos.js";
import {Alert, View} from "react-native";
import {Botao} from "../atomicos/botao.js";
import {Input} from "@rneui/base";
import Regiao from "../conteineres/regiao.js";
import Subtitulo from "../textos/subtitulo.js";
import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";
import interfaceConexao from "../../gerenciadores/conexaoServidor.js";
import {sincronizaLocalizacoes} from "../../acoes/sincronizaLocalizacoes.js";
import {BotaoNav} from "../atomicos/botaoNav.js";
import {useNavigation, useRoute} from "@react-navigation/native";
import {NativeStackScreenProps} from "@react-navigation/native-stack";
import {AppParamList} from "../../App";
import {ParedeCarregando} from "../atomicos/paredeCarregando.js";

type Props = NativeStackScreenProps<AppParamList, "EditaLocalizacao">;

function formatCEP(CEP: string){
	return CEP.length > 5 ? `${CEP.slice(0, 5)}-${CEP.slice(5, 8)}` : CEP;
}

async function enderecoCEP(cep: string) {
	const CEPNumerico = cep.replace(/\D/g, ""); // Remover caracteres não numéricos
	const response = await fetch(`https://viacep.com.br/ws/${CEPNumerico}/json/`);
	if (!response.ok) {
		throw new Error("Erro ao consultar CEP");
	}
	const data = await response.json();

	// A api do ViaCEP retorna um objeto com a chave "erro" igual a true quando o CEP não é encontrado
	if ("erro" in data && data.erro === "true"){
		console.log("CEP " + cep + " não encontrado");
		return null;
	}

	return {
		rua: data.logradouro,
		bairro: data.bairro,
		cep: data.cep,
		cidade: data.localidade,
		estado: data.uf
	};
}

function TelaEditaLocalizacao(){
	
	const navigation = useNavigation<Props["navigation"]>();
	const route = useRoute<Props["route"]>();
	
	const [apelido, setApelido] = useState(route.params.local[3]);

	const enderecoFormatado = route.params.local[4]?.split("|");

	const [rua, setRua] = useState(enderecoFormatado[0]);
	const [numero, setNumero] = useState(enderecoFormatado[1]);
	const [complemento, setComplemento] = useState(enderecoFormatado[2]);
	const [bairro, setBairro] = useState(enderecoFormatado[3]);
	const [cep, setCep] = useState(formatCEP(enderecoFormatado[4]));
	const [cidade, setCidade] = useState(enderecoFormatado[5]);
	const [estado, setEstado] = useState(enderecoFormatado[6]);
	const [loading, setLoading] = useState(false);
	
	const gerenciadorLocal = useContext(contextoGerenciadorLocal);

	const cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	const token = gerenciadorLocal.getLocalmenteMem("TokenUser");
	const idLocalizacao = route.params.local[0].toString();

	const handleCepChange = (cepNovo: string) => {
		const valorNumerico = cepNovo.replace(/\D/g, ""); // Remover caracteres não numéricos
	
		const valorFormatado = formatCEP(valorNumerico);

		// Atualiza o endereço automaticamente ao preencher o CEP
		if (valorNumerico.length == 8) {
			enderecoCEP(valorNumerico).then((ender) => {
				if (ender != null) {
					setRua(ender.rua);
					setBairro(ender.bairro);
					setCidade(ender.cidade);
					setEstado(ender.estado);
				}
			}).catch(() => {
				console.log("Não foi possível consultar o CEP");
			});
		}
	
		setCep(valorFormatado);
	};

	const solicitarMudanca = async () => {

		if (rua == "" || bairro == "" || estado == "" || cidade == ""){
			Alert.alert(
				"Atenção",
				"Os campos Rua, Bairro, Estado e Cidade são obrigatórios.",
				[{text: "Ok"}],
				{cancelable: false}
			);
			return;
		}

		const endereco = rua + "|" + numero + "|" + complemento + "|" + bairro + "|" + cep.replace(/\D/g, "") + "|" + cidade + "|" + estado;

		setLoading(true);
		const [sucesso, respostaApp] = await interfaceConexao.alteraLocalizacao(cpf, token, idLocalizacao, endereco, apelido);
		setLoading(false);

		if (sucesso){
			Alert.alert(
				"Sucesso!",
				"Localização alterada com sucesso!",
				[{text: "Ok"}],
				{cancelable: false}
			);

			await sincronizaLocalizacoes(gerenciadorLocal);

			if (endereco !== route.params.local[4]){
				gerenciadorLocal.setLocalmenteMem("ValidouLocs","f");
				navigation.navigate("ValidacaoLocalizacoes");
			} else{
				navigation.replace("LocaisCadastrados");
			}

		} else{
			Alert.alert(
				"Erro ao alterar localização!",
				respostaApp,
				[{text: "Ok"}],
				{cancelable: false}
			);
		}

	};

	return (
		<Tela alinhadoNoTopo style={{width:"100%"}}>
			{loading && <ParedeCarregando/>}

			<Cabecalho>
				<Titulo>Editar Localização</Titulo>
			</Cabecalho>

			<Regiao deslizavel style={{gap: 0, paddingTop: 0}}>
				{(apelido != "Residência") &&
				<View style={{gap: 10}}>
					<Subtitulo style={{marginBottom:20}}>Alterar Apelido</Subtitulo>
					<Label>Novo Apelido</Label>
					<Input
						maxLength={40}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={apelido}
						onChangeText={(novoValor) => setApelido(novoValor)}
					/>
				</View>}


				<View style={{gap: 10}}>				
					<Subtitulo style={{marginVertical:20}}>Alterar Endereço</Subtitulo>

					<Label>CEP (opcional)</Label>
					<Input
						maxLength={9}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={cep}
						onChangeText={(novoValor) => handleCepChange(novoValor)}
					/>

					<Label>Rua *</Label>
					<Input
						maxLength={170}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={rua}
						onChangeText={(novoValor) => setRua(novoValor)}
					/>

					<Label>Número</Label>
					<Input
						maxLength={15}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={numero}
						onChangeText={(novoValor) => setNumero(novoValor)}
					/>

					<Label>Complemento</Label>
					<Input
						maxLength={15}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={complemento}
						onChangeText={(novoValor) => setComplemento(novoValor)}
					/>

					<Label>Bairro *</Label>
					<Input
						maxLength={30}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={bairro}
						onChangeText={(novoValor) => setBairro(novoValor)}
					/>

					<Label>Cidade *</Label>
					<Input
						maxLength={30}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={cidade}
						onChangeText={(novoValor) => setCidade(novoValor)}
					/>

					<Label>Estado</Label>
					<Input
						maxLength={25}
						inputContainerStyle={containeres.cadastroInputContainer}
						labelStyle={textos.texto}
						autoCapitalize="none"
						value={estado}
						onChangeText={(novoValor) => setEstado(novoValor)}
					/>
				</View>

				<View style={{marginTop:12}}>
					<Botao
						texto="Salvar"
						onPress={solicitarMudanca}
					/>
				</View>
			</Regiao>

			<RodaPe>
				<BotaoNav
					navigation={navigation}
					estiloAux={{marginTop: 12}}
					texto="Voltar"
					returnIcon={true}
					voltar={true}
				/>
			</RodaPe>
		</Tela>
	);
}

export {TelaEditaLocalizacao};