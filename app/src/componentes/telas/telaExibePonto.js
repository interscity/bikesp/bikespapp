import React, { useContext } from "react";

import {BotaoNav} from "../atomicos/botaoNav.js";
import { Mapa } from "../intermediarios/Mapa";
import PropTypes from "prop-types";
import Titulo from "../textos/titulo.js";
import Tela from "../conteineres/tela.js";
import Cabecalho from "../conteineres/cabecalho";
import RodaPe from "../conteineres/rodape";
import { Botao } from "../atomicos/botao.js";
import { Alert } from "react-native";
import interfaceConexao from "../../gerenciadores/conexaoServidor.js";
import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import {useNavigation} from "@react-navigation/native";



function TelaExibePonto(props){
	let local = props.route.params.local;
	let point = [local[1][1], local[1][0]];

	const navigation = useNavigation();

	let gerenciadorLocal = useContext(contextoGerenciadorLocal);

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");

	const handleEditaLocalizacao = async () => {
		let [status,resp ] = await interfaceConexao.checaAlteraLocalizacao(cpf, token);

		if (!status || !resp["resposta"]){
			Alert.alert(
				"Erro ao alterar localização!",
				"Você poderá alterar a localização novamente após " + resp["dias"] + (parseInt(resp["dias"]) >  1 ? " dias.": " dia."),
				[{text: "Ok"}],
				{cancelable: false}
			);

			return;
		}

		if (local[2] === "estacao"){
			Alert.alert(
				"Atenção",
				"Não é possível alterar o nome ou o endereço de uma estação.",
				[{text: "Ok"}],
				{cancelable: false}
			);
			
			return;
		}

		navigation.navigate("EditaLocalizacao", {
			local,
		});
	};


	return (
		<Tela>
			<Cabecalho>
				<Titulo>{local[3]}</Titulo>
			</Cabecalho>

			<Mapa posicaoInicial={point}/>

			<Botao 
				texto="Editar Localização"
				returnIcon={false}
				onPress={handleEditaLocalizacao}
			/>

			<RodaPe>
				<BotaoNav
					texto="Voltar"
					telaDestino="LocaisCadastrados"
					returnIcon={true}
				/>
			</RodaPe>
		</Tela>	  
	);
}

TelaExibePonto.propTypes = {
	route: PropTypes.object,
};

export {TelaExibePonto};