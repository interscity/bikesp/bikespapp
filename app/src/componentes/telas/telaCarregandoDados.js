import React, {useEffect, useContext, useState} from "react";
import { 
	View,
	Text,
	Alert,
	ToastAndroid,
} from "react-native";

import PropTypes from "prop-types";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import ProgressBar from "react-native-progress/Bar";
import DeviceInfo from "react-native-device-info";

import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";
import {contextoTrajeto} from "../../contextos/contextoTrajeto";
import {contextoLocalizador} from "../../contextos/contextoLocalizador.js";
import {contextoGerenciadorNotifis} from "../../contextos/contextoGerenciadorNotifs";
import interfaceConexao from "../../gerenciadores/conexaoServidor.js";

import {tentaRetomarTrajeto} from "../../acoes/tentaRetomarTrajeto.js";
import {sincronizaViagens} from "../../acoes/sincronizaViagens.js";
import {sincronizaContestacoes} from "../../acoes/sincronizaContestacoes.js";
import {sincronizaLocalizacoes} from "../../acoes/sincronizaLocalizacoes.js";
import {geraExtrato} from "../../acoes/geraExtrato.js";
import {geraBonus} from "../../acoes/geraBonus.js";
import {reenviaViagens} from "../../acoes/reenviaViagens.js";
import {armazenaParesValidos} from "../../acoes/armazenaParesValidos.js";
import {containeres} from "../../estilos/containeres.js";
import {textos} from "../../estilos/textos.js";
import {botoes} from "../../estilos/botoes.js";
import Subtitulo from "../textos/subtitulo.js";
import Respiro from "../conteineres/respiro.js";
import {atualizaTokenNotificacao} from "../../acoes/atualizaTokenNotificacao";
import Tela from "../conteineres/tela.js";
import RodaPe from "../conteineres/rodape";
import {useNavigation} from "@react-navigation/native";

function TelaCarregandoDados(){

	const navigation = useNavigation();

	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const gNotifs = useContext(contextoGerenciadorNotifis);
	const trajeto = useContext(contextoTrajeto);
	const localizador = useContext(contextoLocalizador);


	// Progresso de cada funcionalidade
	const [progressoReenvia,atualizaProgressoReenvia] = useState(0);
	const [progressoSincroniza,atualizaProgressoSincroniza] = useState(0);
	const [progressoSincLocs,atualizaProgressoSincLocs] = useState(0);
	const [progressoExtrato,atualizaProgressoExtrato] = useState(0);
	const [progressoBonus,atualizaProgressoBonus] = useState(0);
	const [progressoContestacoes,atualizaProgressoContestacoes] = useState(0);
	const [progressoRetoma,atualizaProgressoRetoma] = useState(0);
	const [progressoParesValidos,atualizaProgressoParesValidos] = useState(0);
	const [progressoDisco,atualizaProgressoDisco] = useState(0);
	const [progressoTokenNotif,atualizaProgressoTokenNotif] = useState(0);
	const [mensagemCarregamento,atualizaMensagemCarregamento] = useState("Carregando");
	let listaProgresso = [progressoReenvia,progressoSincroniza,
		progressoExtrato,progressoBonus,progressoRetoma,
		progressoDisco,progressoParesValidos,progressoContestacoes,progressoSincLocs,
		progressoTokenNotif];
	let progresso = 0;
	listaProgresso.forEach( p => {
		progresso += (1/listaProgresso.length) * p;
	});

	useEffect(() => {

		async function preHome(gDadosLocais){
			let resultado = await interfaceConexao.ping();

			if(resultado[0]){ // Se o servidor está online, faz requisições normais
				// Reenviar viagens
				atualizaMensagemCarregamento("Enviando viagens");
				reenviaViagens(gDadosLocais);
				atualizaProgressoReenvia(100);

				// Sincronizar viagens
				atualizaMensagemCarregamento("Sincronizando viagens");
				const [sincronizou,viagens] = await sincronizaViagens(gDadosLocais,atualizaProgressoSincroniza);
				if (!sincronizou) {
					/* empty */
					viagens;
				}
				atualizaProgressoSincroniza(100);

				// Sincronizar extrato
				atualizaMensagemCarregamento("Sincronizando extrato");
				const [gerouExtrato,extrato] = await geraExtrato(gDadosLocais);
				if (!gerouExtrato) {
					/* empty */
					extrato;
				}
				atualizaProgressoExtrato(100);

				// Sincronizar bônus
				atualizaMensagemCarregamento("Sincronizando bônus");
				const [gerouBonus,bonus] = await geraBonus(gDadosLocais);
				if (!gerouBonus) {
					/* empty */
					bonus;
				}
				atualizaProgressoBonus(100);

				// Sincronizar contestações
				atualizaMensagemCarregamento("Sincronizando contestações");
				const [sincronizouContestacoes,contestacoes] = await sincronizaContestacoes(gDadosLocais);
				if (!sincronizouContestacoes) {
					/* empty */
					contestacoes;
				}
				atualizaProgressoContestacoes(100);

				// Obter localizações
				atualizaMensagemCarregamento("Obtendo localizações");
				const [sincLocs,locs] = await sincronizaLocalizacoes(gDadosLocais);
				if (!sincLocs) {
					/* empty */
					locs;
				}
				atualizaProgressoSincLocs(100);

				const [armazenouParesValidos,paresValidos] = await armazenaParesValidos(gDadosLocais);
				if (!armazenouParesValidos) {
					/* empty */
					paresValidos;
				}
				atualizaProgressoParesValidos(100);

				atualizaMensagemCarregamento("Atualizando notificações");
				const [atualizouTokenNotif, tokenNotif] = await atualizaTokenNotificacao(gDadosLocais);
				if (!atualizouTokenNotif) {
					/* empty */
					console.debug(JSON.stringify(tokenNotif));
				}
				atualizaProgressoTokenNotif(100);
			}else{
				ToastAndroid.show("Você está em modo offline", ToastAndroid.LONG);

				console.log("Ping para o servidor falhou, usuário em modo offline.");

				// Atualiza estados para mover barra de progresso
				atualizaProgressoReenvia(100);
				atualizaProgressoSincroniza(100);
				atualizaProgressoExtrato(100);
				atualizaProgressoBonus(100);
				atualizaProgressoContestacoes(100);
				atualizaProgressoSincLocs(100);
				atualizaProgressoParesValidos(100);
				atualizaProgressoTokenNotif(100);
			}

			atualizaMensagemCarregamento("Verificando trajeto");
			// Atualiza core do geolocalizador, se necessário
			if(gDadosLocais.getLocalmenteMem("UsandoGoogleServices") == "f"){
				localizador.atualizaNucleo(false);
			}

			if(gDadosLocais.getLocalmenteMem("MostrouInstrucionalHome") == "f" && 
			gDadosLocais.getLocalmenteMem("PrimeiraExecucao") == "t"){
				atualizaProgressoRetoma(100);
				navigation.replace("InstrucoesHome");
			}else{
				// Retoma trajeto, se houver trajeto a ser retomado
				tentaRetomarTrajeto(trajeto,localizador,gDadosLocais, gNotifs,
					() => {navigation.replace("Home");});
				atualizaProgressoRetoma(100);
			}

		}

		let loginOuInstrucional = () => {
			if(gDadosLocais.getLocalmenteMem("MostrouInstrucionalCadastro") == "f" && 
			gDadosLocais.getLocalmenteMem("PrimeiraExecucao") == "t"){
				navigation.replace("InstrucoesCadastro");
			}else{
				navigation.replace("Login");
			}
		};

		if (!gDadosLocais.carregouDisco){
			atualizaMensagemCarregamento("Carregando disco");
			gDadosLocais.carregaDadosDisco(async () => {
				atualizaMensagemCarregamento("Verificando sessão");
				let logado = gDadosLocais.getLocalmenteMem("Logado") == "t";
				if(logado){
					let email = gDadosLocais.getLocalmenteMem("NomeUsuario");
					let token = gDadosLocais.getLocalmenteMem("TokenUser");
					let [propriamenteLogado, mensagemErro] = await interfaceConexao.validaSessao(email,token,DeviceInfo.getVersion());

					if(propriamenteLogado){
						preHome(gDadosLocais);		
					}else{
						Alert.alert(
							"Login expirado",
							mensagemErro,
							[{text: "Ok"}],
							{cancelable: false}
						);
						navigation.replace("Login");
					}
				}else{
					loginOuInstrucional();
				}
			},atualizaProgressoDisco);
		}
		else{
			let logado = gDadosLocais.getLocalmenteMem("Logado") == "t";
			if(logado){
				preHome(gDadosLocais);
			}else{
				loginOuInstrucional();
			}
		}
	},[]);

	return(
		<Tela>
			<View 
				testID="carregandoID"
				style={[containeres.containerVertical,{height:containeres.tela.height*0.75/*, backgroundColor:'blue'*/}]}
			>
				<BarraDeProgresso mensagem={mensagemCarregamento} progresso={progresso}/>
			</View>

			<RodaPe style={{marginBottom: 20, flexDirection: "row"}}>
				<Text style={[textos.subtitulo, {textAlign:"center"}]}>BikeSP agradece </Text>
				<Icon name="emoticon-excited-outline" size={textos.subtitulo.fontSize} color={botoes.botaoNav.borderColor} style={{alignSelf: "center"}} />
			</RodaPe>
		</Tela>
	);
}


function BarraDeProgresso(props){
	let progresso = props.progresso/100;
	let mensagem = props.mensagem;
	return(
		<View style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
			<ProgressBar progress={progresso} width={200} color="#c7a80c"/>
			<Respiro pequeno/>
			<Subtitulo> {mensagem} </Subtitulo>
		</View>
	);
}

BarraDeProgresso.propTypes = {
	progresso: PropTypes.number,
	mensagem: PropTypes.string,
};

export {TelaCarregandoDados};
