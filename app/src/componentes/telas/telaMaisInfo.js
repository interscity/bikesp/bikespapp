import React from "react";

import PropTypes from "prop-types";

import {BotaoNav} from "../atomicos/botaoNav.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { textos } from "../../estilos/textos.js";
import Tela from "../conteineres/tela.js";
import Titulo from "../textos/titulo.js";
import Subtitulo from "../textos/subtitulo.js";
import Regiao from "../conteineres/regiao.js";
import Respiro from "../conteineres/respiro.js";
import Bloco from "../conteineres/bloco.js";
import Texto from "../textos/Texto";
import { vh } from "../../estilos/globais.js";

/**
 * Tela para exibir informações úteis sobre o uso do aplicativo durante a viagem. Disponível no botão (?) na tela de viagem.
 * @param {*} props 
 * @returns 
 */
function TelaMaisInfo(props){

	let voltarPara = props.route.params.voltarPara;

	return(
		
		<Tela>	
			<Titulo>Mais Informações</Titulo>
			<Regiao altura={75*vh}>
				<Bloco>
					<Bloco horizontal centro>
						<Icon name="battery-plus" size={textos.subtitulo.fontSize} color="rgb(199, 168, 12)" />
						<Subtitulo>
							Desligue a Otimização de Bateria
						</Subtitulo>
					</Bloco>
					<Texto>
						Funções de otimizar o desempenho da bateria causam prejuízo nas medições, o que pode reprovar as viagens.
					</Texto>
				</Bloco>

				<Respiro />
				
				<Bloco>
					<Bloco horizontal centro>
						<Icon name="map-marker-path" size={textos.subtitulo.fontSize} color="rgb(199, 168, 12)" />
						<Subtitulo>
							Cálculo da Distância das Viagens
						</Subtitulo>
					</Bloco>
					<Texto>
						Para remunerar o usuário é considerada uma rota de referência, cuja distância pode não corresponder à do trajeto real da viagem.
					</Texto>
				</Bloco>
			</Regiao>

			<BotaoNav 
				texto="Voltar"
				telaDestino={voltarPara}
				returnIcon={true}
			/>
		</Tela>
	);
}

TelaMaisInfo.propTypes = {
	route: PropTypes.object,
};

export {TelaMaisInfo};
