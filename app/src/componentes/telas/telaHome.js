import React, {useContext,useEffect,useState,useCallback} from "react";
import {
	Modal,
	AppState,
} from "react-native";

import constantes from "../../../constantes.json";

import {useFocusEffect, useNavigation} from "@react-navigation/native";

import {BotaoHome} from "../atomicos/botaoHome.js";
import {FormataMoeda,FormataDistancia} from "../../classes/utils.js";
import {contextoLocalizador} from "../../contextos/contextoLocalizador.js";
import {contextoTrajeto} from "../../contextos/contextoTrajeto.js";
import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";

import {MenuApp} from "../atomicos/menuApp.js";

import {PopupInsistePermissoes} from "../intermediarios/popupInsistePermissoes.js";
import {PopupProblemaGPS} from "../intermediarios/popupProblemaGPS.js";

import {containeres} from "../../estilos/containeres.js";
import {botoes} from "../../estilos/botoes.js";
import Texto from "../textos/Texto";
import Titulo from "../textos/titulo.js";
import Tela from "../conteineres/tela.js";
import Regiao from "../conteineres/regiao.js";
import RodaPe from "../conteineres/rodape";
import Cabecalho from "../conteineres/cabecalho";

import {verificaRoot} from "../../acoes/verificaRoot";

const descobrePermissoes = {
	func: () => {
		console.log("Ainda nao definida");
	},

	nextStateHook: (proximoEstado) => {
		if (proximoEstado == "active"){
			descobrePermissoes.func();
		}
	}
};

function TelaHome(){

	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const navigation = useNavigation();

	const localizador = useContext(contextoLocalizador);
	
	const trajeto = useContext(contextoTrajeto);
	trajeto.atribuiLocalizador(localizador);

	const [permissoes,setPermissoes] = useState([false,false]);

	let GPSFalhouUltimaViagem = auxDetectaGPSFalhoUltimaViagem(gDadosLocais);

	let recomendaVerConfigs = GPSFalhouUltimaViagem;
	
	const [popupInsisteVisivel,setPopupInsisteVisivel] = useState(false); 
	const [popupProblemaGPS,setPopupProblemaGPSVisivel] = React.useState(recomendaVerConfigs);

	useEffect(() => {
		(async () => {
			console.log("Verificando Root...");
			if (await verificaRoot()) {
				console.log("Root Detectado");
				navigation.replace("Root");
			}
			console.log("Root Não Detectado");
		})();
	}, [navigation]);
	

	descobrePermissoes.func = () => {
		localizador.descobrePermissoes((coarse,fine,back) => {
			back = true;
			if(fine != permissoes[0] || back != permissoes[1]){
				// console.log("Resultado de descobrir permissoes: ", fine,back);
				setPermissoes([fine,back]);
			}
		});
	};
	
	localizador.pedePermissoes((fine)=>{
		if(fine != permissoes[0]){
			// console.log("Resultado de pedir permissoes: ", fine,back);
			setPermissoes([fine,true]);
		}});
	// pedePermissaoDoActivityRecog();	

	let dadosPessoais = gDadosLocais.getDadosGerais();
	const [dadosPessoaisObj,setDadosPessoais] = useState(dadosPessoais);

	const [textoViagem, setTextoViagem] = useState("Iniciar Viagem");
	const validouLocs = (gDadosLocais.getLocalmenteMem("ValidouLocs") !== "f");

	useFocusEffect(
		useCallback(() => { 
			if (trajeto.emAndamento){
				setTextoViagem("Continuar Viagem");
			}
			else{
				setTextoViagem("Iniciar Viagem");
			}
			if (localizador.emMedicao && (!trajeto.emAndamento || trajeto.emPausa)){
				localizador.finalizarMedicao();
			}

			// otimizacoesBateriaLigadas = localizador.estaComOtimizacaoBateria();
			GPSFalhouUltimaViagem = auxDetectaGPSFalhoUltimaViagem(gDadosLocais);
	

			if(GPSFalhouUltimaViagem && !popupProblemaGPS){
				setPopupProblemaGPSVisivel(true);
			}

			let novosDadosPessoais = gDadosLocais.getDadosGerais();
			if (novosDadosPessoais["distanciaPercorrida"] != dadosPessoais["distanciaPercorrida"] ||
				novosDadosPessoais["ganhoTotal"] != dadosPessoais["ganhoTotal"]){
				setDadosPessoais(novosDadosPessoais);
				dadosPessoais = novosDadosPessoais;
			}		
		}, [])
	);

	useEffect(() => { 
		AppState.addEventListener("change", descobrePermissoes.nextStateHook );
	},[]);

	useEffect (() =>{
		if (!validouLocs)
			navigation.navigate("ValidacaoLocalizacoes");
	},[]);

	return (
		<Tela testID="telaHomeTestID" corStatus={popupInsisteVisivel || popupProblemaGPS ? "black": containeres.tela.backgroundColor}>
			{popupInsisteVisivel &&				
				<Modal
					transparent={true}
					animationType = {"slide"}>
					<PopupInsistePermissoes callbackAceito = {() => setPopupInsisteVisivel(false)}/>
				</Modal>
			}
			{popupProblemaGPS &&
				<Modal
					transparent={true}
					animationType = {"slide"}>
					<PopupProblemaGPS 
						callbackAceito={() => {setPopupProblemaGPSVisivel(false);}}
						callbackRecusa={() => {setPopupProblemaGPSVisivel(false);}} />
				</Modal>
			}

			<Cabecalho>
				<MenuApp
					testID={constantes.menuHomeTestID}
				/>
				<Titulo>
					Olá, seja bem vindo ao BikeSP!
				</Titulo>
			</Cabecalho>

			<Regiao>
				<BotaoHome 
					testID={constantes.homeToIniciarViagem}
					texto={textoViagem}
					telaDestino="EmViagem"
					estiloAux = {botoes.botaoNavHome}
					checkCallback = {() =>{
						if(permissoes[0] && permissoes[1]){
							return true;
						}else{
							setPopupInsisteVisivel(true);
							return false;
						}
					}}
					callback={()=>{
						if (!localizador.emMedicao){
							// localizador.pedePermissoes(()=>{localizador.iniciarMedicao()});
							localizador.iniciarMedicao();
						}}}
				/>
				<BotaoHome 
					testID={constantes.homeToHistorico}
					texto="Histórico de Viagens"
					telaDestino="HistoricoViagens"
					estiloAux = {botoes.botaoNavHome}
				/>
				<BotaoHome 
					testID={constantes.homeToLocaisCadastrados}
					texto="Locais Cadastrados"
					telaDestino="LocaisCadastrados"
					estiloAux = {botoes.botaoNavHome}
				/>
				<BotaoHome
					testID={constantes.homeToFaleConosco}
					texto="Fale Conosco"
					telaDestino="FaleConosco"
					estiloAux = {botoes.botaoNavHome}
				/>  
				<BotaoHome
					testID={constantes.homeToInstrucional}
					texto="Preciso de ajuda"
					telaDestino="InstrucoesHome"
					parametrosRota={{revisando: true}}
					estiloAux={botoes.botaoNavHome}
				/>
			</Regiao>

			<RodaPe>
				<Regiao>
					<Texto centro>  
						Você já pedalou <Texto negrito>{FormataDistancia(dadosPessoaisObj.distanciaPercorrida)}</Texto> e 
						ganhou <Texto negrito>{FormataMoeda(dadosPessoaisObj.ganhoTotal)}</Texto> nos últimos {dadosPessoaisObj.dias} dias.    
					</Texto>
					<Texto centro>  
						Parabéns! Continue pedalando.       
					</Texto>
				</Regiao>
			</RodaPe>

		</Tela>
	);
}


/**
 * Dado um GerenciadorDadosLocais, devolve um booleano se a última viagem
 * foi invalida por problemas no GPS.  
 * 
 */
function auxDetectaGPSFalhoUltimaViagem(gDadosLocais){
	const listaViagens = gDadosLocais.getListaViagens();
	
	if (listaViagens.length == 0){
		return false;
	}
	const ultimaViagem = listaViagens[0];

	const motivoCodigo = ultimaViagem.motivoStatus;
	const destino = ultimaViagem.destino;
	const origem = ultimaViagem.origem;

	const problemasGPS = (motivoCodigo === "POUCOS_PONTOS" || motivoCodigo === "TEMPO_SEM_AMOSTRA") && (origem != destino);

	if(String(ultimaViagem.data) === gDadosLocais.getLocalmenteMem("UltimaViagemFalha")){
		return false;
	}

	gDadosLocais.setLocalmenteMem("UltimaViagemFalha",String(ultimaViagem.data));

	return problemasGPS;
}

export {TelaHome};
