import React, { useContext, useCallback, useState, useEffect } from "react";
import {
	AppState,
	PermissionsAndroid,
	Platform,
	Modal,
	Pressable,
} from "react-native";

import PropTypes from "prop-types";

import { BotaoSwitch } from "../atomicos/botaoSwitch.js";
import { BotaoNav } from "../atomicos/botaoNav.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { PopupGenerico } from "../atomicos/popupGenerico.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { contextoLocalizador } from "../../contextos/contextoLocalizador.js";
import { contextoTrajeto } from "../../contextos/contextoTrajeto.js";

import { useFocusEffect } from "@react-navigation/native";
import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import { botoes } from "../../estilos/botoes.js";
import Texto from "../textos/Texto";
import Titulo from "../textos/titulo.js";
import Subtitulo from "../textos/subtitulo.js";
import Respiro from "../conteineres/respiro.js";
import Regiao from "../conteineres/regiao.js";
import Bloco from "../conteineres/bloco.js";
import Tela from "../conteineres/tela.js";
import { corAmareloOuro } from "../../estilos/globais.js";
import Versao from "../textos/versao.js";
import Cabecalho from "../conteineres/cabecalho.tsx";
import RodaPe from "../conteineres/rodape.tsx";

const chaveActivities = "UsandoActivities";

async function setPermissaoAct(set){
	let permissao;
	if (Platform.Version < 28){
		permissao = true;
	}
	else{
		permissao = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACTIVITY_RECOGNITION);

	}
	set(permissao);
}

/**
 * Tela exibida clicando no botão de configurações disponível no menu colapsável na tela inicial.
 * Nessa tela o usuário tem acesso a configurações gerais de usabilidade do aplicativo
 * @param {*} props 
 * @returns 
 */
function TelaConfiguracoes(){

	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const localizador = useContext(contextoLocalizador);
	const trajeto = useContext(contextoTrajeto);

	const [permissaoActivities,setPermissaoActivities] = useState(true);
	const [otimizacoesBateriaLigadas, atualizaOtiBat] = useState(localizador.estaComOtimizacaoBateria());

	const [popupBatVisivel,attVisibilidadePopupBat] = useState(false);
	useFocusEffect(
		useCallback(() => {
			
			const listener = AppState.addEventListener("change", async function next(nextAppState){
				if (nextAppState === "active"){
					await setPermissaoAct(setPermissaoActivities);
					if (!permissaoActivities){
						gDadosLocais.setLocalmenteMem(chaveActivities,"f");
						try {
							trajeto.activityRecognition.disable();
						}
						catch (erro) {
							console.log(erro);
						}
					}
					atualizaOtiBat(localizador.estaComOtimizacaoBateria());
				}
			});

			setPermissaoAct(setPermissaoActivities);

			return () => {listener.remove();};
		}, [])
	);

	useEffect(() => { 
	},[permissaoActivities]);

	function podeMudarEstadoActivities(){
		return permissaoActivities && !trajeto.activityRecognition.isTracking() && !trajeto.emAndamento;
	}

	function onChangeSwitchActivities(val){
		if (val){
			trajeto.activityRecognition.enable();
		}
		else{							
			trajeto.activityRecognition.disable();
		}
	}

	return(
		<Tela alinhadoNoTopo centralizado corStatus={popupBatVisivel? "black": containeres.tela.backgroundColor}>
			<Cabecalho>
				<Titulo> Configurações </Titulo>
			</Cabecalho>

			<Regiao deslizavel style={{alignItems: "center"}}>
				<Bloco>
					<Subtitulo>
						Google Play Services:
					</Subtitulo>
					<Texto>
						Ferramentas da Google que usam seus dados para melhorar a validação de suas viagens. O aplicativo funciona sem elas, mas o GPS pode apresentar problemas e algumas de suas viagens podem ser reprovadas.
					</Texto>
					<Respiro pequeno/>
					<Bloco horizontal recuo comprimento="85%">
						<Bloco>
							<Texto negrito> Aperfeiçoamento do GPS:</Texto>
							<Texto >
								Esse serviço pode fazer seu GPS mais preciso, melhorando o desenho de seus trajetos e a validação de suas viagens.
							</Texto>
						</Bloco>
						<BotaoSwitch 
							gerenciador={gDadosLocais}
							chave="UsandoGoogleServices"
							onChange={(val) => {localizador.atualizaNucleo(val);}}
						/>
					</Bloco>
					<Respiro minusculo/>
					<Bloco horizontal recuo comprimento="85%">
						<Bloco>
							<Texto negrito> Reconhecimento de atividade física: </Texto>
							<Texto>
								Esse serviço facilita a detecção do meio de transporte usado na viagem. Com ele  uma viagem pode ser aceita mesmo com problemas no GPS.
							</Texto>
							{!permissaoActivities &&
								<Texto erro>
									Habilite a permissão para o reconhecimento de atividade física.
								</Texto>
							}
							{permissaoActivities && !podeMudarEstadoActivities() &&
								<Texto erro>
									Esta configuração não pode ser alterada durante uma viagem.
								</Texto>
							}
						</Bloco>
						<BotaoSwitch 
							estadoInicial={!permissaoActivities ? false:null} 
							podeMudarEstado={podeMudarEstadoActivities}
							gerenciador={gDadosLocais}
							chave={chaveActivities}
							onChange={onChangeSwitchActivities}
						/>
					</Bloco>
				</Bloco>
				
				<Respiro pequeno/>

				{otimizacoesBateriaLigadas && (<><Bloco>
					<Subtitulo>
						Otimizações de Bateria:
					</Subtitulo>

					<Texto>
						<Icon name="alert-circle-outline" size={1.2*textos.texto.fontSize} color={corAmareloOuro} />
						{" "}Detectamos que o seu celular está otimizando o uso de bateria do Bike SP. 
						Isso pode comprometer gravemente o funcionamento do seu GPS. 
					</Texto>
					<Respiro pequeno/>
					<Bloco horizontal recuo comprimento="85%">
						<Bloco>
							<Texto negrito >
									Desligar otimizações:
							</Texto>
							<Texto>
									Considere desligar as otimizações caso suas viagens sejam reprovadas por problemas no GPS.
							</Texto>
						</Bloco>
						<Pressable
							style={[{
								alignSelf: "center",
								justifyContent: "center",
								alignItems: "center",
								borderWidth: 0.7*botoes.botaoNav.borderWidth,
								borderColor: botoes.botaoNav.borderColor,
								borderRadius: 0.2*botoes.botaoNav.borderRadius,
								height: 2*textos.texto.fontSize,
								width: 2*textos.texto.fontSize
							}
							]}
							onPress={() => {attVisibilidadePopupBat(true);}}
						>
							<Icon name="battery-alert-variant-outline" size={1.5*textos.texto.fontSize} color="#decc73" />
						</Pressable>
					</Bloco>
				</Bloco>
				<Respiro pequeno />
				</>)}


			</Regiao>

			{popupBatVisivel && 
				<Modal
					transparent={true}
					animationType = {"slide"}>
					<PopupGenerico texto={
						["Você será redirecionado para as configurações do Android.",
							"Você deve então selecionar o filtro de aplicativos no canto superior esquerdo, e alterar seu valor para 'Todos'.",
							"Por fim, encontre o aplicativo Bike SP e clique nele para remover a otimização de bateria.",
						]} 
					callbackAceito={()=>{attVisibilidadePopupBat(false);localizador.abreConfigOtimizacaoBateria();}}
					callbackRecusa={()=>{attVisibilidadePopupBat(false);}}
					textoRecusa={"Cancelar"}
					textoAceito={"OK"}
					mostrarIcone={true}
					fatorEncolhimento={0.2}
					/>
				</Modal>
			}

			<RodaPe margemTopo={16}>
				<BotaoNav 
					texto="Voltar"
					returnIcon={true}
					estiloAux={{marginBottom:8}}
					voltar={true}
				/>
				<Versao/>
			</RodaPe>

		</Tela>
	);
}

TelaConfiguracoes.propTypes = {
	route: PropTypes.object,
};

export {TelaConfiguracoes};
