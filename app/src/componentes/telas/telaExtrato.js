import React ,{ useContext,	useState} from "react";
import {
	View,
	useWindowDimensions,
} from "react-native";
import { FlatList } from "react-native";
import { TabView, TabBar } from "react-native-tab-view";

import PropTypes from "prop-types";
import { BotaoNav } from "../atomicos/botaoNav.js";

import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";

import { BotaoSincronizar } from "../intermediarios/botaoSincronizar.js";

import {geraExtrato} from "../../acoes/geraExtrato.js";
import { ItemBilheteUnico } from "../atomicos/itemBilheteUnico.js";
import { ItemCredito } from "../atomicos/itemCredito.js";
import { containeres } from "../../estilos/containeres.js";
import { corFundo } from "../../estilos/globais.js";

import Titulo from "../textos/titulo.js";

function TelaExtrato(){

	/* O que vai receber do endpoint:
		Tabela Histórico: 
			(bilheteUnico,dataInicio,dataFim,ativo,concedido,aguardandoEnvio)
			
			{"bilheteUnico": [
				["99999999999999", "Tue, 10 Oct 2023 17:14:24 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["99999999999997", "Tue, 10 Oct 2023 17:15:56 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["666677", "Wed, 11 Oct 2023 14:31:26 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["888888", "Mon, 16 Oct 2023 10:53:32 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["1199507891", "Mon, 25 Sep 2023 15:54:33 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["999999", "Mon, 16 Oct 2023 11:00:19 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["345673", "Mon, 23 Oct 2023 09:56:08 GMT", "Mon, 23 Oct 2023 10:06:28 GMT", false, "R$ 0,00", "R$ 0,00"],
				["93479382749823", "Mon, 23 Oct 2023 10:06:28 GMT", null, true, "R$ 0,00", "R$ 666,00"]]
			]}
	

		Tabela Creditos:
			(dataEnvio,bilheteUnico,valor,confirmado)
			{"creditos":[

				["99999999999999", "Sat, 25 Oct 2025 00:00:00 GMT", "R$ 300,00", false],
				["99999999999999", "Fri, 25 Oct 2024 00:00:00 GMT", "R$ 12,90", true],
				["99999999999999", "Wed, 25 Oct 2023 00:00:00 GMT", "R$ 15,90", true]

			]}

		Para formatar as datas, olhar sincronizaViagens.js pois já foi feito ĺá

		Botão de sincronizar deve refazer requisição ao endpoint
		(lembrar de impedir cliques no botão enquanto resposta não chega)

		Variável "extrato" é dict com as infos pra cada tabela
	*/

	// Usado para os tabs	
	const layout = useWindowDimensions();
	const [index, setIndex] = useState(0);
	const [routes] = useState([
		{ key: "creditos", title: "Extrato" },
		{ key: "historico", title: "Meus Cartões" },
	]);
	
	const gerenciadorLocal = useContext(contextoGerenciadorLocal);
	let extratoLocal = JSON.parse(gerenciadorLocal.getLocalmenteMem("Extrato"));
	const [extrato,setExtrato] = useState(extratoLocal); 

	const renderScene =  ({ route }) => {
		switch (route.key) {
		case "historico":
			return <HistoricoTab historico={extrato["bilheteUnico"]} />;
		case "creditos":
			return <CreditosTab creditos={extrato["creditos"]} />;
		}
	};

	return(
		<>
			<View style={{backgroundColor: corFundo}}>
				<Titulo>Extrato</Titulo>
				<BotaoSincronizar
					chaveMemoria={"Extrato"}
					funcaoSincroniza={async ()=>{return await geraExtrato(gerenciadorLocal);}}
					setDados={setExtrato}
				/>
			</View>
			<TabView
				style={{height: 10,}}
				navigationState={{ index, routes }}
				renderScene={renderScene}
				renderTabBar={renderTabBar}
				onIndexChange={setIndex}
				initialLayout={{ width: layout.width}}
			>
			</TabView>

			<View
				style={{
					backgroundColor: containeres.tela.backgroundColor,
					padding: 10
				}}
			>
				<BotaoNav 
					texto="Voltar"
					telaDestino="HistoricoViagens"
					returnIcon={true}
					voltar={true}
				/>
			</View>
            
		</>

	);

}

const compare = (a, b) => {
	
	let dataFuturo = "2050-01-01";

	if (b[2] == a[2]){
		return (new Date(b[1] == null ? dataFuturo : b[1] ) - new Date(a[1] == null ? dataFuturo : a[1]));
	}
	
	return (new Date(b[2] == null ? dataFuturo : b[2] ) - new Date(a[2] == null ? dataFuturo : a[2]));
};
const HistoricoTab = (props) => {

	props.historico.sort(compare);

	return (<View style={{ flex: 1, backgroundColor: containeres.tela.backgroundColor }}>

		<FlatList
			data={props.historico}
			keyExtractor={item => item[0]}
			renderItem={({item}) => (
				<ItemBilheteUnico 
					numero={item[0]}
					dataInicio={item[1]}
					dataFim={item[2]}
					ativo={item[3]}
					concedido={item[4]}
					aguardandoEnvio={item[5]}
				/>)
			}
			persistentScrollbar={true}
		/> 
	</View>);
};

HistoricoTab.propTypes = {
	historico: PropTypes.array
};

const CreditosTab = (props) => {
	return (
	
		<View style={{ flex: 1, backgroundColor: containeres.tela.backgroundColor }}>
			<FlatList
				data={props.creditos}
				keyExtractor={item => item[1]}
				renderItem={({item}) => (
					<ItemCredito 
						data={item[1]}
						numero={item[0]}
						valor={item[2]}
						confirmado={item[3]}
						observacao={item[4]}
					/>)
				}
				persistentScrollbar={true}
			/> 
		</View>);
};

CreditosTab.propTypes = {
	creditos: PropTypes.array
};

const renderTabBar = props => (
	<TabBar
		{...props}
		indicatorStyle={{ backgroundColor: "white" }}
		style={{ backgroundColor: "#c7a80c" }}
	/>
);


export {TelaExtrato};