import React, { useContext, useState } from "react";
import {
	Text,
	View,
	FlatList
} from "react-native";

import { BotaoNav } from "../atomicos/botaoNav.js";
import { BotaoSincronizar } from "../intermediarios/botaoSincronizar.js";

import {sincronizaContestacoes} from "../../acoes/sincronizaContestacoes.js";

import { ItemContestacao } from "../atomicos/itemContestacao.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { containeres } from "../../estilos/containeres.js";
import { listas } from "../../estilos/listas.js";
import { textos } from "../../estilos/textos.js";
import { vh } from "../../estilos/globais.js";
import Tela from "../conteineres/tela.js";
import RodaPe from "../conteineres/rodape.tsx";
import Cabecalho from "../conteineres/cabecalho.tsx";

function TelaContestacoes(){

	const gerenciadorLocal = useContext(contextoGerenciadorLocal);
	let contestacoesLocal = JSON.parse(gerenciadorLocal.getLocalmenteMem("Contestacoes"));
	const [contestacoes,setContestacoes] = useState(contestacoesLocal); 

	return(
		<Tela>
			<Cabecalho style={{width: listas.listaViagens.maxWidth, 
				marginTop: listas.itemListaBonus.height*0.1, 
				alignItems:"center"}}
			>
				<Text style={[textos.titulo,{fontSize:textos.titulo.fontSize*1.15}]}>
					Contestações
				</Text>
			</Cabecalho>

			<View style={[containeres.containerListaBonus,
				{flexDirection: "column"}	
			]}>


				<BotaoSincronizar
					orientation="right"
					chaveMemoria={"Contestacoes"}
					funcaoSincroniza={async ()=>{return await sincronizaContestacoes(gerenciadorLocal);}}
					setDados={setContestacoes}
				/>

				<View style={[
					listas.listaBonus,
				]}>
					<View style={{
						maxHeight: Math.min(Math.max(1,contestacoes.length) * (1.3*listas.itemListaBonus.height),65*vh),
						backgroundColor: containeres.tela.backgroundColor,
						alignItems: "center"
					}}>
						<FlatList
							contentContainerStyle={{
								alignSelf: "baseline"
							}}
							ListEmptyComponent={
								<View style={{alignItems: "center", justifyContent: "space-evenly"}}>
									<Text style={[textos.texto,{fontSize:1.5*textos.texto.fontSize}]}> 
										Nenhuma contestação a ser exibida
									</Text>
								</View>
							}
							data={contestacoes}
							keyExtractor={item => item[0]}
							renderItem={({item}) => (
								<ItemContestacao 
									idViagem={item[0]}
									dataContestacao={item[1]}
									justificativa={item[2]}
									aprovada={item[3]}
									resposta={item[4]}
									remuneracao={item[5]}
								/>)
							}
							persistentScrollbar={true}
						/>
					</View>
				</View>
			</View>
			
			<RodaPe>
				<BotaoNav 
					texto="Voltar"
					returnIcon={true}
					voltar={true}
				/>
			</RodaPe>

		</Tela>
	);
}

export {TelaContestacoes};