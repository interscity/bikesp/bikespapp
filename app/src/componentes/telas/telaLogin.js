import React, {useState, useContext} from "react";
import {
	SafeAreaView,
	StatusBar,
	View,
	Linking,
} from "react-native";
import constantes from "../../../constantes.json";
import { BotaoHome } from "../atomicos/botaoHome.js";
import { ParedeCarregando } from "../atomicos/paredeCarregando.js";

import interfaceConexao from "../../gerenciadores/conexaoServidor.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Input } from "@rneui/themed";

import DeviceInfo from "react-native-device-info";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { Alert } from "react-native";
import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import { botoes } from "../../estilos/botoes.js";
import { listas } from "../../estilos/listas.js";
import Texto from "../textos/Texto";
import Titulo from "../textos/titulo.js";
import Label from "../textos/label.js";
import {useNavigation} from "@react-navigation/native";

/**
 * Tela inicial exibida quando o usuário não está logado a uma conta
 * @param {*} props 
 * @returns 
 */
function TelaLogin(){
	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const navigation = useNavigation();
	const [email, onChangeEmail] = React.useState("");
	const [senha, onChangeSenha] = React.useState("");
	const [msgErroEmail, setEmailError] = React.useState("");
	const [msgErroSenha, setSenhaError] = useState("");
	const [senhaProtegida, setProtecaoSenha] = React.useState(true);
	const [iconName, setIconName] = React.useState("eye");
	const [msgErroServidor, setErroServidor] = useState("");

	let funcionou;
   
	function verificaErro(){
		funcionou = true;

		setEmailError("");
		if(email==""){
			setEmailError("Digite um email");
			funcionou = false;
		}
		// else{
		// 	if(!email.includes("@"))
		// 		setEmailError("Email deve conter @");
		// 		funcionou = false;
		// 	else if(email.length<6)
		// 		setEmailError("Email deve ter mais que 6 caracteres");
		// 		funcionou = false;
		// 	else if(email.indexOf(" ") >= 0)
		// 		setEmailError("Email não pode conter espaços");
		// 		funcionou = false;
		// }

		setSenhaError("");
		// if(senha==""){
		// 	setSenhaError("Digite uma senha");
		// 	funcionou = false;
		// else if(senha.length<6)
		// 	setSenhaError("Senha deve ter mais que 6 caracteres");
		// 		funcionou = false;
            
        
		return funcionou;   
	}

	const [paredeVisivel, setParedeVisivel] = useState(false);

	function visualizarSenha()
	{
		if (senhaProtegida==true){
			setProtecaoSenha(false);
			setIconName("eye-off");
           
		}
		else{
			setProtecaoSenha(true);
			setIconName("eye");
            
		}
	}
    
	return(
		<SafeAreaView style={containeres.tela}>
			<StatusBar
				backgroundColor= {paredeVisivel ? "black": containeres.tela.backgroundColor}	
			/>
			{paredeVisivel &&
				<ParedeCarregando texto="Realizando Login"/>}

			<View style={[containeres.containerHorizontal,{height:containeres.tela.height*0.2}]}>
				<Icon name="bicycle-basket" size={textos.titulo.fontSize*2} color="rgb(199, 168, 12)"/>  
				<Titulo> Login</Titulo>
			</View>

			<View style={[containeres.containerVertical]}>
				<Label>Email</Label>
				<Input
					testID={constantes.emailLoginTestID}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroEmail}
					placeholder="Digite o seu email"
					labelStyle={textos.texto}
					labelProps={{}}
					rightIconContainerStyle={{}}
					autoCapitalize='none'
					value={email}
					onChangeText={onChangeEmail}
					autoComplete="email"
					accessible={true}
					accessibilityLabel="email_input"
				/>
				<Label>Senha</Label>
				<Input
					testID={constantes.senhaLoginTestID}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					disabledInputStyle={{ background: "#ddd" }}
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroSenha}
					placeholder="Digite sua Senha"
					secureTextEntry={senhaProtegida}
					labelStyle={textos.texto}
					labelProps={{}}
					rightIcon={
						<Icon name={iconName} color={botoes.botaoNav.borderColor} size={textos.texto.fontSize*2} 
							onPress={() => visualizarSenha()}/>
					}
					rightIconContainerStyle={{}}
					value={senha}
					autoCapitalize='none'					
					onChangeText={onChangeSenha}
					autoComplete="password"
					accessible={true}
					accessibilityLabel="password_input"
				/>
				<Texto centro redireciona={() => {navigation.navigate("EsqueceuSenha");}}>
						Esqueceu a senha? 
				</Texto>
			</View>
            
			<BotaoHome
				checkCallback = {async () => {
					if(verificaErro()){
						// Espera confirmação do servidor
						setParedeVisivel(true);
						let resultado = await interfaceConexao.tentaLogin(email,senha,DeviceInfo.getVersion());
						if(!resultado[0]){
							setErroServidor(resultado[1]);
							Alert.alert(
								"Problema no login",
								resultado[1],
								[
									{text: "Tentar novamente"}
								],
								{cancelable: true}
							);
						}else{
							setErroServidor("");
							let infoResp = resultado[1];

							let ultimoUsuarioDisco = await gDadosLocais.getLocalmenteDisco("UltimoUsuario");
							let ultimoUsuarioMem = await gDadosLocais.getLocalmenteMem("UltimoUsuario");
							console.log(email,ultimoUsuarioMem,ultimoUsuarioDisco);
							// Se mudou o usuario, reseto dados da memória e do disco
							if (ultimoUsuarioDisco != "" || ultimoUsuarioMem != ""){
								if (ultimoUsuarioMem != "" && email != ultimoUsuarioMem){
									await gDadosLocais.resetaMemDisco();
								}
								else if (ultimoUsuarioDisco != "" && email != ultimoUsuarioDisco){
									await gDadosLocais.resetaMemDisco();
								}
							}	

							gDadosLocais.setLocalmenteMem("BilheteUnico",infoResp["bu"]);
							gDadosLocais.setLocalmenteMem("CPF",infoResp["CPF"]);
							gDadosLocais.setLocalmenteMem("TokenUser",infoResp["token"]);
							gDadosLocais.setLocalmenteMem("ValidadeToken",infoResp["validadeToken"]);
							gDadosLocais.setLocalmenteMem("UltimoUsuario",email);
							if (infoResp["validouLocs"]){
								gDadosLocais.setLocalmenteMem("ValidouLocs","t");
							}
							else{
								gDadosLocais.setLocalmenteMem("ValidouLocs","f");								
							}
						}
						setParedeVisivel(false);
						return resultado[0];
					}else{
						return false;
					}}}
				texto="Fazer o login"
				telaDestino={"CarregandoDados"}
				forceReplace={true}
				estiloAux={{width:listas.itemListaViagens.maxWidth*0.5}}
				callback={async () => {
					console.log("Salvando usuário como sendo '" + email + "'");
					gDadosLocais.setLocalmenteMem("Logado","t");
					gDadosLocais.setLocalmenteMem("NomeUsuario",email);
					await gDadosLocais.salvaDadosDisco();
				}}
			/>

			<View>
				{msgErroServidor != "" && 
					<Texto erro centro> {msgErroServidor} </Texto>}

				<Texto centro redireciona={() => {navigation.navigate("FaleConosco",{"telaOrigem":"Login","textoOrigem":"Voltar à página de login"});}}>
					Dúvidas/Problemas?
				</Texto>
			</View>

			<Texto centro> 
				Não possui uma conta?
				<Texto redireciona={() => {navigation.navigate("Cadastro");}}
					testID={constantes.cadastreSeTestID}> Cadastre-se
				</Texto>
			</Texto>

			<Texto centro> 
				Você pode encontrar mais informações sobre o projeto {" "}
				<Texto redireciona={()=> {Linking.openURL("https://interscity.org/bikesp/piloto/");}}>
					aqui 
				</Texto>
			</Texto>

			<Texto italico centro>Versão v{DeviceInfo.getVersion()}</Texto>            
		</SafeAreaView>
	);

}

export {TelaLogin};
