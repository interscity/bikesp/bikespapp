import React, { useContext, useRef } from "react";
import {
	ScrollView,
	Image,
} from "react-native";

import constantes from "../../../constantes.json";

import PropTypes from "prop-types";

import { NUM_MAX_PAUSAS, MAX_SOMA_TEMPO_PAUSA } from "../../classes/utils.js";

import { Botao } from "../atomicos/botao.js";
import { pedePermissaoDoActivityRecog } from "../atomicos/pedePermissaoProActivityRecog";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { contextoLocalizador } from "../../contextos/contextoLocalizador.js";

import { containeres } from "../../estilos/containeres.js";

import Bloco from "../conteineres/bloco.js";
import Texto from "../textos/Texto";
import Titulo from "../textos/titulo.js";
import Tela from "../conteineres/tela.js";
import { em } from "../../estilos/globais.js";
import {useNavigation} from "@react-navigation/native";

function TelaInstrucionalHome(props){

	const navigation = useNavigation();
	
	const larguraTexto = 18*em;
	
	const parametrosRota = props.route.params;
	const revisando = typeof parametrosRota == "undefined" ? false : parametrosRota.revisando;

	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const localizador = useContext(contextoLocalizador);
	
	let refScrollable = useRef(null);

	let [currentX, updateCurrentX] = React.useState(0);

	let avançaScroll = () => {
		let nextX = currentX + containeres.tela.width;
		refScrollable.current.scrollTo({ x: nextX, y: 0, animated: true });
	};

	// let voltaScroll = () => {
	// 	let nextX = currentX - containeres.tela.width;
	// 	refScrollable.current.scrollTo({x: nextX, y: 0, animated: true});
	// };

	let finaliza = () => {
		gDadosLocais.setLocalmenteMem("MostrouInstrucionalHome", "t");
		gDadosLocais.setLocalmenteMem("PrimeiraExecucao", "f");
		navigation.replace("Home");
	};

	return (
		<Tela>
			<ScrollView
				scrollEnabled={false}
				ref={refScrollable}
				horizontal={true}
				pagingEnabled={true}
				onScroll={(evento) => {
					updateCurrentX(evento.nativeEvent.contentOffset.x);
				}}
			>
				<Tela centralizado style={{ height: "100%" }}>
					<Titulo>
						{revisando ? "Ajuda" : "Quase lá!"}
					</Titulo>

					<Bloco largura={larguraTexto}>
						<Texto centro>
							{revisando ? "Vamos revisar algumas informações que você precisa saber!" : "Antes de continuarmos, temos uma última rodada de informações importantes que você precisa saber."}
						</Texto>
					</Bloco>

					<Botao texto="Vamos lá" onPress={avançaScroll} />
				</Tela>
				<Tela centralizado style={{ height: "100%" }}>
					<Titulo>
						Permissões de Localização
					</Titulo>
					<Bloco largura={larguraTexto}>
						<Texto centro>
							Este app coleta a sua localização enquanto estiver com ele aberto ou minimizado para que suas viagens possam ser tratadas e recompensadas.
							{"\n\n"}
							Por favor, dê ao app a permissão para que ele possa acessar sua localização exata.
						</Texto>
					</Bloco>
					<Botao testID={constantes.permissoesOkInstrucionalPreHome1TestID} texto={"Ok"} onPress={() => {
						localizador.pedePermissoes(() => { avançaScroll(); });
					}} />
					{/* <Botao texto={"Aff"} onPress={voltaScroll} /> */}
					{/* <Botao texto={"Vamos lá!"} onPress={finaliza} /> */}
				</Tela>
				<Tela centralizado style={{ height: "100%" }}>

					<Titulo>
						Permissão de Atividade Física
					</Titulo>
					<Bloco largura={larguraTexto}>
						<Texto centro>
							Além da sua localização, o app Bike SP pode usar a detecção de atividade física. Com ela uma viagem pode ser aceita mesmo com problemas no GPS.
							{"\n\n"}
							Você não precisa conceder essa permissão se não quiser, e é possível desabilitar a funcionalidade a qualquer momento nas configurações.
						</Texto>
					</Bloco>
					<Botao testID={constantes.permissoesOkInstrucionalPreHome2TestID} texto={"Entendi"} onPress={() => {
						pedePermissaoDoActivityRecog();
						avançaScroll();
					}} />
					{/* <Botao texto={"Aff"} onPress={voltaScroll} /> */}
					{/* <Botao texto={"Vamos lá!"} onPress={finaliza} /> */}

				</Tela>
				<Tela centralizado style={{ height: "100%" }}>

					<Titulo>
						Economia de energia
					</Titulo>
					<Bloco largura={larguraTexto}>
						<Texto centro>
							Configurações como &apos;economia de energia&apos; e outras otimizações de bateria podem comprometer o funcionamento do GPS! Lembre-se de desligar a economia de energia antes de registrar viagens.
						</Texto>
						<Image source={require("../../../assets/images/energyOptm.jpeg")} style={{ height: containeres.tela.height * 0.4, width: containeres.tela.width * 0.8, alignSelf: "center" }} />

					</Bloco>
					<Botao testID={constantes.permissoesOkInstrucionalPreHome3TestID} texto={"Ok"} onPress={avançaScroll} />

				</Tela>
				<Tela centralizado style={{ height: "100%" }}>

					<Titulo>
						Contestando Viagens
					</Titulo>
					<Bloco largura={larguraTexto}>
						<Texto centro>
							Acha que uma viagem sua foi reprovada injustamente? Crie uma contestação explicando por que acredita que sua viagem deve ser aprovada.
						</Texto>
						<Image source={require("../../../assets/images/contest_print.jpeg")} style={{ height: containeres.tela.height * 0.4, width: containeres.tela.width * 0.8, alignSelf: "center", resizeMode: "contain" }} />
					</Bloco>
					<Botao testID={constantes.permissoesOkInstrucionalPreHome4TestID} texto={"Entendi"} onPress={avançaScroll} />

				</Tela>
				<Tela centralizado style={{ height: "100%" }}>

					<Titulo>
						Pausas
					</Titulo>
					<Bloco largura={larguraTexto}>
						<Texto centro>
							Vai parar para comer, ou fazer compras? Registre uma pausa! Você pode fazer até {NUM_MAX_PAUSAS} pausas que somam {MAX_SOMA_TEMPO_PAUSA} minutos em cada viagem. Só não esqueça de encerrar a pausa onde a iniciou!
						</Texto>

						<Image source={require("../../../assets/images/pausas_print.jpeg")} style={{ height: containeres.tela.height * 0.4, width: containeres.tela.width * 0.8, alignSelf: "center", resizeMode: "contain" }} />
					</Bloco>
					<Botao testID={constantes.permissoesOkInstrucionalPreHome5TestID} texto={"Ok"} onPress={avançaScroll} />

				</Tela>
				<Tela centralizado style={{ height: "100%" }}>
					<Titulo>
						Viagens perdidas
					</Titulo>

					<Bloco largura={larguraTexto}>
						<Texto centro>
							Fez alguma viagem que não foi registrada? Entre em Fale Conosco e acesse o link em destaque no meio da página.
						</Texto>
						<Image source={require("../../../assets/images/fale_conosco_print.jpeg")} style={{ height: containeres.tela.height * 0.4, width: containeres.tela.width * 0.8, alignSelf: "center", resizeMode: "contain", marginTop: containeres.tela.height * 0.04 }} />
					</Bloco>

					<Botao testID={constantes.permissoesOkInstrucionalPreHome5TestID} texto={"Ok"} onPress={avançaScroll} />
				</Tela>
				<Tela centralizado style={{ height: "100%" }}>

					<Titulo>
						Boas Viagens!
					</Titulo>
					<Bloco largura={larguraTexto}>
						<Texto centro>
							Em respeito à sua privacidade, o app não rastreia sua localização depois que você o encerra.
							Você pode bloquear a tela do seu celular ou usar outros aplicativos enquanto faz uma viagem, mas caso você encerre o app, sua viagem será perdida!
						</Texto>
					</Bloco>
					<Botao testID={constantes.permissoesOkInstrucionalPreHome6TestID} texto={"Beleza!"} onPress={finaliza} />

				</Tela>
			</ScrollView>
		</Tela>
	);
}

TelaInstrucionalHome.propTypes = {
	route: PropTypes.object,
};

export { TelaInstrucionalHome };
