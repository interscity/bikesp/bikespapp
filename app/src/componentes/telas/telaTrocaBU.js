import React ,{useContext,useState} from "react";
import {
	Text,
	View,
	Alert,
	ActivityIndicator
} from "react-native";

import {BotaoHome} from "../atomicos/botaoHome.js";

import { BotaoNav } from "../atomicos/botaoNav.js";

import interfaceConexao from "../../gerenciadores/conexaoServidor.js";
import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";
import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import { botoes } from "../../estilos/botoes.js";
import { listas } from "../../estilos/listas.js";
import Tela from "../conteineres/tela.js";
import Cabecalho from "../conteineres/cabecalho.tsx";
import RodaPe from "../conteineres/rodape.tsx";

function TelaTrocaBU(){
	const gerenciadorLocal = useContext(contextoGerenciadorLocal);

	let cpf = gerenciadorLocal.getLocalmenteMem("CPF");
	let token = gerenciadorLocal.getLocalmenteMem("TokenUser");
	
	const [aguardandoServidor,setAguardandoServidor] = useState(false);

	return(
		<Tela alinhadoNoCentro>
			<Cabecalho>
				<Text style={[textos.titulo,{alignSelf:"center", fontSize: 1.2*textos.titulo.fontSize}]}>Trocar Bilhete Único</Text>
			</Cabecalho>
			<Text style={[textos.texto, 
				{fontSize: 1.4*textos.texto.fontSize,
					padding: textos.texto.fontSize,
					textAlign: "center"}]}>
				Enviaremos um email contendo as instruções para realizar a troca do bilhete único.
				Atente-se ao envio das informações, pois só é possível realizar a troca desse documento 1 vez a cada 30 dias.
			</Text>

			{aguardandoServidor ?
				<View>
					<ActivityIndicator size={containeres.tela.height*0.1} color={botoes.botaoNav.borderColor} />
				</View>
				:
				<View>
					<ActivityIndicator size={containeres.tela.height*0.1} color={"#ffffff00"} />
				</View>
			}

			<View style={[containeres.containerVertical,]}>
				<BotaoHome
					checkCallback = {async () => {
						setAguardandoServidor(true);
						let resultado = await interfaceConexao.requisitaTrocaBU(cpf,token);

						if(!resultado[0]){
							Alert.alert(
								"Houve um problema",
								resultado[1],
								[
									{text: "Voltar"}
								],
								{cancelable: false},
							);
						} else {
							Alert.alert(
								"Sucesso",
								"Enviaremos um email para alteração do documento. Verifique a caixa de spam.",
								[
									{text: "Voltar"}
								],
								{cancelable: false}
							);
						}
						setAguardandoServidor(false);
						return resultado[0];
					}}
					texto="Solicitar troca"
					telaDestino={"Home"}
					disabled={aguardandoServidor}
					estiloAux={{width:listas.itemListaViagens.maxWidth*0.5}}
				/>
			</View>

			<RodaPe>
				<BotaoNav 
					texto="Voltar"
					telaDestino="Home"
					returnIcon={true}
				/>
			</RodaPe>

		</Tela>
	);

}

export {TelaTrocaBU};