import React, {useContext, useState, useEffect} from "react";
import {
	ScrollView,
	Text,
	View,
	Pressable,
	ActivityIndicator,
	Alert,
} from "react-native";

import constantes from "../../../constantes.json";

import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {useNavigation} from "@react-navigation/native";

import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";

import {Botao} from "../atomicos/botao.js";
import {Mapa} from "../intermediarios/Mapa";

import {LimitaExibicaoString} from "../../classes/utils.js";

import {sincronizaLocalizacoes} from "../../acoes/sincronizaLocalizacoes.js";
import {validaLocalizacoes} from "../../acoes/validaLocalizacoes.js";
import {containeres} from "../../estilos/containeres.js";
import {textos} from "../../estilos/textos.js";
import {botoes} from "../../estilos/botoes.js";
import Tela from "../conteineres/tela.js";
import Subtitulo from "../textos/subtitulo.js";
import Texto from "../textos/Texto";
import Regiao from "../conteineres/regiao.js";
import Respiro from "../conteineres/respiro.js";

let reportValidacao = {};

// constantes para os possíveis estados de validação de uma localização
const CORRETO = 1;
const INCORRETO = -1;
const NAO_PREENCHIDO = 0;

function TelaValidacaoLocalizacoes(){
	const gerenciadorLocal = useContext(contextoGerenciadorLocal);
	const [locs,setLocs] = useState(JSON.parse(gerenciadorLocal.getLocalmenteMem("Locais")));
	const [carregandoLocs,setCarregandoLocs] = useState(false);
	const [idxLocalizacao,setIdxLocalizacao] = useState(-1);
	const [mostrarTelaFinal,setMostrarTelaFinal] = useState(false);
	const [impedirNavegacao, setImpedirNavegacao] = useState(true);
	const navigation = useNavigation();

	// Sincroniza localizações e, em caso de erro, devolve usuário para o login
	async function sincronizaLocs(){
		setCarregandoLocs(true);
		const [sucesso,locs] = await sincronizaLocalizacoes(gerenciadorLocal);
		if (!sucesso){
			Alert.alert(
				"Houve um problema",
				"Não foi possível carregar suas localizações. Tente novamente mais tarde.",
				[{text: "Ok"}],
				{cancelable: false}
			);
			return false;
		}

		reportValidacao = {};
		setLocs(locs);

		console.log("CARREGOU");
		setCarregandoLocs(false);
	}

	// Sincronizar as localizações quando a página de validações for carregada
	useEffect(() =>{
		sincronizaLocs();
	},[]);

	// Cria a estrutura resultante da validação das localizações
	useEffect(()=>{
		for (let i = 0; i < locs.length;i++){
			if ((i in reportValidacao) === false){
				reportValidacao[i] = {
					"id":locs[i][0],
					"estado":0,
					"apelido":LimitaExibicaoString(locs[i][3],20),
					"coordenadas":locs[i][1]
				};
			}				
		}
	},[locs]);

	// Impedir o usuário de sair da tela de validações, até que a validação seja finalizada.
	useEffect(
		() =>
			navigation.addListener("beforeRemove", (e) => {

				if (!impedirNavegacao)
					return;
				
				e.preventDefault();
				Alert.alert(
					"Atenção!",
					"Não é possível usar o aplicativo sem validar as localizações. Se alguma localização parece errada, marque essa opção e entre em contato conosco.",
					[
						{ text: "Voltar", style: "cancel", onPress: () => {} }
					]
				);
				

			}),
		[navigation, impedirNavegacao]
	);

	return(
		<Tela centralizado style={{height:"100%"}}>
			<View style={{width: containeres.tela.maxWidth, 
				alignItems:"center"}}
			>
				<Text style={[textos.titulo,{fontSize:textos.titulo.fontSize*1.15, textAlign: "center"}]}>
					{mostrarTelaFinal ? "Resumo da validação" : "Validação das localizações"}
				</Text>
			</View>
			<View style={[{width:containeres.tela.width/1.1,maxHeight:containeres.tela.height/1.2,alignItems:"center"}]}>
				
				{carregandoLocs &&
				<View>
					<ActivityIndicator size={containeres.tela.height*0.1} color={botoes.botaoNav.borderColor} />
				</View>}

				{!carregandoLocs && idxLocalizacao === -1 && 
				<Instrucoes 
					setIdxLocalizacao={setIdxLocalizacao}
					gerenciadorLocal={gerenciadorLocal}
				/>}

				{!mostrarTelaFinal && !carregandoLocs && idxLocalizacao != -1 && 
				<Localizacoes 
					setMostrarTelaFinal={setMostrarTelaFinal}
					locs={locs}
					setIdxLocalizacao={setIdxLocalizacao}
					idxLocalizacao={idxLocalizacao}
				/>}

				{mostrarTelaFinal && 
				<ContainerEnviar
					setIdxLocalizacao={setIdxLocalizacao}
					setMostrarTelaFinal={setMostrarTelaFinal}
					gerenciadorLocal={gerenciadorLocal}
					setImpedirNavegacao={setImpedirNavegacao}
				/>}

			</View>
		</Tela>
	);
}

function Instrucoes(props){

	const setIdxLocalizacao = props.setIdxLocalizacao;
	return(
		<View>
			<Regiao deslizavel>
				<Texto>
					Suas localizações serão exibidas, uma de cada vez, e você deve indicar se elas estão corretamente posicionadas no mapa. Faça isso com atenção, pois a validação de suas viagens depende disso!{"\n"}
				</Texto>
				<Texto>
					Se houver alguma localização errada, os desenvolvedores serão
					notificados do problema e trabalharão o mais rápido possível para corrigi-lo.{"\n"}
				</Texto>
				<Texto>
					Além disso, se alguma localização estiver faltando,
					finalize a validação normalmente e então nos contate pelo email bikesp@ime.usp.br.{"\n"}
				</Texto>
				<Texto>	
					<Texto negrito>
						IMPORTANTE
					</Texto>
					: Você precisa de internet para concluir a validação das localizações.				
				</Texto>
				<Respiro />
				<Botao texto={"Começar"}
					onPress={() => {setIdxLocalizacao(0);}}
				/>
			</Regiao>
		</View>
	);
}

Instrucoes.propTypes = {
	setIdxLocalizacao: PropTypes.func,
};

function Localizacoes(props){
	const setMostrarTelaFinal = props.setMostrarTelaFinal;

	const setIdxLocalizacao = props.setIdxLocalizacao;
	const idxLocalizacao = props.idxLocalizacao;
	const locs = props.locs;
	let coordenada = locs[idxLocalizacao][1];
	let apelido = LimitaExibicaoString(locs[idxLocalizacao][3],40);

	const [estadoValidacao,setEstadoValidacao] = useState(reportValidacao[idxLocalizacao]["estado"]);

	useEffect(()=>{
		reportValidacao[idxLocalizacao]["estado"] = estadoValidacao;
	},[estadoValidacao]);

	useEffect(()=>{
		setEstadoValidacao(reportValidacao[idxLocalizacao]["estado"]);
	},[idxLocalizacao]);

	return(
		<View>
			<View style={[{flexDirection:"column",alignItems:"center",justifyContent:"center"}]}>
				<Subtitulo>
					{apelido}
				</Subtitulo>

				<Texto centro>
					A localização está correta?
				</Texto>

				<View style={[{flexDirection:"row"}]}>
					{/* Marcar localização como correta */}
					<Pressable
						testID={constantes.joiaValidacaoLocalizacaoTestID}
						style={[{marginRight:10}]}
						onPress={() => {
							if (estadoValidacao === CORRETO){
								setEstadoValidacao(NAO_PREENCHIDO);
							}
							else{
								setEstadoValidacao(CORRETO);
							}
						}}
					>
						<Icon name="thumb-up-outline" size={textos.subtitulo.fontSize*1.2}
							color={estadoValidacao === CORRETO ? "#4deb8d": "#c0cccf"}
						/>
					</Pressable>
					{/* Marcar localização como incorreta */}
					<Pressable
						testID={constantes.negativoValidacaoLocalizacaoTestID}
						style={[{marginLeft:10}]}
						onPress={() => {
							if (estadoValidacao === INCORRETO){
								setEstadoValidacao(NAO_PREENCHIDO);
							}
							else{
								setEstadoValidacao(INCORRETO);
							}
						}}
					>
						<Icon name="thumb-down-outline" size={textos.subtitulo.fontSize*1.2} 
							color={estadoValidacao === INCORRETO ? "#ff7070": "#c0cccf"}
						/>
					</Pressable>
				</View>
			</View>

			<View style={[{marginTop:30}]}>
				<Mapa 
					redimensionaWidth={0.9}
					redimensionaHeight={0.8}
					posicaoInicial={[coordenada[1],coordenada[0]]}
				/>
			</View>

			<View style={[{marginTop:30,flexDirection:"row",alignItems:"space-around",justifyContent:"space-evenly"}]}>
				<Botao texto={"Voltar"}
					onPress={() => {setIdxLocalizacao(idxLocalizacao-1);}}
				/>
				{/* Avançar para próxima tela */}
				{idxLocalizacao+1 < locs.length ?
					<Botao texto={"Próxima"}
						estiloAux={estadoValidacao === NAO_PREENCHIDO ? {opacity: 0.5} : {}}
						onPress={() => {
							if (estadoValidacao !== NAO_PREENCHIDO)
								setIdxLocalizacao(idxLocalizacao+1);
						}}
					/>
					:
					<Botao texto={"Finalizar"}
						estiloAux={estadoValidacao === NAO_PREENCHIDO ? {opacity: 0.5} : {}}
						onPress={() => {
							if (estadoValidacao !== NAO_PREENCHIDO)
								setMostrarTelaFinal(true);
						}}
					/>
				}
			</View>
		</View>
	);
}

Localizacoes.propTypes = {
	setMostrarTelaFinal: PropTypes.func,
	setIdxLocalizacao: PropTypes.func,
	idxLocalizacao: PropTypes.number,
	locs: PropTypes.array
};

function ContainerEnviar(props){
	const setMostrarTelaFinal = props.setMostrarTelaFinal;
	const setImpedirNavegacao = props.setImpedirNavegacao;
	const gerenciadorLocal = props.gerenciadorLocal;
	const setIdxLocalizacao = props.setIdxLocalizacao;
	let texto = montaTextoContainerEnviar(reportValidacao);
	const navigation = useNavigation();

	// Função para retornar o nome da tela anterior
	const econtraTelaAnterior = () => {
		const state = navigation.getState();
		const routes = state.routes;
		if (routes.length > 1) {
			const rotaAnterior = routes[routes.length - 2];
			return rotaAnterior.name;
		}
		return null;
	};

	async function enviaValidacao(){
		let locsErradas = montaLocsErradas(reportValidacao);
		const sucesso = await validaLocalizacoes(gerenciadorLocal, locsErradas);
		if (!sucesso){
			Alert.alert(
				"Houve um problema",
				"Não foi possível validar suas localizações. Tente novamente mais tarde.",
				[{text: "Ok"}],
				{cancelable: false}
			);
			// Se a validação não deu certo, recomeçar
			setMostrarTelaFinal(false);
			setIdxLocalizacao(0);
		}

		// Se a tela anterior foi a home, volta pra ela, senão leva para a tela "locaisCadastrados"
		if (econtraTelaAnterior() === "Home"){
			navigation.navigate("Home");
		} else {
			navigation.navigate("LocaisCadastrados");
		}
	}

	const BotaoAceite = () => (
		<Botao
			texto={"Finalizar"}
			onPress={() => {
				setImpedirNavegacao(false);
				enviaValidacao();
			}}
		/>
	);

	const BotaoRevalidar = () => (
		<Botao
			texto="Revalidar"
			onPress={() => {
				setMostrarTelaFinal(false);
				setIdxLocalizacao(0);
			}}
		/>
	);

	return(
		<View
			style={[{width:"90%"}]}
			contentContainerStyle={{alignItems: "center"}}>
			<ScrollView
				persistentScrollbar={true}
				contentContainerStyle={{alignItems: "center"}}
				style={{minHeight:"70%"}}
			>
				{texto.map((pTexto,i) => (
					<View key={i} style={[{marginVertical: "0.5%"}]}
						contentContainerStyle={{alignItems: "center"}}> 
						{pTexto}
					</View>
				))}
			</ScrollView>

			<View style={containeres.containerHorizontal}>
				<BotaoRevalidar/>
				<BotaoAceite/>
			</View>
		</View>
	);
}

ContainerEnviar.propTypes = {
	setMostrarTelaFinal: PropTypes.func,
	setImpedirNavegacao: PropTypes.func,
	gerenciadorLocal: PropTypes.object,
	setIdxLocalizacao: PropTypes.func
};

function montaTextoContainerEnviar(report){
	let texto = [];
	for (let i in report){
		if (report[i]["estado"] === CORRETO){
			texto.push(
				<Text style={[textos.subtitulo]}>
					{report[i]["apelido"] + ": OK"}
				</Text>
			);
		}
		else{
			texto.push(
				<Text style={[textos.subtitulo]}>
					{report[i]["apelido"]}: <Text style={{color: textos.erro.color}}>incorreta</Text> 
				</Text>
			);
		}
	}

	return texto;
}

function montaLocsErradas(report){
	let locsErradas = [];
	for (let i in report){
		if (reportValidacao[i]["estado"] === INCORRETO){
			locsErradas.push(
				{
					"idLocalizacao":reportValidacao[i]["id"],
					"coordenadas":reportValidacao[i]["coordenadas"],
				}
			);
		}
	}
	return locsErradas;
}

export {TelaValidacaoLocalizacoes};
