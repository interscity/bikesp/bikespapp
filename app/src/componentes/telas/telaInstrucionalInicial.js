import React, { useContext, useRef } from "react";
import {
	SafeAreaView,
	ScrollView,
	Linking,
	Text
} from "react-native";

import {Botao} from "../atomicos/botao.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";

import Tela from "../conteineres/tela.js";
import Texto from "../textos/Texto";
import Titulo from "../textos/titulo.js";
import Bloco from "../conteineres/bloco.js";
import {useNavigation} from "@react-navigation/native";

function TelaInstrucionalCadastro(){

	const navigation = useNavigation();

	let gDadosLocais = useContext(contextoGerenciadorLocal);

	let refScrollable = useRef(null);

	let [currentX, updateCurrentX] = React.useState(0);

	let avançaScroll = () => {
		let nextX = currentX + containeres.tela.width;
		refScrollable.current.scrollTo({x: nextX, y: 0, animated: true});
	};

	// let voltaScroll = () => {
	// 	let nextX = currentX - containeres.tela.width;
	// 	refScrollable.current.scrollTo({x: nextX, y: 0, animated: true});
	// };

	let finaliza = () => {
		gDadosLocais.setLocalmenteMem("MostrouInstrucionalCadastro","t");
		navigation.replace("Login");
	};

	return(
		<SafeAreaView style={containeres.tela}>
			<ScrollView 
				ref={refScrollable} 
				horizontal={true}
				pagingEnabled={true}
				style={containeres.containerSubTelas}
				onScroll={(evento) => {
					updateCurrentX(evento.nativeEvent.contentOffset.x);
				}}
			>
				<Tela centralizado>
					<Titulo>
						Bem Vindo(a) ao BikeSP!
					</Titulo>

					<Bloco largura={"90%"}>
						<Texto>
							Ficamos felizes em contar com a sua colaboração para melhorar a mobilidade urbana na cidade de São Paulo!
						</Texto>
					</Bloco>
					<Botao texto={"Avançar"} onPress={avançaScroll} />
				</Tela>
				<Tela centralizado>
					<Titulo>
						Chegou agora?{"\n"}Crie uma senha!
					</Titulo>

					<Bloco largura={"90%"}>
						<Texto>
							Se você já preencheu o formulário com seus dados, mas ainda não tem uma senha, você precisará criar uma pelo próprio app.
							{"\n\n"}
							Aperte o texto &quot;<Texto style={[{textDecorationLine: "underline"}]}>cadastre-se</Texto>&quot; na tela de login, insira seu RG, CPF, Bilhete Único e email e crie sua senha!
							{"\n\n"}
							Se tiver problemas no cadastro ou no login, não esqueça de consultar {" "}
							<Text 
								style={[textos.texto,
									{
										fontSize: 1.15*textos.texto.fontSize,
										textDecorationLine: "underline", 
										color:"rgb(199, 168, 12)"
									}]} 
								onPress={() => { Linking.openURL("https://interscity.org/bikesp/piloto/perguntas-frequentes/");
								}}>
							nosso FAQ
							</Text>!
						</Texto>
					</Bloco>

					<Botao texto={"Vamos lá!"} onPress={finaliza} />
				</Tela>
			</ScrollView>
		</SafeAreaView>
	);
}

export {TelaInstrucionalCadastro};
