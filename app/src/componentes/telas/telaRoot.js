import React from "react";
import { Linking } from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import Tela from "../conteineres/tela.js";
import { textos } from "../../estilos/textos.js";
import Titulo from "../textos/titulo.js";
import Regiao from "../conteineres/regiao.js";
import Texto from "../textos/Texto";
import { corAmareloOuro} from "../../estilos/globais.js";
import { corErro } from "../../estilos/globais.js";
import Bloco from "../conteineres/bloco.js";
import Respiro from "../conteineres/respiro.js";
import Cabecalho from "../conteineres/cabecalho.tsx";

export function TelaRoot(){
	return(
		<Tela centralizado alinhadoNoCentro>
			<Cabecalho>
				<Titulo style={[textos.titulo, { color: corErro }]}>Encontramos um Problema</Titulo>
			</Cabecalho>

			<Regiao deslizavel style={{alignItems: "space-around", height: "100%"}}>
				<Bloco>
					<Texto>
						Detectamos que o seu dispositivo possui programas para alterar sua localização ou que seu sistema operacional foi modificado.
					</Texto>
					<Respiro minusculo/>
					<Texto>
						Por enquanto, seu acesso ao aplicativo está restringido. Se acredita que houve um engano, nos mande um email no endereço abaixo:
					</Texto>
					<Respiro minusculo/>
					<Texto  centro redireciona={() => {Linking.openURL("mailto:bikesp@ime.usp.br");}}>
						<Icon name="email-outline" size={textos.texto.fontSize} color={corAmareloOuro}/>
						bikesp@ime.usp.br
					</Texto>
				</Bloco>

			</Regiao>

		</Tela>
	);
}
