import React, { useContext, useState } from "react";
import {
	Text,
	View,
	FlatList
} from "react-native";

import { BotaoNav } from "../atomicos/botaoNav.js";
import { BotaoSincronizar } from "../intermediarios/botaoSincronizar.js";

import {geraBonus} from "../../acoes/geraBonus.js";

import { ItemBonus } from "../atomicos/itemBonus.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { containeres } from "../../estilos/containeres.js";
import { listas } from "../../estilos/listas.js";
import { textos } from "../../estilos/textos.js";

import Titulo from "../textos/titulo.js";
import Tela from "../conteineres/tela.js";

/**
 * Tela que exibe as informações de bonus, acessível a partir da tela de histórico de viagens
 * @param {*} props 
 * @returns 
 */
function TelaBonus(){

	const gerenciadorLocal = useContext(contextoGerenciadorLocal);

	let bonusLocal = JSON.parse(gerenciadorLocal.getLocalmenteMem("Bonus"));
	const [bonus,setBonus] = useState(bonusLocal); 

	return(
		<Tela>

			<View style={{width: listas.listaViagens.maxWidth, 
				marginTop: listas.itemListaBonus.height*0.1, 
				alignItems:"center"}}
			>
				<Titulo>
					Bônus
				</Titulo>
			</View>

			<View style={[containeres.containerListaBonus,
				{flexDirection: "column"}	
			]}>
				<BotaoSincronizar
					chaveMemoria={"Bonus"}
					funcaoSincroniza={async ()=>{return await geraBonus(gerenciadorLocal);}}
					setDados={setBonus}
				/>

				<View style={[
					listas.listaBonus,
				]}>
					<View style={{
						// maxHeight: Math.min(Math.max(1,bonus.length) * (1.5*listas.itemListaBonus.height),65*vh),
						backgroundColor: containeres.tela.backgroundColor,
						alignItems: "center",
						width: 370,
						height: 350,
						borderRadius: 1
					
					}}>
						<FlatList
							contentContainerStyle={{
								alignSelf: "baseline"
							}}
							ListEmptyComponent={
								<View style={{alignItems: "center", justifyContent: "space-evenly"}}>
									<Text style={[textos.texto,{fontSize:1.5*textos.texto.fontSize}]}> 
										Nenhum bônus a ser exibido
									</Text>
								</View>
							}
							data={bonus}
							keyExtractor={item => item[0]}
							renderItem={({item}) => (
								<ItemBonus 
									idBonus={item[0]}
									nome={item[1]}
									valor={item[2]}
									ativo={item[3]}
									descricao={item[4]}
									dataInicio={item[5]}
									dataFim={item[6]}
									dataConcessao={item[7]}
								/>)
							}
							persistentScrollbar={true}
						/>
					</View>
				</View>
			</View>

			<BotaoNav 
				texto="Voltar"
				telaDestino="HistoricoViagens"
				returnIcon={true}
			/>

		</Tela>
	);
}

export {TelaBonus};