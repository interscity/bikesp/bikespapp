import React, { useContext } from "react";
import { Linking } from "react-native";

import PropTypes from "prop-types";

import { BotaoNav } from "../atomicos/botaoNav.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { SERVER_HOST } from "@env";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";
import { textos } from "../../estilos/textos.js";
import Tela from "../conteineres/tela.js";
import Titulo from "../textos/titulo.js";
import Regiao from "../conteineres/regiao.js";
import Texto from "../textos/Texto";
import { corAmareloOuro} from "../../estilos/globais.js";
import Bloco from "../conteineres/bloco.js";
import Respiro from "../conteineres/respiro.js";
import Versao from "../textos/versao.js";
import Cabecalho from "../conteineres/cabecalho.tsx";
import RodaPe from "../conteineres/rodape.tsx";
/**
 * Tela que exibi informações de contato da administração do aplicativo. Pode ser encontrada através do botão "Fale Conosco" na tela inicial
 * @param {*} props 
 * @returns 
 */
function TelaFaleConosco(props){
	
	let telaOrigem;
	let textoOrigem;
	if (props.route.params == undefined){
		telaOrigem = "Home";
		textoOrigem = "Voltar";
	}
	else{
		telaOrigem = props.route.params.telaOrigem;
		textoOrigem = props.route.params.textoOrigem;
	}

	let exibirLinkFeedback = false;
	let linkFeedbackPerdidas = SERVER_HOST + "/feedbackViagemPerdida/?token=";
	let gerenciadorLocal = useContext(contextoGerenciadorLocal);
	let logado = gerenciadorLocal.getLocalmenteMem("Logado");
	if (logado == "t"){
		exibirLinkFeedback = true;
		let token = gerenciadorLocal.getLocalmenteMem("TokenUser");
		linkFeedbackPerdidas += token;
	}

	return(
		<Tela centralizado alinhadoNoCentro>
			<Cabecalho>
				<Titulo>Fale Conosco</Titulo>
			</Cabecalho>

			<Regiao deslizavel style={{justifyContent:"center", alignItems: "space-around", height: "100%"}}>
				<Bloco>
					<Texto>
						Em caso de dúvidas sobre o projeto piloto ou sobre o aplicativo, nos contate pelo email:
					</Texto>
					<Respiro minusculo/>
					<Texto centro redireciona={() => {Linking.openURL("mailto:bikesp@ime.usp.br");}}>
						<Icon name="email-outline" size={textos.texto.fontSize} color={corAmareloOuro}/>
						bikesp@ime.usp.br
					</Texto>
				</Bloco>


				<Bloco mostrar={exibirLinkFeedback}>
					<Texto>
						Não conseguiu registrar uma viagem? {}
						<Texto redireciona={() => {Linking.openURL(linkFeedbackPerdidas);}}>Acesse este link</Texto>
						{} e nos forneça seu feedback.
					</Texto>
				</Bloco>


				<Bloco>
					<Texto centro redireciona={() => {Linking.openURL("https://interscity.org/bikesp/piloto/perguntas-frequentes/");}}>
						<Icon name="help-circle-outline"  size={textos.texto.fontSize} color={corAmareloOuro}/>
						{} Acesse o FAQ
					</Texto>
					<Respiro minusculo/>
					<Texto centro redireciona={() => {Linking.openURL("https://interscity.org/bikesp/piloto/");}}>
						<Icon name="help-circle-outline"  size={textos.texto.fontSize} color={corAmareloOuro}/>
						{} Mais informações sobre o projeto
					</Texto>
				</Bloco>

				
			</Regiao>

			<RodaPe>
				<BotaoNav 
					texto={textoOrigem}
					telaDestino={telaOrigem}
					returnIcon={true}
					estiloAux={{marginBottom:8}}
				/>
				<Versao />
			</RodaPe>

		</Tela>
	);
}

TelaFaleConosco.propTypes = {
	route: PropTypes.object,
};

export {TelaFaleConosco};
