import React, {useState, useRef, useContext} from "react";
import {
	ScrollView,
	StatusBar,
	Text,
	View,
	Alert,
} from "react-native";

import DeviceInfo from "react-native-device-info";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Input } from "@rneui/themed";

import constantes from "../../../constantes.json";

import {BotaoHome} from "../atomicos/botaoHome.js";
import { ParedeCarregando } from "../atomicos/paredeCarregando.js";

import { checaNaoVazio, checaComprimentoMínimo, checaEmailValido, checaCPFValido, checaBUValido } from "../../classes/verificaInputUtils.js";

import interfaceConexao from "../../gerenciadores/conexaoServidor.js";

import { contextoGerenciadorLocal } from "../../contextos/contextoGerenciadorLocal.js";

import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";
import { botoes } from "../../estilos/botoes.js";
import { listas } from "../../estilos/listas.js";

import Label from "../textos/label.js";
import Texto from "../textos/Texto";
import {useNavigation} from "@react-navigation/native";

/**
 * Tela exibida a partir do botão de cadastro exibido na tela de login
 * @param {*} props 
 * @returns 
 */
function TelaCadastro(){
	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const navigation = useNavigation();
	const [email, onChangeEmail] = useState("");
	const [cpf, onChangeCPF] = useState("");
	const [bu, onChangeBU] = useState("");
	const [senha, onChangeSenha] = useState("");
	const [confirmaSenha, onChangeConfirmaSenha] = useState("");
	const [msgErroEmail, setEmailError] = useState("");
	const [msgErroSenha, setSenhaError] = useState("");
	const [msgErroConfirmaSenha, setConfirmaSenhaError] = useState("");
	const [msgErroCPF, setCPFError] = useState("");
	const [msgErroBU, setBUError] = useState("");
	const [senhaProtegida, setProtecaoSenha] = useState(true);
	const [iconName, setIconName] = useState("eye");
	const [msgErroServidor, setErroServidor] = useState("");
	const [paredeVisivel, setParedeVisivel] = useState(false);

	const emailInputRef = useRef(null);
	const cpfInputRef = useRef(null);
	const BUInputRef = useRef(null);
	const senhaInputRef = useRef(null);
	const senhaConfirmaInputRef = useRef(null);

	function verificaEmail(){
		let emailValido = checaNaoVazio(email,() => {}, () => {setEmailError("Digite seu email");});
		
		if(emailValido){
			emailValido = checaEmailValido(email, () => {}, (x) => {setEmailError(x);});
		}

		if(emailValido){
			setEmailError("");
		} else {
			shakeInput(emailInputRef);
		}

		return emailValido;
	}

	function verificaSenhas(){
		let senhaValida = checaNaoVazio(senha,() => {}, () => {setSenhaError("Digite uma senha");});
		let senhaConfirmada = senha == confirmaSenha;

		if(senhaValida){
			senhaValida = checaComprimentoMínimo(senha,8, () => {}, () => {
				setSenhaError("Sua senha deve ter ao menos 8 caracteres"); senhaInputRef.current.shake();});
		}

		if(senhaValida){
			setSenhaError("");

			if(!senhaConfirmada){
				setConfirmaSenhaError("As duas senhas não são iguais");
				shakeInput(senhaConfirmaInputRef);
			}else{
				setConfirmaSenhaError("");
			}

		}else{
			shakeInput(senhaInputRef);
			setConfirmaSenhaError("");
		}

		return senhaValida && senhaConfirmada;
	}

	function verificaCPF(){
		let cpfValido = checaNaoVazio(cpf,() => {}, () => {setCPFError("Digite seu CPF");});
		
		if(cpfValido){
			cpfValido = checaCPFValido(cpf, () => {}, () => {setCPFError("CPF inválido");});
		}

		if(cpfValido){
			setCPFError("");
		} else {
			shakeInput(cpfInputRef);
		}

		return cpfValido;
	}

	function verificaBU(){
		let buValido = checaNaoVazio(bu,() => {}, () => {setBUError("Digite seu BU"); BUInputRef.current.shake();});
		
		if(buValido){
			buValido = checaBUValido(bu, () => {}, () => {setBUError("BU inválido"); BUInputRef.current.shake();});
		}

		if(buValido){
			setBUError("");
		} else {
			shakeInput(BUInputRef);
		}

		return buValido;
	}

	function verificaInputValido(){

		let emailValido = verificaEmail();
		
		let senhasValidas = verificaSenhas();

		let cpfValido = verificaCPF();

		let buValido = verificaBU();

		return emailValido && senhasValidas && cpfValido && buValido;   
	}

	function visualizarSenha(){

		if (senhaProtegida==true){
			setProtecaoSenha(false);
			setIconName("eye-off");
           
		}
		else{
			setProtecaoSenha(true);
			setIconName("eye");    
		}
	}

	return(
		<ScrollView testID={constantes.scrollCadastroTestID} contentContainerStyle={containeres.tela} style={{backgroundColor: containeres.tela.backgroundColor}}>
			<StatusBar
				backgroundColor= {paredeVisivel ? "black": containeres.tela.backgroundColor}	
			/>
			{paredeVisivel &&
				<ParedeCarregando texto="Conectando-se"/>}
			<View style={[containeres.containerHorizontal,{height:containeres.tela.height*0.15}]}> 
				<Text style={[textos.titulo,{alignSelf:"center", fontSize: 1.4*textos.titulo.fontSize}]}> Cadastro</Text>
			</View>

			<View style={[containeres.containerVertical]}>
				<Label >Email</Label>
				<Input
					testID={constantes.emailCadastroTestID}
					ref={emailInputRef}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroEmail}
					placeholder="nome@exemplo.com"
					labelStyle={textos.texto}
					autoCapitalize='none'
					value={email}
					onChangeText={onChangeEmail}
					autoComplete="username"
				/>
				<Label>CPF</Label>
				<Input
					testID={constantes.cpfCadastroTestID}
					ref={cpfInputRef}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroCPF}
					placeholder="123.456.789-01"
					labelStyle={textos.texto}
					autoCapitalize='none'
					value={cpf}
					onChangeText={onChangeCPF}
					keyboardType="number-pad"
				/>
				<Label>N° Bilhete Único</Label>
				<Input
					testID={constantes.bilheteUnicoCadastroTestID}
					ref={BUInputRef}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroBU}
					placeholder="1234 5678 9012 3456"
					labelStyle={textos.texto}
					autoCapitalize='none'
					value={bu}
					onChangeText={onChangeBU}
					keyboardType="number-pad"
				/>
				<Label>Senha</Label>
				<Input
					testID={constantes.senhaCadastroTestID}
					ref={senhaInputRef}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					disabledInputStyle={{ background: "#ddd" }}
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroSenha}
					placeholder="Digite uma senha"
					secureTextEntry={senhaProtegida}
					labelStyle={textos.texto}
					rightIcon={
						<Icon name={iconName} color={botoes.botaoNav.borderColor} size={textos.texto.fontSize*2} 
							onPress={() => visualizarSenha()}/>
					}
					rightIconContainerStyle={{}}
					value={senha}
					autoCapitalize='none'
					onChangeText={onChangeSenha}
					autoComplete="password"
				/>
				<Label>Confirme sua senha</Label>
				<Input
					testID={constantes.confirmaSenhaCadastroTestID}
					ref={senhaConfirmaInputRef}
					containerStyle={{width:containeres.containerMapa.width*0.95}} 
					disabledInputStyle={{ background: "#ddd" }}
					inputContainerStyle={containeres.cadastroInputContainer}
					errorMessage={msgErroConfirmaSenha}
					placeholder="Digite novamente a senha"
					secureTextEntry={senhaProtegida}
					labelStyle={textos.texto}
					rightIcon={
						<Icon name={iconName} color={botoes.botaoNav.borderColor} size={textos.texto.fontSize*2} 
							onPress={() => visualizarSenha()}/>
					}
					rightIconContainerStyle={{}}
					autoCapitalize='none'
					value={confirmaSenha}
					onChangeText={onChangeConfirmaSenha}
				/>
			</View>
            
			<Text style={[textos.erro, {opacity: msgErroServidor == "" ? 0 : 1}]}> 
				{msgErroServidor}
			</Text>

			<View style={containeres.containerHorizontal}>
				<Texto > 
					Possui uma conta?
					<Texto redireciona={ () => {navigation.navigate("Login");}}> Use ela
					</Texto>				
				</Texto>
			</View>

			<BotaoHome
				checkCallback = {async () => {
					if(verificaInputValido()){
						// Espera confirmação do servidor
						setParedeVisivel(true);
						let resultado = await interfaceConexao.tentaCadastro(email,cpf,bu,senha);
						if(!resultado[0]){
							setErroServidor(resultado[1]);
							Alert.alert(
								"Houve um problema",
								resultado[1],
								[
									{text: "Voltar"}
								],
								{cancelable: false},
							);
						} else {
							let resultadoLogin = await interfaceConexao.tentaLogin(email,senha,DeviceInfo.getVersion());

							if(!resultadoLogin[0]){
								setErroServidor(resultadoLogin[1]);
								Alert.alert(
									"Problema no login",
									resultadoLogin[1],
									[
										{text: "Tentar novamente"}
									],
									{cancelable: true}
								);
								setParedeVisivel(false);
								navigation.replace("Login");
								return false;
							}else{
								setErroServidor("");
								let infoResp = resultadoLogin[1];

								let ultimoUsuarioDisco = await gDadosLocais.getLocalmenteDisco("UltimoUsuario");
								let ultimoUsuarioMem = await gDadosLocais.getLocalmenteMem("UltimoUsuario");
								console.log(email,ultimoUsuarioMem,ultimoUsuarioDisco);
								// Se mudou o usuario, reseto dados da memória e do disco
								if (ultimoUsuarioDisco != "" || ultimoUsuarioMem != ""){
									if (ultimoUsuarioMem != "" && email != ultimoUsuarioMem){
										await gDadosLocais.resetaMemDisco();
									}
									else if (ultimoUsuarioDisco != "" && email != ultimoUsuarioDisco){
										await gDadosLocais.resetaMemDisco();
									}
								}	

								gDadosLocais.setLocalmenteMem("BilheteUnico",infoResp["bu"]);
								gDadosLocais.setLocalmenteMem("CPF",infoResp["CPF"]);
								gDadosLocais.setLocalmenteMem("TokenUser",infoResp["token"]);
								gDadosLocais.setLocalmenteMem("ValidadeToken",infoResp["validadeToken"]);
								gDadosLocais.setLocalmenteMem("UltimoUsuario",email);
								if (infoResp["validouLocs"]){
									gDadosLocais.setLocalmenteMem("ValidouLocs","t");
								}
								else{
									gDadosLocais.setLocalmenteMem("ValidouLocs","f");								
								}
							}
							
						}

						setParedeVisivel(false);
						return resultado[0];
					} else {
						return false;
					}}}
				texto="Cadastrar"
				telaDestino={"CarregandoDados"}
				forceReplace={true}
				estiloAux={{width:listas.itemListaViagens.maxWidth*0.5}}
				callback={async () => {
					console.log("Salvando usuário como sendo '" + email + "'");
					gDadosLocais.setLocalmenteMem("Logado","t");
					gDadosLocais.setLocalmenteMem("NomeUsuario",email);
					await gDadosLocais.salvaDadosDisco();
				}}
			/>

            
		</ScrollView>
	);

}

function shakeInput(inputRef) {
	inputRef.current.shake();
}

export {TelaCadastro};
