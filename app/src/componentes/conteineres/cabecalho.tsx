import React, { ReactNode } from "react";
import { View, ViewStyle } from "react-native";

interface CabecalhoProps {
  altura?: number;
  style?: ViewStyle | ViewStyle[];
  children?: ReactNode;
}

/**
 * Componente pensado para ser incluído dentro do componente Tela e ficar na região superior.
 * @param props.altura Define a altura do Cabecalho (opcional)
 */
const Cabecalho: React.FC<CabecalhoProps> = ({ altura, style, children }) => {
  
	return (
		<View style={[altura ? { height: altura } : {}, style]}>
			{children}
		</View>
	);
};

export default Cabecalho;