import React from "react";
import PropTypes from "prop-types";
import { SafeAreaView, StatusBar, View } from "react-native";
import { containeres } from "../../estilos/containeres";

/**
 * Componente top-level para criação de telas. O conteúdo de todas telas deve estar dentro desse componente
 * Telas cobrem a integridade da fronte do aplicativo. Caso seja necessário conteúdo deslizável, utilize uma `Regiao` internamente. Caso seja necessário inclua os componentes Cabecalho e RodaPe dentro da tela.
 * @param {*} props.barrastatus Habilita ou desabilita a barra de status (habilitada por padrão)
 * @param {*} props.corStatus define a cor da barra de status (containeres.tela.backgroundColor por padrão)
 * @param {*} props.alinhadoNoTopo Define se os items serão alinhados no topo da tela.
 * @param {*} props.alinhadoNoCentro Define se os items serão alinhados no centro da tela. Se nenhum dos alinhamentos for definido os items ficam igualmente espaçados na tela.
 * @param {*} props.centraliado Define se os elementos filho devem ser centralizados.
 * @param {*} props.children
 */
export default function Tela(props) {

	var elementoCabecalho = null;
	var elementoRodaPe = null;

	// Identificar o Cabecalho, o RodaPe e os outros elementos
	const elementosFilhosFiltrados = React.Children.map(props.children, (child) => {
		if (!React.isValidElement(child)) return;

		if (child.type.name === "Cabecalho") {
			elementoCabecalho = React.cloneElement(child, {style: [child.props.style]});
		}

		else if (child.type.name === "RodaPe") {
			elementoRodaPe = React.cloneElement(child, {style: [child.props.style]});
		} 
		
		else {
			return child;
		}
	});

	// Definir alinhamento vertical dos elementos
	var alinhamento;
	if (props.alinhadoNoTopo)
		alinhamento = "flex-start";
	else if (props.alinhadoNoCentro)
		alinhamento = "center";
	else
		alinhamento = "space-around";

	return (
		<SafeAreaView style={[containeres.tela, {flex:1}, props.style]}>

			{elementoCabecalho}
			
			<View style={[{flex:1}, {justifyContent:alinhamento}, props.centralizado && {alignItems:"center"}, props.style]}>
				{props.barraStatus && <StatusBar backgroundColor={props.corStatus} />}
				{elementosFilhosFiltrados}
			</View>

			{elementoRodaPe}

		</SafeAreaView>
	);
}

Tela.defaultProps = {
	barraStatus: true,
	corStatus: containeres.tela.backgroundColor 
};

Tela.propTypes = {
	barraStatus: PropTypes.bool,
	corStatus: PropTypes.string,
	children: PropTypes.node,
	alinhadoNoTopo: PropTypes.any,
	alinhadoNoCentro: PropTypes.any,
	centralizado: PropTypes.any,
	style: PropTypes.any
};
