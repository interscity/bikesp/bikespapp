import React from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import { containeres } from "../../estilos/containeres";

/**
 * Contêiner usado para agrupar elementos dentro de um contexto vertical.
 * @param {*} props.horizontal seus filhos serão dispostos horizontalmente
 * @param {*} props.centro Centraliza os elementos dentro do bloco
 * @param {*} props.espacado Espaça os elementos dentro do bloco para ocupar todo comprimento
 * @param {*} props.mostrar (= true) define se o bloco deve estar visível ou ser oculto
 * @param {*} props.recuo adiciona um recuo à esquerda do bloco, útil para componentes aninhados
 * @param {*} props.comprimento define o comprimento do bloco no sentido do eixo definido (horizontal ou vertical)
 * @param {*} props.largura define a largura do bloco no sentido do perpendicurar ao eixo definido (horizontal ou vertical)
 * @param {*} props.children elementos internos do bloco   
 * @returns 
 */
export default function Bloco(props) {
	return <View style={[containeres.bloco, 
		props.horizontal && containeres.bloco_horizontal, 
		props.centro && containeres.bloco_centro, 
		props.espacado && containeres.bloco_espacado, 
		props.recuo && containeres.recuo,
		props.comprimento && (props.horizontal ? {width: props.comprimento} : {height: props.comprimento}),
		props.largura && (props.horizontal ? {height: props.largura} : {width: props.largura}),
		(!props.mostrar) && {display: "none"}]}>
		{props.children}
	</View>;
}

Bloco.defaultProps = {
	mostrar: true
};

Bloco.propTypes = {
	style: PropTypes.object,
	mostrar: PropTypes.bool,
	centro: PropTypes.bool,
	espacado: PropTypes.bool,
	horizontal: PropTypes.bool,
	recuo: PropTypes.bool,
	comprimento: PropTypes.any,
	largura: PropTypes.any,
	children: PropTypes.node,
};