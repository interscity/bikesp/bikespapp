import React, {ReactNode} from "react";
import {View, ViewStyle} from "react-native";

interface RodaPeProps {
  altura?: number;
  margemTopo?: number;
  style?: ViewStyle | ViewStyle[];
  children?: ReactNode;
}

/**
 * Componente pensado para ser incluído dentro do componente Tela e ficar na região superior.
 * @param props.altura Define a altura do Rodape
 * @param props.margemTopo Define uma margem em relação ao componente superior
 */
const RodaPe: React.FC<RodaPeProps> = ({
	altura,
	margemTopo,
	style,
	children,
}) => {
	return (
		<View
			style={[
				altura ? {height: altura} : {},
				margemTopo ? {marginTop: margemTopo} : {},
				style,
			]}>
			{children}
		</View>
	);
};

export default RodaPe;
