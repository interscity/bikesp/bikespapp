import React from "react";
import PropTypes from "prop-types";
import { ScrollView, View } from "react-native";
import { containeres } from "../../estilos/containeres";

/**
 * Componente pensado para envolver coleções de componentes `Bloco` formando as diferentes regiões de uma tela
 * @param {*} props.deslizavel define a região como uma `ScrollView` 
 * @returns 
 */
export default function Regiao(props) {
	
	if (props.deslizavel) {
		return (
			<View style={[props.altura ? {height: props.altura} : {flex:1}]}>
				<ScrollView contentContainerStyle={[containeres.regiao, props.style]}>
					{props.children}
				</ScrollView>
			</View>
		);
	} else {
		return (
			<View style={[containeres.regiao, props.style, props.altura && {height: props.altura}]}>
				{props.children}
			</View>
		);
	}
}

Regiao.propTypes = {
	deslizavel: PropTypes.bool,
	style: PropTypes.object,
	altura: PropTypes.any,
	children: PropTypes.node
};