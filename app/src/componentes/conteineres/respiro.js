import React from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import { containeres } from "../../estilos/containeres";

/**
 * Simples espaçador invisível para dar um respiro visual em uma lista de itens. 
 * Seu uso é recomendado apenas para separar macroblocos distintos num mesmo contêiner vertical
 * @param {*} props.pequeno cria um respiro pequeno
 * @param {*} props.minusculo cria um respiro minúsculo
 * @returns 
 */
export default function Respiro(props) {
	return <View style={
		props.minusculo ? containeres.respiro_minusculo : 
			props.pequeno ? containeres.respiro_pequeno : containeres.respiro}></View>;
}

Respiro.propTypes = {
	pequeno: PropTypes.bool,
	minusculo: PropTypes.bool
};