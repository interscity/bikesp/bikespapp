import React , {useContext} from "react";
import {
	View,
	Text,
} from "react-native";

import PropTypes from "prop-types";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";

import { IconeSaberMais } from "./iconeSaberMais.js";

import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";

import {FormataDistancia,LocalizacaoMaisProxima,GetApelidoLocalizacaoPorId,RAIO_MAXIMO_TOLERANCIA} from "../../classes/utils.js";
import Texto from "../textos/Texto";

function HeaderInfosViagens(props){
	let locsPossiveis = props.locsPossiveis;
	let localizacao = props.localizacao;

	let locMaisProxima = LocalizacaoMaisProxima(locsPossiveis,localizacao);

	if (locMaisProxima["id"] == null || locMaisProxima["distancia"] > RAIO_MAXIMO_TOLERANCIA){
		return (
			<ForaDeAlcance/>
		);
	}
	else{
		return (
			<DentroDeAlcance idLocalizacao={locMaisProxima["id"]} distancia={locMaisProxima["distancia"]}/>
		);
	}
}

HeaderInfosViagens.propTypes = {
	locsPossiveis: PropTypes.array,
	localizacao: PropTypes.array,
};

function ForaDeAlcance(){
	return (
		<View style={[containeres.headerEmViagem,{backgroundColor:"#f4ffe6"}]}>

			<View style={[containeres.containerHorizontal]}>

				<View style={[containeres.containerVertical,
					{height: containeres.tela.height*0.3,width: containeres.containerMapa.width/4}]}
				>
					<View style={containeres.containerCircularIcon}> 
						<Icon name="map-marker-remove-outline" 
							size={containeres.containerMapa.width*0.15}  
							color={textos.texto.color} 
						/>
					</View>
				</View>

				<View style={[containeres.containerVertical,
					{height: containeres.tela.height*0.3,width: containeres.containerMapa.width*2/3}]}
				>
					<Text style={[textos.texto, {textAlign:"center"}]}> 
						Você está fora do alcance {"\n"} dos seus locais salvos.
					</Text>
				</View>

			</View>

		</View>
	);

}

function DentroDeAlcance(props){

	let idLocalizacao = props.idLocalizacao;
	let distancia = props.distancia;

	const gerenciadorLocal = useContext(contextoGerenciadorLocal);
	const locsPossiveis = JSON.parse(gerenciadorLocal.getLocalmenteMem("Locais"));

	let nomeLocalizacao = GetApelidoLocalizacaoPorId(idLocalizacao,locsPossiveis);

	return (
		<View style={[containeres.headerEmViagem,{backgroundColor:"#f4ffe6"}]}>

			<View style={[containeres.containerHorizontal]}>

				<View style={[containeres.containerVertical,
					{height: containeres.tela.height*0.3,width: containeres.containerMapa.width/4}]}
				>
					<View style={containeres.containerCircularIcon}> 
						<Icon name="map-marker-check-outline" 
							size={containeres.containerMapa.width*0.15}  
							color={textos.texto.color} 
						/>
					</View>
				</View>

				<View style={[containeres.containerVertical,
					{height: containeres.tela.height*0.3,width: containeres.containerMapa.width/2}]}
				>
					<Texto centro>
						Você está a {""}
						<Texto negrito>
							{FormataDistancia(distancia)}
						</Texto>
						{""} de {""}
						<Texto negrito>
							{nomeLocalizacao}
						</Texto>
						.
					</Texto>
				</View>

				<IconeSaberMais 
					voltarPara="EmViagem"
				/>

			</View>

		</View>
	);
}

DentroDeAlcance.propTypes = {
	idLocalizacao: PropTypes.number,
	distancia: PropTypes.number,
};

export {HeaderInfosViagens};
