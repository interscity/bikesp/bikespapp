import React from "react";

import {
	Text,
	View,
} from "react-native";

import PropTypes from "prop-types";

import { listas } from "../../estilos/listas.js";
import { globais } from "../../estilos/globais.js";
import { textos } from "../../estilos/textos.js";

import { getDataFormatada } from "./itemBilheteUnico.js";

const fontScale = 1.1;

function ItemCredito(props){
	const data = getDataFormatada(props.data);
	const observacao = props.observacao;
	return (
		
		<View style={[
			listas.itemLista, 
			listas.itemListaBilheteUnico,
			globais.shadowProp,
			{
				flexDirection: "row"
			}
		]}>
			<View style={[
				{
					flex: 1,
					justifyContent: "space-around"
				}]}>
				<Text
					style={[
						textos.texto,
						{
							textAlign: "center",
							fontSize: fontScale*textos.texto.fontSize
						}
					]}
				>
					<Text style={[textos.negrito]}>BU: </Text> {props.numero}
				</Text>
				<Text
					style={[
						textos.texto,
						{
							textAlign: "center",
							fontSize: fontScale*textos.texto.fontSize
						}
					]}
				>
					<Text style={[textos.negrito]}>Data: </Text> {data}
				</Text>
				{observacao != null && 
					<Text
						style={[
							textos.texto,
							{
								textAlign: "center",
								fontSize: fontScale*textos.texto.fontSize
							}
						]}
					>
						<Text style={[textos.negrito]}>Obs.: </Text> {observacao}
					</Text>}
					
			</View>
			<View style={[
				{
					flex: 1,
					justifyContent: "space-around"
				}]}>

				<Text
					style={[
						textos.texto,
						{
							textAlign: "center",
							fontSize: fontScale*textos.texto.fontSize
						}
					]}
				>
					<Text style={[textos.negrito]}>Valor: </Text> {props.valor}
				</Text>
				<Text style={[
					textos.texto, 
					textos.negrito,
					{
						textAlign: "center",
						fontSize: fontScale*textos.texto.fontSize
					}
				]}>
					{props.confirmado? "Creditado": "Enviado à SPTrans"}
				</Text>
			</View>
		</View>
		
	);

}

ItemCredito.propTypes = {
	numero: PropTypes.string,
	data: PropTypes.string,
	valor: PropTypes.string,
	confirmado: PropTypes.bool,
	observacao: PropTypes.string
};


export {ItemCredito};