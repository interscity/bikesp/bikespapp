import React from "react";

import {
	Text,
	TouchableHighlight,
	View,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import PropTypes from "prop-types";

import { textos } from "../../estilos/textos.js";
import { listas } from "../../estilos/listas.js";
import { em, globais } from "../../estilos/globais.js";

import {LimitaExibicaoString} from "../../classes/utils.js";
import Texto from "../textos/Texto";
import {useNavigation} from "@react-navigation/native";
/**
 * Componente responsável por encapsular um dos itens da Lista de Locais da página de
 * Listar Locais Cadastrados
 * 
 * No futuro, pode levar um link a uma página que exibe a localização do ponto.
 * 
 * Recebe em props:
 *   viagem: Objeto da Classe Viagem.
 */
function ItemListaLocais(props){

	const navigation = useNavigation();

	const iconeTipoSize = 1.8*textos.texto.fontSize;

	const local = props.local;

	const tipo = local[2];
	const apelido = local[3] ? local[3] : "Local de Apelido Desconhecido";//"Terminal muito longe"
	
	let iconeEstado;
	switch (tipo){
	case "qualquer":
		iconeEstado = <Icon name="map-marker" 
			size={iconeTipoSize}
			color="#000000"
		/>;
		break;
	case "estacao":
		iconeEstado = <Icon name="bus" 
			size={iconeTipoSize}
			color="#000000"
			style={{marginRight:0.25*em}}
		/>;
		break;
	default:
		console.error("Tipo de Localização inválido:",tipo);
	}
	return (
		<TouchableHighlight
			activeOpacity={0.7}
			underlayColor="#DDDDDD"
			onPress={() => navigation.navigate("ExibePonto", {
				local: local
			})}
		>
			<View style={[listas.itemLista,listas.itemListaLocais, globais.shadowProp]}>
				<View style={{flexDirection:"row",alignItems: "center",justifyContent: "center"}}>
					{iconeEstado}
					<Texto centro>
						{LimitaExibicaoString(apelido,28)}
					</Texto>
				</View>
				<Text style={[globais.setaIndicaClique]}>
					{">"}
				</Text>
			</View>
		</TouchableHighlight>
	);

}

ItemListaLocais.propTypes = {
	local: PropTypes.array,
};


export {ItemListaLocais};
