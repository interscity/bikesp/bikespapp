import React from "react";

import {
	Text,
	View,
	Modal,
	ActivityIndicator,
} from "react-native";

import PropTypes from "prop-types";

import { containeres } from "../../estilos/containeres";
import { botoes } from "../../estilos/botoes";

function ParedeCarregando (props) {

	return (
		<Modal
			transparent={true}
			animationType = {"slide"}
		>
			<View style={containeres.popUp}>
				<ActivityIndicator size={containeres.containerMapa.height*0.4} color={botoes.botaoNav.borderColor}/>
				<Text>{props.texto}</Text>
			</View>
		</Modal>
	);

}

ParedeCarregando.propTypes = {
	texto: PropTypes.string,
};

export {ParedeCarregando};
