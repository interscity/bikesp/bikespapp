import React, {useContext} from "react";
import {
	View,
	Pressable,
	Alert,
} from "react-native";

import { Menu, Divider } from "react-native-paper";
import PropTypes from "prop-types";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { botoes } from "../../estilos/botoes.js";
import { textos } from "../../estilos/textos.js";
import { containeres } from "../../estilos/containeres.js";

import { contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";
import { contextoTrajeto } from "../../contextos/contextoTrajeto.js";
import { finalizarViagem } from "../../acoes/finalizarViagem.js";
import { contextoLocalizador } from "../../contextos/contextoLocalizador.js";
import { convCoordDictList, LocalizacaoMaisProxima } from "../../classes/utils.js";

import {deslogaUsuario} from "../../acoes/deslogaUsuario.js";
import {useNavigation} from "@react-navigation/native";

function MenuApp(props){
	const navigation = useNavigation();

	const gDadosLocais = useContext(contextoGerenciadorLocal);
	const gLocalizacao = useContext(contextoLocalizador);
	const gTrajeto = useContext(contextoTrajeto);
	const [visible, setVisible] = React.useState(false);

	const openMenu = () => setVisible(true);
  
	const closeMenu = () => setVisible(false);

	async function clicou(paginaDestino){
		closeMenu();
		
		if(paginaDestino === "Login"){

			Alert.alert(
				"Sair",
				"Deseja realmente sair?",
				[
					{
						text: "Cancelar",
						onPress: () => {},
						style: "cancel"
					},
					{
						text: "Sair",
						onPress: async () => {
							if (gDadosLocais.getLocalmenteMem("EmViagem") === "t") {	
								await cancelaViagem(gDadosLocais, gLocalizacao, gTrajeto,navigation);
							}
							await deslogaUsuario(gDadosLocais,false,navigation);
						}
					}
				],
				{ cancelable: true}
			);
		}
		else{
			navigation.navigate(paginaDestino);
		}
		
	}

	return (
		<View 
			style={containeres.containerMenu}>
			<Menu
				contentStyle={{backgroundColor: botoes.botaoNav.backgroundColor}}
				visible={visible}
				onDismiss={closeMenu}
				anchor={
					<Pressable onPress={openMenu}>
						<Icon 
							testID={props.testID}
							name="menu" size={containeres.tela.height*0.05} color={"#c7a80c"} 
						/>
					</Pressable> 
				}>
				<Menu.Item  
					testID={props.testID + ".config"}
					leadingIcon={(props) => <Icon {...props} color={botoes.botaoNav.borderColor} name="cog" />} 
					titleStyle={textos.texto} 
					onPress={async () => {await clicou("Config");}}
					title="Configurações" 
				/>
				<Menu.Item 
					leadingIcon={(props) => <Icon {...props} color={botoes.botaoNav.borderColor} name="bullhorn" />} 
					titleStyle={textos.texto} 
					onPress={async () => {await clicou("Contestacoes");}}
					title="Ver Contestações" 
				/>
				<Menu.Item 
					leadingIcon={(props) => <Icon {...props} color={botoes.botaoNav.borderColor} name="account-edit" />} 
					titleStyle={textos.texto} 
					onPress={async () => {await clicou("TrocaBU");}}
					title="Trocar Bilhete" 
				/>
				<Divider />
				<Menu.Item  
					testID={props.testID + ".sair"}
					leadingIcon={(props) => <Icon {...props} color={botoes.botaoNav.borderColor} name="exit-to-app" />} 
					titleStyle={textos.texto} 
					onPress={async () => {await clicou("Login");}}
					title="Sair" 
				/>
			</Menu>
		</View>
       
	);

}
async function cancelaViagem(gDadosLocais, gLocalizacao, gTrajeto,navigation) {
	const locsPossiveis = JSON.parse(gDadosLocais.getLocalmenteMem("Locais"));
	const localizacao = convCoordDictList(gLocalizacao.getPosicaoAtual());
	let locMaisProxima = LocalizacaoMaisProxima(locsPossiveis, localizacao);
	await finalizarViagem(locMaisProxima["id"], gTrajeto, gDadosLocais,navigation);
}

MenuApp.propTypes = {
	testID: PropTypes.string
};


export {MenuApp};

