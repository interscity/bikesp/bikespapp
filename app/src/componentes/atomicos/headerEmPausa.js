import React from "react";
import {
	View,
	Text
} from "react-native";

import { containeres } from "../../estilos/containeres.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import TempoRestanteTimer from "./tempoRestanteTimer.js";
import Bloco from "../conteineres/bloco.js";
import { vw } from "../../estilos/globais.js";
import { textos } from "../../estilos/textos.js";

function HeaderEmPausa(){

	return (
		<View>
			<View style={containeres.headerEmPausa}>
				<Bloco comprimento={18*vw} largura={18*vw} centro>
					<Icon name="timer-off-outline" size={10*vw} color="#2b2b27" style={{backgroundColor: "#edfceb", padding: 4*vw, borderRadius: 14*vw}}/>
				</Bloco>

				<Bloco horizontal comprimento={60*vw}>
					<Text style={[textos.subtitulo]}>
						Viagem Pausada
					</Text>
				</Bloco>
			</View>
			<TempoRestanteTimer />
		</View>

	);
}

export {HeaderEmPausa};