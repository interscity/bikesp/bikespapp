import React from "react";

import {
	View,
	Pressable,
} from "react-native";

import PropTypes from "prop-types";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { containeres } from "../../estilos/containeres";
import { textos } from "../../estilos/textos";
import {useNavigation} from "@react-navigation/native";

function IconeSaberMais(props) {
	const navigation = useNavigation();

	return (
		<View style={containeres.containerCircularIcon}>
			<Pressable
				onPress={() => navigation.navigate("MaisInfo",{voltarPara:props.voltarPara})}
			>
				<Icon name="progress-question" size={textos.titulo.fontSize*1.5} color="#368DAF"/>
			</Pressable>
		</View>
	);

}

IconeSaberMais.propTypes = {
	voltarPara: PropTypes.string,
};

export {IconeSaberMais};