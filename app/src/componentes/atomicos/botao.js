import React from "react";
import {
	Text,
	TouchableHighlight,
	View
} from "react-native";

import PropTypes from "prop-types";
import { botoes } from "../../estilos/botoes";
import { textos } from "../../estilos/textos";

function Botao(props){
	
	return (
		<TouchableHighlight 
			activeOpacity={0.7}
			underlayColor="#DDDDDD"
			onPress={props.onPress}
			testID={props.testID}
			style={[botoes.botaoNav,props.estiloAux]}>
			<View
				style={{
					flexDirection:"row"
				}}
			>		
				<Text 
					style={[textos.texto, {width: botoes.botaoNav.minWidth,fontSize:textos.texto.fontSize*1.1}]}
				>
					{props.texto}
				</Text>
			</View>
		</TouchableHighlight>
	);
}

Botao.defaultProps = {
	estiloAux: {},
};

Botao.propTypes = {
	texto: PropTypes.string,
	estiloAux: PropTypes.object,
	onPress: PropTypes.func,
	testID: PropTypes.string
};

export {Botao};