import React, {useContext,useState} from "react";
import {
	View,
	Text,
} from "react-native";

import { FormataDuracao } from "../../classes/utils";
import { contextoTrajeto } from "../../contextos/contextoTrajeto.js";

import { containeres } from "../../estilos/containeres.js";
import { textos } from "../../estilos/textos.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Bloco from "../conteineres/bloco.js";
import { vw } from "../../estilos/globais.js";

function HeaderEmViagem(){
	let trajeto = useContext(contextoTrajeto);

	const [duracaoViagem,attDuracaoViagem] = useState(trajeto.getDuracaoTrajeto());

	setTimeout( ()=>{
		attDuracaoViagem(trajeto.getDuracaoTrajeto());
	} ,500);

	return (
		<View style={containeres.headerEmViagem}>
			<Bloco comprimento={18*vw} largura={18*vw} centro>
				<Icon name="bike-fast" size={10*vw} color="#2b2b27" style={{backgroundColor: "#edfceb", padding: 4*vw, borderRadius: 14*vw}}/>
			</Bloco>

			<Bloco largura={60*vw} >
				<Bloco horizontal comprimento={60*vw}>
					<Text style={[textos.subtitulo, 
						{textAlign:"center", padding: 0}]}>
						Viagem em andamento
					</Text>
				</Bloco>

				<Bloco horizontal comprimento={60*vw}>
					{FormataDuracao(duracaoViagem) != "0 s" &&
						<Text  style={[textos.texto, {fontSize: textos.texto.fontSize*1.2}]}>
							{FormataDuracao(duracaoViagem)}
						</Text>
					}
				</Bloco>
			</Bloco>
		</View>

	);
}

export {HeaderEmViagem};