import React from "react";
import {
	TouchableHighlight,
	View
} from "react-native";

import PropTypes from "prop-types";

import { botoes } from "../../estilos/botoes";
import { textos } from "../../estilos/textos";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import Texto from "../textos/Texto";
import {useNavigation} from "@react-navigation/native";

function BotaoNav(props){

	const navg = useNavigation();

	const funcBotao = props.voltar ? () => {navg.goBack();} : () => {navg.navigate(props.telaDestino);};

	return (
		<TouchableHighlight 
			activeOpacity={0.7}
			underlayColor="#DDDDDD"
			onPress={funcBotao}
			style={[botoes.botaoNav,props.estiloAux]}>
			<View
				style={{
					flexDirection:"row"
				}}
			>
				{props.returnIcon && 
				<Icon name="menu-left-outline"
					size={1.5*textos.texto.fontSize}
					color={botoes.botaoNav.borderColor} 
				/>}
		
				<Texto centro>
					{props.texto}
				</Texto>
			</View>
		</TouchableHighlight>


	);


}

BotaoNav.defaultProps = {
	voltar: false,
	returnIcon: false,
	estiloAux: {},
};

BotaoNav.propTypes = {
	returnIcon: PropTypes.bool,
	texto: PropTypes.string,
	estiloAux: PropTypes.object,
	voltar: PropTypes.bool,
	telaDestino: PropTypes.string,
};

export {BotaoNav};
