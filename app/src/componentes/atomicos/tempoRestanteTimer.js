import React, { useContext, useState } from "react";
import { View, Text } from "react-native";
import { FormataDuracao } from "../../classes/utils";
import { contextoTrajeto } from "../../contextos/contextoTrajeto";

import { textos } from "../../estilos/textos";
import { botoes } from "../../estilos/botoes";

import { MAX_SOMA_TEMPO_PAUSA } from "../../classes/utils";
import { vh } from "../../estilos/globais";

function TempoRestanteTimer () {
	let trajeto = useContext(contextoTrajeto);
	let tempoTotal = MAX_SOMA_TEMPO_PAUSA * 60 * 1000;

	const [tempoRestantePausa,attTempoRestantePausa] = useState(trajeto.getTempoPausaRestante());

	let textoTimer;

	if(tempoRestantePausa < 1000 * 60 * 2 && tempoRestantePausa >= 1000 * 60){
		textoTimer = FormataDuracao(tempoRestantePausa) + " restante";
	}else if(tempoRestantePausa > 0){ 
		textoTimer = FormataDuracao(tempoRestantePausa) + " restantes";
	}else{
		textoTimer =  "Tempo Extrapolado";
	}
	setTimeout( ()=>{
		attTempoRestantePausa(trajeto.getTempoPausaRestante());
	} ,500);

	return (
		<View style={{
			position: "absolute",
			bottom: (-3*textos.texto.fontSize - vh),
			width: "100%",
			alignItems: "center",
			zIndex: 2,
		}}>
			<View style={{
				position: "absolute",
				width: 15*textos.texto.fontSize,
				height: 3*textos.texto.fontSize,
				backgroundColor: tempoRestantePausa > 0 ? "#fff" : "#de887c",
				borderRadius: botoes.botaoNav.borderRadius,
				alignItems: "center",
				justifyContent: "center",
			}}>
				<View style={{
					width: 15*textos.texto.fontSize * (tempoRestantePausa / tempoTotal),
					height: 3*textos.texto.fontSize,
					backgroundColor: "#FFD500",
					borderRadius: botoes.botaoNav.borderRadius,
					alignItems: "center",
					justifyContent: "center",
					alignSelf: "flex-start"
				}}>
				</View>
			</View>
			<View style={{
				width: 15*textos.texto.fontSize,
				height: 3*textos.texto.fontSize,
				alignItems: "center",
				justifyContent: "center",
			}}>
				<Text style={{
					color: "black",
					textAlign: "center",
					fontWeight: "400",
					fontSize: 1.5*textos.texto.fontSize,
				}}>{textoTimer}</Text>
			</View>
		</View>
	);
}

export default TempoRestanteTimer;