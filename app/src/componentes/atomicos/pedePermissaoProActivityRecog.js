import { PermissionsAndroid, Platform } from "react-native";

// TODO: Isto é um componente?
async function pedePermissaoDoActivityRecog() {
	if (Platform.Version < 28){
		return true;
	}
	try {
		const granted = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.ACTIVITY_RECOGNITION,
		);
		if (granted === PermissionsAndroid.RESULTS.GRANTED) {
			return true;
		} else {
			return false;
		}
	} catch (err) {
		console.warn(err);
		return false;
	}
}

export { pedePermissaoDoActivityRecog };