import React, {useEffect, useState} from "react";
import {
	Switch,
} from "react-native";

import PropTypes from "prop-types";

import { botoes } from "../../estilos/botoes";

function BotaoSwitch(props){

	const gDadosLocais = props.gerenciador;
	const chave = props.chave;
	const callbackMudanca = props.onChange;
	const podeMudarEstado = props.podeMudarEstado;
	let estadoInicial = props.estadoInicial;
	if (estadoInicial == null){
		estadoInicial = gDadosLocais.getLocalmenteMem(chave) == "t";
	}

	const [estado, atualizaEstado] = useState(estadoInicial);

	function mudouValor(novoValor){
		if (podeMudarEstado()){
			const valSalvar = novoValor ? "t" : "f";
			atualizaEstado(novoValor);
			gDadosLocais.setLocalmenteMem(chave,valSalvar);
			callbackMudanca(novoValor);			
		}
	}

	useEffect(() => {
		atualizaEstado(estadoInicial);
	},[estadoInicial]);

	return (
		<Switch
			trackColor={{false: botoes.switchInativo.track, true: botoes.switchAtivo.track}}
			thumbColor={estado ? botoes.switchAtivo.thumb : botoes.switchInativo.thumb}
			ios_backgroundColor={botoes.switchInativo.track}
			onValueChange={mudouValor}
			value={estado}>
		</Switch>
	);
}

BotaoSwitch.defaultProps = {
	onChange: () => {},
	podeMudarEstado: () =>{return true;}
};

BotaoSwitch.propTypes = {
	gerenciador: PropTypes.object,
	chave: PropTypes.string,	
	onChange: PropTypes.func,
	podeMudarEstado: PropTypes.func,
	estadoInicial: PropTypes.bool
};

export {BotaoSwitch};