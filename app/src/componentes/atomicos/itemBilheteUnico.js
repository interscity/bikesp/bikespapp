import React from "react";

import {
	Text,
	View,
} from "react-native";

import PropTypes from "prop-types";
import moment from "moment";

import { listas } from "../../estilos/listas";
import { textos } from "../../estilos/textos";
import { globais } from "../../estilos/globais";
import Texto from "../textos/Texto";

const fontScale = 0.9;

function ItemBilheteUnico(props){
	const dataInicio = getDataFormatada(props.dataInicio);
	const dataFim = getDataFormatada(props.dataFim);
	const valorCreditado = props.concedido.replace(/\s/g, "");
	const valorACreditar = props.aguardandoEnvio.replace(/\s/g, "");
	const DataFimText = () => (		
		<Text style={[
			textos.texto,
			{
				textAlign: "left",
				fontSize: fontScale*textos.texto.fontSize,
			}
		]}>
			<Text style={textos.negrito}>Fim: </Text>
			{dataFim}
		</Text>	
	);
	return (
		
		<View style={[
			listas.itemLista, 
			listas.itemListaBilheteUnico,
			globais.shadowProp,
			{
				flexDirection: "row"
			}
		]}>

			<View style={[
				{
					flex: 2,
					justifyContent: "space-around"
				}]}>
				
				<View>
					<Text style={[
						textos.texto,
						{
							textAlign: "left",
							fontSize: fontScale*textos.texto.fontSize,
						}
					]}>
						<Text style={[textos.negrito]}>Status: </Text> {props.ativo ? "Ativo" : "Desativado"}
					</Text>
				</View>
				<View>
					<Text style={[
						textos.texto,
						{
							textAlign: "left",
							fontSize: fontScale*textos.texto.fontSize,
						}
					]}>
						<Text style={[textos.negrito]}>Nº: </Text> {props.numero}
					</Text>
				</View>
				<View>
					<Text style={[
						textos.texto,
						{
							textAlign: "left",
							fontSize: fontScale*textos.texto.fontSize,
						}
					]}>
						<Text style={[textos.negrito]}>Inicio: </Text> {dataInicio}
					</Text>
				</View>
				{props.dataFim && DataFimText()} 
			</View>
			<View
				style={{
					flex: 2,
				}}
			>
				<Text style={[
					textos.texto,
					{
						textAlign:"right",
						fontSize: fontScale*textos.texto.fontSize
					}
				]}>
					
					<Texto negrito >Creditado: </Texto> {valorCreditado}
					{"\n"}
					<Texto negrito>A creditar: </Texto> {valorACreditar}
				</Text>
			</View>
		</View>
		
	);

}

function getDataFormatada(data) {
	if (data == null) return "";
	data = data.substring(5);
	data = moment(data);
	data = new Date(data);
	return data.getDate() + "/" + (data.getMonth() + 1) + "/" + data.getFullYear();
}

ItemBilheteUnico.propTypes = {
	numero: PropTypes.string,
	dataInicio: PropTypes.string,
	dataFim: PropTypes.string,
	ativo: PropTypes.bool,
	concedido: PropTypes.string,
	aguardandoEnvio: PropTypes.string
};


export {ItemBilheteUnico};
export {getDataFormatada};
