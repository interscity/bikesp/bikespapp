import React from "react";
import {
	View,
	Text,
} from "react-native";

import { containeres } from "../../estilos/containeres";
import { textos } from "../../estilos/textos";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function HeaderGPSIndisponivel(){

	return (
		<View style={[containeres.headerEmViagem,{backgroundColor:"#c36060"}]}>
		
			<View style={[containeres.containerHorizontal]}>

				<View style={[containeres.containerVertical,
					{height: containeres.tela.height*0.3,width: containeres.containerMapa.width/3}]}
				>
					<View style={[containeres.containerCircularIcon,{backgroundColor:"#edfceb"}]}>
						<Icon name="broadcast-off" size={containeres.containerMapa.width*0.11} color="#c36060" />
					</View>
				</View>

				<View style={[containeres.containerVertical,
					{height: containeres.tela.height*0.3,width: containeres.containerMapa.width/2}]}
				>
					<Text  style={[textos.texto, 
						{textAlign:"center",fontWeight: "800",fontSize:1.4*textos.texto.fontSize}]}>
						Serviço de GPS indisponível
					</Text>
				</View>

			</View>

		</View>

	);
}

export {HeaderGPSIndisponivel};