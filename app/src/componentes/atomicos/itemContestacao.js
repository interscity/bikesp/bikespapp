import React, {useState} from "react";

import {
	Text,
	View,
	Pressable,
	ScrollView
} from "react-native";

import PropTypes from "prop-types";

import { listas } from "../../estilos/listas.js";
import { textos } from "../../estilos/textos.js";
import { botoes } from "../../estilos/botoes.js";
import { globais } from "../../estilos/globais.js";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { getDataFormatada } from "./itemBilheteUnico.js";
import {useNavigation} from "@react-navigation/native";

function ItemContestacao(props){
	
	const idViagem = props.idViagem;
	const dataContestacao = getDataFormatada(props.dataContestacao);
	const justificativa = props.justificativa;
	const aprovada = props.aprovada;
	const resposta = props.resposta;
	const remuneracao = props.remuneracao;

	const [justificativaVisivel,atttVisibiJust] = useState(false);
	const [respostaVisivel,atttVisibiResp] = useState(false);

	const navigation = useNavigation();

	return (
		<View>
			{!justificativaVisivel && !respostaVisivel &&
			<View style={[listas.itemListaContestacoes,globais.shadowProp,
				{
					borderColor: aprovada ? "#4deb8d" : aprovada != null ? "#ff7070" : "#ffec8f",
					borderWidth:1
				}]}>
				<View style={[{
					flexDirection:"row",
					alignItems:"center",
					justifyContent:"flex-start",
					marginTop:listas.itemListaContestacoes.height*0.05,
				}]}>
					<Text style={[textos.texto,{fontSize:textos.texto.fontSize*1.4}]}>
						Contestação {" "}</Text>

					<Pressable
						onPress={()=>{navigation.navigate("DetalhesViagem",{idViagem:idViagem});}}
						style={[botoes.pressableItemContestacao]}
					>
						<Text style={[textos.texto,
							{fontWeight:"bold",}]}
						>
							#{idViagem}
						</Text>
					</Pressable>
					<View style={{flex:1,alignItems:"flex-end"}}>
						{aprovada ?
							<Icon name="checkbox-marked" size={textos.subtitulo.fontSize*1.3} color="#099642"  />:
							aprovada != null ? <Icon name="close-box-outline" size={textos.subtitulo.fontSize*1.3} color="#a34040"  />:
								<Icon name="clock-outline" size={textos.subtitulo.fontSize*1.3} color={botoes.botaoNav.borderColor}  />
						}
					</View>					
				</View>
				<View style={{
					flexDirection:"row",
					alignItems: "center",
					justifyContent: "flex-start",
					marginTop:listas.itemListaContestacoes.height*0.01,
				}}>
					<View style={[{marginLeft:listas.itemListaContestacoes.width/40,marginRight:listas.itemListaContestacoes.width/40}]}>
						<Text style={[textos.texto]}>
							Data da contestação: {dataContestacao}
						</Text>
					</View>
				</View>
				<View style={{
					flexDirection:"row",
					alignItems: "center",
					justifyContent: "flex-start",
					marginTop:listas.itemListaContestacoes.height*0.04,
				}}>
					{aprovada != null &&
					<View style={[{marginLeft:listas.itemListaContestacoes.width/40,marginRight:listas.itemListaContestacoes.width/40}]}>
						<Text style={[textos.texto]}>
							Remuneração final: {remuneracao}
						</Text>
					</View>
					}
				</View>
				<View style={{
					flexDirection:"row",
					alignItems: "space-around",
					justifyContent: "space-around",
					marginTop:listas.itemListaContestacoes.height*0.04,
				}}>
					<Pressable
						style={[botoes.pressableItemContestacao,]}
						onPress={() =>{atttVisibiJust(!justificativaVisivel);}}
					>
						<View style={[{marginLeft:listas.itemListaContestacoes.width/40,marginRight:listas.itemListaContestacoes.width/40}]}>
							<Text style={[textos.texto,{fontSize:textos.texto.fontSize*0.8}]}>
								Sua justificativa
							</Text>
						</View>
					</Pressable>
					{resposta != null && resposta != "" &&
					<Pressable
						style={[botoes.pressableItemContestacao,]}
						onPress={() =>{atttVisibiResp(!respostaVisivel);}}
					>				
						<View style={[{marginLeft:listas.itemListaContestacoes.width/40,marginRight:listas.itemListaContestacoes.width/40}]}>
							<Text style={[textos.texto,{fontSize:textos.texto.fontSize*0.8}]}>
								Resposta do Bike SP
							</Text>
						</View>
					</Pressable>}
				</View>
			</View>}
			{justificativaVisivel && !respostaVisivel &&
			<View>
				<ExibicaoTextao titulo={"Sua justificativa:"} textao={justificativa} attVisibilidade={atttVisibiJust}/>
			</View>}
			{respostaVisivel && !justificativaVisivel &&
			<View>
				<ExibicaoTextao titulo={"Resposta do Bike SP:"} textao={resposta} attVisibilidade={atttVisibiResp}/>
			</View>}
		</View>			
	);
}

ItemContestacao.propTypes = {
	idViagem: PropTypes.number,
	dataContestacao: PropTypes.string,
	justificativa: PropTypes.string,
	aprovada: PropTypes.bool,
	resposta: PropTypes.string,
	remuneracao: PropTypes.string,
};


function ExibicaoTextao(props){
	let titulo = props.titulo;
	let textao = props.textao;
	let attVisibilidade = props.attVisibilidade;
	return (
		<View style={[listas.itemListaContestacoes,globais.shadowProp]}>
			<View style={[{
				flexDirection:"row",
				alignItems:"space-between",
				justifyContent:"space-between",
				marginTop:listas.itemListaContestacoes.height*0.05,
			}]}>
				<Text style={[textos.texto,{fontSize:textos.texto.fontSize*1.2}]}>
					{titulo}
				</Text>
				<Pressable
					style={[botoes.pressableItemContestacao,]}
					onPress={() =>{attVisibilidade(false);}}
				>			
					<View>
						<Text style={[textos.texto,{fontWeight:"bold"}]}>
								Fechar
						</Text>
					</View>
				</Pressable>
			</View>

			<View style={{
				flexDirection:"row",
				alignItems: "center",
				justifyContent: "flex-start",
				marginTop:listas.itemListaContestacoes.height*0.04,
			}}>
				<ScrollView style={[{
					marginLeft:listas.itemListaContestacoes.width/40,
					marginBottom:listas.itemListaContestacoes.width/7.8,
					paddingRight: listas.itemListaContestacoes.width/40}]}
				persistentScrollbar={true}
				nestedScrollEnabled = {true}
				>
					<Text style={[textos.texto,{fontSize:textos.texto.fontSize*0.9,fontStyle: "italic",textAlign:"justify"}]}>
						{textao}
					</Text>
				</ScrollView>
			</View>
		</View>
	);
}

ExibicaoTextao.propTypes = {
	titulo: PropTypes.string,
	textao: PropTypes.string,
	attVisibilidade: PropTypes.func,
};

export {ItemContestacao};
