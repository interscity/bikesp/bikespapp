import React from "react";

import {
	Text,
	View,
	Pressable,
	Image,
	ScrollView
} from "react-native";

import PropTypes from "prop-types";

import { textos } from "../../estilos/textos";
import { botoes } from "../../estilos/botoes";
import { containeres } from "../../estilos/containeres";
import { em } from "../../estilos/globais";

function PopupGenerico (props) {

	const callbackAceito = props.callbackAceito;
	const paragrafos = props.texto;
	const mostrarIcone = props.mostrarIcone;
	const multiplicadorAltura = 1-props.fatorEncolhimento;
	const scrollable = props.scrollable;
	let BotaoAceite = () => (
		<Pressable style = {[botoes.botaoNav]} onPress={callbackAceito} testID={props.testID + ".aceita"}>
			<Text style={[textos.texto, {margin: 0.5*em}]}>{props.textoAceito}</Text>
		</Pressable>
	);

	return (
		<View style={[containeres.popUp, props.style]} testID={props.testID} >
			<View style={[containeres.popUpPermissoes,{
				height: multiplicadorAltura * containeres.popUpPermissoes.height
			}]}>
				{mostrarIcone &&  <Image source = {require("../../../assets/images/exemplo_logo.png")} style={{height:containeres.popUpPermissoes.height*0.08, width:containeres.popUpPermissoes.height*0.08, alignSelf:"center"}} />}
	

				{scrollable ?
					<View style={[{
						maxHeight: multiplicadorAltura * containeres.popUpPermissoes.height * 0.8
					}]}>
						<ScrollView
							persistentScrollbar={true}
						>
							{paragrafos.map((pTexto,i) => (
								<Text key={i} style={[textos.texto]}> 
									{pTexto}
								</Text>
							))}
						</ScrollView>
					</View>
					:
					paragrafos.map((pTexto,i) => (
						<Text key={i} style={[textos.texto,{textAlign:"center", alignSelf:"center"}]}> 
							{pTexto}
						</Text>
					))
				}

				{props.textoRecusa === "" ?
					<BotaoAceite/>
					:
					<View style={containeres.containerHorizontal}>
						<Pressable style = {[botoes.botaoNav]} onPress={props.callbackRecusa}>
							<Text style={[textos.texto, {margin: 0.5*em}]}>{props.textoRecusa}</Text>
						</Pressable>
						<BotaoAceite/>
					</View>
				}
			</View>
		</View>
	);

}

PopupGenerico.defaultProps = {
	mostrarIcone: false,
	testID: "",
	textoAceito: "Entendi",
	// testIDAceita: "",
	textoRecusa: "",
	// testIDRecusa: "",
	fatorEncolhimento: 0,
	scrollable: false
};

PopupGenerico.propTypes = {
	mostrarIcone: PropTypes.bool,
	testID: PropTypes.string,
	callbackAceito: PropTypes.func,
	textoAceito: PropTypes.string,
	// testIDAceita: PropTypes.string,
	callbackRecusa: PropTypes.func,
	textoRecusa: PropTypes.string,
	// testIDRecusa: PropTypes.string,
	texto: PropTypes.arrayOf(PropTypes.string),
	fatorEncolhimento: PropTypes.number,
	scrollable: PropTypes.bool,
	style: PropTypes.object
};

export {PopupGenerico};
