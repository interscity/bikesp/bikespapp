import React, {useContext} from "react";

import {
	Pressable,
	View
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";

import PropTypes from "prop-types";

import { listas } from "../../estilos/listas.js";
import { textos } from "../../estilos/textos.js";
import {  corAmareloOuro, globais } from "../../estilos/globais.js";

import {FormataMoeda,FormataData,FormataDistancia,LimitaExibicaoString,GetApelidoLocalizacaoPorId,GetDistanciaIdealTrajetoIdeal} from "../../classes/utils.js";

import {contextoGerenciadorLocal} from "../../contextos/contextoGerenciadorLocal.js";

import Texto from "../textos/Texto";
import { containeres } from "../../estilos/containeres.js";
import {useNavigation} from "@react-navigation/native";

/**
 * Componente responsável por encapsular um dos itens da Lista de Viagens da página do
 * Histórico de Viagens
 * 
 * Deve fazer um link (via navigation) à página específica da viagem (Detalhes da Viagem)
 * 
 * Recebe em props:
 *   viagem: Objeto da Classe Viagem.
 */
function ItemListaViagens(props){

	const navigation = useNavigation();

	const gDadosLocais = useContext(contextoGerenciadorLocal);

	const iconeEstadoSize = 1.2*textos.texto.fontSize;

	const viagem = props.viagem;
	// const idViagem = viagem.idViagem;
	const dataDia = FormataData(viagem.data,"DD/MM");
	const dataTempo = FormataData(viagem.data,"HH:mm");

	const idOrigem = viagem.idOrigem;
	const idDestino = viagem.idDestino;

	const listaLocs = JSON.parse(gDadosLocais.getLocalmenteMem("Locais"));
	const paresValidos = JSON.parse(gDadosLocais.getLocalmenteMem("ParesValidos"));

	const apelidoOrigem = GetApelidoLocalizacaoPorId(idOrigem, listaLocs);
	const apelidoDestino = GetApelidoLocalizacaoPorId(idDestino, listaLocs);

	const estadoRemuneracao = viagem.statusRemuneracao;
	const remuneracao = FormataMoeda(viagem.remuneracao);	

	let distanciaEtrajeto;
	if (apelidoOrigem == "Desconhecido" || apelidoDestino == "Desconhecido"){
		distanciaEtrajeto = [viagem.distancia,{"posicoes":[]}];
	}
	else{
		distanciaEtrajeto = GetDistanciaIdealTrajetoIdeal(idOrigem,idDestino,paresValidos);
	}

	let distancia = FormataDistancia(distanciaEtrajeto[0]);
	
	let iconeEstado;

	switch (estadoRemuneracao){
	case "Reprovado":
		iconeEstado = <Icon name="close-circle-outline" 
			size={iconeEstadoSize}
			color="#a34040"
		/>;
		break;
	case "Aprovado":
		iconeEstado = <Icon name="checkmark-circle-outline" 
			size={iconeEstadoSize}
			color="#65db65"
		/>;
		break;
	case "EmAnalise":
		iconeEstado = <IconMaterial name="clock-outline"
			size={iconeEstadoSize}
			color="#aaaaaa"
		/>;	
		break;
	default:
		console.error("Status da Viagem Inválido:",estadoRemuneracao);
	}

	return (

		<Pressable
			style={[listas.itemLista,listas.itemListaViagens,globais.shadowProp,{width:"98%"}]}
			onPress={() => navigation.navigate("DetalhesViagem",{viagem:JSON.parse(viagem.paraJSON())})}
		>
			<Texto centro multiplicadorTamanho={0.8}>
				{dataDia}{"\n"}
				{dataTempo}
			</Texto>

			<Texto centro multiplicadorTamanho={0.8}>
				Viagem de {distancia} {"\n"}
				{LimitaExibicaoString(apelidoOrigem,10)} a {LimitaExibicaoString(apelidoDestino,10)}
			</Texto>

			<View style={[containeres.containerHorizontal]}>
				<View style={[containeres.containerVertical]}>
					<Texto multiplicadorTamanho={0.8}>
						{remuneracao}
					</Texto>
					{iconeEstado}
				</View>		
				<Icon name="chevron-forward-outline" size={iconeEstadoSize} color={corAmareloOuro}/>
			</View>
		</Pressable>

	);

}

ItemListaViagens.propTypes = {
	viagem: PropTypes.object,
};


export {ItemListaViagens};
