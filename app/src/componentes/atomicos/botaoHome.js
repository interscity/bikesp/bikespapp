import React from "react";
import {
	View,
	TouchableHighlight
} from "react-native";

import PropTypes from "prop-types";

import { botoes } from "../../estilos/botoes";
import { textos } from "../../estilos/textos";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Texto from "../textos/Texto";
import {useNavigation} from "@react-navigation/native";


function BotaoHome(props){

	const navigation = useNavigation();

	let	funcSucesso = () => {navigation.navigate(props.telaDestino,props.parametrosRota);};
	if(props.forceReplace){
		funcSucesso = () => {navigation.replace(props.telaDestino,props.parametrosRota);};
	}

	return (
		<TouchableHighlight 
			disabled={props.disabled}
			testID={props.testID}
			activeOpacity={0.2}
			underlayColor="#DDDDDD"
			style={{borderRadius: botoes.botaoNav.borderRadius}}
			onPress={async () => { (await props.checkCallback()) && (funcSucesso() || props.callback() );}}
		>	
			<View style={[botoes.botaoNav,props.estiloAux]}>
				{props.returnIcon && <Icon name="menu-left-outline"
					size={1.5*textos.texto.fontSize}
					color={botoes.botaoNav.borderColor} 
				/>}
				<Texto centro 
					//style={[textos.texto, {width: botoes.botaoNav.minWidth,fontSize:1.5*textos.texto.fontSize}]}
				>
					{props.texto}
				</Texto>
			</View>
		</TouchableHighlight>
	);
}

BotaoHome.defaultProps = {
	disabled:false,
	returnIcon: false,
	forceReplace: false,
	estiloAux: {},
	checkCallback:()=> (true),
	callback: ()=>{},
	testID: ""
};

BotaoHome.propTypes = {
	disabled: PropTypes.bool,
	returnIcon: PropTypes.bool,
	forceReplace: PropTypes.bool,
	texto: PropTypes.string,
	estiloAux: PropTypes.object,
	telaDestino: PropTypes.string,
	parametrosRota: PropTypes.object,
	checkCallback: PropTypes.func,
	callback:PropTypes.func,
	testID: PropTypes.string
};

export {BotaoHome};

