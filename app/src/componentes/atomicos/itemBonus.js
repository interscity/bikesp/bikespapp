import React from "react";

import {
	Text,
	View,
} from "react-native";

import PropTypes from "prop-types";

import { listas } from "../../estilos/listas";
import { textos } from "../../estilos/textos";
import { globais } from "../../estilos/globais";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";


function ItemBonus(props){
	
	const idBonus = props.idBonus;
	const nome = props.nome;
	const valor = props.valor;
	const ativo = props.ativo;
	const descricao = props.descricao;
	// const dataInicio = props.dataInicio;
	// const dataFim = props.dataFim;
	let dataConcessao = props.dataConcessao;

	let concedido = false;
	if (dataConcessao != null){
		concedido = true;
	}

	return (
		<View style={[listas.itemListaBonus,globais.shadowProp,  {height: "auto", flexDirection: "column", }]}>
			<View style={[{
				flexDirection:"column",
				alignItems:"flex-start",
				justifyContent:"flex-start",
				marginTop:listas.itemListaBonus.height*0.05,
			}]}>
				<Text style={[textos.texto,{fontWeight:"bold",fontSize:textos.texto.fontSize*0.9}]}>
					{nome} {" "}
					<Text style={[{fontWeight:"normal"}]}>
						(#{idBonus})
					</Text>
				</Text>
			</View>
			<View style={{
				flexDirection:"row",
				alignItems: "center",
				justifyContent: "flex-start",
				marginTop:listas.itemListaBonus.height*0.04,
			}}>
				<Text style={[textos.texto]}>
					Concedido:
				</Text>
				<View>
					{concedido ?
						<Icon name="checkbox-marked" size={textos.subtitulo.fontSize} color="#099642"  />:
						ativo ? <Icon name="checkbox-blank" size={textos.subtitulo.fontSize} color="#d1d1c9"  />:
							<Icon name="close-box-outline" size={textos.subtitulo.fontSize} color="#a34040"  />
					}
				</View>
				<View style={[{marginLeft:listas.itemListaBonus.width/40,marginRight:listas.itemListaBonus.width/40}]}>
					<Text style={[textos.texto]}>
						Valor: {valor}
					</Text>
				</View>
			</View>
			{descricao != null &&
			<View>
				<Text style={[textos.texto]}>
					Descrição: {descricao}
				</Text>
			</View>}
		</View>				
	);

}

ItemBonus.propTypes = {
	idBonus: PropTypes.number,
	nome: PropTypes.string,
	valor: PropTypes.string,
	ativo: PropTypes.bool,
	descricao: PropTypes.string,
	dataInicio: PropTypes.string,
	dataFim: PropTypes.string,
	dataConcessao: PropTypes.string
};


export {ItemBonus};
