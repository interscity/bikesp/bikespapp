import React from "react";
import PropTypes from "prop-types";
import { Pressable } from "react-native";
import Texto from "../textos/Texto";
import { botoes } from "../../estilos/botoes";
import { ajustaCor, corAmareloOuro } from "../../estilos/globais";
import { Icon } from "react-native-vector-icons/MaterialCommunityIcons";
import { textos } from "../../estilos/textos";

/**
 * Componente de botão genérico com texto e callback associada ao evento `onPress`, encapsula um `Pressable`.
 * A formatação padrão é `botoes.botao`
 * @param {string} props.destaque aplica a cor informada como cor de fundo e deixa o texto interno em negrito 
 * @param {boolean} props.desativado booleando que indica se o botão deve ser desativado
 * @param {function} props.dispara callback a ser chamada pelo evento `onPress`
 * @param {object} props.navega objeto contendo o objeto navigation, a tela destino como string e a informação 
 * se a navegação deve substituir a tela atual ou só adicionar ela à pilha {destino: "AlgumaTela", substitui: false, navigation: navigation}
 * @param {string} props.texto conteúdo textual do botão
 * @param {string} props.icone icone a ser usado como conteúdo do botão (sobreescreve o `texto` passado)
 * @param {string} props.testID 
 * @returns 
 */
export default function Botao(props) {
	const navegaFunc = props.navega 
		&& (props.navega.substitui 
			? props.navega.navigation.replace 
			: props.navega.navigation.navigation);

	return <Pressable style={({pressed}) =>  [
		botoes.botao, 
		props.destaque && {backgroundColor: props.destaque, borderColor: ajustaCor(props.destaque, -20)}, 
		props.desativado && botoes.inativo,
		pressed && botoes.pressionado,
	]}
	onPress={async () => {
		await props.preDisparo() 
		&& (navegaFunc() || props.dispara()); 
	}}
	disabled={props.desativado}
	testID={props.testID}
	>
		{props.icone ? 
			<Icon name={props.icone} size={textos.icone.size} color={corAmareloOuro}/>
			: 
			<Texto negrito={props.destaque}>{props.texto}</Texto>
		}
	</Pressable>;
}

Botao.defaultProps = {
	desativado: false,
	dispara: () => {},
	preDisparo: () => true,
	navega: {}
};

Botao.propTypes = {
	destaque: PropTypes.string,
	desativado: PropTypes.bool,
	dispara: PropTypes.func,
	preDisparo: PropTypes.func,
	navega: PropTypes.object,
	texto: PropTypes.string,
	icone: PropTypes.string,
	testID: PropTypes.string
};
