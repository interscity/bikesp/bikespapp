import killServer from "../__tests__/testSetup/jestAfterTests.js";
import resetaBD from "../__tests__/testSetup/resetaBD.js";

module.exports = async () => {
	await resetaBD();
	killServer();
	require('../node_modules/detox/internals.js').cleanup();
}
