/* eslint-disable no-undef */
import { clicaEntendiHome, clicaEntendiGenerico , 
	logaNoApp, clicaIniciarViagem, cadastraNoApp, 
clicaOkGenerico, validaLocalizacoes, passaFluxoPreCadastro, passaFluxoPreHome } from "../setupFunctions.js";
import resetaBD from "../../__tests__/testSetup/resetaBD.js";

import atualizaLocalizacao from "../atualizaLocalizacao.js";

import viagens from "../viagensMockadas.json";
time = new Date().getTime();
const intervaloEntreUpdate = 5000;
const timeoutParaRenderizar = 5000;

function testeViagem(viagem, nomeDaViagem) {

	describe("Testando a viagem " + nomeDaViagem, () => {
		beforeAll(async() => {
			await resetaBD();
			
			coordenadaInicial = [viagem[0]["lat"],viagem[0]["lng"]];
			await atualizaLocalizacao(1,coordenadaInicial[0],coordenadaInicial[1]);

			await device.launchApp({
				launchArgs: {
					mockPosition: "true",
					positions: viagem,
					interval: intervaloEntreUpdate
				}
			});
			await passaFluxoPreCadastro();
			await cadastraNoApp();
			await logaNoApp();			
			await validaLocalizacoes();
			await passaFluxoPreHome();
			await clicaIniciarViagem();
		});

		it("Testa o trajeto", async () => {
			const iniciaViagem = element(by.text("Iniciar Viagem"));
			await expect(iniciaViagem).toBeVisible();

			await device.disableSynchronization();
			await iniciaViagem.longPress();
			
			await waitFor(element(by.text("Finalizar viagem"))).toBeVisible().withTimeout(timeoutParaRenderizar);
			const finalizaViagem = element(by.text("Finalizar viagem"));

			await new Promise(r => setTimeout(r, viagem.length * intervaloEntreUpdate));

			await finalizaViagem.longPress();
			await device.enableSynchronization();
		});

	});
}


for (let i = 0; i < viagens.length; i++) {
	console.log(viagens);
	testeViagem(viagens[i].data, viagens[i].nomeDaViagem);
}