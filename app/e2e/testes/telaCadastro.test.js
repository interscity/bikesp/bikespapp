import constantes from "../../constantes.json";

import resetaBD from "../../__tests__/testSetup/resetaBD.js";
import { email,senha,bilheteUnico,cpf,passaFluxoPreCadastro } from "../setupFunctions.js";

/* eslint-disable no-undef */
describe("Cadastro", () => {
	beforeAll(async () => {
		await resetaBD();
		await device.launchApp();
	});

	it("Um usuário deve conseguir percorrer o fluxo de pré cadastro", async () => {

		const botaoAvancar = element(by.text("Avançar"));
		await expect(botaoAvancar).toBeVisible();
		await botaoAvancar.longPress();

		const botaoVamosLa = element(by.text("Vamos lá!"));
		await expect(botaoVamosLa).toBeVisible();
		await botaoVamosLa.longPress();
	});

	it("Um usuário deve conseguir se cadastrar dentro do aplicativo", async () => {

		const botaoCadastreSe = element(by.id(constantes.cadastreSeTestID));
		await expect(botaoCadastreSe).toBeVisible();

		await botaoCadastreSe.longPress();

		const emailInput = element(by.id(constantes.emailCadastroTestID));
		const cpfInput = element(by.id(constantes.cpfCadastroTestID));
		const bilheteUnicoInput = element(by.id(constantes.bilheteUnicoCadastroTestID));
		const senhaInput = element(by.id(constantes.senhaCadastroTestID));
		const confirmaSenhaInput = element(by.id(constantes.confirmaSenhaCadastroTestID));

		await emailInput.replaceText(email);
		await cpfInput.replaceText(cpf);
		await bilheteUnicoInput.replaceText(bilheteUnico);
		await senhaInput.replaceText(senha);
		await confirmaSenhaInput.replaceText(senha);

		await expect(emailInput).toBeVisible();
		await expect(cpfInput).toBeVisible();
		await expect(bilheteUnicoInput).toBeVisible();
		await expect(confirmaSenhaInput).toBeVisible();

		await element(by.id(constantes.scrollCadastroTestID)).scrollTo('bottom');

		const botaoCadastro = element(by.text("Cadastrar"));

		await expect(botaoCadastro).toBeVisible();

		await expect(emailInput).toHaveText(email);
		await expect(bilheteUnicoInput).toHaveText(bilheteUnico);
		await expect(senhaInput).toHaveText(senha);
		await expect(confirmaSenhaInput).toHaveText(senha);
 
		await botaoCadastro.longPress();

		const botaoConfirmar = element(by.text("Voltar"));
		await expect(botaoConfirmar).toBeVisible();

		await botaoConfirmar.longPress();

		await expect(element(by.text("Fazer o login"))).toBeVisible();

	});


});