/* eslint-disable no-undef */
import { clicaEntendiHome, clicaEntendiGenerico, logaNoApp, cadastraNoApp,
	clicaOkGenerico, validaLocalizacoes,
passaFluxoPreCadastro, passaFluxoPreHome } from "../setupFunctions";
import constantes from "../../constantes.json";
import resetaBD from "../../__tests__/testSetup/resetaBD.js";

describe("Navegação da Home", () => {
	beforeAll( async () => {
		await resetaBD();
		await device.launchApp();
		await passaFluxoPreCadastro();
		await cadastraNoApp();
		await logaNoApp();
		await validaLocalizacoes();
		await passaFluxoPreHome();
	});

	it("O usuário deveria ser capaz de entrar na tela fale conosco", async () => {
		const navFaleConosco = element(by.id(constantes.homeToFaleConosco));
		await expect(navFaleConosco).toBeVisible();
		await navFaleConosco.longPress();
		await expect(element(by.text("FALE CONOSCO"))).toBeVisible();
		await device.pressBack();
	});

	it("O usuario deve conseguir navegar para a tela de locais cadastrados", async () => {
		const navVerLocais = element(by.id(constantes.homeToLocaisCadastrados));

		await expect(navVerLocais).toBeVisible();
		await navVerLocais.longPress();
		await expect(element(by.text("Locais Cadastrados"))).toBeVisible();
		await device.pressBack();
	});

	it("O usuario deve entrar na tela de historico de viagens", async () => {
		const navHistorico = element(by.id(constantes.homeToHistorico));

		await expect(navHistorico).toBeVisible();
		await navHistorico.longPress();
		await expect(element(by.text("Histórico de Viagens"))).toBeVisible();
		await device.pressBack();
	});

	it("O usuario deve conseguir navegar para a tela 'em viagem'", async () => {
		const navEmViagem = element(by.id(constantes.homeToIniciarViagem));

		await expect(navEmViagem).toBeVisible();
		await navEmViagem.longPress();
		await expect(element(by.text("Iniciar Viagem"))).toBeVisible();
		await device.pressBack();
	});
});