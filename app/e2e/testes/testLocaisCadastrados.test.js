/* eslint-disable no-undef */
import { clicaEntendiHome, logaNoApp, clicaLocaisCadastrados, 
	cadastraNoApp, clicaOkGenerico, validaLocalizacoes,
passaFluxoPreCadastro, passaFluxoPreHome } from "./../setupFunctions";

import resetaBD from "../../__tests__/testSetup/resetaBD.js";

describe("Locais Cadastrados", () => {
	beforeAll(async() => {
		await resetaBD();
		await device.launchApp();
		await passaFluxoPreCadastro();
		await cadastraNoApp();
		await logaNoApp();
		await validaLocalizacoes();
		await passaFluxoPreHome();
		await clicaLocaisCadastrados();
	});

	it("Os locais cadastrados deveriam ser os do usuário de teste", async () => {
		await expect(element(by.text("Residência"))).toBeVisible();
	});

});