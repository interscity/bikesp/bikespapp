import constantes from "../../constantes.json";
import resetaBD from "../../__tests__/testSetup/resetaBD.js";
import { email,senha,cadastraNoApp,clicaOkGenerico,validaLocalizacoes,passaFluxoPreCadastro,passaFluxoPreHome } from "../setupFunctions.js";

/* eslint-disable no-undef */
describe("Login", () => {
	beforeAll(async () => {
		await resetaBD();
		await device.launchApp();
		await passaFluxoPreCadastro();
		await cadastraNoApp();
	});

	it("Um usuário deve conseguir logar dentro do aplicativo", async () => {

		const emailInput = element(by.id(constantes.emailLoginTestID));
		const senhaInput = element(by.id(constantes.senhaLoginTestID));
		const botaoLogin = element(by.text("Fazer o login"));

		await expect(emailInput).toBeVisible();
		await expect(senhaInput).toBeVisible();
		await expect(botaoLogin).toBeVisible();
		await emailInput.replaceText(email);
		await senhaInput.replaceText(senha);

		await expect(emailInput).toHaveText(email);
		await expect(senhaInput).toHaveText(senha);

		await botaoLogin.longPress();

		await waitFor(element(by.text("Quase lá!"))).toBeVisible().withTimeout(10000);
	});

	it ("Um usuário deve conseguir dar Logout no app", async ()=> {
		await validaLocalizacoes();
		await waitFor(element(by.text("Permissões de Localização"))).toBeVisible().withTimeout(10000);

		await passaFluxoPreHome();

		const menuHome = element(by.id(constantes.menuHomeTestID));
		await expect(menuHome).toBeVisible();
		await menuHome.longPress();

		const menuSairButton = element(by.id(constantes.menuHomeSairTestID));
		await expect(menuSairButton).toBeVisible();
		await menuSairButton.longPress();

		const emailInput = element(by.id(constantes.emailLoginTestID));
		await expect(emailInput).toBeVisible();
	});

});