/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  rootDir: '..',
  testMatch: ['<rootDir>/e2e/testes/*.test.js'],
  testTimeout: 120000,
  maxWorkers: 1,
  globalSetup: '<rootDir>/e2e/globalSetup.js',
  globalTeardown: '<rootDir>/e2e/globalTeardown.js',
  reporters: ['detox/runners/jest/reporter'],
  testEnvironment: 'detox/runners/jest/testEnvironment',
  verbose: true,
  preset: "react-native",
  haste: {
    defaultPlatform: "android"
  },
  transformIgnorePatterns: [
    "<rootDir>/node_modules/(?!(@react-native|react-native|react-native-vector-icons)/)"
  ]
}; 
