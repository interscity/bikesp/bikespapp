/* eslint-disable no-undef */
import constantes from "../constantes.json";

export let email = "testador@email.c";
export let senha = "senhafake";
export let cpf = "12345678910";
export let bilheteUnico = "12345678910";

export async function logaNoApp() {

	const emailInput = element(by.id(constantes.emailLoginTestID));
	const senhaInput = element(by.id(constantes.senhaLoginTestID));
	const botaoLogin = element(by.text("Fazer o login"));
	
	await emailInput.replaceText(email);
	await senhaInput.replaceText(senha);

	await expect(emailInput).toHaveText(email);
	await expect(senhaInput).toHaveText(senha);

	await botaoLogin.longPress();
}

export async function validaLocalizacoes() {
	
	await waitFor(element(by.text("Quase lá!"))).toBeVisible().withTimeout(10000);
	const botaoVamosLa = element(by.text("Vamos lá!"));
	await botaoVamosLa.longPress();
	const botaoComecar = element(by.text("Começar"));
	await botaoComecar.longPress();

	const joia = element(by.id(constantes.joiaValidacaoLocalizacaoTestID));
	const botaoProximo = element(by.text("Próxima"));	
	while (true){
		await joia.longPress();
		try {
			await botaoProximo.longPress();
		}
		catch {
			break;
		}		
	}
	const botaoFinalizar = element(by.text("Finalizar"));
	await botaoFinalizar.longPress();
	const botaoConfirmar = element(by.text("Confirmar"));
	await botaoConfirmar.longPress();
}

export async function passaFluxoPreCadastro(){
	const botaoAvancar = element(by.text("Avançar"));
	await botaoAvancar.longPress();
	const botaoVamosLa = element(by.text("Vamos lá!"));
	await botaoVamosLa.longPress();
}

export async function passaFluxoPreHome(){
	await waitFor(element(by.text("Permissões de Localização"))).toBeVisible().withTimeout(10000);
	await element(by.id(constantes.permissoesOkInstrucionalPreHome1TestID)).longPress();
	await element(by.id(constantes.permissoesOkInstrucionalPreHome2TestID)).longPress();
	await element(by.id(constantes.permissoesOkInstrucionalPreHome3TestID)).longPress();
	await element(by.id(constantes.permissoesOkInstrucionalPreHome4TestID)).longPress();
	await element(by.id(constantes.permissoesOkInstrucionalPreHome5TestID)).longPress();
	await element(by.id(constantes.permissoesOkInstrucionalPreHome6TestID)).longPress();
}

export async function cadastraNoApp(){

	const botaoCadastreSe = element(by.id(constantes.cadastreSeTestID));
	
	await expect(botaoCadastreSe).toBeVisible();

	await botaoCadastreSe.longPress();

	const emailInput = element(by.id(constantes.emailCadastroTestID));
	const cpfInput = element(by.id(constantes.cpfCadastroTestID));
	const bilheteUnicoInput = element(by.id(constantes.bilheteUnicoCadastroTestID));
	const senhaInput = element(by.id(constantes.senhaCadastroTestID));
	const confirmaSenhaInput = element(by.id(constantes.confirmaSenhaCadastroTestID));

	await emailInput.replaceText(email);
	await cpfInput.replaceText(cpf);
	await bilheteUnicoInput.replaceText(bilheteUnico);
	await senhaInput.replaceText(senha);
	await confirmaSenhaInput.replaceText(senha);

	await element(by.id(constantes.scrollCadastroTestID)).scrollTo('bottom');

	const botaoCadastro = element(by.text("Cadastrar"));

	await botaoCadastro.longPress();

	const botaoConfirmar = element(by.text("Voltar"));

	await botaoConfirmar.longPress();
}


export async function clicaEntendiHome() {
	const entendiButton = element(by.id(constantes.permissoesEntendiButtonTestID));
	await entendiButton.longPress();
}

export async function clicaEntendiGenerico() {
	const entendiButton = element(by.text("Entendi"));
	await entendiButton.longPress();
}

export async function clicaOkGenerico() {
	const okButton = element(by.text("OK"));
	await okButton.longPress();
}

export async function clicaLocaisCadastrados() {
	const navVerLocais = element(by.id(constantes.homeToLocaisCadastrados));

	await navVerLocais.longPress(); 
}

export async function clicaIniciarViagem() {
	const navEmViagem = element(by.id(constantes.homeToIniciarViagem));
	await navEmViagem.longPress();
}
