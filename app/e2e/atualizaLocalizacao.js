/*
	Executa o script do servidor para atualizar uma localização
*/

import cwd from 'cwd';
import { exec } from 'child_process';

export default async function atualizaLocalizacao(idLocalizacao,latitude,longitude){

  	let executaAtualizaLocalizacao = 'bash --rcfile -c <(echo ". ~/.bashrc; poetry run python ' + process.cwd() +
	'/../servidor/tests/e2e/atualiza_localizacao.py -id ' + idLocalizacao + ' -lat ' + latitude + ' -lon ' + longitude +'")' ;
	
	return await new Promise((resolve, reject) => {
	      exec(executaAtualizaLocalizacao,
	        {shell:"/bin/bash",cwd:process.cwd() + "/../servidor/"},
	        function(
	          error,
	          stdout,
	          stderr
	      ) {
	          if (error !== null) {
	              console.log('exec error: ' + error);
	              return reject(error);
	          }
	          resolve();
	      });
	  });
}